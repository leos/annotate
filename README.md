## License
    Copyright 2022 European Commission

    Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
    You may not use this work except in compliance with the Licence.
    You may obtain a copy of the Licence at:

        https://joinup.ec.europa.eu/software/page/eupl

    Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the Licence for the specific language governing permissions and limitations under the Licence.


ANNOTATE APPLICATION
--------------------------------------------------------------------------------
Annotate 
This application is divided in following parts:
1)  client
2)  server
3)  config
4)  web

CLIENT
--------------------------------------------------------------------------------
Client part is an angular application which runs in the scope of another webapp 
    to provide annotation functionality.


SERVER
--------------------------------------------------------------------------------
Server part is an API server which expose an API for client to consume.


CONFIG
--------------------------------------------------------------------------------
Config part contains property files used by the application.


WEB
--------------------------------------------------------------------------------
Web part generates Web Application Resource (.war file) containing client and
    server functionality.


DEVELOPMENT OF APPLICATION
--------------------------------------------------------------------------------
Prerequisite
1) Maven 3.3.9+
2) JDK 1.8_0_151+
3) Maven should have internet connectivity. You may have to add proxy in maven 
    settings.xml and {annotate}\client\.npmrc
4) Tomcat 8.5.20, WebLogic 12.2.x or higher.
5) If Eclipse is used as development IDE, Oxygen release is recommended as it 
    supports the Tomcat server version mentioned.


COMPILING
--------------------------------------------------------------------------------
1) UNZIP the Zip in folder henceforth referred as {annotate}

2) Check or modify the database that should be used by the server application.
   To do so, set the according profile in file 
   {annotate}\config\src\main\filters\local.properties file or appropiate environment.
   - if you want to use H2 in-memory database, set
        annotate.db=h2
   - if you want to use Oracle database, set
        annotate.db=oracle
     and a maven dependency to Oracle database library has to be added to server or
     to web module.

3) To compile for environment {env} (possible values-dev/local), run the following 
   command in {annotate}
	$ mvn clean install -Denv={env}
   For example
	$ mvn clean install {by default local environment is used}
           or
	$ mvn clean install -Denv=local
	It injects appropriate values from property files.

4) Now, on successful finish, WAR file would be visible in {annotate}/web/target
    folder.

5) This WAR file can be deployed in Tomcat, WebLogic or any other application 
    server.

6) Configuration files generated on {annotate}/config/target/generated-config folder
    have to be added to application server classpath. These files will be loaded by
    application on start up.


DB SETUP
--------------------------------------------------------------------------------
A) For H2
    If you want to use H2 in-memory database, there is nothing more to do here.
    The H2 database is initialized automatically during deployment step using 
    the schema script file located at 
        {annotate}/server/src/main/resources/schema-h2.sql
    and filled with required data from the 
        {annotate}/server/src/main/resources/data-h2.sql

B) For Oracle
    If you want to use Oracle database, however, the database schema needs to be 
    created initially. Appropriate scripts for the creation of all required elements
    like tables, triggers, sequences, ... are found at 
        {annotate}/server/src/etc/scripts/database/consolidated/schema-oracle.sql
    1) Log in to your database and run these scripts, making sure you have  
        appropriate permissions to create these objects (CREATE SEQUENCE,  
        CREATE TABLE and CREATE TRIGGER should be sufficient).
    2) Next, some initial configuration needs to be inserted into the database. 
        This data is contained in a script that can be found at
         {annotate}/server/src/etc/scripts/database/consolidated/data-oracle.sql
    3) Log in to your database and run this script.


DEPLOYMENT
--------------------------------------------------------------------------------
A) For TOMCAT
    1) Create data source jdbc/leosDB for DB connections in context.xml.
    2) Deploy the WAR server-{env}.war in TOMCAT with context root at '/annotate' 
	    with port 9099
    3) Welcome page will be available at http://localhost:9099/annotate/app.html


B) For WEBLOGIC
    1) Create data source jdbc/leosDB for DB connections
    2) Deploy the WAR annotate.war in WebLogic at port 9099
    3) Welcome page will be available at http://localhost:9099/annotate/app.html


RUN in LOCAL
--------------------------------------------------------------------------------
    1) Run {annotate}/run-annotate.bat
    2) Check if the client is correctly loaded if replies the js resource http://localhost:9099/annotate/client/boot.js


NOTES
--------------------------------------------------------------------------------
1) The server can work with http/https both.
2) Configurable properties are present at following places
	{annotate}/config/src/main/filters/common.properties
	{annotate}/config/src/main/filters/{env}.properties
3) When using Oracle, you may need to install oracle driver in MAVEN as this is 
	not open source. 
4) When using Oracle, an entry for the default group needs to be created in the 
    GROUPS table. The "Name" column of this group needs to coincide to the corres-
    ponding property in common.properties file (defaultgroup.name).

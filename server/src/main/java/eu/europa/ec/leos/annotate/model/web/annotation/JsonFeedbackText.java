package eu.europa.ec.leos.annotate.model.web.annotation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonFeedbackText {
    private final String text;

    @JsonCreator
    public JsonFeedbackText(@JsonProperty("text") final String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }
}

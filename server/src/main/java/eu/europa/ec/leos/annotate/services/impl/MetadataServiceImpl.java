/*
 * Copyright 2018-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.services.impl;

import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.model.ResponseStatus;
import eu.europa.ec.leos.annotate.model.SimpleMetadata;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Document;
import eu.europa.ec.leos.annotate.model.entity.Group;
import eu.europa.ec.leos.annotate.model.entity.Metadata;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.model.web.ContributionStatusUpdateRequest;
import eu.europa.ec.leos.annotate.model.web.StatusUpdateRequest;
import eu.europa.ec.leos.annotate.repository.MetadataRepository;
import eu.europa.ec.leos.annotate.services.*;
import eu.europa.ec.leos.annotate.services.exceptions.CannotCreateMetadataException;
import eu.europa.ec.leos.annotate.services.exceptions.CannotUpdateAnnotationStatusException;
import eu.europa.ec.leos.annotate.services.exceptions.MissingPermissionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nonnull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MetadataServiceImpl implements MetadataService {

    private static final Logger LOG = LoggerFactory.getLogger(MetadataServiceImpl.class);

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------

    private AnnotationPermissionService annotPermService;
    private MetadataMatchingService metadataMatchingService;
    private DocumentService documentService;
    private GroupService groupService;
    private MetadataRepository metadataRepos;

    // -------------------------------------
    // Constructors & Setters
    // -------------------------------------

    public MetadataServiceImpl() {
        // required default constructor for autowired instantiation
    }

    @Autowired
    public MetadataServiceImpl(final AnnotationPermissionService annotPermService,
            final MetadataMatchingService metadataMatchingService, final DocumentService documentService,
            final GroupService groupService, final MetadataRepository metadataRepos) {
        this.annotPermService = annotPermService;
        this.metadataMatchingService = metadataMatchingService;
        this.documentService = documentService;
        this.groupService = groupService;
        this.metadataRepos = metadataRepos;
    }

    // -------------------------------------
    // Service functionality
    // -------------------------------------

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentGroupSystemid(final Document document, final Group group, final String systemId) {

        return metadataRepos.findByDocumentAndGroupAndSystemId(document, group, systemId);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentGroupSystemid(final String docUri, final String groupName, final String systemId) {

        final Document document = documentService.findDocumentByUri(docUri);
        if (document == null) {
            LOG.debug("No document found -> no metadata found");
            return new ArrayList<Metadata>();
        }

        final Group group = groupService.findGroupByName(groupName);
        if (group == null) {
            LOG.debug("No group found -> no metadata found");
            return new ArrayList<Metadata>();
        }

        return findMetadataOfDocumentGroupSystemid(document, group, systemId);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentGroupSystemidSent(final Document document, final Group group, final String systemId) {

        return metadataRepos.findByDocumentAndGroupAndSystemIdAndResponseStatus(document, group, systemId, ResponseStatus.SENT);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentSystemidGroupIds(final Document document, final String systemId, final List<Long> groupIds) {

        // null will provoke a Hibernate exception - we return an exception before, which is better understandable
        Assert.notNull(groupIds, "groupIds must not be null");

        return metadataRepos.findByDocumentAndSystemIdAndGroupIdIsIn(document, systemId, groupIds);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentSystemidSent(final Document document, final String systemId) {

        return metadataRepos.findByDocumentAndSystemIdAndResponseStatus(document, systemId, ResponseStatus.SENT);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfDocumentGroupSystemidInPreparation(final Document document, final Group group, final String systemId) {

        return metadataRepos.findByDocumentAndGroupAndSystemIdAndResponseStatus(document, group, systemId, ResponseStatus.IN_PREPARATION);
    }

    @Override
    @Nonnull
    public List<Metadata> findMetadataOfContributionId(final String contributionId) {

        final List<Metadata> iscMetadata = metadataRepos.findBySystemId(Authorities.ISC);
        if (iscMetadata.isEmpty()) {
            LOG.debug("No metadata entities found for systemId 'ISC'");
            return iscMetadata;
        }

        final SimpleMetadata metadataRequired = new SimpleMetadata();
        metadataRequired.put(Metadata.PROP_CONTRIBUTION_ID, contributionId);

        final List<Metadata> filteredMeta = new ArrayList<Metadata>();
        for (final Metadata candidateMeta : iscMetadata) {

            if (metadataMatchingService.areAllMetadataContainedInDbMetadata(
                    metadataRequired, candidateMeta)) {
                filteredMeta.add(candidateMeta);
            }
        }

        return filteredMeta;
    }

    @Override
    public List<Metadata> removeContributionsInPreparation(final List<Metadata> items) {

        if (CollectionUtils.isEmpty(items)) return new ArrayList<Metadata>();

        // note: result is never null, even if all items were filtered out
        return items.stream()
                .filter(meta -> !MetadataHandler.isIscContributionInPreparation(meta))
                .collect(Collectors.toList());
    }

    @Override
    public Metadata saveMetadata(final Metadata metadata) throws CannotCreateMetadataException {

        if (metadata == null) {
            LOG.error("Received metadata for saving is null!");
            throw new CannotCreateMetadataException(new IllegalArgumentException("Metadata is null"));
        }

        Metadata modifiedMetadata;
        try {
            modifiedMetadata = metadataRepos.save(metadata);
        } catch (Exception e) {
            LOG.error("Exception upon saving metadata");
            throw new CannotCreateMetadataException(e);
        }

        return modifiedMetadata;
    }

    @Override
    public void deleteMetadataById(final long metadataId) {

        try {
            metadataRepos.deleteById(metadataId);
        } catch (Exception e) {
            LOG.error("Could not delete metadata set with ID " + metadataId, e);
        }
    }

    @Override
    @Nonnull
    public List<Metadata> updateMetadata(final StatusUpdateRequest updateRequest,
            final UserInformation userInfo,
            final LocalDateTime timestamp)
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        if (userInfo == null) {
            throw new CannotUpdateAnnotationStatusException(new IllegalArgumentException("userInfo is null"));
        }

        // create a Metadata instance from given parameters - throws exception if required mandatory information is missing
        @SuppressWarnings("PMD.PrematureDeclaration") // variable is declared here to throw exception early, if needed
        final Metadata metaFromUpdateRequest = createMetadataFromStatusUpdateRequest(updateRequest);

        if (!annotPermService.userMayUpdateMetadata(userInfo)) {
            throw new MissingPermissionException(String.format("%s (%s)", userInfo.getLogin(), userInfo.getAuthority()));
        }

        // finally search for the items requiring to be updated
        // first step: get all candidates associated to document, group and system ID
        final String authority = userInfo.getAuthority();
        final List<Metadata> metaCandidates = findMetadataOfDocumentGroupSystemid(metaFromUpdateRequest.getDocument(), metaFromUpdateRequest.getGroup(),
                authority);
        if (metaCandidates.isEmpty()) {
            LOG.warn("No annotations are assigned to the group, document, authority/systemId; thus no metadata can be updated");
            return new ArrayList<Metadata>(); // we don't throw an exception any more since the caller might have more tasks to execute
        }

        // second step: keep only those having at least the required metadata
        final List<Metadata> filteredMetadata = metaCandidates.stream()
                .filter(meta -> metadataMatchingService.areAllMetadataContainedInDbMetadata(updateRequest.getMetadataToMatch(), meta))
                .collect(Collectors.toList());
        if (filteredMetadata.isEmpty()) {
            LOG.warn("No annotations assigned to the group, document, authority/systemId have the required properties; thus no metadata can be updated");
            return new ArrayList<Metadata>(); // we don't throw an exception any more since the caller might have more tasks to execute
        }

        // third step: finally do the update
        final List<Metadata> metadataSetsUpdated;

        if (updateRequest.isMigrateVersion()) {
            metadataSetsUpdated = increaseResponseVersionAndSave(filteredMetadata);
        } else {
            metadataSetsUpdated = updateResponseStatus(filteredMetadata, updateRequest, userInfo, timestamp);
        }

        return metadataSetsUpdated;
    }

    @Nonnull
    private List<Metadata> updateResponseStatus(final List<Metadata> filteredMetadata,
            final StatusUpdateRequest updateRequest,
            final UserInformation userInfo,
            final LocalDateTime timestamp) throws CannotUpdateAnnotationStatusException {

        final ResponseStatus targetRespStatus = updateRequest.getResponseStatus();
        final Long userId = userInfo.getUser().getId();
        final Group group = groupService.findGroupByName(updateRequest.getGroup());
        
        final List<Metadata> metadataSetsUpdated = new ArrayList<Metadata>();
        for (final Metadata metaFound : filteredMetadata) {

            if (!MetadataHandler.updateMetadataIfDifferentResponseStatus(metaFound, targetRespStatus, userId,
                    group.getId(), timestamp)) {
                LOG.debug("Skipping metadata set with id '{}' since it already has target response status", metaFound.getId());
                continue;
            }

            try {
                saveMetadata(metaFound);
            } catch (CannotCreateMetadataException e) {
                throw new CannotUpdateAnnotationStatusException(
                        String.format("Metadata with id '%s' could not be updated: %s", metaFound.getId(), e.getMessage()), e);
            }
            // keep track of successfully updated metadata sets
            metadataSetsUpdated.add(metaFound);
        }
        
        return metadataSetsUpdated;
    }
    
    @Nonnull
    private List<Metadata> increaseResponseVersionAndSave(final List<Metadata> filteredMetadata) 
            throws CannotUpdateAnnotationStatusException {
        
        final List<Metadata> metadataSetsUpdated = new ArrayList<Metadata>();
        for (final Metadata metaFound : filteredMetadata) {

            if (!MetadataHandler.increaseResponseVersion(metaFound)) {
                LOG.debug("Skipping metadata set with id '{}' since it has no response version");
                continue;
            }

            try {
                saveMetadata(metaFound);
            } catch (CannotCreateMetadataException e) {
                throw new CannotUpdateAnnotationStatusException(
                        String.format("Metadata with id '%s' could not be updated: %s", metaFound.getId(), e.getMessage()), e);
            }
            // keep track of successfully updated metadata sets
            metadataSetsUpdated.add(metaFound);
        }
        
        return metadataSetsUpdated;
    }
    
    @Override
    @Nonnull
    public List<Metadata> updateContributionStatus(final ContributionStatusUpdateRequest updateRequest,
            final UserInformation userInfo)
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        if (userInfo == null) {
            throw new CannotUpdateAnnotationStatusException(
                    new IllegalArgumentException("no user given for updating contribution status"));
        }
        Assert.notNull(updateRequest, "Required data for intended contribution update missing");
        Assert.hasLength(updateRequest.getContributionId(), "Contribution ID to be changed status missing");
        Assert.notNull(updateRequest.getContributionStatus(), "Intended target status for contributions missing");
        if (updateRequest.getContributionStatus() == ResponseStatus.UNKNOWN) {
            throw new IllegalArgumentException("Desired contribution status is undefined");
        }

        if (!annotPermService.userMayUpdateMetadata(userInfo)) {
            throw new MissingPermissionException(String.format("%s (%s)", userInfo.getLogin(), userInfo.getAuthority()));
        }

        // finally search for the items requiring to be updated
        final List<Metadata> filteredMetadata = findMetadataOfContributionId(updateRequest.getContributionId());
        if (filteredMetadata.isEmpty()) {
            LOG.warn("No annotations have the required contributionId or already have desired contributionStatus; thus no metadata can be updated");
            return new ArrayList<Metadata>(); // we don't throw an exception any more since the caller might have more tasks to execute
        }

        // do the update
        final ResponseStatus targetContribStatus = updateRequest.getContributionStatus();
        final List<Metadata> metadataSetsUpdated = new ArrayList<>();
        for (final Metadata metaFound : filteredMetadata) {

            if (!MetadataHandler.updateMetadataIfDifferentContributionStatus(metaFound, targetContribStatus)) {
                LOG.debug("Skipping metadata set with id '{}' since it already has target contributionStatus",
                        metaFound.getId());
                continue;
            }

            try {
                // keep track of successfully updated metadata sets
                metadataSetsUpdated.add(saveMetadata(metaFound));
            } catch (CannotCreateMetadataException e) {
                throw new CannotUpdateAnnotationStatusException(
                        String.format("Metadata with id '%s' could not be updated: %s", metaFound.getId(), e.getMessage()), e);
            }
        }
        return metadataSetsUpdated;

    }

    @Override
    public Metadata createMetadataFromStatusUpdateRequest(final StatusUpdateRequest updateRequest) throws CannotUpdateAnnotationStatusException {

        Assert.notNull(updateRequest, "information about the metadata to be updated is required");
        Assert.hasLength(updateRequest.getGroup(), "group assigned to metadata to be updated is required");
        Assert.hasLength(updateRequest.getUri(), "document URI assigned to metadata to be updated is required");

        final Document foundDocument = documentService.findDocumentByUri(updateRequest.getUri());
        if (foundDocument == null) {
            LOG.error("No document found with given URI; thus no metadata can be updated");
            throw new CannotUpdateAnnotationStatusException("Given URI is unknown");
        }

        final Group foundGroup = groupService.findGroupByName(updateRequest.getGroup());
        if (foundGroup == null) {
            LOG.error("No group with given name found; thus no metadata can be updated");
            throw new CannotUpdateAnnotationStatusException("Given group is unknown");
        }

        // we use ISC here as status update requests are valid for ISC only
        final Metadata metadata = new Metadata(foundDocument, foundGroup, Authorities.ISC);
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(metadata, updateRequest.getMetadataToMatch());
        return metadata;
    }

}

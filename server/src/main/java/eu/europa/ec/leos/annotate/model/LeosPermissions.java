package eu.europa.ec.leos.annotate.model;

import java.time.LocalDateTime;
import java.util.List;

public class LeosPermissions {
    private final String userId;
    private final List<String> permissions;
    private final LocalDateTime created;

    public LeosPermissions(final String userId,
                           final List<String> permissions) {
        this(userId, permissions, LocalDateTime.now());
    }

    public LeosPermissions(final String userId,
                           final List<String> permissions,
                           final LocalDateTime created) {
        this.userId = userId;
        this.permissions = permissions;
        this.created = created;
    }

    public String getUserId() {
        return this.userId;
    }

    public List<String> getPermissions() {
        return this.permissions;
    }

    public LocalDateTime getCreated() {
        return this.created;
    }
}

package eu.europa.ec.leos.annotate.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.leos.annotate.model.TemporaryAnnotations;
import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotation;
import eu.europa.ec.leos.annotate.services.TemporaryDataService;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCache;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCleanerRunnable;
import eu.europa.ec.leos.annotate.services.impl.util.ZipContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class TemporaryDataServiceImpl implements TemporaryDataService {
    private static final Logger LOG = LoggerFactory.getLogger(TemporaryDataServiceImpl.class);

    /**
     * Default interval for the clean task scheduler in minutes
     * */
    private static final int DEFAULT_CLEAN_TASK_SCHEDULER_INTERVAL = 15;

    /**
     * Time in minutes the data lives in the {@link TemporaryDataCache} until they are removed.
     * */
    private static final int TEMPORARY_DATA_TIME_TO_LIVE = 20;

    private int currentCleanTaskSchedulerInterval;

    private final ScheduledExecutorService schedulerExecutorService;
    private ScheduledFuture<?> scheduledFuture;

    public TemporaryDataServiceImpl() {
        this.currentCleanTaskSchedulerInterval = TemporaryDataServiceImpl.DEFAULT_CLEAN_TASK_SCHEDULER_INTERVAL;
        this.schedulerExecutorService = Executors.newScheduledThreadPool(1);
        this.scheduledFuture = null;
        this.startCleanTaskScheduler();
    }

    @Override
    public String storeTemporaryData(final TemporaryDataJson temporaryDataJson) {
        final TemporaryData temporaryData = this.getTemporaryDataCache().newTemporaryData(temporaryDataJson);
        return temporaryData.getId();
    }

    @Override
    public boolean removeTemporaryData(final String id) {
        try {
            return this.getTemporaryDataCache().removeTemporaryData(id);
        } catch (TemporaryDataNotFound ex) {
            return false;
        }
    }

    @Override
    public TemporaryData getTemporaryData(final String id) throws TemporaryDataNotFound {
        return this.getTemporaryDataCache().getTemporaryDataWithId(id);
    }

    @Override
    public TemporaryAnnotations getTemporaryAnnotations(final TemporaryData temporaryData, final String documentName) {
        Map<String, Object> jsonData = temporaryData.getTemporaryDataJson().getData();
        if (!jsonData.containsKey("annotations")) {
            return this.getEmptyTemporaryAnnotations(documentName);
        }

        List<TemporaryAnnotations> temporaryAnnotationsList = (List<TemporaryAnnotations>) jsonData.get("annotations");
        Optional<TemporaryAnnotations> filterResult = temporaryAnnotationsList.stream()
                .filter(temporaryAnnotations -> temporaryAnnotations.getDocumentName().equals(documentName))
                .findFirst();
        return filterResult.orElse(this.getEmptyTemporaryAnnotations(documentName));
    }

    private TemporaryAnnotations getEmptyTemporaryAnnotations(final String documentName) {
        return new TemporaryAnnotations(documentName, Collections.emptyList());
    }

    @Override
    public TemporaryAnnotations convertZipContentToTemporaryAnnotations(final ZipContent zipContent) throws IOException {
        final String documentName = zipContent.getName().replace("annot_", "").replace(".xml", "");
        return this.convertAnnotJsonToTemporaryAnnotations(documentName, zipContent.getData());
    }

    @Override
    public List<TemporaryAnnotations> readAnnotationsFromZipContent(final List<ZipContent> zipContentList) {
        return zipContentList.stream()
                .filter(zipContent -> zipContent.getName().startsWith("annot_"))
                .map(zipContent -> {
                    try {
                        return this.convertZipContentToTemporaryAnnotations(zipContent);
                    } catch(IOException ex) {
                        return null;
                    }
                }).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public TemporaryDataJson addTemporaryAnnotationsToTemporaryDataJson(List<TemporaryAnnotations> temporaryAnnotationsList) {
        final Map<String, Object> temporaryJsonData = new HashMap<>();
        temporaryJsonData.put("annotations", temporaryAnnotationsList);
        return new TemporaryDataJson(temporaryJsonData);
    }

    @Override
    public TemporaryAnnotations convertAnnotJsonToTemporaryAnnotations(final String documentName,
                                                                       final byte[] bytes) throws IOException {
        final Map<String, Object> annotJson = this.getObjectMapper().readValue(bytes, this.getHashMapTypeReference());
        return new TemporaryAnnotations(documentName, (List<JsonAnnotation>)annotJson.getOrDefault("rows", Collections.emptyList()));
    }

    private TypeReference<HashMap<String,Object>> getHashMapTypeReference() {
        return new TypeReference<HashMap<String,Object>>() {};
    }

    private TemporaryDataCache getTemporaryDataCache() {
        return TemporaryDataCache.getInstance();
    }

    @Override
    public boolean startCleanTaskScheduler() {
        if (this.isCleanTaskSchedulerRunning()) {
            return false;
        }

        final TemporaryDataCleanerRunnable runnable = new TemporaryDataCleanerRunnable(TemporaryDataServiceImpl.TEMPORARY_DATA_TIME_TO_LIVE,
                this.getTemporaryDataCache());
        this.scheduledFuture = this.schedulerExecutorService.scheduleAtFixedRate(runnable,
                this.currentCleanTaskSchedulerInterval,
                this.currentCleanTaskSchedulerInterval,
                TimeUnit.MINUTES);

        LOG.debug("Temporary data cleanup scheduler is running");
        return true;
    }

    @Override
    public void setCleanTaskInterval(final int interval) {
        boolean restartScheduler = this.isCleanTaskSchedulerRunning();
        if (restartScheduler) {
            this.stopCleanTaskScheduler();
        }

        this.currentCleanTaskSchedulerInterval = interval;

        if (restartScheduler) {
            this.startCleanTaskScheduler();
        }
    }

    @Override
    public boolean isCleanTaskSchedulerRunning() {
        if (this.scheduledFuture == null) {
            return false;
        }
        return !this.scheduledFuture.isCancelled() && !this.scheduledFuture.isDone();
    }

    @Override
    public boolean isTemporaryDataJson(final byte[] jsonBytes) {
        try {
            final TemporaryDataJson temporaryDataJson = this.getObjectMapper().readValue(jsonBytes, TemporaryDataJson.class);
            return (temporaryDataJson.getData() != null);
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public boolean stopCleanTaskScheduler() {
        if (this.scheduledFuture == null) {
            return false;
        }

        LOG.debug("Temporary data cleanup scheduler was stopped");
        return this.scheduledFuture.cancel(false);
    }

    @Override
    public int getCleanTaskInterval() {
        return this.currentCleanTaskSchedulerInterval;
    }

    private ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

}

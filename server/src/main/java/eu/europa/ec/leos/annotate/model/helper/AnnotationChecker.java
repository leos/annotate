/*
 * Copyright 2018-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.model.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

import eu.europa.ec.leos.annotate.model.entity.Annotation;

/**
 * this class contains helper functions for identifying certain natures of annotations;
 * if such nature is depending on metadata, the according functionality in {@link MetadataHandler} is called
 */
public final class AnnotationChecker {

    private static final Logger LOG = LoggerFactory.getLogger(AnnotationChecker.class);
    
    private AnnotationChecker() {
        // Prevent instantiation as all methods are static.
    }

    /**
     * check whether a given annotation represents a highlight
     * 
     * @param annotation 
     *        the annotation to be checked
     * 
     * @return true if the annotation was identified as a highlight
     */
    public static boolean isHighlight(final Annotation annotation) {

        Assert.notNull(annotation, "Required annotation missing");

        return TagListChecker.hasHighlightTag(annotation.getTags());
    }

    /**
     * checks if the associate metadata of the annotation has the response status set to "SENT"
     */
    public static boolean isResponseStatusSent(final Annotation annotation) {

        if (annotation.getMetadata() == null) {
            return false;
        }

        return MetadataHandler.isResponseStatusSent(annotation.getMetadata());
    }

    /**
     * check whether a given annotation represents a suggestion
     * 
     * @param annotation 
     *        the annotation to be checked
     * 
     * @return true if the annotation was identified as a suggestion
     */
    public static boolean isSuggestion(final Annotation annotation) {

        Assert.notNull(annotation, "Required annotation missing");

        return TagListChecker.hasSuggestionTag(annotation.getTags());
    }

    /**
     * states whether the given annotation denotes a reply to another annotation
     */
    public static boolean isReply(final Annotation annotation) {
        return annotation.getReferences() != null && !annotation.getReferences().isEmpty();
    }

    /**
     * checks if a given annotation represents a reply to another given annotation, and if the latter
     * represents an ISC contribution
     * 
     * @param annot
     *        the annotation to be checked for being a reply
     * @param rootAnnot
     *        the annotation to be checked for being the root of the annotation thread in which annot occurs
     */
    public static boolean isReplyOfIscContribution(final Annotation annot, final Annotation rootAnnot) {

        if (annot == null || rootAnnot == null) return false;
        if (!isReply(annot)) return false;

        // exit if the given root is not an absolute root for an annotation thread
        if (isReply(rootAnnot)) return false;

        // they don't belong together
        if (!annot.getRootAnnotationId().equals(rootAnnot.getId())) return false;

        // at this point we know that <annot> is a reply of <rootAnnot>
        return isIscContribution(rootAnnot);
    }

    public static boolean isIscAnnotation(final Annotation annot) {

        Assert.notNull(annot, "Given annotation is null!");
        return MetadataHandler.isIscMetadata(annot.getMetadata());
    }


    public static boolean isContribution(final Annotation annot) {

        Assert.notNull(annot, "Given annotation is null!");

        return MetadataHandler.isContribution(annot.getMetadata());
    }
    
    
    public static boolean isIscContribution(final Annotation annot) {

        Assert.notNull(annot, "Given annotation is null!");
        return isIscAnnotation(annot) && isContribution(annot);
    }
    

    public static Annotation findRoot(final Annotation annot, final List<Annotation> annots) {

        Assert.notNull(annot, "Undefined annotation's root cannot be searched for");
        if (CollectionUtils.isEmpty(annots)) return null;

        final String rootId = annot.getRootAnnotationId();
        if (!StringUtils.hasLength(rootId)) {
            LOG.debug("Given annotation with id '{}' has empty root ID, i.e. is a root itself", annot.getId());
            return null;
        }

        final Optional<Annotation> foundRoot = annots.stream()
                .filter(ann -> ann.getId().equals(rootId)).findFirst();
        if (!foundRoot.isPresent()) {
            LOG.warn("No root found for annotation with id '{}', expected to find '{}'",
                    annot.getId(), rootId);
            return null;
        }
        return foundRoot.get();
    }
}

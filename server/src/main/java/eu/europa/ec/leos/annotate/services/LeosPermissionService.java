package eu.europa.ec.leos.annotate.services;

import java.util.List;

public interface LeosPermissionService {
    void addUserPermission(final String userId, List<String> permissions);
    
    void addUserPermission(final String userId, String permissions);

    void removeUserPermission(final String userId);

    List<String> getUserPermissions(final String userId);

    void clearCache();

    void flushCache();

    int cacheSize();
}

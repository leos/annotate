package eu.europa.ec.leos.annotate.services.impl;

import eu.europa.ec.leos.annotate.model.LeosPermissions;
import eu.europa.ec.leos.annotate.services.LeosPermissionService;
import eu.europa.ec.leos.annotate.services.impl.util.LeosPermissionCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class LeosPermissionServiceImpl implements LeosPermissionService {
    private static final Logger LOG = LoggerFactory.getLogger(LeosPermissionServiceImpl.class);

    private final LeosPermissionCache leosPermissionCache;

    public LeosPermissionServiceImpl() {
        this.leosPermissionCache = LeosPermissionCache.getInstance();
    }

    @Override
    public void addUserPermission(final String userId, List<String> permissions) {
        this.leosPermissionCache.addUserPermissions(userId, permissions);
        LOG.debug("Leos permission for user with id '{}' added", userId);
    }

    @Override
    public void addUserPermission(final String userId, String permissions) {
        
        List<String> perms = Stream.of(permissions).collect(Collectors.toList());
        addUserPermission(userId, perms);
    }
    
    @Override
    public void removeUserPermission(final String userId) {
        this.leosPermissionCache.removeUserPermission(userId);
        LOG.debug("Leos permission for user with id '{}' removed", userId);
    }

    @Override
    public List<String> getUserPermissions(final String userId) {
        final LeosPermissions userPermissions = this.leosPermissionCache.getUserPermissions(userId);
        return (userPermissions != null) ? userPermissions.getPermissions() : Collections.emptyList();
    }

    @Override
    public void clearCache() {
        this.leosPermissionCache.clear();
    }

    @Override
    public void flushCache() {
        this.leosPermissionCache.flush();
    }

    @Override
    public int cacheSize() {
        return this.leosPermissionCache.size();
    }
}

/*
 * Copyright 2018 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.annotate.controllers;

import eu.europa.ec.leos.annotate.aspects.NoAuthAnnotation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;

/*
    <!--

        Copyright 2018 European Commission

        Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
        You may not use this work except in compliance with the Licence.
        You may obtain a copy of the Licence at:

            https://joinup.ec.europa.eu/software/page/eupl

        Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the Licence for the specific language governing permissions and limitations under the Licence.

    -->
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Annotate</title>
        <base target="_top" href="/"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" type="image/x-icon" href="favicon.ico"/>
        <script type="text/javascript">
            function addScriptElement(parent, type, src, innerText) {
                let scriptElement = window.document.createElement('script');
                scriptElement.setAttribute('type', 'text/javascript')
                if (src != null) {
                    scriptElement.setAttribute('src', src);
                }
                if (innerText != null) {
                    scriptElement.innerText = innerText;
                }
                parent.appendChild(scriptElement);
            }

            function addLinkElement(parent, rel, type, href) {
                let linkElement = window.document.createElement('link');
                if (rel != null) {
                    linkElement.setAttribute('rel', rel);
                }
                if (type != null) {
                    linkElement.setAttribute('type', type);
                }
                if(href != null) {
                    linkElement.setAttribute('href', href);
                }
                parent.appendChild(linkElement);
            }

            let queryParams = new URLSearchParams(window.location.search);
            let baseElement = window.document.getElementsByTagName('base')?.item(0);
            let basePath = queryParams.get('basePath');
            if (((basePath?.length || 0) > 0)) {
                baseElement?.setAttribute('href', basePath);
            } else {
                basePath = baseElement.getAttribute('href');
            }

            let now = Date.now();
            addScriptElement(window.document.head, 'text/javascript', `scripts/katex.min.js?v=${now}`, null);
            addScriptElement(window.document.head, 'text/javascript', null, `const CKEDITOR_BASEPATH="${basePath}${basePath.endsWith('/') ? '' : '/'}ckeditor/";`);
            addScriptElement(window.document.head, 'text/javascript', `ckeditor/ckeditor.js?v=${now}`, null);

            addLinkElement(window.document.head, 'stylesheet', null, `styles/katex.min.css?v=${now}`);
            addLinkElement(window.document.head, 'stylesheet', null, `toastr.css?v=${now}`);
            addLinkElement(window.document.head, 'stylesheet', null, `sidebar.css?v=${now}`);

            addScriptElement(window.document.body, 'module', `runtime.js?v=${now}`, null);
            addScriptElement(window.document.body, 'module', `polyfills.js?v=${now}`, null);
            addScriptElement(window.document.body, 'module', `main.js?v=${now}`, null);
        </script>
    </head>
    <body>
        <hypothesis-app></hypothesis-app>
    </body>
</html>
* */


@Controller
@RequestMapping("/")
public class AppController {
    @Value("${annotate.client.basePath}")
    private String defaultBasePath;

    @NoAuthAnnotation
    @RequestMapping(value = {"app.html"}, produces = MediaType.TEXT_HTML_VALUE)
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    public ModelAndView getAppHtml(final HttpServletRequest request, final HttpServletResponse response) {
        String basePath = request.getParameter("basePath");
        if (!StringUtils.hasLength(basePath)) {
            basePath = defaultBasePath;
        }
        if (!basePath.startsWith("/")) {
            basePath = "/" + basePath;
        }
        if (!basePath.endsWith("/")) {
            basePath =  basePath + "/";
        }

        final ModelAndView modelAndView = new ModelAndView("app");
        final ModelMap model = new ModelMap();
        model.addAttribute("basePath", basePath);
        model.addAttribute("version", String.valueOf(Instant.now().toEpochMilli()));
        modelAndView.addAllObjects(model);
        return modelAndView;
    }
}
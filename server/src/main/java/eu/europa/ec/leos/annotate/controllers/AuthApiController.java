/*
 * Copyright 2018-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.controllers;

import eu.europa.ec.leos.annotate.aspects.NoAuthAnnotation;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Token;
import eu.europa.ec.leos.annotate.model.helper.TokenHandler;
import eu.europa.ec.leos.annotate.model.web.token.JsonAuthenticationFailure;
import eu.europa.ec.leos.annotate.model.web.token.JsonTokenRequest;
import eu.europa.ec.leos.annotate.model.web.token.JsonTokenResponse;
import eu.europa.ec.leos.annotate.services.AuthenticationService;
import eu.europa.ec.leos.annotate.services.UserGroupService;
import eu.europa.ec.leos.annotate.services.UserService;
import eu.europa.ec.leos.annotate.services.exceptions.CannotStoreTokenException;
import eu.europa.ec.leos.annotate.services.exceptions.NoClientsAvailableException;
import eu.europa.ec.leos.annotate.services.exceptions.TokenFromUnknownClientException;
import eu.europa.ec.leos.annotate.services.exceptions.TokenInvalidForClientAuthorityException;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * API functions related to authentication
 */
@RestController
@RequestMapping("/api")
public class AuthApiController {
    private static final Logger LOG = LoggerFactory.getLogger(AuthApiController.class);

    private static final String GRANT_TYPE = "grant_type";
    private static final String BEARER_GRANT_TYPE = "jwt-bearer";
    private static final String BEARER_PARAMETER = "assertion";
    private static final String REFRESH_GRANT_TYPE = "refresh_token";
    private static final String REFRESH_GRANT_PARAMETER = "refresh_token";
    private static final String CONTEXT_PARAMETER = "context";

    private enum GrantType {
        Access,
        Refresh,
        Unsupported
    }
    
    // -------------------------------------
    // Required services and repositories
    // -------------------------------------
    @Autowired
    private AuthenticationService authService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserGroupService userGroupService;

    // -------------------------------------
    // Constructor
    // -------------------------------------

    // note: custom constructor in order to ease testability by benefiting from dependency injection
    @NoAuthAnnotation
    @Autowired
    public AuthApiController(final UserService userService, final AuthenticationService authService, final UserGroupService userGroupService) {
        if (this.userService == null) {
            this.userService = userService;
        }
        if (this.authService == null) {
            this.authService = authService;
        }
        if(this.userGroupService == null) {
            this.userGroupService = userGroupService;
        }
    }

    // -------------------------------------
    // API endpoints
    // -------------------------------------
    /**
     * Endpoint for token exchange 
     * 
     * transforms an id_token to an access token
     * verifies the id_token on that way
     *
     * @param request Incoming request
     * @param response Outgoing response, containing API new tokens (access, refresh) as JSON body
     *
     */
    @NoAuthAnnotation
    @PostMapping(value = {"/token"}, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @SuppressWarnings("PMD.ConfusingTernary")
    public ResponseEntity<Object> requestToken(final HttpServletRequest request,
                                               final HttpServletResponse response,
                                               @RequestBody final JsonTokenRequest jsonBody) {
        // all responses are supposed to be without caching
        LOG.info("Json request");
        setResponseHeader(response);
        final TokenRequest tokenRequest = TokenRequest.fromJsonTokenRequest(jsonBody);
        return _getToken(tokenRequest);
    }

    /**
     * Endpoint for token exchange
     *
     * transforms an id_token to an access token
     * verifies the id_token on that way
     *
     * @param request Incoming request
     * @param response Outgoing response, containing API new tokens (access, refresh) as JSON body
     *
     */
    @NoAuthAnnotation
    @PostMapping(value = {"/token"}, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @SuppressWarnings("PMD.ConfusingTernary")
    public ResponseEntity<Object> requestToken(final HttpServletRequest request,
                                               final HttpServletResponse response,
                                               @RequestParam final MultiValueMap<String,String> parameters) {
        // all responses are supposed to be without caching
        setResponseHeader(response);
        final TokenRequest tokenRequest = TokenRequest.fromMultiValueMap(parameters);
        return _getToken(tokenRequest);
    }

    /**
     * Endpoint for token exchange 
     * 
     * transforms an id_token to an access token
     * verifies the id_token on that way
     *
     * @param request Incoming request
     * @param response Outgoing response, containing API new tokens (access, refresh) as JSON body
     *
     */
    @NoAuthAnnotation
    @GetMapping(value = {"/token"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @SuppressWarnings("PMD.ConfusingTernary")
    public ResponseEntity<Object> getToken(final HttpServletRequest request,
                                           final HttpServletResponse response,
                                           @RequestParam(GRANT_TYPE) final String grantType,
                                           @RequestParam(BEARER_PARAMETER) final String assertion,
                                           @RequestParam(CONTEXT_PARAMETER) final String context,
                                           @RequestParam(REFRESH_GRANT_PARAMETER) final String refreshToken)  {
        // all responses are supposed to be without caching
        setResponseHeader(response);
        final TokenRequest tokenRequest = new TokenRequest(grantType, assertion, context, refreshToken);
        return _getToken(tokenRequest);
    }

    private ResponseEntity<Object> _getToken(final TokenRequest request) {
        // all responses are supposed to be without caching
        final String grantType = request.grantType;
        if (!StringUtils.hasLength(grantType)) {
            return new ResponseEntity<Object>(JsonAuthenticationFailure.getInvalidRequestResult(), HttpStatus.BAD_REQUEST);
        }

        final GrantType requestedGrant = getGrantType(grantType);
        JsonAuthenticationFailure errorResult;

        // request access token
        switch(requestedGrant)
        {
            case Access:
                try {
                    final Pair<Token, JsonAuthenticationFailure> result = getAccessToken(request);
                    final Token token = result.getValue0();
                    errorResult = result.getValue1();
                    if (token != null) {
                        return new ResponseEntity<Object>(new JsonTokenResponse(token), HttpStatus.OK);
                    }

                } catch (NoClientsAvailableException ncae) {
                    LOG.error("No clients are available", ncae);
                    errorResult = JsonAuthenticationFailure.getUnknownClientResult();
                } catch (TokenFromUnknownClientException tfuce) {
                    LOG.error("Error decoding JWT token", tfuce);
                    errorResult = JsonAuthenticationFailure.getUnknownClientResult();
                } catch (TokenInvalidForClientAuthorityException tifcae) {
                    LOG.error("JWT token received from authority not authorized to authenticate user", tifcae);
                    errorResult = JsonAuthenticationFailure.getTokenInvalidForClientAuthorityResult();
                } catch (Exception e) {
                    LOG.error("Unexpected error", e);
                    errorResult = JsonAuthenticationFailure.getUnknownUserResult();
                }

                break;

            case Refresh:
                final Pair<Token, JsonAuthenticationFailure> result = getRefreshToken(request);
                final Token token = result.getValue0();
                errorResult = result.getValue1();
                if (token != null) {
                    return new ResponseEntity<Object>(new JsonTokenResponse(token), HttpStatus.OK);
                }
                break;

            case Unsupported:
            default:
                errorResult = JsonAuthenticationFailure.getUnsupportedGrantTypeResult();
                break;
        }

        return new ResponseEntity<Object>(errorResult, HttpStatus.BAD_REQUEST);
    }

    private GrantType getGrantType(final String grantType) {

        if (!StringUtils.hasLength(grantType)) return GrantType.Unsupported;

        if (grantType.equals(REFRESH_GRANT_TYPE)) return GrantType.Refresh;
        if(grantType.contains(BEARER_GRANT_TYPE)) return GrantType.Access;
        
        return GrantType.Unsupported;
    }

    private UserInformation populateUserInfo(final TokenRequest request)
            throws NoClientsAvailableException, TokenFromUnknownClientException, TokenInvalidForClientAuthorityException {

        final UserInformation userInfo = authService.getUserLoginFromTokenWithContext(request.assertion,
                getRequestContext(request));
        LOG.debug("User '{} - {}' requesting for login with jwt-token", userInfo.getLogin(), userInfo.getContext());

        // create the user in our database, if not yet existing
        try {
            userInfo.setUser(userService.createUserIfNotExists(userInfo.getLogin(), userInfo.getContext()));
        } catch (Exception ex) {
            LOG.warn("Exception occurred while creating user, cannot create user! {}", ex.getMessage());
        }

        // check if user details are available in UD-REPO
        try {
            userInfo.setUserDetails(userService.getUserDetailsFromUserRepo(userInfo.getLogin(), userInfo.getContext()));
        } catch (Exception ex) {
            LOG.warn("Exception occurred while fetching user details from ud-repo: {}", ex.getMessage());
        }

        return userInfo;
    }

    private String getRequestContext(final TokenRequest request) {
        if (userService.isContextEnabled()) {
            return request.context;
        }
        return null;
    }

    /**
     * main logic for retrieval of a new access token for an incoming request
     * 
     * @param request incoming request
     * @return pair of {@link Token} and error (whereas only one is set)
     */
    private Pair<Token, JsonAuthenticationFailure> getAccessToken(final TokenRequest request)
            throws NoClientsAvailableException, TokenFromUnknownClientException, TokenInvalidForClientAuthorityException {

        final UserInformation userInfo = populateUserInfo(request);

        if (userInfo.getUserDetails() == null) {
            // user is not known in UD-REPO, so we deny access
            LOG.error("User '{}' was not found in UD-REPO, deny access!", userInfo.getLogin());
            return new Pair<>(null, JsonAuthenticationFailure.getUnknownUserResult());
        }

        // user was found, generate tokens and allow access
        final Token token = authService.createToken(userInfo);

        CompletableFuture<String> storeUserEntityToken =
             CompletableFuture.supplyAsync(()->{
                 userGroupService.addUserToEntityGroup(userInfo);

                 // store access token / refresh token in database
                 authService.saveTokensForUser(userInfo, token);
                return "done";
             });

        /* comment to make storage action asynchronous */
        try {
            storeUserEntityToken.get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Error while storing user '{}' and token '{}' information: '{}'", userInfo.getLogin(), token, e.getMessage());
        }

        return new Pair<>(token, null);
    }


    /**
     * main logic for retrieval of a new refresh token for an incoming request
     * @param request incoming request
     * @return pair of {@link Token} and error (whereas only one is set)
     */
    private Pair<Token, JsonAuthenticationFailure> getRefreshToken(final TokenRequest request) {

        JsonAuthenticationFailure errorResult = null;
        Token token = null;
        final String refreshTokenParam = request.refreshToken;

        // get user with matching refresh token
        final UserInformation userInfo = authService.findUserByRefreshToken(refreshTokenParam);
        if (userInfo == null) {
            // previous refresh token probably not provided -> error
            errorResult = JsonAuthenticationFailure.getInvalidRefreshTokenResult();

        } else {
            if (userInfo.getCurrentToken() == null) {

                // no token found -> unknown token received
                errorResult = JsonAuthenticationFailure.getInvalidRefreshTokenResult();
            } else {
                // note on test coverage: if token found and not expired, then a user is returned -> missed branch noted here cannot occur
                if (TokenHandler.isRefreshTokenExpired(userInfo.getCurrentToken())) {
                    // token found, but expired
                    errorResult = JsonAuthenticationFailure.getRefreshTokenExpiredResult();
                } else {
                    // token found and still valid
                    try {
                        // store new access token / refresh token in database
                        token = authService.generateAndSaveTokensForUser(userInfo);
                    } catch (CannotStoreTokenException cste) {
                        LOG.error("Unexpected error", cste);
                        errorResult = JsonAuthenticationFailure.getUnknownUserResult();
                    }
                }
            }
        }

        return new Pair<Token, JsonAuthenticationFailure>(token, errorResult);
    }

    private void setResponseHeader(final HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    }

    public static final class TokenRequest {
        private final String grantType;
        private final String assertion;
        private final String context;
        private final String refreshToken;

        public TokenRequest(final String grantType,
                            final String assertion,
                            final String context,
                            final String refreshToken) {
            this.grantType = grantType;
            this.assertion = assertion;
            this.context = context;
            this.refreshToken = refreshToken;
        }

        public static TokenRequest fromJsonTokenRequest(final JsonTokenRequest request) {
            return new TokenRequest(request.getGrantType(),
                    request.getAssertion(),
                    request.getContext(),
                    request.getRefreshToken());
        }

        public static TokenRequest fromMultiValueMap(final MultiValueMap<String,String> request) {
            return new TokenRequest(request.getFirst(GRANT_TYPE),
                    request.getFirst(BEARER_PARAMETER),
                    request.getFirst(CONTEXT_PARAMETER),
                    request.getFirst(REFRESH_GRANT_PARAMETER));
        }
    }
}

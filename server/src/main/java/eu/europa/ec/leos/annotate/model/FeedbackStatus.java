package eu.europa.ec.leos.annotate.model;

public enum FeedbackStatus {
    IN_PREPARATION(0L),
    SENT(1L);

    private final Long value;

    FeedbackStatus(final Long value) {
        this.value = value;
    }

    static public FeedbackStatus valueOf(final Long value) {
        if (value == null) return null;
        if (value == FeedbackStatus.IN_PREPARATION.value) {
            return FeedbackStatus.IN_PREPARATION;
        }
        if (value == FeedbackStatus.SENT.value) {
            return FeedbackStatus.SENT;
        }
        return null;
    }

    public Long getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}

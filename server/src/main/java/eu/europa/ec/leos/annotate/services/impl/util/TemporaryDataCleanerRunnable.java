package eu.europa.ec.leos.annotate.services.impl.util;

import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Runnable check for {@link TemporaryData} that are older than a defined threshold
 * */
public class TemporaryDataCleanerRunnable implements Runnable {
    private final int timeToLive;
    private final TemporaryDataCache temporaryDataCache;
    private final Object mutex;

    /**
     * Constructor
     * @param timeToLive Time in minutes an {@link TemporaryData} is stored
     * @param temporaryDataCache Cache instance to use
     * */
    public TemporaryDataCleanerRunnable(final int timeToLive,
                                        final TemporaryDataCache temporaryDataCache) {
        this.timeToLive = timeToLive;
        this.temporaryDataCache = temporaryDataCache;
        this.mutex = new Object();
    }

    @Override
    public void run() {
        synchronized(this.mutex) {
            List<TemporaryData> temporaryDataList = this.temporaryDataCache.getAll();
            List<String> temporaryDataIds = temporaryDataList.stream()
                    .filter(this::isRemoveTemporaryData)
                    .map(TemporaryData::getId)
                    .collect(Collectors.toList());
            for (final String temporaryDataId : temporaryDataIds) this.removeTemporaryData(temporaryDataId);
        }
    }

    /**
     * Check if {@link TemporaryData} reached time to live
     * @param temporaryData {@link TemporaryData} to check
     * @return `true` if the temporary data reach time to live
     * */
    private boolean isRemoveTemporaryData(final TemporaryData temporaryData) {
        final LocalDateTime maxLifeTime = temporaryData.getCreated().plusMinutes(this.timeToLive);
        return maxLifeTime.isBefore(LocalDateTime.now());
    }

    /**
     * Removed an {@link TemporaryData} from cache
     * @param id ID to remove from cache
     * */
    private void removeTemporaryData(final String id) {
        try {
            this.temporaryDataCache.removeTemporaryData(id);
        } catch(TemporaryDataNotFound ignored) {
        }
    }
}

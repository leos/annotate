/*
 * Copyright 2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.annotate.utils;

/**
 * Provide methods for string handling
 * */
public class StringUtils {
    /**
     * Checks if a string is empty
     * @param value String value to check
     * @return `true` in case the string is empty else `false`
     * */
    public static boolean isEmpty(final String value) {
        if (value == null) {
            return true;
        }
        return value.isEmpty();
    }

    /**
     * Checks if two strings are equal
     * @param value01 First string value to check
     * @param value02 Second value to check
     * @return `true` if strings are equal else `false`
     * */
    public static boolean isEqual(final String value01, final String value02) {
        if (value01 == null) {
            return (value02 == null);
        }
        if (value02 == null) {
            return false;
        }
        return value01.equals(value02);
    }
}

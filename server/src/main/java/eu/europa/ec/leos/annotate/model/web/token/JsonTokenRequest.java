/*
 * Copyright 2018-2023 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.model.web.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonTokenRequest {
    private final String grantType;
    private final String assertion;
    private final String context;

    private final String refreshToken;

    public JsonTokenRequest(final String grantType,
                            final String assertion,
                            final String context,
                            final String refreshToken) {
        this.grantType = grantType;
        this.assertion = assertion;
        this.context = context;
        this.refreshToken = refreshToken;
    }

    @JsonCreator
    public JsonTokenRequest(@JsonProperty("grant_type") final List<String> grantType,
                            @JsonProperty("assertion") final List<String> assertion,
                            @JsonProperty("context") final List<String> context,
                            @JsonProperty("refresh_token") final List<String> refreshToken) {
        this.grantType = getFirst(grantType);
        this.assertion = getFirst(assertion);
        this.context = getFirst(context);
        this.refreshToken = getFirst(refreshToken);
    }

    public String getGrantType() {
        return this.grantType;
    }

    public String getAssertion() {
        return this.assertion;
    }

    public String getContext() {
        return this.context;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    private String getFirst(final List<String> list) {
        if (list == null || list.isEmpty()) return null;
        return list.get(0);
    }
}

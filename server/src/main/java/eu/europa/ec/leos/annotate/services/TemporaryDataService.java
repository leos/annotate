package eu.europa.ec.leos.annotate.services;

import eu.europa.ec.leos.annotate.model.TemporaryAnnotations;
import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCache;
import eu.europa.ec.leos.annotate.services.impl.util.ZipContent;

import java.io.IOException;
import java.util.List;

/**
 * Provide methods to handle temporary data
 * */
public interface TemporaryDataService {
    /**
     * Store the temporary data property JSON in the {@link TemporaryDataCache}
     * @param temporaryDataJson JSON to store
     * @return ID of the created cache item
     * */
    String storeTemporaryData(final TemporaryDataJson temporaryDataJson);

    /**
     * Removes a temporary data item from the {@link TemporaryDataCache}
     * @param id ID of the item to remove
     * @return `true` if the item was removed else `false`
     * */
    boolean removeTemporaryData(final String id);

    /**
     * Read specific temporary Data from the {@link TemporaryDataCache}
     * @param id ID of the item to read
     * @return {@link TemporaryData} object
     * */
    TemporaryData getTemporaryData(final String id) throws TemporaryDataNotFound;

    /**
     * Search for annotations for a specified document within the json of {@link TemporaryData}
     * @param temporaryData {@link TemporaryData} object to look for
     * @param documentName Name of the document to look for
     * @return {@link TemporaryAnnotations} of the searched document. In case the annotations for the document are not found
     * an object with an empty annotations list is returned.
     * */
    TemporaryAnnotations getTemporaryAnnotations(final TemporaryData temporaryData, final String documentName);

    /**
     * Converts the content of a leg files annot file to {@link TemporaryAnnotations}
     * @param documentName Name of the document the annotations belong
     * @param bytes Content of the annot file as bytes
     * @return Object of {@link TemporaryAnnotations}
     * */
    TemporaryAnnotations convertAnnotJsonToTemporaryAnnotations(final String documentName, final byte[] bytes) throws IOException;

    /**
     * Converts the content of a leg files annot file to {@link TemporaryAnnotations}
     * @param zipContent Content to convert
     * @return Object of {@link TemporaryAnnotations}
     * */
    TemporaryAnnotations convertZipContentToTemporaryAnnotations(final ZipContent zipContent) throws IOException;

    /**
     * Read the annot files within a zip and return the content as {@link TemporaryAnnotations}
     * @param zipContentList Content of a zip file
     * @return List of {@link TemporaryAnnotations}
     * */
    List<TemporaryAnnotations> readAnnotationsFromZipContent(final List<ZipContent> zipContentList);

    /**
     * Add a list of {@link TemporaryAnnotations} to a {@link TemporaryDataJson}
     * @param temporaryAnnotationsList List of {@link TemporaryAnnotations} to add
     * @return {@link TemporaryDataJson} provide  a list of {@link TemporaryAnnotations}
     */
    TemporaryDataJson addTemporaryAnnotationsToTemporaryDataJson(final List<TemporaryAnnotations> temporaryAnnotationsList);

    /**
     * Set the interval for the clean task scheduler
     * @param interval New interval in minutes
     * */
    void setCleanTaskInterval(final int interval);

    /**
     * Return the current interval for the clean task scheduler
     * @return Clean task interval in minutes
     * */
    int getCleanTaskInterval();

    /**
     * Schedules cleaner task that removes the temporary data that are older as a
     * defined threshold.
     * <br><br>
     * See: {@link TemporaryDataService#getCleanTaskInterval()}
     * @return `true` if the scheduler was started else `false`
     * */
    boolean startCleanTaskScheduler();

    /**
     * Stops a running clean task scheduler
     * @return `true` if the scheduler was stopped else `false`
     * */
    boolean stopCleanTaskScheduler();

    /**
     * Checks if a cleaner task is running
     * @return `true` if the task scheduler is running else `false`
     * */
    boolean isCleanTaskSchedulerRunning();

    /**
     * Checks if given bytes can be converted to {@link TemporaryDataJson}
     * @param jsonBytes JSON object as byte array
     * @return `true` if conversation succeeds else `false`
     * */
    boolean isTemporaryDataJson(final byte[] jsonBytes);
}

package eu.europa.ec.leos.annotate.services.impl.util;

import eu.europa.ec.leos.annotate.model.LeosPermissions;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class LeosPermissionCache {
    private final static LeosPermissionCache globalInstance = new LeosPermissionCache();
    private final static int TIME_TO_LIVE = 60;

    private final Map<String, LeosPermissions> leosPermissions;
    private final Object mutex;

    public static LeosPermissionCache getInstance() {
        return LeosPermissionCache.globalInstance;
    }

    private LeosPermissionCache() {
        this.leosPermissions = new HashMap<>();
        this.mutex = new Object();
    }


    public LeosPermissions getUserPermissions(final String userId) {
        synchronized (this.mutex) {
            if (!this.leosPermissions.containsKey(userId)) {
                return null;
            }
            return this.leosPermissions.get(userId);
        }
    }

    public void addUserPermissions(final String userId, final List<String> permissions) {
        synchronized (this.mutex) {
            if (userId == null || userId.isEmpty()) {
                return;
            }
            this.leosPermissions.put(userId, new LeosPermissions(userId, permissions));
        }
    }

    public void removeUserPermission(final String userId) {
        synchronized (this.mutex) {
            if (userId == null) {
                return;
            }
            this.leosPermissions.remove(userId);
        }
    }

    public void clear() {
        synchronized (this.mutex) {
            this.leosPermissions.clear();
        }
    }

    public void flush() {
        synchronized (this.mutex) {
            final LocalDateTime now = LocalDateTime.now();
            final List<String> userIds = this.leosPermissions.values().stream()
                    .filter(permissions -> permissions.getCreated().plusMinutes(TIME_TO_LIVE).isBefore(now))
                    .map(LeosPermissions::getUserId)
                    .collect(Collectors.toList());
            userIds.stream()
                    .filter(Objects::nonNull)
                    .forEach(this.leosPermissions::remove);
        }
    }

    public int size() {
        synchronized (this.mutex) {
            return this.leosPermissions.size();
        }
    }
}

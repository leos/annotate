/*
 * Copyright 2018-2023 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.services.impl.util;

import eu.europa.ec.leos.annotate.Generated;
import eu.europa.ec.leos.annotate.model.AnnotationStatus;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Annotation;
import eu.europa.ec.leos.annotate.model.helper.AnnotationChecker;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotation;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotationPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

public final class PermissionManager {

    private static final Logger LOG = LoggerFactory.getLogger(PermissionManager.class);

    public static final String GROUP_PERMISSION_PREFIX = "group:";
    
    // constants used in LeosPermissionService
    public static final String LEOS_PERMISSION_DELETE = "CAN_DELETE";
    public static final String LEOS_PERMISSION_EDIT_ANNOTS = "CAN_EDIT_ALL_ANNOTATIONS";

    private PermissionManager() {
        // Prevent instantiation as all methods are static.
    }

    /**
     * individual permissions that might be assigned
     */
    public final static class PossiblePermissions {

        private static final String NO_PERMISSION = "";
        
        private final String USER_PERMISSION;
        private final String ANNOT_GROUP_PERMISSION;
        private final String EVERYBODY_PERMISSION;

        public PossiblePermissions(final String userOnly, final String group, final String defaultGroupName) {

            USER_PERMISSION = userOnly;
            ANNOT_GROUP_PERMISSION = GROUP_PERMISSION_PREFIX + group;
            EVERYBODY_PERMISSION = GROUP_PERMISSION_PREFIX + defaultGroupName;
        }

        @Generated
        public List<String> getNoPermission() {
            return Collections.singletonList(NO_PERMISSION);
        }

        @Generated
        public List<String> getUserPermission() {
            return Collections.singletonList(USER_PERMISSION);
        }

        @Generated
        public List<String> getGroupPermission() {
            return Collections.singletonList(ANNOT_GROUP_PERMISSION);
        }

        @Generated
        public List<String> getEverybodyPermission() {
            return Collections.singletonList(EVERYBODY_PERMISSION);
        }
    }

    public static List<String> getContributionDeleteEditPermissions(
            final Annotation annot, final Annotation rootAnnot,
            final PossiblePermissions possiblePerms, final UserInformation userInfo) {

        if (annot.getStatus() == AnnotationStatus.DELETED) {
            return possiblePerms.getNoPermission();
        } else if (isIscContributionOrReplyToOne(annot, rootAnnot)) {
            // contributions may always be edited or deleted when they are visible
            // contribution replies may always be edited or deleted when they are visible
            return possiblePerms.getUserPermission();
        } else {
            // everything else is read-only
            return possiblePerms.getNoPermission();
        }
    }

    private static boolean isIscContributionOrReplyToOne(
            final Annotation annot, final Annotation rootAnnot) {
        
        return AnnotationChecker.isIscContribution(annot)
                || AnnotationChecker.isReplyOfIscContribution(annot, rootAnnot);
    }
    
    public static List<String> getIscDeleteEditPermissions(final Annotation annot, final PossiblePermissions possiblePerms, final UserInformation userInfo) {

        if (AnnotationChecker.isResponseStatusSent(annot)) {

            final String responseId = MetadataHandler.getResponseId(annot.getMetadata()); // note: Metadata cannot be null
            final boolean userIsFromSameEntity = EntityChecker.isResponseFromUsersEntity(userInfo, responseId);

            // ISC: if the user is from the same group as the SENT annotation, he may delete and edit it; unless it was already deleted
            if (annot.getStatus() == AnnotationStatus.DELETED) {
                return possiblePerms.getNoPermission();
            } else if (userIsFromSameEntity) {
                return possiblePerms.getUserPermission();
            } else {
                // otherwise, it is read-only
                return possiblePerms.getNoPermission();
            }
        } else {
            return possiblePerms.getUserPermission();
        }
    }

    public static List<String> getLeosDeletePermissions(final Annotation annot, final PossiblePermissions possiblePerms) {

        if (AnnotationChecker.isResponseStatusSent(annot)) {
            // all users may delete: in order to simulate "everybody", we assign the default group
            // in which all users are members
            return possiblePerms.getEverybodyPermission();
        } else {
            return possiblePerms.getUserPermission();
        }
    }

    public static List<String> getLeosEditPermissions(final Annotation annot, final PossiblePermissions possiblePerms) {

        if (AnnotationChecker.isResponseStatusSent(annot)) {

            // in EdiT, SENT annotations may not be edited
            return possiblePerms.getNoPermission();
        } else {
            return possiblePerms.getUserPermission();
        }
    }

    @SuppressWarnings("PMD.TooFewBranchesForASwitchStatement")
    public static void setAdminPermissions(final Annotation annot, final PossiblePermissions possiblePerms, final UserInformation userInfo,
            final JsonAnnotationPermissions permissions) {

        switch (userInfo.getSearchUser()) {

            case Contribution:

                if (AnnotationAuthorChecker.isContributorOfAnnotation(annot, userInfo.getUser())) {
                    permissions.setAdmin(possiblePerms.getUserPermission());
                } else {
                    permissions.setAdmin(possiblePerms.getNoPermission());
                }
                break;

            default:
                // standard EdiT or ISC user

                if (AnnotationChecker.isResponseStatusSent(annot)) {
                    permissions.setAdmin(possiblePerms.getNoPermission());
                } else {
                    permissions.setAdmin(possiblePerms.getUserPermission());
                }
                break;
        }
    }

    public static void setDeletePermissions(
            final Annotation annot, final Annotation rootAnnot,
            final PossiblePermissions possiblePerms, final UserInformation userInfo,
            final JsonAnnotationPermissions permissions) {

        switch (userInfo.getSearchUser()) {

            case Contribution:
                permissions.setDelete(getContributionDeleteEditPermissions(annot, rootAnnot, possiblePerms, userInfo));
                break;

            case EdiT:
                permissions.setDelete(getLeosDeletePermissions(annot, possiblePerms));
                break;

            case ISC:
                permissions.setDelete(getIscDeleteEditPermissions(annot, possiblePerms, userInfo));
                break;

            default:
                // this case should not occur
                LOG.error("Asked for delete permissions for unknown user type.");
                permissions.setDelete(null);
                break;
        }
    }

    public static void setEditPermissions(final Annotation annot, final Annotation rootAnnot,
            final PossiblePermissions possiblePerms, final UserInformation userInfo,
            final JsonAnnotationPermissions permissions) {

        switch (userInfo.getSearchUser()) {

            case Contribution:
                permissions.setUpdate(getContributionDeleteEditPermissions(annot, rootAnnot, possiblePerms, userInfo));
                break;

            case EdiT:
                permissions.setUpdate(getLeosEditPermissions(annot, possiblePerms));
                break;

            case ISC:
                permissions.setUpdate(getIscDeleteEditPermissions(annot, possiblePerms, userInfo));
                break;

            default:
                // this case should not occur
                LOG.error("Asked for edit permissions for unknown user type.");
                permissions.setUpdate(null);
                break;
        }
    }

    public static void setReadPermissions(final Annotation annot, final PossiblePermissions possiblePerms,
            final JsonAnnotationPermissions permissions) {

        permissions.setRead(annot.isShared() ? possiblePerms.getGroupPermission() : possiblePerms.getUserPermission());
    }

    public static boolean isSharedAnnotation(final JsonAnnotation jsAnnot) {

        final JsonAnnotationPermissions perms = jsAnnot.getPermissions();
        final List<String> readPerms = perms.getRead();
        if(CollectionUtils.isEmpty(readPerms)) return false;
        
        return readPerms.stream().anyMatch(perm -> perm.startsWith(GROUP_PERMISSION_PREFIX));
    }

}

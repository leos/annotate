/*
 * Copyright 2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.annotate.services.impl.util;

import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;
import eu.europa.ec.leos.annotate.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Manages a global list of {@link TemporaryData}.
 * */
public final class TemporaryDataCache {
    /**
     * Global cache instance
     * */
    private final static TemporaryDataCache globalInstance = new TemporaryDataCache();

    private final List<TemporaryData> temporaryDataList;

    private final Object mutex;

    public TemporaryDataCache() {
        this.temporaryDataList = new ArrayList<>();
        this.mutex = new Object();
    }

    /**
     * Adds a temporary data object to the cache
     * @param temporaryData {@link TemporaryData} object to add
     * */
    public void addTemporaryData(final TemporaryData temporaryData) {
        synchronized (this.mutex) {
            this.temporaryDataList.add(temporaryData);
        }
    }

    /**
     * Adds a new temporary data to the cache and creates a new id
     * @param temporaryDataJson Temporary data JSON to add
     * @return New created {@link TemporaryData} object
     * */
    synchronized public TemporaryData newTemporaryData(final TemporaryDataJson temporaryDataJson) {
        final TemporaryData temporaryData = new TemporaryData(temporaryDataJson);
        this.temporaryDataList.add(temporaryData);
        return temporaryData;
    }

    /**
     * Search for a {@link TemporaryData} object with given id
     * @param temporaryDataId  TemporaryData id to look for
     * @return {@link Optional} with an {@link TemporaryData} object.
     * @throws TemporaryDataNotFound In case the temporary data item with specified id is not found
     * */
    public TemporaryData getTemporaryDataWithId(final String temporaryDataId) throws TemporaryDataNotFound {
        synchronized (this.mutex) {
            if (StringUtils.isEmpty(temporaryDataId)) {
                throw new TemporaryDataNotFound("EMPTY");
            }

            Optional<TemporaryData> optional = this.temporaryDataList.stream()
                    .filter(temporaryData -> this.isTemporaryDataIdEqual(temporaryData, temporaryDataId))
                    .findFirst();
            if (!optional.isPresent()) {
                throw new TemporaryDataNotFound(temporaryDataId);
            }
            return optional.get();
        }
    }

    /**
     * Checks if a specific matches the id of an {@link TemporaryData}
     * @param temporaryData {@link TemporaryData} to check
     * @param temporaryDataId Id to check
     * @return `true` if the ids are equal else `false`
     * */
    private boolean isTemporaryDataIdEqual(final TemporaryData temporaryData, final String temporaryDataId) {
        return StringUtils.isEqual(temporaryData.getId(), temporaryDataId);
    }

    /**
     * Removes a {@link TemporaryData}  with a specific id
     * @param temporaryDataId ID of the temporary data to remove
     * @return `true` if the object was removed else `false`
     * @throws TemporaryDataNotFound In case the temporary data item with specified id is not found
     * */
    public boolean removeTemporaryData(final String temporaryDataId) throws TemporaryDataNotFound {
        synchronized (this.mutex) {
            if (StringUtils.isEmpty(temporaryDataId)) {
                return false;
            }

            TemporaryData temporaryData = this.getTemporaryDataWithId(temporaryDataId);
            return this.temporaryDataList.remove(temporaryData);
        }
    }

    /**
     * Checks if {@link TemporaryData} with specific id exists
     * @param temporaryDataId ID to look for
     * @return `true` if item with specific id exists
     * */
    public boolean isTemporaryDataExist(final String temporaryDataId) {
        synchronized (this.mutex) {
            try {
                this.getTemporaryDataWithId(temporaryDataId);
                return true;
            } catch(TemporaryDataNotFound ex) {
                return false;
            }
        }
    }

    /**
     * Return all cached temporary data
     * @return List of {@link TemporaryData}
     * */
    public List<TemporaryData> getAll() {
        synchronized (this.mutex) {
            return this.temporaryDataList;
        }
    }

    /**
     * Clear the whole cache
     * */
    public void clear() {
        synchronized (this.mutex) {
            this.temporaryDataList.clear();
        }
    }

    /**
     * Current amount of items in the cache
     * @return Amount of items in the cache
     * */
    public int size() {
        synchronized (this.mutex) {
            return this.temporaryDataList.size();
        }
    }

    /**
     * Return the global managed {@link TemporaryDataCache} object
     * @return Instance of {@link TemporaryDataCache}
     * */
    public static TemporaryDataCache getInstance() {
        return TemporaryDataCache.globalInstance;
    }
}

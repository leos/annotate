package eu.europa.ec.leos.annotate.model.web.status;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StatusResponse {
    @JsonProperty("status")
    private String status;

    public StatusResponse() {
        this(null);
    }

    public StatusResponse(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }
}

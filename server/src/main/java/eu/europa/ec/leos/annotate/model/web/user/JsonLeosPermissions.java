package eu.europa.ec.leos.annotate.model.web.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonLeosPermissions {
    private final List<String> permissions;

    @JsonCreator
    public JsonLeosPermissions(@JsonProperty("permissions") final List<String> permissions) {
        this.permissions = permissions;
    }

    public List<String> getPermissions() {
        return this.permissions;
    }
}

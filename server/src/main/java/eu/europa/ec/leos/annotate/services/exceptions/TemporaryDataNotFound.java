package eu.europa.ec.leos.annotate.services.exceptions;

/**
 * Exception is thrown in case a searched temporary data item is not found
 * */
public class TemporaryDataNotFound extends Exception {
    private static final long serialVersionUID = -7277208810817027960L;

    private final String temporaryDataId;

    /**
     * Constructor
     * @param id ID of the searched item
     * */
    public TemporaryDataNotFound(final String id) {
        super(String.format("Temporary data with id '%s' not found", id));
        this.temporaryDataId = id;
    }

    /**
     * Return the id of the searched item
     * @return ID of the not found item
     * */
    public String getTemporaryDataId() {
        return this.temporaryDataId;
    }
}

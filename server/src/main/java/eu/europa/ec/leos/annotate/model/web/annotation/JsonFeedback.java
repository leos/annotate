package eu.europa.ec.leos.annotate.model.web.annotation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.europa.ec.leos.annotate.model.web.helper.LocalDateTimeDeserializer;
import eu.europa.ec.leos.annotate.model.web.helper.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class JsonFeedback {
    private final String text;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private final LocalDateTime updatedAt;
    private final String updatedByUser;

    private final String updatedByDisplayName;

    private final String status;

    @JsonCreator
    public JsonFeedback(@JsonProperty("text") final String text,
                        @JsonProperty("updatedAt") final LocalDateTime updatedAt,
                        @JsonProperty("updatedByUser") final String updatedByUser,
                        @JsonProperty("updatedByDisplayName") final String updatedByDisplayName,
                        @JsonProperty("status") final String status) {
        this.text = text;
        this.updatedAt = updatedAt;
        this.updatedByUser = updatedByUser;
        this.updatedByDisplayName = updatedByDisplayName;
        this.status = status;
    }

    public String getText() {
        return this.text;
    }

    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public String getUpdatedByUser() {
        return this.updatedByUser;
    }

    public String getUpdatedByDisplayName() {
        return this.updatedByDisplayName;
    }

    public String getStatus() {
        return this.status;
    }
}

/*
 * Copyright 2019-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.services.impl;

import eu.europa.ec.leos.annotate.model.AnnotationComparator;
import eu.europa.ec.leos.annotate.model.FeedbackStatus;
import eu.europa.ec.leos.annotate.model.UserDetails;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Annotation;
import eu.europa.ec.leos.annotate.model.entity.Tag;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.helper.AnnotationChecker;
import eu.europa.ec.leos.annotate.model.helper.AnnotationReferencesHandler;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.model.search.AnnotationSearchOptions;
import eu.europa.ec.leos.annotate.model.search.AnnotationSearchResult;
import eu.europa.ec.leos.annotate.model.search.Consts;
import eu.europa.ec.leos.annotate.model.search.DocumentAnnotationsResult;
import eu.europa.ec.leos.annotate.model.search.helper.AnnotationSearchOptionsHandler;
import eu.europa.ec.leos.annotate.model.web.annotation.*;
import eu.europa.ec.leos.annotate.model.web.user.JsonUserInfo;
import eu.europa.ec.leos.annotate.services.AnnotationConversionService;
import eu.europa.ec.leos.annotate.services.AnnotationPermissionService;
import eu.europa.ec.leos.annotate.services.GroupService;
import eu.europa.ec.leos.annotate.services.UserService;
import eu.europa.ec.leos.annotate.services.impl.util.HypothesisUsernameConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Nonnull;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * class containing functionality for converting annotations into their JSON representation  
 */
@Service
public class AnnotationConversionServiceImpl implements AnnotationConversionService {

    private static final Logger LOG = LoggerFactory.getLogger(AnnotationConversionServiceImpl.class);

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------

    private AnnotationPermissionService annotPermService;
    // note: should not be declared final, as otherwise mocking it will give problems
    @SuppressWarnings("PMD.ImmutableField")
    private UserService userService;
    private GroupService groupService;

    // -------------------------------------
    // Constructors & Setters
    // -------------------------------------

    public AnnotationConversionServiceImpl() {
        // required default constructor for autowired instantiation
    }

    public AnnotationConversionServiceImpl(final UserService userServ) {
        this.userService = userServ;
    }

    public AnnotationConversionServiceImpl(final UserService userServ, final AnnotationPermissionService permServ) {
        this.userService = userServ;
        this.annotPermService = permServ;
    }

    @Autowired
    public AnnotationConversionServiceImpl(final AnnotationPermissionService annotPermService,
            final UserService userService, final GroupService groupService) {
        this.annotPermService = annotPermService;
        this.userService = userService;
        this.groupService = groupService;
    }

    // required for mocking
    public void setPermissionService(final AnnotationPermissionService permServ) {
        this.annotPermService = permServ;
    }

    // -------------------------------------
    // Service functionality
    // -------------------------------------

    @Override
    public JsonAnnotation convertToJsonAnnotation(
            final Annotation annot,
            final UserInformation userInfo) {

        return convertToJsonAnnotation(annot, null, userInfo);
    }

    @Override
    public JsonAnnotation convertToJsonAnnotation(
            final Annotation annot,
            final Annotation rootAnnot,
            final UserInformation userInfo) {

        if (annot == null) {
            LOG.error("Received null for annotation to be converted to JsonAnnotation");
            return null;
        }

        URI docUri = null;
        try {
            docUri = new URI(annot.getDocument().getUri()); // document is mandatory property of annotation (thus not null)
        } catch (URISyntaxException e) {
            LOG.error("Unexpected error converting document URI '" + annot.getDocument().getUri() + "'", e);
        }

        final JsonAnnotation result = new JsonAnnotation();
        result.setId(annot.getId());
        result.setLinkedAnnotationId(annot.getLinkedAnnotationId());
        result.setText(annot.getText());
        result.setPrecedingText(annot.getPrecedingText());
        result.setSucceedingText(annot.getSucceedingText());
        result.setUpdated(annot.getUpdated());
        result.setUri(docUri);

        // document info
        final JsonAnnotationDocument doc = new JsonAnnotationDocument();
        doc.setTitle(annot.getDocument().getTitle());

        final JsonAnnotationDocumentLink docLink = new JsonAnnotationDocumentLink();
        docLink.setHref(docUri);
        doc.setLink(Collections.singletonList(docLink));

        // metadata is appended as a child of document
        final String annotAuthority = annot.getMetadata().getSystemId(); // metadata is mandatory property of annotation (thus not null)
        doc.setMetadata(MetadataHandler.getAllMetadataAsSimpleMetadata(annot.getMetadata()));
        result.setDocument(doc);

        // targets
        final JsonAnnotationTargets targets = new JsonAnnotationTargets(null);
        targets.setDeserializedSelectors(annot.getTargetSelectors());
        targets.setSource(docUri);
        result.setTarget(Collections.singletonList(targets));

        // group info
        final String groupName = annot.getGroup().getName(); // group is mandatory property of annotation (thus not null) - accessed via metadata
        result.setGroup(groupName);

        final String userAccountForHypo = HypothesisUsernameConverter.getHypothesisUserAccountFromUser(annot.getUser(), annotAuthority);
        // check if we are to anonymise the creator of the annotation - this is when the annotation(or its root 10.01.2023) is SENT
        final boolean anonymizeUser = AnnotationChecker.isResponseStatusSent(annot) || ((rootAnnot != null)  && AnnotationChecker.isResponseStatusSent(rootAnnot));
        setUserInfo(result, annot, userAccountForHypo,anonymizeUser);

        result.setPermissions(
                annotPermService.getJsonAnnotationPermissions(annot, rootAnnot, groupName, userAccountForHypo, userInfo));

        // tags
        if (!CollectionUtils.isEmpty(annot.getTags())) {
            // convert from List<Tag> to List<String>
            result.setTags(annot.getTags().stream().map(Tag::getName).collect(Collectors.toList()));
        }

        // references
        result.setReferences(AnnotationReferencesHandler.getReferencesList(annot));

        // annotation status
        result.setStatus(getJsonAnnotationStatus(annot, annotAuthority));

        // Feedback text and metadata
        final JsonFeedback jsonFeedback = this.getJsonFeedback(annot.getFeedbackText(),
                annot.getFeedbackUpdated(),
                annot.getFeedbackUpdatedByUser(),
                annot.getFeedbackStatus(),
                annotAuthority);
        result.setFeedback(jsonFeedback);

        // justification (only filled for suggestions)
        if (StringUtils.hasLength(annot.getJustificationText())) {

            final JsonAnnotationJustification justif = new JsonAnnotationJustification();
            justif.setText(annot.getJustificationText());
            result.setJustification(justif);
        }

        // forwarding info
        result.setForwarded(annot.isForwarded());
        result.setForwardedJustification(annot.getForwardJustification());
        result.setOriginGroup(annot.getOriginGroup() == null ? "" : annot.getOriginGroup().getName());
        result.setReplyText(annot.getReplyText());

        return result;
    }

    @Override
    public JsonFeedback convertToJsonAnnotationFeedback(Annotation annot) {
        final String annotAuthority = annot.getMetadata().getSystemId();
        final JsonFeedback jsonFeedback = this.getJsonFeedback(annot.getFeedbackText(),
                annot.getFeedbackUpdated(),
                annot.getFeedbackUpdatedByUser(),
                annot.getFeedbackStatus(),
                annotAuthority);
        return jsonFeedback;
    }

    @Override
    public JsonAnnotationStatus getJsonAnnotationStatus(
            final Annotation annot,
            final String authority) {

        final JsonAnnotationStatus status = new JsonAnnotationStatus();
        status.setStatus(annot.getStatus());
        status.setUpdated(annot.getStatusUpdated());

        String annotAuthority = authority;
        if (!StringUtils.hasLength(annotAuthority)) {
            annotAuthority = annot.getMetadata().getSystemId();
        }

        if (annot.getStatusUpdatedByUser() == null) {
            status.setUser_info(null);
            status.setUpdated_by(null);
        } else {
            final User modifyingUser = userService.getUserById(annot.getStatusUpdatedByUser());
            if (modifyingUser == null) {
                LOG.warn("Could not resolved user by his ID ({})", annot.getStatusUpdatedByUser());
            } else {
                status.setUser_info(HypothesisUsernameConverter.getHypothesisUserAccountFromUser(modifyingUser, annotAuthority));

                String updatingGroup;
                if (annot.getStatusUpdatedByGroup() != null) { // NOPMD
                    updatingGroup = groupService.getGroupName(annot.getStatusUpdatedByGroup().longValue());
                } else {

                    // old method, can remain as a fallback: read the user's "main entity"
                    updatingGroup = readUserMainEntityFromRepo(modifyingUser);
                }
                status.setUpdated_by(updatingGroup);
            }
        }

        status.setSentDeleted(annot.isSentDeleted());

        return status;
    }

    private JsonFeedback getJsonFeedback(final String feedbackText,
                                         final LocalDateTime feedbackUpdated,
                                         final Long feedbackUpdatedById,
                                         final FeedbackStatus feedbackStatus,
                                         final String annotAuthority) {
        final String updatedByUser = (feedbackUpdatedById != null) ? this.getFeedbackEditorUser(feedbackUpdatedById, annotAuthority) : null;
        final String updatedByDisplayName = (feedbackUpdatedById != null) ? this.getFeedbackEditorDisplayName(feedbackUpdatedById) : null;
        final String status = (feedbackStatus != null) ? feedbackStatus.toString() : FeedbackStatus.IN_PREPARATION.toString();
        return new JsonFeedback(feedbackText, feedbackUpdated, updatedByUser, updatedByDisplayName, status);
    }

    private String getFeedbackEditorDisplayName(final Long userId) {
        if (userId == null) return null;
        final User user = userService.getUserById(userId);
        if (user == null) return "Unknown User";

        final UserDetails userDetails = userService.getUserDetailsFromUserRepo(user.getLogin(), user.getContext());
        if (userDetails == null) return user.getLogin();
        return userDetails.getDisplayName();
    }

    private String getFeedbackEditorUser(final Long userId, final String annotAuthority) {
        if (userId == null) return null;
        final User user = userService.getUserById(userId);
        if (user == null) return null;
        return HypothesisUsernameConverter.getHypothesisUserAccountFromUser(user, annotAuthority);
    }

    /**
     * setting the user information for the converted {@link JsonAnnotation}
     * 
     * @param result
     *        the converted {@link JsonAnnotation}
     * @param annot
     *        the {@link Annotation} to be converted
     * @param userAccountForHypo
     *        the user account (used when user is not to be anonymised)
     */
    private void setUserInfo(final JsonAnnotation result, final Annotation annot, final String userAccountForHypo,
    		final boolean anonymizeUser) {

        result.setCreated(annot.getCreated()); // overwritten below if needed

        // user info
        if (anonymizeUser) {

            String iscReference = MetadataHandler.getIscReference(annot.getMetadata()); // metadata is mandatory property of annotation (thus not null)
            if (!StringUtils.hasLength(iscReference)) {
                iscReference = "unknown ISC reference";
            }

            if (result.getUser_info() == null) {
                result.setUser_info(new JsonUserInfo());
            }
            result.getUser_info().setDisplay_name(iscReference);

            // finally, we overwrite the "created date" with the date at which the response status was updated
            final LocalDateTime responseUpdated = annot.getMetadata().getResponseStatusUpdated();
            if (responseUpdated != null) { // usually set, but maybe not in all tests
                result.setCreated(responseUpdated);
            }
        } else {
            // show "real" user information, if available
            result.setUser(userAccountForHypo);

            // retrieve user's display name to have "nice names" being displayed for each annotation
            String displayName;
            final UserDetails userDetails = userService.getUserDetailsFromUserRepo(annot.getUser().getLogin(), annot.getUser().getContext());
            if (userDetails == null) {
                // usually, all users should be found in the UD repo, so this case is unlikely to occur...
                // ... but the network could be down or whatever, therefore we use a fallback
                // -> why? because annotate/hypothes.is client starts acting weird if the information is missing, and
                // might even use the "display_name" provided in the current user's profile FOR ALL USERS, which is totally wrong!!!
                // ... and that simply must be avoided!
                displayName = annot.getUser().getLogin();
                // skip setting entity name as we don't have any reasonable fallback for this

            } else {
                displayName = userDetails.getDisplayName();
            }

            if (result.getUser_info() == null) {
                result.setUser_info(new JsonUserInfo());
            }
            result.getUser_info().setDisplay_name(displayName);

            // note: here we show the real date of creating the annotation
        }

        updateEntityName(result, annot);
    }

    @Override
    public void updateEntityName(final JsonAnnotation result, final Annotation annot) {

        String entityName = "";

        if (annot.getConnectedEntity() == null) {

            // check if we are to anonymise the creator of the annotation - this is when the annotation is SENT
            final boolean anonymizeUser = AnnotationChecker.isResponseStatusSent(annot);

            // user info
            if (anonymizeUser) {
                final UserDetails userDetails = userService.getUserDetailsFromUserRepo(annot.getUser().getLogin(), annot.getUser().getContext());
                if (userDetails == null) {
                    entityName = "unknown";
                } else {
                    entityName = userDetails.getEntities().get(0).getName(); // use old default: first entity received
                }

                // we use the entity for the user as well as for the display name of the user
                // note: setting it only in the "entity name" instead of display name makes the appearance
                // become distorted
                result.setUser(entityName);

            } else {
                final UserDetails userDetails = userService.getUserDetailsFromUserRepo(annot.getUser().getLogin(), annot.getUser().getContext());
                // skip setting entity name in the case of userDetails being null as we don't have any reasonable fallback for this
                if (userDetails != null) {
                    entityName = userDetails.getEntities().get(0).getName();
                }
            }
        } else {
            // we apply the entity stored for the annotation at the time of creation/update
            entityName = annot.getConnectedEntity().getDisplayName();
        }

        if (result.getUser_info() == null) {
            result.setUser_info(new JsonUserInfo());
        }
        result.getUser_info().setEntity_name(entityName);
    }

    @Override
    public JsonSearchResult convertToJsonSearchResult(
            final DocumentAnnotationsResult documentAnnotationsResult,
            final UserInformation userInfo) {

        return convertToJsonSearchResult(documentAnnotationsResult.getSearchResult(),
                documentAnnotationsResult.getReplies(),
                documentAnnotationsResult.getSearchOptions(),
                userInfo);
    };

    @Override
    public JsonSearchResult convertToJsonSearchResult(final AnnotationSearchResult annotationResult, final List<Annotation> replies,
            final AnnotationSearchOptions options, final UserInformation userInfo) {

        if (annotationResult == null) {
            LOG.info("There is no search result to be converted to JSON response format");
            return null;
        }

        if (options == null) {
            LOG.info("No search options given for creating search result");
            return null;
        }

        // hand over the information which type of user is requesting the search;
        // hereby, support users should are treated like EdiT users
        Assert.notNull(userInfo, "Required user information missing");
        if (userInfo.getSearchUser() == Consts.SearchUserType.Unknown) {
            LOG.error("Unknown user type found; cannot assign permissions!");
            return null;
        }

        // convert annotations and their replies separately; might be merged later, if demanded
        final List<JsonAnnotation> annotationsAsJson = annotationResult.getItems().stream()
                .map(ann -> convertToJsonAnnotation(ann, userInfo))
                .collect(Collectors.toList());

        List<JsonAnnotation> repliesAsJson = new ArrayList<JsonAnnotation>();
        if (replies != null) {
            repliesAsJson = replies.stream()
                    .map(ann -> convertReplyToJson(ann, annotationResult.getItems(), userInfo))
                    .collect(Collectors.toList());
        }

        // depending on how the results were requested, we create one result type or another
        if (options.isSeparateReplies()) {

            return new JsonSearchResultWithSeparateReplies(annotationsAsJson, repliesAsJson,
                    annotationResult.getTotalItems());

        } else {
            // merge replies into annotations, re-sort according to options
            // note: the annotations list might be implemented by an unmodifiable collection; therefore, we have to copy the items...
            final List<JsonAnnotation> mergedList = Stream.of(annotationsAsJson, repliesAsJson).flatMap(item -> item == null ? null : item.stream())
                    .collect(Collectors.toList());
            mergedList.sort(new AnnotationComparator(AnnotationSearchOptionsHandler.getSort(options)));

            return new JsonSearchResult(mergedList, annotationResult.getTotalItems());
        }
    }

    private String readUserMainEntityFromRepo(@Nonnull final User user) {

        try {
            final UserDetails userDetails = userService.getUserDetailsFromUserRepo(user.getLogin(), user.getContext());
            if (userDetails != null) {
                // use main entity of user
                return userDetails.getEntities().get(0).getName();
            }
        } catch (Exception ex) {
            LOG.error("Error getting user Details from UD repo (for retrieving user's main entity)", ex);
        }

        return "";
    }

    private JsonAnnotation convertReplyToJson(final Annotation ann, final List<Annotation> nonReplies,
            final UserInformation userInfo) {

        final Annotation rootAnnot = AnnotationChecker.findRoot(ann, nonReplies);
        return convertToJsonAnnotation(ann, rootAnnot, userInfo);
    }
}

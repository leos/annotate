/*
 * Copyright 2018-2019 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.leos.annotate.Generated;

import java.util.Objects;

/**
 * class representing the special entities assigned to a user in UD-repo
 */
public class SpecialUserEntity {

    @SuppressWarnings("PMD.ShortVariable")
    private String userId;
    private String entity;

    // -----------------------------------------------------------
    // Constructors
    // -----------------------------------------------------------
    @JsonCreator
    public SpecialUserEntity(@JsonProperty("userId") final String userIdParam,
                             @JsonProperty("entity") final String entityParam) {

        this.userId = userIdParam;
        this.entity = entityParam;
    }

    // -----------------------------------------------------------
    // Getters & setters
    // -----------------------------------------------------------
    @Generated
    public String getUserId() {
        return userId;
    }
    @Generated
    public void setUserId(String userId) {
        this.userId = userId;
    }
    @Generated
    public String getEntity() {
        return entity;
    }
    @Generated
    public void setEntity(String entity) {
        this.entity = entity;
    }


    // -------------------------------------
    // equals and hashCode
    // -------------------------------------
    @Generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpecialUserEntity that = (SpecialUserEntity) o;
        return userId.equals(that.userId) && entity.equals(that.entity);
    }
    @Generated
    @Override
    public int hashCode() {
        return Objects.hash(userId, entity);
    }
}

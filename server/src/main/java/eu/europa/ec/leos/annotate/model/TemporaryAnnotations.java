package eu.europa.ec.leos.annotate.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.leos.annotate.Generated;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotation;

import java.util.List;

/**
 * Class provide information to store annotations in temporary cache
 * */
public class TemporaryAnnotations {
    private final String documentName;
    private final List<JsonAnnotation> rows;

    /**
     * Constructor
     * @param documentName Name of the document the annotations belong
     * @param rows Json content of an annot_ file
     * */
    @JsonCreator
    public TemporaryAnnotations(final String documentName,
                                final List<JsonAnnotation> rows) {
        this.documentName = documentName;
        this.rows = rows;
    }

    /**
     * Return the name of the document the annotations belong
     * @return Name of a document
     * */
    @JsonProperty("documentName")
    @Generated
    public String getDocumentName() {
        return documentName;
    }

    /**
     * Return the JSON content of an annot_ file
     * @return JSON object as string
     * */
    @JsonProperty("rows")
    @Generated
    public List<JsonAnnotation> getRows() {
        return this.rows;
    }

    @JsonIgnore
    public int getAnnotationsCount() {
        return this.rows.size();
    }
}

package eu.europa.ec.leos.annotate.model.web;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostTemporaryDataResponse {
    private final String createdId;

    @JsonCreator
    public PostTemporaryDataResponse(@JsonProperty("createdId") final String createdId) {
        this.createdId = createdId;
    }

    public String getCreatedId() {
        return this.createdId;
    }
}


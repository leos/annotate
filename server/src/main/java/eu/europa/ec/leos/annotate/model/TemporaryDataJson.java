package eu.europa.ec.leos.annotate.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.leos.annotate.Generated;

import java.util.Map;

public class TemporaryDataJson {
    private final Map<String, Object> data;

    public TemporaryDataJson() {
        this.data = null;
    }

    public TemporaryDataJson(final Map<String, Object> data) {
        this.data = data;
    }

    @JsonProperty("temporary")
    @Generated
    public Map<String, Object> getData() {
        return this.data;
    }
}

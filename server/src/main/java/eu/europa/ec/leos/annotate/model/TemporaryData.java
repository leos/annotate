/*
 * Copyright 2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.annotate.model;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * Stores the temporary json data
 * */
public class TemporaryData {
    private final String id;
    private final TemporaryDataJson temporaryDataJson;
    private final LocalDateTime created;

    /**
     * Constructor
     * <br><br>
     * Generates an {@link java.util.UUID} as id and using {@link LocalDateTime#now()} as created date
     * @param temporaryDataJson Json string of the hypothesis read only data
     * */
    public TemporaryData(final TemporaryDataJson temporaryDataJson) {
        this(UUID.randomUUID().toString(), temporaryDataJson, LocalDateTime.now());
    }

    /**
     * Constructor
     * <br><br>
     * Using {@link LocalDateTime#now()} as created date
     * @param id Identifier for the client the configuration belongs
     * @param temporaryDataJson Json string of the hypothesis read only data
     * */
    public TemporaryData(final String id,
                         final TemporaryDataJson temporaryDataJson) {
        this(id, temporaryDataJson, LocalDateTime.now());
    }

    /**
     * Constructor
     * @param id Identifier for the client the configuration belongs
     * @param temporaryDataJson Json string of the hypothesis read only data
     * @param created Time the object was added
     * */
    public TemporaryData(final String id,
                         final TemporaryDataJson temporaryDataJson,
                         final LocalDateTime created) {
        this.id = id;
        this.temporaryDataJson = temporaryDataJson;
        this.created = created;
    }

    /**
     * Return the identifier for the client the configuration belongs
     * */
    public String getId() {
        return this.id;
    }

    /**
     * Return the Json string of the hypothesis read only data
     * @return Object of {@link TemporaryDataJson}
     * */
    public TemporaryDataJson getTemporaryDataJson() {
        return this.temporaryDataJson;
    }

    /**
     * Return the date time the configuration was added
     * @return Created date as {@link LocalDateTime}
     * */
    public LocalDateTime getCreated() {
        return created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemporaryData that = (TemporaryData) o;
        return Objects.equals(this.id, that.getId())
                && Objects.equals(this.temporaryDataJson, that.getTemporaryDataJson())
                && Objects.equals(this.created, that.getCreated());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.temporaryDataJson, this.created);
    }
}

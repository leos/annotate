/*
 * Copyright 2019-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.integration.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.*;
import eu.europa.ec.leos.annotate.model.ResponseStatus;
import eu.europa.ec.leos.annotate.model.SimpleMetadata;
import eu.europa.ec.leos.annotate.model.UserDetails;
import eu.europa.ec.leos.annotate.model.UserEntity;
import eu.europa.ec.leos.annotate.model.entity.*;
import eu.europa.ec.leos.annotate.model.web.JsonFailureResponse;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotation;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotationDocumentLink;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonSearchResult;
//import eu.europa.ec.leos.annotate.model.web.status.PublishContributionsSuccessResponse;
import eu.europa.ec.leos.annotate.model.web.status.StatusUpdateSuccessResponse;
import eu.europa.ec.leos.annotate.repository.GroupRepository;
import eu.europa.ec.leos.annotate.repository.TokenRepository;
import eu.europa.ec.leos.annotate.repository.UserGroupRepository;
import eu.europa.ec.leos.annotate.repository.UserRepository;
import eu.europa.ec.leos.annotate.services.impl.util.UserDetailsCache;
import org.assertj.core.api.StringAssert;
//import org.assertj.core.api.StringAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.CollectionUtils;
//import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
@SuppressWarnings("PMD.TooManyMethods")
public class ContributorTest {

    /**
     * Tests around contributions and the new (co-)contribution workflow (ANOT-355, ANOT-372) 
     * note: annotations are posted to DIGIT group although users also belong to SG
     *       and although the annotations denote an SG response 
     */

    private static final String uriString = "uri://LEOS/dummy_bill_for_test";
    private static final String IscRef = "ISCReference";
    private static final String IscRefVal = "ISC/2019/4";
    private static final String UnexpExc = "Unexpected exception: ";
    private static final String ContributionId = "contribId1";

    private String idAnnot_SG, idAnnot_SGReply, idAnnot_Contrib, idAnnot_ContribReply;

    private Group groupDigit;
    private User sgUser, contribUser;
    private Token sgToken, contribToken;

    @SuppressWarnings("PMD.ShortVariable")
    private final static String SG = "SG";

    // annotation IDs
    private static final String ANNOT_SG = "Annot_SG";
    private static final String ANNOT_SG_REPLY = "Annot_SG_Reply";
    private static final String ANNOT_CONTRIB = "Annot_Contrib";
    private static final String ANNOT_CONTRIB_REPLY = "Annot_Contrib_Reply";

    // commonly occurring results
    private static final List<String> emptyResult = Arrays.asList();
    private List<String> contribWithReply, contribWithoutReply,
            annotWithReply, allWithoutContribReply, all;

    private enum QueryMode {
        StandardUser, Contribution
    }

    private enum ExpectedPermissions {
        ALL, USER, NONE
    }

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------

    @Autowired
    private GroupRepository groupRepos;

    @Autowired
    private UserGroupRepository userGroupRepos;

    @Autowired
    private UserRepository userRepos;

    @Autowired
    private TokenRepository tokenRepos;

    @Autowired
    private UserDetailsCache userDetailsCache;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    // -------------------------------------
    // Cleanup of database content
    // -------------------------------------
    @Before
    public void setupTests() {

        TestDbHelper.cleanupRepositories(this);
        userDetailsCache.clear();

        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

        prepareDb();
    }

    @After
    public void cleanDatabaseAfterTests() {

        TestDbHelper.cleanupRepositories(this);
        userDetailsCache.clear();
    }

    // -------------------------------------
    // Test helper functions
    // -------------------------------------

    private void prepareDb() {

        final Group groupWorld = TestDbHelper.insertDefaultGroup(groupRepos);
        final Group groupSg = new Group(SG, SG, SG, true);
        groupDigit = new Group("DIGIT", true); // just needs to be present
        groupRepos.saveAll(Arrays.asList(groupSg, groupDigit));

        final String sgWorldUserLogin = "sgAndWorld";
        final String contribUserLogin = "contrib";

        // create users and assign them to their groups
        sgUser = new User(sgWorldUserLogin);
        contribUser = new User(contribUserLogin);
        userRepos.saveAll(Arrays.asList(sgUser, contribUser));

        final List<UserEntity> entitiesSg = Arrays.asList(new UserEntity("3", "SG", "SG"));

        // cache info for users in order to speed up test execution
        userDetailsCache.cache(sgUser.getLogin(), null, new UserDetails(sgUser.getLogin(), Long.valueOf(3), SG, "user3", entitiesSg, "", null));
        userDetailsCache.cache(contribUser.getLogin(), null, new UserDetails(contribUser.getLogin(), Long.valueOf(3), SG, "user4", entitiesSg, "", null));

        userGroupRepos.save(new UserGroup(sgUser.getId(), groupWorld.getId()));
        userGroupRepos.save(new UserGroup(sgUser.getId(), groupSg.getId()));
        userGroupRepos.save(new UserGroup(sgUser.getId(), groupDigit.getId()));
        userGroupRepos.save(new UserGroup(contribUser.getId(), groupWorld.getId()));
        userGroupRepos.save(new UserGroup(contribUser.getId(), groupSg.getId()));
        userGroupRepos.save(new UserGroup(contribUser.getId(), groupDigit.getId()));

        sgToken = new Token(sgUser, Authorities.ISC, "sgA", LocalDateTime.now().plusMinutes(5), "sgR", LocalDateTime.now().plusMinutes(5));
        contribToken = new Token(contribUser, Authorities.ISC, "conA", LocalDateTime.now().plusMinutes(5), "conR", LocalDateTime.now().plusMinutes(5));
        tokenRepos.saveAll(Arrays.asList(sgToken, contribToken));
    }

    private JsonAnnotation createAnnotation(final String annotId, final boolean isPublic, final User user,
            final Group group, final String responseId, final int responseVersion,
            final String contribId, final ResponseStatus contribStatus,
            final Token token) {

        JsonAnnotation jsAnnot;

        if (isPublic) {
            jsAnnot = TestData.getTestAnnotationObject(user.getLogin());
        } else {
            jsAnnot = TestData.getTestPrivateAnnotationObject(user.getLogin());
        }
        jsAnnot.setText(annotId);
        jsAnnot.setTags(Arrays.asList(Annotation.ANNOTATION_COMMENT));
        jsAnnot.setGroup(group.getName());

        final SimpleMetadata meta = getMetadataForQuery(responseVersion, responseId);
        meta.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.IN_PREPARATION.toString());

        if (StringUtils.hasLength(contribId)) {
            meta.put(Metadata.PROP_CONTRIBUTION_ID, contribId);
        }
        if (contribStatus != ResponseStatus.UNKNOWN) {
            meta.put(Metadata.PROP_CONTRIBUTION_STATUS, contribStatus.toString());
        }

        jsAnnot.getDocument().setMetadata(meta);
        jsAnnot.getDocument().setLink(Arrays.asList(new JsonAnnotationDocumentLink(URI.create(uriString))));
        jsAnnot.getDocument().setTitle("title");

        jsAnnot.setUri(URI.create(uriString));

        // send annotation creation request
        return executeAnnotationCreationRequest(jsAnnot, token.getAccessToken());
    }

    private JsonAnnotation createReply(final String replyId, final String parentId,
            final User user, final Group group, final Token token) {

        final JsonAnnotation jsAnnot = TestData.getTestReplyToAnnotation(user.getLogin(),
                URI.create(uriString), Arrays.asList(parentId));
        jsAnnot.setText(replyId);
        jsAnnot.setGroup(group.getName());

        return executeAnnotationCreationRequest(jsAnnot, token.getAccessToken());
    }

    private SimpleMetadata getMetadataForQuery(final int responseVersion, final String responseId) {

        // ISC reference 1, responseId, responseVersion
        final SimpleMetadata meta = new SimpleMetadata();
        meta.put(Metadata.PROP_RESPONSE_VERSION, Integer.toString(responseVersion));
        meta.put(Metadata.PROP_RESPONSE_ID, responseId);
        meta.put(IscRef, IscRefVal);

        return meta;
    }

    /**
     *  create a contribution in ISC annotation - should be visible by the creator and other group members;
     *  in addition, the contribution view shows other annotations created in the group
     */
    public void step1_createAnnotationAndContribution() {

        final JsonAnnotation jsAnnotSg = createAnnotation(ANNOT_SG, true, sgUser, groupDigit, SG, 1,
                "", ResponseStatus.UNKNOWN, sgToken);
        idAnnot_SG = jsAnnotSg.getId();
        final JsonAnnotation jsAnnotSgReply = createReply(ANNOT_SG_REPLY, idAnnot_SG,
                sgUser, groupDigit, sgToken);
        idAnnot_SGReply = jsAnnotSgReply.getId();

        final JsonAnnotation jsAnnotContrib = createAnnotation(ANNOT_CONTRIB, true, contribUser, groupDigit, SG, 1,
                ContributionId, ResponseStatus.IN_PREPARATION, contribToken);
        idAnnot_Contrib = jsAnnotContrib.getId();
        final JsonAnnotation jsAnnotContribReply = createReply(ANNOT_CONTRIB_REPLY, idAnnot_Contrib,
                contribUser, groupDigit, contribToken);
        idAnnot_ContribReply = jsAnnotContribReply.getId();

        contribWithReply = Arrays.asList(idAnnot_Contrib, idAnnot_ContribReply);
        contribWithoutReply = Arrays.asList(idAnnot_Contrib);
        annotWithReply = Arrays.asList(idAnnot_SG, idAnnot_SGReply);
        allWithoutContribReply = Arrays.asList(idAnnot_SG, idAnnot_SGReply, idAnnot_Contrib);
        all = Arrays.asList(idAnnot_SG, idAnnot_SGReply, idAnnot_Contrib, idAnnot_ContribReply);
    }

    @Test
    public void test1_searchWithoutMetadata() {

        step1_createAnnotationAndContribution();

        // answer view shows all annotations, but no contribution replies
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG, "",
                QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, annotWithReply);
        // full permission on the annotations
        verifyPermissionEditDelete(annotsSgUser, annotWithReply, ExpectedPermissions.USER);

        // contribution view shows all annotations including contribution and their replies
        final List<JsonAnnotation> annotsContrib = executeSearchRequest(contribToken, SG, "",
                QueryMode.Contribution);
        verifyFoundAnnots(annotsContrib, all);
        // DG annotations are read-only, yet contributions can be edited and deleted
        verifyPermissionEditDelete(annotsContrib, Arrays.asList(idAnnot_SG, idAnnot_SGReply),
                ExpectedPermissions.NONE);
        verifyPermissionEditDelete(annotsContrib, Arrays.asList(idAnnot_Contrib, idAnnot_ContribReply),
                ExpectedPermissions.USER);
    }

    @Test
    public void test1_searchForAnswerInPreparation() throws JsonProcessingException {

        step1_createAnnotationAndContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.IN_PREPARATION.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows annotations, but no contributions
        // (as it's no contribution search and they are still IN_PREPARATION)
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, annotWithReply);

        // full permission on thehis annotations
        verifyPermissionEditDelete(annotsSgUser, annotWithReply, ExpectedPermissions.USER);

        // contribution view shows annotations, and contribution with replies
        // NOTE: normal users will have the possibility to run this search, therefore they can
        // see the contributions and interact with them; the "Chef de file" however will not be able to
        // run the contribution search, therefore he only sees what the standard user sees (cf. above)
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, all);

        // to check that visibility does not depend on the user, we run a contribution search
        // using the sgUser's token
        final List<JsonAnnotation> annotsSgUserContribMode = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsSgUserContribMode, all);

        // DG annotations are read-only, yet contributions can be edited and deleted
        verifyPermissionEditDelete(annotsSgUserContribMode, annotWithReply,
                ExpectedPermissions.NONE);
        verifyPermissionEditDelete(annotsSgUserContribMode, contribWithReply,
                ExpectedPermissions.USER);
    }

    @Test
    public void test1_searchForContributionsInPreparation() throws JsonProcessingException {

        step1_createAnnotationAndContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_STATUS, ResponseStatus.IN_PREPARATION.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // standard answer view contains nothing (as the contributions are not SENT yet)
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows the contribution and its replies
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, contribWithReply);
        verifyPermissionEditDelete(annotsContribUser, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test1_searchForContributionsBeingSent() throws JsonProcessingException {

        step1_createAnnotationAndContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_STATUS, ResponseStatus.SENT.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows nothing
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows nothing
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, emptyResult);
    }

    @Test
    public void test1_searchForSentAnswer() throws JsonProcessingException {

        step1_createAnnotationAndContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.SENT.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows nothing
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows nothing
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, emptyResult);
    }

    @Test
    public void test1_searchForContributionId() throws JsonProcessingException {

        step1_createAnnotationAndContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_ID, ContributionId);

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows nothing (DG annotations don't match and the
        // contributions are still hidden)
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows the contributions completely (including replies)
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, contribWithReply);
        verifyPermissionEditDelete(annotsContribUser, contribWithReply, ExpectedPermissions.USER);
    }

    /**
     * "send" the contribution
     */
    public void step2_sendContribution() {

        executeContributionStatusUpdateRequest(ContributionId, contribToken, HttpStatus.OK);

        // check that it can't be done again as the contribution already has the status changed
        executeContributionStatusUpdateRequest(ContributionId, contribToken, HttpStatus.NOT_FOUND);
    }

    @Test
    public void test2_searchWithoutMetadata() {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        // answer view shows all annotations, but no contribution replies
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG, "",
                QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, allWithoutContribReply);
        verifyPermissionEditDelete(annotsSgUser, allWithoutContribReply, ExpectedPermissions.USER);

        // the contribution view shows all annotation, but no contribution replies
        // user has permissions on the contributions, but none on the other annotations
        final List<JsonAnnotation> annotsContrib = executeSearchRequest(contribToken, SG, "",
                QueryMode.Contribution);
        verifyFoundAnnots(annotsContrib, all);
        verifyPermissionEditDelete(annotsContrib, annotWithReply, ExpectedPermissions.NONE);
        verifyPermissionEditDelete(annotsContrib, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test2_searchForAnswerInPreparation() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.IN_PREPARATION.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows everything, but no contribution replies
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, allWithoutContribReply);
        verifyPermissionEditDelete(annotsSgUser, allWithoutContribReply, ExpectedPermissions.USER);

        // contribution sees everything
        final List<JsonAnnotation> annotsContrib = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContrib, all);
        verifyPermissionEditDelete(annotsContrib, annotWithReply, ExpectedPermissions.NONE);
        verifyPermissionEditDelete(annotsContrib, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test2_searchForContributionsInPreparation() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_STATUS, ResponseStatus.IN_PREPARATION.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows nothing
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows nothing
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, emptyResult);
    }

    @Test
    public void test2_searchForContributionsBeingSent() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_STATUS, ResponseStatus.SENT.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows only contribution, but without replies
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, contribWithoutReply);
        verifyPermissionEditDelete(annotsSgUser, contribWithoutReply, ExpectedPermissions.USER);

        // contribution view shows contribution with replies
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, contribWithReply);
        verifyPermissionEditDelete(annotsContribUser, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test2_searchForContributionId() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_ID, ContributionId);

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows only the contribution
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, contribWithoutReply);
        verifyPermissionEditDelete(annotsSgUser, contribWithoutReply, ExpectedPermissions.USER);

        // contribution view shows the contribution and its replies
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, contribWithReply);
        verifyPermissionEditDelete(annotsContribUser, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test2_searchForSentAnswer() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.SENT.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // answer view shows nothing
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, emptyResult);

        // contribution view shows nothing
        final List<JsonAnnotation> annotsContribUser = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContribUser, emptyResult);
    }

    /**
    * send the annotations (as an official answer)
    */
    public void step3_sendAnswer() throws JsonProcessingException {

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.IN_PREPARATION.toString());

        final String serializedMetadataToMatch = SerialisationHelper.serialize(metaToMatch);
        executeStatusUpdateRequest(groupDigit, uriString, serializedMetadataToMatch, sgToken);
    }

    @Test
    public void test3_searchForSentAnswer() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();
        step3_sendAnswer();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.SENT.toString());

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // the answer view shows all annotations, but no contribution replies!
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, allWithoutContribReply);
        verifyPermissionEditDelete(annotsSgUser, allWithoutContribReply, ExpectedPermissions.USER);

        // the contribution view shows all annotations, including the contributions' replies!
        final List<JsonAnnotation> annotsContrib = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContrib, all);
        verifyPermissionEditDelete(annotsContrib, annotWithReply, ExpectedPermissions.NONE);
        verifyPermissionEditDelete(annotsContrib, contribWithReply, ExpectedPermissions.USER);
    }

    @Test
    public void test3_searchForContributionId() throws JsonProcessingException {

        step1_createAnnotationAndContribution();
        step2_sendContribution();
        step3_sendAnswer();

        final SimpleMetadata metaToMatch = getMetadataForQuery(1, SG);
        metaToMatch.put(Metadata.PROP_CONTRIBUTION_ID, ContributionId);

        final String serializedMetadataToMatch = "[" + SerialisationHelper.serialize(metaToMatch) + "]";

        // the answer view shows contribution without reply
        // note: contribution replies are not visible after the contributions were SENT
        final List<JsonAnnotation> annotsSgUser = executeSearchRequest(sgToken, SG,
                serializedMetadataToMatch, QueryMode.StandardUser);
        verifyFoundAnnots(annotsSgUser, contribWithoutReply);
        verifyPermissionEditDelete(annotsSgUser, contribWithoutReply, ExpectedPermissions.USER);

        // the contribution view shows contribution with reply (since it is a contribution search)
        final List<JsonAnnotation> annotsContrib = executeSearchRequest(contribToken, SG,
                serializedMetadataToMatch, QueryMode.Contribution);
        verifyFoundAnnots(annotsContrib, contribWithReply);
        verifyPermissionEditDelete(annotsContrib, contribWithReply, ExpectedPermissions.USER);
    }

    // --------------------------------------------
    // HELPER METHODS FOR EXECUTING SERVER CALLS
    // --------------------------------------------

    // create a new annotation
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    private JsonAnnotation executeAnnotationCreationRequest(final JsonAnnotation jsAnnot,
            final String accessToken) {

        try {
            final String serializedAnnotation = SerialisationHelper.serialize(jsAnnot);

            final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/annotations")
                    .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + accessToken)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(serializedAnnotation);

            final ResultActions result = this.mockMvc.perform(builder);

            // expected: Http 200
            result.andExpect(MockMvcResultMatchers.status().isOk());

            final MvcResult resultContent = result.andReturn();
            final String responseString = resultContent.getResponse().getContentAsString();

            // ID must have been set
            final JsonAnnotation jsResponse = SerialisationHelper.deserializeJsonAnnotation(responseString);
            Assert.assertNotNull(jsResponse);
            Assert.assertFalse(jsResponse.getId().isEmpty());

            return jsResponse;
        } catch (Exception e) {
            Assert.fail(UnexpExc + e.getMessage());
        }

        return null;
    }

    // launch status update
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    private StatusUpdateSuccessResponse executeStatusUpdateRequest(final Group group, final String uri, final String metadatas,
            final Token token) {

        try {
            final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                    .post("/api/changeStatus?group=" + group.getName() + "&uri=" + uri +
                            "&responseStatus=SENT")
                    .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + token.getAccessToken())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(metadatas);

            final ResultActions result = this.mockMvc.perform(builder);

            // expected: Http 200
            result.andExpect(MockMvcResultMatchers.status().isOk());

            final MvcResult resultContent = result.andReturn();
            final String responseString = resultContent.getResponse().getContentAsString();

            // should be success message
            return SerialisationHelper.deserializeJsonStatusUpdateSuccessResponse(responseString);
        } catch (Exception e) {
            Assert.fail(UnexpExc + e.getMessage());
        }

        return null;
    }

    // launch request to update the contribution status
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    private void executeContributionStatusUpdateRequest(final String contributionId,
            final Token token, final HttpStatus expectedStatus) {

        try {
            final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                    .post("/api/changeContributionStatus?contributionId=" + contributionId +
                            "&contributionStatus=SENT")
                    .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + token.getAccessToken());

            final ResultActions result = this.mockMvc.perform(builder);

            if (expectedStatus == HttpStatus.OK) {
                // expected: Http 200
                result.andExpect(MockMvcResultMatchers.status().isOk());

                final MvcResult resultContent = result.andReturn();
                final String responseString = resultContent.getResponse().getContentAsString();

                // should be success message
                final StatusUpdateSuccessResponse resp = SerialisationHelper.deserializeJsonStatusUpdateSuccessResponse(responseString);
                Assert.assertNotNull(resp);

            } else if (expectedStatus == HttpStatus.NOT_FOUND) {
                result.andExpect(MockMvcResultMatchers.status().isNotFound());

                final MvcResult resultContent = result.andReturn();
                final String responseString = resultContent.getResponse().getContentAsString();

                final JsonFailureResponse failure = SerialisationHelper.deserializeJsonFailureResponse(responseString);
                Assert.assertNotNull(failure);
            }
        } catch (Exception e) {
            Assert.fail(UnexpExc + e.getMessage());
        }
    }

    // run a search
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    private List<JsonAnnotation> executeSearchRequest(final Token userToken, final String connectedEntity,
            final String metadatasets, final QueryMode queryMode) {

        try {
            final StringBuffer assembledUrl = new StringBuffer("/api/search?_separate_replies=false&sort=created&order=asc&uri=").append(uriString);

            if (Authorities.isIsc(userToken.getAuthority())) {
                // always ask with DIGIT since this is what is done in our ISC scenario that we want to reproduce
                assembledUrl.append("&group=DIGIT&connectedEntity=")
                        .append(connectedEntity);
            } else if (Authorities.isLeos(userToken.getAuthority())) {
                assembledUrl.append("&group=").append(connectedEntity);
            }

            if (StringUtils.hasLength(metadatasets)) {
                assembledUrl.append("&metadatasets=").append(metadatasets);
            }

            if (queryMode == QueryMode.Contribution) {
                // required for setting permissions correctly?
                assembledUrl.append("&mode=private");
            }

            // send search request
            final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                    .post(assembledUrl.toString())
                    .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + userToken.getAccessToken());
            final ResultActions result = this.mockMvc.perform(builder);

            // expected: Http 200
            result.andExpect(MockMvcResultMatchers.status().isOk());

            final MvcResult resultContent = result.andReturn();
            final String responseString = resultContent.getResponse().getContentAsString();

            // check that the expected annotation was returned (compare IDs)
            final JsonSearchResult jsResponse = SerialisationHelper.deserializeJsonSearchResult(responseString);
            Assert.assertNotNull(jsResponse);

            return jsResponse.getRows();
        } catch (Exception e) {
            Assert.fail(UnexpExc + e.getMessage());
        }

        return null;
    }

    // --------------------------------------------
    // VERIFICATION METHODS
    // --------------------------------------------

    private void verifyFoundAnnots(final List<JsonAnnotation> foundAnnots, final List<String> expectedAnnotIds) {

        Assert.assertEquals("Number of received annotation is not the expected one", expectedAnnotIds.size(), foundAnnots.size());

        final List<String> receivedIds = foundAnnots.stream().map(jsAnn -> jsAnn.getId()).collect(Collectors.toList());

        // verify that all IDs are found
        for (int i = 0; i < foundAnnots.size(); i++) {
            Assert.assertTrue("Received annotation " + receivedIds.get(i) + " unexpected!", expectedAnnotIds.contains(receivedIds.get(i)));
            Assert.assertTrue("Expected annotation " + expectedAnnotIds.get(i) + " not received!", receivedIds.contains(expectedAnnotIds.get(i)));
        }
    }

    // verify a couple of annotations all having same expected permissions
    private void verifyPermissionEditDelete(final List<JsonAnnotation> jsAnnots,
            final List<String> annotsToCheck,
            final ExpectedPermissions expectedPerm) {

        annotsToCheck.forEach(annot -> verifyPermissionEditDelete(jsAnnots, annot, expectedPerm,
                expectedPerm));
    }

    // verifies the editing ("update") and deletion permissions that are set to the annotation
    private void verifyPermissionEditDelete(final List<JsonAnnotation> jsAnnots, final String annotId,
            final ExpectedPermissions expectedPermEdit, final ExpectedPermissions expectedPermDelete) {

        Assert.assertNotNull(jsAnnots);

        final JsonAnnotation jsAnnot = jsAnnots.stream().filter(jsAnn -> jsAnn.getId().equals(annotId)).findFirst().get();
        Assert.assertNotNull(jsAnnot.getPermissions());
        Assert.assertFalse(CollectionUtils.isEmpty(jsAnnot.getPermissions().getUpdate()));
        Assert.assertFalse(CollectionUtils.isEmpty(jsAnnot.getPermissions().getDelete()));

        final String updatePerm = jsAnnot.getPermissions().getUpdate().get(0);
        verifyPermission(updatePerm, expectedPermEdit);

        final String deletePerm = jsAnnot.getPermissions().getDelete().get(0);
        verifyPermission(deletePerm, expectedPermDelete);
    }

    private void verifyPermission(final String actualPerm, final ExpectedPermissions expectedPerm) {

        final StringAssert strAss = new StringAssert(actualPerm);

        if (expectedPerm == ExpectedPermissions.USER) {
            strAss.startsWith("acct:");
        } else if (expectedPerm == ExpectedPermissions.NONE) {
            strAss.isEmpty();
        }
    }
}

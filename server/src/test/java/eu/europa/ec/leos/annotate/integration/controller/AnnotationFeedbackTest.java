package eu.europa.ec.leos.annotate.integration.controller;

import eu.europa.ec.leos.annotate.helper.SerialisationHelper;
import eu.europa.ec.leos.annotate.helper.TestData;
import eu.europa.ec.leos.annotate.helper.TestDbHelper;
import eu.europa.ec.leos.annotate.helper.TestHelper;
import eu.europa.ec.leos.annotate.model.entity.Token;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonAnnotation;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonFeedback;
import eu.europa.ec.leos.annotate.repository.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class AnnotationFeedbackTest {

    private static final String POST_FEEDBACK_URL = "/api/annotations/%s/feedback";
    private static final String CREATE_ANNOTATION_URL = "/api/annotations?connectedEntity=%s";
    private static final String ACCESS_TOKEN = "demoaccesstoken";
    private static final String REFRESH_TOKEN = "helloRefresh";
    private static final String ACCT = "acct:user@domain.eu";

    private User theUser;

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------
    @Autowired
    @Qualifier("annotationTestRepos")
    private AnnotationTestRepository annotRepos;

    @Autowired
    private GroupRepository groupRepos;

    @Autowired
    private UserRepository userRepos;

    @Autowired
    private TokenRepository tokenRepos;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setupTests() {

        TestDbHelper.cleanupRepositories(this);
        TestDbHelper.insertDefaultGroup(groupRepos);

        theUser = new User("demo");
        userRepos.save(theUser);
        tokenRepos.save(new Token(theUser, "auth", ACCESS_TOKEN, LocalDateTime.now().plusMinutes(5), REFRESH_TOKEN, LocalDateTime.now().plusMinutes(5)));

        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @After
    public void cleanDatabaseAfterTests() {
        TestDbHelper.cleanupRepositories(this);
    }

    @Test
    public void testAddAnnotationFeedback() throws Exception {
        final String feedbackJson = "{ \"text\": \"This is a comment\" }";
        final String annotationId = this.createAnnotation();
        final String postUrl = String.format(POST_FEEDBACK_URL, annotationId);
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .patch(postUrl)
                .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + ACCESS_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(feedbackJson);

        final ResultActions result = this.mockMvc.perform(builder);

        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());
        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();

        // ID must have been set
        final JsonFeedback jsResponse = SerialisationHelper.deserializeJsonFeedback(responseString);
        Assert.assertNotNull(jsResponse);
        Assert.assertNotNull(jsResponse.getText());
        Assert.assertEquals("This is a comment", jsResponse.getText());
        Assert.assertNotNull(jsResponse.getUpdatedAt());
        Assert.assertNotNull(jsResponse.getUpdatedByUser());
        Assert.assertNotNull(jsResponse.getUpdatedByDisplayName());
    }

    public String createAnnotation() throws Exception {
        // add the user to a group
        final String ENTITY = "SomeDg";

        final JsonAnnotation jsAnnot = TestData.getTestAnnotationObject(ACCT);
        final String serializedAnnotation = SerialisationHelper.serialize(jsAnnot);
        final String postUrl = String.format(CREATE_ANNOTATION_URL, ENTITY);

        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(postUrl)
                .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + ACCESS_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializedAnnotation);

        final ResultActions result = this.mockMvc.perform(builder);

        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();

        // ID must have been set
        final JsonAnnotation jsResponse = SerialisationHelper.deserializeJsonAnnotation(responseString);
        Assert.assertNotNull(jsResponse);
        Assert.assertFalse(jsResponse.getId().isEmpty());

        return jsResponse.getId();
    }
}

package eu.europa.ec.leos.annotate.integration.services;

import eu.europa.ec.leos.annotate.services.LeosPermissionService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@ActiveProfiles("test")
public class LeosPermissionServiceTest {
    @Autowired
    private LeosPermissionService leosPermissionService;

    private static final String TEST_USER_ID_01 = "test-user-01";
    private static final String TEST_USER_ID_02 = "test-user-02";

    @Before
    public void beforeTests() {
        this.leosPermissionService.addUserPermission(TEST_USER_ID_01, Collections.singletonList("CAN_READ"));
        this.leosPermissionService.addUserPermission(TEST_USER_ID_02, Collections.singletonList("CAN_READ"));
    }

    @After
    public void afterTests() {
        this.leosPermissionService.clearCache();
    }

    @Test
    public void testGetUserPermissions() {
        final List<String> permissions = this.leosPermissionService.getUserPermissions(TEST_USER_ID_01);
        Assert.assertNotNull(permissions);
        Assert.assertFalse(permissions.isEmpty());
        Assert.assertEquals(1, permissions.size());
    }

    @Test
    public void testRemoveUserPermissions() {
        this.leosPermissionService.removeUserPermission(TEST_USER_ID_01);
        Assert.assertEquals(1, this.leosPermissionService.cacheSize());

        final List<String> permissions = this.leosPermissionService.getUserPermissions(TEST_USER_ID_01);
        Assert.assertTrue(permissions.isEmpty());
    }

    @Test
    public void testClearCache() {
        this.leosPermissionService.clearCache();
        Assert.assertEquals(0, this.leosPermissionService.cacheSize());
    }
}

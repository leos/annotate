package eu.europa.ec.leos.annotate.integration.services.impl.util;

import eu.europa.ec.leos.annotate.services.impl.util.ZipContent;
import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

public class ZipContentTest {
    @Test
    public void testNameAndPath() {
        final ZipContent zipContent = new ZipContent("/media/test.xml", null);
        Assert.assertEquals("/media/test.xml", zipContent.getFullName());
        Assert.assertEquals("test", zipContent.getName());
        Assert.assertEquals("media/", zipContent.getPath());
    }

    @Test
    public void testSetData() {
        ZipContent zipContent = new ZipContent("/media/test.xml", null);
        Assert.assertNotNull(zipContent.getData());
        Assert.assertEquals(0, zipContent.getData().length);

        final byte[] testBytes = "{ \"data\": \"This is a text\" }".getBytes(StandardCharsets.UTF_8);
        zipContent.setData(testBytes);
        Assert.assertNotNull(zipContent.getData());
        Assert.assertEquals(testBytes.length, zipContent.getData().length);
    }
}

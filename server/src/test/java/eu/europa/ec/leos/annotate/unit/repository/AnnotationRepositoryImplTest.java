/*
 * Copyright 2018 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.unit.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import eu.europa.ec.leos.annotate.repository.AnnotationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class AnnotationRepositoryImplTest {

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------
    @Qualifier("annotationRepos")
    @Autowired
    private AnnotationRepository annotRepos;

    // -------------------------------------
    // Tests
    // -------------------------------------

    @Test(expected = RuntimeException.class)
    public void testDeleteAllMustNotBeCalled() {

        annotRepos.deleteAll();
    }
}

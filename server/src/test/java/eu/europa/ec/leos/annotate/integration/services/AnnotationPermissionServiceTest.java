/*
 * Copyright 2019-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.integration.services;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.SpotBugsAnnotations;
import eu.europa.ec.leos.annotate.model.ResponseStatus;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Annotation;
import eu.europa.ec.leos.annotate.model.entity.Group;
import eu.europa.ec.leos.annotate.model.entity.Metadata;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.services.LeosPermissionService;
import eu.europa.ec.leos.annotate.services.UserGroupService;
import eu.europa.ec.leos.annotate.services.exceptions.MissingPermissionException;
import eu.europa.ec.leos.annotate.services.impl.AnnotationPermissionServiceImpl;
import eu.europa.ec.leos.annotate.services.impl.UserServiceImpl;
import eu.europa.ec.leos.annotate.services.impl.util.PermissionManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AnnotationPermissionServiceTest {

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------
    @InjectMocks
    private AnnotationPermissionServiceImpl annotPermMockService;

    @Mock
    private UserServiceImpl userService;

    @Mock
    private UserGroupService userGroupService;

    @Mock
    private LeosPermissionService leosPermissionService;

    // -------------------------------------
    // Tests
    // -------------------------------------

    @Test(expected = IllegalArgumentException.class)
    @SuppressFBWarnings(value = SpotBugsAnnotations.KnownNullValue, justification = SpotBugsAnnotations.KnownNullValueReason)
    public void testHasPermissionToUpdate_AnnotationNull() {

        final Annotation annot = null;
        final UserInformation userinfo = new UserInformation("itsme", null, Authorities.EdiT);
        annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, userinfo);
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressFBWarnings(value = SpotBugsAnnotations.KnownNullValue, justification = SpotBugsAnnotations.KnownNullValueReason)
    public void testHasPermissionToUpdate_UserinfoNull() {

        final Annotation annot = new Annotation();
        final UserInformation userinfo = null;

        annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, userinfo);
    }

    @Test
    public void testHasPermissionToUpdate_OtherMetadata_EditUser() {

        final String LOGIN = "theuser";

        final User user = new User(LOGIN);
        user.setId(2L);
        Mockito.when(userService.findByLoginAndContext(LOGIN, null)).thenReturn(user);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        annot.setMetadata(meta);
        annot.setUser(user);

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);

        // verify: status is not SENT, it is the user's annotation -> it can be updated
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(annot.getUser().getLogin(), null, Authorities.EdiT)));
    }

    // user wanting to update is not the annotation's creator -> refused
    @Test
    public void testHasPermissionToUpdate_OtherMetadata_OtherEditUser() {

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        annot.setMetadata(meta);
        annot.setUser(new User("theuser"));

        final String login = "nottheuser";
        final User user = new User(login);
        user.setId(1L); // Use a different ID as the annotation's creator ID is 0.
        Mockito.when(userService.findByLoginAndContext(login, null)).thenReturn(user);

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);

        // verify: status is not SENT -> ok, but user is not the annotation's creator
        Assert.assertFalse(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(login, null, Authorities.EdiT)));
    }

    /**
     * user wanting to update is:
     *  - from same group
     *  - not the annotation's creator
     *  - annotation is from CONTRIBUTION
     *  -> allowed
     */
    @Test
    public void testHasPermissionToUpdate_OtherMetadata_OtherIscUser_ContributionAnnot() {

        final String LOGIN_ANNOT = "annotUser";
        final String LOGIN_OTHER = "other";

        final User annotUser = new User(LOGIN_ANNOT);
        final User otherUser = new User(LOGIN_OTHER);
        final Group group = new Group("ourgroup", true);

        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);
        Mockito.when(userGroupService.isUserMemberOfGroup(otherUser, group)).thenReturn(true);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        //Test also ignoreCase comparison
        MetadataHandler.setOriginMode(meta, "pRiVaTe");
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(annotUser);

        otherUser.setId(1L); // Use a different ID as the annotation's creator ID is 0.
        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);

        // verify: status is not SENT -> ok
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, 
                new UserInformation(otherUser, null, Authorities.ISC)));
    }

    /**
     * user wanting to update is:
     *  - from same group
     *  - not the annotation's creator
     *  - annotation is NOT from CONTRIBUTION
     *  -> accepted
     */
    @Test
    public void testHasPermissionToUpdate_OtherMetadata_OtherIscUser_NOTContributionAnnot() {
        final Group group = new Group("ourgroup", true);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(new User("theuser"));

        final String login = "nottheuser";
        final User user = new User(login);
        user.setId(1L); // Use a different ID as the annotation's creator ID is 0.
        Mockito.when(userService.findByLoginAndContext(login, null)).thenReturn(user);
        Mockito.when(userGroupService.isUserMemberOfGroup(user, group)).thenReturn(true);

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);

        // verify: status is not SENT -> ok, user is not the annotation's creator but annotation is from CONTRIBUTION
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(user, Authorities.ISC)));
    }

    @Test
    public void testHasPermissionToUpdate_OtherMetadata_IscUser() {
        final Group group = new Group("mygroup", true);

        final String LOGIN = "dave";
        final User user = new User(LOGIN);
        user.setId(Long.valueOf(8));

        Mockito.when(userService.findByLoginAndContext(LOGIN, null)).thenReturn(user);
        Mockito.when(userGroupService.isUserMemberOfGroup(user, group)).thenReturn(true);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(user);

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);

        // verify: status is not SENT -> it can be updated
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(user, Authorities.ISC)));
    }

    @Test
    public void testHasPermissionToUpdateNotSuccessful_Sent_EditUser() {

        final String login = "a";
        final User user = new User(login);
        Mockito.when(userService.findByLoginAndContext(login, null)).thenReturn(user);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        annot.setMetadata(meta);
        annot.setUser(user);

        meta.setResponseStatus(ResponseStatus.SENT);

        // verify: status is SENT -> it cannot be updated any more
        Assert.assertFalse(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(annot.getUser().getLogin(), null, Authorities.EdiT)));
    }

    // an ISC user wants to update a SENT annotation; he belongs to the same group as the annotation
    @Test
    public void testHasPermissionToUpdateSuccessful_IscUserOfSameGroup() {

        final String LOGIN_ANNOT = "annotUser";
        final String LOGIN_OTHER = "other";

        final User annotUser = new User(LOGIN_ANNOT);
        final User otherUser = new User(LOGIN_OTHER);
        final Group group = new Group("mygroup", true);

        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);
        Mockito.when(userGroupService.isUserMemberOfGroup(otherUser, group)).thenReturn(true);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(annotUser);

        meta.setResponseStatus(ResponseStatus.SENT);

        // verify: status is SENT, but user belongs to same group -> it can be updated
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(otherUser, Authorities.ISC)));
    }

    // an ISC user wants to update a SENT annotation; he does not belong to the same group as the annotation
    @Test
    public void testHasPermissionToUpdateSuccessful_IscUserOfOtherGroup() {

        final String LOGIN_ANNOT = "annotUser";
        final String LOGIN_OTHER = "other";

        final User annotUser = new User(LOGIN_ANNOT);
        final User otherUser = new User(LOGIN_OTHER);
        final Group group = new Group("mygroup", true);

        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);
        Mockito.when(userGroupService.isUserMemberOfGroup(otherUser, group)).thenReturn(false);

        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(annotUser);

        meta.setResponseStatus(ResponseStatus.SENT);

        // verify: status is SENT, but user does not belong to same group -> it cannot be updated
        Assert.assertFalse(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, new UserInformation(otherUser, Authorities.ISC)));
    }
    
    /**
     *  verify that permission for updating an EdiT annotation is granted when all requirements
     *  including LEOS permissions are given 
     */
    @Test
    public void testHasPermissionToUpdateEditAnnotationWithLeosPermissionGiven() {
        
        final String LOGIN_ANNOT = "annotUser";
        final String LOGIN_OTHER = "other";

        final User annotUser = new User(LOGIN_ANNOT);
        annotUser.setId(1L);
        final User otherUser = new User(LOGIN_OTHER);
        otherUser.setId(2L);
        final Group group = new Group("mygroup", true);

        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);
        
        final List<String> permissions = new ArrayList<String>();
        permissions.add(PermissionManager.LEOS_PERMISSION_EDIT_ANNOTS);
        Mockito.when(leosPermissionService.getUserPermissions("2")).thenReturn(permissions);
        
        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        annot.setMetadata(meta);
        annot.setUser(annotUser);
        
        final UserInformation userInfo = new UserInformation(otherUser, Authorities.EdiT);
        
        Assert.assertTrue(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, userInfo));
    }
    
    /**
     *  verify that permission for updating an ISC annotation is not granted even when all requirements
     *  including LEOS permissions are given 
     */
    @Test
    public void testHasPermissionToUpdateIscAnnotationWithLeosPermissionGiven() {
        
        final String LOGIN_ANNOT = "annotUser";
        final String LOGIN_OTHER = "other";

        final User annotUser = new User(LOGIN_ANNOT);
        annotUser.setId(1L);
        final User otherUser = new User(LOGIN_OTHER);
        otherUser.setId(2L);
        final Group group = new Group("mygroup", true);

        Mockito.when(userService.findByLoginAndContext(LOGIN_OTHER, null)).thenReturn(otherUser);
        
        final List<String> permissions = new ArrayList<String>();
        permissions.add(PermissionManager.LEOS_PERMISSION_EDIT_ANNOTS);
        Mockito.when(leosPermissionService.getUserPermissions("2")).thenReturn(permissions);
        
        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setGroup(group);
        meta.setSystemId(Authorities.ISC);
        annot.setMetadata(meta);
        annot.setUser(annotUser);
        
        final UserInformation userInfo = new UserInformation(otherUser, Authorities.EdiT);
        
        Assert.assertFalse(annotPermMockService.hasUserPermissionToUpdateAnnotation(annot, userInfo));
    }

    @Test
    public void testValidateUserAnnotationApiPermission() {
        
        final UserInformation editUserInfo =  new UserInformation(new User("editUser"), Authorities.EdiT);
        final UserInformation iscUserInfo = new UserInformation(new User("iscUser"), Authorities.ISC);
        // No exception is expected here
        try {
            annotPermMockService.validateUserAnnotationApiPermission(editUserInfo);
            annotPermMockService.validateUserAnnotationApiPermission(iscUserInfo);
        } catch (Exception ex) {
            Assert.fail("Exception raised but not expected ('" + ex.getMessage() + "')");
        }

    }

    @Test(expected = MissingPermissionException.class)
    public void testValidateUserAnnotationApiPermissionWithError() throws Exception {
        
        final UserInformation supportUserInfo =  new UserInformation(new User("supportUser"), Authorities.Support);
        
        annotPermMockService.validateUserAnnotationApiPermission(supportUserInfo);
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_SameEditUser() {
        final String loginUser = "annotUser";
        final User annotUser = new User(loginUser);
        final UserInformation annotUserInfo = new UserInformation(annotUser, Authorities.EdiT);
        final Group group = new Group("ourGroup", true);

        Mockito.when(userService.findByLoginAndContext(loginUser, null)).thenReturn(annotUser);

        final Annotation annot = new Annotation();
        annot.setId("abcd12345");
        annot.setUser(annotUser);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.EdiT);
        meta.setGroup(group);
        annot.setMetadata(meta);

        Assert.assertTrue(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, annotUserInfo,
                annot.getMetadata().getSystemId()));
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_SameIscUser() {
        final String loginUser = "iscUser";
        final User annotUser = new User(loginUser);
        final UserInformation annotUserInfo = new UserInformation(annotUser, Authorities.ISC);
        final Group group = new Group("iscGroup", true);

        Mockito.when(userService.findByLoginAndContext(loginUser, null)).thenReturn(annotUser);

        final Annotation annot = new Annotation();
        annot.setId("abcd12345");
        annot.setUser(annotUser);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.ISC);
        meta.setGroup(group);
        annot.setMetadata(meta);

        Assert.assertTrue(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, annotUserInfo,
                annot.getMetadata().getSystemId()));
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_UsersOfTheSameGroup() {
        final String loginUserName = "loginUser";
        final String annotUserName = "annotUser";
        final User annotUser = new User(annotUserName);
        annotUser.setId(1L);

        final User loginUser = new User(loginUserName);
        loginUser.setId(2L);

        final UserInformation loginUserInfo = new UserInformation(loginUser, Authorities.EdiT);
        final Group group = new Group("ourGroup", true);
        group.setId(1L);

        Mockito.when(userService.findByLoginAndContext(loginUserName, null)).thenReturn(loginUser);
        Mockito.when(userGroupService.isUserMemberOfGroup(loginUser, group)).thenReturn(true);

        final Annotation annot = new Annotation();
        annot.setId("abcd12345");
        annot.setUser(annotUser);
        annot.setShared(true);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.EdiT);
        meta.setGroup(group);
        annot.setMetadata(meta);

        Assert.assertTrue(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, loginUserInfo,
                annot.getMetadata().getSystemId()));
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_AnnotNotShared() {
        final String loginUserName = "loginUser";
        final String annotUserName = "annotUser";
        final User annotUser = new User(annotUserName);
        annotUser.setId(1L);
        final User loginUser = new User(loginUserName);
        loginUser.setId(2L);
        final UserInformation loginUserInfo = new UserInformation(loginUser, Authorities.EdiT);
        final Group group = new Group("ourGroup", true);

        Mockito.when(userService.findByLoginAndContext(loginUserName, null)).thenReturn(loginUser);

        final Annotation annot = new Annotation();
        annot.setShared(false);
        annot.setId("abcd12345");
        annot.setUser(annotUser);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.EdiT);
        meta.setGroup(group);
        annot.setMetadata(meta);

        Assert.assertFalse(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, loginUserInfo,
                annot.getMetadata().getSystemId()));
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_IscAnnotAndEditUser() {
        final String loginUserName = "loginUser";
        final User annotUser = new User("iscUser");
        final User loginUser = new User(loginUserName);
        final UserInformation loginUserInfo = new UserInformation(loginUser, Authorities.EdiT);
        final Group group = new Group("iscGroup", true);

        Mockito.when(userService.findByLoginAndContext(loginUserName, null)).thenReturn(loginUser);

        final Annotation annot = new Annotation();
        annot.setId("abcd12345");
        annot.setUser(annotUser);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.ISC);
        meta.setGroup(group);
        annot.setMetadata(meta);

        Assert.assertFalse(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, loginUserInfo,
                annot.getMetadata().getSystemId()));
    }

    @Test
    public void testHasPermissionTSeeUpdateInformation_IscAnnotAndEditUserWithSentStatus() {
        final String loginUserName = "loginUser";
        final User annotUser = new User("iscUser");
        final User loginUser = new User(loginUserName);
        final UserInformation loginUserInfo = new UserInformation(loginUser, Authorities.EdiT);
        final Group group = new Group("iscGroup", true);

        Mockito.when(userService.findByLoginAndContext(loginUserName, null)).thenReturn(loginUser);

        final Annotation annot = new Annotation();
        annot.setId("abcd12345");
        annot.setUser(annotUser);

        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.ISC);
        meta.setGroup(group);
        meta.setResponseStatus(ResponseStatus.SENT);
        annot.setMetadata(meta);

        Assert.assertTrue(annotPermMockService.hasUserPermissionToSeeUpdateInformation(annot, loginUserInfo,
                annot.getMetadata().getSystemId()));
    }
}

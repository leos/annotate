/*
 * Copyright 2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.integration;

import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.TestDbHelper;
import eu.europa.ec.leos.annotate.model.*;
import eu.europa.ec.leos.annotate.model.entity.*;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.model.web.ContributionStatusUpdateRequest;
import eu.europa.ec.leos.annotate.repository.*;
import eu.europa.ec.leos.annotate.services.StatusUpdateService;
import eu.europa.ec.leos.annotate.services.exceptions.CannotUpdateAnnotationStatusException;
import eu.europa.ec.leos.annotate.services.exceptions.MissingPermissionException;
import eu.europa.ec.leos.annotate.services.impl.util.UserDetailsCache;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class ChangeContributionStatusTest {

    private static final String uriString = "uri://mydoc";
    private static final String dummySelector = "[{\"selector\":null,\"source\":\"" + uriString + "\"}]";
    private static final String DIGIT = "DIGIT";
    private static final String ANNOT_A = "idA", ANNOT_B = "idB", ANNOT_C = "idC", ANNOT_D = "idD";
    private static final String ANNOT_E = "idE", ANNOT_F = "idF", ANNOT_G = "idG", ANNOT_H = "idH";

    private User digitWorldUser;

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------

    @Autowired
    @Qualifier("annotationTestRepos")
    private AnnotationTestRepository annotRepos;

    @Autowired
    private DocumentRepository documentRepos;

    @Autowired
    private MetadataRepository metadataRepos;

    @Autowired
    private GroupRepository groupRepos;

    @Autowired
    private UserGroupRepository userGroupRepos;

    @Autowired
    private UserRepository userRepos;

    @Autowired
    private UserDetailsCache userDetailsCache;

    @Autowired
    private StatusUpdateService statusService;

    // -------------------------------------
    // Cleanup of database content
    // -------------------------------------
    @Before
    public void setupTests() {

        TestDbHelper.cleanupRepositories(this);

        createTestData();
    }

    @After
    public void cleanDatabaseAfterTests() {

        TestDbHelper.cleanupRepositories(this);
    }

    // -------------------------------------
    // Test data
    // -------------------------------------

    @SuppressWarnings("PMD.ExcessiveMethodLength")
    private void createTestData() {

        final Group groupWorld = TestDbHelper.insertDefaultGroup(groupRepos);
        final Group groupDigit = new Group(DIGIT, DIGIT, DIGIT, true);
        groupRepos.save(groupDigit);

        final String digitWorldUserLogin = "digitAndWorld";

        // create user and assign to the group
        digitWorldUser = new User(digitWorldUserLogin);
        userRepos.save(digitWorldUser);

        final List<UserEntity> entitiesDigit = Arrays.asList(new UserEntity("2", DIGIT, DIGIT));

        // cache info for users in order to speed up test execution
        userDetailsCache.cache(digitWorldUser.getLogin(), null,
                new UserDetails(digitWorldUser.getLogin(), Long.valueOf(2), DIGIT, "user2", entitiesDigit, "", null));

        userGroupRepos.save(new UserGroup(digitWorldUser.getId(), groupWorld.getId()));
        userGroupRepos.save(new UserGroup(digitWorldUser.getId(), groupDigit.getId()));

        // create a document
        final Document document = new Document(URI.create(uriString), "title");
        documentRepos.save(document);

        // we want to create the following annotations/metadata, all created by DIGIT:
        // A: LEOS, contributionId=1
        // B: LEOS, metadata but no contributionId
        // C: ISC, no metadata
        // D: ISC, metadata but no contributionId
        // E: ISC, contributionId=1, no contributionStatus
        // F: ISC, contributionId=1, contributionStatus IN_PREPARATION
        // G: ISC, contributionId=1, contributionStatus SENT
        // H: ISC, contributionId=abc, contributionStatus IN_PREPARATION

        final Metadata metaEdit = new Metadata(document, groupDigit, Authorities.EdiT);

        final Metadata metaEditWithStuff = new Metadata(metaEdit);
        metaEditWithStuff.setKeyValuePairs("a:b");

        final Metadata metaIsc = new Metadata(document, groupDigit, Authorities.ISC);

        final Metadata metaIscWithStuff = new Metadata(metaIsc);
        metaIscWithStuff.setKeyValuePairs("a:b");

        // create annotations, linking needs to be done later
        createAnnotation(ANNOT_A, digitWorldUser, "1", null, metaEdit);
        createAnnotation(ANNOT_B, digitWorldUser, null, null, metaEditWithStuff);
        createAnnotation(ANNOT_C, digitWorldUser, null, null, metaIsc);
        createAnnotation(ANNOT_D, digitWorldUser, null, null, metaIscWithStuff);
        createAnnotation(ANNOT_E, digitWorldUser, "1", null, metaIsc);
        createAnnotation(ANNOT_F, digitWorldUser, "1", ResponseStatus.IN_PREPARATION, metaIsc);
        createAnnotation(ANNOT_G, digitWorldUser, "1", ResponseStatus.SENT, metaIsc);
        createAnnotation(ANNOT_H, digitWorldUser, "abc", ResponseStatus.IN_PREPARATION, metaIsc);
    }

    // creation of annotation
    private Annotation createAnnotation(final String annotId, final User user,
            final String contribId, final ResponseStatus contribStatus, final Metadata basicMeta) {

        final Annotation annot = new Annotation();
        annot.setId(annotId);
        annot.setUser(user);

        final Metadata meta = new Metadata(basicMeta);
        final SimpleMetadata contribMeta = new SimpleMetadata();
        if (StringUtils.hasLength(contribId)) contribMeta.put(Metadata.PROP_CONTRIBUTION_ID, contribId);
        if (contribStatus != null) contribMeta.put(Metadata.PROP_CONTRIBUTION_STATUS, contribStatus.toString());

        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(meta, contribMeta);
        metadataRepos.save(meta);
        annot.setMetadata(meta);

        // mandatory fields
        annot.setId(annotId);
        annot.setCreated(LocalDateTime.now(java.time.ZoneOffset.UTC).minusMinutes(5));
        annot.setUpdated(LocalDateTime.now(java.time.ZoneOffset.UTC).minusMinutes(2));
        annot.setTargetSelectors(dummySelector);

        annot.setShared(true);

        return annotRepos.save(annot);
    }

    // -------------------------------------
    // Tests
    // -------------------------------------

    @Test
    public void testChangingContribId1ToSent()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ResponseStatus targetContributionStatus = ResponseStatus.SENT;

        final UserInformation userInfo = getUserInfoWithToken();

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("1");
        updateRequest.setContributionStatus(targetContributionStatus);

        final ResponseStatusUpdateResult updateResult = statusService.changeContributionStatus(updateRequest, userInfo);

        // verify that the annotations E and F were updated;
        // annotation G already has desired contribution status
        verifyUpdated(updateResult, Arrays.asList(ANNOT_E, ANNOT_F), targetContributionStatus);
    }

    @Test
    public void testChangingContribId1ToInPreparation()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ResponseStatus targetContributionStatus = ResponseStatus.IN_PREPARATION;

        final UserInformation userInfo = getUserInfoWithToken();

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("1");
        updateRequest.setContributionStatus(targetContributionStatus);

        final ResponseStatusUpdateResult updateResult = statusService.changeContributionStatus(updateRequest, userInfo);

        // verify that the annotations E and G were updated;
        // annotation F already has desired contribution status
        verifyUpdated(updateResult, Arrays.asList(ANNOT_E, ANNOT_G), targetContributionStatus);
    }

    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void testServiceThrowsExceptionWhenContribIdNotExisting()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ResponseStatus targetContributionStatus = ResponseStatus.SENT;

        final UserInformation userInfo = getUserInfoWithToken();

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("2");
        updateRequest.setContributionStatus(targetContributionStatus);

        statusService.changeContributionStatus(updateRequest, userInfo);
    }

    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void testNoIscMetadataAvailable()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        // remove all ISC metadata (to cover a specific code branch with this test)
        final List<Metadata> metasToDelete = metadataRepos.findBySystemId(Authorities.ISC);
        for (final Metadata meta : metasToDelete) {
            metadataRepos.delete(meta);
        }

        final UserInformation userInfo = getUserInfoWithToken();

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("99");
        updateRequest.setContributionStatus(ResponseStatus.SENT);

        statusService.changeContributionStatus(updateRequest, userInfo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenBothParametersNull()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        statusService.changeContributionStatus(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenUserInfoNull()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        statusService.changeContributionStatus(updateRequest, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenUpdateRequestNull()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        statusService.changeContributionStatus(null, getUserInfoWithToken());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenUpdateRequestWithoutContributionId()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionStatus(ResponseStatus.IN_PREPARATION);
        statusService.changeContributionStatus(updateRequest, getUserInfoWithToken());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenUpdateRequestWithoutContributionStatus()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("8abc");
        updateRequest.setContributionStatus(null);
        statusService.changeContributionStatus(updateRequest, getUserInfoWithToken());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServiceThrowsExceptionWhenUpdateRequestWithUnknownContributionStatus()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("8abc");
        // ContributionStatus by default initialized with "UNKNOWN" value
        statusService.changeContributionStatus(updateRequest, getUserInfoWithToken());
    }

    @Test(expected = MissingPermissionException.class)
    public void testEditUserMayNotUpdateContributionStatus()
            throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final ContributionStatusUpdateRequest updateRequest = new ContributionStatusUpdateRequest();
        updateRequest.setContributionId("4");
        updateRequest.setContributionStatus(ResponseStatus.SENT);

        statusService.changeContributionStatus(updateRequest, getEdiTUserInfoWithToken());
    }

    // -------------------------------------
    // Helper and verification functions
    // -------------------------------------

    private UserInformation getUserInfoWithToken() {

        return new UserInformation(new Token(digitWorldUser, Authorities.ISC, "acc1",
                LocalDateTime.now().plusMinutes(5), "ref1", LocalDateTime.now().plusMinutes(5)));
    }

    private UserInformation getEdiTUserInfoWithToken() {

        return new UserInformation(new Token(digitWorldUser, Authorities.EdiT, "acc1",
                LocalDateTime.now().plusMinutes(5), "ref1", LocalDateTime.now().plusMinutes(5)));
    }

    private void verifyUpdated(final ResponseStatusUpdateResult updateResult,
            final List<String> annotIds, final ResponseStatus expectedContribStatus) {

        Assert.assertNotNull(updateResult);
        Assert.assertEquals(2, updateResult.getUpdatedAnnotIds().size());

        for (final String annotId : annotIds) {
            assertAnnotationContained(updateResult.getUpdatedAnnotIds(), annotId);

            assertHasNewUpdatedTimestamp(annotId);

            assertMetadataHasContributionStatus(annotId, expectedContribStatus);
        }
    }

    private void assertAnnotationContained(final List<String> items, final String annotId) {
        Assert.assertTrue(items.stream().anyMatch(annId -> annId.equals(annotId)));
    }

    private void assertHasNewUpdatedTimestamp(final String annotId) {

        // heuristic: "updated" timestamp was changed within last five seconds
        final Annotation annot = annotRepos.findById(annotId).get();
        Assert.assertTrue(annot.getUpdated().isAfter(LocalDateTime.now(java.time.ZoneOffset.UTC).minusSeconds(5)));
    }

    private void assertMetadataHasContributionStatus(final String annotId, final ResponseStatus contribStatus) {

        final Annotation annot = annotRepos.findById(annotId).get();
        Assert.assertEquals(contribStatus, MetadataHandler.getContributionStatus(annot.getMetadata()));
    }
}

package eu.europa.ec.leos.annotate.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.TestDbHelper;
import eu.europa.ec.leos.annotate.helper.TestHelper;
import eu.europa.ec.leos.annotate.model.entity.Token;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.web.PostTemporaryDataResponse;
import eu.europa.ec.leos.annotate.model.web.annotation.JsonSearchResult;
import eu.europa.ec.leos.annotate.repository.TokenRepository;
import eu.europa.ec.leos.annotate.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StreamUtils;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class TemporaryAnnotationsTest {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    private MockMvc mockMvc;

    private InputStream legInputStream;

    private final String REQUEST_URL = "/api/annotations/temporary";
    private final String ACCESS_TOKEN = "demoaccesstoken";

    @Before
    public void beforeTests() throws IOException {
        this.legInputStream = this.getClass().getClassLoader().getResourceAsStream("samples/TemporaryAnnotationsSample.leg");
        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

        final User demoUser = new User("demo");
        this.userRepository.save(demoUser);
        final Token token = new Token(demoUser, Authorities.EdiT, ACCESS_TOKEN, LocalDateTime.now().plusMinutes(5),
                "refr", LocalDateTime.now());
        this.tokenRepository.save(token);
    }

    @After
    public void afterTest() {
        TestDbHelper.cleanupRepositories(this);
        if (this.legInputStream != null) {
            try {
                this.legInputStream.close();
            } catch (Exception ex) {
            }
        }
    }

    @Test
    public void testPostTemporaryAnnotations() throws Exception {
        final String temporaryDataId = this.postTemporaryData();
        Assert.assertNotNull(temporaryDataId);
        Assert.assertFalse(temporaryDataId.isEmpty());
    }

    @Test
    public void testGetTemporaryAnnotations() throws Exception {
        final String temporaryDataId = this.postTemporaryData();
        final String requestUrl = String.format("%s?id=%s&document=%s", REQUEST_URL, temporaryDataId, "EXPL_MEMORANDUM-cl82ru3k90009dg81dth4p27i-en");

        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(requestUrl)
                .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + ACCESS_TOKEN)
                .accept(MediaType.APPLICATION_JSON);
        final ResultActions result = this.mockMvc.perform(builder);
        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();
        Assert.assertFalse(responseString.isEmpty());

        final ObjectMapper objectMapper = new ObjectMapper();
        final JsonSearchResult responseObject = objectMapper.readValue(responseString, JsonSearchResult.class);
        Assert.assertNotNull(responseObject.getRows());
        Assert.assertEquals(1, responseObject.getRows().size());
    }

    private String postTemporaryData() throws Exception {
        final byte[] legFile = this.inputStreamToBytes(this.legInputStream);
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(this.REQUEST_URL)
                .content(legFile)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .accept(MediaType.APPLICATION_JSON)
                .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + ACCESS_TOKEN);
        final ResultActions result = this.mockMvc.perform(builder);
        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();
        Assert.assertFalse(responseString.isEmpty());

        final ObjectMapper objectMapper = new ObjectMapper();
        final PostTemporaryDataResponse responseObject = objectMapper.readValue(responseString, PostTemporaryDataResponse.class);
        Assert.assertFalse(responseObject.getCreatedId().isEmpty());
        return responseObject.getCreatedId();
    }

    private byte[] inputStreamToBytes(final InputStream inputStream) throws IOException {
        final byte[] legFileBytes =  StreamUtils.copyToByteArray(inputStream);
        Assert.assertNotNull("Byte array of leg file is empty", legFileBytes);
        return legFileBytes;
    }
}

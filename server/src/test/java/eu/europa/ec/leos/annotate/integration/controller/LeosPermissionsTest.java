package eu.europa.ec.leos.annotate.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.TestDbHelper;
import eu.europa.ec.leos.annotate.helper.TestHelper;
import eu.europa.ec.leos.annotate.model.entity.Token;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.web.status.StatusResponse;
import eu.europa.ec.leos.annotate.model.web.user.JsonLeosPermissions;
import eu.europa.ec.leos.annotate.repository.TokenRepository;
import eu.europa.ec.leos.annotate.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class LeosPermissionsTest {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    private MockMvc mockMvc;

    private final String REQUEST_URL = "/api/user/permissions";
    private final String ACCESS_TOKEN = "demoaccesstoken";

    @Before
    public void beforeTests() throws IOException {
        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

        final User demoUser = new User("demo");
        this.userRepository.save(demoUser);
        final Token token = new Token(demoUser, Authorities.EdiT, ACCESS_TOKEN, LocalDateTime.now().plusMinutes(5),
                "refr", LocalDateTime.now());
        this.tokenRepository.save(token);
    }

    @After
    public void afterTest() {
        TestDbHelper.cleanupRepositories(this);
    }

    @Test
    public void testPostLeosPermissions() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();
        final byte[] requestObject = objectMapper.writeValueAsBytes(new JsonLeosPermissions(Collections.singletonList("CAN_READ")));
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(REQUEST_URL)
                .header(TestHelper.AUTH_HEADER, TestHelper.AUTH_BEARER + ACCESS_TOKEN)
                .content(requestObject)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        final ResultActions result = this.mockMvc.perform(builder);
        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();
        Assert.assertFalse(responseString.isEmpty());

        final StatusResponse responseObject = objectMapper.readValue(responseString, StatusResponse.class);
        Assert.assertTrue(responseObject.getStatus().equalsIgnoreCase("ok"));
    }

}


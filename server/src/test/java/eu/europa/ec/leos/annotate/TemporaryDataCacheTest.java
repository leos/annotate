package eu.europa.ec.leos.annotate;


import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCache;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemporaryDataCacheTest {
    private TemporaryDataCache temporaryDataCache;

    @Before
    public void beforeTests() {
        this.temporaryDataCache = TemporaryDataCache.getInstance();
        Assert.assertNotNull(this.temporaryDataCache);

        if (this.temporaryDataCache.size() > 0) {
            this.temporaryDataCache.clear();
        }
    }

    @Test
    public void testAddTemporaryData() throws TemporaryDataNotFound {
        final TemporaryData temporaryData = new TemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.addTemporaryData(temporaryData);
        Assert.assertEquals(1, this.temporaryDataCache.size());
        final TemporaryData cacheData = this.temporaryDataCache.getTemporaryDataWithId(temporaryData.getId());
        Assert.assertEquals(temporaryData.getId(), cacheData.getId());
    }

    @Test
    public void testNewTemporaryData() {
        final TemporaryData temporaryData = this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        final TemporaryDataJson defaultDataJson = this.getDefaultTemporaryDataJson();
        Assert.assertNotNull(temporaryData);
        Assert.assertNotNull(temporaryData.getId());
        Assert.assertNotNull(temporaryData.getCreated());
        Assert.assertEquals(defaultDataJson.getData().get("annotations"), temporaryData.getTemporaryDataJson().getData().get("annotations"));
        Assert.assertEquals(1, this.temporaryDataCache.size());
    }

    @Test
    public void testRemoveTemporaryData() throws TemporaryDataNotFound {
        final TemporaryData temporaryData = new TemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.addTemporaryData(temporaryData);
        Assert.assertEquals(1, this.temporaryDataCache.size());

        this.temporaryDataCache.removeTemporaryData(temporaryData.getId());
        Assert.assertEquals(0, this.temporaryDataCache.size());
    }

    @Test
    public void testAll() {
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());

        final List<TemporaryData> temporaryDataList = this.temporaryDataCache.getAll();
        Assert.assertEquals(3, temporaryDataList.size());
    }

    @Test
    public void testClear() {
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        Assert.assertEquals(3, this.temporaryDataCache.size());

        this.temporaryDataCache.clear();
        Assert.assertEquals(0, this.temporaryDataCache.size());
    }

    @Test
    public void testIsTemporaryExist() {
        final TemporaryData temporaryData = this.temporaryDataCache.newTemporaryData(this.getDefaultTemporaryDataJson());
        Assert.assertTrue(this.temporaryDataCache.isTemporaryDataExist(temporaryData.getId()));
    }

    private TemporaryDataJson getDefaultTemporaryDataJson() {
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("annotations", Collections.emptyList());
        return new TemporaryDataJson(jsonObject);
    }
}

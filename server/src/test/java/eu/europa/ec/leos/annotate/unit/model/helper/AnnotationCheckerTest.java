/*
 * Copyright 2018-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.unit.model.helper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.europa.ec.leos.annotate.helper.SpotBugsAnnotations;
import eu.europa.ec.leos.annotate.model.ResponseStatus;
import eu.europa.ec.leos.annotate.model.entity.Annotation;
import eu.europa.ec.leos.annotate.model.entity.Metadata;
import eu.europa.ec.leos.annotate.model.entity.Tag;
import eu.europa.ec.leos.annotate.model.helper.AnnotationChecker;
import eu.europa.ec.leos.annotate.model.helper.TagBuilder;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class AnnotationCheckerTest {

    @Test
    public void testHighlightIsRecognized() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_HIGHLIGHT), annot));

        assertTrue(AnnotationChecker.isHighlight(annot));
    }

    @Test
    public void testCommentIsNotRecognizedAsHighlight() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_COMMENT), annot));

        assertFalse(AnnotationChecker.isHighlight(annot));
    }

    @Test
    public void testSuggestionIsNotRecognizedAsHighlight() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_SUGGESTION), annot));

        assertFalse(AnnotationChecker.isHighlight(annot));
    }

    @Test
    public void testAnnotationWithoutTagsIsNotRecognizedAsHighlight() {

        final Annotation annot = new Annotation();
        annot.setTags(null);

        assertFalse(AnnotationChecker.isHighlight(annot));

        // set empty list of tags -> still no suggestion
        annot.setTags(new ArrayList<Tag>());
        assertFalse(AnnotationChecker.isHighlight(annot));
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressFBWarnings(value = SpotBugsAnnotations.ExceptionIgnored, justification = SpotBugsAnnotations.ExceptionIgnored)
    public void testUndefinedAnnotationIsNotRecognizedAsHighlight() {

        AnnotationChecker.isHighlight(null);
    }

    @Test
    public void testResponseSent() {

        final Annotation ann = new Annotation();

        assertFalse(AnnotationChecker.isResponseStatusSent(ann));

        final Metadata meta = new Metadata();
        ann.setMetadata(meta);
        assertFalse(AnnotationChecker.isResponseStatusSent(ann));

        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);
        assertFalse(AnnotationChecker.isResponseStatusSent(ann));

        meta.setResponseStatus(ResponseStatus.SENT);
        assertTrue(AnnotationChecker.isResponseStatusSent(ann));
    }

    @Test
    public void testSuggestionIsRecognized() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_SUGGESTION), annot));

        assertTrue(AnnotationChecker.isSuggestion(annot));
    }

    @Test
    public void testCommentIsNotRecognizedAsSuggestion() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_COMMENT), annot));

        assertFalse(AnnotationChecker.isSuggestion(annot));
    }

    @Test
    public void testHighlightIsNotRecognizedAsSuggestion() {

        final Annotation annot = new Annotation();
        annot.setTags(TagBuilder.getTagList(Arrays.asList(Annotation.ANNOTATION_HIGHLIGHT), annot));

        assertFalse(AnnotationChecker.isSuggestion(annot));
    }

    @Test
    public void testAnnotationWithoutTagsIsNotRecognizedAsSuggestion() {

        final Annotation annot = new Annotation();
        annot.setTags(null);

        assertFalse(AnnotationChecker.isSuggestion(annot));

        // set empty list of tags -> still no suggestion
        annot.setTags(new ArrayList<Tag>());
        assertFalse(AnnotationChecker.isSuggestion(annot));
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressFBWarnings(value = SpotBugsAnnotations.ExceptionIgnored, justification = SpotBugsAnnotations.ExceptionIgnored)
    public void testUndefinedAnnotationIsNotRecognizedAsSuggestion() {

        AnnotationChecker.isSuggestion(null);
    }

    // -------------------------------------
    // Tests for finding an annotation's root annotation among candidates
    // -------------------------------------

    @Test(expected = IllegalArgumentException.class)
    public void testCannotFindRootWithoutInput() {

        AnnotationChecker.findRoot(null, null);
    }

    @Test
    public void testCannotFindRootWithoutAnnotationList() {

        assertNull(AnnotationChecker.findRoot(new Annotation(), null));
    }

    @Test
    public void testCannotFindRootForNonReply() {

        // first argument should be the reply for which the root is searched, but here it is no reply
        assertNull(AnnotationChecker.findRoot(new Annotation(), Arrays.asList(new Annotation())));
    }

    @Test
    public void testCannotFindRoot() {

        // we set a root id for which there is no matching root in the given candidate list
        final Annotation annot = new Annotation();
        annot.setRootAnnotationId("a");
        annot.setReferences(annot.getRootAnnotationId());

        final Annotation candidate = new Annotation();
        candidate.setId("whoknows");

        assertNull(AnnotationChecker.findRoot(annot, Arrays.asList(candidate)));
    }

    @Test
    public void testCanFindRoot() {

        final String rootId = "root";

        // we set a root id for which there is no matching root in the given candidate list
        final Annotation annot = new Annotation();
        annot.setRootAnnotationId(rootId);
        annot.setReferences(rootId);

        final Annotation root = new Annotation();
        root.setId(rootId);

        final Annotation nonRoot = new Annotation();
        nonRoot.setId("nottheroot");

        // act
        final Annotation foundRoot = AnnotationChecker.findRoot(annot,
                Arrays.asList(nonRoot, root));

        // verify
        assertNotNull(foundRoot);
        assertEquals(rootId, foundRoot.getId());
    }

    // -------------------------------------
    // Tests for checking if an annotation is a reply to an ISC contribution
    // -------------------------------------

    @Test
    public void cannotCheckIfIsIscReplyWithoutInputs() {

        assertFalse(AnnotationChecker.isReplyOfIscContribution(null, null));
        assertFalse(AnnotationChecker.isReplyOfIscContribution(new Annotation(), null));
    }

    @Test
    public void checkIfIsIscReplyGetsNoReply() {

        assertFalse(AnnotationChecker.isReplyOfIscContribution(new Annotation(),
                new Annotation()));
    }

    @Test
    public void checkIfIsIscReplyGetsNoRootForExpectedRoot() {

        final String rootId = "root", anotherRootId = "anotherRoot";

        final Annotation annotToCheck = new Annotation();
        annotToCheck.setReferences(rootId);
        annotToCheck.setRootAnnotationId(rootId); // computed column, so we need to fill it in tests

        final Annotation annotPotentialContrib = new Annotation();
        annotPotentialContrib.setReferences(anotherRootId);
        annotPotentialContrib.setRootAnnotationId(anotherRootId);

        assertFalse(AnnotationChecker.isReplyOfIscContribution(annotToCheck,
                annotPotentialContrib));
    }

    @Test
    public void checkIfIsIscReply_AnnotationsNotRelated() {

        final String rootId = "root2", notTheSameRootId = "anotherRoot";

        final Annotation annotToCheck = new Annotation();
        annotToCheck.setRootAnnotationId(rootId);
        annotToCheck.setReferences(rootId);

        final Annotation annotPotentialContrib = new Annotation();
        annotPotentialContrib.setId(notTheSameRootId);

        assertFalse(AnnotationChecker.isReplyOfIscContribution(annotToCheck,
                annotPotentialContrib));
    }
}

package eu.europa.ec.leos.annotate.services;

import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.services.exceptions.TemporaryDataNotFound;
import eu.europa.ec.leos.annotate.utils.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@ActiveProfiles("test")
@SuppressWarnings("PMD.TooManyMethods")
public class TemporaryDataServiceTest {

    @Autowired
    private TemporaryDataService temporaryDataService;

    @Test
    public void testStoreTemporaryData() {
        final String temporaryDataId = this.temporaryDataService.storeTemporaryData(this.getDefaultTemporaryDataJson());
        Assert.assertFalse(StringUtils.isEmpty(temporaryDataId));
    }

    @Test
    public void testGetTemporaryData() throws TemporaryDataNotFound {
        final String temporaryDataId = this.temporaryDataService.storeTemporaryData(this.getDefaultTemporaryDataJson());
        final TemporaryData temporaryData = this.temporaryDataService.getTemporaryData(temporaryDataId);
        final TemporaryDataJson defaultDataJson = this.getDefaultTemporaryDataJson();

        Assert.assertNotNull(temporaryData);
        Assert.assertNotNull(temporaryData.getCreated());
        Assert.assertEquals(temporaryDataId, temporaryData.getId());
        Assert.assertTrue(temporaryData.getTemporaryDataJson().getData().containsKey("annotations"));
        Assert.assertEquals(defaultDataJson.getData().get("annotations"), temporaryData.getTemporaryDataJson().getData().get("annotations"));
    }

    @Test(expected = TemporaryDataNotFound.class)
    public void testGetTemporaryDataWithError() throws TemporaryDataNotFound {
        this.temporaryDataService.getTemporaryData("WRONG_ID");
    }

    @Test(expected = TemporaryDataNotFound.class)
    public void testRemoveTemporaryData() throws TemporaryDataNotFound {
        final String temporaryDataId = this.temporaryDataService.storeTemporaryData(this.getDefaultTemporaryDataJson());
        try {
            this.temporaryDataService.getTemporaryData(temporaryDataId);
        } catch(TemporaryDataNotFound ex) {
            Assert.fail("Exception raised but no expected");
        }

        this.temporaryDataService.removeTemporaryData(temporaryDataId);
        this.temporaryDataService.getTemporaryData(temporaryDataId);
    }

    @Test
    public void testIsTemporaryDataJson() {
        final String jsonString = "{ \"temporary\": { \"annotations\": []  } }";
        final String wrongJson = "{ data: {} }";

        Assert.assertTrue(this.temporaryDataService.isTemporaryDataJson(jsonString.getBytes(StandardCharsets.UTF_8)));
        Assert.assertFalse(this.temporaryDataService.isTemporaryDataJson(wrongJson.getBytes(StandardCharsets.UTF_8)));
        Assert.assertFalse(this.temporaryDataService.isTemporaryDataJson(new byte[0]));
        Assert.assertFalse(this.temporaryDataService.isTemporaryDataJson(null));
    }

    private TemporaryDataJson getDefaultTemporaryDataJson() {
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("annotations", Collections.emptyList());
        return new TemporaryDataJson(jsonObject);
    }

    @Test
    public void testCleanTaskSchedulerInterval() {
        final int newInterval = 10;
        this.temporaryDataService.setCleanTaskInterval(newInterval);
        Assert.assertEquals(newInterval, this.temporaryDataService.getCleanTaskInterval());
    }

    @Test
    public void testCleanTaskSchedulerHandling() {
        // Scheduler start automatically at service creation
        Assert.assertTrue(this.temporaryDataService.isCleanTaskSchedulerRunning());
        Assert.assertTrue(this.temporaryDataService.stopCleanTaskScheduler());
        Assert.assertFalse(this.temporaryDataService.isCleanTaskSchedulerRunning());
        Assert.assertFalse(this.temporaryDataService.stopCleanTaskScheduler());

        Assert.assertTrue(this.temporaryDataService.startCleanTaskScheduler());
        Assert.assertTrue(this.temporaryDataService.isCleanTaskSchedulerRunning());
        Assert.assertFalse(this.temporaryDataService.startCleanTaskScheduler());
    }
}

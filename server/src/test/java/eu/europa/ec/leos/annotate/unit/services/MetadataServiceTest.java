/*
 * Copyright 2018-2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.unit.services;

import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.helper.TestDbHelper;
import eu.europa.ec.leos.annotate.model.ResponseStatus;
import eu.europa.ec.leos.annotate.model.SimpleMetadata;
import eu.europa.ec.leos.annotate.model.UserInformation;
import eu.europa.ec.leos.annotate.model.entity.Document;
import eu.europa.ec.leos.annotate.model.entity.Group;
import eu.europa.ec.leos.annotate.model.entity.Metadata;
import eu.europa.ec.leos.annotate.model.entity.User;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.model.web.StatusUpdateRequest;
import eu.europa.ec.leos.annotate.repository.DocumentRepository;
import eu.europa.ec.leos.annotate.repository.GroupRepository;
import eu.europa.ec.leos.annotate.repository.MetadataRepository;
import eu.europa.ec.leos.annotate.repository.UserRepository;
import eu.europa.ec.leos.annotate.services.MetadataService;
import eu.europa.ec.leos.annotate.services.exceptions.CannotCreateMetadataException;
import eu.europa.ec.leos.annotate.services.exceptions.CannotUpdateAnnotationStatusException;
import eu.europa.ec.leos.annotate.services.exceptions.MissingPermissionException;
import eu.europa.ec.leos.annotate.services.impl.MetadataServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.mockito.Mockito;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@ActiveProfiles("test")
@SuppressWarnings("PMD.TooManyMethods")
public class MetadataServiceTest {

    // -------------------------------------
    // Required services and repositories
    // -------------------------------------
    @Autowired
    private MetadataService metadataService;

    @Autowired
    private DocumentRepository documentRepos;

    @Autowired
    private GroupRepository groupRepos;

    @Autowired
    private MetadataRepository metadataRepos;

    @Autowired
    private UserRepository userRepos;

    private Group defaultGroup;

    private static final String SYS_ID = "systemId";
    private static final String VERSION = "version";

    // -------------------------------------
    // Cleanup of database content
    // -------------------------------------
    @Before
    public void cleanDatabaseBeforeTests() {

        TestDbHelper.cleanupRepositories(this);
        defaultGroup = TestDbHelper.insertDefaultGroup(groupRepos);
    }

    @After
    public void cleanDatabaseAfterTests() {

        TestDbHelper.cleanupRepositories(this);
    }

    // -------------------------------------
    // Tests
    // -------------------------------------

    // test that saving empty metadata is prohibited
    @Test(expected = CannotCreateMetadataException.class)
    public void testSaveEmptyMetadata() throws Exception {

        metadataService.saveMetadata(null);
    }

    // test that saving metadata filled with minimal values is working
    @Test
    public void testSaveSuccessful() throws URISyntaxException, CannotCreateMetadataException {

        final Document doc = new Document(new URI("www.a.eu"), "sometitle");
        documentRepos.save(doc);

        final Metadata meta = new Metadata(doc, defaultGroup, Authorities.EdiT);
        metadataService.saveMetadata(meta);

        Assert.assertEquals(1, metadataRepos.count());
    }

    // test that trying to save metadata with missing dependent data throws expected exception
    @Test(expected = CannotCreateMetadataException.class)
    public void testSaveUnsuccessful_missingDependency() throws Exception {

        metadataService.saveMetadata(new Metadata(null, defaultGroup, Authorities.EdiT));
    }

    // test that the given "metadata block" is chunked and reassembled as expected
    @Test
    public void testDistributionOfKeyValuePairs() throws URISyntaxException {

        final String newSystemId = "theSystemId";
        final String propWithQuotes = "\"anotherQuoteAtBeginning";
        final String expectedConcatKVPairs = "someProp:08/15\npropWithQuote:\"anotherQuoteAtBeginning\n";

        final Document doc = new Document(new URI("www.abc.eu"), "mytitle");
        documentRepos.save(doc);

        // insert some metadata into the HashMap, parts of them will be redistributed
        final Metadata meta = new Metadata(doc, defaultGroup, Authorities.EdiT);
        final SimpleMetadata kvPairs = new SimpleMetadata();
        kvPairs.put(SYS_ID, newSystemId);
        kvPairs.put(Metadata.PROP_RESPONSE_STATUS, ResponseStatus.IN_PREPARATION.toString());
        kvPairs.put(VERSION, "1.1");
        kvPairs.put("someProp", "08/15");
        kvPairs.put("propWithQuote", propWithQuotes);
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(meta, kvPairs);

        // verify that the kvPairs were chunked as expected
        Assert.assertEquals(newSystemId, meta.getSystemId());
        Assert.assertEquals(ResponseStatus.IN_PREPARATION, meta.getResponseStatus());
        Assert.assertTrue(meta.getKeyValuePairs().contains("someProp:08/15"));
        Assert.assertTrue(meta.getKeyValuePairs().contains("propWithQuote:" + propWithQuotes));
        Assert.assertEquals(meta.getVersion(), "1.1");
        Assert.assertEquals(expectedConcatKVPairs, meta.getKeyValuePairs());

        // verify that asking for the HashMap assembles it again correctly,
        // including the redistributed properties not saved in the "keyValuePairs" property
        final SimpleMetadata retrieved = MetadataHandler.getKeyValuePropertyAsSimpleMetadata(meta);
        Assert.assertNotNull(retrieved);
        Assert.assertEquals(2, retrieved.keySet().size());
        Assert.assertEquals("08/15", retrieved.get("someProp"));
        Assert.assertEquals(propWithQuotes, retrieved.get("propWithQuote"));
        // note: systemId, version and responseStatus are not contained!

        // verify that when asking FOR ALL items, systemId and responseStatus are contained
        final SimpleMetadata retrievedAll = MetadataHandler.getAllMetadataAsSimpleMetadata(meta);
        Assert.assertNotNull(retrievedAll);
        Assert.assertEquals(5, retrievedAll.keySet().size());
        Assert.assertEquals("08/15", retrievedAll.get("someProp"));
        Assert.assertEquals(propWithQuotes, retrievedAll.get("propWithQuote"));
        Assert.assertEquals(newSystemId, retrievedAll.get(SYS_ID));
        Assert.assertEquals("IN_PREPARATION", retrievedAll.get(Metadata.PROP_RESPONSE_STATUS));
        Assert.assertEquals("1.1", retrievedAll.get(VERSION));
    }

    // test behavior when invalid (or only partially valid/available) metadata is processed
    @Test
    public void testDistributionOfInvalidKeyValuePairs() {

        // insert some metadata into the HashMap, parts of them will be redistributed
        final Metadata meta = new Metadata(null, defaultGroup, "");
        final SimpleMetadata kvPairs = new SimpleMetadata();
        // no systemId!
        kvPairs.put(Metadata.PROP_RESPONSE_STATUS, "UNKNOWN_VALUE"); // will raise internal enum conversion error
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(meta, kvPairs);

        Assert.assertFalse(StringUtils.hasLength(meta.getSystemId()));
        Assert.assertNull(meta.getResponseStatus());

        // test opposite direction
        meta.setKeyValuePairs("key value no colon\n");
        final SimpleMetadata converted = MetadataHandler.getKeyValuePropertyAsSimpleMetadata(meta);
        Assert.assertNotNull(converted);
        Assert.assertEquals(0, converted.keySet().size());

        // test opposite direction WITH ALL metadata
        final SimpleMetadata convertedFull = MetadataHandler.getAllMetadataAsSimpleMetadata(meta);
        Assert.assertNotNull(convertedFull);
        Assert.assertEquals(1, convertedFull.keySet().size());
        Assert.assertNotNull(convertedFull.get(SYS_ID));
    }

    // test that the various find methods return correct result for a Metadata object not being SENT
    @Test
    public void testFindMetadataMethods_MetadataNotSent() throws URISyntaxException {

        final String SystemId = Authorities.EdiT;

        final Document doc = new Document(new URI("leos://5"), "title5");
        documentRepos.save(doc);

        final Group group = new Group("thegroup2", false);
        groupRepos.save(group);

        final Metadata metaNotSent = new Metadata(doc, group, SystemId);
        metaNotSent.setResponseStatus(ResponseStatus.IN_PREPARATION);
        metadataRepos.save(metaNotSent);

        // functions returning matched object
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentGroupSystemid(doc, group, SystemId).size());
        Assert.assertEquals(0, metadataService.findMetadataOfDocumentGroupSystemidSent(doc, group, SystemId).size());

        // functions returning lists of matched objects - never return null, but at least empty list
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentSystemidGroupIds(doc, SystemId, Arrays.asList(group.getId())).size());
        Assert.assertEquals(0, metadataService.findMetadataOfDocumentSystemidGroupIds(doc, SystemId, Arrays.asList(group.getId() + 4)).size()); // ID unknown
        Assert.assertEquals(0, metadataService.findMetadataOfDocumentSystemidSent(doc, SystemId).size());
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentGroupSystemidInPreparation(doc, group, SystemId).size());
    }

    // test that the various find methods return correct result for a Metadata object being SENT
    @Test
    public void testFindMetadataMethods_MetadataSent() throws URISyntaxException {

        final String SystemId = Authorities.EdiT;

        final Document doc = new Document(new URI("leos://6"), "title6");
        documentRepos.save(doc);

        final Group group = new Group("othergroup", false);
        groupRepos.save(group);

        final Metadata metaNotSent = new Metadata(doc, group, SystemId);
        metaNotSent.setResponseStatus(ResponseStatus.SENT);
        metadataRepos.save(metaNotSent);

        // functions returning matched object
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentGroupSystemid(doc, group, SystemId).size()); // SENT status does not matter
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentGroupSystemidSent(doc, group, SystemId).size());

        // functions returning lists of matched objects - never return null, but at least empty list
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentSystemidGroupIds(doc, SystemId, Arrays.asList(group.getId())).size());
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentSystemidGroupIds(doc, SystemId,
                Arrays.asList(group.getId(), group.getId() + 4)).size()); // some unknown
        Assert.assertEquals(1, metadataService.findMetadataOfDocumentSystemidSent(doc, SystemId).size());
        Assert.assertEquals(0, metadataService.findMetadataOfDocumentGroupSystemidInPreparation(doc, group, SystemId).size());
    }

    // test that calling one of the find methods with an empty list is prohibited
    @Test(expected = IllegalArgumentException.class)
    public void testFindMetadataOfDocumentSystemidGroupIds_NoEmptyListAccepted() throws Exception {

        final Document document = new Document(new URI("leos://1"), "title1");
        documentRepos.save(document);

        final Group group = new Group("mygroup", false);
        groupRepos.save(group);

        metadataService.findMetadataOfDocumentSystemidGroupIds(document, "system", null);
    }

    // test that searching for metadata without an existing document returns an empty list
    @Test
    public void testFindMetadata_NoDocument() {

        final List<Metadata> items = metadataService.findMetadataOfDocumentGroupSystemid("http://LEOS/2", null, "");
        Assert.assertNotNull(items);
        Assert.assertEquals(0, items.size());
    }

    // test that searching for metadata without an existing group returns an empty list
    @Test
    public void testFindMetadata_NoGroup() {

        final Document document = new Document(URI.create("leos://1"), "title1");
        documentRepos.save(document);

        final List<Metadata> items = metadataService.findMetadataOfDocumentGroupSystemid(document.getUri(), "dontknow", "");
        Assert.assertNotNull(items);
        Assert.assertEquals(0, items.size());
    }

    // -------------------------------------
    // Tests for updating metadata status
    // -------------------------------------

    // status update is not executed without request parameters
    @Test(expected = IllegalArgumentException.class)
    public void updateMetadataWithoutRequestParameters() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("", null, "");
        metadataService.updateMetadata(null, userInfo, null);
    }

    // status update is not executed without required user information
    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void updateMetadataWithoutUser() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        metadataService.updateMetadata(null, null, null);
    }

    // status update not executed without group set in request parameters
    @Test(expected = IllegalArgumentException.class)
    public void updateMetadataWithoutRequestGroup() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("", null, "");
        final StatusUpdateRequest sur = new StatusUpdateRequest();
        metadataService.updateMetadata(sur, userInfo, null);
    }

    // status update not executed without uri set in request parameters
    @Test(expected = IllegalArgumentException.class)
    public void updateMetadataWithoutRequestUri() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("", null, "");
        final StatusUpdateRequest sur = new StatusUpdateRequest();
        sur.setGroup("somegroups");
        metadataService.updateMetadata(sur, userInfo, null);
    }

    // status update not executed when user is not an ISC user
    @Test(expected = MissingPermissionException.class)
    public void updateMetadata_MissingPermission() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        // document and group exist in DB
        final String DOCURI = "http://dummy2";
        final String GROUPNAME = "usergroup";
        documentRepos.save(new Document(URI.create(DOCURI), "hello"));
        groupRepos.save(new Group(GROUPNAME, true));

        final UserInformation userInfo = new UserInformation("", null, Authorities.EdiT); // LEOS/EdiT user may not launch the update
        final StatusUpdateRequest sur = new StatusUpdateRequest(GROUPNAME, DOCURI, ResponseStatus.IN_PREPARATION);

        // update request should be refused
        metadataService.updateMetadata(sur, userInfo, null);
    }

    // status update not executed when referenced document is not contained in database
    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void updateMetadata_DocumentUnknown() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("", null, Authorities.ISC);
        final StatusUpdateRequest sur = new StatusUpdateRequest("somegroup", "http://dummy2", ResponseStatus.IN_PREPARATION);

        // document is not saved in documentRepos
        metadataService.updateMetadata(sur, userInfo, null);
    }

    // status update not executed when referenced group is not contained in database
    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void updateMetadata_GroupUnknown() throws URISyntaxException, CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("me", null, Authorities.ISC);
        final String uri = "http://dummy";
        final StatusUpdateRequest sur = new StatusUpdateRequest("somegroup", uri, ResponseStatus.IN_PREPARATION);

        documentRepos.save(new Document(new URI(uri), "title"));

        // group is not saved in groupRepos
        metadataService.updateMetadata(sur, userInfo, null);
    }

    // status update not executed when no matching metadata is not contained in database
    @Test
    public void updateMetadata_MetadataUnknown() throws URISyntaxException, CannotUpdateAnnotationStatusException, MissingPermissionException {

        final UserInformation userInfo = new UserInformation("login", null, Authorities.ISC);
        final String uri = "http://dummy";
        final String groupName = "thegroup";
        final StatusUpdateRequest sur = new StatusUpdateRequest(groupName, uri, ResponseStatus.IN_PREPARATION);
        final LocalDateTime timestamp = LocalDateTime.now(java.time.ZoneOffset.UTC);

        documentRepos.save(new Document(new URI(uri), "title"));
        groupRepos.save(new Group(groupName, false));

        // metadata is not saved in metadataRepos
        final List<Metadata> updatedMetadata = metadataService.updateMetadata(sur, userInfo, timestamp);
        Assert.assertNotNull(updatedMetadata);
        Assert.assertEquals(0, updatedMetadata.size());
    }

    // status update finds no matching metadata
    @Test
    public void updateMetadata_NotFound() throws MissingPermissionException, URISyntaxException, CannotUpdateAnnotationStatusException {

        final List<Object> prep = prepareMetadataUpdateSuccessfulTest();
        final StatusUpdateRequest sur = (StatusUpdateRequest) prep.get(0);
        final UserInformation userInfo = (UserInformation) prep.get(1);
        final LocalDateTime timestamp = LocalDateTime.now(java.time.ZoneOffset.UTC);

        final List<Metadata> updatedMetadata = metadataService.updateMetadata(sur, userInfo, timestamp);
        Assert.assertNotNull(updatedMetadata);
        Assert.assertEquals(0, updatedMetadata.size());
    }

    // status update executed correctly for a single metadata set
    @Test
    public void updateMetadataSuccessful_SingleMetadata() throws MissingPermissionException, URISyntaxException, CannotUpdateAnnotationStatusException {

        final List<Object> prep = prepareMetadataUpdateSuccessfulTest();
        final StatusUpdateRequest sur = (StatusUpdateRequest) prep.get(0);
        final UserInformation userInfo = (UserInformation) prep.get(1);
        final Metadata meta = (Metadata) prep.get(2);
        final User user = (User) prep.get(3);
        final Group group = groupRepos.findByName(sur.getGroup());
        final LocalDateTime timestamp = LocalDateTime.now(java.time.ZoneOffset.UTC);

        // also set the responseStatus of the database metadata -> then there is a full match
        meta.setResponseStatus(ResponseStatus.IN_PREPARATION);
        metadataRepos.save(meta);

        final List<Metadata> updateResult = metadataService.updateMetadata(sur, userInfo, timestamp);
        Assert.assertEquals(1, updateResult.size());
        Assert.assertEquals(meta.getId(), updateResult.get(0).getId());

        final Metadata readMetadata = metadataRepos.findById(meta.getId()).get();
        Assert.assertEquals(timestamp, readMetadata.getResponseStatusUpdated());
        Assert.assertEquals(user.getId(), readMetadata.getResponseStatusUpdatedByUser());
        Assert.assertEquals(group.getId(), readMetadata.getResponseStatusUpdatedByGroup());

        // add another item to the database, which has even more properties -> should also be found using same update request
    }

    // status update executed correctly for several metadata sets
    @Test
    public void updateMetadataSuccessful_SeveralMetadata() throws MissingPermissionException, URISyntaxException, CannotUpdateAnnotationStatusException {

        final List<Object> prep = prepareMetadataUpdateSuccessfulTest();
        final StatusUpdateRequest sur = (StatusUpdateRequest) prep.get(0);
        final UserInformation userInfo = (UserInformation) prep.get(1);
        final Metadata firstMeta = (Metadata) prep.get(2);
        final LocalDateTime timestamp = LocalDateTime.now(java.time.ZoneOffset.UTC);

        // also set the responseStatus of the database metadata -> then there is a full match
        firstMeta.setResponseStatus(ResponseStatus.IN_PREPARATION);
        metadataRepos.save(firstMeta);

        // add another item to the database, which has even more properties
        // -> should also be found using same update request
        final Metadata secondMeta = new Metadata(firstMeta);
        final SimpleMetadata kvpSecond = MetadataHandler.getKeyValuePropertyAsSimpleMetadata(secondMeta);
        kvpSecond.put("anotherprop", "anotherval");
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(secondMeta, kvpSecond);
        metadataRepos.save(secondMeta);

        // add a third item which has different response status -> should not match!
        final Metadata thirdMeta = new Metadata(firstMeta);
        thirdMeta.setResponseStatus(ResponseStatus.SENT);
        metadataRepos.save(thirdMeta);

        // launch update request - only meta and secondMeta should match
        final List<Metadata> updateResult = metadataService.updateMetadata(sur, userInfo, timestamp);
        Assert.assertEquals(2, updateResult.size());
        Assert.assertTrue(updateResult.stream().anyMatch(meta -> meta.getId() == firstMeta.getId()));
        Assert.assertTrue(updateResult.stream().anyMatch(meta -> meta.getId() == secondMeta.getId()));
        Assert.assertTrue(updateResult.stream().allMatch(
                meta -> meta.getResponseStatusUpdated().equals(timestamp)));
    }

    @Test
    public void testExceptionIsCaughtWhenCannotDeleteMetadataById() {

        final MetadataRepository mockMetaRepos = Mockito.mock(MetadataRepository.class);
        Mockito.doThrow(new IllegalArgumentException()).when(mockMetaRepos).deleteById(Mockito.anyLong());

        final MetadataService mockMetadataService = new MetadataServiceImpl(null, null, null, null,
                mockMetaRepos);

        mockMetadataService.deleteMetadataById(3);
    }

    // ---------------------------------------
    // TEST FOR UPDATING CONTRIBUTION STATUS
    // ---------------------------------------

    // contribution status update is not executed without required user information
    @Test(expected = CannotUpdateAnnotationStatusException.class)
    public void updateContributionMetadataWithoutUser() throws CannotUpdateAnnotationStatusException, MissingPermissionException {

        metadataService.updateContributionStatus(null, null);
    }

    // ---------------------------------------
    // TEST FOR REMOVAL OF CONTRIBUTIONS IN PREPARATION
    // ---------------------------------------

    // empty list as input for removal delivers again an empty list
    @Test
    public void testRemovalOfEmptyContributionListGivesBackInput() {

        final List<Metadata> result = metadataService.removeContributionsInPreparation(null);
        Assert.assertNotNull(result);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
    }

    // if all items are filtered out, a non-null list is returned
    @Test
    public void testRemovalOfAllContributionsGivesNonNullList() {

        final Metadata meta = getContributionMetadata(Authorities.ISC, ResponseStatus.IN_PREPARATION);

        final List<Metadata> result = metadataService.removeContributionsInPreparation(Arrays.asList(meta));

        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());
    }

    // verify that only the ISC contribution items are filtered out
    @Test
    public void testRemovalOfAllContributions() {

        final Metadata metaEditPrep = getContributionMetadata(Authorities.EdiT,
                ResponseStatus.IN_PREPARATION); // wrong systemId

        final Metadata metaIscPrep = getContributionMetadata(Authorities.ISC,
                ResponseStatus.IN_PREPARATION); // this is the only item to be filtered out

        final Metadata metaIscSent = getContributionMetadata(Authorities.ISC,
                ResponseStatus.SENT); // wrong contribution status

        final Metadata metaIscNoContrib = getContributionMetadata(Authorities.ISC,
                ResponseStatus.IN_PREPARATION);
        metaIscNoContrib.setKeyValuePairs(""); // missing contribution ID

        final List<Metadata> result = metadataService.removeContributionsInPreparation(
                Arrays.asList(metaEditPrep, metaIscPrep, metaIscSent, metaIscNoContrib));

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());
        
        // verify that no entry satisfies the criteria any more for being thrown out
        Assert.assertTrue(result.stream()
                .allMatch(item -> !MetadataHandler.isIscContributionInPreparation(item)));
    }

    // ---------------------------------------
    // HELPER FUNCTIONS
    // ---------------------------------------
    private Metadata getContributionMetadata(final String authority, final ResponseStatus respStatus) {

        final Metadata meta = new Metadata();
        meta.setSystemId(authority);
        
        final SimpleMetadata simpleMeta = new SimpleMetadata();
        simpleMeta.put(Metadata.PROP_CONTRIBUTION_ID, "contribId");
        simpleMeta.put(Metadata.PROP_CONTRIBUTION_STATUS, respStatus.toString());
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(meta, simpleMeta);

        return meta;
    }

    private List<Object> prepareMetadataUpdateSuccessfulTest() throws URISyntaxException {

        final String authority = Authorities.ISC;
        final String login = "login";
        final UserInformation userInfo = new UserInformation(login, null, authority);
        final String uri = "http://dummy";
        final String groupName = "thegroup";
        final String ISC_REF = "ISCReference";
        final String ISC_REF_VAL = "ISC/2018/421";

        final SimpleMetadata metadataToMatch = new SimpleMetadata();
        metadataToMatch.put(ISC_REF, ISC_REF_VAL);
        metadataToMatch.put(Metadata.PROP_RESPONSE_STATUS, "IN_PREPARATION");

        final StatusUpdateRequest sur = new StatusUpdateRequest(groupName, uri, ResponseStatus.SENT);
        sur.setMetadataToMatch(metadataToMatch);

        // save data that does not yet fully match
        final Document doc = new Document(new URI(uri), "title");
        documentRepos.save(doc);
        final Group group = new Group(groupName, false);
        groupRepos.save(group);

        final Metadata meta = new Metadata(doc, group, authority);
        meta.setKeyValuePairs(ISC_REF + ":" + ISC_REF_VAL);
        metadataRepos.save(meta);

        final User user = new User(login);
        userRepos.save(user);
        userInfo.setUser(user);

        return Arrays.asList(sur, userInfo, meta, user); // ugly; nice Tuple classes could be great...
    }

}

package eu.europa.ec.leos.annotate.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@ActiveProfiles("test")
@SuppressWarnings("PMD.TooManyMethods")
public class StringUtilsTest {

    @Test
    public void testIsEmpty() {
        Assert.assertTrue(StringUtils.isEmpty(""));
        Assert.assertTrue(StringUtils.isEmpty(null));
        Assert.assertFalse(StringUtils.isEmpty("NOT-EMPTY"));
    }

    @Test
    public void testIsEqual() {
        Assert.assertTrue(StringUtils.isEqual("", ""));
        Assert.assertTrue(StringUtils.isEqual(null,null));
        Assert.assertTrue(StringUtils.isEqual("EQUAL", "EQUAL"));
        Assert.assertFalse(StringUtils.isEqual("Equal", "EQUal"));
        Assert.assertFalse(StringUtils.isEqual(null,""));
        Assert.assertFalse(StringUtils.isEqual("", null));
    }
}

/*
 * Copyright 2022 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.unit.services.impl.util;

import eu.europa.ec.leos.annotate.Authorities;
import eu.europa.ec.leos.annotate.model.AnnotationStatus;
import eu.europa.ec.leos.annotate.model.SimpleMetadata;
import eu.europa.ec.leos.annotate.model.entity.Annotation;
import eu.europa.ec.leos.annotate.model.entity.Metadata;
import eu.europa.ec.leos.annotate.model.helper.MetadataHandler;
import eu.europa.ec.leos.annotate.services.impl.util.PermissionManager;
import eu.europa.ec.leos.annotate.services.impl.util.PermissionManager.PossiblePermissions;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class PermissionManagerTest {

    @Test
    public void checkNoEditPermissionOnDeletedAnnotation() {
        
        final Annotation annot = new Annotation();
        annot.setStatus(AnnotationStatus.DELETED);
        
        final PossiblePermissions perm = new PossiblePermissions("me", "group1", "world");
        
        final List<String> resultingPerms = 
                PermissionManager.getContributionDeleteEditPermissions(annot, null, perm, null);
        Assert.assertEquals(1, resultingPerms.size());
        Assert.assertEquals("", resultingPerms.get(0));
    }
    
    @Test
    public void checkNoEditPermissionOnNormalAnnotation() {
        
        final Annotation annot = new Annotation();
        annot.setMetadata(new Metadata());
        annot.setStatus(AnnotationStatus.NORMAL);
        
        final PossiblePermissions perm = new PossiblePermissions("me", "group1", "world");
        
        final List<String> resultingPerms = 
                PermissionManager.getContributionDeleteEditPermissions(annot, null, perm, null);
        Assert.assertEquals(1, resultingPerms.size());
        Assert.assertEquals("", resultingPerms.get(0));
    }
    
    @Test
    public void checkHasEditPermissionOnContribution() {
        
        final Annotation annot = new Annotation();
        final Metadata meta = new Metadata();
        meta.setSystemId(Authorities.ISC);
        
        final SimpleMetadata simpleMeta = new SimpleMetadata();
        simpleMeta.put(Metadata.PROP_CONTRIBUTION_ID, "contribId");
        
        MetadataHandler.setKeyValuePropertyFromSimpleMetadata(meta, simpleMeta);
        annot.setMetadata(meta);
        annot.setStatus(AnnotationStatus.NORMAL);
        
        final PossiblePermissions perm = new PossiblePermissions("myself", "group1", "world");
        
        final List<String> resultingPerms = 
                PermissionManager.getContributionDeleteEditPermissions(annot, null, perm, null);
        Assert.assertEquals(1, resultingPerms.size());
        Assert.assertEquals("myself", resultingPerms.get(0));
    }
}

package eu.europa.ec.leos.annotate.integration.services.impl.util;

import eu.europa.ec.leos.annotate.model.TemporaryData;
import eu.europa.ec.leos.annotate.model.TemporaryDataJson;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCache;
import eu.europa.ec.leos.annotate.services.impl.util.TemporaryDataCleanerRunnable;
import org.junit.Assert;
import org.junit.Test;

public class TemporaryDataCleanerRunnableTest {
    @Test
    public void testCleanerRun() {
        final TemporaryDataCache temporaryDataCache = new TemporaryDataCache();
        final TemporaryDataJson temporaryDataJson = new TemporaryDataJson();
        final TemporaryData temporaryData = new TemporaryData(temporaryDataJson);
        temporaryDataCache.addTemporaryData(temporaryData);
        Assert.assertEquals(1, temporaryDataCache.size());

        TemporaryDataCleanerRunnable runnable = new TemporaryDataCleanerRunnable(-1, temporaryDataCache);
        runnable.run();
        Assert.assertEquals(0, temporaryDataCache.size());
    }

}

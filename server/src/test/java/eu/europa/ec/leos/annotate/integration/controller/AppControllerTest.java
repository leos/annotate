/*
 * Copyright 2018 European Commission
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.annotate.integration.controller;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.europa.ec.leos.annotate.helper.SpotBugsAnnotations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.config.name=annotate")
@WebAppConfiguration
@ActiveProfiles("test")
public class AppControllerTest {

    /**
     * Test that the app.html endpoint actually return a html
     */

    // -------------------------------------
    // Required services, repositories, mocks
    // -------------------------------------

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    // -------------------------------------
    // Preparation of mock objects
    // -------------------------------------
    @Before
    public void setupTests() {

        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    // -------------------------------------
    // Tests
    // -------------------------------------

    /**
     * successfully retrieve the API root, expected HTTP 200 and the some data
     */
    @Test
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    public void testAppHtml() throws Exception {
        // send API root request
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/app.html");

        final ResultActions result = this.mockMvc.perform(builder);

        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();
        Assert.assertNotNull(responseString);

        final int baseStartAt = responseString.indexOf("<base");
        Assert.assertTrue(baseStartAt > -1);

        final int hrefStartAt = responseString.indexOf("href=", baseStartAt);
        final int hrefEndAt = responseString.indexOf("\"", hrefStartAt+6);
        final String hrefValue = responseString.substring(hrefStartAt+6, hrefEndAt);
        Assert.assertEquals("/annotate/client/", hrefValue);

        final int versionStartAt = responseString.indexOf("?v=");
        final int versionValueEndAt = responseString.indexOf("\"", versionStartAt);
        final String versionValue = responseString.substring(versionStartAt+3, versionValueEndAt);
        Assert.assertFalse(versionValue.isEmpty());
    }

    @Test
    @SuppressFBWarnings(value = SpotBugsAnnotations.FieldNotInitialized, justification = SpotBugsAnnotations.FieldNotInitializedReason)
    public void testAppHtmlWithBasePathParam() throws Exception {
        // send API root request
        final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/app.html?basePath=/myClient");
        final ResultActions result = this.mockMvc.perform(builder);

        // expected: Http 200
        result.andExpect(MockMvcResultMatchers.status().isOk());

        final MvcResult resultContent = result.andReturn();
        final String responseString = resultContent.getResponse().getContentAsString();
        Assert.assertNotNull(responseString);

        final int baseStartAt = responseString.indexOf("<base");
        Assert.assertTrue(baseStartAt > -1);

        final int hrefStartAt = responseString.indexOf("href=", baseStartAt);
        final int hrefEndAt = responseString.indexOf("\"", hrefStartAt+6);
        final String hrefValue = responseString.substring(hrefStartAt+6, hrefEndAt);
        Assert.assertEquals("/myClient/", hrefValue);
    }
}

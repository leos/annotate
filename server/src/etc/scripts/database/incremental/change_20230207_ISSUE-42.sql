--
-- Copyright 2022 European Commission
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
--     https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

------------------------------------
-- Changes to initial Oracle
--
-- change initiated by ISSUE-42
------------------------------------

ALTER TABLE "ANNOTATIONS" ADD ("FEEDBACK_STATUS" NUMBER(1, 0));
ALTER TABLE "ANNOTATIONS" ADD ("FEEDBACK_UPDATED" DATE);
ALTER TABLE "ANNOTATIONS" ADD ("FEEDBACK_UPDATED_BY_USER" NUMBER);

COMMENT ON COLUMN "ANNOTATIONS"."FEEDBACK_STATUS" IS 'Feedback processing status: IN_PREPARATION(0), SENT(1)';
COMMENT ON COLUMN "ANNOTATIONS"."FEEDBACK_UPDATED" IS 'Last date when the feedback was updated';
COMMENT ON COLUMN "ANNOTATIONS"."FEEDBACK_UPDATED_BY_USER" IS 'User that last updated the feedback';
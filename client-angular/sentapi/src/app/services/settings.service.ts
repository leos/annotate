import { jsonConfigsFrom } from '../../../../shared/config/json-config';
import { hostPageConfig } from '../../../../sidebar/src/app/sidebar/host-config';
import * as apiUrlUtil from '../../../../sidebar/src/app/sidebar/get-api-url';
import { Injectable } from '@angular/core';
import { ISentApiSettings } from '../models/SentApiSettings';

export interface ISettingsService {
    getSettings(): ISentApiSettings;
}

@Injectable()
export class SettingsService implements ISettingsService {
    private settings: ISentApiSettings;

    constructor() {
        var settings: any = jsonConfigsFrom(window.document) as any;
        settings = Object.assign(settings, hostPageConfig(window)) as any;
        settings.apiUrl = apiUrlUtil.getApiUrl(settings);
        this.settings = settings as ISentApiSettings;
    }

    public getSettings(): ISentApiSettings {
        return Object.assign({}, this.settings);
    }
}


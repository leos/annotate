'use strict';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http'
import * as RESPONSE_STATUSES from "../../../../shared/response-status";
import { v4 as uuid_v4 } from 'uuid';
import { Observable, catchError, forkJoin, tap, throwError, of } from 'rxjs';
import { SettingsService } from './settings.service';
import { ISentApiSettings } from '../models/SentApiSettings';
import { IHostBridgeDocument, ISentApiHostBridge } from '../models/SentApiHostBridge';

export interface SendResultCallbackResult {
  result: string;
  message: string;
}

export type SendResultCallback = (result: SendResultCallbackResult) => void;

interface UriResponse {
  status: string;
  uri: string;
}

interface IMessageResponse {
  status: string;
  message: string;
}

class MessageResponse implements IMessageResponse  {
  constructor(public status: string, public message: string){};
}

export interface IApiService {
  sendRequest(uris: string[], callback: SendResultCallback): void;
}

@Injectable()
export class ApiService implements IApiService {
  private hostBridge: ISentApiHostBridge;
  private token: string;
  private clientId: string;
  private retries: number;
  private status: string;
  private settings: ISentApiSettings;

  constructor(settingsService: SettingsService, private httpClient: HttpClient) {
    this.settings = settingsService.getSettings();
    this.hostBridge = (window.document as IHostBridgeDocument).hostBridge;
    this.token = "";
    this.clientId = uuid_v4();
    this.retries = 0;
    this.status = RESPONSE_STATUSES.SENT
  }

  public sendRequest(uris: string[], callback: SendResultCallback): void {
    const group: string = this.settings.parameters?.group || '';
    const metadata: any =  this.settings.metadata;

    let responses: any[] = [];
    let nbrFailure: number = 0;
    let sentapiPromises: Observable<any>[] = [];

    console.log("Request SecurityToken to be sent to host");
    if (!this.hostBridge.requestSecurityToken) {
      console.log("Error while getting token from configuration");
      this._parseResult({status: "FAIL", message: "Error while getting token from configuration"}, callback);
      return;
    }

    const self = this;
    // Add handler on host bridge to let leos application responds
    this.hostBridge.responseSecurityToken = (data: string) => {
      console.log("Received message from host for request SecurityToken");
      let timeout = setTimeout((() => self._parseResult({status: "FAIL", message: "Couldn't get token, check configuration and server"}, callback)), 30000);
      self._getToken(data).subscribe({ 
        next(response: HttpResponse<any>){
          clearTimeout(timeout);
          self.token = response.body.access_token;
          let timeouts: any = {};

          uris.forEach((uri: string) => {
            timeouts[uri] = setTimeout((() => self._parseResult({status: "FAIL", message: "Timeout sending change status request"}, callback)), 30000);
            let sentapiPromise: Observable<any> = self._changeStatus(self.token, group, uri, metadata).pipe(
              tap(() => {
                clearTimeout(timeouts[uri]);
                responses.push({"uri": uri, "status": "SUCCESS"});
              }),
              catchError((response: HttpResponse<any>) => {
                clearTimeout(timeouts[uri]);
                if (response.status === 404) {  
                  //Didn't find any annotations for this document
                  responses.push({"uri": uri, "status": "SUCCESS"});
                  return of();
                }

                nbrFailure++;
                if (response.status >= 500) {  
                  //Didn't find any annotations for this document
                  console.log("Error while sending status request for uri " + uri + "; looks that server is down - " + response.status); 
                  responses.push({"uri": uri, "status": "FAIL"});
                  return of();
                }
                console.log("Error while sending status request for uri " + uri + "; the reason is " + self._getReason(response));
                responses.push({"uri": uri, "status": "FAIL"});
                return of();        
              })
            );
            sentapiPromises.push(sentapiPromise);
          });
          forkJoin(sentapiPromises).subscribe({
            error() {
              console.log(`Error while sending status requests. ${responses?.length || 0} of ${uris.length} responses passed.`);
              self._parseResult(responses, callback, nbrFailure);
            },            
            complete() {
                console.log(`Finished all status requests`);
                self._parseResult(responses, callback, nbrFailure);
            },
          })
        }, 
        error(response: any) {
          clearTimeout(timeout);
          console.log("Error while getting token; the reason is " + self._getReason(response));
          self._parseResult({status: "FAIL", message: "Token request failure"}, callback);
        }
      });
    }
    this.hostBridge.requestSecurityToken();
  }

  private _getToken(token: string): Observable<HttpResponse<any>> {
    return this.httpClient.post(this.settings.apiUrl + "token", 
      `assertion=${token}&grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer`,
      {
        headers: new HttpHeaders({
          "X-Client-Id": this.clientId,
          "Content-Type": "application/x-www-form-urlencoded",
          timeout: '30000'
        }),
        observe: 'response'
      }
    )
  }

  private _changeStatus(token: string, group: string, uri: string, metadata: any): Observable<HttpResponse<any>> {
    return this.httpClient.post(this.settings.apiUrl + `changeStatus?group=${group}&uri=${uri}&responseStatus=${this.status}`,
      metadata,
      {
        headers: new HttpHeaders({
          "X-Client-Id": this.clientId,
          "Authorization": "Bearer " + token,
          timeout: '30000'
        }),
        observe: 'response'
      }
    );
  }

  private _parseResult(result: UriResponse[] | MessageResponse | undefined | null, callback: SendResultCallback, nbrFailure?: number): void {
    const _nbrFailure: number = nbrFailure || 0;
    if (result instanceof Array && _nbrFailure < (result?.length || 0)) {
      if (_nbrFailure == 0 && this.retries == 0) {
        return callback({result: "SUCCESS", message: "Annotations successfully set to 'SENT'"});
      }
      if (_nbrFailure == 0 && this.retries > 0) {
        return callback({result: "FAIL", message: "Rollback done"});
      }
      if (this.retries < 5) {
        let newUris: string[] = [];
        result?.forEach((res: UriResponse) => {
          if (res.status === "SUCCESS") {
            newUris.push(res.uri);
          }
        });
        this.retries++;
        this.status = RESPONSE_STATUSES.IN_PREPARATION;
        this.sendRequest(newUris, callback);   
        return;
      }
      return callback({result: "FAIL", message: "Consumed all retries for rollback"});
    }

    if (result == undefined || !(result instanceof MessageResponse)) return callback({result: "FAIL", message: `${(result as any)?.message}`});
    if (result.status === "SUCCESS") {
      return callback({result: "SUCCESS", message: result.message});
    }
    console.log("Error occurred: " + result.message);
    callback({result: "FAIL", message: result.message});
  }

  private _getReason(response: any): string {
    if (!response) return `Response: '${response}'`;
    if (!response.status && !response.data) 
      return "Response data: '" + response.data + ", status: '" + response.status +"'";
    if (response.status && !response.data) 
      return response.status;
    if (response.data.reason) 
      return response.data.reason;
    if (response.data.error_description) 
      return response.data.error_description; 
    return `Response data reason: '${response.data.reason}', error_description: '${response.data.error_description}'`;
  }
}
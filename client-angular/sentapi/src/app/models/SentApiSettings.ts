export interface ISentApiSettingsMessageButtons {
    yes?: string;
}
    
export interface ISentApiSettingsMessage {
    title?: string;
    confirmMessage?: string;
    failureMessage?: string;
    buttons?: ISentApiSettingsMessageButtons;
}

export interface ISentApiSettingsParameters {
    group?: string;
    uris?: string[];
}


export interface ISentApiSettings {
    assetRoot?: string;
    apiUrl?: string;
    message?: ISentApiSettingsMessage;
    parameters?: ISentApiSettingsParameters;
    metadata: any;
}
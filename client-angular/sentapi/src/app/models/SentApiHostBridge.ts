export interface ISentApiHostBridge {
    callback?: Function;
    requestSecurityToken?: Function;
    responseSecurityToken?: Function;
}


export interface IHostBridgeDocument extends Document {
    hostBridge: ISentApiHostBridge;
}
  
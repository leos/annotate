import { ApplicationRef, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ApiService } from './services/api.service';
import { SettingsService } from './services/settings.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    SettingsService
  ]
})
export class AppModule {
  public ngDoBootstrap(appRef: ApplicationRef) {
    const element = document.querySelector('annotate-sentapi-app');
    appRef.bootstrap(AppComponent, element);
  }
}

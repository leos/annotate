import { Component, OnInit } from '@angular/core';
import { ApiService, SendResultCallbackResult } from './services/api.service';
import { SettingsService } from './services/settings.service';
import { ISentApiSettingsMessageButtons } from './models/SentApiSettings';
import { ISentApiHostBridge } from './models/SentApiHostBridge';

interface HostBridgeDocument extends Document {
  hostBridge: any;
}

@Component({
  selector: 'sentapi-app',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  public title = 'annotate-sentapi';

  public isError: boolean;
  public message: string;
  public buttons: ISentApiSettingsMessageButtons;

  private uris?: string[];
  private hostBridge: ISentApiHostBridge;
  private sending: boolean;
  private sent: boolean;
  private _callback?: Function;
  private failureMessage: string;
  private $window: Window;
  private settings: any;

  constructor(private api: ApiService, settingsService: SettingsService) {
    this.$window = window;
    this.settings = settingsService.getSettings();
    this.sending = false;
    this.sent = false;
    this.isError = false;
    this.uris = undefined;
    this.message = '';
    this.failureMessage = '';
    this.buttons = {};
    this.hostBridge = (window.document as HostBridgeDocument).hostBridge;
  }

  public get isSending(): boolean {
    return this.sending;
  }

  public get isSent(): boolean {
    return this.sent;
  }

  public ngOnInit(): void {
    if (this.hostBridge?.callback && typeof(this.hostBridge.callback) === 'function') {
      this._callback = this.hostBridge.callback;
    }
    else {
      this.isError = true;
      this.message = "Missing callback in configuration";
      console.log(this.message);
    }

    if (this.settings.message && this.settings.message.title) {
      this.$window.document.title = this.settings.message.title;
    }
    else {
      this.isError = true;
      this.message = "Missing title in configuration";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }

    if (this.settings.message && this.settings.message.confirmMessage) {
      this.message = this.settings.message.confirmMessage;
    }
    else {
      this.isError = true;
      this.message = "Missing message in configuration";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }
  
    if (this.settings.message && this.settings.message.failureMessage) {
      this.failureMessage = this.settings.message.failureMessage;
    }
    else {
      this.isError = true;
      this.failureMessage = "Missing failure message in configuration";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }
    
    this.buttons = {};
    if (this.settings.message && this.settings.message.buttons && this.settings.message.buttons.yes) {
      this.buttons.yes = this.settings.message.buttons.yes;
    }
    else {
      this.isError = true;
      this.message = "Missing button's text in configuration";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }
    if (!(this.settings && this.settings.parameters && this.settings.parameters.group)) {
      this.isError = true;
      this.message = "Group parameter is mandatory";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }

    this.uris = undefined;
    if (!(this.settings && this.settings.parameters && this.settings.parameters.uris)) {
      this.isError = true;
      this.message = "URIs parameter is mandatory";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    } else {
        this.uris = this.settings.parameters.uris;
    }
  
    if (!(this.settings && this.settings.metadata)) {
      this.isError = true;
      this.message = "Metadata parameter is mandatory";
      console.log(this.message);
      this.callback({result: "FAIL", message: this.message});
    }
  }

  private callback(value: any) {
    if (!this._callback) return;
    this._callback(value);
  }

  private requestCallback(response: SendResultCallbackResult): void {
    this.sending = false;
    this.sent = true;
    this.callback(response);

    if (response.result == "SUCCESS") {
      window.close();
      return;
    }

    this.isError = true;
    this.message = this.failureMessage;
  }

  public sendRequest(): void {
    this.sending = true;
    const self = this;
    this.api.sendRequest(this.uris || [], (response: SendResultCallbackResult) => self.requestCallback(response));
  }
}

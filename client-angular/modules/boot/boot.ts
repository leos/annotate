interface SidebarApp {
    url?: string;
    id?: string;
}

export interface BootSettingsManifest {
  [property: string]: string;
}

export interface BootSettings {
    assetRoot?: string;
    manifest?: BootSettingsManifest;
    sidebarApp?: SidebarApp;
}


function getScriptIdFromSrcPath(path: string): string {
  var atIndex = path.lastIndexOf('/') + 1;
  var scriptId = path.substring(atIndex);

  var toIndex = scriptId.lastIndexOf('.');
  scriptId = scriptId.substring(0, toIndex+1);
  scriptId = scriptId.replace('.', '-');
  return scriptId;
}


function injectStylesheet(_document: Document, href: string): void {
  var link = _document.createElement('link');
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = href;
  _document.head.appendChild(link);
}

function injectScript(_document: Document, src: string): void {
  var script = _document.createElement('script');
  script.type = 'text/javascript';
  script.id = 'script-' + getScriptIdFromSrcPath(src);
  script.src = src;

  // Set 'async' to false to maintain execution order of scripts.
  // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
  script.async = false;
  _document.head.appendChild(script);
}

function injectAssets(_document: Document, settings: BootSettings, assets: string[]) {
  const versionParam = Date.now();
  assets.forEach((path: string) => {
    var url = settings.assetRoot + (settings.assetRoot?.endsWith("/") ? '' : '/') + path + `?v=${versionParam}`;
    if (url.match(/\.css/)) {
      injectStylesheet(_document, url);
    } else {
      injectScript(_document, url);
    }
  });
}

function bootAdditionalHypothesisClient(_document: Document, settings: BootSettings, appId: string) {
  const hypothesisAppEl: Element | null = _document.querySelector(`hypothesis-app-${appId}`);
  if (hypothesisAppEl) {
    return;
  }

  var sidebarUrl = _document.createElement('link');
  sidebarUrl.rel = `sidebar-${appId}`;
  sidebarUrl.href = settings.sidebarApp?.url || '';
  sidebarUrl.type = 'application/annotator+html';
  _document.head.appendChild(sidebarUrl);

  injectAssets(_document, settings, ['scripts/annotator.bundle.js']);
}

function bootHypothesisClient(_document: Document, settings: BootSettings): void {
  // Detect presence of Hypothesis in the page
  const appLinkEl: Element | null = _document.querySelector('link[type="application/annotator+html"]');
  if (appLinkEl) {
    if (settings.sidebarApp?.id) {
        bootAdditionalHypothesisClient(_document, settings, settings.sidebarApp.id);
    }
    return;
  }

  // Register the URL of the sidebar app which the Hypothesis client should load.
  // The <link> tag is also used by browser extensions etc. to detect the
  // presence of the Hypothesis client on the page.
  const sidebarUrl: HTMLLinkElement = _document.createElement('link');
  sidebarUrl.rel = 'sidebar';
  sidebarUrl.href = settings.sidebarApp?.url || '';
  sidebarUrl.type = 'application/annotator+html';
  _document.head.appendChild(sidebarUrl);

  // Register the URL of the annotation client which is currently being used to drive
  // annotation interactions.
  const clientUrl: HTMLLinkElement = _document.createElement('link');
  clientUrl.rel = 'hypothesis-client';
  clientUrl.href = (settings.assetRoot || '') + '/';
  clientUrl.type = 'application/annotator+javascript';
  _document.head.appendChild(clientUrl);

  injectAssets(_document, settings, [
    // Vendor code and polyfills
    //'scripts/polyfills.bundle.js',
    'scripts/jquery.min.js',

    // Main entry point for the client
    'scripts/annotator.bundle.js',

    'styles/icomoon.css',
    'styles/opensans.css',
    'styles/annotator.css',
    'styles/pdfjs-overrides.css',
  ]);
}

export function bootHypothesisApp(_document: Document, settings: BootSettings = {}): void {
  if (_document.querySelector('hypothesis-app')) return;
  bootHypothesisClient(_document, settings);
}

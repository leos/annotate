import { bootHypothesisApp } from './boot';
import { jsonConfigsFrom } from '../../shared/config/json-config';
import { IHypothesisJsonConfig } from "../../shared/models/config.model";

const jsonConfig: IHypothesisJsonConfig = jsonConfigsFrom(window.document);

bootHypothesisApp(window.document, {
    assetRoot: jsonConfig.assetRoot,
    sidebarApp: {
        url: jsonConfig.sidebarAppUrl,
        id: jsonConfig.sidebarAppId
    },
    manifest: {}
});
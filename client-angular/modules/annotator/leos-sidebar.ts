/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Do LEOS specific actions in this class
import $ from './imports/jquery';

import Sidebar, { ISidebar } from './sidebar';
import HostBridgeManager, { IHostBridgeManager } from './host-bridge-manager';
import LeosSuggestionSelection, { ILeosSuggestionSelection } from './anchoring/suggestion-selection';
import * as events from '../../shared/sidebar-events';
import * as LEOS_config from '../../shared/config';
import * as LEOS_SYSTEM_IDS from '../../shared/system-id';
import * as OPERATION_MODES from '../../shared/operationMode';
import * as ANNOT_POPUP_DEFAULT_STATUS from '../../shared/annotationPopupDefaultStatus';
import { IToolbar } from './plugin/toolbar';
import { Annotation } from '../../shared/models/annotation.model';
import { AnnotatorAnchor, AnnotatorDocumentInfo } from './models/annotator';
import {Subject, take} from "rxjs";

const {
  FRAME_DEFAULT_WIDTH,
} = LEOS_config;
const {
  FRAME_DEFAULT_MIN_WIDTH,
} = LEOS_config;
const {
  FRAME_DEFAULT_CLOSE_WITH,
} = LEOS_config;

type ExtendedElement = Element & { offsetLeft: number, offsetTop: number, offsetHeight: number };
type ExtendedHtmlElement = HTMLElement & { width: number, height: number, getContext: any  };
type ExtendedParentNode = ParentNode & { nextElementSibling: any, previousElementSibling: any };
type ExtendedEvent = Event & { operationMode: any };
type FilterParentEvent = Event & {filterParentElementId?: string};

function hasZeroRectSize(element?: Element | null): boolean {
  if (!element) return true;
  if (element.getBoundingClientRect().width > 0) return false;
  return element.getBoundingClientRect().height == 0;
}


function toggleAnnotationHighlight(element: Element, highlightTag: string, showHighlight: boolean): void {
  if (showHighlight) {
      $(element).addClass(highlightTag);
  } else {
      $(element).removeClass(highlightTag);
  }
}


function toggleAnnotationHighlights(annotation: Annotation | undefined, hypothesisHighlights: Element[]): void {
  if (!annotation) {
      return;
  }

  let showHighlight = (annotation.showHighlight === undefined) ? true : annotation.showHighlight;
  let isLeosHighlight = annotation.tags && annotation.tags[0] && (annotation.tags[0] === 'highlight');
  let highlightTag = isLeosHighlight ? 'leos-annotator-highlight' : 'annotator-hl';
  hypothesisHighlights.filter(highlightEl => highlightEl.classList.contains(`hl-${annotation.id}`))
      .filter(annotationHighlightEl => showHighlight ? !annotationHighlightEl.classList.contains(highlightTag) : annotationHighlightEl.classList.contains(highlightTag))
      .forEach(annotationHighlightEl => { toggleAnnotationHighlight(annotationHighlightEl, highlightTag, showHighlight) });
}

export interface ILeosSidebar extends ISidebar {
  readonly hostBridgeManager?: IHostBridgeManager;
  setAllVisibleGuideLines(shouldShowGuideLines?: boolean, storePrevState?: boolean): void;
  getAnnotationPopupStatus(): boolean;
  changeAnnotationPopupStatus(shouldShowAnnotationPopup?: boolean): void;
  restoreAllGuideLinesState(): void;
  refreshAnnotations(): void;
  isHighlightsVisible(): boolean;
  isGuideLinesVisible(): boolean;
  setIsHighlightsVisible(visible: boolean): void;
  setIsGuideLinesVisible(visible: boolean): void;
  getVisibleGuideLinesPrevState(): boolean;
  newDocumentNoteClick(): void;
}

export default class LeosSidebar extends Sidebar implements ILeosSidebar {
  private _appId: string | null;
  private cachedCoordinates: any;
  private coordinatesSetS = new Subject<boolean>();
  private isAngularUI: boolean;
  private leosSuggestionSelection: ILeosSuggestionSelection;

  readonly hostBridgeManager?: IHostBridgeManager;
  private frameCurrentWidth?: number;
  private hoveredAnnotations: any;
  private selectedAnnotations: any;
  private leosCanvas: HTMLCanvasElement | null = null;

  constructor(element: Element, config: any) {
    super(element, config);

    const self = this;
    this.cachedCoordinates = null;
    this.isAngularUI = Boolean(config.isAngularUI);
    this.selectedAnnotations = {};
    this.hoveredAnnotations = {};

    this._appId = config.sidebarAppId;
    const container: HTMLElement | undefined = !this.container ? undefined : this.container[0];
    container?.addEventListener('annotationRefresh', () => self._refresh());
    container?.addEventListener('annotationSidebarResize', () => self._setFramePosition());
    container?.ownerDocument?.defaultView?.addEventListener('resize', () => self._onResize(true));
    container?.addEventListener('scroll', () => self._onScroll());
    $(window).on('click', (event: any) => self._onWindowClick(event, self));
    container?.addEventListener('filterHighlights', (event: FilterParentEvent) => self.options.Document.filterParentElementId = event.filterParentElementId);
    
    
    config.docType = 'LeosDocument';
    if (container) {
      this._setupCanvas(container);
      this.hostBridgeManager = new HostBridgeManager((container as any)['hostBridge'], this.crossframe);
    }

    this.leosSuggestionSelection =
        new LeosSuggestionSelection(this.options.Document.allowedSelectorTags, this.options.Document.editableSelector, this.options.Document.notAllowedSuggestSelector);

    this.crossframe.on('scrollAnnotations', () => {
      const self = this;
      this.coordinatesSetS.asObservable().pipe(take(1)).subscribe((scroll: boolean ) => {
        if (scroll) {
          setTimeout(() => self._onScroll(), 250);
        }
      })
    });

    this.crossframe.on('requestAnnotationAnchor', (annot: Annotation, callback: Function) => {
      const suggestionPositionSelector = self.leosSuggestionSelection.getSuggestionSelectors(annot, self.anchors);
      if (suggestionPositionSelector != null) {
        return callback(null, suggestionPositionSelector);
      } else {
        return callback('No available highlights');
      }
    });

    this.adderCtrl.extend(this.adder[0], (container as any)['hostBridge'], {
      onComment: this.onComment.bind(this),
      onSuggest: this.onSuggest.bind(this),
      onHighlight: this.onHighlight.bind(this),
    });

    this.on('panelReady', () => {
      self._setFrameStyling();
    });

    this.on(events.ANNOTATIONS_LOADED, () => {
      if (eval(localStorage.getItem('shouldAnnotationTabOpen') || '')) {
        self.onDefaultLoad();
        localStorage.setItem('shouldAnnotationTabOpen', 'false');
        self._refreshAnnotationLinkLines(self.visibleGuideLines, self, null);
      }
    });

    this.on('LEOS_annotationsSynced', () => {
      self._setupAnchorListeners(self);
      self._setupOverlaidAnnotations();
      self._refreshAnnotationLinkLines(self.visibleGuideLines, self, 500);
      self._filterHighlights(self);
    });

    this.crossframe.on('LEOS_cancelFilterHighlights', () => { return self.options.Document.filterParentElementId = null; });
    this.crossframe.on('LEOS_clearSelection', () => self._onClearSelection());
    this.crossframe.on('LEOS_updateIdForCreatedAnnotation',
      (annotationTag: string, createdAnnotationId: string) => self._updateIdForCreatedAnnotation(annotationTag, createdAnnotationId, self));
    this.crossframe.on('LEOS_createdAnnotation', (annotationTag: string, createdAnnotationId: string) => self._setupOverlaidAnnotations());
    this.crossframe.on('LEOS_refreshAnnotationLinkLines', () => self._refreshAnnotationLinkLines(this.visibleGuideLines, self, null));
    this.crossframe.on('LEOS_syncCanvasResp', (response: any) => self.drawGuideLines(response, self));
    this.crossframe.on('LEOS_selectAnnotation', (annotation: Annotation) => self._crossFrameSelectAnnotation(annotation, self));
    this.crossframe.on('LEOS_selectAnnotations', (annotations: Annotation[]) => self._crossFrameSelectAnnotations(annotations, self));
    this.crossframe.on('LEOS_deselectAllAnnotations', () => self._deselectAllAnnotations(self));
    this.crossframe.on('LEOS_setSidebarToggleBtnEnabled', (enabled?: boolean | null) => self.setSidebarToggleBtnEnabled(enabled));
    this.crossframe.on('focusAnnotations', (tags?: string[] | null) => {
      if (!tags) { tags = []; } 
      self.focusAnnotations(tags, self); 
    });
    this.crossframe.on('LEOS_refreshAnnotationHighlights', (annotations: Annotation[]) => { self._refreshAnnotationHighlights(annotations) });

    this._onResize( false);

    if (!this.options.Document.showGuideLinesButton) {
      this.plugins['Toolbar']?.disableGuideLinesBtn();
    }
    if (this.options.Document.annotationPopupDefaultStatus === ANNOT_POPUP_DEFAULT_STATUS.HIDE) {
      this.plugins['Toolbar']?.disableAnnotPopupBtn();
    }

    this._setToolbarMode();

  }

  protected override _onSelection(range: Range): void {
    let self = this;
    if ((self.adderCtrl.hostBridge["requestUserPermissions"]) && (typeof self.adderCtrl.hostBridge["requestUserPermissions"] == 'function')) {
      // Add handler on host bridge to let leos application responds
      self.adderCtrl.hostBridge["responseUserPermissions"] = function (data: any) {
        if (data.includes('CAN_COMMENT') || data.includes('CAN_SUGGEST')) {
          self.handleOnSelection(range);
        }
      };
      self.adderCtrl.hostBridge["requestUserPermissions"]();
    }
  }

  private handleOnSelection(range: Range): void {
    if ((this.options.Document.operationMode === OPERATION_MODES.READ_ONLY || this.options.Document.operationMode === OPERATION_MODES.STORED_READ_ONLY) || !this.annotationPopupStatus) {
      return;
    }

    if (this._hasElements($(range.endContainer.parentNode!).closest('.leos-authnote-table').toArray())) {
      this.adderCtrl.disableAnnotations();
      return;
    } else {
      this.adderCtrl.enableAnnotations();
    }

    if (this._isMovedElement(range.startContainer.parentElement)) {
      this.adderCtrl.disableCommentButton();
      this.adderCtrl.disableSuggestionButton();
      this.adderCtrl.disableHighlightButton();
      super._onSelection(range);
      return;
    }

    const isSuggestionAllowed = this.leosSuggestionSelection.isSelectionAllowedForSuggestion(range.cloneContents(), range.commonAncestorContainer as Element);
    if (!this.options.Document.disableSuggestionButton && isSuggestionAllowed) {
      this.adderCtrl.enableSuggestionButton();
    } else {
      this.adderCtrl.disableSuggestionButton();
    }

    const isHighlightAllowed = this.options.Document.authority !== LEOS_SYSTEM_IDS.ISC;
    if (isHighlightAllowed) {
      this.adderCtrl.addHighlightButton();
      if(this.options.Document.disableSuggestionButton) {
        this.adderCtrl.disableHighlightButton();
      }
    } else { 
      this.adderCtrl.removeHighlightButton();
    }
    super._onSelection(range);
  }

  private _isMovedElement(element: HTMLElement | null | undefined): boolean {
    if (!element) {
      return false;
    }

    let nextElement: HTMLElement | null = element;
    for(let i = 0; i<10; i++) {
      if (!nextElement) {
        return false;
      }
      if (nextElement.getAttribute('leos:softaction') === 'move_to') {
        return true;
      }
      nextElement = nextElement.parentElement;
    }
    return false;
  }

  override getDocumentInfo(): Promise<AnnotatorDocumentInfo> {
    const getInfoPromise = super.getDocumentInfo();
    const self = this;
    return getInfoPromise.then((docInfo) => {
      return self.plugins['Document']?.getLeosDocumentMetadata()
        .then((leosMetadata: any) => {
          if (leosMetadata != null) {
            docInfo.metadata.metadata = leosMetadata;
          }
          return {
            uri: docInfo.uri,
            metadata: docInfo.metadata,
            frameIdentifier: docInfo.frameIdentifier,
          };
        })
        .catch(() => {
          return {
            uri: docInfo.uri,
            metadata: docInfo.metadata,
            frameIdentifier: docInfo.frameIdentifier,
          };
        }
    )});
  }

  setAllVisibleGuideLines(shouldShowGuideLines: boolean = false, storePrevState: boolean = false) {
    if (this.visibleHighlights) {
      this.crossframe.call('LEOS_setVisibleGuideLines', shouldShowGuideLines, storePrevState);
      this.publish('LEOS_setVisibleGuideLines', shouldShowGuideLines, storePrevState);
      setTimeout((() => this._refreshAnnotationLinkLines(shouldShowGuideLines, this, null)), 500);
    }
  }

  newDocumentNoteClick(): void {
    let self = this;
    if ((self.adderCtrl.hostBridge["requestUserPermissions"]) && (typeof self.adderCtrl.hostBridge["requestUserPermissions"] == 'function')) {
      // Add handler on host bridge to let leos application responds
      self.adderCtrl.hostBridge["responseUserPermissions"] = function (data: any) {
        if (data.indexOf('CAN_UPDATE') !== -1) {
          self.createPageNote();
          self.show();
        }
      };
      self.adderCtrl.hostBridge["requestUserPermissions"]();
    }
  }

  restoreAllGuideLinesState() {
    this.crossframe.call('LEOS_restoreGuideLinesState');
    this.publish('LEOS_restoreGuideLinesState');
    setTimeout((() => this._refreshAnnotationLinkLinesAndUpdateIcon(this.visibleGuideLines, this, null)), 500);
  }

  changeAnnotationPopupStatus(shouldShowAnnotationPopup?: boolean) {
    if (shouldShowAnnotationPopup !== undefined) {
      this.annotationPopupStatus = shouldShowAnnotationPopup;
    } else {
      this.annotationPopupStatus = !this.annotationPopupStatus;
    }
  }

  private setSidebarToggleBtnEnabled(enable?: boolean | null): void {
    if (!!enable) {
      this.toolbarClass?.enableToggleBtn();
    }
    this.toolbarClass?.disableToggleBtn();
  }

  getAnnotationPopupStatus(): boolean {
    return this.annotationPopupStatus;
  }

  isHighlightsVisible(): boolean {
    return this.visibleHighlights;
  }

  isGuideLinesVisible(): boolean {
    return this.visibleGuideLines;
  }

  setIsHighlightsVisible(visible: boolean): void {
    this.visibleHighlights = visible;
  }

  setIsGuideLinesVisible(visible: boolean): void {
    this.visibleGuideLines = visible;
  }

  getVisibleGuideLinesPrevState(): boolean {
    return this.visibleGuideLinesPrevState;
  }

  override show() {
    super.show();
    if ((this.frame.width() !== this.frameCurrentWidth) && ((this.frame?.width() || 0) > FRAME_DEFAULT_CLOSE_WITH)) {
      this.frameCurrentWidth = this.frame.width();
    }
    this.frame.css({'margin-left': `${-1 * (this.frameCurrentWidth || 0)}px`});
    this.frame.css({'width': `${this.frameCurrentWidth}px`});
    if (this.visibleHighlights) {
      this.restoreAllGuideLinesState();
    }
  }

  override hide() {
    super.hide();
    this._clearAnnotationLinkLines(this);
    if ((this.frameCurrentWidth != null) && ((this.frame?.width() || 0) > FRAME_DEFAULT_CLOSE_WITH)) {
      this.frameCurrentWidth = this.frame.width();
    } else {
      this.frameCurrentWidth = this._isMilestoneExplorer() ? (FRAME_DEFAULT_WIDTH - 150) : FRAME_DEFAULT_WIDTH;
    }
    this.frame.css({'margin-left': `${-1 * FRAME_DEFAULT_CLOSE_WITH}px`});
    this.frame.css({'width': `${FRAME_DEFAULT_CLOSE_WITH}px`});
  }

  override destroy() {
    super.destroy();
    this._cleanup();
  }


  // Copied methods
  onSuggest() {
    this._onResize( true);
    const range = !this.selectedRanges ? undefined : this.selectedRanges[0];
    if (range != null) {
      const annotationText = this.leosSuggestionSelection.extractSuggestionTextFromSelection(range.cloneContents()) || undefined;

      this.createAnnotation({$suggestion: true, tags: ['suggestion'], text: annotationText});
    }
    document.getSelection()?.removeAllRanges();
  }

  onComment() {
    this._onResize( true);
    this.createAnnotation({$comment: true, tags: ['comment']});
    document.getSelection()?.removeAllRanges();
  }

  onHighlight() {
    this._onResize( true);
    this.setVisibleHighlights(true);
    this.createAnnotation({$highlight: true, tags: ['highlight']});
    document.getSelection()?.removeAllRanges();
  }

  onDefaultLoad() {
    this.showAnnotations([{tags: ['comment','suggestion','highlight']}]);
  }

  focusAnnotations(tags: string[], scope: LeosSidebar) {
    for (let anchor of scope.anchors) {
      if (anchor.highlights != null) {
        const toggle = tags.includes(anchor?.annotation?.$tag || '');
        $(anchor.highlights).toggleClass('annotator-hl-focused', toggle);
        scope.hoveredAnnotations[anchor?.annotation?.$tag || ''] = toggle;
      }
    }
    this.drawGuideLines(null, scope);
  }

  //NOTE If coordinates = null, cached values are used. Useful when redraw is needed but coordinates are sure not to have changed their values
  drawGuideLines(coordinates: any, scope: LeosSidebar) {
    if (coordinates !== null) {
      this.cachedCoordinates = coordinates;
    }
    // Check whether this is set as tests fail otherwise.
    if (this && (this.cachedCoordinates === null) || !scope.visibleGuideLines || !this.isOpen()) {
      return;
    }
    this._clearAnnotationLinkLines(this);
    if (this.leosCanvas) {
      const context: CanvasRenderingContext2D = this.leosCanvas.getContext('2d')!;
      const hypothesisFrame = this._getHypothesisFrame();
      const hypothesisIFrameOffset = (!hypothesisFrame.offsetParent ? hypothesisFrame : hypothesisFrame.offsetParent) as ExtendedElement;
      const endOfPageHorzCoords = this._getEndOfPageHorzCoordinates();
      const annLineDrawControl: any = {}; //control object used to limit to one guide line annotations composed by more than one lines of text

      //    get all highlight anchors
      const highlights = this._getHighlights();
      if (highlights && (highlights.length > 0)) {
        for (let highlight of highlights) {
          const annotation = $(highlight).data('annotation');
          const drawControlTag = `${annotation.$tag}-${annotation.id}`;
          if (!annLineDrawControl[drawControlTag]) {
            annLineDrawControl[drawControlTag] = true;
            this._configCanvasContextForAnnotation(context, highlight, scope);
            //        select the correct annotation that relates to the current highlight in the loop
            const annotationCoordinate = this._getAnnotationCoordinate(this.cachedCoordinates, annotation.id);
            if (annotationCoordinate) {
              const highlightContainer = this._getScrollParent(highlight);
              const highlightContainerRect = highlightContainer!.getBoundingClientRect()!;
              const docViewportTopLimit = highlightContainerRect.top;
              const docViewportBottomLimit = highlightContainerRect.bottom;
              const highlightRect = highlight.getBoundingClientRect();
              const iframeRect = hypothesisIFrameOffset.getBoundingClientRect();
              const anchorEndpointTop = annotationCoordinate.y + iframeRect.top;
              //          highlight is visible on the viewport
              const anchorInViewport = (highlightRect.top >= (docViewportTopLimit + 5)) &&
                                 (highlightRect.top <= docViewportBottomLimit) &&
                                 (highlightRect.right >= highlightContainerRect.left) &&
                                 (highlightRect.right <= highlightContainerRect.right);
              //          the annotation on the sidebar is on the viewport
              //          Note: following hardcoded values are hypothesis element dims that cannot be retrieved by document query due to crossDomain issues
              const anchorEndpointInViewport = (anchorEndpointTop >= (iframeRect.top + 40)) &&
                                         (anchorEndpointTop <= (iframeRect.bottom - 15));
              //TODO Check fail (anchorInViewport). Guide lines are not drawn
              if (anchorInViewport && anchorEndpointInViewport) {
                const fromLeft = highlightRect.right - 5;
                const fromTop = highlightRect.top;
                const toLeft = annotationCoordinate.x - 10;
                this._drawGuidLine(context, endOfPageHorzCoords, fromLeft, fromTop, toLeft, anchorEndpointTop);
              }
            }
          }
        }
        this.coordinatesSetS.next(true);
      }
    }
  }

  _getScrollParent(node: Element) {
    const regex = /(auto|scroll)/;
    const parents = (_node: Element, ps: Element[]): Element[] => {
      if (_node.parentNode === null) {
        return ps;
      }
      return parents(_node.parentNode as Element, [...ps, _node as Element]);
    };

    const style = (_node: Element, prop: string): string => getComputedStyle(_node, null).getPropertyValue(prop);
    const overflow = (_node: Element): string => style(_node, 'overflow') + style(_node, 'overflow-y') + style(_node, 'overflow-x');
    const scroll = (_node: Element): boolean => regex.test(overflow(_node));

    const scrollParent = (_node: Element): Element | undefined => {
      /* eslint-disable consistent-return */
      if (!(_node instanceof HTMLElement || _node instanceof SVGElement)) {
        return;
      }

      const ps = parents(_node.parentNode as Element, []);

      for (let i = 0; i < ps.length; i += 1) {
        if (scroll(ps[i])) {
          return ps[i];
        }
      }

      return document.scrollingElement || document.documentElement;
      /* eslint-enable consistent-return */
    };

    return scrollParent(node);
  }

  _refreshOrDrawGuideLines(selectFromDocument: boolean, scope: LeosSidebar): void {
    if (selectFromDocument) {
      this._refreshAnnotationLinkLines(scope.visibleGuideLines, scope, 500);
    } else {
      this.drawGuideLines(null, scope);
    }
  }
  _hasElements (list: any[] | null): boolean { return (list != null) && (list.length > 0); }

  _setFrameStyling () {
  //Set Sidebar specific styling to stick to the container
    this.frame.addClass('leos-sidebar');
    this._setFramePosition();
  }

  _setFramePosition () {
    const position = this.element![0].getBoundingClientRect();
    this.frame[0].style.top = position.top + 'px';
    this.frame[0].style.height = position.height + 'px';
    this.frame[0].style.left = this._isScrollbarVisible() ? (position.right - 18) + 'px' : position.right + 'px';
  }

  _isMilestoneExplorer (): boolean { return $('#milestonedocContainer').get(0) !== undefined; }

  _getFrameWidth (sidebarWidth: number, currentWidth: number): number {
    if (this._isMilestoneExplorer()) {
      return currentWidth;
    } else if (sidebarWidth > FRAME_DEFAULT_WIDTH) {
      return FRAME_DEFAULT_WIDTH;
    } else if (sidebarWidth < FRAME_DEFAULT_MIN_WIDTH) {
      return FRAME_DEFAULT_MIN_WIDTH;
    } else {
      return sidebarWidth;
    }
  }

  _onResize (redraw: boolean = false) {
    const collapsed = this.crossframe.annotator.frame.hasClass('annotator-collapsed');
    const frameWidth = this.element![0].ownerDocument!.defaultView!.outerWidth;
    const sidebarWidth = (frameWidth * Math.exp(((frameWidth + 70) * 4.6) / 1920)) / 100;
    this.frameCurrentWidth = this._getFrameWidth(sidebarWidth, this.frameCurrentWidth || 0);
    if (redraw && !collapsed) {
      this.frame.css({'margin-left': `${-1 * this.frameCurrentWidth}px`});
      this.frame.css({'width': `${this.frameCurrentWidth}px`});
    }
    this._setupCanvas(this.container![0]);
    this._refreshAnnotationLinkLines(this.visibleGuideLines, this, null);
  }

  private _isScrollbarVisible (): boolean {
    return this.container![0].scrollHeight > this.container![0].clientHeight;
  }

  // LEOS-4588 while on read only operation mode, annotation and new note buttons should be disabled (on milestones explorer and editing toc)
  private _setToolbarMode(): void {
    if (!this.plugins['Toolbar']) return;

    const toolbar = this.plugins['Toolbar'] as IToolbar;
    if (this.options.Document.operationMode === OPERATION_MODES.READ_ONLY || this.options.Document.operationMode === OPERATION_MODES.STORED_READ_ONLY) {
      toolbar.disableAnnotPopupBtn();
      toolbar.disableNewNoteBtn();
    } else {
      toolbar.enableAnnotPopupBtn();
      toolbar.enableNewNoteBtn();
    }
  }

  public refreshAnnotations(): void {
    this._refresh();
  }

  private _refresh(): void {
    let extendedEvent = event as ExtendedEvent;
    this.options.Document.operationMode = extendedEvent.operationMode; //LEOS-3796
    this._setToolbarMode();
    if (this.crossframe != null) {
      this.crossframe.call('LEOS_changeOperationMode', extendedEvent.operationMode);
    }
    // A list of annotations that need to be refreshed.
    const refreshAnnotations: Set<Annotation> = new Set();
    // Find all the anchors that have been invalidated by page state changes.
    // Would be better to loop on all existing annotations, but didn't find any other way to access annotations from sidebar
    if (this.anchors != null) {
      let anchor;
      for (anchor of this.anchors) {
      // The annotations for these anchors need to be refreshed.
      // Remove highlights in the DOM
        if (anchor.highlights != null) {
          for (let h of anchor.highlights) {
            if (h.parentNode != null) {
              $(h).replaceWith(h.childNodes as any);
            }
          }
          delete anchor.highlights;
          delete anchor.range;
          // Add annotation to be anchored again
          if (anchor.annotation) {
            refreshAnnotations.add(anchor.annotation);
          }
        }
      }

      const self = this;
      refreshAnnotations.forEach((annotation: Annotation) => {
        self.anchor(annotation, undefined, false);
      }
      , this);
    }

    this.setVisibleHighlights(true);
    if (this.crossframe != null) this.crossframe.call('reloadAnnotations');
  }

  private _setupCanvas(container: Element): void {
    const canvasWidth = container.ownerDocument?.defaultView?.innerWidth || 0;
    const canvasHeight = container.ownerDocument?.defaultView?.innerHeight || 0;
    if (!this.leosCanvas) {
      this.leosCanvas = Object.assign(container.ownerDocument.createElement('canvas'), {
        id: 'leosCanvas',
        className: 'leos-guideline-canvas',
      });
      $(this.leosCanvas).insertBefore(container);
    }
    this.leosCanvas.width = canvasWidth;
    this.leosCanvas.height = canvasHeight;

    const context = this.leosCanvas.getContext('2d')!;
    context.setLineDash([5,5]);
    context.lineWidth = 3;
    context.strokeStyle='#FDD7DF'; //default color
  }

  private _setupAnchorListeners (scope: LeosSidebar): void {
    const highlights = this._getHighlights() || [];
    for (let highlight of highlights) {
      if (!$(highlight).data('has-listeners-set') && (highlight.lastElementChild === null)) {
        highlight.addEventListener('mouseover', event => this._onHighlightFocus(event, true, scope));
        highlight.addEventListener('mouseout', event => this._onHighlightFocus(event, false, scope));
        highlight.addEventListener('click', event => this._onHighlightClick(event, true, scope));
        $(highlight).data('has-listeners-set', true);
      }
    }
  }

  private _setupOverlaidAnnotations(): void {
    const highlights = this._getHighlights();
    if (highlights) {
      for (let highlight of highlights) {
        const annotation: Annotation = $(highlight).data('annotation');
        const parentNode = highlight.parentNode as ExtendedParentNode;
        const previousSiblingAnnotation: Annotation = $(parentNode.previousElementSibling).data('annotation');
        const nextSiblingAnnotation: Annotation = $(parentNode.nextElementSibling).data('annotation');
        if (annotation && annotation.id && ((previousSiblingAnnotation && (annotation.id === previousSiblingAnnotation.id)) || (nextSiblingAnnotation && (annotation.id === nextSiblingAnnotation.id)))) {
          $(highlight).addClass('no-pointer-events');
        }
      }
    }
  }

  private _onHighlightFocus (event: Event, isMouseEnter: boolean, scope: LeosSidebar): void {
    const annotation = $(event.target!).data('annotation');
    if (annotation) {
      const anchor = this._getAnchorFromAnnotationTag(annotation.$tag, scope);
      if (anchor) {
        $(anchor.highlights!).toggleClass('annotator-hl-focused', isMouseEnter);
        scope.hoveredAnnotations[annotation.$tag] = isMouseEnter;
        this.drawGuideLines(null, scope);
      }
    }
  }

  private _onHighlightClick(event: Event, isMouseEnter: boolean, scope: LeosSidebar): void {
    if(!this.isOpen()){
      this.show();
    }
    this._onHighlightFocus(event, true, scope);
  }

  private _refreshAnnotationLinkLinesAndUpdateIcon (visibleGuideLines: boolean, scope: LeosSidebar, delayResp: any): void {
    this._refreshAnnotationLinkLines(visibleGuideLines, scope, delayResp);
    scope.publish('LEOS_setVisibleGuideLines', visibleGuideLines, false);
  }

  private _refreshAnnotationLinkLines (visibleGuideLines: boolean, scope: LeosSidebar, delayResp: any): void {
    if (visibleGuideLines && scope != null) {
      const hypothesisIFrame = this._getHypothesisFrame();
      if (!hypothesisIFrame) return;
      const hypothesisIFrameOffset = (!hypothesisIFrame.offsetParent ? hypothesisIFrame : hypothesisIFrame.offsetParent) as ExtendedElement;
      if (this.isAngularUI && !hypothesisIFrameOffset) {
        // Workaround: The first time this is called, the eui-column has been
        // collapsed but the sidebar has not been notified yet (toggle button
        // programmatic click triggered)
        return;
      }
      const iFrameOffsetLeft = hypothesisIFrameOffset.getBoundingClientRect().left;
      scope.crossframe.call('LEOS_syncCanvas', iFrameOffsetLeft, delayResp);
    } else {
      this._clearAnnotationLinkLines(this);
    }
  }

  //   select annotation from Anchor
  private _onWindowClick (event: JQueryEventObject, scope: LeosSidebar): void {
    this._toggleAnnotationSelectedState($(event.target).data('annotation'), true, scope);
  }

  //   select annotation from Annotation on sidebar
  private _crossFrameSelectAnnotation (annotation: Annotation, scope: LeosSidebar): void {
    this._toggleAnnotationSelectedState(annotation, false, scope);
  }

  //   select multiple annotations from Annotation on sidebar
  private _crossFrameSelectAnnotations(annotations: Annotation[], scope: LeosSidebar) {
    annotations.map((annotation) => this._changeAnnotationSelectedState(annotation, false, scope, true));
  }


  private _toggleAnnotationSelectedState (annotation: Annotation, selectFromDocument:  boolean, scope: LeosSidebar): void {
    const targetSelectionState = (annotation != undefined) && !scope.selectedAnnotations[annotation.$tag!];
    this._changeAnnotationSelectedState(annotation, selectFromDocument, scope, targetSelectionState);
  }

  /**
   *
   * @param annotation
   * @param selectFromDocument true : click was done on Anchor. false : click was done on Annotation on sidebar
   * @param scope
   * @param isSelectedTargetState
   * @returns
   */
  private _changeAnnotationSelectedState (annotation: Annotation | null, selectFromDocument: boolean, scope: LeosSidebar, isSelectedTargetState: boolean): void {
    //      if anchor not selected -> select anchor, drawGuideLines
    //      if anchor selected -> de-select anchor, drawGuideLines, clear filtering
    //      if click outside annotations -> de-select all anchors, drawGuideLines, clear filtering
    if (!annotation) {
      this._clearSelectedAnnotations(scope);
      this._sendClearSelectedAnnotationsToSidebar(scope, true, '');
      this._refreshOrDrawGuideLines(selectFromDocument, scope);
      return;
    }

    if (this._isPageNote(annotation) || this._isOrphan(annotation)) {
      this._clearSelectedAnnotations(scope);
      this._refreshOrDrawGuideLines(selectFromDocument, scope);
      return;
    }

    if (isSelectedTargetState && selectFromDocument) {  //If select on anchor - select only one anchor and filter by it
      this._sendClearSelectedAnnotationsToSidebar(scope, false, annotation.id!);
    }

    this._setAnnotationSelectedState(annotation, scope, isSelectedTargetState);

    if (!isSelectedTargetState && selectFromDocument) { //If anchor de-selected - deselect only one anchor
      this._sendClearSelectedAnnotationsToSidebar(scope, true, annotation.id!);
    }

    this._refreshOrDrawGuideLines(selectFromDocument, scope);
  }


  private _setAnnotationSelectedState (annotation: Annotation, scope: LeosSidebar, isSelectedTargetState: boolean): void {
    scope.selectedAnnotations[annotation.$tag!] = isSelectedTargetState;
    const anchor = this._getAnchorFromAnnotationTag(annotation.$tag!, scope);
    $(anchor!.highlights!).toggleClass('annotator-hl-selected', isSelectedTargetState);
    $(anchor!.highlights!).find('.annotator-hl').toggleClass('transparent-bg', isSelectedTargetState);
  }


  private _clearSelectedAnnotations (scope: LeosSidebar): void {
    scope.selectedAnnotations = {};
    const highlights = this._getHighlights();
    if ((highlights != undefined ) && (highlights.length > 0)) {
      highlights?.toggleClass('annotator-hl-selected', false);
      highlights?.find('.annotator-hl').toggleClass('transparent-bg', false);
    }
  }

  private _sendClearSelectedAnnotationsToSidebar (scope: LeosSidebar, useTimeout: boolean, annotationIdToSelectInstead: string) {
    if (useTimeout && scope.crossframe != null) {
      setTimeout(() => scope.crossframe.call('LEOS_clearSelectedAnnotations', annotationIdToSelectInstead), 500);
    } else if (scope.crossframe != null) {
      scope.crossframe.call('LEOS_clearSelectedAnnotations', annotationIdToSelectInstead);
    }
  }

  private _deselectAllAnnotations (scope: LeosSidebar): void {
    this._clearSelectedAnnotations(scope);
    this._refreshOrDrawGuideLines(false, scope);
  }

  private _getAnchorFromAnnotationTag (tag: string, scope: LeosSidebar): AnnotatorAnchor | null {
    for (let anchor of scope.anchors) {
      if (anchor.highlights != null && anchor.annotation?.$tag === tag) {
        return anchor;
      }
    }
    return null;
  }

  private _isOrphan (annotation: Annotation): boolean {
    return this._hasSelector(annotation) && (annotation.$orphan === true);
  }

  private _isPageNote (annotation: Annotation): boolean {
    return !this._hasSelector(annotation) && !this._isReply(annotation);
  }

  private _hasSelector (annotation: Annotation): boolean {
    return !!(annotation.target && (annotation.target.length > 0) && annotation.target[0].selector);
  }

  private _isReply (annotation: Annotation): boolean {
    return (annotation.references || []).length > 0;
  }

  private _configCanvasContextForAnnotation (canvasContext: CanvasRenderingContext2D, highlight: HTMLElement, scope: LeosSidebar): void {
    let lineColor: string;
    const annotation = $(highlight).data('annotation');
    //    Config line color
    if (lineColor = $(highlight).css( 'background-color' )) {
      canvasContext.strokeStyle = lineColor;
    }

    //    Config line type: dashed for regular, solid for focused
    if (!!scope.selectedAnnotations[annotation.$tag]) {
      canvasContext.setLineDash([]);
      canvasContext.strokeStyle = LEOS_config.LEOS_SELECTED_ANNOTATION_COLOR;
    } else if (!!scope.hoveredAnnotations[annotation.$tag]) {
      canvasContext.setLineDash([]);
      canvasContext.strokeStyle = LEOS_config.LEOS_HOVER_ANNOTATION_COLOR;
    } else { //Dashed line
      canvasContext.setLineDash([5,5]);
    }
  }

  private _drawGuidLine(canvasContext: CanvasRenderingContext2D, endOfPageHorzCoords: number, fromLeft: number, fromTop: number, toLeft: number, toTop: number): void {
    canvasContext.beginPath();
    canvasContext.moveTo(fromLeft, fromTop);
    canvasContext.lineTo(fromLeft, fromTop - 5);
    canvasContext.lineTo(endOfPageHorzCoords, fromTop - 5);
    canvasContext.lineTo(toLeft, toTop);
    canvasContext.stroke();
  }

  private _getAnnotationCoordinate(coordinates: any, id: string) {
    if (coordinates && id) {
      for (let annotationCoordinate of coordinates) {
        if (annotationCoordinate.id === id) {
          return annotationCoordinate;
        }
      }
    }
  }

  private _getHypothesisFrame (): HTMLElement {
    const hypothesisFrames = this._appId ? $(`#sidebar-frame-${this._appId}`) : $('#sidebar-frame-main');
    return (hypothesisFrames.length > 0) ? hypothesisFrames[0] : $("iframe[name='hyp_sidebar_frame']")[0];
  }

  _getEndOfPageHorzCoordinates() {
    let docChild: Element | null = this.container![0].querySelector('doc :first-child, bill :first-child, documentcollection :first-child, akomantoso :first-child');
    if (hasZeroRectSize(docChild || undefined)) {
      // Sometimes the preface element (first child) is ignored (rect size is zero).
      // In this case take the mainbody element.
      docChild = this.container![0].querySelector('doc mainbody, bill mainbody, documentcollection mainbody, coverpage');
    }
    const endOfDocCoordinates = !hasZeroRectSize(docChild) ? (docChild!.getBoundingClientRect().right - 20) : 0.0;
    const annotFrameLeftOffset = this._getHypothesisFrame().offsetParent ? this._getHypothesisFrame().offsetParent!.getBoundingClientRect().left - 20
        : this._getHypothesisFrame().getBoundingClientRect().left - 20;
    return Math.min(endOfDocCoordinates, annotFrameLeftOffset);
  }

  private _clearAnnotationLinkLines(scope?: LeosSidebar): void {
    if (scope?.leosCanvas) {
      const ctx = scope.leosCanvas.getContext('2d');
      ctx?.clearRect(0, 0, scope.leosCanvas.width, scope.leosCanvas.height);
    }
  }


  // Method called for CREATE, UPDATE and DELETE.
  // createdAnnotationId is only != null on CREATE
  private _updateIdForCreatedAnnotation (annotationTag: string, createdAnnotationId: string, scope: LeosSidebar): void {
    const highlights = this._getHighlights();
    if (createdAnnotationId && highlights && (highlights.length > 0)) {
      for (let highlight of highlights) {
        const highL = $(highlight), annotation = highL.data('annotation');
        if (annotation.$tag === annotationTag) {
          annotation.id = createdAnnotationId;
          let annId = 'hl-'+createdAnnotationId;
          if (!highL.hasClass(annId))
            highL.addClass(annId);
        }
      }
      this._refreshAnnotationLinkLines(scope.visibleGuideLines, scope, null);
    }
  }

  private _refreshAnnotationHighlights(annotations: Annotation[]): void {
    if (!annotations || annotations.length == 0) {
        return;
    }

    const hypothesisHighlights = this._getHighlights()?.get() || [];
    if (hypothesisHighlights.length == 0) {
        return;
    }

    annotations.forEach((annotation) => { toggleAnnotationHighlights(annotation, hypothesisHighlights) });
  }

  //    refresh lines for all three events

  private _onScroll(): void {
    this._scrollAnnotations();
    this._refreshAnnotationLinkLines(this.visibleGuideLines, this, null);
  }

  private _scrollAnnotations(): void {
    const highlights = this._getHighlights();
    var visibleAnchors = [];
    if (highlights && (highlights.length > 3)) {
      const hypothesisFrame = this._getHypothesisFrame();
      const hypothesisIFrameOffset = (!hypothesisFrame.offsetParent ? hypothesisFrame : hypothesisFrame.offsetParent) as ExtendedElement;
      const docViewportTopLimit = hypothesisIFrameOffset.offsetTop;
      const docViewportBottomLimit = hypothesisIFrameOffset.offsetHeight + hypothesisIFrameOffset.offsetTop;
      var annotCount = 0;
      var lastTag = "";
      for (let highlight of highlights) {
        const annotation = $(highlight).data('annotation');
        //check if Anchor is visible
        if (annotation.$tag != lastTag && this._isHighlightVisible(highlight, docViewportTopLimit, docViewportBottomLimit)) {
          //check if annotation is visible
          lastTag = annotation.$tag;
          if (this._isAnnotationVisibleInSidebar(annotation, docViewportTopLimit, docViewportBottomLimit)) {
            if (annotCount > 0) {
              return;
            }
            else {
              visibleAnchors.push(highlight);
              annotCount++;
            }
          }
          else {
            visibleAnchors.push(highlight);
          }
        }
      }

      if (visibleAnchors.length > 0) {
        const annotation = $(visibleAnchors[0]).data('annotation');
        this.crossframe.call("scrollToAnnotation", annotation.$tag)
      }
    }
  }

  private _isAnnotationVisibleInSidebar(annotation: Annotation, docViewportTopLimit: number, docViewportBottomLimit: number): boolean {
    const annotationCoordinate = this._getAnnotationCoordinate(this.cachedCoordinates, annotation.id || '');
    if (!annotationCoordinate) {
      return false;
    }
    const anchorEndpointTop: number = annotationCoordinate.y + docViewportTopLimit;
    const anchorEndpointInViewport: boolean = (anchorEndpointTop >= (docViewportTopLimit + 40)) &&
                                         (anchorEndpointTop <= (docViewportBottomLimit - 40));
    return anchorEndpointInViewport;
  }

  private _isHighlightVisible(highlight: Element, docViewportTopLimit: number, docViewportBottomLimit: number): boolean {
    const highlightRect: DOMRect = highlight.getBoundingClientRect();
    //highlight is visible on the viewport
    const anchorInViewport = (highlightRect.top >= (docViewportTopLimit + 5)) &&
    (highlightRect.top <= docViewportBottomLimit);

    return anchorInViewport;
  }

  private _filterHighlights(scope: any): void {
    if (scope.options.Document.filterParentElementId) {
      const highlights = $(`#${scope.options.Document.filterParentElementId}`).find('hypothesis-highlight');
      if (highlights && (highlights.length > 0)) {
        for (let highlight of highlights) {
          let h: any = highlight;
          if (h.parentNode != null) {
            $(h).replaceWith(h.childNodes);
          }
        }
      }
    }
  }

  private _getHighlights() {
    return this.container?.find('hypothesis-highlight');
  }

  private _cleanup(): void {
    this.leosCanvas = null;
    if (this._appId) {
        return;
    }
    // TODO : Temporary solution to cleanup references to annotation scripts
    const quote = (regex: string) => regex.replace(/([()[{*+.$^\\|?\/])/g, '\\$1');

    const annotateScripts = document.querySelectorAll(`script[src*=${quote(this.contextRoot)}]`);
    annotateScripts.forEach((annotateScript: Element) => annotateScript.parentNode?.removeChild(annotateScript));

    const annotateLinks = document.querySelectorAll(`link[href*=${quote(this.contextRoot)}]`);
    annotateLinks.forEach((annotateLink: Element) => annotateLink.parentNode?.removeChild(annotateLink));

    const configScripts = document.querySelectorAll('script.js-hypothesis-config');
    configScripts.forEach((configScript: Element) => configScript.parentNode?.removeChild(configScript));
  }

};

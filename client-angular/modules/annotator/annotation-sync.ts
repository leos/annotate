'use strict';

import { IBridgeService } from "../../shared/bridge";
import { Annotation } from '../../shared/models/annotation.model';

type OnFunction = (event: string, callback: Function) => void;
type EmitFunction = (event: string, ...params: any[]) => void;

export type AnnotationSyncOptions = {
  on?: OnFunction;
  emit?: EmitFunction
}

/**
 * Body content of a RPC message
 */
type RPCMessageBody = {
  tag: string,
  msg: Annotation
}

/** 
 * AnnotationSync listens for messages from the sidebar app indicating that
 * annotations have been added or removed and relays them to Guest.
 *
 * It also listens for events from Guest when new annotations are created or
 * annotations successfully anchor and relays these to the sidebar app.
 */
export interface IAnnotationSync {
  cache: Object;
  sync(annotations: Annotation[]): IAnnotationSync;
}

export default class AnnotationSync implements IAnnotationSync {
  private _cache: Map<String, Annotation>;
  private on: OnFunction;
  private emit: EmitFunction;
  private eventListeners: any;
  private channelListeners: any;

  constructor(private bridge: IBridgeService, options: AnnotationSyncOptions){
    if (!options.on) {
      throw new Error('options.on unspecified for AnnotationSync.');
    }
  
    if (!options.emit) {
      throw new Error('options.emit unspecified for AnnotationSync.');
    }
    
    this._cache = new Map();
    this.on = options.on;
    this.emit = options.emit;
    
    const self = this;
    const deleteAnnotation = function(body: RPCMessageBody, cb: Function): void {
      const annotation: Annotation = self.parse(body);
      self._cache.delete(annotation?.$tag || '');
      self.emit('annotationDeleted', annotation);
      cb(null, self.format(annotation));
    }

    const loadAnnotations = function(bodies: RPCMessageBody[], cb: Function): any {
      var i: number;
      const annotations: Annotation[] = [];
      for (i = 0; i < bodies.length; i++) {
        annotations.push(self.parse(bodies[i]));
      }
      self.emit('annotationsLoaded', annotations);
      return cb(null, annotations);
    }

    this.channelListeners = {
      'deleteAnnotation': deleteAnnotation,
      'loadAnnotations': loadAnnotations,      
    }

    const beforeAnnotationCreated = (annotation: Annotation): any => {
      if (annotation.$tag) {
        return undefined;
      }
      return self.mkCallRemotelyAndParseResults('beforeCreateAnnotation')(annotation);
    } 
    // Handlers for events coming from this frame, to send them across the channel
    this.eventListeners = {
      'beforeAnnotationCreated': beforeAnnotationCreated.bind(this)
    }

    // Listen locally for interesting events
    Object.keys(this.eventListeners).forEach(function(eventName) {
      var listener = self.eventListeners[eventName];
      self.on(eventName, function(annotation: Annotation) {
        listener.apply(self, [annotation]);
      });
    });

    // Register remotely invokable methods
    Object.keys(this.channelListeners).forEach((eventName: string) => {
      self.bridge.on(eventName, (data: any, callbackFunction: Function) => {
        var listener = self.channelListeners[eventName];
        listener.apply(self, [data, callbackFunction]);
      });
    });
  }
  
  get cache(): Object {
    if (this._cache.size == 0) return {};
    const cacheCopy: any = {}
    this._cache.forEach((value: Annotation, key: String) => {
      cacheCopy[key.toString()] = Object.assign({}, value);
    });
    return cacheCopy;
  }

  sync(annotations: Annotation[]): IAnnotationSync {
    var i: number;
    const formattedAnnotations: RPCMessageBody[] = [];
    for (i = 0; i < annotations.length; i++) {
      formattedAnnotations.push(this.format(annotations[i]));
    }

    const bridgeSync = (err: any, annotations: Annotation[]): Annotation[] => {
      let i: number;
      let parsedAnnotations: Annotation[] = [];
      annotations = annotations || [];

      for (i = 0; i < annotations.length; i++) {
        parsedAnnotations.push(this.parse(formattedAnnotations[i]));
      }
      return parsedAnnotations;
    }
    this.bridge.call('sync', formattedAnnotations, bridgeSync.bind(this));
    return this;
  }

  private mkCallRemotelyAndParseResults(method: string, callBack?: Function): (annotation: Annotation) => void {
    const self = this;
    const result = (annotation: Annotation) => {
      /** Wrap the callback function to first parse returned items */
      const wrappedCallback = function(failure: any, results: any[]) {
        if (failure === null) {
          self.parseResults(results);
        }
        if (typeof callBack === 'function') {
          callBack(failure, results);
        }
      };
      // Call the remote method
      self.bridge.call(method, self.format(annotation), wrappedCallback);
    }
    return result;
  }

  /** 
   * Parse returned message bodies to update cache with any changes made remotely 
   */
  private parseResults(results: any[]) {
    if (results.length == 0) return;
    for (const result of results) {
      let bodies: RPCMessageBody[] = [].concat(result);
      for (const body of bodies) {
        if (body !== null) {
          this.parse(body);
        }
      }
    }
  }

  /** 
   *  Assign a non-enumerable tag to objects which cross the bridge.
   * This tag is used to identify the objects between message.
   */ 
  private tag(ann: Annotation, tag?: string): Annotation {
    if (ann.$tag) return ann;

    const tagValue: string = !tag ? (window.btoa(Math.random().toString()).toString()) : `${tag}`;
    Object.defineProperty(ann, '$tag', {
      value: tagValue,
    });
    this._cache.set(tagValue, ann);
    return ann;
  }

  /** 
   * Parse a message body from a RPC call with the provided parser. 
   */
  private parse(body: RPCMessageBody): Annotation {
    const annotation: Annotation = this._cache.has(body.tag) ? this._cache.get(body.tag) as Annotation : {};
    var merged: Annotation = Object.assign(annotation, body.msg);
    return this.tag(merged, body.tag);
  }

  /** 
   * Format an annotation into an RPC message body with the provided formatter.
   */ 
  private format(ann: Annotation): RPCMessageBody {
    this.tag(ann);
    return {
      tag: ann.$tag || '',
      msg: ann,
    };
  };  
}
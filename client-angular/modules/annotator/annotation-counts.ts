'use strict';

import * as events from '../../shared/bridge-events';
import { ICrossFrame } from './plugin/cross-frame';

const ANNOTATION_COUNT_ATTR = 'data-hypothesis-annotation-count';

/**
 * Update the elements in the container element with the count data attribute
 * with the new annotation count.
 *
 * @param {Element} rootEl - The DOM element which contains the elements that
 * display annotation count.
 */

export function annotationCounts(rootEl: Element, crossframe: ICrossFrame) {
  const updateAnnotationCountElems = (newCount: number | string) => {
    var elems = rootEl.querySelectorAll('['+ANNOTATION_COUNT_ATTR+']');
    Array.from(elems).forEach(function(elem: Element) {
      elem.textContent = `${newCount}`;
    });
  }
  crossframe.on(events.PUBLIC_ANNOTATION_COUNT_CHANGED, updateAnnotationCountElems);
}

'use strict';

var SIDEBAR_TRIGGER_BTN_ATTR = 'data-hypothesis-trigger';

/**
 * Show the sidebar when user clicks on an element with the
 * trigger data attribute.
 *
 * @param {Element | Document} rootEl - The DOM element which contains the trigger elements.
 * @param {Function} showFn - Function which shows the sidebar.
 */

export function trigger(rootEl: Element | Document, showFn: VoidFunction): void {
  const handleCommand = (event: Event) => {
    showFn();
    event.stopPropagation();
  }

  const triggerElems = rootEl.querySelectorAll('['+SIDEBAR_TRIGGER_BTN_ATTR+']');
  Array.from(triggerElems).forEach(function(triggerElem: Element) {
    triggerElem.addEventListener('click', handleCommand);
  });
}

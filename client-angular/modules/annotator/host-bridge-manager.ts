/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS201: Simplify complex destructure assignments
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */

import { Annotation } from '../../shared/models/annotation.model';
import { IDelegator } from './delegator';
import { ICrossFrame } from './plugin/cross-frame';

const REQUEST_TYPES = ['SecurityToken',
    'UserPermissions',
    'MergeSuggestion',
    'MergeSuggestions',
    'DocumentMetadata',
    'SearchMetadata',
    'SearchUsers',
    'ListCollaborators'];

export interface IHostBridgeManager {
  readonly callbackManager: Object;
  readonly hostBridge: Object;
  readonly crossframe: ICrossFrame;
}

export default class HostBridgeManager implements IHostBridgeManager {
    readonly callbackManager: Object;
    readonly hostBridge: any;
    readonly crossframe: ICrossFrame;

    constructor(hostBridge: any, crossframe: ICrossFrame) {
      this.callbackManager = {};
      this.crossframe = crossframe;
      this.hostBridge = hostBridge;

      if ((this.crossframe != null) && (this.hostBridge != null)) {
        for (let requestType of REQUEST_TYPES) {
          this._addHostBridgeHandlers(requestType);
        }
        this.hostBridge['stateChangeHandler'] = (state: any) => this.crossframe.call('stateChangeHandler', state);
        this.hostBridge['requestFilteredAnnotations'] = (callback: Function) => this.crossframe.call('LEOS_requestFilteredAnnotations', callback);
        this.crossframe.on('LEOS_responseFilteredAnnotations', (annotations: Annotation[]) => this.hostBridge['responseFilteredAnnotations'](annotations));

        // Add specific handlers about fork&Merge feature
        this.crossframe.on('requestStoredDocumentAnnotations', (uri: string, callback: Function) => {
          console.log(`Request StoredDocumentAnnotations to be sent to host`);
          if (this.hostBridge['requestStoredDocumentAnnotations'] && (typeof this.hostBridge['requestStoredDocumentAnnotations'] === 'function')) {
              // Add handler on host bridge to let leos application responds
              this.hostBridge['responseStoredDocumentAnnotations'] = function(annotations: any) {
                  console.log(`Received message from host for request StoredDocumentAnnotations`);
                  callback(null, annotations);
              };
              this.hostBridge['requestStoredDocumentAnnotations'](uri);
          } else {
              callback(null, '');
          }
        });
        this.crossframe.on('requestCountSentFeedbacks', (ids: string[]) => {
          console.log(`Request CountSentFeedbacks to be sent to host`);
          if (this.hostBridge['requestCountSentFeedbacks'] && (typeof this.hostBridge['requestCountSentFeedbacks'] === 'function')) {
              this.hostBridge['requestCountSentFeedbacks'](ids);
          }
        });
      }
    }

    private _addHostBridgeHandlers(requestType: string): IDelegator {
        const self = this;
        // Add listeners on cross frame (communication between iframe and host) for each request type
        return this.crossframe.on(`request${requestType}`, (...args1: any[]) => {
          const adjustedLength = Math.max(args1.length, 1),
            args = args1.slice(0, adjustedLength - 1),
            callback = args1[adjustedLength - 1];
          console.log(`Request ${requestType} to be sent to host`);
          if (self.hostBridge[`request${requestType}`] && (typeof self.hostBridge[`request${requestType}`] === 'function')) {
            // Add handler on host bridge to let leos application responds
            self.hostBridge[`response${requestType}`] = function(data: any) {
              console.log(`Received message from host for request ${requestType}`);
              return callback(null, data);
            };
            return self.hostBridge[`request${requestType}`](...(args || []));
          } else {
            return callback('No available request handler on bridge');
          }
        });
    };
};


// Do LEOS specific actions here
import $ from '../imports/jquery';
import Document, { IDocumentMeta } from './document';
import * as leosAnchoring from '../anchoring/leos';

type LeosDocumentAnnotator = {
  anchoring?: any;
}

export interface ILeosDocument extends IDocumentMeta {
  annotator: LeosDocumentAnnotator;
  getLeosDocumentMetadata(): Promise<any>;
}

export default class LeosDocument extends Document implements ILeosDocument {
  private containerSelector: string;
  private documentIdSelector: string;
  private legFileName: string;
  public annotator: LeosDocumentAnnotator;

  constructor(element: Element, config: any) {
    super($(`${config.annotationContainer}`)[0], config);
    // Make a definitive check for commentable area Or even better -> read from config
    this.containerSelector = `${config.annotationContainer}`;
    this.documentIdSelector = `${config.annotationContainer} ${config.leosDocumentRootNode}`;
    this.legFileName = config.legFileName;
    this.annotator = {};
  }

  getLeosDocumentMetadata(): Promise<any> {
    const element: any = this.getElement();
    return new Promise((resolve, reject) => {
      const promiseTimeout = setTimeout(() => reject('timeout')
        , 500);

      if (element['hostBridge'] != null) {
        element['hostBridge'].responseDocumentMetadata = function(metadata: string) {
          console.log('Received message from host for request DocumentMetadata');
          const leosMetadata = JSON.parse(metadata);
          return resolve(leosMetadata);
        };
        if (element['hostBridge'].requestDocumentMetadata != null) {
          return element['hostBridge'].requestDocumentMetadata();
        }
      }
    });
  }
    
  override pluginInit() {
    super.pluginInit();
    this.annotator.anchoring = leosAnchoring;
  }

  /** LEOS-2789 the reference element 'root' is now defined in the plugin document */
  override getElement(): Element {
    //The root element tag is NOT taken in account while building the xpath by HTML and RANGE classes,
    //In XPath, we will get sth like '//akomantoso[1]/...'.
    const documentContainer = $(this.containerSelector)[0];
    return documentContainer;
  }

  /** 
   * LEOS document will provide its own static id
   */
  protected override _getDocumentHref(): string | Function {
    const leosDocument = $(this.documentIdSelector)[0];
    if (leosDocument.id) {
      if (this.containerSelector === '#contributionViewMainContainer') {
        return `uri://LEOS/` + this.legFileName + `/${leosDocument.id}`;
      } else {
        return `uri://LEOS/${leosDocument.id}`;
      }

    }
    return super._getDocumentHref();
  }
}

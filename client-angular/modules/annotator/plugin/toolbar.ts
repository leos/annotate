/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import Plugin, { IPlugin } from '../plugin';
import $ from '../imports/jquery';
import * as LEOS_config from '../../../shared/config';
import { ILeosSidebar } from '../leos-sidebar';


interface ButtonEvents {
  click: (event: JQuery.Event) => void;
}

interface ButtonModel {
  title: string;
  name: string;
  class: string;
  on: ButtonEvents;
}

function makeButton(item: ButtonModel): HTMLElement {
  const anchor: JQuery<HTMLElement> = $('<button></button>')
    .attr('href', '')
    .attr('title', item.title)
    .attr('name', item.name)
    .on('click', item.on.click)
    .addClass('annotator-frame-button')
    .addClass(item.class);
  const button: JQuery<HTMLElement> = $('<li></li>').append(anchor);
  return button[0];
};

const HIDE_CLASS = 'annotator-hide';

export interface IToolbar extends IPlugin {
  annotator: ILeosSidebar;
  toolbar: JQuery<HTMLElement>;
  onSetVisibleHighlights(state?: boolean): void;
  onSetVisibleGuideLines(state?: boolean): void;
  onEnableAnnotationPopup(state?: boolean): void;
  disableMinimizeBtn(): void;
  disableHighlightsBtn(): void;
  disableNewNoteBtn(): void;
  enableNewNoteBtn(): void;
  disableCloseBtn(): void;
  disableToggleBtn(): void;
  enableToggleBtn(): void;
  disableGuideLinesBtn(): void;
  disableAnnotPopupBtn(): void;
  enableAnnotPopupBtn(): void;
  getWidth(): number;
  hideCloseBtn(): void;
  showCloseBtn(): void;
  showCollapseSidebarBtn(): void;
  showExpandSidebarBtn(): void;
}

export default class Toolbar extends Plugin implements IToolbar {
    private buttons!: JQuery<HTMLElement>;
    private html: string;

    /**
     * Is injected while runtime from the Guest instance.
    */
    public annotator!: ILeosSidebar;
    public toolbar!: JQuery<HTMLElement>;

    constructor(element: Element, options: any) {
      super(element, options);
      this.html = '<div class="annotator-toolbar"></div>';
    }

    override pluginInit() {
      super.pluginInit();
      localStorage.setItem('shouldAnnotationTabOpen', 'true');

      this.toolbar = $(this.html);
      this.annotator.toolbar = this.toolbar;
      if (this.options.container != null) {
        $(this.options.container).append(this.toolbar);
      } else {
        $(this.element!).append(this.toolbar);
      }

      const items: ButtonModel[] = [{
        'title': 'Close Sidebar',
        'class': 'annotator-frame-button--sidebar_close h-icon-close',
        'name': 'sidebar-close',
        'on': {
          'click': ((event: any) => {
            event.preventDefault();
            event.stopPropagation();
            this.annotator.hide();
            this.toolbar.find('[name=sidebar-close]').hide();
          }).bind(this),
        },
      }
      , {
        'title': 'Toggle or Resize Sidebar',
        'class': 'h-icon-chevron-left',
        'name': 'sidebar-toggle',
        'on': {
          'click': ((event: any) => {
            event.preventDefault();
            event.stopPropagation();
            const collapsed = this.annotator.frame.hasClass('annotator-collapsed');
            if (collapsed) {
              this.annotator.show();
              localStorage.setItem('shouldAnnotationTabOpen', 'true');
            } else {
              this.annotator.hide();
              localStorage.setItem('shouldAnnotationTabOpen', 'false');
              const state = false;
              const storePrevState = true;
              this.annotator.setAllVisibleGuideLines(state, storePrevState);
            }
          }).bind(this),
        },
      }
      , {
        'title': this.annotator.getAnnotationPopupStatus() ? "Disable Annotations' Popup" : "Enable Annotations' Popup",
        'class': this.annotator.getAnnotationPopupStatus() ? 'disable-anot-icon' : 'enable-anot-icon',
        'name': 'anot-state',
        'on': {
          'click': ((event: any) => {
            this.annotator.changeAnnotationPopupStatus();
            this.onEnableAnnotationPopup(this.annotator.getAnnotationPopupStatus());
            event.preventDefault();
            event.stopPropagation();
          }).bind(this),
        },
      } , {
        'title': "Refresh Annotations",
        'class': 'h-icon-toolbar-refresh',
        'name': 'refresh-annotations',
        'on': {
            'click': ((event: any) => {
                event.preventDefault();
                event.stopPropagation();
                this.annotator.refreshAnnotations();
            }).bind(this),
        },
      }
      , {
        'title': 'Hide Highlights',
        'class': 'h-icon-toolbar-eye',
        'name': 'highlight-visibility',
        'on': {
          'click': ((event: any) => {
            event.preventDefault();
            event.stopPropagation();

            const state = !this.annotator.isHighlightsVisible();
            this.annotator.setAllVisibleHighlights(state);
            if (state && ((this.annotator?.frame.width() || 0) >= LEOS_config.FRAME_DEFAULT_MIN_WIDTH)) {
              this.annotator.restoreAllGuideLinesState();
            } else {
              this.annotator.setAllVisibleGuideLines(false, true);
            }
            this.onEnableAnnotationPopup(this.annotator.getAnnotationPopupStatus());
            this.onSetVisibleHighlights(state);
            this.onSetVisibleGuideLines(state && this.annotator.getVisibleGuideLinesPrevState());
          }).bind(this),
        },
      }
      , {
        //      LEOS change 3630
        'title': 'Hide Line Guides',
        'class': 'h-icon-guidelines-enabled',
        'name': 'lineguide-visibility',
        'on': {
          'click': ((event: any) => {
            event.preventDefault();
            event.stopPropagation();
            
            if (!this.annotator.isHighlightsVisible()) return;
            let state: boolean = !this.annotator.isGuideLinesVisible();
            if ((this.annotator?.frame.width() || 0) < LEOS_config.FRAME_DEFAULT_MIN_WIDTH) {
              state = false;
            }
            this.annotator.setAllVisibleGuideLines(state, false);
            this.onSetVisibleGuideLines(state);
          }).bind(this),
        },
      }
      , {
        // LEOS change 3632
        'title': 'New Document Note',
        'class': 'h-icon-toolbar-document',
        'name': 'insert-note',
        'on': {
          'click': ((event: any) => {
            event.preventDefault();
            event.stopPropagation();
            this.annotator.newDocumentNoteClick();
          }).bind(this),
        },
      },
      ];

      this.annotator.setIsHighlightsVisible(true);
      this.annotator.setIsGuideLinesVisible(false);
      this.annotator.setAllVisibleGuideLines(false);

      this.buttons = $(items.map((item) => makeButton(item)));

      const list: JQuery<HTMLElement> = $('<ul></ul>');
      this.buttons.appendTo(list);
      this.toolbar.append(list);

      // Remove focus from the anchors when clicked, this removes the focus
      // styles intended only for keyboard navigation. IE/FF apply the focus
      // psuedo-class to a clicked element.
      this.toolbar.on('mouseup', 'a', (event: any) => $(event.target).trigger('blur'));
    }

    onSetVisibleHighlights(state?: boolean) {
      if (state) {
        $('[name=highlight-visibility]')
          .removeClass('h-icon-toolbar-eye-disabled')
          .addClass('h-icon-toolbar-eye')
          .prop('title', 'Hide Highlights');
      } else {
        $('[name=highlight-visibility]')
          .removeClass('h-icon-toolbar-eye')
          .addClass('h-icon-toolbar-eye-disabled')
          .prop('title', 'Show Highlights');
      }
    }

    onSetVisibleGuideLines(state?: boolean) {
      if (state) {
        $('[name=lineguide-visibility]')
          .removeClass('h-icon-guidelines-disabled')
          .addClass('h-icon-guidelines-enabled')
          .prop('title', 'Hide Guide Lines');
      } else {
        $('[name=lineguide-visibility]')
          .removeClass('h-icon-guidelines-enabled')
          .addClass('h-icon-guidelines-disabled')
          .prop('title', 'Show Guide Lines');
      }
    }

    //     LEOS change 4453
    onEnableAnnotationPopup(state?: boolean) {
      if (state) {
        $('[name=anot-state]')
          .removeClass('enable-anot-icon')
          .addClass('disable-anot-icon')
          .prop('title', 'Disable Annotations\' Popup');
      } else {
        $('[name=anot-state]')
          .removeClass('disable-anot-icon')
          .addClass('enable-anot-icon')
          .prop('title', 'Enable Annotations\' Popup');
      }
    }
    //---------------------

    disableMinimizeBtn() {
      $('[name=sidebar-toggle]').remove();
    }

    disableHighlightsBtn() {
      $('[name=highlight-visibility]').remove();
    }

    disableNewNoteBtn() {
      $('[name=insert-note]').hide();
    }

    enableNewNoteBtn() {
      $('[name=insert-note]').show();
    }

    disableCloseBtn() {
      $('[name=sidebar-close]').remove();
    }

    enableToggleBtn() {
      $('[name=sidebar-toggle]').show();
    }

    disableToggleBtn() {
      $('[name=sidebar-toggle]').hide();
    }

    disableGuideLinesBtn() {
      $('[name=lineguide-visibility]').remove();
    }

    //     LEOS change 4453
    disableAnnotPopupBtn() {
      $('[name=anot-state]').hide();
    }

    enableAnnotPopupBtn() {
      $('[name=anot-state]').show();
    }
    //---------------------

    getWidth(): number {
      return parseInt(window.getComputedStyle(this.toolbar[0]).width);
    }

    hideCloseBtn() {
      $('[name=sidebar-close]').hide();
    }

    showCloseBtn() {
      $('[name=sidebar-close]').show();
    }

    showCollapseSidebarBtn() {
      $('[name=sidebar-toggle]')
        .removeClass('h-icon-chevron-left')
        .addClass('h-icon-chevron-right');
    }

    showExpandSidebarBtn() {
      $('[name=sidebar-toggle]')
        .removeClass('h-icon-chevron-right')
        .addClass('h-icon-chevron-left');
    }
};
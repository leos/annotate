import Plugin from '../plugin';
import AnnotationSync from '../annotation-sync';
import { IAnnotationSync, AnnotationSyncOptions } from '../annotation-sync';
import {DefaultBridgeService as Bridge,  IBridgeService } from '../../../shared/bridge';
import { Discovery, DisocveryOptions, IDiscovery } from '../../../shared/discovery';
import * as FrameUtil from '../util/frame-util';
import { IFrameObserver, FrameObserver } from '../frame-observer';
import { IDelegator, SubscribeCallback } from '../delegator';
import { Annotation } from '../../../shared/models/annotation.model';
import { IFrameRPC } from '../../../shared/frame-rpc';
import { IPlugin } from '../plugin';
import { IHost } from '../host';

// Extracts individual keys from an object and returns a new one.
function extract(obj: any, ...keys: string[]): Object {
  const ret: any = {};
  for (let key of keys) { if (obj.hasOwnProperty(key)) { ret[key] = obj[key]; } }
  return ret;
};

export interface CrossFrameOptions {
  config: any;
  on: (event: string, handler: SubscribeCallback) => IDelegator;
  emit: (event: string, ...args: any[]) => IDelegator;
}

export interface ICrossFrame extends IPlugin {
  annotator: IHost; 
  call(message: string, ...args: any[]): Promise<any>;
  onConnect(fn: Function): IBridgeService;
  sync(annotations: Annotation[]): IAnnotationSync;
}

/** 
 * Class for establishing a messaging connection to the parent sidebar as well
 * as keeping the annotation state in sync with the sidebar application, this
 * frame acts as the bridge client, the sidebar is the server. This plugin
 * can also be used to send messages through to the sidebar using the
 * call method. This plugin also enables the discovery and management of
 * not yet known frames in a multiple frame scenario.
 */
export default class CrossFrame extends Plugin implements ICrossFrame {
  // Set by Guest
  public annotator!: IHost;
  private bridge: IBridgeService;
  private annotationSync: IAnnotationSync;
  private frameObserver: IFrameObserver;
  private frameIdentifiers: Map<HTMLIFrameElement, string>;
  private discovery: IDiscovery;
  private config: any;

  constructor(elem: Element, options: CrossFrameOptions) {
    super(elem, options);
  
    this.config = Object.assign({}, options.config);

    let discoveryOpts: DisocveryOptions = extract(options, 'server');
    discoveryOpts.appId = options.config.sidebarAppId;
    this.discovery = new Discovery(window, discoveryOpts);

    this.bridge = new Bridge();

    const annotSyncOpts: AnnotationSyncOptions = extract(options, 'on', 'emit');
    this.annotationSync = new AnnotationSync(this.bridge, annotSyncOpts);

    this.frameObserver = new FrameObserver(elem);
    this.frameIdentifiers = new Map();

    const _onHandler = (event: string, fn: Function): IDelegator => {
      this.bridge.on(event, fn)
      return this;
    }
    this.on = _onHandler.bind(this);
  }

  override pluginInit() {
    const self = this;
    const config = this.config;

    const _injectToFrame = function(frame: HTMLIFrameElement) {
      if (!FrameUtil.hasHypothesis(frame)) {
        // Take the embed script location from the config
        // until an alternative solution comes around.
        const {
          clientUrl,
        } = config;

        FrameUtil.isLoaded(frame, function() {
          const subFrameIdentifier: string = self.discovery.generateToken();
          self.frameIdentifiers.set(frame, subFrameIdentifier);
          const injectedConfig = Object.assign({}, config, {subFrameIdentifier});

          FrameUtil.injectHypothesis(frame, clientUrl, injectedConfig);
        });
      }
    };

    const _iframeUnloaded = function(frame: HTMLIFrameElement) {
      self.bridge.call('destroyFrame', self.frameIdentifiers.get(frame));
      self.frameIdentifiers.delete(frame);
    };

    this.discovery.startDiscovery(
      (source: Window, origin: string, token: string): IFrameRPC => self.bridge.createChannel(source, origin, token)
    );
    this.frameObserver.observe(_injectToFrame, _iframeUnloaded);
  }

  onConnect(fn: Function): IBridgeService {
    return this.bridge.onConnect(fn)
  }

  sync(annotations: Annotation[]): IAnnotationSync {
    return this.annotationSync.sync(annotations)
  }

  override destroy() {
      super.destroy();
      this.bridge.destroy();
      this.discovery.stopDiscovery();
      this.frameObserver.disconnect();
  };

  call(message: string, ...args: any[]): Promise<any> { 
    return this.bridge.call(message, ...args) 
  };
}

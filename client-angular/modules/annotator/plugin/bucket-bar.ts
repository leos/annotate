/*
 * DS201: Simplify complex destructure assignments
 * DS202: Simplify dynamic range loops
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import * as Raf from 'raf';
import $ from '../imports/jquery';
import Plugin, { IPlugin } from '../plugin';
import scrollIntoView from '../imports/scroll-into-view';
import { highlighterFacade } from '../highlighter';
import { IHost } from '../host';
import { Annotation } from '../../../shared/models/annotation.model';
import { AnnotatorAnchor } from '../models/annotator';

const raf = (Raf as any).default;
const highlighter: any = highlighterFacade;

const BUCKET_SIZE = 16;                              // Regular bucket size
const BUCKET_NAV_SIZE = BUCKET_SIZE + 6;             // Bucket plus arrow (up/down)
const BUCKET_TOP_THRESHOLD = 115 + BUCKET_NAV_SIZE;  // Toolbar
const BUCKET_BAR_TEMPLATE = `<div class="annotator-bucket-bar"></div>`;

// Scroll to the next closest anchor off screen in the given direction.
function scrollToClosest (anchors: any[], direction?: string | null) {
  const dir = direction === 'up' ? +1 : -1;
  var {next} = anchors.reduce(function(acc, anchor) {
    let start;
    if (!(anchor.highlights != null ? anchor.highlights.length : undefined)) {
      return acc;
    }

    ({start, next} = acc);
    const rect = highlighter['getBoundingClientRect'](anchor.highlights);

    // Ignore if it's not in the right direction.
    if ((dir === 1) && (rect.top >= BUCKET_TOP_THRESHOLD)) {
      return acc;
    } else if ((dir === -1) && (rect.top <= (window.innerHeight - BUCKET_NAV_SIZE))) {
      return acc;
    }

    // Select the closest to carry forward
    if ((next == null)) {
      return {
        start: rect.top,
        next: anchor,
      };
    } else if ((start * dir) < (rect.top * dir)) {
      return {
        start: rect.top,
        next: anchor,
      };
    } else {
      return acc;
    }
  }
  , {});

  scrollIntoView(next.highlights[0]);
};


function _collate(a: any[], b: any[]): number {
  for (let i = 0, end = a.length-1, asc = 0 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
    if (a[i] < b[i]) {
      return -1;
    }
    if (a[i] > b[i]) {
      return 1;
    }
  }
  return 0;
}

// String is only used in tests
type Bucket = (AnnotatorAnchor | string)[];

export interface IBucketBar extends IPlugin {
    annotator: IHost;
    readonly html: string;
    /** Update sometime soon */
    update(): number | null;
}

export default class BucketBar extends Plugin implements IBucketBar {
    // Is set by Guest class
    public annotator!: IHost;
    public buckets: Bucket[];
    public index: number[];
    public tabs?: JQuery<HTMLElement>;
    readonly html: string;
    private _updatePending: number | null;

    static BUCKET_SIZE: number = BUCKET_SIZE;
    static BUCKET_NAV_SIZE: number = BUCKET_NAV_SIZE;
    static BUCKET_TOP_THRESHOLD: number = BUCKET_TOP_THRESHOLD;

    constructor(element: Element, options: any) {
      // TODO: Verify that this constructor works, rewrite because of CoffeeScript -> JS (this before super access)
      super($(BUCKET_BAR_TEMPLATE)[0], options);
      this.html = BUCKET_BAR_TEMPLATE;
      this.update = this.update.bind(this);

      // Plugin options
      if (options !== null) {
        this.options = options;
      }

      // gapSize parameter is used by the clustering algorithm
      // If an annotation is farther then this gapSize from the next bucket
      // then that annotation will not be merged into the bucket
      if (!this.options.gapSize) this.options = Object.assign(this.options, {gapSize: 60});

      // Selectors for the scrollable elements on the page
      if (!this.options.scrollables) this.options = Object.assign(this.options, {scrollables: ['body']});

      // buckets of annotations that overlap
      this.buckets = [];

      // index for fast hit detection in the buckets
      this.index = [];

      // tab elements
      this.tabs = undefined;

      this._updatePending = null;

      if (!this.options.container) {
          $(element).append(this.element!);
      }
    }

    override pluginInit() {
        var self = this;

        this.initWithOptions(this.element!, this.options);
        if (this.options.container != null) {
            $(this.options.container).append(this.element!);
        }

        $(window).on('resize scroll', () => self.update());
        (this.options.scrollables || []).map((scrollable: any) =>
            $(scrollable).on('resize scroll', () => self.update()));
    }

    override destroy() {
        var self = this;

        $(window).off('resize scroll', () => self.update());

        (this.options.scrollables || []).map((scrollable: any) =>
            $(scrollable).off('resize scroll', () => self.update()));
    }

    // Update sometime soon
    update(): number | null {
        var self = this;
        if (this._updatePending != null) { return null; }
        //From Documentation:
        //var handle = raf(callback)
        //callback is the function to invoke in the next frame. 
        //handle is a long integer value that uniquely identifies the entry in the callback list. 
        //This is a non-zero value, but you may not make any other assumptions about its value.
        return this._updatePending = raf(() => {
            self._updatePending = null;
            self._update();
        });
    }

    private _update(): void {
      // Keep track of buckets of annotations above and below the viewport
      const self = this;
      const above: AnnotatorAnchor[] = [];
      const below: AnnotatorAnchor[] = [];

      const sidebarContainerRect: DOMRect = this.annotator.frame[0].getBoundingClientRect(); //LEOS Change taking account of sidebar frame position
      // Construct indicator points
      //Points is an array providing array items with following types [number, number, AnnotatorAnchor];
      const points: any[] = this.annotator.anchors.reduce((points: any[], anchor: AnnotatorAnchor) => {
        if (!(anchor.highlights != null ? anchor.highlights.length : undefined)) {
          return points;
        }

        const rect = highlighter['getBoundingClientRect'](anchor.highlights);
        const x = rect.top - sidebarContainerRect.top; //LEOS Change - alignement of the pins: takes account of sidebar position and height compared to the window
        const h = rect.bottom - rect.top;

        if (x < BUCKET_TOP_THRESHOLD) {
          if (!above.includes(anchor)) { above.push(anchor); }
        } else if (x > (sidebarContainerRect.height - BUCKET_NAV_SIZE)) { //LEOS Change - alignement of the pins: takes account of sidebar position and height compared to the window
          if (!below.includes(anchor)) { below.push(anchor); }
        } else {
          points.push([x, 1, anchor]);
          points.push([x + h, -1, anchor]);
        }
        return points;
      }
      , []);

      // Accumulate the overlapping annotations into buckets.
      // The algorithm goes like this:
      // - Collate the points by sorting on position then delta (+1 or -1)
      // - Reduce over the sorted points
      //   - For +1 points, add the annotation at this point to an array of
      //     "carried" annotations. If it already exists, increase the
      //     corresponding value in an array of counts which maintains the
      //     number of points that include this annotation.
      //   - For -1 points, decrement the value for the annotation at this point
      //     in the carried array of counts. If the count is now zero, remove the
      //     annotation from the carried array of annotations.
      //   - If this point is the first, last, sufficiently far from the previous,
      //     or there are no more carried annotations, add a bucket marker at this
      //     point.
      //   - Otherwise, if the last bucket was not isolated (the one before it
      //     has at least one annotation) then remove it and ensure that its
      //     annotations and the carried annotations are merged into the previous
      //     bucket.
      ({buckets: self.buckets, index: self.index} = points
        .sort(_collate)
        .reduce(({buckets, index, carry}, ...rest) => {
          let a, 
            d, 
            i, 
            j, 
            x;
          let points;
          [x, d, a] = rest[0], i = rest[1], points = rest[2];
          if (d > 0) {                                            // Add annotation
            if ((j = carry.anchors.indexOf(a)) < 0) {
              carry.anchors.unshift(a);
              carry.counts.unshift(1);
            } else {
              carry.counts[j]++;
            }
          } else {                                                // Remove annotation
            j = carry.anchors.indexOf(a);                       // XXX: assert(i >= 0)
            if (--carry.counts[j] === 0) {
              carry.anchors.splice(j, 1);
              carry.counts.splice(j, 1);
            }
          }

          if (
            ((index.length === 0) || (i === (points.length - 1))) ||  // First or last?
          (carry.anchors.length === 0) ||                      // A zero marker?
          ((x - index[index.length-1]) > self.options.gapSize)      // A large gap?
          ) {                                                   // Mark a new bucket.
            buckets.push(carry.anchors.slice());
            index.push(x);
          } else {
          // Merge the previous bucket, making sure its predecessor contains
          // all the carried annotations and the annotations in the previous
          // bucket.
            let a0, 
              last, 
              toMerge;
            if (buckets[buckets.length-2] != null && buckets[buckets.length-2].length) {
              last = buckets[buckets.length-2];
              toMerge = buckets.pop();
              index.pop();
            } else {
              last = buckets[buckets.length-1];
              toMerge = [];
            }
            for (a0 of carry.anchors) { if (!last.includes(a0)) { last.push(a0); } }
            for (a0 of toMerge) { if (!last.includes(a0)) { last.push(a0); } }
          }

          return {buckets, index, carry};
        }
        , {
          buckets: [],
          index: [],
          carry: {
            anchors: [],
            counts: [],
            latest: 0,
          },
        }
       ));

      // Scroll up
      self.buckets.unshift([], above, []);
      self.index.unshift(0, BUCKET_TOP_THRESHOLD - 1, BUCKET_TOP_THRESHOLD);

      // Scroll down
      self.buckets.push([], below, []);
      self.index.push(sidebarContainerRect.height - BUCKET_NAV_SIZE,    //LEOS Change alignement of the bottom pin: takes account of sidebar height - NOT window height
        (sidebarContainerRect.height - BUCKET_NAV_SIZE) + 1,          //LEOS Change alignement of the bottom pin: takes account of sidebar height - NOT window height
        sidebarContainerRect.height);                                 //LEOS Change alignement of the bottom pin: takes account of sidebar height - NOT window height

      // Calculate the total count for each bucket (without replies) and the
      // maximum count.any
      let max = 0;
      for (let b of this.buckets) {
        max = Math.max(max, b.length);
      }

      // Update the data bindings
      const {
        element,
      } = this;

      // Keep track of tabs to keep element creation to a minimum.
      if (!this.tabs) { this.tabs = $([]); }

      // Remove any extra tabs and update @tabs. 
      this.tabs.slice(this.buckets.length).remove();   
      this.tabs = this.tabs.slice(0, this.buckets.length);

      // Create any new tabs if needed.
      $.each(this.buckets.slice(this.tabs.length), () => {
        const div: JQuery<HTMLElement> = $('<div/>').appendTo(element!);

        (self.tabs as any).push(div[0]);

        div.addClass('annotator-bucket-indicator')

        // Focus corresponding highlights bucket when mouse is hovered
        // TODO: This should use event delegation on the container.
          .on('mousemove', (event: JQueryEventObject) => {
            const bucket: number = self.tabs!.index(event.currentTarget);
            for (let anchor of self.annotator.anchors) {
              const toggle = self.buckets[bucket].includes(anchor);
              $(anchor.highlights || []).toggleClass('annotator-hl-focused', toggle);
            };
          }).on('mouseout', (event: JQueryEventObject) => {
            const bucket = self.tabs!.index(event.currentTarget);
            self.buckets[bucket].map((anchor) =>
              $((anchor as AnnotatorAnchor).highlights || []).removeClass('annotator-hl-focused'));
          }).on('click', (event: JQueryEventObject) => {
            const bucket = self.tabs!.index(event.currentTarget);
            event.stopPropagation();

            // If it's the upper tab, scroll to next anchor above
            if (self.isUpper(bucket)) {
              scrollToClosest(self.buckets[bucket], 'up');
              // If it's the lower tab, scroll to next anchor below
            } else if (self.isLower(bucket)) {
              scrollToClosest(self.buckets[bucket], 'down');
            } else {
              const annotations = (self.buckets[bucket].map((anchor: any) => anchor!.annotation));
              self.annotator.selectAnnotations(annotations,  //LEOS Change: correction for null pointer exception
                (event.ctrlKey || event.metaKey));
            }
          });
      });
      this._buildTabs();
    }

    public _buildTabs(): void {
      this.tabs?.each((d: number, el: HTMLElement) => {
        let bucketSize: number;
        const _el: JQuery<HTMLElement> = $(el);
        const bucket: Bucket | null = this.buckets[d];
        const bucketLength: number | undefined = (bucket != null) ? bucket.length : undefined;

        let title = 'Show one annotation';
        if (bucketLength !== 1) {
          title = `Show ${bucketLength} annotations`;
        }

        _el.attr('title', title);
        _el.toggleClass('upper', this.isUpper(d));
        _el.toggleClass('lower', this.isLower(d));

        if (this.isUpper(d) || this.isLower(d)) {
          bucketSize = BUCKET_NAV_SIZE;
        } else {
          bucketSize = BUCKET_SIZE;
        }

        _el.css({
          top: (this.index[d] + this.index[d+1]) / 2,
          marginTop: -bucketSize / 2,
          display: !bucketLength ? 'none' : '',
        });

        if (bucket) {
          _el.html(`<div class='label'>${bucketLength}</div>`);
        }
      });
    }

    public isUpper(i: number): boolean { return i === 1; }
    public isLower(i: number): boolean { return i === (this.index.length - 2); }
};
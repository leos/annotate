/*
 * decaffeinate suggestions:
 * DS201: Simplify complex destructure assignments
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import * as Raf from 'raf';
import scrollIntoView from './imports/scroll-into-view';

import Delegator from './delegator';
import { SubscribeCallback, IDelegator } from './delegator';
import $ from './imports/jquery';
import * as adder from './adder';
import { highlighterFacade } from './highlighter';
import * as rangeUtil from './range-util';
import { selections } from './selections';
import * as xpathRange from './anchoring/range';
import { normalizeURI } from './util/url';
import * as anchoringHtml from './anchoring/leos';
import { CrossFrameOptions, ICrossFrame } from './plugin/cross-frame';
import { Annotation } from '../../shared/models/annotation.model';
import { IBucketBar } from './plugin/bucket-bar';
import { IDocumentMeta } from './plugin/document';
import { AnnotatorAnchorSelector, AnnotatorDocumentInfo } from './models/annotator';
import { LeosSelector } from './anchoring/leos-types';
import { AnnotatorAnchor, AnnotatorAnchorTarget } from './models/annotator';

const highlighter: any = highlighterFacade;
const raf = (Raf as any).default;

function animationPromise(fn: Function): Promise<any> { 
  return new Promise((resolve, reject) => raf(function () {
    try {
      return resolve(fn());
    } catch (error) {
      return reject(error);
    }
  }));
};

const SHOW_HIGHLIGHTS_CLASS = 'annotator-highlights-always-on';

type HTMLAdder = { adder: string };

type AnnotationEvent = JQueryEventObject & { annotations?: Annotation[] };

export interface IGuest extends IDelegator {
  adder: JQuery<HTMLElement>;
  adderCtrl: adder.IAdder;
  anchors: AnnotatorAnchor[];
  toolbar: JQuery<HTMLElement>;
  createPageNote(): Annotation;
  createAnnotation(annotation?: Annotation | null): Annotation;
  selectAnnotations(annotations: Annotation[], toggle?: boolean): any;
} 

export default class Guest extends Delegator implements IGuest {
    public anchors: AnnotatorAnchor[];
    public toolbar!: JQuery<HTMLElement>;

    protected visibleHighlights: boolean;
    protected visibleGuideLines: boolean;
    protected crossframe: ICrossFrame;
    protected contextRoot!: string;
    protected plugins: any;
    protected selectedRanges?: Range[] | null;
    protected _adder: JQuery<HTMLElement>;
    protected _adderCtrl: adder.IAdder;
    protected frameIdentifier: string | null;
    protected html: HTMLAdder;
    protected selections: any;
    protected visibleGuideLinesPrevState: boolean;

    constructor(element: Element, config: any) {
      super(element, config);

      if (!this.options) this.options = { Document: {}, TextSelection: {} };
      if (!this.options.Document) this.options.Document = {};
      if (!this.options.TextSelection) this.options.TextSelection = {};
      this.initWithOptions(element, config);

      if (!this.events) {
        this.events = new Map();
      }
      this.events.set('.annotator-hl click', 'onHighlightClick');
      this.events.set('.annotator-hl mouseover', 'onHighlightMouseover');
      this.events.set('.annotator-hl mouseout', 'onHighlightMouseout');
      this.events.set('click', 'onElementClick',);
      this.events.set('touchstart', 'onElementTouchStart');

      // Internal state
      this.plugins = {};
      this.anchors = [];
      this.visibleHighlights = true;
      this.visibleGuideLines = true; //LEOS Change
      this.visibleGuideLinesPrevState = true; //LEOS Change
      this.frameIdentifier = null;

      this.html = { adder: '<hypothesis-adder></hypothesis-adder>' };

      this._adder = $(this.html.adder).appendTo(this.element!).hide();

      const self = this;
      this._adderCtrl = new adder.Adder(this._adder[0], {
        onAnnotate() {
          self.createAnnotation(null);
          return document.getSelection()?.removeAllRanges();
        },
        onHighlight() {
          self.setVisibleHighlights(true);
          self.createHighlight();
          return document.getSelection()?.removeAllRanges();
        },
      });

      const isValidRange = (range: Range): boolean => { // LEOS Change: only allow selections inside akn document
        const parent = range.commonAncestorContainer.parentElement;
        return (parent != null) &&
          (parent.closest(config.annotationContainer) != null) && // inside akn document element
          parent.closest('.leos-placeholder') == null; // ...but outside ckeditor
      }
      this.selections = selections(element as HTMLElement)?.subscribe({ //LEOS Change: selections allowed on element not document
        next(range: Range) {
          if (range && isValidRange(range)) {
            return self._onSelection(range);
          } else {
            return self._onClearSelection();
          }
        },
      });

      // Set the frame identifier if it's available.
      // The "top" guest instance will have this as null since it's in a top frame not a sub frame
      this.frameIdentifier = config.subFrameIdentifier || null;

      const cfOptions: CrossFrameOptions = {
        config: config,
        on: (event: string, handler: SubscribeCallback): IDelegator => {
          return self.subscribe(event, handler);
        },
        emit: (event: string, ...args: any[]): IDelegator => {
          return self.publish(event, args);
        }
      }

      if (typeof (config.clientUrl) !== 'undefined') {
        const hypothesisUrl = new URL(config.clientUrl);
        // Set options
        this.contextRoot = hypothesisUrl.pathname.substring(0, hypothesisUrl.pathname.indexOf('/', 2));
      }

      this.addPlugin('CrossFrame', cfOptions);
      this.crossframe = this.plugins['CrossFrame'];

      this.crossframe.onConnect(() => self._setupInitialState(config));
      this._connectAnnotationSync(this.crossframe);
      this._connectAnnotationUISync(this.crossframe);
      this.loadPlugins();
    }

    get adder(): JQuery<HTMLElement> {
      return this._adder;
    }

    get adderCtrl(): adder.IAdder {
      return this._adderCtrl;
    }

    loadPlugins(): void {
      // Load plugins
      for (let name of Object.keys(this.options || {})) {
        let opts = this.options[name];
        if(name === 'Document' && !!this.options['legFileName']) {
          opts.legFileName = this.options['legFileName'];
        }
        if (!this.options.pluginClasses.hasOwnProperty(name)) continue;
        if (this.plugins.hasOwnProperty(name)) continue;
        this.addPlugin(name, opts);
      }
    }

    addPlugin(name: string, options: any): IGuest {
      if (this.plugins.hasOwnProperty(name)) {
        console.error('You cannot have more than one instance of any plugin.');
        return this;
      }

      const klass = this.options.pluginClasses[name];
      if (typeof klass !== 'function') {
        console.error('Could not load ' + name + ' plugin. Have you included the appropriate <script> tag?');
        return this;
      }

      let klassInstance: any = new klass(this.element![0], options);
      klassInstance.annotator = this;
      if (typeof klassInstance.pluginInit === 'function') {
        klassInstance.pluginInit();
      }
      this.plugins[name] = klassInstance;
      return this; // allow chaining
    }

    // Get the document info
    getDocumentInfo(): Promise<AnnotatorDocumentInfo> {
      let metadataPromise: Promise<any>;
      let uriPromise: Promise<any>;

      if (this.plugins['PDF'] != undefined) {
        metadataPromise = Promise.resolve(this.plugins['PDF'].getMetadata());
        let uri: string = this.plugins['PDF'].uri();
        uriPromise = Promise.resolve(uri);
      } else if (this.plugins['Document'] != undefined) {
        let uri = this.plugins['Document'].uri();
        uriPromise = Promise.resolve(uri);
        metadataPromise = Promise.resolve(this.plugins['Document'].metadata);
      } else {
        const error = new Error('Error reading document info. No fitting plugin found!');
        metadataPromise = Promise.reject(error);
        uriPromise = Promise.reject(error);
      }

      uriPromise = uriPromise.catch(() => decodeURIComponent(window.location.href));
      metadataPromise = metadataPromise.catch(() => ({
        title: document.title,
        link: [{ href: decodeURIComponent(window.location.href) }],
      }));

      const frameIdentifier: string | null = this.frameIdentifier;
      return Promise.all([metadataPromise, uriPromise]).then((...args) => {
        const [metadata, href] = args[0];
        return {
          uri: normalizeURI(href),
          metadata,
          frameIdentifier,
        };
      });
    }

    protected _setupInitialState(config: any) {
      this.publish('panelReady');
      this.setVisibleHighlights(config.showHighlights === 'always');
      this.setVisibleGuideLines(config.showGuideLinesButton);
    }

    protected _connectAnnotationSync(crossframe: ICrossFrame): IDelegator {
      const self = this;

      this.subscribe('annotationDeleted', (annotation: Annotation) => {
        self.detach(annotation);
      });

      return this.subscribe('annotationsLoaded', (annotations: Annotation[]) => {
        let addToPromiseChain = false;
        for (let annotation of annotations) {
          var promiseChain = self.anchor(annotation, promiseChain, addToPromiseChain);
          addToPromiseChain = true;
        }
      });
    }

    protected _connectAnnotationUISync(crossframe: ICrossFrame): void {
      const self = this;

      crossframe.on('scrollToAnnotation', (tag: string) => {
        for (let anchor of self.anchors) {
          if (anchor.highlights != null && anchor.annotation?.$tag === tag) {
            const event = new CustomEvent('scrolltorange', {
              bubbles: true,
              cancelable: true,
              detail: anchor.range,
            });
            if (typeof (self.element![0]) !== 'undefined') {
              const defaultNotPrevented = self.element![0].dispatchEvent(event);
              if (defaultNotPrevented) {
                scrollIntoView(anchor.highlights[0]);
              }
            }
          }
        }
      });

      crossframe.on('getDocumentInfo', (cb: any) => self.getDocumentInfo()
        .then(info => cb(undefined, info))
        .catch(reason => cb(reason, undefined))
      );

      crossframe.on('setVisibleHighlights', (state?: boolean) => self.setVisibleHighlights(state));

      crossframe.on('LEOS_setVisibleGuideLines', (state?: boolean, storePrevState?: boolean) => self.setVisibleGuideLines(state, storePrevState));

      crossframe.on('LEOS_restoreGuideLinesState', () => self.restoreGuideLinesState());
    }

    
    override destroy(): void {
      $('#annotator-dynamic-style').remove();

      this.selections.unsubscribe();
      this._adder.remove();

      this.element?.find('.annotator-hl').each(function () {
        $(this).contents().insertBefore(this);
        $(this).remove();
      });

      this.element?.data('annotator', null);

      for (let name in this.plugins) {
        if (!this.plugins.hasOwnProperty(name)) continue;
        (this.plugins[name] as IDelegator).destroy();
      }

      return super.destroy();
    }

    protected anchor(annotation: Annotation, promiseChain: any, addToPromiseChain: boolean = false) {
      const self = this;
      const root = this.element![0];

      // Anchors for all annotations are in the `anchors` instance property. These
      // are anchors for this annotation only. After all the targets have been
      // processed these will be appended to the list of anchors known to the
      // instance. Anchors hold an annotation, a target of that annotation, a
      // document range for that target and an Array of highlights.
      const anchors: Promise<AnnotatorAnchor>[] = [];

      // The targets that are already anchored. This function consults this to
      // determine which targets can be left alone.
      const anchoredTargets = [];

      // These are the highlights for existing anchors of this annotation with
      // targets that have since been removed from the annotation. These will
      // be removed by this function.
      let deadHighlights: HTMLElement[] = [];

      // Initialize the target array.
      if (annotation.target == null) { annotation.target = []; }

      const locate = function (target: AnnotatorAnchorTarget) {
        // Check that the anchor has a TextQuoteSelector -- without a
        // TextQuoteSelector we have no basis on which to verify that we have
        // reanchored correctly and so we shouldn't even try.
        //
        // Returning an anchor without a range will result in this annotation being
        // treated as an orphan (assuming no other targets anchor).
        if (!(target.selector || []).some(s => s.type === 'TextQuoteSelector')) {
          return Promise.resolve({ annotation, target });
        }

        // Find a target using the anchoring module.
        const options = {
          ignoreSelector: '[class^="annotator-"]',
        };
        return anchoringHtml.anchor(root, target.selector || [], options)
          .then(range => ({
            annotation,
            target,
            range,
          }))
          .catch(() => ({
            annotation,
            target,
          }));
      };


      /** 
       * If the original match has been removed, 'dom-anchor-text-quote' returns the next best match, without taking the selector's prefix and suffix into account.
       * This method only compares some characters of the prefix, which is usually good enough. Increasing the number of characters compared or taking the suffix
       * into account are possible improvements.
       */
      const removeRangeIgnoringPrefix = (anchor: AnnotatorAnchor) => {
        if ((anchor.range == null)) {
          return anchor;
        }

        const selector: any = (anchor.target?.selector || []).find(s => s.type === 'LeosSelector') as LeosSelector;
        if ((selector == null)) {
          return anchor;
        }

        let charactersToCompare = 20;
        const anchorLeosSelector = selector as LeosSelector;
        const rangeStartContainerText = anchor.range.startContainer.textContent;
        const previousRangeCharacters = rangeStartContainerText?.substring(anchor.range.startOffset - charactersToCompare, anchor.range.startOffset);
        if (!previousRangeCharacters || previousRangeCharacters.trim()  === '') {
          return anchor;
        }

        if (previousRangeCharacters.length < charactersToCompare) {
          charactersToCompare = previousRangeCharacters.length;
        }

        const selectorPreviousCharacters = anchorLeosSelector.prefix.substring(anchorLeosSelector.prefix.length - charactersToCompare);

        if (previousRangeCharacters !== selectorPreviousCharacters) {
          anchor.range = undefined;
        }

        return anchor;
      };

      /** Remove the range of a comment anchor if text coverage is lower than a given threshold */
      const removeCommentRangeIfNotMatchEnough = (anchor: AnnotatorAnchor): AnnotatorAnchor => {
        if (!(anchor.annotation?.tags && anchor.annotation.tags[0] && (anchor.annotation.tags[0] === 'comment'))) {
          return anchor;
        }

        if ((anchor.range == null)) {
          return anchor;
        }

        // If anchor text coverage is lower then threshold (in percent), comment will be moved to orphans
        const coverageThreshold = 66;
        const selector = (anchor.target?.selector || []).find(selector => (selector.type === 'LeosSelector'));

        if ((selector == null)) {
          return anchor;
        }

        const anchorLeosSelector = selector as LeosSelector;
        const rangeText = anchor.range.toString();
        const anchorText = anchorLeosSelector.exact;
        // Compare range and anchor text to set new range if they differ
        if (rangeText !== anchorText) {
          const rangeTextWords = rangeText.trim().split(' ');
          const anchorTextWords = anchorText.trim().split(' ');

          // Counter for found matching words
          let matchCounter = 0;
          // End offset of the last found word
          let endOffsetLastWordMatch = 0;

          for (let item of Array.from(anchorTextWords)) {
            let word = item as string;
            const indexOfWord = rangeText.indexOf(word, endOffsetLastWordMatch);
            if (indexOfWord >= endOffsetLastWordMatch) {
              endOffsetLastWordMatch = indexOfWord + word.length;
              matchCounter++;
            }
          }

          const fixed = ((matchCounter / rangeTextWords.length) * 100).toFixed(2);
          const totalMatchInPercent = Number(fixed);

          // Coverage of anchor and rangeText in percent
          if (totalMatchInPercent >= coverageThreshold) {
            if ((anchor.range?.endContainer.textContent?.length || 0) < endOffsetLastWordMatch) {
              endOffsetLastWordMatch = 0;
            }
            anchor.range.setEnd(anchor.range.endContainer, endOffsetLastWordMatch);
          }

          if (totalMatchInPercent < coverageThreshold) {
            anchor.range = undefined;
          }
        }

        return anchor;
      };

      /** Mark a suggestion if the "exact" text does not match 100%. */
      const markSuggestionRangeWithoutExactTextMatch = (anchor: AnnotatorAnchor): AnnotatorAnchor => {
        if (anchor.range == null) {
          return anchor;
        }
        if (anchor.annotation?.tags?.length === 0) {
          return anchor;
        }

        if (anchor.annotation?.tags?.at(0) !== 'suggestion') {
          return anchor;
        }

        const selectors = (anchor.target?.selector != null) ? anchor.target.selector : [];
        const textQuoteSelector = selectors.find(selector => (selector.type === 'TextQuoteSelector'));
        if (!textQuoteSelector) {
            return anchor;
        }
        anchor.annotation.isRangeNotExactMatch = !rangeUtil.isRangeTextMatchSelectorText(anchor, textQuoteSelector);
        return anchor;
      };

      const markSuggestionForDeletedAnchor = (anchor: AnnotatorAnchor): AnnotatorAnchor => {
        if (anchor.annotation?.isRangeNotExactMatch) {
          return anchor;
        }
        if (anchor.annotation?.tags?.length === 0) {
          return anchor;
        }

        if (anchor.annotation?.tags?.at(0) !== 'suggestion') {
          return anchor;
        }

        const selectors = (anchor.target?.selector != null) ? anchor.target.selector : [];
        const leosSelector: LeosSelector | undefined = selectors.find(selector => (selector.type === 'LeosSelector')) as LeosSelector;
        if (!leosSelector) {
            return anchor;
        }

        anchor.annotation.$leosDeleted = rangeUtil.isSelectorTextDeleted(leosSelector, root);
        return anchor;
      }

      const addAnchoredRangeText = (anchor: AnnotatorAnchor): AnnotatorAnchor => {
        if (!anchor.range) {
          return anchor;
        }

        const rangeText = anchor.range.toString();

        anchor.annotation = Object.assign(anchor.annotation || {}, {anchoredRangeText: rangeText});
        return anchor;
      };

      const fixRangeIfNeeded = (anchor: AnnotatorAnchor): AnnotatorAnchor => {
        let selectors = (anchor.target?.selector != null) ? anchor.target.selector : [];
        let leosSelector: LeosSelector | undefined = selectors.find(selector => (selector.type === 'LeosSelector')) as LeosSelector;
        if (!leosSelector) {
          return anchor;
        }

        if (anchor.range) {
          return fixExistingRangeIfNeeded(anchor, leosSelector);
        }
        return rangeUtil.findNextBestRange(anchor, leosSelector, root);
      }

      const fixExistingRangeIfNeeded = function (anchor: AnnotatorAnchor, leosSelector: LeosSelector) {
        if (anchor.annotation) {
          anchor.annotation.$selectorMoved = rangeUtil.isLeosSelectorMoved(leosSelector, root);
        }
        if (!anchor.annotation?.$selectorMoved 
          && !anchor.annotation?.$leosDeleted 
          && rangeUtil.isRangeTextMatchSelectorText(anchor, leosSelector)) {
            return anchor;
        }
        return rangeUtil.findNextBestRange(anchor, leosSelector, root);
      }

      const highlight = (anchor: AnnotatorAnchor): Promise<AnnotatorAnchor> => {
        // Highlight the range for an anchor.
        if (!anchor.range) { return Promise.resolve(anchor) };
        return animationPromise(function () {
          let highl  = '', hlid = `hl-`;
          let visible = (typeof annotation.visible == 'undefined') || annotation.visible;

          //LEOS Change - differentiate between annotation/suggestion and highlight
          if (anchor.annotation?.tags?.at(0) === 'highlight')
            highl = `leos-annotator-highlight`;
          else
            highl = `annotator-hl`;

          const range = xpathRange.sniff(anchor.range || {}) as xpathRange.AnnotatorRange;
          if (!range) return anchor;
          const normedRange: xpathRange.INormalizedRange = range.normalize(root)!;
          let normAnc: JQuery<Element> = $(normedRange.commonAncestor);
          let temporaryId: string = Math.random().toString().replace("0.", "");
          hlid += typeof annotation.id == 'undefined' ? temporaryId : annotation.id;

          let highlights = normAnc.find('hypothesis-highlight.'+hlid);
          if (highlights.length == 0) {
            highlights = normAnc.find('hypothesis-highlight.hl-'+temporaryId);//if id come for new annot
          }

          if (highlights.length == 0) {
            highlights = highlighter['highlightRange'](normedRange, (visible ? highl+' ':'') + hlid);
            anchor.highlights = highlights.toArray();
          } else {
            if ((hlid != 'hl-'+ temporaryId) && highlights.hasClass('hl-'+ temporaryId)){
              highlights.removeClass('hl-'+ temporaryId);
              highlights.addClass(hlid);
            }
            anchor.highlights = highlights.toArray();
            highlights.toggleClass(highl, visible);
          }
          $(highlights).data('annotation', anchor.annotation || null);
          return anchor;
        });

      };

      const sync = (anchors: AnnotatorAnchor[]): AnnotatorAnchor[]  => {
        // Store the results of anchoring.

        // An annotation is considered to be an orphan if it has at least one
        // target with selectors, and all targets with selectors failed to anchor
        // (i.e. we didn't find it in the page and thus it has no range).
        let hasAnchorableTargets = false;
        let hasAnchoredTargets = false;
        for (let anchor of anchors) {
          if (anchor.target?.selector != null) {
            hasAnchorableTargets = true;
            if (anchor.range != null) {
              hasAnchoredTargets = true;
              break;
            }
          }
        }
        annotation = Object.assign(annotation, {
          '$orphan': hasAnchorableTargets && !hasAnchoredTargets,
          '$leosDeleted': anchors.some((anchor) => !!anchor.annotation?.$leosDeleted),
          '$selectorMoved': anchors.some((anchor) => !!anchor.annotation?.$selectorMoved)
        });

        // Add the anchors for this annotation to instance storage.
        self.anchors = self.anchors.concat(anchors);

        // Let plugins know about the new information.
        if (self.plugins.hasOwnProperty('BucketBar')) {
          (self.plugins['BucketBar'] as IBucketBar).update();
        }
        if (self.plugins.hasOwnProperty('CrossFrame')) {
          (self.plugins['CrossFrame'] as ICrossFrame).sync([annotation]);
        }
        self.publish('LEOS_annotationsSynced');

        return anchors;
      };

      if (addToPromiseChain) {
        return promiseChain.then(() => self.anchor(annotation, [], false));
      } else {
        // Remove all the anchors for this annotation from the instance storage.
        for (const anchor of self.anchors.splice(0, self.anchors.length)) {
          if (anchor.annotation === annotation) {
            // Anchors are valid as long as they still have a range and their target
            // is still in the list of targets for this annotation.
            if ((anchor.range != null) && annotation.target.includes(anchor.target || {})) {
              anchors.push(Promise.resolve(anchor));
              if (anchor.target) anchoredTargets.push(anchor.target);
            } else if (anchor.highlights != null) {
              // These highlights are no longer valid and should be removed.
              deadHighlights = deadHighlights.concat(anchor.highlights);
              delete anchor.highlights;
              delete anchor.range;
            }
          } else {
            // These can be ignored, so push them back onto the new list.
            self.anchors.push(anchor);
          }
        }

        // Remove all the highlights that have no corresponding target anymore.
        raf(() => highlighter['removeHighlights'](deadHighlights));

        // Anchor any targets of this annotation that are not anchored already.
        for (let target of annotation.target) {
          if (!anchoredTargets.includes(target)) {
            const anchor: Promise<AnnotatorAnchor> = locate(target)
                .then(removeRangeIgnoringPrefix)
                .then(addAnchoredRangeText)
                .then(markSuggestionRangeWithoutExactTextMatch)
                .then(markSuggestionForDeletedAnchor)
                .then(fixRangeIfNeeded)
                .then(highlight)
            anchors.push(anchor);
          }
        }

        return Promise.all(anchors).then(sync);
      }
    }

    detach(annotation: Annotation): number {
      const self = this;
      const anchors: AnnotatorAnchor[] = [];
      let unhighlight: HTMLElement[] = [];

      for (let anchor of this.anchors) {
        if (anchor.annotation === annotation) {
          unhighlight = unhighlight.concat(anchor.highlights || []);
        } else {
          anchors.push(anchor);
        }
      }

      this.anchors = anchors;
      return raf(() => {
        highlighter['removeHighlights'](unhighlight);
        return (self.plugins.hasOwnProperty('BucketBar') ? (self.plugins['BucketBar'] as IBucketBar).update() : undefined);
      });
    }

    createPageNote(): Annotation {
      return this.createAnnotation({$pageNote: true});
    }

    createAnnotation(annotation: Annotation | null): Annotation {
      const self = this;
      if (annotation == null) { annotation = {}; }
      const root: Element = this.element![0];

      const ranges = this.selectedRanges || [];
      this.selectedRanges = null;

      const getSelectors = function (range:Range | xpathRange.AnnotatorRange) {
        if (!!annotation?.$pageNote) return [];
        const options = {
          ignoreSelector: '[class^="annotator-"]',
        };
        // Returns an array of selectors for the passed range.
        return anchoringHtml.describe(root, range, options);
      };

      const setDocumentInfo = function (info: AnnotatorDocumentInfo) {
        if (!annotation) return null;
        annotation.document = info.metadata;
        annotation.uri = info.uri;
        return annotation;
      };

      const setTargets = function (...args: any[]): Annotation | null {
        // `selectors` is an array of arrays: each item is an array of selectors
        // identifying a distinct target.
        let info,
          selectors;
        [info, selectors] = args[0];
        const source = info.uri;
        if (annotation) {
          annotation.target = (selectors.map((selector: any) => ({ source, selector })));
        }
        return annotation;
      };

      const info: Promise<AnnotatorDocumentInfo> = this.getDocumentInfo();
      const metadata: Promise<Annotation | null> = info.then(setDocumentInfo);
      const selectors = Promise.all(ranges.map(getSelectors));
      if (annotation.$suggestion) { selectors.then(selectorsValue => self.addSucceedingAndPrecedingText(annotation!, ranges[0], selectorsValue[0])); }
      const targets = Promise.all([info, selectors]).then(setTargets);

      targets.then(() => self.publish('beforeAnnotationCreated', [annotation]));
      targets.then(() => self.anchor(annotation!, [], false));

      if (!annotation.$highlight) {
        if (this.crossframe != null) {
          this.crossframe.call('showSidebar');
        }
      }
      return annotation;
    }

    addSucceedingAndPrecedingText(annotation: Annotation, range: Range, selectors: AnnotatorAnchorSelector[]): string | null {
      if (!range || !selectors) { return null; }
      let textInCommonAncestor: string = (range.commonAncestorContainer as any).innerText;
      if (!textInCommonAncestor) {
        textInCommonAncestor = range.commonAncestorContainer?.parentElement?.innerText || '';
      }
      const leosSelector: LeosSelector = selectors.find(s => s.type === 'LeosSelector') as LeosSelector;
      if (leosSelector) {
        annotation.precedingText = textInCommonAncestor.substring(0, leosSelector.start);
        annotation.succeedingText = textInCommonAncestor.substring(leosSelector.end);
        return annotation.succeedingText;
      }
      return null;
    }

    createHighlight(): Annotation {
      return this.createAnnotation({ $highlight: true });
    }

    createComment(): Annotation {
      var self = this;
      const annotation: Annotation = { document: undefined, uri: '', target: undefined };

      const prepare = function (info: any) {
        annotation.document = info.metadata;
        annotation.uri = info.uri;
        return annotation.target = [{ source: info.uri }];
      };

      this.getDocumentInfo()
        .then(prepare)
        .then(() => self.publish('beforeAnnotationCreated', [annotation]));

      return annotation;
    }

    // Public: Deletes the annotation by removing the highlight from the DOM.
    // Publishes the 'annotationDeleted' event on completion.
    //
    // annotation - An annotation Object to delete.
    //
    // Returns deleted annotation.
    deleteAnnotation(annotation: Annotation): Annotation {
      if (annotation.highlights != null) {
        for (let h of annotation.highlights) {
          if (h.parentNode != null) {
            const nodes: Node[] = [];
            h.childNodes.forEach((child) => {
              nodes.push(child as Node);
            });
            $(h).replaceWith(nodes as Element[]);
          }
        }
      }

      this.publish('annotationDeleted', [annotation]);
      return annotation;
    }

    showAnnotations(annotations: Annotation[]): void {
      const tags = Array.isArray(annotations) ? annotations.map(el => el.$tag) : [];
      if (this.crossframe != null) {
        this.crossframe.call('showAnnotations', tags);
        this.crossframe.call('showSidebar');
      }
    }

    toggleAnnotationSelection(annotations: Annotation[]): void {
      const tags = Array.isArray(annotations) ? annotations.map(el => el.$tag) : [];
      if (this.crossframe != null) {
        this.crossframe.call('toggleAnnotationSelection', tags);
      }
    }

    updateAnnotations(annotations: Annotation[]): void {
      const tags = Array.isArray(annotations) ? annotations.map(el => el.$tag) : [];
      if (this.crossframe != null) {
        this.crossframe.call('updateAnnotations', tags);
      }
    }

    // TODO: See for other strange effects. The "this" in methods call the outmost focusAnnotations (leos-sidebar) that takes 2 parameters
    // but then this is undefined, we want the this.crossframe.call. Why did this work before, CoffeeScript should behave the same.
    callFocusAnnotations(annotations: Annotation[]): void {
      const tags = Array.isArray(annotations) ? annotations.map(el => el.$tag) : [];
      if (this.crossframe != null) {
        this.crossframe.call('focusAnnotations', tags);
      }
    }

    protected _getRoot(): Element {
      //Root element is coming from the document plugin
      //LEOS-2789 to evaluate ranges correctly, the reference element 'root' should be parent of 'akamontoso' as xpaths are stored like /akamontoso/.../.../
      if (this.plugins.hasOwnProperty('PDF')) {
        return this.plugins['PDF'].getElement();
      } else if (this.plugins.hasOwnProperty('Document')) {
        return (this.plugins['Document'] as IDocumentMeta).getElement();
      } else {
        return this.element![0];
      }
    }

    protected _onSelection(range: Range): void {
      const selection = document.getSelection();
      if (!selection) {
        this._onClearSelection();
        return;
      }

      const isBackwards = rangeUtil.isSelectionBackwards(selection);
      const focusRect = rangeUtil.selectionFocusRect(selection);
      if (!focusRect) {
        // The selected range does not contain any text
        this._onClearSelection();
        return;
      }

      this.selectedRanges = [range];

      $('.annotator-toolbar .h-icon-note')
        .attr('title', 'New Annotation')
        .removeClass('h-icon-note')
        .addClass('h-icon-annotate');

      const { left, top, arrowDirection } = this._adderCtrl.target(focusRect, isBackwards);
      this._adderCtrl.showAt(left, top, arrowDirection);
    }

    protected _onClearSelection(): void {
      this._adderCtrl.hide();
      this.selectedRanges = [];

      //    LEOS change 3632
      $('.annotator-toolbar .h-icon-annotate')
        .attr('title', 'New Document Note')
        .removeClass('h-icon-annotate')
        .addClass('h-icon-note');
    }

    selectAnnotations(annotations: Annotation[], toggle: boolean = false): void {
      if (toggle) {
        this.toggleAnnotationSelection(annotations);
      } else {
        this.showAnnotations(annotations);
      }
    }

    onElementClick(event: Event): void {
      if (false) { //!@selectedTargets?.length #LEOS Change: Disable auto close of Sidebar
        if (this.crossframe != null) this.crossframe.call('hideSidebar');
      }
    }

    onElementTouchStart(event: Event): void {
      // Mobile browsers do not register click events on
      // elements without cursor: pointer. So instead of
      // adding that to every element, we can add the initial
      // touchstart event which is always registered to
      // make up for the lack of click support for all elements.
      if (false) { //!@selectedTargets?.length #LEOS Change: Disable auto close of Sidebar
        if (this.crossframe != null) this.crossframe.call('hideSidebar');
      }
    }

    onHighlightMouseover(event: AnnotationEvent): void {
      const self = this;
      if (!this.visibleHighlights) { return; }
      const annotation = $(event.currentTarget).data('annotation');
      const annotations = event.annotations != null ? event.annotations : (event.annotations = []);
      annotations.push(annotation);

      // The innermost highlight will execute this.
      // The timeout gives time for the event to bubble, letting any overlapping
      // highlights have time to add their annotations to the list stored on the
      // event object.
      if (event.target === event.currentTarget) {
        setTimeout(() => self.callFocusAnnotations(annotations));
      }
    }

    onHighlightMouseout(event: Event): void {
      if (!this.visibleHighlights) { return; }
      this.callFocusAnnotations([]);
    }

    onHighlightClick(event: AnnotationEvent): void {
      const self = this;
      if (!this.visibleHighlights) { return; }
      const annotation = $(event.currentTarget).data('annotation');
      const annotations = event.annotations != null ? event.annotations : (event.annotations = []);
      annotations.push(annotation);

      // See the comment in onHighlightMouseover
      if (event.target === event.currentTarget) {
        const xor = (event.metaKey || event.ctrlKey);
        setTimeout(() => self.selectAnnotations(annotations, xor));
      }
    }

    // Pass true to show the highlights in the frame or false to disable.
    setVisibleHighlights(shouldShowHighlights: boolean = false): void {
      this.toggleHighlightClass(shouldShowHighlights);
    }

    toggleHighlightClass(shouldShowHighlights: boolean = false): void {
      if (shouldShowHighlights) {
        this.element?.addClass(SHOW_HIGHLIGHTS_CLASS);
      } else {
        this.element?.removeClass(SHOW_HIGHLIGHTS_CLASS);
      }

      this.visibleHighlights = shouldShowHighlights;
    }

    //Toggles guidelines visibility, if StoreState = true, saves previous state
    setVisibleGuideLines(shouldShowGuideLines: boolean = false, storePrevState: boolean = false): void {
      if (storePrevState) {
        this.visibleGuideLinesPrevState = this.visibleGuideLines;
      }
      this.visibleGuideLines = shouldShowGuideLines;
    }

    restoreGuideLinesState(): void {
      this.visibleGuideLines = this.visibleGuideLinesPrevState;
    }
};

import { AnnotatorRange } from './range';
import {
  FragmentAnchor,
  RangeAnchor,
  TextPositionAnchor,
  TextQuoteAnchor,
  SelectorModel,
  TextPositionSelector,
  TextQuoteSelector,
  RangeSelector,
  AnchoringAnchor,
  AnchoringSelector,
  FragmentSelector,
  anchoringAnchorForType
} from './types';

export interface AnchoringHtml {
  /**
   * Anchor a set of selectors.
   *
   * This function converts a set of selectors into a document range.
   * It encapsulates the core anchoring algorithm, using the selectors alone or
   * in combination to establish the best anchor within the document.
   *
   * :param Element root: The root element of the anchoring context.
   * :param Array selectors: The selectors to try.
   * :param Object options: Options to pass to the anchor implementations.
   * :return: A Promise that resolves to a Range on success.
   * :rtype: Promise
   */  
  anchor(root: Element, selectors: SelectorModel[], options: any): Promise<any>;

  describe(root: Element, range: Range | AnnotatorRange, options: any): AnchoringSelector[];

  /***
   * @param type: Class of FragmentAnchor, RangeAnchor, TextPositionAnchor, TextQuoteAnchor
   * @param selector: Selector of type FragmentAnchor, TextPositionSelector, TextQuoteSelector, RangeSelector
   */  
  querySelector(type: any, root: Element, selector: any, options: any): Promise<any>;
}

/**
 * @param type: typeof FragmentAnchor, RangeAnchor, TextPositionAnchor, TextQuoteAnchor
 * @param selector: Selector of type FragmentAnchor, TextPositionSelector, TextQuoteSelector, RangeSelector
 */
function querySelector(type: any, root: Element, selector: any, options: any): Promise<any> {
  const doQuery = function(resolve: (value: any) => void, reject: (reason?:any) => void) {
    try {
      const anchor: any = type.fromSelector(root, selector, options);
      const range = anchor.toRange(options);
      return resolve(range);
    } catch (error) {
      return reject(error);
    }
  };
  return new Promise(doQuery);
};

/**
 * Anchor a set of selectors.
 *
 * This function converts a set of selectors into a document range.
 * It encapsulates the core anchoring algorithm, using the selectors alone or
 * in combination to establish the best anchor within the document.
 *
 * :param Element root: The root element of the anchoring context.
 * :param Array selectors: The selectors to try.
 * :param Object options: Options to pass to the anchor implementations.
 * :return: A Promise that resolves to a Range on success.
 * :rtype: Promise
 */
export function anchor(root: Element, selectors: AnchoringSelector[], options: any): Promise<any> {
  // Selectors
  if (options == null) { options = {}; }
  // Fragment selector
  let fragment: FragmentSelector | null = null;
  let position: TextPositionSelector | null = null;
  let quote: TextQuoteSelector | null  = null;
  let range: RangeSelector | null = null;

  // Collect all the selectors
  for (let selector of (selectors || [])) {
    switch (selector.type) {
    case 'FragmentSelector':
      fragment = selector as FragmentSelector;
      break;
    case 'TextPositionSelector':
      position = selector as TextPositionSelector;
      options.hint = position.start;  // TextQuoteAnchor hint
      break;
    case 'TextQuoteSelector':
      quote = selector as TextQuoteSelector;
      break;
    case 'RangeSelector':
      range = selector as RangeSelector;
      break;
    }
  }

  // Assert the quote matches the stored quote, if applicable
  const maybeAssertQuote = function(range: Range): Range {
    if (quote != null && quote.exact != null && range.toString() !== quote.exact) {
      throw new Error('quote mismatch');
    } else {
      return range;
    }
  };

  // From a default of failure, we build up catch clauses to try selectors in
  // order, from simple to complex.
  let promise: Promise<any> = Promise.reject('unable to anchor');

  if (fragment != null) {
    promise = promise.catch(() => querySelector(FragmentAnchor, root, fragment, options)
      .then(maybeAssertQuote));
  }

  if (range != null) {
    promise = promise.catch(() => querySelector(RangeAnchor, root, range, options)
      .then(maybeAssertQuote));
  }

  if (position != null) {
    promise = promise.catch(() => querySelector(TextPositionAnchor, root, position, options)
      .then(maybeAssertQuote));
  }

  if (quote != null) {
    // Note: similarity of the quote is implied.
    promise = promise.catch(() => querySelector(TextQuoteAnchor, root, quote, options));
  }

  return promise;
};


export function describe(root: Element, range: Range | AnnotatorRange, options: any): AnchoringSelector[] {
  if (options == null) { options = {}; }
  const types = [FragmentAnchor, RangeAnchor, TextPositionAnchor, TextQuoteAnchor];

  const selectors: AnchoringSelector[] = [];
  for (let type of types) {
    try {
      var anchor: AnchoringAnchor = anchoringAnchorForType(root, range, type);
      selectors.push(anchor.toSelector(options));
    } catch (error) {
      continue;
    }
  }
  return selectors;
};
/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// This is a modified copy of
// https://github.com/openannotation/annotator/blob/v1.2.x/src/util.coffee

import $ from '../imports/jquery';

import { simpleXPathJQuery, simpleXPathPure, findChild } from './xpath';

// Public: Flatten a nested array structure
//
// Returns an array
function flatten(array: any[]): any[] {
  const _flatten = function(ary: any[]) {
    let flat: any[] = [];

    for (let el of ary) {
      flat = flat.concat(el && $.isArray(el) ? flatten(el) : el);
    }

    return flat;
  };

  return _flatten(array);
};

// Public: Finds all text nodes within the elements in the current collection.
//
// Returns a new jQuery collection of text nodes.
export function getTextNodes(jq: JQuery<Element> | JQuery<HTMLElement> | JQuery<Node>): JQuery<HTMLElement> {
  var _getTextNodes = (node: Node): Node[] => {
    let _node: Node | null = node;
    if (_node && (_node.nodeType !== Node.TEXT_NODE)) {
      let nodes: Node[] = [];

      // If not a comment then traverse children collecting text nodes.
      // We traverse the child nodes manually rather than using the .childNodes
      // property because IE9 does not update the .childNodes property after
      // .splitText() is called on a child text node.
      if (_node.nodeType !== Node.COMMENT_NODE) {
        // Start at the last child and walk backwards through siblings.
        _node = _node.lastChild;
        while (_node) {
          nodes = nodes.concat(_getTextNodes(_node!));
          _node = _node.previousSibling;
        }
      }

      // Finally reverse the array so that nodes are in the correct order.
      return nodes.reverse();
    } else {
      return [_node];
    }
  };

  return jq.map(function() { return flatten(_getTextNodes(this)); });
};

// Public: determine the last text node inside or before the given node
export function getLastTextNodeUpTo(n: Node): Node | null {
  switch (n.nodeType) {
  case Node.TEXT_NODE:
    return n; // We have found our text node.
  case Node.ELEMENT_NODE:
    // This is an element, we need to dig in
    if (n.lastChild != null) { // Does it have children at all?
      const result = getLastTextNodeUpTo(n.lastChild);
      if (result != null) { return result; }
    }
    break;
  default:
  }
  // Not a text node, and not an element node.
  // Could not find a text node in current node, go backwards
  const nextSibling: Node | null = n.previousSibling;
  if (nextSibling != null) {
    return getLastTextNodeUpTo(nextSibling);
  } else {
    return null;
  }
};

// Public: determine the first text node in or after the given jQuery node.
export function getFirstTextNodeNotBefore(n: Node): Node | null {
  switch (n.nodeType) {
  case Node.TEXT_NODE:
    return n; // We have found our text node.
  case Node.ELEMENT_NODE:
    // This is an element, we need to dig in
    if (n.firstChild != null) { // Does it have children at all?
      const result = getFirstTextNodeNotBefore(n.firstChild);
      if (result != null) { return result; }
    }
    break;
  default:
  }
  // Not a text or an element node.
  // Could not find a text node in current node, go forward
  const nextSibling: Node | null = n.nextSibling;
  if (nextSibling != null) {
    return getFirstTextNodeNotBefore(nextSibling);
  } else {
    return null;
  }
};

export function xpathFromNode(el: Node, relativeRoot: Node): string[] {
  let result: string[] = [];
  try {
    result = simpleXPathJQuery(el as Element, relativeRoot);
  } catch (exception) {
    console.log('jQuery-based XPath construction failed! Falling back to manual.');
    result = simpleXPathPure(el as Element, relativeRoot);
  }
  return result;
};

export function nodeFromXPath(xp: string, root: Node): Node {
  const steps = xp.substring(1).split('/');
  let node = root;
  for (let step of steps) {
    let [name, strIdx] = step.split('[');


    const idx: number = (strIdx != null) ? parseInt((strIdx != null ? strIdx.split(']') : ['1'])[0]) : 1;
    node = findChild(node, name.toLowerCase(), idx);
  }

  return node;
};
// This module exports a set of classes for converting between DOM `Range`
// objects and different types of selector. It is mostly a thin wrapper around a
// set of anchoring libraries. It serves two main purposes:
//
//  1. Providing a consistent interface across different types of anchor.
//  2. Insulating the rest of the code from API changes in the underyling anchoring
//     libraries.

import * as domAnchorTextPosition from '../imports/dom-anchor-text-position';
import * as domAnchorTextQuote from '../imports/dom-anchor-text-quote';
import * as domAnchorFragment from '../imports/dom-anchor-fragment';
import { 
  AnnotatorRange, 
  INormalizedRange, 
  SerializedRange, 
  SerializedRangeModel, 
  sniff
} from './range';

export { TextPositionSelector } from '../imports/dom-anchor-text-position';
export { TextQuoteSelector } from '../imports/dom-anchor-text-quote';
export { FragmentSelector } from '../imports/dom-anchor-fragment';


// Helper function for throwing common errors
function missingParameter(name: string) {
  throw new Error('missing required parameter "' + name + '"');
};

export interface SelectorModel {
  type: string;
}

export interface RangeSelector extends SelectorModel {
  startContainer: string;
  startOffset: number;
  endContainer: string;
  endOffset: number;
}

type SerializeOptions = {
  ignoreSelector?: string
}

export type AnchoringAnchorType = typeof FragmentAnchor | typeof TextPositionAnchor | typeof TextQuoteAnchor | typeof RangeAnchor;

export function anchoringAnchorForType(root: Element, range: Range | AnnotatorRange, type: AnchoringAnchorType): AnchoringAnchor {
  switch (type) {
    case RangeAnchor:
      return RangeAnchor.fromRange(root, range as AnnotatorRange);
    case TextPositionAnchor:
      return TextPositionAnchor.fromRange(root, range as Range);         
    case TextQuoteAnchor:
      return TextQuoteAnchor.fromRange(root, range as Range);
    default:
      return FragmentAnchor.fromRange(root, range as Range);
  }
}

export type AnchoringAnchor = FragmentAnchor | RangeAnchor | TextPositionAnchor | TextQuoteAnchor;

export type AnchoringSelector = domAnchorFragment.FragmentSelector 
  | RangeSelector 
  | domAnchorTextPosition.TextPositionSelector 
  | domAnchorTextQuote.TextQuoteSelector;

/**
 * Wrapper class for domAnchorFragment
 */
export class FragmentAnchor {
  private domAnchor: any;

  /**
   * Creates an instance of FragmentAnchor
   * @param root - Root element of the anchor.
   * @param id - Id of the root element.
   * @param domAnchor - Optional. If set, root and id MUST BE null.
  */
  constructor(root: Element | null, id: string | null, domAnchor?: any) {
    if (!domAnchor) {
      this.domAnchor = new domAnchorFragment.FragmentAnchor(root || undefined, id || undefined);
    } else {
      this.domAnchor = domAnchor;
    }
  }

  get id(): string {
    return this.domAnchor.id;
  }

  get root(): Element {
    return this.domAnchor.root;
  }

  static fromRange(root: Element, range: Range): FragmentAnchor {
    return new FragmentAnchor(null, null, domAnchorFragment.fromRange(root, range)); 
  }

  static fromSelector(root: Element): FragmentAnchor {
    return new FragmentAnchor(null, null, domAnchorFragment.fromSelector(root));
  }

  toRange(): Range {
    return this.domAnchor.toRange();
  }

  toSelector(): domAnchorFragment.FragmentSelector {
    return this.domAnchor.toSelector();
  }
}

/**
 * class:: RangeAnchor(range)
 *
 * This anchor type represents a DOM Range.
 *
 * :param Range range: A range describing the anchor.
 */
export class RangeAnchor {
  readonly range: INormalizedRange | null;

  constructor(private root: Element, range: AnnotatorRange) {
    if (!range || !root) missingParameter('range');
    const xPathRange = sniff(range) as AnnotatorRange;
    this.range = xPathRange.normalize(this.root);
  }

  static fromRange(root: Element, range: AnnotatorRange): RangeAnchor {
    return new RangeAnchor(root, range);
  }

  // Create and anchor using the saved Range selector.
  static fromSelector(root: Element, selector: RangeSelector): RangeAnchor {
    const data: SerializedRangeModel = {
      start: selector.startContainer,
      startOffset: selector.startOffset,
      end: selector.endContainer,
      endOffset: selector.endOffset,
    };
    const range = new SerializedRange(data);
    return new RangeAnchor(root, range);
  }

  toRange(): Range {
    return this.range!.toRange();
  }

  toSelector(options?: SerializeOptions): RangeSelector {
    options = options || {};
    const range = this.range!.serialize(this.root, options.ignoreSelector);
    return {
      type: 'RangeSelector',
      startContainer: range.start,
      startOffset: range.startOffset,
      endContainer: range.end,
      endOffset: range.endOffset,
    };
  }
}

/**
 * Converts between TextPositionSelector selectors and Range objects.
 */
export class TextPositionAnchor {
  constructor(private root: Element, private start: number, private end: number) {}

  static fromRange(root: Element, range: Range): TextPositionAnchor {
    const selector = domAnchorTextPosition.fromRange(root, range);
    return TextPositionAnchor.fromSelector(root, selector);
  }

  static fromSelector(root: Element, selector: domAnchorTextPosition.TextPositionSelector): TextPositionAnchor {
    return new TextPositionAnchor(root, selector.start, selector.end);
  }

  toSelector(): domAnchorTextPosition.TextPositionSelector {
    return {
      type: 'TextPositionSelector',
      start: this.start,
      end: this.end,
    };
  }

  toRange(): Range {
    return domAnchorTextPosition.toRange(this.root, {start: this.start, end: this.end});
  }
}


interface TextQuoteAnchorContext {
  prefix?: string;
  suffix?: string;
}

/**
 * Converts between TextQuoteSelector selectors and Range objects.
 */
export class TextQuoteAnchor {
  private context: TextQuoteAnchorContext;

  constructor(private root: Element, private exact: string, context?: TextQuoteAnchorContext) {
    this.root = root;
    this.exact = exact;
    this.context = context || {};
  }

  static fromRange(root: Element, range: Range): TextQuoteAnchor {
    const selector = domAnchorTextQuote.fromRange(root, range);
    return TextQuoteAnchor.fromSelector(root, selector);
  }

  static fromSelector(root: Element, selector: domAnchorTextQuote.TextQuoteSelector): TextQuoteAnchor {
    const {prefix, suffix} = selector;
    return new TextQuoteAnchor(root, selector.exact, {prefix, suffix});
  }

  toSelector(): domAnchorTextQuote.TextQuoteSelector {
    return {
      type: 'TextQuoteSelector',
      exact: this.exact,
      prefix: this.context.prefix,
      suffix: this.context.suffix,
    };
  }

  toRange(options: any): Range {
    options = options || {};
    const range = domAnchorTextQuote.toRange(this.root, this.toSelector(), options);
    if (range === null) {
      throw new Error('Quote not found');
    }
    return range;
  }

  toPositionAnchor(options: any): TextPositionAnchor { 
    options = options || {};
    const anchor = domAnchorTextQuote.toTextPosition(this.root, this.toSelector(), options);
    if (anchor === null) {
      throw new Error('Quote not found');
    }
    return new TextPositionAnchor(this.root, anchor.start, anchor.end);
  }
}
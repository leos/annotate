import * as html from './html';
import { configFrom } from '../../../shared/config';
import { HypothesisConfigWindow } from '../../../shared/models/config.model';
import { AnchoringAnchorType } from './types';

const config = configFrom(window as HypothesisConfigWindow);

import {
  LeosAnchor,
  LeosSelector,
  FragmentAnchor,
  RangeAnchor,
  TextPositionAnchor,
  TextQuoteAnchor,
} from './leos-types';

import { AnchoringAnchor, SelectorModel, anchoringAnchorForType } from './types';
import { AnnotatorAnchorSelector } from '../models/annotator';
import { AnnotatorRange } from './range';

function querySelector(type: any, root: Element, selector: SelectorModel, options: any): Promise<Range> {
  return new Promise((resolve, reject) => {
    try {
      const anchor = type.fromSelector(root, selector, options);
      const range: Range = anchor.toRange(options);
      return resolve(range);
    } catch (error) {
      return reject(error);
    }
  });
};


export function anchor(root: Element, selectors: AnnotatorAnchorSelector[], options: any): Promise<any> {
  if (options == null) { options = {}; }
  let leos: LeosSelector | null = null;

  const getLeosSelector = (): LeosSelector | null => {
    for (let selector of (selectors || [])) {
      switch (selector.type) {
      case 'LeosSelector':
        return selector as LeosSelector;
      }
    }
    return null;
  }
  leos = getLeosSelector();

  if (leos != null) {
    return querySelector(LeosAnchor, root, leos, options);
  } else {
    return html.anchor(root, selectors, options);
  }
};

export function describe(root: Element, range: Range | AnnotatorRange, options: any): AnnotatorAnchorSelector[] {
  if (options == null) { options = {}; }
  const types = [LeosAnchor, FragmentAnchor, RangeAnchor, TextPositionAnchor, TextQuoteAnchor];

  const selectors = [];
  for (let type of types) {
    try {
      let anchor = (type == LeosAnchor) ? LeosAnchor.fromRange(root, range as Range, options) : anchoringAnchorForType(root, range, type as AnchoringAnchorType);
      selectors.push(anchor.toSelector(options));
    } catch (error) {
      continue;
    }
  }

  if (config.ignoredTags == null) return selectors;
  //LEOS-2789 replace wrappers tags by "//" in xpaths
  const tags = config.ignoredTags.join('|');
  const matchTags = new RegExp('(' + tags + ')(\\[\\d+\\])?', 'g');
  const matchSlashes = new RegExp('\\/(\\/)+\\/', 'g');
  selectors.filter(s => s.type === 'RangeSelector').forEach(function(selector: any) {
    selector.endContainer = selector.endContainer.replace(matchTags, '').replace(matchSlashes, '//');
    selector.startContainer = selector.startContainer.replace(matchTags, '').replace(matchSlashes, '//');
  });
  return selectors;
}
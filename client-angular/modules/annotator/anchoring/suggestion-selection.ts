/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import $ from '../imports/jquery';
import RulesEngine from '../utils/rules-engine';
import { Suggestion } from '../../../shared/models/annotation.model';
import { AnnotatorAnchor } from '../models/annotator';

/**
 * Extends string indexOf functionnality
 * @param src Source string
 * @param pattern Pattern to look for
 * @param n - 
 * @returns Index if found
 */
function nthIndexOf(src: string, pattern: string, n: number): number {
  let i = -1;
  while (n-- && (i++ < src.length)) {
    i = src.indexOf(pattern, i);
    if (i < 0) {
      break;
    }
  }
  return i;
}

function escape (str: string): string {
  return str.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/])/g, '\\$1');
}

const HIGHLIGHT_TAG = 'hypothesis-highlight';

/**
 * Returns true if selection is done on an editable part
 * @param element: range common ancestor
 * @param editableSelector: attribute name which define if an element is editable or not
 * @return: returns true if selection is done on an editable part.
 */
function _isEditable (element: Element, editableSelector: string): boolean {
    const editableSelectors = editableSelector.split(',');

    for (let editIndex in editableSelectors) {
      var editableElement;
      const editSelector = editableSelectors[editIndex];
      if (editSelector === '') {
        return true;
      }

      try {
        editableElement = $(element).closest(editSelector);
      } catch (error) {
        console.log('Error: ' + error);
      }

      if ((editableElement != null) && (editableElement.length > 0)) {
        return true;
      }
    }
    return false;
};

/**
 * Returns true if selection for suggestion is allowed - checks the parent's elements
 * @param element: range common ancestor
 * @param notAllowedSuggestSelector: attribute containing JQuery selector of not allowed parent elements
 * @return: returns true if selection is allowed.
 */
function _isAllowed(element: Element, notAllowedSuggestSelector: string): boolean {
    let notAllowedSuggestElement: JQuery<Element>;
    let closestEditableElement: JQuery<Element>;
    try {
      notAllowedSuggestElement = $(element).closest(notAllowedSuggestSelector);  //grandparent
      closestEditableElement = $(element).closest("[leos\\:editable=true]");      //parent
    } catch (error) {
      return true;
    }

    if ((notAllowedSuggestElement != null && notAllowedSuggestElement.length > 0)
        //only if any of the ancients is blocked, we check if someone closer to us in the hierarchy is allowed
        && !notAllowedSuggestElement[0].contains(closestEditableElement[0])) {
      return false;
    } else {
      return true;
    }
};

/**
 * It gets the closest parent of @param element with a non null/empty id
 * @param element
 * @return: returns the closest parent of @param element with a non empty id or returns null if not found.
 */
function _getClosestAncestorWithId(element: Element): Element | null {
    if ((element != null) && (element.id != null) && (element.id !== '')) {
      return element;
    } else if (element.parentElement != null) {
      return _getClosestAncestorWithId(element.parentElement);
    } else {
      return null;
    }
};

export interface SuggestionSelector {
  origText: string;
  elementId: string;
  startOffset: number;
  endOffset: number;
  parentElementId: string;
  completeOuterHTML: string;
}

export interface ILeosSuggestionSelection {
    /**
     * Extracts from selection the content in Html removing allowed elements tags
     * @param selection's range document fragment
     * @return: returns from selection the content in Html removing allowed elements tags.
     */
    extractSuggestionTextFromSelection(documentFragment: DocumentFragment): string | null;

    /**
     * While accepting a suggestion (@param annot) it generates appropriated array of selectors to send to LEOS
     * @param suggestion: suggestion itself
     * @param anchors: all anchors of the document
     * @return: returns array of selectors, one selector contains
     *          {origText: this.textContent, elementId: elementId, startOffset: startOffset, endOffset: endOffset, parentElementId: parentElementId}.
     */
    getSuggestionSelectors(suggestion: Suggestion, anchors: AnnotatorAnchor[]): SuggestionSelector | null;

    /**
     * Returns true if selection text contains only text or allowed elements
     * @param selection's range
     * @return: returns true if selection text contains only text or allowed elements.
     */
    isSelectionAllowedForSuggestion(documentFragment: DocumentFragment, rangeCommonAncestorContainer: Element): boolean;
}

export default class LeosSuggestionSelection implements ILeosSuggestionSelection {
    constructor(private allowedSelectorTags: string, 
      private editableSelector: string, 
      private notAllowedSuggestSelector: string) {
    }

    /**
     * Returns true if selection text contains only text or allowed elements
     * @param selection's range
     * @return: returns true if selection text contains only text or allowed elements.
     */
    isSelectionAllowedForSuggestion(documentFragment: DocumentFragment, rangeCommonAncestorContainer: Element): boolean {
      const self = this;
      const treeWalker = document.createTreeWalker(documentFragment, NodeFilter.SHOW_ELEMENT, { acceptNode(node: Element) {
        if ((node.nodeType === Node.TEXT_NODE) || ((node.nodeType === Node.ELEMENT_NODE) && (node.matches(self.allowedSelectorTags) || node.matches(HIGHLIGHT_TAG)))) {
          return NodeFilter.FILTER_SKIP;
        } else {
          return NodeFilter.FILTER_ACCEPT;
        }
      },
      });
      return ((treeWalker.nextNode() === null) && _isEditable(rangeCommonAncestorContainer, self.editableSelector) && _isAllowed(rangeCommonAncestorContainer, self.notAllowedSuggestSelector));
    }

    /**
     * Extracts from selection the content in Html removing allowed elements tags
     * @param selection's range document fragment
     * @return: returns from selection the content in Html removing allowed elements tags.
     */
    extractSuggestionTextFromSelection(documentFragment: DocumentFragment): string | null {
      const self = this;
      const rulesEngine = new RulesEngine();
      const tmpDiv = document.createElement('div');
      tmpDiv.appendChild(documentFragment);
      const selectionsRules = { element: {
        [self.allowedSelectorTags]() {
          (this['insertAdjacentHTML'] as any)('afterend', this['innerHTML']);
          (this['parentNode'] as any).removeChild(this);
        },
        [HIGHLIGHT_TAG]() {
          (this['insertAdjacentHTML'] as any)('afterend', this['innerHTML']);
          (this['parentNode'] as any).removeChild(this);
        },
      },
      };

      rulesEngine.processElement(selectionsRules, tmpDiv);
      return tmpDiv.textContent;
    }

    // FIXME temporary fix we consider that only one highlight will be used here
    /**
     * While accepting a suggestion (@param annot) it generates appropriated array of selectors to send to LEOS
     * @param suggestion: suggestion itself
     * @param anchors: all anchors of the document
     * @return: returns array of selectors, one selector contains
     *          {origText: this.textContent, elementId: elementId, startOffset: startOffset, endOffset: endOffset, parentElementId: parentElementId}.
     */
    getSuggestionSelectors(suggestion: Suggestion, anchors: AnnotatorAnchor[]): SuggestionSelector | null {
      const anchor = anchors.find((a: AnnotatorAnchor) => {
        return a.annotation?.$tag === suggestion.$tag;
      });
      if (anchor != null) {
        let highlightedText = '';
        let completeOuterHTML = '';
        if (anchor.highlights != null) {
          let grandParentElement: Element | null = null;
          anchor.highlights.every(h => {
            highlightedText += h.textContent;
            return completeOuterHTML += h.outerHTML;
          });
          const parentElement = _getClosestAncestorWithId(anchor.range?.commonAncestorContainer as Element); //Common Ancestor could a highlight or a wrapper, find appropriate one
          const childHighlights = parentElement!.querySelectorAll(HIGHLIGHT_TAG);
          const hIndex = Array.prototype.indexOf.call(childHighlights, anchor.highlights[0]);
          const eltContent = parentElement!.innerHTML;
          const startOffset = nthIndexOf(eltContent, `<${HIGHLIGHT_TAG}`, hIndex + 1); // Takes only the first one: temporary fix because until now only one text node
          // could highlighted
          const endOffset = startOffset + highlightedText.length;
          const elementId = parentElement?.id;
          if (parentElement?.parentElement != null) {
            grandParentElement = _getClosestAncestorWithId(parentElement.parentElement);
          }
          return {
            origText: highlightedText,
            elementId: elementId || '',
            startOffset,
            endOffset,
            parentElementId: grandParentElement?.id || '',
            completeOuterHTML,
          };
        } else {
          return null;
        }
      } else {
        return null;
      }
    }
}

import * as ref from './types';
import * as domAnchorTextQuote from '../imports/dom-anchor-text-quote';
import * as domAnchorTextPosition from '../imports/dom-anchor-text-position';
import { configFrom } from '../../../shared/config';
import { HypothesisConfigWindow } from '../../../shared/models/config.model';

const config = configFrom(window as HypothesisConfigWindow);
export const RangeAnchor = ref.RangeAnchor;
export const FragmentAnchor = ref.FragmentAnchor;
export const TextPositionAnchor = ref.TextPositionAnchor;
export const TextQuoteAnchor = ref.TextQuoteAnchor;

function _getRootElement(root: Element, id: string): Element | null {
  if ((root == null) && (id == null)) {
    throw new Error('Element not found');
  }
  try {
    let rootElement = root.querySelector(`#${id}`);
    return rootElement ? rootElement : root.querySelector(`#revision-${id}`);
  } catch (error1) {
    throw new Error('Element not found');
  }
};

export interface LeosParent {
  id: string;
  tag: string;
}

function _getParents(root: Element, id: string): LeosParent[] {
  let parents: LeosParent[] = [];
  let element = _getRootElement(root, id);
  while ((element = element!.parentNode as Element) && (element.tagName.toLowerCase() !== config.leosDocumentRootNode)) {
    if (element.id) {
      parents.push({id : element.id, tag : element.tagName.toLowerCase()});
    }
  }
  return parents;
};

interface AnchorModel {
  exact?: string;
  prefix?: string;
  suffix?: string;
  start?: number;
  end?: number;
  id: string;
  root: Element;
  parents?: LeosParent[];
}

/**
 * Returns a new anchor object where superfluous whitespace
 * of @param anchor.prefix, @param anchor.exact and @param anchor.suffix
 * are removed. Additionally the @param anchor.start and @param anchor.end
 * indicees are adjusted accordingly.
 */

function _adjustAnchorWhitespace(anchor: AnchorModel): any {
    var adjustedAnchor, adjustedEndIndex, adjustedStartIndex, textLengthDifferenceStart, textLengthDifferenceTotal;

    if (!anchor.prefix || !anchor.exact || !anchor.suffix) {
        return anchor;
    }
    adjustedAnchor = new LeosAnchor(anchor != undefined ? anchor.root : null, 
      anchor != undefined ? anchor.id : '', 
      anchor != undefined ? anchor.parents || [] : [], 
      anchor.exact, 
      anchor.prefix, 
      anchor.suffix,
      anchor != undefined ? (anchor.start || 0) : 0, 
      anchor != undefined ? (anchor.end || 0) : 0);
    adjustedAnchor.prefix = _removeSuperfluousWhitespace(adjustedAnchor.prefix);
    adjustedAnchor.exact = _removeSuperfluousWhitespace(adjustedAnchor.exact);
    adjustedAnchor.suffix = _removeSuperfluousWhitespace(adjustedAnchor.suffix);

    textLengthDifferenceStart = anchor.prefix.length - adjustedAnchor.prefix.length;
    textLengthDifferenceTotal = textLengthDifferenceStart;
    textLengthDifferenceTotal += anchor.exact.length - adjustedAnchor.exact.length;
    adjustedStartIndex = Math.max(0, (anchor?.start || 0) - textLengthDifferenceStart);
    adjustedEndIndex = Math.max(0, (anchor?.end || 0)  - textLengthDifferenceTotal);
    adjustedAnchor.start = adjustedStartIndex;
    adjustedAnchor.end = adjustedEndIndex;

    return adjustedAnchor;
};


/**
 * Removes linefeeds ("\n") and replaces multiple successive space characters with a single one.
 * Note: it does not use "\s" for matching the space characters in order to preserve non-breaking spaces (char code: 160), which turned out to improve matching
 * accuracy.
 */
const _removeSuperfluousWhitespace = function(stringToClean: string): string {
  if (!stringToClean) {
    return stringToClean;
  }
  return stringToClean.replace(/[\n ]+/g, " ");
};

export interface LeosSelector extends ref.SelectorModel {
  id: string,
  parents: LeosParent[], 
  exact: string, 
  prefix: string, 
  suffix: string, 
  start: number, 
  end: number 
}

/**
* Converts between TextPositionSelector selectors and Range objects.
*/
export class LeosAnchor {
    readonly type: string = 'LeosAnchor';

    constructor(readonly root: Element | null, 
      readonly id: string, 
      readonly parents: LeosParent[], 
      public exact: string, 
      public prefix: string, 
      public suffix: string, 
      public start: number, 
      public end: number) {
    }

    static fromRange(root: Element, range: Range, options: any): LeosAnchor {
      var domAnchorPositionSelector, domAnchorQuoteSelector, fragmentSelector, selector;
      fragmentSelector = FragmentAnchor.fromRange(root, range);
      domAnchorQuoteSelector = domAnchorTextQuote.fromRange(_getRootElement(root, fragmentSelector.id) || undefined, range);
      domAnchorPositionSelector = domAnchorTextPosition.fromRange(_getRootElement(root, fragmentSelector.id) || undefined, range);
      selector = new LeosAnchor(root, fragmentSelector.id, _getParents(root, fragmentSelector.id), domAnchorQuoteSelector.exact || '', domAnchorQuoteSelector.prefix || '', domAnchorQuoteSelector.suffix || '', domAnchorPositionSelector.start, domAnchorPositionSelector.end);
      return LeosAnchor.fromSelector(root, selector);
    };

    static fromSelector(root: Element, selector: LeosSelector): LeosAnchor {
      return new LeosAnchor(root, selector.id, selector.parents, selector.exact, selector.prefix, selector.suffix, selector.start, selector.end);
    };

    toPositionAnchor(options: any): ref.TextPositionAnchor {
        if (options == null) {
            options = {};
        }
        const positionAnchorRoot: Element | null = _getRootElement(this.root!, this.id);
        const anchor = domAnchorTextQuote.toTextPosition(positionAnchorRoot || undefined, this.toSelector(), options);
        if (anchor === null) {
            throw new Error('Quote not found');
        }
        return new TextPositionAnchor(positionAnchorRoot!, anchor.start, anchor.end);
    };

    toRange(options: any): Range | undefined {
        var dummyTextNode, error, range, ref1, rootNode, whitespaceAdjustedAnchor;
        if (options == null) {
          options = {};
        }
        try {
            options.hint = this.toSelector().start;
            rootNode = _getRootElement(this.root!, this.id);
            dummyTextNode = document.createTextNode("");
            rootNode?.appendChild(dummyTextNode);
            whitespaceAdjustedAnchor = _adjustAnchorWhitespace(this as AnchorModel);
            range = domAnchorTextQuote.toRange(rootNode || undefined, this.toSelector(), options);
            if (range === null) {
              range = domAnchorTextQuote.toRange(rootNode || undefined, whitespaceAdjustedAnchor.toSelector(), options);
            }
            if (range === null) {
              range = domAnchorTextPosition.toRange(rootNode || undefined, this.toSelector());
            }
            if (range === null) {
              range = domAnchorTextPosition.toRange(rootNode || undefined, whitespaceAdjustedAnchor.toSelector());
            }
        } catch (error1: any) {
            error = error1;
            if (error.message.indexOf("Failed to execute 'setEnd' on 'Range'") !== -1) {
              if ((this.start == null) || (this.end == null) || (this.start === (ref1 = this.end) && ref1 === 0)) {
                throw new Error('Range creation failed');
              }
              this.end -= 1;
              range = domAnchorTextPosition.toRange(_getRootElement(this.root!, this.id) || undefined, this.toSelector());
            }
        }
        if (range === null) {
            throw new Error('Range creation failed');
        }
        return range;
    };

    toSelector(): LeosSelector {
      return {
        type: 'LeosSelector',
        id: this.id,
        parents: this.parents,
        exact: this.exact,
        prefix: this.prefix,
        suffix: this.suffix,
        start: this.start,
        end: this.end
      };
    };
}
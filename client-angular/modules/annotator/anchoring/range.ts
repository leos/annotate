// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// This is a modified copy of
// https://github.com/openannotation/annotator/blob/v1.2.x/src/range.coffee

import $ from '../imports/jquery';

import * as Util from './util';

export class RangeError extends Error {
  readonly type: string;
  readonly parent: any;

  constructor(type: string, message: string, parent: any=null) {
    // this cannot be used before calling super, cannot use super(@type, @message,...)
    // see http://coffeescript.org/#breaking-changes-classes
    super(message);
    this.type = type;
    this.parent = parent;
  }
};

export interface IAnnotatorRange {
  /**
   * Normalize works around the fact that browsers don't generate
   * ranges/selections in a consistent manner. Some (Safari) will create
   * ranges that have (say) a textNode startContainer and elementNode
   * endContainer. Others (Firefox) seem to only ever generate
   * textNode/textNode or elementNode/elementNode pairs.
   *
   * @returns An instance of NormalizedRange
   */ 
  normalize(root: Element | null) : INormalizedRange | null;

  /** 
   * Creates a range suitable for storage.
   *
   * @param root           - A root Element from which to anchor the serialisation.
   * @param ignoreSelector - A selector String of elements to ignore. For example
   *                  elements injected by the annotator.
   *
   * @return An instance of SerializedRange.
   */
  serialize(root: Element, ignoreSelector?: string): ISerializedRange;
}

/**
 * Creates a wrapper around a range object obtained from a DOMSelection.
 */
export interface IBrowserRange extends IAnnotatorRange {
  readonly commonAncestorContainer?: Node;
  readonly startContainer?: Node;
  readonly startOffset?: number;
  readonly endContainer?: Node;
  readonly endOffset?: number;
}

export interface BrowserRangeModel {
  commonAncestorContainer?: Node;
  startContainer?: Node;
  startOffset?: number;
  endContainer?: Node;
  endOffset?: number;
}

/** 
 * A normalised range is most commonly used throughout the annotator.
 * its the result of a deserialised SerializedRange or a BrowserRange with
 * out browser inconsistencies.
 */ 
export interface INormalizedRange extends IAnnotatorRange {
  readonly commonAncestor: Element;
  readonly start: Node;
  readonly end: Node;

  /**
   * Limits the nodes within the NormalizedRange to those contained
   * withing the bounds parameter. It returns an updated range with all
   * properties updated. NOTE: Method returns null if all nodes fall outside
   * of the bounds.
   *
   * @param {Element} bounds - An Element to limit the range to.
   *
   * @return Updated self or null.
   */
  limit(bounds: Element): INormalizedRange | null;

  /**
   * Creates a concatenated String of the contents of all the text nodes
   * within the range.
   *
   * @return a String.
   */
  text(): string;

  /**
   * Fetches only the text nodes within th range.
   *
   * @return An Array of TextNode instances.
   */
  textNodes(): HTMLElement[];

  /** 
   * Converts the Normalized range to a native browser range.
   *
   * See: https://developer.mozilla.org/en/DOM/range
   *
   * Examples
   *
   *   selection = window.getSelection()
   *   selection.removeAllRanges()
   *   selection.addRange(normedRange.toRange())
   *
   * @return a Range object.
   */
  toRange(): Range;
}

export interface NormalizedRangeModel {
  commonAncestor?: Element;
  start?: Node;
  end?: Node;
}

export interface ISerializedRange extends IAnnotatorRange {
  readonly start: string;
  readonly startOffset: number;
  readonly end: string;
  readonly endOffset: number;

  /**
   * @return The range as an Object literal.
   * */
  toObject(): SerializedRangeModel;
}

export interface SerializedRangeModel {
  start?: string;
  startOffset?: number;
  end?: string;
  endOffset?: number;
}

export type AnnotatorRange = IBrowserRange | INormalizedRange | ISerializedRange;

/** Creates a wrapper around a range object obtained from a DOMSelection. */
export class BrowserRange implements IBrowserRange {
  readonly commonAncestorContainer: Node | undefined;
  readonly startContainer: Node | undefined;
  readonly startOffset: number | undefined;
  readonly endContainer: Node | undefined;
  readonly endOffset: number | undefined;
  private tainted: boolean | undefined;

  /**
   *  Public: Creates an instance of BrowserRange.
   *
   * object - A range object obtained via DOMSelection#getRangeAt().
   *
   * Examples
   *
   *   selection = window.getSelection()
   *   range = new Range.BrowserRange(selection.getRangeAt(0))
   *
   * Returns an instance of BrowserRange.
   */ 
  constructor(obj: BrowserRangeModel) {
    this.commonAncestorContainer = obj.commonAncestorContainer;
    this.startContainer          = obj.startContainer;
    this.startOffset             = obj.startOffset;
    this.endContainer            = obj.endContainer;
    this.endOffset               = obj.endOffset;
    this.tainted = false;
  }

  normalize(root: Element | null): INormalizedRange | null {
    if (this.tainted) {
      console.error('You may only call normalize() once on a BrowserRange!');
      return null;
    } else {
      this.tainted = true;
    }

    const r: {
      start: Node | null,
      startOffset: number | null,
      end: Node | null,
      endOffset: number | null
    } = { start: null, startOffset: null, end: null, endOffset: null};

    // Look at the start
    if (this.startContainer?.nodeType === Node.ELEMENT_NODE) {
      // We are dealing with element nodes
      if (this.startOffset! < this.startContainer.childNodes.length) {
        r.start = Util.getFirstTextNodeNotBefore(this.startContainer.childNodes[this.startOffset!]);
      } else {
        r.start = Util.getFirstTextNodeNotBefore(this.startContainer);
      }
      r.startOffset = 0;
    } else {
      // We are dealing with simple text nodes
      r.start = this.startContainer || null;
      r.startOffset = this.startOffset || null;
    }

    // Look at the end
    if (this.endContainer?.nodeType === Node.ELEMENT_NODE) {
      // Get specified node.
      let node: Node | null = this.endContainer.childNodes[this.endOffset!];

      if (node != null) { // Does that node exist?
        // Look for a text node either at the immediate beginning of node
        let n: Node | null = node;
        while ((n != null) && (n.nodeType !== Node.TEXT_NODE)) {
          n = n.firstChild;
        }
        if (n != null) { // Did we find a text node at the start of this element?
          r.end = n;
          r.endOffset = 0;
        }
      }

      if (r.end == null) {
        // We need to find a text node in the previous sibling of the node at the
        // given offset, if one exists, or in the previous sibling of its container.
        if (this.endOffset) {
          node = this.endContainer.childNodes[this.endOffset - 1];
        } else {
          node = this.endContainer.previousSibling;
        }
        r.end = Util.getLastTextNodeUpTo(node!);
        r.endOffset = r.end?.nodeValue?.length || 0;
      }

    } else { // We are dealing with simple text nodes
      r.end = this.endContainer || null;
      r.endOffset = this.endOffset || null;
    }

    // We have collected the initial data.

    // Now let's start to slice & dice the text elements!
    const nr: {
      commonAncestor: Node | null,
      start: Node | null,
      end: Node | null
    } = {commonAncestor: null, start: null, end: null};

    if ((r.startOffset || 0) > 0) {
      // Do we really have to cut?
      if (!r.start!.nextSibling || ((r.start?.nodeValue?.length || 0) > r.startOffset!)) {
        // Yes. Cut.
        nr.start = (r.start as any)?.splitText(r.startOffset!) as Node;
      } else {
        // Avoid splitting off zero-length pieces.
        nr.start = r.start?.nextSibling || null;
      }
    } else {
      nr.start = r.start;
    }

    // is the whole selection inside one text element ?
    if (r.start === r.end) {
      if (nr.start?.nodeValue && (nr.start.nodeValue.length > ((r.endOffset || 0) - (r.startOffset || 0)))) {
        (nr.start as any).splitText((r.endOffset || 0) - (r.startOffset || 0));
      }
      nr.end = nr.start;
    } else { // no, the end of the selection is in a separate text element
      // does the end need to be cut?
      if ((r.end?.nodeValue?.length || 0) > (r.endOffset || 0)) {
        (r.end as any).splitText(r.endOffset);
      }
      nr.end = r.end;
    }

    // Make sure the common ancestor is an element node.
    nr.commonAncestor = this.commonAncestorContainer || null;
    while (nr.commonAncestor && (nr.commonAncestor.nodeType !== Node.ELEMENT_NODE)) {
      nr.commonAncestor = nr.commonAncestor.parentNode;
    }

    return new NormalizedRange(nr as NormalizedRangeModel);
  }

  serialize(root: Element, ignoreSelector?: string): ISerializedRange {
    let range: INormalizedRange = this.normalize(root)!;
    return range.serialize(root, ignoreSelector);
  }
};

/**  
 * A normalised range is most commonly used throughout the annotator.
 * its the result of a deserialised SerializedRange or a BrowserRange with
 * out browser inconsistencies.
 */
export class NormalizedRange implements INormalizedRange {
  private _commonAncestor: Element | undefined;
  private _start: Element;
  private _end: Element;

  // Public: Creates an instance of a NormalizedRange.
  //
  // This is usually created by calling the .normalize() method on one of the
  // other Range classes rather than manually.
  //
  // obj - An Object literal. Should have the following properties.
  //       commonAncestor: A Element that encompasses both the start and end nodes
  //       start:          The first TextNode in the range.
  //       end             The last TextNode in the range.
  //
  // Returns an instance of NormalizedRange.
  constructor(obj: NormalizedRangeModel) {
    this._commonAncestor = obj.commonAncestor;
    this._start          = obj.start as Element;
    this._end            = obj.end as Element;
  }

  get commonAncestor(): Element {
    return this._commonAncestor!;
  }

  get start(): Node {
    return this._start;
  }

  get end(): Node {
    return this._end;
  }

  normalize(root: Element): INormalizedRange {
    return this;
  }

  limit(bounds: Element): INormalizedRange | null {
    const nodes = $.grep(this.textNodes(), (node: any) => (node.parentNode === bounds) || $.contains(bounds, node.parentNode));

    if (!nodes.length) { return null; }

    this._start = nodes[0];
    this._end   = nodes[nodes.length - 1];

    const startParents = $(this.start).parents();
    for (let parent of $(this.end).parents()) {
      if (startParents.index(parent) !== -1) {
        this._commonAncestor = parent;
        break;
      }
    }
    return this;
  }

  serialize(root: Element, ignoreSelector?: string): ISerializedRange {

    const serialization = function(node: Element, isEnd: boolean): (string | number)[] {
      let origParent: JQuery<HTMLElement>;
      if (ignoreSelector) {
        origParent = $(node).parents(`:not(${ignoreSelector})`).eq(0);
      } else {
        origParent = $(node).parent() as JQuery<HTMLElement>;
      }

      const xpath = Util.xpathFromNode(origParent[0], root)[0];
      const textNodes = Util.getTextNodes(origParent);

      // Calculate real offset as the combined length of all the
      // preceding textNode siblings. We include the length of the
      // node if it's the end node.
      const nodes = textNodes.slice(0, textNodes.index(node));
      let offset = 0;
      for (let n of nodes) {
        offset +=( n.nodeValue?.length || 0);
      }

      if (isEnd) { return [xpath, offset + (node.nodeValue?.length || 0)]; } else { return [xpath, offset]; }
    };

    const start = serialization(this._start, false);
    const end   = serialization(this._end, true);

    return new SerializedRange({
      // XPath strings
      start: start[0] as string,
      end: end[0] as string,
      // Character offsets (integer)
      startOffset: start[1] as number,
      endOffset: end[1] as number,
    });
  }

  text(): string {
    return (this.textNodes().map((node: any) =>
      node.nodeValue)
    ).join('');
  }

  textNodes(): HTMLElement[] {
    const textNodes = Util.getTextNodes($(this.commonAncestor) as JQuery<HTMLElement>);
    const [start, end] = [textNodes.index(this._start), textNodes.index(this._end)];
    // Return the textNodes that fall between the start and end indexes.
    return $.makeArray(textNodes.slice(start, +end + 1 || undefined));
  }

  toRange(): Range {
    const range = document.createRange();
    range.setStartBefore(this.start);
    range.setEndAfter(this.end);
    return range;
  }
};

// Public: A range suitable for storing in local storage or serializing to JSON.
export class SerializedRange implements ISerializedRange {
  readonly start: string;
  readonly startOffset: number;
  readonly end: string;
  readonly endOffset: number;

  /** 
   * Creates a SerializedRange
   *
   * @param obj - The stored object. It should have the following properties.
   *       start:       An xpath to the Element containing the first TextNode
   *                    relative to the root Element.
   *       startOffset: The offset to the start of the selection from obj.start.
   *       end:         An xpath to the Element containing the last TextNode
   *                    relative to the root Element.
   *       startOffset: The offset to the end of the selection from obj.end.
   */ 
  constructor(obj: SerializedRangeModel) {
    this.start       = obj.start || '';
    this.startOffset = obj.startOffset || 0;
    this.end         = obj.end || '';
    this.endOffset   = obj.endOffset || 0;
  }

  normalize(root: Element): INormalizedRange | null {
    const range: any = {startContainer:null, endContainer: null, commonAncestorContainer: null};
    const self: any = this;

    for (let p of ['start', 'end']) {
      var node;
      try {

        node = nodeFromXPath(self[p], root);
      } catch (e) {
        throw new RangeError(p, `Error while finding ${p} node: ${self[p]}: ` + e, e);
      }

      if (!node) {
        throw new RangeError(p, `Couldn't find ${p} node: ${self[p]}`);
      }

      // Unfortunately, we *can't* guarantee only one textNode per
      // elementNode, so we have to walk along the element's textNodes until
      // the combined length of the textNodes to that point exceeds or
      // matches the value of the offset.
      let length = 0;
      let targetOffset = self[p + 'Offset'];

      // Range excludes its endpoint because it describes the boundary position.
      // Target the string index of the last character inside the range.
      if (p === 'end') { targetOffset--; }

      for (let tn of Util.getTextNodes($(node))) {
        if ((length + (tn.nodeValue?.length || 0)) > targetOffset) {
          range[p + 'Container'] = tn;
          range[p + 'Offset'] = self[p + 'Offset'] - length;
          break;
        } else {
          length += (tn.nodeValue?.length || 0);
        }
      }

      // If we fall off the end of the for loop without having set
      // 'startOffset'/'endOffset', the element has shorter content than when
      // we annotated, so throw an error:
      if ((range[p + 'Offset'] == null)) {
        throw new RangeError(`${p}offset`, `Couldn't find offset ${self[p + 'Offset']} in element ${self[p]}`);
      }
    }

    // Here's an elegant next step...
    //
    //   range.commonAncestorContainer = $(range.startContainer).parents().has(range.endContainer)[0]
    //
    // ...but unfortunately Node.contains() is broken in Safari 5.1.5 (7534.55.3)
    // and presumably other earlier versions of WebKit. In particular, in a
    // document like
    //
    //   <p>Hello</p>
    //
    // the code
    //
    //   p = document.getElementsByTagName('p')[0]
    //   p.contains(p.firstChild)
    //
    // returns `false`. Yay.
    //
    // So instead, we step through the parents from the bottom up and use
    // Node.compareDocumentPosition() to decide when to set the
    // commonAncestorContainer and bail out.

    const contains =
      (document.compareDocumentPosition == null) ?
        // IE
        (a: any, b: any) => a.contains(b)
        :
        // Everyone else
        (a: Node, b: Node) => a.compareDocumentPosition(b) & 16;

    $(range.startContainer).parents().each(() => {
      if (contains(self, range.endContainer)) {
        range.commonAncestorContainer = this;
        return false;
      }
      return;
    });

    return new BrowserRange(range).normalize(root);
  }

  serialize(root: Element, ignoreSelector?: string): ISerializedRange {
    let range: any = this.normalize(root);
    return range.serialize(root, ignoreSelector);
  }

  toObject(): SerializedRangeModel {
    return {
      start: this.start,
      startOffset: this.startOffset,
      end: this.end,
      endOffset: this.endOffset,
    };
  }
}

/** 
 * Determines the type of Range of the provided object and returns
 * a suitable Range instance.
 *
 * @param r - A range Object.
 *
 * Examples
 *
 *   selection = window.getSelection()
 *   Range.sniff(selection.getRangeAt(0))
 * 
 *   Returns a BrowserRange instance.
 *
 * @return A Range object or false.
 */
export function sniff(r: any): AnnotatorRange | boolean {
  if (r['commonAncestorContainer'] != null) {
      return new BrowserRange(r as BrowserRangeModel);
  }
  else if (typeof r['start'] === 'string') {
      return new SerializedRange(r as SerializedRangeModel);
  }
  else if ((r['start'] != null) && (typeof r['start'] === 'object')) {
      return new NormalizedRange(r as NormalizedRangeModel);
  }
  else {
      console.error('Could not sniff range type');
      return false;
  }
}


/** 
 * Finds an Element Node using an XPath relative to the document root.
 *
 * If the document is served as application/xhtml+xml it will try and resolve
 * any namespaces within the XPath.
 *
 * @param xpath - An XPath String to query.
 * @param root -
 *
 * Examples
 *
 *   node = Range.nodeFromXPath('/html/body/div/p[2]')
 *   if node
 *     # Do something with the node.
 *
 * @return The Node if found otherwise null.
 */
export function nodeFromXPath(xpath: string, root: Node): Node | null {
  if (root == null) { root = document; }
  const evaluateXPath = function(xp: string, nsResolver: XPathNSResolver | null) {
    try {
      return document.evaluate('.' + xp, root, nsResolver, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    } catch (exception) {
      // There are cases when the evaluation fails, because the
      // HTML documents contains nodes with invalid names,
      // for example tags with equal signs in them, or something like that.
      // In these cases, the XPath expressions will have these abominations,
      // too, and then they can not be evaluated.
      // In these cases, we get an XPathException, with error code 52.
      // See http://www.w3.org/TR/DOM-Level-3-XPath/xpath.html#XPathException
      // This does not necessarily make any sense, but this what we see
      // happening.
      console.log('XPath evaluation failed.');
      console.log('Trying fallback...');
      // We have a an 'evaluator' for the really simple expressions that
      // should work for the simple expressions we generate.
      return Util.nodeFromXPath(xp, root);
    }
  };

  if (!$.isXMLDoc(document.documentElement)) {
    return evaluateXPath(xpath, null);
  } else {
    // We're in an XML document, create a namespace resolver function to try
    // and resolve any namespaces in the current document.
    // https://developer.mozilla.org/en/DOM/document.createNSResolver
    let ownerDocument: any = document.ownerDocument;
    let documentElement = (!ownerDocument) ? document.documentElement : ownerDocument.documentElement;
    let customResolver = document.createNSResolver(documentElement);
    let node = evaluateXPath(xpath, customResolver);

    if (node != null) return node;
    // If the previous search failed to find a node then we must try to
    // provide a custom namespace resolver to take into account the default
    // namespace. We also prefix all node names with a custom xhtml namespace
    // eg. 'div' => 'xhtml:div'.
    xpath = (xpath.split('/').map((segment) =>
      segment && (segment.indexOf(':') === -1) ?
        segment.replace(/^([a-z]+)/, 'xhtml:$1')
        : segment)
    ).join('/');

    // Find the default document namespace.
    const namespace = document.lookupNamespaceURI(null);

    // Try and resolve the namespace, first seeing if it is an xhtml node
    // otherwise check the head attributes.
    const _customResolver = function(ns: string | null): string | null {
      return (ns === 'xhtml') ? namespace : document.documentElement.getAttribute(`xmlns:${ns}`);
    }
    return evaluateXPath(xpath, _customResolver);
  }
}
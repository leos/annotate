// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// This is a modified copy of
// https://github.com/openannotation/annotator/blob/v1.2.x/src/xpath.coffee

import $ from '../imports/jquery';

// A simple XPath evaluator using jQuery which can evaluate queries of
export function simpleXPathJQuery(element: Element, relativeRoot: Node): string[] {
  const self = $(element) as JQuery<HTMLElement>;
  const jq: JQuery<string> = self.map(function() {
    let path = '';
    let elem: HTMLElement = element as HTMLElement;

    while (((elem != null ? elem.nodeType : undefined) === Node.ELEMENT_NODE) && (elem !== relativeRoot)) {
      const tagName = elem.tagName.replace(':', '\\:');
      let idx = ($(elem.parentNode!).children(tagName).index(elem) + 1).toString();

      idx  = `[${idx}]`;
      path = '/' + elem.tagName.toLowerCase() + idx + path;
      elem = elem.parentNode as HTMLElement;
    }

    return path;
  });

  return jq.get();
};

// A simple XPath evaluator using only standard DOM methods which can
// evaluate queries of the form /tag[index]/tag[index].
export function simpleXPathPure(element: Element, relativeRoot: Node): string[] {

  const getPathSegment = function(node: Node): string {
    const name = getNodeName(node);
    const pos = getNodePosition(node);
    return `${name}[${pos}]`;
  };

  const rootNode: Node = relativeRoot;

  const getPathTo = function(node: Node | null): string {
    let xpath = '';
    while (node !== rootNode) {
      if (node == null) {
        throw new Error('Called getPathTo on a node which was not a descendant of @rootNode. ' + rootNode);
      }
      xpath = (getPathSegment(node)) + '/' + xpath;
      node = node.parentNode;
    }
    xpath = '/' + xpath;
    xpath = xpath.replace(/\/$/, '');
    return xpath;
  };

  const self = $(element) as JQuery<HTMLElement>
  const jq: JQuery<string> = self.map(function() {
    const el: HTMLElement = element as HTMLElement;
    const path = getPathTo(el);
    return path;
  });

  return jq.get();
};

export function findChild(node: Node, type: string, index: number): Node {
  if (!node.hasChildNodes()) {
    throw new Error('XPath error: node has no children!');
  }
  const childList: NodeList = node.childNodes;
  const children: Node[] = [];

  childList.forEach((child: Node) => {
    children.push(child);
  });

  let found = 0;
  for (const child of children) {
    const name: string = getNodeName(child);
    if (name === type) {
      found += 1;
      if (found === index) {
        return child;
      }
    }
  }
  throw new Error('XPath error: wanted child not found.');
};

// Get the node name for use in generating an xpath expression.
function getNodeName(node: Node): string {
  const nodeName = node.nodeName.toLowerCase();
  switch (nodeName) {
  case '#text': return 'text()';
  case '#comment': return 'comment()';
  case '#cdata-section': return 'cdata-section()';
  default: return nodeName;
  }
};

// Get the index of the node as it appears in its parent's child list
function getNodePosition(node: Node): number {
  let pos = 0;
  let tmp: Node | null = node;
  while (tmp) {
    if (tmp != null && tmp.nodeName === node.nodeName) {
      pos++;
    }
    tmp = tmp.previousSibling;
  }
  return pos;
};
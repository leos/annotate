/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
'use strict';

export interface EngineRules {
  //Properties provide funtcions that can be accessed by its key. Similar to Map<string, Function>.
  text?: any;
  element?: any;
  other?: any;
}

export interface IRulesEngine {
  processChildren(engineRules: EngineRules, element: Element, ...args: any[]): void;
  processElement(engineRules: EngineRules, element: Element, ...args: any[]): void;
}

export default class RulesEngine implements IRulesEngine {
  constructor() {
  }

  private _applyRules(engineRules: EngineRules, element: Element, ...args: any[]): void {
    const engineRule: string = this._getNodeExplicitType(element.nodeType);
    const rules: any = (engineRules as any)[engineRule];
    for (let rule in (rules || [])) {
      try {
        // using rule name as selector
        if ((rule !== '$') && element.matches(rule)) {
          rules[rule].apply(element, args);
        }
      } catch (e) {
        console.log(`Error in rule: ${rule.toString()} - ${e}`);
      }
      // calling default rule $
      rules['$']?.apply(element, args);
    }
  }

  private _getNodeExplicitType(nodeType: number): string {
    switch (nodeType) {
        case Node.TEXT_NODE:
          return 'text';
        case Node.ELEMENT_NODE:
          return 'element';
        default:
          return 'other';
    }
  }

  processElement(engineRules: EngineRules, element: Element, ...args: any[]): void {
    this.processChildren(engineRules, element, ...args);
    this._applyRules(engineRules, element, ...args);
  }

  processChildren(engineRules: EngineRules, element: Element, ...args: any[]): void {
    const self = this;
    const childNodes = Array.prototype.slice.call(element.childNodes);
    if (childNodes && (childNodes.length > 0)) {
      childNodes.forEach((function(node) {
        self.processElement(engineRules, node, ...args);
      }), this);
    }
  }
};
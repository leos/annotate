
import { configFrom } from '../../shared/config';
//import '../vendor/polyfills';

// Applications
import Guest from './guest';
import Sidebar from './sidebar';
import PdfSidebar from './pdf-sidebar';
import LeosSidebar from './leos-sidebar'; // LEOS Change

// Plugins
import Toolbar from './plugin/toolbar';
import LeosDocument from './plugin/leos-document';
import CrossFrame from './plugin/cross-frame';
import { HypothesisConfigWindow } from '../../shared/models/config.model';

type ExtendedWindow = Window & typeof globalThis & { __hypothesis_frame: boolean, PDFViewerApplication: any, wgxpath: any };

let windowExt = window as ExtendedWindow;

function initAnnotatorModule() {
  var config: any = configFrom(window as HypothesisConfigWindow);
  //TODO : have a better check to identify AKN LEOS Change
  var leos: Element | null = window.parent.document.querySelector(`${config.annotationContainer} ${config.leosDocumentRootNode}`) ??
      window.document.querySelector(`${config.annotationContainer} ${config.leosDocumentRootNode}`);

  const appLinkRel = config.sidebarAppId ? `sidebar-${config.sidebarAppId}` : 'sidebar';
  var appLinkEl: Element | null = window.parent.document.querySelector(`link[type="application/annotator+html"][rel="${appLinkRel}"]`) ??
      window.document.querySelector(`link[type="application/annotator+html"][rel="${appLinkRel}"]`);
  var Klass: any = leos ? LeosSidebar : Sidebar;

  if (windowExt.PDFViewerApplication) {
    Klass = PdfSidebar;
  }

  if (config.hasOwnProperty('constructor')) {
    Klass = config.constructor;
    delete config.constructor;
  }

  if (config.subFrameIdentifier) {
    Klass = Guest;

    // Other modules use this to detect if this
    // frame context belongs to hypothesis.
    // Needs to be a global property that's set.
    windowExt.__hypothesis_frame = true;
  }
  var pluginClasses: any = {
      // UI plugins
      Toolbar: Toolbar,
      // Document type plugins
      Document: LeosDocument, //LEOS Change
      // Cross-frame communication
      CrossFrame: CrossFrame,
  };

  config.pluginClasses = pluginClasses;

  const annotator = new Klass(document.body, config);
  appLinkEl?.addEventListener(config.sidebarAppId ? `destroy_${config.sidebarAppId}` : 'destroy', () => {
    appLinkEl?.parentElement?.removeChild(appLinkEl);
    annotator.destroy();
  });
}

initAnnotatorModule();
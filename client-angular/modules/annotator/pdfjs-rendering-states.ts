'use strict';

/**
 * Enum values for page rendering states (IRenderableView#renderingState)
 * in PDF.js. Taken from web/pdf_rendering_queue.js in the PDF.js library.
 *
 * Reproduced here because this enum is not exported consistently across
 * different versions of PDF.js
 */
const INITIAL = 0;
const RUNNING = 1;
const PAUSED = 2;
const FINISHED = 3;

export {
    INITIAL,
    RUNNING,
    PAUSED,
    FINISHED
};

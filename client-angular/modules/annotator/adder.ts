'use strict';

import * as clsNames from 'classnames';
import { IHostBridgeManager } from './host-bridge-manager';
const classnames: (...args: any) => string = (clsNames as any).default;

const template = `
<hypothesis-adder-toolbar class="annotator-adder js-adder">
  <hypothesis-adder-actions class="annotator-adder-actions">
    <button class="annotator-adder-actions__button js-annotate-btn">
      <div class="h-icon-annotate"></div>
      <span class="annotator-adder-actions__label" data-action="comment">Annotate</span>
    </button>
    <button class="annotator-adder-actions__button h-icon-highlight js-highlight-btn">
      <span class="annotator-adder-actions__label" data-action="highlight">Highlight</span>
    </button>
  </hypothesis-adder-actions>
</hypothesis-adder-toolbar>
`;

const leos_template = `
<button class="annotator-adder-actions__button js-annotate-btn">
  <div class="h-icon-annotate"></div>
  <span class="annotator-adder-actions__label" data-action="comment">Comment</span>
</button>
<button class="annotator-adder-actions__button js-suggestion-btn">
  <div class="h-icon-suggestion"></div>
  <span class="annotator-adder-actions__label" data-action="suggestion">Suggest</span>
</button>
<button class="annotator-adder-actions__button js-highlight-btn">
  <div class="h-icon-highlight"></div>
  <span class="annotator-adder-actions__label" data-action="highlight">Highlight</span>
</button>
`;

const ANNOTATE_BTN_CLASS = 'js-annotate-btn';
const ANNOTATE_BTN_SELECTOR = '.js-annotate-btn';

const HIGHLIGHT_BTN_SELECTOR = '.js-highlight-btn';

const SUGGESTION_BTN_SELECTOR = '.js-suggestion-btn';
const COMMENT_BTN_SELECTOR = '.js-annotate-btn';
var REMOVED_HIGHLIGHT_BTN: HTMLElement | null = null;



/**
 * @typedef AdderTarget
 * @prop {number} left - Offset from left edge of viewport.
 * @prop {number} top - Offset from top edge of viewport.
 * @prop {number} arrowDirection - Direction of the adder's arrow.
 */
export interface AdderTarget {
  left: number,
  top: number,
  arrowDirection: number,
}


export enum ArrowPointing {
  DOWN = 1,
  UP = 2
}

/**
 * Show the adder above the selection with an arrow pointing down at the
 * selected text.
 */
export const ARROW_POINTING_DOWN = ArrowPointing.DOWN;

/**
 * Show the adder above the selection with an arrow pointing up at the
 * selected text.
 */
export const ARROW_POINTING_UP = ArrowPointing.UP;

function toPx(pixels: number): string {
  return pixels.toString() + 'px';
}

var ARROW_HEIGHT = 10;

// The preferred gap between the end of the text selection and the adder's
// arrow position.
var ARROW_H_MARGIN = 20;

type ElementExtension = Element & { createShadowRoot?: () => ShadowRoot }

function attachShadow(element: ElementExtension): ShadowRoot | null {
  if (typeof (element) === 'undefined') {
    return null;
  } else if (element.attachShadow) {
    // Shadow DOM v1 (Chrome v53, Safari 10)
    return element.attachShadow({ mode: 'open' });
  } else if (element.createShadowRoot) {
    // Shadow DOM v0 (Chrome ~35-52)
    return element.createShadowRoot();
  } else {
    return null;
  }
}

/**
 * Return the closest ancestor of `el` which has been positioned.
 *
 * If no ancestor has been positioned, returns the root element.
 *
 * @param {Element} el
 * @return {Element}
 */
function nearestPositionedAncestor(el: Element): Element | null {
  var parentEl = el.parentElement;
  while (parentEl?.parentElement) {
    if (getComputedStyle(parentEl).position !== 'static') {
      break;
    }
    parentEl = parentEl.parentElement;
  }
  return parentEl;
}

/**
 * LEOS Change added this function
 * Checks if an element is scrollable.
 * See https://gist.github.com/jeffturcotte/1100144.
 */
function isScrollable(el: Element): boolean {
  return (el.scrollWidth > el.clientWidth) || (el.scrollHeight > el.clientHeight);
}

/**
 * Create the DOM structure for the Adder.
 *
 * Returns the root DOM node for the adder, which may be in a shadow tree.
 */
function createAdderDOM(container: ElementExtension): Element | null {
  var element: Element | null = null;

  // If the browser supports Shadow DOM, use it to isolate the adder
  // from the page's CSS
  //
  // See https://developers.google.com/web/fundamentals/primers/shadowdom/
  var shadowRoot = attachShadow(container);
  if (shadowRoot !== null) {
    shadowRoot.innerHTML = template;
    element = shadowRoot.querySelector('.js-adder');

    // Load stylesheets required by adder into shadow DOM element
    var adderStyles = Array.from(document.styleSheets).map(function (sheet) {
      return sheet.href;
    }).filter(function (url) {
      return (url || '').match(/(icomoon|annotator)\.css/);
    });

    // Stylesheet <link> elements are inert inside shadow roots [1]. Until
    // Shadow DOM implementations support external stylesheets [2], grab the
    // relevant CSS files from the current page and `@import` them.
    //
    // [1] http://stackoverflow.com/questions/27746590
    // [2] https://github.com/w3c/webcomponents/issues/530
    //
    // This will unfortunately break if the page blocks inline stylesheets via
    // CSP, but that appears to be rare and if this happens, the user will still
    // get a usable adder, albeit one that uses browser default styles for the
    // toolbar.
    var styleEl = document.createElement('style');
    styleEl.textContent = adderStyles.map(function (url) {
      return '@import "' + url + '";';
    }).join('\n');
    shadowRoot.appendChild(styleEl);
  } else if (container) {
    container.innerHTML = template;
    element = container.querySelector('.js-adder');
  }
  return element;
}

export interface AdderOptions {
  onAnnotate?: VoidFunction;
  onHighlight?: VoidFunction;
  onComment?: VoidFunction;
  onSuggest?: VoidFunction;
}

/**
 * Annotation 'adder' toolbar which appears next to the selection
 * and provides controls for the user to create new annotations.
 */
export interface IAdder {
  element: Element;
  options: AdderOptions;
  hostBridge: any;

  /** Hide the adder */
  hide(): void;

  /**
   * Return the best position to show the adder in order to target the
   * selected text in `targetRect`.
   *
   * @param {DOMRect} targetRect - The rect of text to target, in viewport
   *        coordinates.
   * @param {boolean} isSelectionBackwards - True if the selection was made
   *        backwards, such that the focus point is mosty likely at the top-left
   *        edge of `targetRect`.
   * @return {Target}
   */
  target(targetRect: DOMRect, isSelectionBackwards: boolean): AdderTarget;

  /**
   * Show the adder at the given position and with the arrow pointing in
   * `arrowDirection`.
   *
   * @param {number} left - Horizontal offset from left edge of viewport.
   * @param {number} top - Vertical offset from top edge of viewport.
   */
  showAt(left: number, top: number, arrowDirection: number): void;
  
  handleCommentCommand(event: Event): void;
  handleSuggestCommand(event: Event): void;
  handleHighlightCommand(event: Event): void;
  extend(container: HTMLElement, hostBridge: any, options: AdderOptions): void;
  disableAnnotations(): void;
  enableAnnotations(): void;
  disableCommentButton(): void;
  enableCommentButton(): void;
  disableSuggestionButton(): void;
  enableSuggestionButton(): void;
  disableHighlightButton(): void;
  enableHighlightButton(): void;
  removeHighlightButton(): void;
  addHighlightButton(): void;
}

type AdderContainer = HTMLElement | null;

/**
 * Annotation 'adder' toolbar which appears next to the selection
 * and provides controls for the user to create new annotations.
 */
export class Adder implements IAdder {
  private _element: HTMLElement;
  private _container: AdderContainer;
  private _view: Window | null;
  private _width!: () => number;
  private _height!: () => number;
  private _enterTimeout: any;
  private _options!: AdderOptions;
  private _hostBridge!: any;

  /**
   * Construct the toolbar and populate the UI.
   *
   * The adder is initially hidden.
   *
   * @param {AdderContainer} container - The DOM element into which the adder will be created
   * @param {Object} options - Options object specifying `onAnnotate` and `onHighlight`
   *        event handlers.
   */
  constructor(container: AdderContainer, options: AdderOptions) {
    this._element = createAdderDOM(container!) as HTMLElement;
    this._container = container;
    this._view = null;

    var handleCommand = (event: any) => {
      event.preventDefault();
      event.stopPropagation();

      var isAnnotateCommand = event.target.classList.contains(ANNOTATE_BTN_CLASS);

      if (isAnnotateCommand) {
        if (options.onAnnotate) options.onAnnotate();
      } else if(options.onHighlight) {
        options.onHighlight();
      }

      this.hide();
    };

    if (container) {
      // Set initial style
      Object.assign(container.style, {
        display: 'block',

        // take position out of layout flow initially
        position: 'absolute',
        top: 0,

        // Assign a high Z-index so that the adder shows above any content on the
        // page
        zIndex: 999,
      });

      // The adder is hidden using the `visibility` property rather than `display`
      // so that we can compute its size in order to position it before display.
      this._element.style.visibility = 'hidden';

      this._view = this._element.ownerDocument.defaultView;

      this._element?.querySelector(ANNOTATE_BTN_SELECTOR)?.addEventListener('click', handleCommand);

      this._element?.querySelector(HIGHLIGHT_BTN_SELECTOR)?.addEventListener('click', handleCommand);

      this._width = () => this._element.getBoundingClientRect().width;
      this._height = () => this._element.getBoundingClientRect().height;
    }

    this._enterTimeout = null;
  }

  get element(): HTMLElement {
    return this._element;
  }

  get options(): AdderOptions {
    return this._options;
  }

  get hostBridge(): Object {
    return this._hostBridge;
  }

  /** Hide the adder */
  hide(): void {
    clearTimeout(this._enterTimeout);
    this._element.className = classnames({ 'annotator-adder': true });
    this._element.style.visibility = 'hidden';
  }

  /**
   * Return the best position to show the adder in order to target the
   * selected text in `targetRect`.
   *
   * @param {DOMRect} targetRect - The rect of text to target, in viewport
   *        coordinates.
   * @param {boolean} isSelectionBackwards - True if the selection was made
   *        backwards, such that the focus point is mosty likely at the top-left
   *        edge of `targetRect`.
   * @return {Target}
   */
  target(targetRect: DOMRect, isSelectionBackwards: boolean): AdderTarget {
    // Set the initial arrow direction based on whether the selection was made
    // forwards/upwards or downwards/backwards.
    var arrowDirection: ArrowPointing;
    if (isSelectionBackwards) {
      arrowDirection = ARROW_POINTING_DOWN;
    } else {
      arrowDirection = ARROW_POINTING_UP;
    }
    var top: number;
    var left: number;

    // Position the adder such that the arrow it is above or below the selection
    // and close to the end.
    var hMargin = Math.min(ARROW_H_MARGIN, targetRect.width);
    if (isSelectionBackwards) {
      left = targetRect.left - this._width() / 2 + hMargin;
    } else {
      left = targetRect.left + targetRect.width - this._width() / 2 - hMargin;
    }

    // Flip arrow direction if adder would appear above the top or below the
    // bottom of the viewport.
    if (targetRect.top - this._height() < 0 &&
      arrowDirection === ARROW_POINTING_DOWN) {
      arrowDirection = ARROW_POINTING_UP;
    } else if (targetRect.top + this._height() > this._view!.innerHeight) {
      arrowDirection = ARROW_POINTING_DOWN;
    }

    if (arrowDirection === ARROW_POINTING_UP) {
      top = targetRect.top + targetRect.height + ARROW_HEIGHT;
    } else {
      top = targetRect.top - this._height() - ARROW_HEIGHT;
    }

    // Constrain the adder to the viewport.
    left = Math.max(left, 0);
    left = Math.min(left, this._view!.innerWidth - this._width());

    top = Math.max(top, 0);
    top = Math.min(top, this._view!.innerHeight - this._height());

    return { top, left, arrowDirection };
  }

  /**
   * Show the adder at the given position and with the arrow pointing in
   * `arrowDirection`.
   *
   * @param {number} left - Horizontal offset from left edge of viewport.
   * @param {number} top - Vertical offset from top edge of viewport.
   */
  showAt(left: number, top: number, arrowDirection: number): void {
    this._element.className = classnames({
      'annotator-adder': true,
      'annotator-adder--arrow-down': arrowDirection === ARROW_POINTING_DOWN,
      'annotator-adder--arrow-up': arrowDirection === ARROW_POINTING_UP,
    });

    // Some sites make big assumptions about interactive
    // elements on the page. Some want to hide interactive elements
    // after use. So we need to make sure the button stays displayed
    // the way it was originally displayed - without the inline styles
    // See: https://github.com/hypothesis/client/issues/137
    (this._element.querySelector(ANNOTATE_BTN_SELECTOR) as HTMLElement).style.display = '';
    var highlightButton = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR) as HTMLElement; // LEOS 3725 Change - Remove highlight option for ISC
    if (highlightButton) {
      highlightButton.style.display = '';
    }

    // Translate the (left, top) viewport coordinates into positions relative to
    // the adder's nearest positioned ancestor (NPA).
    //
    // Typically the adder is a child of the `<body>` and the NPA is the root
    // `<html>` element. However page styling may make the `<body>` positioned.
    // See https://github.com/hypothesis/client/issues/487.
    var positionedAncestor: Element = nearestPositionedAncestor(this._container!)!;
    var parentRect: DOMRect = positionedAncestor.getBoundingClientRect();

    //LEOS Change: added scroll position for inline containers
    //Done to place correctly the menu "Annotate Highlight" while selecting some text in a scrollable container
    //Needed this update as by default the document's body is taken as container
    var parentTop = parentRect.top;
    var parentLeft = parentRect.left;
    if (isScrollable(positionedAncestor)) {
      parentTop = parentTop - positionedAncestor.scrollTop;
      parentLeft = parentLeft - positionedAncestor.scrollLeft;
    }

    Object.assign(this._container!.style, {
      top: toPx(top - parentTop),
      left: toPx(left - parentLeft),
    });
    this._element.style.visibility = 'visible';

    clearTimeout(this._enterTimeout);
    this._enterTimeout = setTimeout(() => {
      this._element.className += ' is-active';
    }, 1);
  }

  handleCommentCommand(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    if (this._options.onComment) this._options.onComment();
    this.hide();
  }

  handleSuggestCommand(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    if(this._options.onSuggest) {
      this._options.onSuggest();
    }

    this.hide();
  }

  handleHighlightCommand(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    if(this._options.onHighlight) {
      this._options.onHighlight();
    }

    this.hide();
  }

  extend(container: HTMLElement, hostBridge: any, options: AdderOptions): void {
    this._hostBridge = hostBridge;
    this._options = options;
    let self = this;

    if (container.shadowRoot != null) {
      container.shadowRoot.querySelector("hypothesis-adder-actions")!.innerHTML = leos_template;
      this._element = container.shadowRoot.querySelector('.js-adder')!;
    }
    else {
      container.querySelector("hypothesis-adder-actions")!.innerHTML = leos_template;
      this._element = container.querySelector('.js-adder')!;
    }

    let suggestBtn: HTMLButtonElement | null = this._element.querySelector(SUGGESTION_BTN_SELECTOR);
    let commentBtn: HTMLButtonElement | null = this._element.querySelector(COMMENT_BTN_SELECTOR);
    let highlightBtn: HTMLButtonElement | null = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR);
    if (highlightBtn) {
      highlightBtn.onclick = this.handleHighlightCommand.bind(this);
    }

    if (commentBtn) {
      commentBtn.onclick = this.handleCommentCommand.bind(this);
    }

    if (suggestBtn) {
      if ((self._hostBridge["requestUserPermissions"]) && (typeof self._hostBridge["requestUserPermissions"] == 'function')) {
        // Add handler on host bridge to let leos application responds
        self._hostBridge["responseUserPermissions"] = function (data: any) {
          if (data.indexOf('CAN_SUGGEST') !== -1) {
            suggestBtn!.style.display = "flex";
          }
        };
        self._hostBridge["requestUserPermissions"]();
      }

      suggestBtn.style.display = "none";
      suggestBtn.onclick = this.handleSuggestCommand.bind(this);
    }
  }

  disableAnnotations(): void {
    this.disableCommentButton();
    this.disableSuggestionButton();
    this.disableHighlightButton();
  }

  enableAnnotations(): void {
    this.enableCommentButton();
    this.enableSuggestionButton();
    this.enableHighlightButton();
  }

  disableCommentButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(COMMENT_BTN_SELECTOR);
    if (button) {
      button.onclick = null;
      button.classList.add("annotator-disabled");
      button.setAttribute('data-title', 'Selection contains elements for which comments are not allowed.');
    }
  }

  enableCommentButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(COMMENT_BTN_SELECTOR);
    if (button) {
      button.onclick = this.handleCommentCommand.bind(this);
      button.classList.remove("annotator-disabled");
      button.removeAttribute('data-title');
    }
  }

  disableSuggestionButton(): void {
    var button: HTMLButtonElement | null = this._element.querySelector(SUGGESTION_BTN_SELECTOR);
    if (button) {
      button.onclick = null;
      button.classList.add("annotator-disabled");
      button.setAttribute('data-title', 'Selection contains elements for which suggestions are not allowed.');
    }
  }

  enableSuggestionButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(SUGGESTION_BTN_SELECTOR);
    if (button) {
      button.onclick = this.handleSuggestCommand.bind(this);
      button.classList.remove("annotator-disabled");
      button.removeAttribute('data-title');
    }
  }

  disableHighlightButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR);
    if (button) {
      button.onclick = null;
      button.classList.add("annotator-disabled");
      button.setAttribute('data-title', 'Selection contains elements for which highlights are not allowed.');
    }
  }

  enableHighlightButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR);
    if (button) {
      button.onclick = this.handleHighlightCommand.bind(this);
      button.classList.remove("annotator-disabled");
      button.removeAttribute('data-title');
    }
  }

  removeHighlightButton(): void {
    let button: HTMLButtonElement | null = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR);
    if (button) {
      REMOVED_HIGHLIGHT_BTN = button;
      button.parentNode?.removeChild(button);
    }
  }

  addHighlightButton(): void {
    // All buttons share the same parent
    var highlightButton: Element | null = this._element.querySelector(HIGHLIGHT_BTN_SELECTOR);
    if (!highlightButton && REMOVED_HIGHLIGHT_BTN) {
      var parent: ParentNode | undefined | null = this._element.querySelector(SUGGESTION_BTN_SELECTOR)?.parentNode;
      if (parent) {
        parent.appendChild(REMOVED_HIGHLIGHT_BTN);
      }
    }
  }
}
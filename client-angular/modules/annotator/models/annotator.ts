import { Annotation } from '../../../shared/models/annotation.model';
import { LeosSelector } from '../anchoring/leos-types';
import { AnchoringSelector } from '../anchoring/types';

export interface AnnotatorDocumentInfo {
    uri: string;
    metadata: any;
    frameIdentifier: string | null;
}

export type AnnotatorAnchorSelector = AnchoringSelector | LeosSelector;

export interface AnnotatorAnchorTarget {
    selector?: AnnotatorAnchorSelector[];
    source?: string;
}

export interface AnnotatorAnchor {
    annotation?: Annotation;
    highlights?: HTMLElement[];
    range?: Range;
    target?: AnnotatorAnchorTarget;
}
/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import * as Raf from 'raf';
import * as Hammer from 'hammerjs';

import Host, { IHost } from './host';
import { annotationCounts } from './annotation-counts';
import { trigger as sidebarTrigger } from './sidebar-trigger';
import * as events from '../../shared/bridge-events';
import * as features from './features';
import { IBucketBar } from './plugin/bucket-bar';
import { IToolbar } from './plugin/toolbar';

const raf = (Raf as any).default;
// Minimum width to which the frame can be resized.
const MIN_RESIZE = 280;

export interface SidebarGestureState {
  initial: number | null,
  final: number | null,
}

export interface ISidebar extends IHost {
  gestureState: SidebarGestureState | null;
  hide(): void;
  show(): void;
  onPan(event: HammerInput): void;
  onSwipe(event: HammerInput): void;
  isOpen(): boolean;
  setAllVisibleHighlights(shouldShowHighlights: boolean): void;
}

export default class Sidebar extends Host implements ISidebar {
    public gestureState: SidebarGestureState | null;
 
    protected renderFrame: number | null ;
    protected onHelpRequest?: VoidFunction;
    protected onProfileRequest?: VoidFunction;
    protected onSignupRequest?: VoidFunction;
    protected onLoginRequest?: VoidFunction;
    protected onLogoutRequest?: VoidFunction;
    protected onLayoutChange: Function;
    protected toolbarWidth?: number;
    protected toolbarClass?: IToolbar;

    constructor(element: Element, config: any) {
      super(element, config, false);
  
      if (!this.options.Toolbar) {
        this.options.Toolbar = { container: '.annotator-frame' };
        this.addPlugin('Toolbar', this.options.Toolbar);
      }
      this.initWithOptions(element, config);
      this.appendAppToFrame();


      this.renderFrame = null;
      this.gestureState = null;
      this._notifyOfLayoutChange = this._notifyOfLayoutChange.bind(this);
      this.onPan = this.onPan.bind(this);
      this.onSwipe = this.onSwipe.bind(this);
      this.hide();

      if (config.openSidebar || config.annotations || config.query) {
        this.on('panelReady', () => this.show());
      }

      if (this.plugins['BucketBar'] != null) {
        const bucketBar = this.plugins['BucketBar']!;
        (bucketBar as IBucketBar).element?.on('click', event => this.show());
      }

      if (this.plugins['Toolbar'] != null) {
        let toolbar = this.plugins['Toolbar'] as IToolbar;
        this.toolbarClass = toolbar;
        this.toolbarWidth = toolbar.getWidth();
        if (config.theme === 'clean') {
          toolbar.disableMinimizeBtn();
          toolbar.disableHighlightsBtn();
          toolbar.disableNewNoteBtn();
        } else {
          toolbar.disableCloseBtn();
          if (`${config.showSidebarToggleBtn}` !== "true") {
            toolbar.disableToggleBtn();
          }
        }

        this._setupGestures();
      }

      // The partner-provided callback functions.
      const serviceConfig = config.services != null ? config.services[0] : undefined;
      if (serviceConfig) {
        this.onLoginRequest = serviceConfig.onLoginRequest;
        this.onLogoutRequest = serviceConfig.onLogoutRequest;
        this.onSignupRequest = serviceConfig.onSignupRequest;
        this.onProfileRequest = serviceConfig.onProfileRequest;
        this.onHelpRequest = serviceConfig.onHelpRequest;
      }

      this.onLayoutChange = config.onLayoutChange;

      // initial layout notification
      this._notifyOfLayoutChange(false);

      this._setupSidebarEvents();
    }

    protected _setupSidebarEvents(): ISidebar {
      var self = this;
      annotationCounts(document.body, this.crossframe);
      sidebarTrigger(document.body, () => self.show());
      features.init(this.crossframe);

      this.crossframe.on('showSidebar', () => self.show());
      this.crossframe.on('hideSidebar', () => self.hide());
      this.crossframe.on(events.LOGIN_REQUESTED, () => {
        if (this.onLoginRequest) {
          this.onLoginRequest();
        }
      });
      this.crossframe.on(events.LOGOUT_REQUESTED, () => {
        if (self.onLogoutRequest) {
          self.onLogoutRequest();
        }
      });
      this.crossframe.on(events.SIGNUP_REQUESTED, () => {
        if (self.onSignupRequest) {
          self.onSignupRequest();
        }
      });
      this.crossframe.on(events.PROFILE_REQUESTED, () => {
        if (self.onProfileRequest) {
          self.onProfileRequest();
        }
      });
      this.crossframe.on(events.HELP_REQUESTED, () => {
        if (self.onHelpRequest) {
          self.onHelpRequest();
        }
      });
      // Return this for chaining
      return this;
    }

    protected _setupGestures(): ISidebar | null {
      // Check whether @toolbar is set as tests fail otherwise.
      if (!this.toolbar) { return null; }
      const $toggle = this.toolbar.find('[name=sidebar-toggle]');

      if (!$toggle[0]) return null;
      // Prevent any default gestures on the handle
      $toggle.on('touchmove', event => event.preventDefault());

      // Set up the Hammer instance and handlers
      const hammerManager = new Hammer.Manager($toggle[0]);

      const _onPan = (input: HammerInput): void => {
        this.onPan(input);
      }
      hammerManager.on('panstart panend panleft panright', _onPan.bind(this));

      const _onSwipe = (input: HammerInput): void => {
        this.onSwipe(input);
      }
      hammerManager.on('swipeleft swiperight', _onSwipe.bind(this));

      // Set up the gesture recognition
      const pan = hammerManager.add(new Hammer.Pan({ direction: Hammer.DIRECTION_HORIZONTAL }));
      const swipe = hammerManager.add(new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL }));
      swipe.recognizeWith(pan);

      // Set up the initial state
      this._initializeGestureState();

      // Return this for chaining
      return this;
    }

    protected _initializeGestureState(): void {
      this.gestureState = {
        initial: null,
        final: null,
      };
    }

    /** Schedule any changes needed to update the sidebar layout. */
    protected _updateLayout(): void {
      const self = this;
      // Only schedule one frame at a time
      if (this.renderFrame) { return; }

      // Schedule a frame
      this.renderFrame = raf(() => {
        self.renderFrame = null;  // Clear the schedule

        // Process the resize gesture
        if (this.gestureState?.final !== this.gestureState?.initial) {
          const m = self.gestureState?.final;
          const w = -(m || 0);
          self.frame.css('margin-left', `${m}px`);
          self.frame.css('width', `${w}px`);
          self._notifyOfLayoutChange();
        }
      });
    }

    /**
     * Notify integrator when sidebar layout changes via `onLayoutChange` callback.
     *
     * @param [boolean] explicitExpandedState - `true` or `false` if the sidebar
     *   is being directly opened or closed, as opposed to being resized via
     *   the sidebar's drag handles.
     */
    protected _notifyOfLayoutChange(explicitExpandedState: boolean = false): void {
      const toolbarWidth = this.toolbarWidth || 0;

      // The sidebar structure is:
      //
      // [ Toolbar    ][                                   ]
      // [ ---------- ][ Sidebar iframe container (@frame) ]
      // [ Bucket Bar ][                                   ]
      //
      // The sidebar iframe is hidden or shown by adjusting the left margin of its
      // container.

      if (this.onLayoutChange) {
        const rect = this.frame[0].getBoundingClientRect();
        const computedStyle = window.getComputedStyle(this.frame[0]);
        const width = parseInt(computedStyle.width);
        const leftMargin = parseInt(computedStyle.marginLeft);

        // The width of the sidebar that is visible on screen, including the
        // toolbar, which is always visible.
        let frameVisibleWidth = toolbarWidth;

        if (explicitExpandedState != null) {
          // When we are explicitly saying to open or close, jump
          // straight to the upper and lower bounding widths.
          if (explicitExpandedState) {
            frameVisibleWidth += width;
          }
        } else if (leftMargin < MIN_RESIZE) {
          // When the width hits its threshold of MIN_RESIZE,
          // the left margin continues to push the sidebar off screen.
          // So it's the best indicator of width when we get below that threshold.
          // Note: when we hit the right edge, it will be -0
          frameVisibleWidth += -leftMargin;
        } else {
          frameVisibleWidth += width;
        }

        // Since we have added logic on if this is an explicit show/hide
        // and applied proper width to the visible value above, we can infer
        // expanded state on that width value vs the lower bound
        const expanded = frameVisibleWidth > toolbarWidth;

        this.onLayoutChange({
          expanded,
          width: expanded ? frameVisibleWidth : toolbarWidth,
          height: rect.height,
        });
      }
    }

    onPan(event: HammerInput): void {
      switch (event.type) {
        case 'panstart':
          // Initialize the gesture state
          this._initializeGestureState();
          // Immadiate response
          this.frame.addClass('annotator-no-transition');
          // Escape iframe capture
          this.frame.css('pointer-events', 'none');
          // Set origin margin
          if (!this.gestureState) return;
          this.gestureState.initial = parseInt(getComputedStyle(this.frame[0]).marginLeft);
          break;

        case 'panend':
          // Re-enable transitions
          this.frame.removeClass('annotator-no-transition');
          // Re-enable iframe events
          this.frame.css('pointer-events', '');
          // Snap open or closed
          if (this.gestureState?.final == undefined) {
            this.hide();
          } else {
            this.show();
          }

          // Reset the gesture state
          this._initializeGestureState();
          break;

        case 'panleft': case 'panright':
          if (!this.gestureState?.initial) { return; }
          // Compute new margin from delta and initial conditions
          var m = this.gestureState.initial;
          var d = event.deltaX;
          this.gestureState.final = Math.min(Math.round(m + d), 0);
          // Start updating
          this._updateLayout();
          break;
      }

    }

    onSwipe(event: HammerInput): void {
      switch (event.type) {
        case 'swipeleft':
          this.show();
          break;
      }
    }


    
    show(): void {
      this.frame.css({ 'margin-left': `${-1 * (this.frame?.width() || 0)}px` });
      this.frame.removeClass('annotator-collapsed');

      if (this.plugins['Toolbar'] != null) {
        let toolbar = this.plugins['Toolbar'] as IToolbar;
        toolbar.showCollapseSidebarBtn();
        toolbar.showCloseBtn();
      }


      if (this.options.showHighlights === 'whenSidebarOpen') {
        this.setVisibleHighlights(true);
      }

      this._notifyOfLayoutChange(true);
      this.crossframe.call('sidebarOpened');
    }

    hide(): void {
      this.frame.css({ 'margin-left': '' });
      this.frame.addClass('annotator-collapsed');
  
      if (this.plugins['Toolbar'] != null) {
        const toolbar = this.plugins['Toolbar'] as IToolbar;
        toolbar.hideCloseBtn();
        toolbar.showExpandSidebarBtn();
      }

      if (this.options.showHighlights === 'whenSidebarOpen') {
        this.setVisibleHighlights(false);
      }

      this._notifyOfLayoutChange(false);
      this.crossframe.call('sidebarClosed');
    }

    isOpen(): boolean {
      return !this.frame.hasClass('annotator-collapsed');
    }

    setAllVisibleHighlights(shouldShowHighlights: boolean = false): void {
      this.crossframe.call('setVisibleHighlights', shouldShowHighlights);

      // Let the Toolbar know about this event
      this.publish('setVisibleHighlights', shouldShowHighlights);
    }
};
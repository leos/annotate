/**
 * Return the next node after `node` in a tree order traversal of the document.
 */
function nextNode(node: Node, skipChildren: boolean = false): Node | null {
    var _node: Node | null = node;

    if (!skipChildren && _node.firstChild) {
      return _node.firstChild;
    }
  
    do {
      if (_node.nextSibling) {
        return _node.nextSibling;
      }
  
      _node = _node.parentNode;
    } while (_node);
    return _node;
}
  
function firstNode(range: Range): Node {
    if (range.startContainer.nodeType === Node.ELEMENT_NODE) {
      var node = range.startContainer.childNodes[range.startOffset];
      return node || nextNode(range.startContainer, true
      /* skip children */
      );
    }
  
    return range.startContainer;
}
  
function firstNodeAfter(range: Range): Node | null {
    if (range.endContainer.nodeType === Node.ELEMENT_NODE) {
      var node = range.endContainer.childNodes[range.endOffset];
      return node || nextNode(range.endContainer, true
      /* skip children */
      );
    }
    return nextNode(range.endContainer);
}
  
function forEachNodeInRange(range: Range, cb: Function): void {
    var node: Node | null = firstNode(range);
    var pastEnd = firstNodeAfter(range);

    while (node !== pastEnd) {
        cb(node);
        node = nextNode(node!);
    }
}
/**
 * A polyfill for Range.toString().
 * Spec: https://dom.spec.whatwg.org/#dom-range-stringifier
 *
 * Works around the buggy Range.toString() implementation in IE and Edge.
 * See https://github.com/tilgovi/dom-anchor-text-position/issues/4
 */
export default function rangeToString(range: Range): string {
    // This is a fairly direct translation of the Range.toString() implementation
    // in Blink.
    var text: string = '';
    forEachNodeInRange(range, function (node: Node) {
      if (node.nodeType !== Node.TEXT_NODE) {
        return;
      }
  
      var start = node === range.startContainer ? range.startOffset : 0;
      var end = node === range.endContainer ? range.endOffset : (node?.textContent?.length || 0);
      text += node?.textContent?.slice(start, end) || '';
    });
    return text;
}
/** 
 * Copy of the dom-anchor-text-position project including types
 *  
 * This library offers conversion between a DOM Range instance and a text position selector 
 * as defined by the Web Annotation Data Model.
 * 
 * @see https://github.com/tilgovi/dom-anchor-text-position
 */
import seek from '../dom-seek';
import rangeToString from "./range-to-string";

const SHOW_TEXT = 4;

export interface TextPositionSelector {
    type: string;
    start: number;
    end: number;
}

export function fromRange(root: Element | undefined, range: Range | undefined): TextPositionSelector {
    if (root === undefined) {
      throw new Error('missing required parameter "root"')
    }
    if (range === undefined) {
      throw new Error('missing required parameter "range"')
    }
  
    let document = root.ownerDocument
    let prefix = document.createRange()
  
    let startNode = range.startContainer
    let startOffset = range.startOffset
  
    prefix.setStart(root, 0)
    prefix.setEnd(startNode, startOffset)
  
    let start = rangeToString(prefix).length
    let end = start + rangeToString(range).length
  
    return {
      type: 'TextPositionSelector',
      start: start,
      end: end,
    }
}

export function toRange(root: Element | undefined, selector: any = {}): Range {
    if (root === undefined) {
      throw new Error('missing required parameter "root"')
    }
  
    const document = root.ownerDocument
    const range = document.createRange()
    const iter = document.createNodeIterator(root, SHOW_TEXT)
  
    const start = selector.start || 0
    const end = selector.end || start
  
    const startOffset = start - seek(iter, start);
    const startNode = iter.referenceNode;
  
    const remainder = end - start + startOffset;
  
    const endOffset = remainder - seek(iter, remainder);
    const endNode = iter.referenceNode;
  
    range.setStart(startNode, startOffset)
    range.setEnd(endNode, endOffset)
  
    return range;
}
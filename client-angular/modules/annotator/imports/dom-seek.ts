/**
 * Copy of the dom-seek project including types
 * @see https://github.com/tilgovi/dom-seek
 */
const E_END = 'Iterator exhausted before seek ended.';
const E_SHOW = 'Argument 1 of seek must use filter NodeFilter.SHOW_TEXT.';
const E_WHERE = 'Argument 2 of seek must be an integer or a Text Node.';
const DOCUMENT_POSITION_PRECEDING = 2;
const SHOW_TEXT = 4;
const TEXT_NODE = 3;

interface SeekPredicates {
    forward: () => boolean;
    backward: () => boolean;
}

class SeekError extends Error {
    public code?: number;

    constructor(msg: string) {
        super(msg)
    }

    override toString(): string {
        return "InvalidStateError: ".concat(E_SHOW);
    }
}

/**
 * Adjust the position of a NodeIterator by an offset measured in text code units or to the position immediately before a target node.
 *
 * If the whatToShow attribute of iter is any value other than NodeFilter.SHOW_TEXT, throw an InvalidStateError DOMException.
 *
 * If where is a positive integer, seek the iterator forward until the sum of the text code unit lengths of all nodes that the iterator traverses is as close as possible to where without exceeding it.
 *
 * If where is a negative integer, seek the iterator backward until the sum of the text code unit lengths of all nodes that the iterator traverses is as close as possible to the positive value of where without exceeding it.
 * 
 * If where is a node, seek the iterator forward or backward until its pointer is positioned immediately before the target node.
 * 
 * If where is any other value, throw a TypeError exception.
 *
 * If the where argument specifies a target beyond the bounds of the root attribute of the iterator, throw a RangError exception.
 *
 * After this function returns, the pointerBeforeReferencNode property of the iterator should be true. The function may return a value less than where if returning where exactly would result in the iterator pointing after the last text node that its root node contains.
 * 
 * @returns The number of text code units between the initial and final iterator positions. This number will be negative when the traversal causes the iterator to traverse backward in document order.
 */
export default function seek(iter: NodeIterator, where: any): number {
  if (iter.whatToShow !== SHOW_TEXT) {
    var error; // istanbul ignore next

    try {
      error = new DOMException(E_SHOW, 'InvalidStateError');
    } catch (_unused) {
      error = new SeekError(E_SHOW);
      error.code = 11;
      error.name = 'InvalidStateError';
    }

    throw error;
  }

  var count: number = 0;
  var node: Node | null = iter.referenceNode;
  var predicates: SeekPredicates | null = null;

  if (isInteger(where)) {
    predicates = {
      forward: function forward() {
        return count < where;
      },
      backward: function backward() {
        return count > where || !iter.pointerBeforeReferenceNode;
      }
    };
  } else if (isText(where)) {
    var forward = before(node, where) ? function () {
      return false;
    } : function () {
      return node !== where;
    };

    var backward = function backward() {
      return node !== where || !iter.pointerBeforeReferenceNode;
    };

    predicates = {
      forward: forward,
      backward: backward
    };
  } else {
    throw new TypeError(E_WHERE);
  }

  while (predicates.forward()) {
    node = iter.nextNode();

    if (node === null) {
      throw new RangeError(E_END);
    }

    count += node?.nodeValue?.length || 0;
  }

  if (iter.nextNode()) {
    node = iter.previousNode();
  }

  while (predicates.backward()) {
    node = iter.previousNode();

    if (node === null) {
      throw new RangeError(E_END);
    }

    count -= node?.nodeValue?.length || 0;
  }

  if (!isText(iter.referenceNode)) {
    throw new RangeError(E_END);
  }

  return count;
}

function isInteger(n: any): boolean {
  if (typeof n !== 'number') return false;
  return isFinite(n) && Math.floor(n) === n;
}

function isText(node: Node): boolean {
  return node.nodeType === TEXT_NODE;
}

function before(ref: Node, node: Node): number {
  return ref.compareDocumentPosition(node) & DOCUMENT_POSITION_PRECEDING;
}
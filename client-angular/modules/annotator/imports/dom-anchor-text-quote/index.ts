/** 
 * Copy of the dom-anchor-text-position project including types
 * 
 * Find and describe ranges of a document using text quotes.
 *
 * This library is capable of searching for text content within a DOM node, utilizing optional context strings and/or offset hint to disambiguate matches.
 *
 * Functions are provided for finding a quote within a DOM node and returning either the text offsets of the best match or a DOM Range instance, as well as functions to perform the inverse operations of describing a Range or a sub-string identified by text offsets in terms of the selected text and its surrounding context.
 *
 * Searching is performed using the diff-match-patch library to support finding approximate matches.
 * 
 * @see https://github.com/tilgovi/dom-anchor-text-position
 */
import * as _DiffMatchPatch from 'diff-match-patch'
import * as textPosition from '../dom-anchor-text-position'

const DiffMatchPatch = (_DiffMatchPatch as any).default;

// The DiffMatchPatch bitap has a hard 32-character pattern length limit.
const SLICE_LENGTH = 32
const SLICE_RE = new RegExp('(.|[\r\n]){1,' + String(SLICE_LENGTH) + '}', 'g')
const CONTEXT_LENGTH = SLICE_LENGTH

export interface TextQuoteSelector {
    type: string;
    prefix?: string;
    exact: string;
    suffix?: string;
}

export function fromRange(root: Element | undefined, range: Range | undefined): TextQuoteSelector {
  if (root === undefined) {
    throw new Error('missing required parameter "root"')
  }
  if (range === undefined) {
    throw new Error('missing required parameter "range"')
  }

  let position = textPosition.fromRange(root, range)
  return fromTextPosition(root, position)
}


export function fromTextPosition(root: Element | undefined, selector: any): TextQuoteSelector {
  if (root === undefined) {
    throw new Error('missing required parameter "root"')
  }
  if (selector === undefined) {
    throw new Error('missing required parameter "selector"')
  }

  let {start} = selector
  if (start === undefined) {
    throw new Error('selector missing required property "start"')
  }
  if (start < 0) {
    throw new Error('property "start" must be a non-negative integer')
  }

  let {end} = selector
  if (end === undefined) {
    throw new Error('selector missing required property "end"')
  }
  if (end < 0) {
    throw new Error('property "end" must be a non-negative integer')
  }

  let exact = root.textContent?.substring(start, end)

  let prefixStart = Math.max(0, start - CONTEXT_LENGTH)
  let prefix = root.textContent?.substring(prefixStart, start)

  let suffixEnd = Math.min((root.textContent?.length || 0), end + CONTEXT_LENGTH)
  let suffix = root.textContent?.substring(end, suffixEnd)

  return {
    type: 'TextQuoteSelector',
    exact: exact || '', 
    prefix: prefix || '', 
    suffix: suffix || ''
  }
}


export function toRange(root: Element | undefined, selector: any, options: any = {}): Range | null {
  let position = toTextPosition(root, selector, options)
  if (position === null) {
    return null
  } else {
    return textPosition.toRange(root, position)
  }
}

export function toTextPosition(root: Element | undefined, selector: any, options: any = {}): textPosition.TextPositionSelector | null {
  if (root === undefined) {
    throw new Error('missing required parameter "root"')
  }
  if (selector === undefined) {
    throw new Error('missing required parameter "selector"')
  }

  let {exact} = selector
  if (exact === undefined) {
    throw new Error('selector missing required property "exact"')
  }

  let {prefix, suffix} = selector
  let {hint} = options
  let dmp = new DiffMatchPatch()

  dmp.Match_Distance = (root.textContent?.length || 0) * 2

  // Work around a hard limit of the DiffMatchPatch bitap implementation.
  // The search pattern must be no more than SLICE_LENGTH characters.
  let slices = exact.match(SLICE_RE)
  let loc = (hint === undefined) ? (((root.textContent?.length || 0) / 2) | 0) : hint
  let start = Number.POSITIVE_INFINITY
  let end = Number.NEGATIVE_INFINITY
  let result = -1
  let havePrefix = prefix !== undefined
  let haveSuffix = suffix !== undefined
  let foundPrefix = false

  // If the prefix is known then search for that first.
  if (havePrefix) {
    result = dmp.match_main(root.textContent, prefix, loc)
    if (result > -1) {
      loc = result + prefix.length
      foundPrefix = true
    }
  }

  // If we have a suffix, and the prefix wasn't found, then search for it.
  if (haveSuffix && !foundPrefix) {
    result = dmp.match_main(root.textContent, suffix, loc + exact.length)
    if (result > -1) {
      loc = result - exact.length
    }
  }

  // Search for the first slice.
  let firstSlice = slices.shift()
  result = dmp.match_main(root.textContent, firstSlice, loc)
  if (result > -1) {
    start = result
    loc = end = start + firstSlice.length
  } else {
    return null
  }

  // Create a fold function that will reduce slices to positional extents.
  let foldSlices = (acc: any, slice: any) => {
    if (!acc) {
      // A search for an earlier slice of the pattern failed to match.
      return null
    }

    let result = dmp.match_main(root.textContent, slice, acc.loc)
    if (result === -1) {
      return null
    }

    // The next slice should follow this one closely.
    acc.loc = result + slice.length

    // Expand the start and end to a quote that includes all the slices.
    acc.start = Math.min(acc.start, result)
    acc.end = Math.max(acc.end, result + slice.length)

    return acc
  }

  // Use the fold function to establish the full quote extents.
  // Expect the slices to be close to one another.
  // This distance is deliberately generous for now.
  dmp.Match_Distance = 64
  const acc = slices.reduce(foldSlices, {start, end, loc})
  if (!acc) {
    return null
  }

  return {
    type: 'TextPositionSelector',
    start: acc.start, 
    end: acc.end
  }
}
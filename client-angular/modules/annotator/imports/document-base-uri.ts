/**
 * Re-Implementation of the document-base-uri project
 * @see https://github.com/tilgovi/document-base-uri
 */

var baseURI: string | undefined;

export function getDocumentBaseUri(): string {
    if (baseURI != undefined) return baseURI;

    baseURI = document.baseURI;
    if (baseURI != undefined) return baseURI;

    var baseEls = document.getElementsByTagName('base');
    for (var i = 0 ; i < baseEls.length ; i++) {
        if (!!baseEls[i].href) {
            baseURI = baseEls[i].href;
            break;
        }
    }    
    return (baseURI || document.documentURI);
}
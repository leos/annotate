/** 
 * ReImplementation of the dom-anchor-fragment project including types
 *  
 * This library offers conversion between a DOM Range instance and a text position selector 
 * as defined by the Web Annotation Data Model.
 * 
 * @see https://github.com/tilgovi/dom-anchor-fragment
 */
'use strict';

export interface FragmentSelector {
    type: string;
    value: string;
    conformsTo: string;
}

export interface IFragmentAnchor {
    root: Element;
    id: string;
    toSelector(): FragmentSelector;
}

export class FragmentAnchor implements IFragmentAnchor {
    readonly root: Element;
    readonly id: string;

    constructor(root: Element | undefined, id: string | undefined) {
        if (root === undefined) {
            throw new Error('missing required parameter "root"');
        }
        if (id === undefined) {
            throw new Error('missing required parameter "id"');
        }
        this.root = root;
        this.id = id;        
    }

    toRange(): Range {
        const el: Element | null = this.root.querySelector('#' + this.id);
        if (el == null) {
          throw new Error('no element found with id "' + this.id + '"');
        }

        const range: Range = this.root.ownerDocument.createRange();
        range.selectNodeContents(el);
        return range;        
    }

    toSelector(): FragmentSelector {
        const el: Element | null = this.root.querySelector('#' + this.id);
        if (el == null) {
          throw new Error('no element found with id "' + this.id + '"');
        }
  
        var conformsTo = 'https://tools.ietf.org/html/rfc3236';
        if (el instanceof SVGElement) {
          conformsTo = 'http://www.w3.org/TR/SVG/';
        }
  
        return {
          type: 'FragmentSelector',
          value: this.id,
          conformsTo: conformsTo
        };        
    }
}

export function fromRange(root: Element | undefined, range: Range | undefined): FragmentAnchor {
    if (root === undefined) {
        throw new Error('missing required parameter "root"');
    }
    if (range === undefined) {
        throw new Error('missing required parameter "range"');
    }

    var el: any = range.commonAncestorContainer;
    while (el != null && !el.id) {
        if (root.compareDocumentPosition(el) & Node.DOCUMENT_POSITION_CONTAINED_BY) {
          el = el.parentElement;
        } else {
          throw new Error('no fragment identifier found');
        }
    }
    return new FragmentAnchor(root, el.id);
}

export function fromSelector(root: Element, selector: any = {}): FragmentAnchor {
    return new FragmentAnchor(root, selector.value);
}



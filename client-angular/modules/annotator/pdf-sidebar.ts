'use strict';

import Sidebar from './sidebar';

const DEFAULT_CONFIG = {
  TextSelection: {},
  PDF: {},
  BucketBar: {
    container: '.annotator-frame',
    scrollables: ['#viewerContainer'],
  },
  Toolbar: {
    container: '.annotator-frame',
  },
};

class PdfSidebar extends Sidebar {
  constructor(element: Element, config: any) {
    super(element, Object.assign({}, DEFAULT_CONFIG, config));
  }
}

export default PdfSidebar;

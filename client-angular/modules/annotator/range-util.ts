'use strict';

import { LeosAnchor, LeosParent, LeosSelector } from "./anchoring/leos-types";
import { AnnotatorAnchor } from "./models/annotator";
import { AnnotatorAnchorSelector } from "./models/annotator";

export interface ILeosOffset {
  readonly start: number; 
  readonly end: number;
  readonly isRangeSet: boolean;
}

export class LeosOffset implements ILeosOffset {
  constructor(readonly start: number, readonly end: number) {}

  get isRangeSet(): boolean {
    return (this.start > -1) && (this.end > -1);
  }

  static create(start: number, end: number): ILeosOffset {
    return new LeosOffset(start, end);
  }
}

interface IRangeSearchOptions {
  isMovedParagraph?: boolean;
  isLeosDeleted?: boolean;
}

interface WordCount {
  /** Number of words that can be combined to a related sentence */
  count: number;
  /** Start offset of the related sentence. -1 if relatedWordsCount == 0. */
  startOffset: number;
  /**  End offset of the related sentence. -1 if relatedWordsCount == 0. */
  endOffset: number;
}

/**
 * Returns true if the start point of a selection occurs after the end point,
 * in document order.
 */
export function isSelectionBackwards(selection: Selection): boolean {
  if (selection.focusNode === selection.anchorNode) {
    return selection.focusOffset < selection.anchorOffset;
  }

  var range: Range = selection.getRangeAt(0);
  return range.startContainer === selection.focusNode;
}

/**
 * Returns true if `node` lies within a range.
 *
 * This is a simplified version of `Range.isPointInRange()` for compatibility
 * with IE.
 *
 * @param {Range} range
 * @param {Node} node
 */
export function isNodeInRange(range: Range, node: Node): boolean {
  if (node === range.startContainer || node === range.endContainer) {
    return true;
  }

  var nodeRange = node.ownerDocument!.createRange()!;
  nodeRange.selectNode(node);
  var isAtOrBeforeStart =
    range.compareBoundaryPoints(Range.START_TO_START, nodeRange) <= 0;
  var isAtOrAfterEnd =
    range.compareBoundaryPoints(Range.END_TO_END, nodeRange) >= 0;
  nodeRange.detach();
  return isAtOrBeforeStart && isAtOrAfterEnd;
}

/**
 * Iterate over all Node(s) in `range` in document order and invoke `callback`
 * for each of them.
 *
 * @param {Range} range
 * @param {Function} callback
 */
function forEachNodeInRange(range: Range, callback: Function) {
  var root = range.commonAncestorContainer;

  // The `whatToShow`, `filter` and `expandEntityReferences` arguments are
  // mandatory in IE although optional according to the spec.
  var nodeIter: NodeIterator = root.ownerDocument!.createNodeIterator(root,
    NodeFilter.SHOW_ALL, null /* filter */);

  var currentNode: Node | null;
  while (currentNode = nodeIter.nextNode()) { // eslint-disable-line no-cond-assign
    if (isNodeInRange(range, currentNode)) {
      callback(currentNode);
    }
  }
}

/**
 * Returns the bounding rectangles of non-whitespace text nodes in `range`.
 *
 * @param {Range} range
 * @return {Array<Rect>} Array of bounding rects in viewport coordinates.
 */
export function getTextBoundingBoxes(range: Range): DOMRect[] {
  var whitespaceOnly = /^\s*$/;
  var textNodes: Node[] = [];
  forEachNodeInRange(range, (node: Node) => {
    if (node.nodeType === Node.TEXT_NODE &&
        !node.textContent?.match(whitespaceOnly)) {
      textNodes.push(node);
    }
  });

  var rects: DOMRect[] = [];
  textNodes.forEach((node: Node) => {
    var nodeRange: Range = node.ownerDocument!.createRange();
    nodeRange.selectNodeContents(node);
    if (node === range.startContainer) {
      nodeRange.setStart(node, range.startOffset);
    }
    if (node === range.endContainer) {
      nodeRange.setEnd(node, range.endOffset);
    }
    if (nodeRange.collapsed) {
      // If the range ends at the start of this text node or starts at the end
      // of this node then do not include it.
      return rects;
    }

    // Measure the range and translate from viewport to document coordinates
    var viewportRects: DOMRect[] = Array.from(nodeRange.getClientRects());
    nodeRange.detach();
    rects = rects.concat(viewportRects);
    return rects;
  });
  return rects;
}

/**
 * Returns the rectangle, in viewport coordinates, for the line of text
 * containing the focus point of a Selection.
 *
 * Returns null if the selection is empty.
 *
 * @param {Selection} selection
 * @return {Rect|null}
 */
export function selectionFocusRect(selection: Selection): DOMRect | null {
  if (selection.isCollapsed) {
    return null;
  }
  var textBoxes = getTextBoundingBoxes(selection.getRangeAt(0));
  if (textBoxes.length === 0) {
    return null;
  }

  if (isSelectionBackwards(selection)) {
    return textBoxes[0];
  } else {
    return textBoxes[textBoxes.length - 1];
  }
}

export function isRangeTextMatchSelectorText(anchor: AnnotatorAnchor, selector: AnnotatorAnchorSelector): boolean {
    const rangeText = anchor.range?.toString() || '';
    const anchorText = (selector as any)['exact'] || null;
    return (rangeText === anchorText);
}

export function isLeosSelectorMoved(selector: LeosSelector, root: HTMLElement | Document): boolean {
  // Search id of the moved to paragraph element
  return selector.parents.find((parent: LeosParent) => {
    return (root.querySelector(`#${parent.id}`)?.getAttribute('leos:softaction') === 'move_from')
  }) != undefined;
}

export function isSelectorTextDeleted(selector: LeosSelector, root: HTMLElement | Document = document): boolean {
  const selectorElement: Element | null =  root.querySelector(`#${selector.id}`);
  const delElements = selectorElement?.getElementsByTagName('del');
  if ((delElements == undefined) || (delElements.length == 0)) {
    return false;
  }

  const delElement: HTMLElement = delElements[0];
  const delText = delElement.textContent || '';
  return delText.indexOf(selector.exact.trim()) >= 0;
}

/**
  Try to look for a fitting range for an anchor without a range.

  The method try to find a node by using the information stored in the anchors leos selector.

  @param anchor Anchor without a range
  @param leosSelector Leos selector of the anchor
  @param root Root element to confine the search within
  @return Anchor with fixed range. In case no fitting node was found the range stays null.
*/
export function findNextBestRange(anchor: AnnotatorAnchor, leosSelector: LeosSelector, root: HTMLElement | Document = document): AnnotatorAnchor {
      let selectorElement = root.querySelector(`#${leosSelector.id}`) as HTMLElement;
      if (selectorElement && !anchor.annotation?.$leosDeleted) {
          if (anchor.annotation?.$selectorMoved) {
            selectorElement = _findMovedElement(leosSelector, root) || selectorElement;
            anchor.range = undefined;
          }

          //Origin selector element still exist. Check text.
          let leosOffset: ILeosOffset = (anchor.range && !anchor.annotation?.$selectorMoved) ? _findAnchorTextMatch(anchor, leosSelector)
              : _findElementTextMatch(selectorElement, leosSelector);
          if (leosOffset.isRangeSet) return _fixRange(anchor, selectorElement, leosOffset);
      }

      // Origin selector element does not exist. Try to find some existing parent.
      selectorElement = _findElementFromParents(leosSelector.parents, root) as HTMLElement;
      if (selectorElement) {
          anchor.range = _createOneNodeRange(selectorElement) || undefined;
      }
      return anchor;
};

function _findMovedElement(leosSelector: LeosSelector, root: HTMLElement | Document): HTMLElement | null {
  const parentSelector: LeosParent | undefined = leosSelector.parents.find((parent: LeosParent) => {
    return (root.querySelector(`#${parent.id}`)?.getAttribute('leos:softaction') === 'move_from')
  });
  if (!parentSelector) return null;

  const paragraphElement = root.querySelector(`#${parentSelector.id}`);
  const selectorElement = paragraphElement?.querySelector(`#${leosSelector.id}`);
  return selectorElement as HTMLElement;
}

function _findElementFromParents(parents?: LeosParent[], root: HTMLElement | Document = document): Element | null {
    if (!parents) return null;
    let element = null;
    let i = 0;
    while ((i < parents.length) && (parents[i].id.toLowerCase() !== '_body')) {
        element = root.querySelector(`#${parents[i].id}`);
        if (element || (!element && (['part', 'section', 'chapter', 'title', 'article', 'level'].indexOf(parents[i].tag.toLowerCase()) !== -1))) {
            break;
        }
        i++;
    }
    return element;
};

/**
 Look for the index of the last text match of a selector
 @return Index of the last word of the initial text that was found in the document range
*/
function _findAnchorTextMatch(anchor: AnnotatorAnchor, selector: LeosSelector): ILeosOffset {
    let currentText = anchor.range?.toString() || '';
    return _findTextMatch(currentText, selector);
}

function _findElementTextMatch(element: HTMLElement, selector: LeosSelector): ILeosOffset {
    let aknpElements = element.getElementsByTagName('aknp');
    let currentText = (aknpElements.length == 0) ? element.innerText : (aknpElements[0] as HTMLElement).innerText;
    return _findTextMatch(currentText, selector);
}

function _findTextMatch(currentText: string, selector: LeosSelector): ILeosOffset {
    let initialText = selector.exact;
    if (initialText) initialText = initialText.trim();
    if (currentText) currentText = currentText.trim();
    return _findTextMatchByWords(currentText, initialText, selector.start);
}

function _findTextMatchByWords(currentText: string, initialText: string, selectorStart: number | null): ILeosOffset {
    let words = initialText.trim().split(' ');
    let currentWords = currentText.trim().split(' ');
    let startOffset = -1;
    let endOffset = -1;
    let wordsCount = words.length
    let wordIndex = 0;
    let highestRelatedWordsCount = 0;

    // If current and initial start text are not equal, selector start offset can not be used
    let selectorStartOffset = (_compareWordList(words, currentWords, 3) && !!selectorStart) ? selectorStart : 0;
    while ((wordIndex < wordsCount) && (highestRelatedWordsCount == 0)) {
        //Counts the number of words that can be combined to a related sentence
        let relatedWordsCount: any = _getRelatedWordsCount(words, wordIndex, currentText);
        wordIndex = (relatedWordsCount.count == 0) ? wordIndex+1 : wordIndex+relatedWordsCount.count;

        if (relatedWordsCount.count <= highestRelatedWordsCount) continue;
        startOffset = selectorStartOffset + relatedWordsCount.startOffset;
        endOffset = selectorStartOffset + relatedWordsCount.endOffset;
        highestRelatedWordsCount = relatedWordsCount.count;
    }
    endOffset = (currentText.length < endOffset) ? currentText.length : endOffset;

    return LeosOffset.create(startOffset, endOffset);
}

/**
    Compare two string / word lists and check if a given number
    of words are equal. Beginning by index 0.

    @param wordList1 First word list to check
    @param wordList2 Second word list to check
    @param wordCount Number of words to compare.
    @return `true` if given number (wordCount) of words are equal else `false`.
*/
function _compareWordList(wordList1: string[], wordList2: string[], wordCount: number): boolean {
    if (wordList1.length < wordCount || wordList2.length < wordCount) {
        return false;
    }
    // Compare first x words
    for (let i = 0; i < wordCount; i++) {
        if (wordList1[i] !== wordList2[i]) return false;
    }
    return true;
}

/**
    Counts the number of words that can be combined to a related sentence
    @param words: List of words
    @param wordIndex: Index of the starting word
    @param srcText: Text to look for a related sentence
    @return Object with properties:
        {
            count: Number of words that can be combined to a related sentence
            startOffset: Start offset of the related sentence. -1 if relatedWordsCount == 0.
            endOffset: End offset of the related sentence. -1 if relatedWordsCount == 0.
        }
*/
function _getRelatedWordsCount(words: string[], startAt: number, srcText: string): WordCount {
    const emptyResult = {count: 0, startOffset: -1, endOffset: -1};
    if (startAt >= words.length) return emptyResult;
    if (srcText.length == 0) return emptyResult;

    let relatedWordsCount = 0;
    let sentence = words[startAt];
    let sentenceIndex = -1;
    let readIndex = startAt;
    let wordsCount = words.length;

    let sentenceReadIndex = _indexOfWord(sentence, srcText, 0);
    while ((readIndex < wordsCount) && (sentenceReadIndex > -1)) {
        readIndex++;
        relatedWordsCount++;
        sentenceIndex = sentenceReadIndex;

        if (readIndex >= wordsCount) continue;
        let nextWord = words[readIndex];
        sentenceReadIndex = srcText.indexOf(`${sentence} ${nextWord}`);
        sentence = (sentenceReadIndex > -1) ? `${sentence} ${nextWord}` : sentence;
    }

    let result = {
        count: relatedWordsCount,
        startOffset: (relatedWordsCount > 0) ? sentenceIndex : -1,
        endOffset: (relatedWordsCount > 0) ? (sentenceIndex+sentence.length) : -1
    }
    return result;
}

function _indexOfSentence(sentence: string, text: string, isOneWord: boolean): number {
    if (isOneWord) {
        return _indexOfWord(sentence, text, 0);
    }
    return text.indexOf(sentence);
}

function _indexOfWord(word: string, text: string, offset: number): number {
    if (text.length == 0) return -1;

    let startOffset = (offset && (offset > 0)) ? offset : 0;
    if (startOffset >= text.length) return -1;

    if (startOffset == 0) {
        // Check if the word is at the beginning of the current text
        // Read one more sign to be sure that the search word is not a part of another word
        let prefix = text.substring(0, word.length + 1).trim();
        if (prefix === word) return 0;
    }

    // Check if the word is in the middle of a text
    let indexOfWordWithSpace = text.indexOf(` ${word}`, startOffset);
    let indexOfWordWithNewLine = text.indexOf(`\n${word}`, startOffset);
    if (indexOfWordWithSpace > -1 || indexOfWordWithNewLine > -1) {
        if (indexOfWordWithNewLine == -1) return (indexOfWordWithSpace + 1);
        if (indexOfWordWithSpace == -1) return (indexOfWordWithNewLine + 1);
        return (indexOfWordWithSpace <= indexOfWordWithNewLine) ? indexOfWordWithSpace : indexOfWordWithNewLine;
    }

    // Check if the word is at the end of the current text
    let readIndex = text.length - (word.length + 1);
    let suffix = text.substring(readIndex).trim();
    if (suffix === word) return (readIndex + 1);

    return text.indexOf(`${word}`, startOffset);
}

function _fixRange(anchor: AnnotatorAnchor, element: Element, leosOffset: ILeosOffset): AnnotatorAnchor {
    let startOffset = (leosOffset.start > -1) ? leosOffset.start : 0;
    let endOffset = (leosOffset.end > -1) ? leosOffset.end : 0;
    let rangeContainer = anchor.range ? anchor.range.startContainer : element;
    anchor.range = _createTextNodeRange(rangeContainer, startOffset, endOffset) || undefined;
    return anchor;
}

function _createTextNodeRange(node: Node, startOffset: number, endOffset: number): Range | null {
    if (startOffset == 0 && endOffset == 0) {
        return _createOneNodeRange(node);
    }

    let range = document.createRange();
    let isStartSet = false;
    let textSize = 0;
    let processedTextSize = 0;
    let textNodes = _isTextNode(node) ? [node] : _filterTextNodesFromNodeList(node.childNodes);

    for (let textNode of textNodes) {
        let nodeText = textNode.textContent;
        if (!nodeText) continue;

        textSize += nodeText.length;
        if (!isStartSet) {
            if (startOffset < textSize) {
                range.setStart(textNode, startOffset - processedTextSize);
                isStartSet = true;
            }
        }
        if (endOffset <= textSize) {
            range.setEnd(textNode, (endOffset - processedTextSize));
            break;
        }
        processedTextSize = textSize;
    }
    return range;
}

function _filterTextNodesFromNodeList(nodeList: NodeList): Node[] {
    if (nodeList.length == 0) {
        return [];
    }

    let textNodes: Node[] = [];
    nodeList.forEach((node, index, listObj) => {
        if (_isElementNode(node)) textNodes = textNodes.concat(_filterTextNodesFromNodeList(node.childNodes));
        if (_isTextNode(node)) textNodes.push(node);
    });
    return textNodes;
}

function _isTextNode(node: Node): boolean {
    return (node.nodeType === 3);
}

function _isElementNode(node: Node): boolean {
    return (node.nodeType === 1);
}

function _createOneNodeRange(node: Node): Range | null {
    if (!node) return null;
    let range = document.createRange();
    range.setStart(node, 0);
    range.setEnd(node, 0);
    return range;
}

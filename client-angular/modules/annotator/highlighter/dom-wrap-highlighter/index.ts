/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import { INormalizedRange } from '../../anchoring/range';
import $ from '../../imports/jquery';


interface HTMLElementWithData extends HTMLElement {
  data?: string;
}

// Public: Wraps the DOM Nodes within the provided range with a highlight
// element of the specified class and returns the highlight Elements.
//
// normedRange - A NormalizedRange to be highlighted.
// cssClass - A CSS class to use for the highlight (default: 'annotator-hl')
//
// Returns an array of highlight Elements.
export function highlightRange(normedRange: INormalizedRange, cssClass: string = 'annotator-hl') {
  if (cssClass == null) { cssClass = 'annotator-hl'; }

  // A custom element name is used here rather than `<span>` to reduce the
  // likelihood of highlights being hidden by page styling.
  const hl: JQuery<HTMLElement> = $(`<hypothesis-highlight class='${cssClass}'></hypothesis-highlight>`);

  // Ignore text nodes that contain only whitespace characters. This prevents
  // spans being injected between elements that can only contain a restricted
  // subset of nodes such as table rows and lists. This does mean that there
  // may be the odd abandoned whitespace node in a paragraph that is skipped
  // but better than breaking table layouts.
  var textNodes: HTMLElement[] = normedRange.textNodes();
  if (textNodes.length > 1) {
    textNodes = textNodes.filter((node: HTMLElementWithData) => (node.data || '').length > 0);
  }
  const nodes: JQuery<HTMLElement> = $(textNodes);
  return nodes.wrap(hl).parent();
};

export function removeHighlights(highlights: HTMLElement[]): JQuery<HTMLElement>[] {
  const result: JQuery<HTMLElement>[] = [];
  for (let h of highlights) {
    if (h.parentNode != null) {
      result.push($(h).removeClass('annotator-hl'));//*/replaceWith(h.childNodes));
    }
  }
  return result;
};

export type BoundingClientRect = {
  top: number,
  left: number,
  bottom: number,
  right: number
}

function toBoundingClientRect(domRect: DOMRect): BoundingClientRect {
  return {
    top: domRect.top,
    left: domRect.left,
    bottom: domRect.bottom,
    right: domRect.right
  }
}

// Get the bounding client rectangle of a collection in viewport coordinates.
// Unfortunately, Chrome has issues[1] with Range.getBoundingClient rect or we
// could just use that.
// [1] https://code.google.com/p/chromium/issues/detail?id=324437
export function getBoundingClientRect(collection: Element[]): BoundingClientRect {
  // Reduce the client rectangles of the highlights to a bounding box
  const rects: DOMRect[] = collection.map((n: Element) => n.getBoundingClientRect());
  return rects.map(toBoundingClientRect).reduce((acc, r) => ({
    top: Math.min(acc.top, r.top),
    left: Math.min(acc.left, r.left),
    bottom: Math.max(acc.bottom, r.bottom),
    right: Math.max(acc.right, r.right),
  }));
};
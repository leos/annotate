'use strict';

import * as _domWrapHighlighter from './dom-wrap-highlighter';
import * as _overlayHighlighter from './overlay-highlighter';
import * as features from '../features';

const overlayHighlighter: any = _overlayHighlighter;
const domWrapHighlighter: any = _domWrapHighlighter

// we need a facade for the highlighter interface
// that will let us lazy check the overlay_highlighter feature
// flag and later determine which interface should be used.
var highlighterFacade: any = {};
var overlayFlagEnabled: boolean | undefined = undefined;

Object.keys(domWrapHighlighter).forEach((methodName)=>{
  highlighterFacade[methodName] = (...args: any[])=>{
    // lazy check the value but we will
    // use that first value as the rule throughout
    // the in memory session
    if(overlayFlagEnabled === undefined){
      overlayFlagEnabled = features.flagEnabled('overlay_highlighter');
    }

    const method = overlayFlagEnabled ? overlayHighlighter[methodName] : domWrapHighlighter[methodName];
    return method.apply(null, args);
  };
});

export { highlighterFacade };

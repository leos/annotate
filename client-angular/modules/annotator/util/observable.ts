'use strict';

import { Observable, Subscriber, TeardownLogic, Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';

/**
  * Returns an observable of events emitted by a DOM event source
  * (eg. an Element, Document or Window).
  *
  * @param {EventTarget} src - The event source.
  * @param {Array<string>} eventNames - List of events to subscribe to
  */
export function listen(src: EventTarget, eventNames: string[]): Observable<Event> {
  return new Observable((subscriber: Subscriber<Event>): TeardownLogic => {
    eventNames.forEach((eventName: string) => {
      src.addEventListener(eventName, (event: Event) => {
        subscriber.next(event);
      });
    });
    return (): void => {
      eventNames.forEach((eventName: string) => {
        src.removeEventListener(eventName, (event: Event) => {
          subscriber.next(event);
        });
      });
    }
  });
}

/**
 * Delay events from a source Observable by `delay` ms.
 */
export function delay(delay: number, src: Observable<any>): Observable<any> {
  return new Observable((obs: Subscriber<any>): TeardownLogic => {
    var timeouts: number[] = [];
    var sub = src.subscribe({
      next: (value: any) => {
        var t: number = window.setTimeout(function () {
          timeouts = timeouts.filter((other: number) => { return other !== t; });
          obs.next(value);
        }, delay);
        timeouts.push(t);
      },
    });
    return () => {
      timeouts.forEach((timeout: number) => window.clearTimeout(timeout));
      sub.unsubscribe();
    };
  });
}

 /**
  * Buffers events from a source Observable, waiting for a pause of `delay`
  * ms with no events before emitting the last value from `src`.
  *
  * @param {number} delay
  * @param {Observable<T>} src
  * @return {Observable<T>}
  */
export function buffer(delay: number, src: Observable<any>): Observable<any> {
  return new Observable((obs: Subscriber<any>): TeardownLogic => {
    var lastValue: any;
    var timeout: number;

    var sub = src.subscribe({
      next: (value: any) => {
        lastValue = value;
        clearTimeout(timeout);
        timeout = window.setTimeout(() => {
          obs.next(lastValue)
        }, delay);
      },
    });

    return (): void => {
      sub.unsubscribe();
      window.clearTimeout(timeout);
    };
  });
}

 /**
  * Merges multiple streams of values into a single stream.
  *
  * @param {Array<Observable>} sources
  * @return Observable
  */
export function merge(sources: Observable<any>[]): Observable<any> {
  return new Observable((obs: Subscriber<any>): TeardownLogic => {
    var subs = sources.map(function (src) {
      return src.subscribe({
        next: (value: any) => {
          obs.next(value);
        },
      });
    });

    return (): void => {
      subs.forEach((sub: Subscription) => {
        sub.unsubscribe();
      });
    };
  });
}

/** Drop the first `n` events from the `src` Observable. */
export function drop(src: Observable<any>, n: number) {
  return src.pipe(
    skip(n)
  )
}

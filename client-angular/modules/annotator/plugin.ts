import Delegator, { IDelegator } from './delegator';

export interface IPlugin extends IDelegator {
  pluginInit(): void;
}

export default class Plugin extends Delegator implements IPlugin {
  constructor(element: Element, options: any) {
    super(element, options);
  }

  pluginInit() {}
}

'use strict';

import * as FrameUtil from './util/frame-util';
import * as _ from 'lodash';

const debounce = _.debounce;

// Find difference of two arrays
function difference(arrayA: Array<any>, arrayB: Array<any>): any[] {
  return arrayA.filter(x => !arrayB.includes(x));
};

export const DEBOUNCE_WAIT = 40;

type OnFrameAddedCallback = (frame: HTMLIFrameElement) => void;
type OnFrameRemovedCallback = (frame: HTMLIFrameElement) => void;

export interface IFrameObserver {
  observe(onFrameAddedCallback: OnFrameAddedCallback, onFrameRemovedCallback: OnFrameRemovedCallback): void;
  disconnect(): void;
}

export class FrameObserver implements IFrameObserver {
  private _target: Element;
  private _handledFrames: HTMLIFrameElement[];
  private _mutationObserver: MutationObserver;
  private _onFrameAdded?: OnFrameAddedCallback;
  private _onFrameRemoved?: OnFrameRemovedCallback;

  constructor (target: Element) {
    this._target = target;
    this._handledFrames = [];

    const self = this;
    this._mutationObserver = new MutationObserver(debounce(() => {
      self._discoverFrames();
    }, DEBOUNCE_WAIT));
  }

  observe (onFrameAddedCallback: OnFrameAddedCallback, onFrameRemovedCallback: OnFrameRemovedCallback) {
    this._onFrameAdded = onFrameAddedCallback;
    this._onFrameRemoved = onFrameRemovedCallback;

    this._discoverFrames();
    this._mutationObserver.observe(this._target, {
      childList: true,
      subtree: true,
    });
  }

  disconnect () {
    this._mutationObserver.disconnect();
  }

  private _addFrame (frame: HTMLIFrameElement) {
    const onIsDocumentReady = () => {
      const self = this;
      frame?.contentWindow?.addEventListener('unload', () => {
        self._removeFrame(frame);
      });
      this._handledFrames.push(frame);
      if (this._onFrameAdded) {
        this._onFrameAdded(frame);    
      }
    };

    if (FrameUtil.isAccessible(frame)) {
      FrameUtil.isDocumentReady(frame, onIsDocumentReady.bind(this));
    } else {
      // Could warn here that frame was not cross origin accessible
    }
  }

  private _removeFrame (frame: HTMLIFrameElement) {
    if (this._onFrameRemoved) {
      this._onFrameRemoved(frame);    
    }

    // Remove the frame from our list
    this._handledFrames = this._handledFrames.filter(x => x !== frame);
  }

  private _discoverFrames() {
    let frames = FrameUtil.findFrames(this._target);

    for (let frame of frames) {
      if (!this._handledFrames.includes(frame)) {
        this._addFrame(frame);
      }
    }

    for (let frame of difference(this._handledFrames, frames)) {
      this._removeFrame(frame);
    }
  }
}
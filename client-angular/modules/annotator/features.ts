'use strict';

import * as events from '../../shared/bridge-events';
import { ICrossFrame } from './plugin/cross-frame';

var featuresCache: any = {};

function _set(features: Object) {
  featuresCache = features || {};
}

export function init(crossframe: ICrossFrame) {
    crossframe.on(events.FEATURE_FLAGS_UPDATED, _set);
}

export function reset() {
    _set({});
}

export function flagEnabled(flag: PropertyKey): boolean {
    if(featuresCache.hasOwnProperty(flag)) return featuresCache[flag];
    console.warn('looked up unknown feature', flag);
    return false;
}
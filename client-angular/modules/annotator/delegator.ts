/*
 * decaffeinate suggestions:
 * DS201: Simplify complex destructure assignments
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import $ from './imports/jquery';

import * as ANNOT_POPUP_DEFAULT_STATUS from '../../shared/annotationPopupDefaultStatus';

function _isScrollable (element: Element): boolean {
  return (element.scrollWidth > element.clientWidth) || (element.scrollHeight > element.clientHeight);
} 

function _nearestNonScrollableAncestor(element: Element): Element {
  if(typeof(element) === 'undefined') { return element; }
  let parentEl = element;
  while (parentEl.parentElement) {
    if (!_isScrollable(parentEl)) {
      break;
    }
    parentEl = parentEl.parentElement;
  }
  return parentEl;
};

/**
 * Native jQuery events that should recieve an event object. Plugins can
 * add their own methods to this if required.
 */ 
function _natives(): string[] {
  const specials: string[] = Object.keys($.event.special || {});
  return `\
  blur focus focusin focusout load resize scroll unload click dblclick
  mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave
  change select submit keydown keypress keyup error\
  `.split(/[^a-z]+/).concat(specials);
};

const NATIVES = _natives();


export interface DelegatorEventInfo {
  selector: string;
  event: string;
  functionName: string;
}

/** 
 * Parse the @events object of a Delegator into an array of objects containing
 * string-valued "selector", "event", and "func" keys.
 */ 
function _parseEvents(events: Map<string, string>): DelegatorEventInfo[] {
  if (events.size == 0) return [];

  const eventInfos: DelegatorEventInfo[] = [];
  var array: string[];
  var adjustedLength: number;
  var selector: string;
  var event: string;

  events.forEach((value: string, key: string) => {
    array = key.split(' ');
    adjustedLength = Math.max(array.length, 1);
    selector = array.slice(0, adjustedLength - 1).join(' ');
    event = array[adjustedLength - 1];
    eventInfos.push({
      selector: selector,
      event: event,
      functionName: value,
    })
  });
  return eventInfos;
};

/** 
 * Checks to see if the provided event is a DOM event supported by jQuery or
 * a custom user event.
 *
 * event - String event name.
 *
 * Examples
 *
 *   _isCustomEvent('click')              # => false
 *   _isCustomEvent('mousedown')          # => false
 *   _isCustomEvent('annotation:created') # => true
 *
 * Returns true if event is a custom user event.
 */ 
function _isCustomEvent(event: string): boolean {
  [event] = event.split('.');
  return $.inArray(event, NATIVES) === -1;
};

function getClosureKey(selector: string, event: string, functionName: string): string {
  return `${selector}/${event}/${functionName}`;
}

export interface SubscribeCallback extends Function {
  guid?: string | null;
}

export type SubscribeFunction = (event: string, callback: SubscribeCallback) => IDelegator;

/**
 * Delegator is the base class that all of Annotators objects inherit
 * from. It provides basic functionality such as instance options, event
 * delegation and pub/sub methods.
 */
export interface IDelegator {
  element?: JQuery<HTMLElement>;

  on: SubscribeFunction;

  /** 
   * Destroy the instance, unbinding all events.
   */ 
  destroy(): void;

  /** 
   * Binds the function names in the @events Object to their events.
   *
   * The @events Object should be a set of key/value pairs where the key is the
   * event name with optional CSS selector. The value should be a String method
   * name on the current class.
   *
   * This is called by the default Delegator constructor and so shouldn't usually
   * need to be called by the user.
   *
   * Examples
   *
   *   # This will bind the clickedElement() method to the click event on @element.
   *   @options = {"click": "clickedElement"}
   *
   *   # This will delegate the submitForm() method to the submit event on the
   *   # form within the @element.
   *   @options = {"form submit": "submitForm"}
   *
   *   # This will bind the updateAnnotationStore() method to the custom
   *   # annotation:save event. NOTE: Because this is a custom event the
   *   # Delegator#subscribe() method will be used and updateAnnotationStore()
   *   # will not receive an event parameter like the previous two examples.
   *   @options = {"annotation:save": "updateAnnotationStore"}
   *
   */
  addEvents(): void;

  /** 
   * Unbinds functions previously bound to events by addEvents().
   *
   * The @events Object should be a set of key/value pairs where the key is the
   * event name with optional CSS selector. The value should be a String method
   * name on the current class.
   *
   */
  removeEvents(): void;

  /** 
   * Fires an event and calls all subscribed callbacks with any parameters
   * provided. This is essentially an alias of @element.triggerHandler() but
   * should be used to fire custom events.
   *
   * NOTE: Events fired using .publish() will not bubble up the DOM.
   *
   * event  - A String event name.
   * params - An Array of parameters to provide to callbacks.
   *
   * Examples
   *
   *   instance.subscribe('annotation:save', (msg) -> console.log(msg))
   *   instance.publish('annotation:save', ['Hello World'])
   *   # => Outputs "Hello World"
   *
   * Returns itself.
   */ 
  publish(...args: any[]): IDelegator;
  
  /** 
   * Listens for custom event which when published will call the provided
   * callback. This is essentially a wrapper around @element.bind() but removes
   * the event parameter that jQuery event callbacks always recieve. These
   * parameters are unnessecary for custom events.
   *
   * event    - A String event name.
   * callback - A callback function called when the event is published.
   *
   * Examples
   *
   *   instance.subscribe('annotation:save', (msg) -> console.log(msg))
   *   instance.publish('annotation:save', ['Hello World'])
   *   # => Outputs "Hello World"
   *
   * Returns itself.
   */ 
  subscribe: SubscribeFunction
  
  /** 
   * Unsubscribes a callback from an event. The callback will no longer
   * be called when the event is published.
   *
   * event    - A String event name.
   * callback - A callback function to be removed.
   *
   * Examples
   *
   *   callback = (msg) -> console.log(msg)
   *   instance.subscribe('annotation:save', callback)
   *   instance.publish('annotation:save', ['Hello World'])
   *   # => Outputs "Hello World"
   *
   *   instance.unsubscribe('annotation:save', callback)
   *   instance.publish('annotation:save', ['Hello Again'])
   *   # => No output.
   *
   * Returns itself.
   */ 
  unsubscribe(...args: any[]): IDelegator;
}

/*
** Adapted from:
** https://github.com/openannotation/annotator/blob/v1.2.x/src/class.coffee
**
** Annotator v1.2.10
** https://github.com/openannotation/annotator
**
** Copyright 2015, the Annotator project contributors.
** Dual licensed under the MIT and GPLv3 licenses.
** https://github.com/openannotation/annotator/blob/master/LICENSE
*/

/** 
 * Delegator is the base class that all of Annotators objects inherit
 * from. It provides basic functionality such as instance options, event
 * delegation and pub/sub methods.
 */ 
export default class Delegator implements IDelegator {
  /**  
   * Events object. This contains a key/pair hash of events/methods that
   * should be bound. See Delegator#addEvents() for usage.
   * 
   * Is set in the constructor's of child classes
  */
  protected events: Map<string, string>;

  /** Options object. Extended on initialisation. */
  protected options: any;

  /** A jQuery object wrapping the DOM Element provided on initialisation. */
  public element?: JQuery<HTMLElement>;

  /** First non scrollable parent of the container to get an element on which side bar can be fixed */
  protected container?: JQuery<HTMLElement>;

  protected annotationPopupStatus: boolean;

  /** 
   *  Delegator creates closures for each event it binds.
   */
  protected _closures: any;

  public on: SubscribeFunction;

  /** Constructor function that sets up the instance. Binds the @events
   * hash and extends the @options object.
   *
   * element - The DOM element that this instance represents.
   * config - An Object literal of config settings.
   *
   * Examples
   *
   *   element  = document.getElementById('my-element')
   *    instance = new Delegator(element, {
   *     option: 'my-option'
   *   })
   *
   * Returns a new instance of Delegator.
   */
  constructor(element: Element, config: any) {
    this.events = new Map();
    this.options = {};
    this.annotationPopupStatus = true;
    this.element = undefined;
    this.options = $.extend(true, {}, this.options, config);
    this._closures = {};

    this.initWithOptions(element, config);

    const onFunction: SubscribeFunction = (event: string, callback: SubscribeCallback): IDelegator => this.subscribe(event, callback);
    this.on = onFunction.bind(this);
    this.addEvents();
  }

  protected initWithOptions(el: JQuery<HTMLElement> | Element, config: any) {
      let element: JQuery<HTMLElement> = $(el) as JQuery<HTMLElement>;
      if (!this.options.hasOwnProperty('Document')) {
          this.element = element;
          return;
      }

      // This is set for the normal toolbar
      this.options.Document.leosDocumentRootNode = config.leosDocumentRootNode;
      this.options.Document.annotationContainer = config.annotationContainer;
      this.options.Document.allowedSelectorTags = config.allowedSelectorTags;
      this.options.Document.editableSelector = config.editableSelector;
      this.options.Document.notAllowedSuggestSelector = config.notAllowedSuggestSelector;
      this.options.Document.operationMode = config.operationMode;
      this.options.Document.annotationPopupDefaultStatus = config.annotationPopupDefaultStatus;
      this.options.Document.showStatusFilter = config.showStatusFilter;
      this.options.Document.showGuideLinesButton = config.showGuideLinesButton;
      this.options.Document.disableSuggestionButton = config.disableSuggestionButton;
      this.options.Document.disableHighlightButton = config.disableHighlightButton;
      this.options.Document.connectedEntity = config.connectedEntity;
      this.options.Document.coordinators = config.coordinators;
      this.options.Document.context = config.context;

      if (Array.isArray(config.services) && config.services.length > 0) {
        this.options.Document.authority = config.services[0].authority;
      }
      if (this.options.hasOwnProperty('BucketBar')) {
        //Set the scrollable element on which the bucket bar should be synced
        this.options.BucketBar.scrollables = [`${this.options.Document.annotationContainer}`];
      }

      this.annotationPopupStatus = true;
      if (this.options.Document.annotationPopupDefaultStatus === ANNOT_POPUP_DEFAULT_STATUS.OFF) {
        this.annotationPopupStatus = false;
      }

      //Get first non scrollable parent of the container to get an element on which side bar can be fixed
      this.container = $(this.options.Document.annotationContainer);
      if (this.container[0]) {
        let nearestNonScrollableAncestor = _nearestNonScrollableAncestor(this.container[0]);
        // plugins set their own elements, for the toolbar it is set above using _nearestNonScrollableAncestor
        element = $(nearestNonScrollableAncestor) as JQuery<HTMLElement>;
      }
      this.element = element;
  }

  destroy() {
    this.removeEvents();
  }

  addEvents() {
    _parseEvents(this.events).map((event) => this._addEvent(event.selector, event.event, event.functionName));
  }

  removeEvents() {
    _parseEvents(this.events).map((event) => this._removeEvent(event.selector, event.event, event.functionName));
  }

  /** 
   * Binds an event to a callback function represented by a String. A selector
   * can be provided in order to watch for events on a child element.
   *
   * The event can be any standard event supported by jQuery or a custom String.
   * If a custom string is used the callback function will not recieve an
   * event object as it's first parameter.
   *
   * selector     - Selector String matching child elements. (default: '')
   * event        - The event to listen for.
   * functionName - A String function name to bind to the event.
   *
   * Examples
   *
   *   # Listens for all click events on instance.element.
   *   instance._addEvent('', 'click', 'onClick')
   *
   *   # Delegates the instance.onInputFocus() method to focus events on all
   *   # form inputs within instance.element.
   *   instance._addEvent('form :input', 'focus', 'onInputFocus')
   *
   * Returns itself.
   */
  protected _addEvent(selector: string, event: string, functionName: string): Delegator {
    const self: any = this;
    const closure = () => { return self[functionName].apply(self, arguments); };

    if ((selector === '') && _isCustomEvent(event)) {
      this.subscribe(event, closure);
    } else {
      this.element?.delegate(selector, event, closure);
    }

    const closureKey: string = getClosureKey(selector, event, functionName);
    this._closures[closureKey] = closure;
    return this;
  }

  /** 
   * Unbinds a function previously bound to an event by the _addEvent method.
   *
   * Takes the same arguments as _addEvent(), and an event will only be
   * successfully unbound if the arguments to removeEvent() are exactly the same
   * as the original arguments to _addEvent(). This would usually be called by
   * _removeEvents().
   *
   * selector     - Selector String matching child elements. (default: '')
   * event        - The event to listen for.
   * functionName - A String function name to bind to the event.
   *
   * Returns itself.
   */ 
  protected _removeEvent(selector: any, event: string, functionName: string): Delegator {
    const closureKey: string = getClosureKey(selector, event, functionName);
    const closure = this._closures[closureKey];

    if ((selector === '') && _isCustomEvent(event)) {
      this.unsubscribe([event, closure]);
    } else {
      this.element?.undelegate(selector, event, closure);
    }

    delete this._closures[closureKey];

    return this;
  }

  publish(...args: any): IDelegator {
    this.element?.triggerHandler.apply(this.element, args);
    return this;
  }

  subscribe(event: string, callback: SubscribeCallback): IDelegator {
    const self = this;
    const closure: any = (...args: any[]) => { 
      return callback.apply(self, args.slice(1));
    };
    const jquery: any = $;
    // Ensure both functions have the same unique id so that jQuery will accept
    // callback when unbinding closure.
    closure.guid = (callback.guid = (jquery.guid += 1));

    this.element?.bind(event, closure);
    return this;
  }

  unsubscribe(...args: any): IDelegator {
    this.element?.unbind.apply(this.element, args);
    return this;
  }
};
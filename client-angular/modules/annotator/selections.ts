'use strict';

import { Observable, of } from 'rxjs';
import { filter, mergeMap, map } from 'rxjs/operators';
import * as observableUtil from './util/observable';


//Changed for LEOS: document is the element where selections is allowed

type DetailEvent = Event & { detail: number };

/** Returns the selected `DOMRange` in `document`. */
function selectedRange(document: HTMLElement | Document, event: DetailEvent): Range | null {
  var selection: Selection | null;
  if (document instanceof HTMLElement) { // LEOS Change
    selection = document.ownerDocument?.getSelection();
  }
  else {
    selection = document.getSelection();
  }
  if (!selection?.rangeCount || selection.getRangeAt(0).collapsed) {
    return null;
  }
  else if (event.detail < 3) { // LEOS Change avoid triple click: LEOS-4432
    return selection.getRangeAt(0);
  }
  else {
    return null;
  }
}

/**
 * Returns an Observable stream of text selections in the current document.
 *
 * New values are emitted when the user finishes making a selection
 * (represented by a `DOMRange`) or clears a selection (represented by `null`).
 *
 * A value will be emitted with the selected range at the time of subscription
 * on the next tick.
 *
 * @return Observable<DOMRange|null>
 */
export function selections(document: HTMLElement | Document): Observable<any> | null {

  // Get a stream of selection changes that occur whilst the user is not
  // making a selection with the mouse.
  var isMouseDown: boolean;
  var selectionEvents = observableUtil.listen(document, ['mousedown', 'mouseup', 'selectionchange'])
    .pipe(
      filter((event: any) => {
        if (event.type === 'mousedown' || event.type === 'mouseup') {
          isMouseDown = (event.type === 'mousedown');
          return false;
        } else {
          return !isMouseDown;
        }
      })
    );

  // LEOS-2764 filter events containing a type: these events should not be handled by Hypothesis
  // LEOS Change LEOS-2809: Block the mouseup event when previous mousedown event's target is editable
  var blockNextMouseUp: boolean;
  var mouseSelectionEvents = observableUtil.listen(document,['mousedown', 'mouseup'])
    .pipe(
      filter((event: any) => {
        if (event.type === 'mousedown') {
          blockNextMouseUp = event.hostEventType;
          return false;
        } else {
          return (!blockNextMouseUp) && (!event.hostEventType);
        }
      }) 
    )

  var events = observableUtil.merge([
    // Add a delay before checking the state of the selection because
    // the selection is not updated immediately after a 'mouseup' event
    // but only on the next tick of the event loop.
    observableUtil.buffer(10, mouseSelectionEvents),

    // Buffer selection changes to avoid continually emitting events whilst the
    // user drags the selection handles on mobile devices
    observableUtil.buffer(100, selectionEvents),

    // Emit an initial event on the next tick
    observableUtil.delay(0, of({})),
  ]);

  return events.pipe(
    map((event: DetailEvent) => selectedRange(document, event)!)
  );
}

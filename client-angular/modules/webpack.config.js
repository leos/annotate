const path = require('path');

module.exports = {
  entry: {
    annotator: ['./build/modules/annotator/index.js', './annotator/styles/annotator.scss', './annotator/styles/pdfjs-overrides.scss'],
	  boot: ['./build/modules/boot/index.js'],
    bootSentApi: ['./build/modules/bootSentApi/index.js'],
  },
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        type: "asset/resource",
        generator: {
          filename: "[name].css",
        },
        use: ["sass-loader"],
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
  },
};
import { v4 as uuidv4 } from 'uuid';

export interface BootSettings {
    assetRoot?: string;
}

function injectStylesheet(doc: Document, href: string) {
    const link = doc.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    doc.head.appendChild(link);
}
  
function injectScript(doc: Document, src: string) {
    const script = doc.createElement('script');
    script.type = 'text/javascript';
    script.src = src;

    // Set 'async' to false to maintain execution order of scripts.
    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
    script.async = false;
    doc.head.appendChild(script);
}
  
function injectAssets(doc: Document, settings: BootSettings, assets: string[]) {
    const versionParam = Date.now();
    assets.forEach((path: string) => {
        var url = settings.assetRoot + (settings.assetRoot?.endsWith("/") ? '' : '/') + path + `?v=${versionParam}`;
        if (url.match(/\.css/)) {
            injectStylesheet(doc, url);
        } else {
            injectScript(doc, url);
        }
    });
}

/**
 * Bootstrap the sidebar application which displays annotations.
 */
function bootSentApi(doc: Document, config: BootSettings) {
    injectAssets(doc, config, [
        'sentapi/sentapi.css',
        'sentapi/runtime.js',
        'sentapi/polyfills.js',
        'sentapi/main.js',
    ]);
}

function injectApp(doc: Document) {
    var app = doc.createElement('annotate-sentapi-app') as any;
    app.async = false;
    doc.body.appendChild(app);
}

export function bootSentApiApp(_document: Document, config: BootSettings) {
    bootSentApi(_document, config);
    injectApp(document);
}

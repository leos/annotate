import { bootSentApiApp } from './boot';
import { jsonConfigsFrom } from '../../shared/config/json-config';

interface AssetRootConfig {
    assetRoot?: string;
}

const jsonConfig: AssetRootConfig = jsonConfigsFrom(window.document);

bootSentApiApp(window.document, {
    assetRoot: jsonConfig.assetRoot
});
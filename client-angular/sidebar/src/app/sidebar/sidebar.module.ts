import { NgModule } from '@angular/core';
import { RouterLink } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { SidebarRoutingModule } from '../routing/sidebar-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Angular Addons
import { Angulartics2Module } from 'angulartics2';

// Components
import {AnnotationComponent} from './components/annotation.component';
import {AnnotationActionButtonComponent} from './components/annotation-action-button.component';
import {AnnotationActionsMenuButtonComponent} from './components/annotation-actions-menu-button.component';
import {AnnotationFeedbackComponent} from './components/annotation-feedback.component';
import {AnnotationHeaderComponent} from './components/annotation-header.component';
import {AnnotationShareDialogComponent} from './components/annotation-share-dialog.component';
import {AnnotationThreadComponent} from './components/annotation-thread.component';
import {AnnotationViewerContentComponent} from './components/annotation-viewer-content.component';
import {CkEditorMarkdownComponent} from './components/ckeditor-markdown.component';
import {DropdownMenuComponent} from './components/dropdown-menu.component';
import {DropdownMenuBtnComponent} from './components/dropdown-menu-btn.component';
import {ExcerptComponent} from './components/excerpt.component';
import {FilterButtonComponent} from './components/filter-button.component';
import {FilterPaneComponent} from './components/filter-pane.component';
import {GroupListComponent} from './components/group-list.component';
import {HelpLinkComponent} from './components/help-link.component';
import {HelpPanelComponent} from './components/help-panel.component';
import {SidebarComponent} from './components/sidebar.component';
import {SidebarHeaderComponent} from './components/sidebar-header.component';
import {ToggleSidebarButtonComponent} from './components/toggle-sidebar-button.component';
import {LoggedoutMessageComponent} from './components/loggedout-message.component';
import {LoginControlComponent} from './components/login-control.component';
import {MarkdownComponent} from './components/markdown.component';
import {ModerationBannerComponent} from './components/moderation-banner.component';
import {MultipleSelectAutocomplete} from './components/multiple-select-autocomplete.component';
import {NewNoteBtnComponent} from './components/new-note-btn.component';
import {PublishAnnotationBtnComponent} from './components/publish-annotation-btn.component';
import {SearchInputComponent} from './components/search-input.component';
import {SelectionPaneComponent} from './components/selection-pane.component';
import {SelectionPaneActionsDropdownComponent} from './components/selection-pane-actions-dropdown.component';
import {SelectionTabsComponent} from './components/selection-tabs.component';
import {ShareDialogComponent} from './components/share-dialog.component';
import {SidebarContentComponent} from './components/sidebar-content.component';
import {SidebarTutorialComponent} from './components/sidebar-tutorial.component';
import {SortDropdownComponent} from './components/sort-dropdown.component';
import {StreamContentComponent} from './components/stream-content.component';
import {SuggestionButtonsComponent} from './components/suggestion-buttons.component';
import {SvgIconComponent} from './components/svg-icon.component';
import {ThreadListComponent} from './components/thread-list.component';
import {TimestampComponent} from './components/timestamp.component';
import {TopBarComponent} from './components/top-bar.component';

// Services
import {AnalyticsService} from './services/analytics.service';
import {AnnotationMapperService} from './services/annotation-mapper.service';
import {ApiService} from './services/api.service';
import {ApiRoutesService} from './services/api-routes.service';
import { BridgeService } from './services/bridge.service';
import {DraftsService} from './services/drafts.service';
import { DiscoveryService } from './services/discovery.service';
import {FeaturesService} from './services/features.service';
import {FlashService} from './services/flash.service';
import {FrameSyncService} from './services/frame-sync.service';
import {GroupsService} from './services/groups.service';
import {HypothesisStoreService} from './store';
import {HypothesisConfigurationService} from './services/hypothesis-configuration.service';
import {LocalStorageService} from './services/local-storage.service';
import { LeosOAuthAuthService } from './services/oauth-auth.service';
import {PendingRequestsService} from './services/pending-requests.service';
import { LeosPermissionsService } from './services/permissions.service';
import {QueryParserService} from './services/query-parser.service';
import {RootThreadService} from './services/root-thread.service';
import {ScopeService} from './services/scope.service';
import {SearchFilterService} from './services/search-filter.service';
import {ServiceUrlService} from './services/service-url.service';
import {SessionService} from './services/session.service';
import {StreamFilterService} from './services/stream-filter.service';
import {StreamerService} from './services/streamer.services';
import {SuggestionMergerService} from './services/suggestion-merger.service';
import {TagsService} from './services/tags.service';
import {TimeService} from './services/time.service';
import {UnicodeService} from './services/unicode.service';
import {ViewFilterService} from './services/view-filter.service';
import { IdService } from './services/id.service';

//Directives
import { BrandingDirective } from '../sidebar/directive/h-branding.directive';
import { AutofocusDirective } from '../sidebar/directive/h-autofocus.directive';
import { OnTouchDirective } from '../sidebar/directive/h-on-touch.directive';
import { TooltipDirective } from '../sidebar/directive/h-tooltip.directive';
import { SpinnerDirective  } from '../sidebar/directive/spinner.directive';
import { WindowScrollDirective } from '../sidebar/directive/window-scroll.directive';
import { CKEditorModule } from 'ckeditor4-angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbDropdownModule,
    CKEditorModule,
    HttpClientModule,
    FormsModule,
    RouterLink,
    SidebarRoutingModule,
    ToastrModule.forRoot(),
    Angulartics2Module.forRoot()
  ],
  exports: [
    SidebarRoutingModule,
    AutofocusDirective,
    BrandingDirective,
    OnTouchDirective,
    TooltipDirective,
    SpinnerDirective,
    WindowScrollDirective,
    SidebarComponent,
    // Components
    AnnotationComponent,
    AnnotationActionButtonComponent,
    AnnotationActionsMenuButtonComponent,
    AnnotationFeedbackComponent,
    AnnotationHeaderComponent,
    AnnotationShareDialogComponent,
    AnnotationThreadComponent,
    AnnotationViewerContentComponent,
    CkEditorMarkdownComponent,
    DropdownMenuComponent,
    DropdownMenuBtnComponent,
    ExcerptComponent,
    FilterButtonComponent,
    FilterPaneComponent,
    GroupListComponent,
    HelpLinkComponent,
    HelpPanelComponent,
    SidebarComponent,
    LoggedoutMessageComponent,
    LoginControlComponent,
    MarkdownComponent,
    MultipleSelectAutocomplete,
    ModerationBannerComponent,
    NewNoteBtnComponent,
    PublishAnnotationBtnComponent,
    SearchInputComponent,
    SelectionPaneComponent,
    SelectionPaneActionsDropdownComponent,
    SelectionTabsComponent,
    ShareDialogComponent,
    SidebarContentComponent,
    SidebarHeaderComponent,
    SidebarTutorialComponent,
    SortDropdownComponent,
    StreamContentComponent,
    SuggestionButtonsComponent,
    SvgIconComponent,
    ThreadListComponent,
    TimestampComponent,
    ToggleSidebarButtonComponent,
    TopBarComponent
  ],
  declarations: [
    // Directives
    AutofocusDirective,
    BrandingDirective,
    OnTouchDirective,
    TooltipDirective,
    SpinnerDirective,
    WindowScrollDirective,
    // Components
    AnnotationComponent,
    AnnotationActionButtonComponent,
    AnnotationActionsMenuButtonComponent,
    AnnotationFeedbackComponent,
    AnnotationHeaderComponent,
    AnnotationShareDialogComponent,
    AnnotationThreadComponent,
    AnnotationViewerContentComponent,
    CkEditorMarkdownComponent,
    DropdownMenuComponent,
    DropdownMenuBtnComponent,
    ExcerptComponent,
    FilterButtonComponent,
    FilterPaneComponent,
    GroupListComponent,
    HelpLinkComponent,
    HelpPanelComponent,
    SidebarComponent,
    LoggedoutMessageComponent,
    LoginControlComponent,
    MarkdownComponent,
    MultipleSelectAutocomplete,
    ModerationBannerComponent,
    NewNoteBtnComponent,
    PublishAnnotationBtnComponent,
    SearchInputComponent,
    SelectionPaneComponent,
    SelectionPaneActionsDropdownComponent,
    SelectionTabsComponent,
    ShareDialogComponent,
    SidebarContentComponent,
    SidebarHeaderComponent,
    SidebarTutorialComponent,
    SortDropdownComponent,
    StreamContentComponent,
    SuggestionButtonsComponent,
    SvgIconComponent,
    ThreadListComponent,
    TimestampComponent,
    ToggleSidebarButtonComponent,
    TopBarComponent
  ],
  providers: [
    HypothesisConfigurationService,
    ApiService,
    AnalyticsService,
    AnnotationMapperService,
    ApiRoutesService,
    BridgeService,
    DraftsService,
    DiscoveryService,
    FeaturesService,
    FlashService,
    FrameSyncService,
    GroupsService,
    HypothesisStoreService,
    IdService,
    LocalStorageService,
    LeosOAuthAuthService,
    PendingRequestsService,
    LeosPermissionsService,
    QueryParserService,
    RootThreadService,
    ScopeService,
    SearchFilterService,
    ServiceUrlService,
    SessionService,
    StreamFilterService,
    StreamerService,
    SuggestionMergerService,
    TagsService,
    TimeService,
    UnicodeService,
    ViewFilterService
  ]  
})
export class SidebarModule { 
}

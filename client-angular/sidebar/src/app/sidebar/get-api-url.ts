'use strict';

import { HypothesisServiceConfig, IHypothesisJsonConfig } from '../../../../shared/models/config.model';
import { serviceConfig } from './service-config';

/**
 * Function that returns apiUrl from the settings object.
 *
 * @param {object} settings - The settings object
 * @returns {string} The apiUrl from the service or the default apiUrl from the settings
 * @throws {Error} If the settings has a service but the service doesn't have an apiUrl
 *
 */
export function getApiUrl(settings: IHypothesisJsonConfig): string | undefined {
  var service: HypothesisServiceConfig | null = serviceConfig(settings);

  if (service) {
    // If the host page contains a service setting then the client should default to
    // using that apiUrl.
    if (service.apiUrl) {
      return service.apiUrl;
    } else {
      throw new Error('Service should contain an apiUrl value.');
    }
  }
  return settings.apiUrl;
}

export function getWSApiUrl(settings: IHypothesisJsonConfig): string | undefined  {
    var service: HypothesisServiceConfig | null = serviceConfig(settings);

    if (service) {
        // If the host page contains a service setting then the client should default to
        // using that websocketUrl.
        if (service.websocketUrl) {
            return service.websocketUrl;
        } else {
            throw new Error('Service should contain an websocketUrl value.');
        }
    }
    return settings.websocketUrl;
}
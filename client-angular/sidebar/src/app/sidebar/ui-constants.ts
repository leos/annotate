'use strict';

export enum UiConstants {
  TAB_ANNOTATIONS = 'annotation',
  TAB_NOTES = 'note',
  TAB_ORPHANS = 'orphan'
}

/**
 * uiConstants is a set of globally used constants across the application.
 */
export const TAB_ANNOTATIONS = UiConstants.TAB_ANNOTATIONS;
export const TAB_NOTES = UiConstants.TAB_NOTES;
export const TAB_ORPHANS = UiConstants.TAB_ORPHANS;
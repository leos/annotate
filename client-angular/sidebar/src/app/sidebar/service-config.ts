'use strict';

import { HypothesisServiceConfig, IHypothesisJsonConfig } from "../../../../shared/models/config.model";

/**
 * Return the configuration for the annotation service which the client would retrieve
 * annotations from which may contain the authority, grantToken and icon.
 *
 * @param {IHypothesisJsonSettings} settings - The settings object which would contain the services array.
 */
export const serviceConfig = (settings: IHypothesisJsonConfig): HypothesisServiceConfig | null => {
  if (!Array.isArray(settings.services) || settings.services.length === 0) {
    return null;
  }
  return settings.services[0];
}
'use strict';

import { SessionState } from '../models/store.model';

/**
 * Returns true if the sidebar tutorial has to be shown to a user for a given session.
 */
export function shouldShowSidebarTutorial(sessionState: SessionState): boolean {
  return !!sessionState.preferences?.show_sidebar_tutorial;
}
'use strict';

import { Subscriber, Observable, of } from 'rxjs';
import { IStoreService } from '../store';

/**
 * Return a value from app state when it meets certain criteria.
 *
 * `await` returns a Promise which resolves when a selector function,
 * which reads values from a Redux store, returns non-null.
 *
 * @param {Object} store - Redux store
 * @param {Function<T|null>} selector - Function which returns a value from the
 *   store if the criteria is met or `null` otherwise.
 * @return {Promise<T>}
 */
export function awaitStateChange(store: IStoreService<any>, selector: Function): Observable<any> {
  var result = selector(store);
  if (result !== null) return of(result);

  return new Observable((subscriber: Subscriber<any>) => {
    const unsubscribe = store.subscribe(() => {
      const result = selector(store);
      if (result !== null) {
        unsubscribe();
        subscriber.next(result);
        subscriber.complete();
      }
    });
  })
}

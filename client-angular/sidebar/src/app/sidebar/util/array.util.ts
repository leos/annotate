'use strict';

/**
 * Return the number of elements in `ary` for which `predicate` returns true.
 *
 * @param {Array} ary
 * @param {Function} predicate
 */
export function countIf(ary: any[], predicate: Function): number {
  return ary.reduce(function (count: number, item: any) {
    return predicate(item) ? count + 1 : count;
  }, 0);
}

/**
 * Create a new array with the result of calling `mapFn` on every element in
 * `ary`.
 *
 * Only truthy values are included in the resulting array.
 *
 * @param {Array} ary
 * @param {Function} mapFn
 */
export function filterMap(ary: any[], mapFn: Function): any[] {
  return ary.reduce((newArray: any[], item: any) => {
    var mapped = mapFn(item);
    if (mapped) {
      newArray.push(mapped);
    }
    return newArray;
  }, []);
}

/**
 * Convert an array to a set represented as an object.
 *
 * @param {string[]} list - List of keys for the set.
 */
export function toSet(list: string[]): Object {
  return list.reduce(function (set: any, key: string) {
    set[key] = true;
    return set;
  }, {});
}
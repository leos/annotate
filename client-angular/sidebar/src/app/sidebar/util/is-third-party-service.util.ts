'use strict';

import { HypothesisServiceConfig, IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { serviceConfig } from '../service-config';

/**
 * Return `true` if the first configured service is a "third-party" service.
 *
 * Return `true` if the first custom annotation service configured in the
 * services array in the host page is a third-party service, `false` otherwise.
 *
 * If no custom annotation services are configured then return `false`.
 *
 * @param {Object} settings - the sidebar settings object
 *
 */
export function isThirdPartyService(settings: IHypothesisJsonConfig): boolean {
  const service: HypothesisServiceConfig | null = serviceConfig(settings);

  if (service === null) {
    return false;
  }

  if (!service.hasOwnProperty('authority')) {
    return false;
  }

  return (service.authority !== settings.authDomain);
}

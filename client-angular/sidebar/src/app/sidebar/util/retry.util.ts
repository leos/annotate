'use strict';

import { operation as retryOperation, OperationOptions } from 'retry';

export type OperationFunction = () => Promise<any>;

/**
 * Retry a Promise-returning operation until it succeeds or
 * fails after a set number of attempts.
 *
 * @param {OperationFunction} opFn - The operation to retry
 * @param {retry.OperationOptions} options - The options object to pass to retry.operation()
 *
 * @return A promise for the first successful result of the operation, if
 *         it succeeds within the allowed number of attempts.
 */
export function retryPromiseOperation(opFn: OperationFunction, options?: OperationOptions): Promise<any> {
  return new Promise(function (resolve, reject) {
    var operation = retryOperation(options);
    operation.attempt(function () {
      opFn().then(function (result) {
        operation.retry();
        resolve(result);
      }).catch(function (err) {
        if (!operation.retry(err)) {
          reject(err);
        }
      });
    });
  });
}

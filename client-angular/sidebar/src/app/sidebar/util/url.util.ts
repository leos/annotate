'use strict';

export type TUrlAndParams = {
  /**
   * The expanded URL
   */
  url: string,

  /***
   * Dictionary of unused parameters.
   */
  params: any
}

/**
 * Replace parameters in a URL template with values from a `params` object.
 *
 * Returns an object containing the expanded URL and a dictionary of unused
 * parameters.
 *
 *   replaceURLParams('/things/:id', {id: 'foo', q: 'bar'}) =>
 *     {url: '/things/foo', params: {q: 'bar'}}
 */
export function replaceURLParams(url: string, params: any): TUrlAndParams {
  var unusedParams: any = {};
  for (var param in params) {
    if (params.hasOwnProperty(param)) {
      var value = params[param];
      var urlParam = ':' + param;
      if (url.indexOf(urlParam) !== -1) {
        url = url.replace(urlParam, encodeURIComponent(value));
      } else {
        unusedParams[param] = value;
      }
    }
  }
  return {url: url, params: unusedParams};
}

/**
 * Resolve a relative URL against a base URL to get an absolute URL.
 *
 * @param {string} relativeURL
 * @param {string} baseURL
 */
export function resolve(relativeURL: string, baseURL: string): string {
  return new URL(relativeURL, baseURL).href;
}

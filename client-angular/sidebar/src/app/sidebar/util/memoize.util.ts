'use strict';

export type MemoizeFunction<T,K> = (arg: T) => K;

function isDeepEqual(obj1: any, obj2: any): boolean {
  if (obj1 == null) return (obj2 == null);
  if (obj1 == undefined) return (obj2 == undefined);
  if (obj2 == null) return false;
  if (obj2 == undefined) return false; 

  const obj1Keys = Object.keys(obj1);
  if (obj1Keys.length != Object.keys(obj2).length) return false;
  if (obj1Keys.some((key: string) => !Object.hasOwn(obj2, key))) return false;
  return !obj1Keys.some((key: string) => {
    if (obj1[key] && (typeof(obj1[key]) == 'object')) {
      return !isDeepEqual(obj1[key], obj2[key]);
    }
    return !isEqual(obj1[key], obj2[key]);
  });
}

function isEqual(obj1: any, obj2: any): boolean {
  if (typeof(obj1) != typeof(obj2)) return false;
  if (typeof(obj1) != 'object' && typeof(obj2) != 'object') {
    return obj1 === obj2;
  }
  if (typeof(obj1) != 'object' || typeof(obj2) != 'object') {
    return false;
  }
  return isDeepEqual(obj1, obj2);
}


/**
 * A simple memoization class which caches the last result of
 * a single-argument function.
 *
 * The argument to the input function may be of any type and is compared
 * using reference equality.
 */
export class Memoizer<T,K> {
  protected memoizeFunction: MemoizeFunction<T,K>;
  protected lastArgument?: T;
  protected lastResult?: K;

  constructor(memoizeFunction: MemoizeFunction<T,K>) {
    this.memoizeFunction = memoizeFunction;
    this.lastArgument = undefined;
    this.lastResult = undefined;
  }

  memoize(arg: T): K | undefined {
    if (isEqual(arg, this.lastArgument)) {
      return this.lastResult;
    }
    this.lastResult = this.memoizeFunction(arg);
    this.lastArgument = Object.assign({}, arg);
    return this.lastResult;
  }
}

/**
 * A simple memoization function which caches the last result of
 * a single-argument function.
 *
 * The argument to the input function may be of any type and is compared
 * using reference equality.
 */
export function memoize<T,K>(fn: Function): (arg: T) => K {
  if (fn.length !== 1) {
    throw new Error('Memoize input must be a function of one argument');
  }

  var lastArg: T;
  var lastResult: K;

  return function (arg: T) {
    if (arg === lastArg) {
      return lastResult;
    }
    lastArg = arg;
    lastResult = fn(arg);
    return lastResult;
  };
}

'use strict';

import { Group, GroupOrganization } from '../models/groups.model';

 // TODO: Update when this is a property available on the API response
const DEFAULT_ORG_ID = '__default__';

/**
 * Generate consistent object keys for organizations so that they
 * may be sorted
 *
 * @param {Object} organization
 * @return {String}
 */
function orgKey (organization: GroupOrganization): string {
  if (organization.id === DEFAULT_ORG_ID) {  return DEFAULT_ORG_ID; }
  return `${(organization.name || '').toLowerCase()}${organization.id}`;
}

/**
 * Add a clone of the group object to the given organization object's
 * groups Array.
 *
 * @param {Group} group
 * @param {Object} organization
 * @return undefined - organization is mutated in place
 */
function addGroup (group: Group, organization: GroupOrganization): Group[] {
  // Object.assign won't suffice because of nested objects on groups
  const groupObj: Group = Object.assign({}, group);
  const groupList: Group[] = organization.groups || [];

  if (!groupList.length && group.organization && group.organization.logo) {
    groupObj.logo = group.organization.logo;
  }
  return groupList.concat(Object.freeze(groupObj));
}

/**
 * Iterate over groups and locate unique organizations. Slot groups into
 * their appropriate "parent" organizations.
 *
 * @param {Array<Group>} groups
 * @return {Object} - A collection of all unique organizations, containing
 *                    their groups. Keyed by each org's "orgKey"
 */
function organizations (groups: Group[]): any {
  const orgs: any = {};
  groups.forEach((group: Group) => {
    // Ignore groups with undefined or non-object organizations
    if (typeof group.organization !== 'object') { return; }
    const orgId: string = orgKey(group.organization);
    if (typeof orgs[orgId] === 'undefined') { // First time we've seen this org
      orgs[orgId] = Object.assign({}, group.organization);
      orgs[orgId].groups = []; // Will hold this org's groups
    }
    orgs[orgId].groups = addGroup(group, orgs[orgId]); // Add the current group to its organization's groups
  });
  return orgs;
}

/**
 * Take groups as returned from API service and sort them by which organization
 * they are in (all groups within a given organization will be contiguous
 * in the resulting Array).
 *
 * Groups with no organization or an unexpanded organization
 * will be omitted from the resulting Array.
 *
 * Organization ordering is by name, secondarily (pub)ID. Groups in the default
 * organization will appear at the end of the list. The first group
 * in each organization will have a logo property (if available on the
 * organization).
 *
 * @param {Array<Group>} groups
 * @return {Array<Object>} - groups sorted by which organization they're in
 */
export function groupsByOrganization(groups: Group[]): Group[] {
  const orgs = organizations(groups);
  var defaultOrganizationGroups: Group[] = [];
  var sortedGroups: Group[] = [];

  const sortedOrgKeys: string[] = Object.keys(orgs).sort();
  sortedOrgKeys.forEach((orgKey: string) => {
    const org = orgs[orgKey] as GroupOrganization;
    if (orgKey === DEFAULT_ORG_ID) { // Handle default groups separately
      defaultOrganizationGroups.push(...(org.groups || []));
    } else {
      sortedGroups.push(...(org.groups || []));
    }
  });

  if (defaultOrganizationGroups.length > 0) { // Put default groups at end
    sortedGroups.push(...defaultOrganizationGroups);
  }
  return sortedGroups;
}

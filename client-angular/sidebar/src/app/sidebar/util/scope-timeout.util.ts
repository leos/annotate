'use strict';

import { IScopeService } from "../services/scope.service";


/**
 * Sets a timeout which is linked to the lifetime of an Angular scope.
 *
 * When the scope is destroyed, the timeout will be cleared if it has
 * not already fired.
 *
 * The callback is not invoked within a $scope.$apply() context. It is up
 * to the caller to do that if necessary.
 *
 * @param {Scope} $scope - An Angular scope
 * @param {Function} fn - Callback to invoke with setTimeout
 * @param {number} delay - Delay argument to pass to setTimeout
 * @param {Function} setTimeoutFn - 
 * @param {Function} clearTimeoutFn - 
 */
export default function setScopeTimeout($scope: IScopeService, fn: Function, delay: number, setTimeoutFn?: Function, clearTimeoutFn?: Function) {
  const _setTimeoutFn = setTimeoutFn || setTimeout;
  const _clearTimeoutFn = clearTimeoutFn || clearTimeout;

  var removeDestroyHandler: any;
  const id = _setTimeoutFn(function () {
    removeDestroyHandler();
    fn();
  }, delay);
  removeDestroyHandler = $scope.$on('$destroy', () => {
    _clearTimeoutFn(id);
  });
};

'use strict';

/**
 * Parses H account names of the form 'acct:<username>@<provider>'
 * into a {username, provider} object or null if the input does not
 * match the expected form.
 */
type AccountInfo = {
  username: string,
  provider: string
}

export function parseAccountID(user: string | null | undefined): AccountInfo | undefined {
  if (!user) return undefined;

  const match = user.match(/^acct:([^@]+)@(.+)/);
  if (!match) return undefined;

  return {
    username: match[1],
    provider: match[2],
  };
}

export function buildAccountId(user: string, authDomain: string): string | undefined {
  if (!user || !authDomain) return undefined;
  return `acct:${user}@${authDomain}`;
}

/**
 * Returns the username part of an account ID or an empty string.
 */
export function username(user: string | null | undefined): string {
  const account: AccountInfo | undefined = parseAccountID(user);
  if (!account) return '';
  return account.username;
}

/**
 * Returns true if the authority is of a 3rd party user.
 */
export function isThirdPartyUser(user: string | null | undefined, authDomain: string): boolean {
  const account = parseAccountID(user);
  if (!account) return false;
  return account.provider !== authDomain;
}
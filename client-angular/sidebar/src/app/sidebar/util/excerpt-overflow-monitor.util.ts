'use strict';

const toPx = (val: number): string => {
  return val.toString() + 'px';
}


export type ExcerptState = {
  enabled?: boolean,
  overflowHysteresis?: number,
  collapsedHeight?: number,
  collapse?: boolean,
  animate?: boolean,
}

/**
 * Interface used by ExcerptOverflowMonitor to retrieve the state of the
 * <excerpt> and report when the state changes.
 *
 * interface Excerpt {
 *   getState(): State;
 *   contentHeight(): number | undefined;
 *   onOverflowChanged(): void;
 * }
 */
export interface Excerpt {
  getState(): ExcerptState;
  contentHeight(): number | undefined;
  onOverflowChanged(overflowing: boolean): void;
}

export type RequestAnimationFrame = (callback: Function) => number | void;

export type ContentStyle = {
  'max-height'?: string
}

export interface IExcerptOverflowMonitor {
  /**
   * Schedule a deferred check of whether the content is collapsed.
   */  
  check(): void;

  /**
   * Returns an object mapping CSS properties to values that should be applied
   * to an excerpt's content element in order to truncate it based on the
   * current overflow state.
   */
  contentStyle(): ContentStyle;
}

type WindowRequestAnimationFrame = (callback: FrameRequestCallback) => number | void;

/**
 * A helper for the <excerpt> component which handles determinination of the
 * overflow state and content styling given the current state of the component
 * and the height of its contents.
 *
 * When the state of the excerpt or its content changes, the component should
 * call check() to schedule an async update of the overflow state.
 *
 * @param {Excerpt} excerpt - Interface used to query the current state of the
 *        excerpt and notify it when the overflow state changes.
 * @param {WindowRequestAnimationFrame} requestAnimationFrame -
 *        Function called to schedule an async recalculation of the overflow
 *        state.
 */
export class ExcerptOverflowMonitor implements IExcerptOverflowMonitor {
  private pendingUpdate: boolean;
  private prevOverflowing: boolean | undefined;

  readonly contentStyle : () => ContentStyle;
  readonly check: VoidFunction;

  constructor(private excerpt: Excerpt, private requestAnimationFrame: WindowRequestAnimationFrame) {
    this.pendingUpdate = false;
    this.prevOverflowing = undefined;

    // contentStyle() and check() are assigned as another classe's method.
    // That means the `this` operator points to the other class instance instead of the ExcerptOverflowMonitor instance.
    // To prevent this we bind the contentStyle to the current instance
    const contentStyle = (): ContentStyle => this._contentStyle();
    this.contentStyle = contentStyle.bind(this);

    const check = () => this._check();
    this.check = check.bind(this);
  }

  private update() {
    var state: ExcerptState = this.excerpt.getState();

    if (!this.pendingUpdate) return;

    this.pendingUpdate = false;

    var overflowing = false;
    if (state.enabled) {
      var hysteresisPx: number = state.overflowHysteresis || 0;
      overflowing = (this.excerpt.contentHeight() || 0) > ((state.collapsedHeight || 0) + hysteresisPx);
    }
    if (overflowing === this.prevOverflowing) return;

    this.prevOverflowing = overflowing;
    this.excerpt.onOverflowChanged(overflowing);
  }

  private _check() {
    if (this.pendingUpdate) return;
    this.pendingUpdate = true;

    const self = this;
    this.requestAnimationFrame(() => self.update());
  }
    
  private _contentStyle(): ContentStyle {
    var state: ExcerptState = this.excerpt.getState();
    if (!state.enabled) return {};

    var maxHeight: string = '';
    if (this.prevOverflowing) {
      if (state.collapse) {
        maxHeight = toPx(state.collapsedHeight || 0);
      } else if (state.animate) {
        // Animating the height change requires that the final
        // height be specified exactly, rather than relying on
        // auto height
        maxHeight = toPx(this.excerpt.contentHeight() || 0);
      }
    } else if (typeof this.prevOverflowing === 'undefined' &&
                state.collapse) {
      // If the excerpt is collapsed but the overflowing state has not yet
      // been computed then the exact max height is unknown, but it will be
      // in the range [state.collapsedHeight, state.collapsedHeight +
      // state.overflowHysteresis]
      //
      // Here we guess that the final content height is most likely to be
      // either less than `collapsedHeight` or more than `collapsedHeight` +
      // `overflowHysteresis`, in which case it will be truncated to
      // `collapsedHeight`.
      maxHeight = toPx(state.collapsedHeight || 0);
    }

    return {
      'max-height': maxHeight,
    };
  } 
  
}
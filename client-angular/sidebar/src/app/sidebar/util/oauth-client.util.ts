'use strict';

import {default as queryString} from 'query-string';
import * as random from './random.util';
import { IPendingRequestsService } from '../services/pending-requests.service'
import { TokenInfo, OAuthClientConfig } from '../models/oauth-auth.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, map, mergeMap, throwError, of, Subscriber } from 'rxjs';

/**
* Create and show a pop-up window for use with `OAuthClient#authorize`.
*
* This function _must_ be called in the same turn of the event loop as the
* button or link which initiates login to avoid triggering the popup blocker
* in certain browsers. See https://github.com/hypothesis/client/issues/534
* and https://github.com/hypothesis/client/issues/535.
*
* @param {Window} $window - The parent of the created window.
* @return {Window} The new popup window.
*/
export function openAuthPopupWindow($window: Window): Window | undefined {
    // In Chrome & Firefox the sizes passed to `window.open` are used for the
    // viewport size. In Safari the size is used for the window size including
    // title bar etc. There is enough vertical space at the bottom to allow for
    // this.
    //
    // See https://bugs.webkit.org/show_bug.cgi?id=143678
    const width  = 475;
    const height = 430;
    const left   = $window.screen.width / 2 - width / 2;
    const top    = $window.screen.height /2 - height / 2;

    // Generate settings for `window.open` in the required comma-separated
    // key=value format.
    const authWindowSettings: string = queryString.stringify({
        left: left,
        top: top,
        width: width,
        height: height,
    }).replace(/&/g, ',');

    return $window.open('about:blank', 'Log in to Hypothesis', authWindowSettings) || undefined;
}

type AuthResponse = {
  state: string;
  message: string;
  code?: string;
};

/**
 * Return a new TokenInfo object from the given tokenUrl endpoint response.
 * @param {Object} response - The HTTP response from a POST to the tokenUrl
 *                            endpoint (an Angular $http response object).
 * @returns {TokenInfo}
 */
function tokenInfoFrom(response: HttpResponse<Object>): TokenInfo {
    const data: any = response.body;
    return {
        accessToken: data['access_token'],

        // Set the expiry date to some time slightly before that implied by
        // `expires_in` to account for the delay in the client receiving the
        // response.
        expiresAt: Date.now() + ((data['expires_in'] - 10) * 1000),

        refreshToken: data['refresh_token'],
    };
}

/**
 * Generate a short random string suitable for use as the "state" param in
 * authorization requests.
 *
 * See https://tools.ietf.org/html/rfc6749#section-4.1.1.
 */
function generateState(): string {
  return random.generateHexString(16);
}

/**
 * OAuthClient handles interaction with the annotation service's OAuth
 * endpoints.
 */
export interface IOAuthClient {
    /**
    * OAuth client ID
    */
    clientId: string | undefined;

    /**
    * Value for the X-Client-Id request header
    */
    xClientId: string | undefined;

    /**
    * OAuth token exchange/refresh endpoint
    */
    tokenEndpoint: string | undefined;

    /**
    * OAuth authorization endpoint
    */
    authorizationEndpoint: string | undefined;

    /**
    * RFC 7009 token revocation endpoint
    */
    revokeEndpoint: string | undefined;

    /**
    * User´s context
    */
    context: string | undefined;

    /**
    * Authorization "state" parameter generator
    */
    generateState(): string | undefined;

    /**
    * Exchange an authorization code for access and refresh tokens.
    *
    * @param {string} code
    * @return {Observable<TokenInfo>}
    */
    exchangeAuthCode(code: string): Observable<TokenInfo>;

    /**
    * Exchange a grant token for access and refresh tokens.
    *
    * See https://tools.ietf.org/html/rfc7523#section-4
    *
    * @param {string} token
    * @return {Observable<TokenInfo>}
    */
    exchangeGrantToken(token: string): Observable<TokenInfo>;

    /**
    * Refresh an access and refresh token pair.
    *
    * See https://tools.ietf.org/html/rfc6749#section-6
    *
    * @param {string} refreshToken
    * @return {Observable<TokenInfo>}
    */
    refreshToken(refreshToken: string): Observable<TokenInfo>;

    /**
    * Revoke an access and refresh token pair.
    *
    * @param {string} accessToken
    * @return {Observable}
    */
    revokeToken(accessToken: string): Observable<HttpResponse<Object>>

    /**
    * Prompt the user for permission to access their data.
    *
    * Returns an authorization code which can be passed to `exchangeAuthCode`.
    *
    * @param {Window} $window - Window which will receive the auth response.
    * @param {Window} authWindow - Popup window where the login prompt will be shown.
    *   This should be created using `openAuthPopupWindow`.
    * @return {Observable<string>}
    */
    authorize($window: Window, authWindow: Window): Observable<string|undefined>;
}

export default class OAuthClient implements IOAuthClient {
    /**
    * Create a new OAuthClient
    *
    * @param {Object} $http - HTTP client
    * @param {OAuthClientConfig} config
    */
    constructor(private $http: HttpClient, 
        private config: OAuthClientConfig,
        private pendingRequestsService: IPendingRequestsService) {
            // Test seam
            this.generateState = config.generateState || generateState;
    }

    get clientId(): string | undefined {
        return this.config.clientId;
    }

    get xClientId(): string | undefined {
        return this.config.xClientId;
    }

    get tokenEndpoint(): string | undefined {
        return this.config.tokenEndpoint;
    }

    get authorizationEndpoint(): string | undefined {
        return this.config.authorizationEndpoint;
    }

    get revokeEndpoint(): string | undefined {
        return this.config.revokeEndpoint;
    }

    get context(): string | undefined {
        return this.config.context;
    }

    generateState(): string | undefined {
        if (!this.config.generateState) return undefined;
        return this.config.generateState();
    }

    exchangeAuthCode(code: string): Observable<TokenInfo> {
        const data = {
            client_id: this.clientId,
            grant_type: 'authorization_code',
            code,
        };
        return this._formPost(this.tokenEndpoint || '', data).pipe(
            mergeMap((response: HttpResponse<Object>) => {
                if (response.status !== 200) {
                    return throwError(() => new Error('Authorization code exchange failed'));
                } 
                return of(tokenInfoFrom(response));
            })
        )
    }

    exchangeGrantToken(token: string): Observable<TokenInfo> {
        var data = {
          grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
          assertion: token,
          context: this.context
        };
        return this._formPost(this.tokenEndpoint || '', data).pipe(
            mergeMap((response: HttpResponse<Object>) => {
                if (response.status !== 200) {
                    return throwError(() => new Error('Failed to retrieve access token'));
                } 
                return of(tokenInfoFrom(response));
            })
        );
    }

    refreshToken(refreshToken: string): Observable<TokenInfo> {
        var data = { grant_type: 'refresh_token', refresh_token: refreshToken };
        return this._formPost(this.tokenEndpoint || '', data).pipe(
            mergeMap((response: HttpResponse<Object>) => {
                if (response.status !== 200) {
                    return throwError(() => new Error('Failed to refresh access token'));
                } 
                return of(tokenInfoFrom(response));
            })
        )
    }

    revokeToken(accessToken: string): Observable<HttpResponse<Object>> {
        return this._formPost(this.revokeEndpoint || '', { token: accessToken });
    }

    authorize($window: Window, authWindow: Window): Observable<string|undefined> {
        // Random state string used to check that auth messages came from the popup
        // window that we opened.
        const state = this.generateState();

        // Authorize user and retrieve grant token
        var authUrl = this.authorizationEndpoint;
        authUrl += '?' + queryString.stringify({
            client_id: this.clientId,
            origin: $window.location.origin,
            response_mode: 'web_message',
            response_type: 'code',
            state: state,
        });
        authWindow.location = authUrl || '';

        // Observable which provide authorization code or throw an error when the user accepts or closes the
        // auth popup.
        return new Observable<AuthResponse|undefined>((subscriber: Subscriber<AuthResponse|undefined>) => {
            function authRespListener (event: MessageEvent<any>) {
                if (typeof event.data !== 'object') {
                    return undefined;
                }
                if (event.data.state !== state) {
                    // This message came from a different popup window.
                    return undefined;
                }
                if (event.data.type === 'authorization_response') {
                    subscriber.next(event.data);
                }
                if (event.data.type === 'authorization_canceled') {
                    subscriber.error(new Error('Authorization window was closed'));
                }
                $window.removeEventListener('message', authRespListener);
                subscriber.complete();           
            }
            $window.addEventListener('message', authRespListener);
        }).pipe(
            map((response: AuthResponse|undefined) => response?.code)
        );
    }

    /**
    * Make an `application/x-www-form-urlencoded` POST request.
    *
    * @param {string} url
    * @param {Object} data - Parameter dictionary
    */
    private _formPost(url: string, data: Object): Observable<HttpResponse<Object>> {
        data = queryString.stringify(data);

        var headers: any = {'Content-Type': 'application/x-www-form-urlencoded'}
        if (this.xClientId) {
            headers['X-Client-Id'] = this.xClientId;
        }
        return  this.pendingRequestsService.watchRequest(this.$http.post(url, data, {
            headers: headers,
            observe: 'response'
        }));
    }
}

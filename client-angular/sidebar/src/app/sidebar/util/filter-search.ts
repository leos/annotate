import { IHypothesisSearchController } from "../models/hypothesis-app.model"; 
import { IHypothesisStore } from "../store";

export class DefaultFilterSearchController implements IHypothesisSearchController {
    constructor(private store: IHypothesisStore) {
    }

    query(): string | undefined {
        return this.store.state.filterQuery;
    }

    update(query: string): void {
        this.store.selection.setFilterQuery(query);
    }

    filterReseted(): boolean {
        return this.store.state.filterReseted;
    }

    setFilterReseted(filterReset: boolean): void {
        this.store.selection.setFilterReseted(filterReset);
    }
}
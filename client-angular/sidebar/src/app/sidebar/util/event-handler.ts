export default class EventHandler {
    private eventListener: Map<string, Function[]>;
    
    constructor() {
        this.eventListener = new Map();
    }

    public emit(event: string, ...args: any[]): void {
        if (!this.eventListener.has(event)) return;
        this.eventListener.get(event)?.forEach((callback: Function) => {
            if (callback.length <= 1) {
                callback(event);
            } else {
                callback(event, ...args);
            }
        });
    }    

    public on(event: string, callback: Function): void {
        if (this.eventListener.has(event)) {
            this.eventListener.get(event)?.push(callback);
        } else {
            this.eventListener.set(event, [callback]);
        }
    }

    public off(event: string): boolean {
        return this.eventListener.delete(event);
    }

    public getEventListener(): Object {
        var listener: any = {};        
        this.eventListener.forEach((value: Function[], key: string) => {
            listener[key] = value;
        });
        return listener;
    }
}
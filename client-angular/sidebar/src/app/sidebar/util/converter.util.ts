import { ProfileUserInfo, ProfileUserInfoEntity } from '../models/session.model'
import { AnnotationUserInfo, AnnotationUserEntity } from '../../../../../shared/models/annotation.model'
import { UserProfile } from '../models/session.model';
import { HypothesisAuthState } from '../models/hypothesis-app.model';
import { parseAccountID } from '../util/account-id.util';

/**
 * Return the user's authentication status from their profile.
 *
 * @param {UserProfile} profile - The profile object from the API.
 */
export function authStateFromUserProfile(profile: UserProfile): HypothesisAuthState {
    if (profile.userid) {
      var parsed = parseAccountID(profile.userid)!;
      var displayName = parsed.username;
      if (profile.user_info && profile.user_info.display_name) {
        displayName = profile.user_info.display_name;
      }
      return {
        status: 'logged-in',
        displayName,
        userid: profile.userid,
        username: parsed.username,
        provider: parsed.provider,
      };
    } else {
      return {status: 'logged-out'};
    }
}

export function profileUserInfoToAnnotationUserInfo(userInfo?: ProfileUserInfo): AnnotationUserInfo | undefined {
    if (userInfo == undefined) return undefined;
    return {
        display_name: userInfo.display_name,
        entity_name: userInfo.entity_name,
        entities: (!userInfo.entities) ? undefined : (userInfo.entities.map(profileUserEntityToAnnotationUserEntity) as AnnotationUserEntity[])
    }

}

export function profileUserEntityToAnnotationUserEntity(userEntity?: ProfileUserInfoEntity): AnnotationUserEntity | undefined {
    if (!userEntity) return undefined;
    return {
        id: userEntity.id || '',
        name: userEntity.name,
        organizationName: userEntity.organizationName
    }
}
'use strict';

import { Observable, tap, catchError, of, finalize, throwError } from 'rxjs';
import EventEmitter from './imports/event-emitter';

type SearchClientOpts = {
  incremental?: boolean;
  chunkSize?: number;
}

export type SearchFunction = (params: any, data?: any) => Observable<any>;

/**
 * Client for the Hypothesis search API.
 *
 * SearchClient handles paging through results, canceling search etc.
 */
export default class SearchClient extends EventEmitter {
  private _results: any;
  private _canceled: any;
  private _searchFn: SearchFunction;
  private _incremental: boolean;
  private _chunkSize: number;

  /**
   * @param {Object} searchFn - Function for querying the search API
   * @param {Object} opts - Search options
   */
  constructor(searchFn: SearchFunction, opts?: SearchClientOpts) {
    super();
    opts = opts || {};

    var DEFAULT_CHUNK_SIZE = 200;
    this._searchFn = searchFn;
    this._chunkSize = opts.chunkSize || DEFAULT_CHUNK_SIZE;
    if (typeof opts.incremental !== 'undefined') {
      this._incremental = opts.incremental;
    } else {
      this._incremental = true;
    }
    this._canceled = false;
  }

  private _getBatch(query: any, offset: number) {
    var searchQuery = Object.assign({
      limit: this._chunkSize,
      offset: offset,
      sort: 'created',
      order: 'asc',
      _separate_replies: true,
    }, query);

    var self = this;
    this._searchFn(searchQuery).subscribe({
      next(results: any) {
        if (self._canceled) return;
        var chunk = results.rows.concat(results.replies || []);
        if (self._incremental) {
          self.emit('results', chunk);
        } else {
          self._results = self._results.concat(chunk);
        }
  
        // Check if there are additional pages of results to fetch. In addition to
        // checking the `total` figure from the server, we also require that at
        // least one result was returned in the current page, otherwise we would
        // end up repeating the same query for the next page. If the server's
        // `total` count is incorrect for any reason, that will lead to the client
        // polling the server indefinitely.
        var nextOffset = offset + results.rows.length;
        if (results.total > nextOffset && chunk.length > 0) {
          self._getBatch(query, nextOffset);
        } else {
          if (!self._incremental) {
            self.emit('results', self._results);
          }
          self.emit('end');
        }
      },
      error(err: any) {
        if (self._canceled) return throwError(() => new Error(err));
        self.emit('error', err);
        return throwError(() => new Error(err));
      },
      complete() {
        if (self._canceled) return;
        self.emit('end');
      }
    });
  }

  /**
   * Perform a search against the Hypothesis API.
   *
   * Emits a 'results' event with an array of annotations as they become
   * available (in incremental mode) or when all annotations are available
   * (in non-incremental mode).
   *
   * Emits an 'error' event if the search fails.
   * Emits an 'end' event once the search completes.
   */
  get(query: any) {
    this._results = [];
    this._getBatch(query, 0);
  }

  getTemporary(id: string, document: string) {
    this._results = [];
    this._getTemporaryBatch(id, document);
  }

  private _getTemporaryBatch(id: string, document: string) {
    var searchQuery = { id: id, document: document };
    var self = this;

    this._searchFn(searchQuery).pipe(
      tap((results: any) =>{
        if (self._canceled) return;
        var chunk = results.rows.concat(results.replies || []);
        self._results = self._results.concat(chunk);
        self.emit('results', self._results);
        self.emit('end');
      }),
      catchError((err: any) => {
        if (self._canceled) return throwError(() => err);
        self.emit('error', err);        
        return throwError(() => err);
      }),
      finalize(() => {
        if (self._canceled) return;
        self.emit('end');        
      })
    );
  }

  /**
   * Cancel the current search and emit the 'end' event.
   * No further events will be emitted after this.
   */
  cancel() {
    this._canceled = true;
    this.emit('end');
  }
}
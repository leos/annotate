import { V4Options, v4 as uuid_v4 } from 'uuid';

export interface IIdService {
    generateUUIDv4(options?: V4Options): string;
    getGlobalClientId(): string;
}

export class IdService implements IIdService {
    private static globalClientId?: string;

    constructor() {
    }

    generateUUIDv4(options?: V4Options): string {
        return uuid_v4(options);
    }

    getGlobalClientId(): string {
        if (!IdService.globalClientId) {
            IdService.globalClientId = uuid_v4();
        }
        return IdService.globalClientId;
    }
}

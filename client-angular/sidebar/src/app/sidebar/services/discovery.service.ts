import { Injectable } from "@angular/core";
import { DisocveryOptions, Discovery } from '../../../../../shared/discovery';

const _generateToken = () => ('' + Math.random()).replace(/\D/g, '');

export interface IDiscoveryService {
    generateToken(): string;
    createDiscovery(target: Window, options?: DisocveryOptions): Discovery;
  }
  
  @Injectable()
  export class DiscoveryService implements IDiscoveryService {
    constructor(){};
  
    generateToken(): string {
      return _generateToken();
    }
  
    createDiscovery(target: Window, options?: DisocveryOptions): Discovery {
      return new Discovery(target, options);
    }
  }
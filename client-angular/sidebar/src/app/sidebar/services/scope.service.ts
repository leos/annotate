
import EventHandler from '../util/event-handler';
import { ChangeDetectorRef } from '@angular/core';
import { Observable, finalize } from 'rxjs';
import { Injectable } from '@angular/core';

export interface IScopeService {
    $new(): IScopeService;
    $apply(changeDetectorRef: ChangeDetectorRef, execute?: VoidFunction): void;
    $applyObservable(changeDetectorRef: ChangeDetectorRef, observable: Observable<any>): void;
    $applyPromise(changeDetectorRef: ChangeDetectorRef, promise: Promise<any>): void;
    $broadcast(event: string, ...args: any[]): void;
    $on(event: string, callback: Function): VoidFunction;
    $off(event: string): boolean;
    $listener(): Object; 

    getProperty(key: string): any;
    storeProperty(key: string, value: any): void;
}

@Injectable()
export class ScopeService implements IScopeService {
    private eventHandler: EventHandler;
    private storedProperties: Map<string, any>;

    constructor() {
        this.eventHandler = new EventHandler();
        this.storedProperties = new Map();
    }

    public $new(): IScopeService {
        return new ScopeService();
    }

    public $broadcast(event: string, ...args: any[]) {
        this.eventHandler.emit(event, ...args);
    }

    public $on(event: string, callback: Function): VoidFunction {
        this.eventHandler.on(event, callback);

        const eventHandler = this.eventHandler;
        return (): void => {
            eventHandler.off(event);
        }
    }

    public $off(event: string): boolean {
        return this.eventHandler.off(event);
    }

    public $listener(): Object {
        return this.eventHandler.getEventListener();
    }

    public $apply(changeDetectorRef: ChangeDetectorRef, execute?: VoidFunction): void {
        //changeDetectorRef.detach();
        if (execute) execute();
        changeDetectorRef.detectChanges();
        //changeDetectorRef.reattach();
    }

    public $applyObservable(changeDetectorRef: ChangeDetectorRef, observable: Observable<any>): void {
        //changeDetectorRef.detach();
        observable.pipe(
            finalize(() => {
                changeDetectorRef.detectChanges();
                //changeDetectorRef.reattach();
            })
        )
    }

    public $applyPromise(changeDetectorRef: ChangeDetectorRef, promise: Promise<any>): void {
        changeDetectorRef.detach();
        promise.finally(() => {
            changeDetectorRef.detectChanges();
            changeDetectorRef.reattach();
        });
    }


    public storeProperty(key: string, value: any) {
        this.storedProperties.set(key, value); 
    }

    public getProperty(key: string): any {
        return this.storedProperties.get(key)
    }
}
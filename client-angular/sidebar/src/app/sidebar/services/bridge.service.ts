import { Injectable } from "@angular/core";
import { DefaultBridgeService } from '../../../../../shared/bridge';

@Injectable()
export class BridgeService extends DefaultBridgeService {
    constructor() {
        super();
    }
}
'use strict';

import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import {PendingRequestsService} from './pending-requests.service';
import { Observable, map, mergeMap, throwError, of, retry, tap } from 'rxjs';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';
import { IdService } from './id.service';

export interface IApiRoutesService {
    getJSON(url: string): Observable<any>;
    routes(): Observable<any>;
    links(): Observable<any>;
}

/**
 * A service which fetches and caches API route metadata.
 */
@Injectable()
export class ApiRoutesService implements IApiRoutesService {
    // Cache of route name => route metadata from API root.
    private routeCache: any;

    // Cache of links to pages on the service fetched from the API's "links"
    // endpoint.
    private linkCache: any;

    private clientId: string;

    private settings: IHypothesisJsonConfig;
    constructor(private $http: HttpClient, 
        configurationService: HypothesisConfigurationService,
        idService: IdService,
        private pendingRequestsService: PendingRequestsService) {
            this.settings = configurationService.getConfiguration();
            this.routeCache = undefined;
            this.linkCache = undefined;
            this.clientId = idService.getGlobalClientId();
    }

    getJSON(url: string): Observable<any> {
        const httpClient = this.$http;
        return this.pendingRequestsService.watchRequest(
            httpClient.get<any>(url, {observe: 'response', headers: new HttpHeaders({'X-Client-Id': this.clientId})}).pipe(
                mergeMap((response: HttpResponse<any>) => { 
                    if (response.status !== 200) {
                        return throwError(() => new Error(`Fetching ${url} failed`));
                    }
                    return of(response.body!);
                })
            )
        );
    }

    /**
    * Fetch and cache API route metadata.
    *
    * Routes are fetched without any authentication and therefore assumed to be
    * the same regardless of whether the user is authenticated or not.
    *
    * @return {Observable<any>} - Map of routes to route metadata.
    */
    routes(): Observable<any> {
        if (this.routeCache != undefined) return of(this.routeCache); 
        const self = this;
        return this.getJSON(self.settings.apiUrl || '').pipe(
            retry({
                count: 1,
                delay: 3000,
                resetOnSuccess: true
            }),
            map((index: any) => {
                return index['links'];
            }),
            mergeMap((links: any) => {
                self.routeCache = links;
                return of(links);
            }),
        );
    }

    /**
    * Fetch and cache service page links from the API.
    *
    * @return {Observable<any>} - Map of link name to URL
    */
    links(): Observable<any> {
        if (this.linkCache != undefined) return of(this.linkCache);
        const self = this;
        return this.routes().pipe(
            map((routes: any)  => {
                return self.getJSON(routes['links']['url']);
            }),
            mergeMap((linksUrl: any) => {
                self.linkCache = linksUrl;
                return linksUrl;
            })
        );
    }
}

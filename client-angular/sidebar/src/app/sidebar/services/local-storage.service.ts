'use strict';

import { Injectable } from "@angular/core";

/**
 * Fallback in-memory store if `localStorage` is not read/writable.
 */
export interface IInMemoryStorage {
    getItem(key: string): any;
    setItem(key: string, value: any): void;
    removeItem(key: string): void;
}

class InMemoryStorage implements IInMemoryStorage {
    private _store: {
        [propName: string]: any
    };

    constructor() {
        this._store = {};
    }

    getItem(key: string): any {
        return (key in this._store) ? this._store[key] : null;
    }

    setItem(key: string, value: any) {
        this._store[key] = value;
    }

    removeItem(key: string) {
        delete this._store[key];
    }
}

export interface ILocalStorageService {
    getItem(key: string): any;
    getObject(key: string): any;
    setItem(key: string, value: any): void;
    setObject(key: string, value: any): void;
    removeItem(key: string): void;
}

/**
 * A wrapper around the `localStorage` API which provides a fallback to
 * in-memory storage in browsers that block access to `window.localStorage`.
 * in third-party iframes.
 */
@Injectable()
export class LocalStorageService implements ILocalStorageService {
    private storage: IInMemoryStorage | Storage;

    constructor() {

        try {
            const testKey = 'hypothesis.testKey';
            // Test whether we can read/write localStorage.
            this.storage = window.localStorage;
            window.localStorage.setItem(testKey, testKey);
            window.localStorage.getItem(testKey);
            window.localStorage.removeItem(testKey);
        } catch (e) {
            this.storage = new InMemoryStorage();
        }
    }

    getItem(key: string): any {
        return this.storage.getItem(key);
    }

    getObject(key: string): any {
        var item = this.storage.getItem(key);
        return item ? JSON.parse(item) : null;
    }

    setItem(key: string, value: any) {
        this.storage.setItem(key, value);
    }

    setObject(key: string, value: any) {
        const repr: string = JSON.stringify(value);
        this.storage.setItem(key, repr);
    }

    removeItem(key: string) {
        this.storage.removeItem(key);
    }
}
'use strict';

import { Injectable } from '@angular/core';

const minute = 60;
const hour = minute * 60;

type ITimeFormat = {
  format: (date?: number | Date) => string;
}

type ITimeIntl = {
  DateTimeFormat?: any;
}

type BreakpointFormat = (date: Date, now: Date, Intl?: ITimeIntl) => string;

type BreakpointTestFunction = (date: Date, now: Date) => boolean;

type Breakpoint = {
  test: BreakpointTestFunction,
  format: BreakpointFormat,
  nextUpdate: number | null
}

function lessThanThirtySecondsAgo(date: Date, now: Date): boolean {
  return ((now.getTime() - date.getTime()) < 30 * 1000);
}

function lessThanOneMinuteAgo(date: Date, now: Date): boolean  {
  return ((now.getTime() - date.getTime()) < 60 * 1000);
}

function lessThanOneHourAgo(date: Date, now: Date): boolean  {
  return ((now.getTime() - date.getTime()) < (60 * 60 * 1000));
}

function lessThanOneDayAgo(date: Date, now: Date): boolean  {
  return ((now.getTime() - date.getTime()) < (24 * 60 * 60 * 1000));
}

function thisYear(date: Date, now: Date): boolean  {
  return date.getFullYear() === now.getFullYear();
}

function delta(date: Date, now: Date): number  {
  return Math.round((now.getTime() - date.getTime()) / 1000);
}

function nSec(date: Date, now: Date): string  {
  return '{} secs'.replace('{}', Math.floor(delta(date, now)).toString());
}

function nMin(date: Date, now: Date): string {
  var n = Math.floor(delta(date, now) / minute);
  var template = '{} min';

  if (n > 1) {
    template = template + 's';
  }

  return template.replace('{}', n.toString());
}

function nHr(date: Date, now: Date): string {
  var n = Math.floor(delta(date, now) / hour);
  var template = '{} hr';

  if (n > 1) {
    template = template + 's';
  }

  return template.replace('{}', n.toString());
}

export type TimeServiceDate = Date | number | string | null;

export interface ITimeService {
  /**
   * Starts an interval whose frequency decays depending on the relative
   * age of 'date'.
   *
   * This can be used to refresh parts of a UI whose
   * update frequency depends on the age of a timestamp.
   *
   * @return {Function} A function that cancels the automatic refresh.
   */
  decayingInterval(date: TimeServiceDate, callback: Function): VoidFunction;

  /**
   * Formats a date as a string relative to the current date.
   *
   * @param {number} date - The absolute timestamp to format.
   * @return {string} A 'fuzzy' string describing the relative age of the date.
   */
  toFuzzyString(date: TimeServiceDate, Intl?: ITimeIntl | null): string;

  nextFuzzyUpdate(date: TimeServiceDate): number | null;
}

@Injectable()
export class TimeService implements ITimeService {
  
  // Cached DateTimeFormat instances,
  // because instantiating a DateTimeFormat is expensive.  
  private formatters: any;
  private breakpoints: Breakpoint[];
  
  constructor() {
    this.formatters = {}

    const _dayAndMonth = (date: Date, now: Date, Intl?: ITimeIntl) => this.dayAndMonth(date, now, Intl);
    const _dayAndMonthAndYear = (date: Date, now: Date, Intl?: ITimeIntl) => this.dayAndMonthAndYear(date, now, Intl);

    this.breakpoints = [
      {
        test: lessThanThirtySecondsAgo,
        format: function () {return 'Just now';},
        nextUpdate: 1,
      },
      {
        test: lessThanOneMinuteAgo,
        format: nSec,
        nextUpdate: 1,
      },
      {
        test: lessThanOneHourAgo,
        format: nMin,
        nextUpdate: minute,
      },
      {
        test: lessThanOneDayAgo,
        format: nHr,
        nextUpdate: hour,
      },
      {
        test: thisYear,
        format: _dayAndMonth.bind(this),
        nextUpdate: null,
      },
      {
        test: function () {return true;},
        format: _dayAndMonthAndYear.bind(this),
        nextUpdate: null,
      },
    ];
  }

  /**
   * Efficiently return `date` formatted with `options`.
   *
   * This is a wrapper for Intl.DateTimeFormat.format() that caches
   * DateTimeFormat instances because they're expensive to create.
   * Calling Date.toLocaleDateString() lots of times is also expensive in some
   * browsers as it appears to create a new formatter for each call.
   *
   * @returns {string}
   *
   */
  private format(date: Date, options: any, Intl?: ITimeIntl | null): string {
    // If the tests have passed in a mock Intl then use it, otherwise use the
    // real one.
    const _Intl: ITimeIntl = !Intl ? window.Intl : Intl;

    // IE < 11, Safari <= 9.0.
    if(!_Intl || !_Intl.DateTimeFormat) return date.toDateString();

    const key: string = JSON.stringify(options);
    if (!this.formatters[key]) {
      this.formatters[key] = new _Intl.DateTimeFormat(undefined,options);
    }
    const formatter = this.formatters[key] as ITimeFormat;
    return (formatter).format(date);
  }

  private dayAndMonth(date: Date, now: Date, Intl?: ITimeIntl): string {
    return this.format(date, {month: 'short', day: 'numeric'}, Intl);
  }
  
  private dayAndMonthAndYear(date: Date, now: Date, Intl?: ITimeIntl): string {
    return this.format(date, {day: 'numeric', month: 'short', year: 'numeric'}, Intl);
  }

  private getBreakpoint(date: Date | number | string, now: Date): Breakpoint | null {
    // Turn the given ISO 8601 string into a Date object.
    const _date = new Date(date);
    const breakpoint: Breakpoint | undefined = this.breakpoints.find((breakpoint: Breakpoint) => breakpoint.test(_date, now))
    return breakpoint || null;
  }

  nextFuzzyUpdate(date: TimeServiceDate): number | null {
    if (!date) return null;
  
    const breakpoint = this.getBreakpoint(date, new Date());
    if (breakpoint === null) return null;
    if (breakpoint.nextUpdate === null) return null;

    var secs: number = breakpoint.nextUpdate;
    // We don't want to refresh anything more often than 5 seconds
    secs = Math.max(secs, 5);
  
    // setTimeout limit is MAX_INT32=(2^31-1) (in ms),
    // which is about 24.8 days. So we don't set up any timeouts
    // longer than 24 days, that is, 2073600 seconds.
    secs = Math.min(secs, 2073600);
  
    return secs;    
  }

  decayingInterval(date: TimeServiceDate, callback: Function): VoidFunction {
    const self: ITimeService = this;
    var timer: number;

    const update = function () {
      const fuzzyUpdate: number | null = self.nextFuzzyUpdate(date);
      if (fuzzyUpdate === null) return;

      const nextUpdate = (1000 * fuzzyUpdate) + 500;
      timer = window.setTimeout(() => {
        callback(date);
        update();
      }, nextUpdate);
    };
    update();
  
    return function () {
      clearTimeout(timer);
    };    
  }

  toFuzzyString(date: TimeServiceDate, Intl?: ITimeIntl | null): string {
    if (!date) return '';
    const now = new Date();
    const breakpoint = this.getBreakpoint(date, now);
    if (!breakpoint) return '';
    return breakpoint.format(new Date(date), now, Intl || undefined);  
  }
}
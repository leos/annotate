'use strict';

import * as events from '../events';
import { resolve } from '../util/url.util';
import { serviceConfig } from '../service-config';
import { TokenInfo } from '../models/oauth-auth.model';
import OAuthClient, { IOAuthClient, openAuthPopupWindow } from '../util/oauth-client.util';
import {ApiRoutesService} from './api-routes.service';
import {FlashService} from './flash.service';
import {LocalStorageService} from './local-storage.service';
import { HypothesisServiceConfig, IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Injectable } from '@angular/core';
import {ScopeService} from './scope.service';
import {
    Observable,
    Subscriber,
    map,
    mergeMap,
    tap,
    zip,
    catchError,
    of,
    throwError,
    from
} from 'rxjs';
import {PendingRequestsService} from './pending-requests.service';
import { HttpClient } from '@angular/common/http';
import {BridgeService} from '../services/bridge.service';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';
import { IdService } from './id.service';

/**
 * @typedef RefreshOptions
 * @property {boolean} persist - True if access tokens should be persisted for
 *   use in future sessions.
 */
type RefreshOptions = {
    persist?: boolean;
}

type TokenGetter = () => Observable<string>;

export interface IOAuthAuthService {
    tokenGetter: TokenGetter;

    /**
    * Show an error message telling the user that the access token has expired.
    */
    showAccessTokenExpiredErrorMessage(message: string): void;

    /**
    * Return the storage key used for storing access/refresh token data for a given
    * annotation service.
    */
    storageKey(): string;

    /**
    * Fetch the last-saved access/refresh tokens for `authority` from local
    * storage.
    */
    loadToken(): TokenInfo | null;

    /**
    * Persist access & refresh tokens for future use.
    */
    saveToken(token: TokenInfo): void;

    /**
    * Exchange the refresh token for a new access token and refresh token pair.
    *
    * @param {string} refreshToken
    * @param {RefreshOptions} options
    * @return {Promise<TokenInfo>} Promise for the new access token
    */
    refreshAccessToken(refreshToken: string, options: RefreshOptions): Observable<TokenInfo>;

    getOauthClient(): Observable<IOAuthClient>;

    /**
    * Login to the annotation service using OAuth.
    *
    * This displays a popup window which allows the user to login to the service
    * (if necessary) and then responds with an auth code which the client can
    * then exchange for access and refresh tokens.
    */
    login(): Observable<any>;

    /**
    * Log out of the service (in the client only).
    *
    * This revokes and then forgets any OAuth credentials that the user has.
    */
    logout(): Observable<any>;
}

export interface ILeosOAuthAuthService extends IOAuthAuthService {
}

/**
 * Authorization service.
 *
 * This service is responsible for acquiring access tokens for making API
 * requests and making them available via the `tokenGetter()` method.
 *
 * Access tokens are acquired via the OAuth authorization flow, loading valid
 * tokens from a previous session or, on some websites, by exchanging a grant
 * token provided by the host page.
 *
 * Interaction with OAuth endpoints in the annotation service is delegated to
 * the `OAuthClient` class.
 */
@Injectable()
export class OAuthAuthService implements IOAuthAuthService {
    /**
     * Authorization code from auth popup window.
     */
    protected authCode?: string;

    /**
     * Token info retrieved via `POST /api/token` endpoint.
     *
     * Resolves to `null` if the user is not logged in.
     *
     * @type {Promise<TokenInfo|null>}
     */
    protected tokenInfoPromise?: Observable<TokenInfo|null>;

    protected client?: IOAuthClient;

    protected clientId: string;

    /**
     * Absolute URL of the `/api/token` endpoint.
     */
    protected tokenUrl: string;

    public tokenGetter: TokenGetter;

    protected settings: IHypothesisJsonConfig;
    protected $window: Window;

    constructor(protected $http: HttpClient,
        protected apiRoutes: ApiRoutesService,
        protected flash: FlashService,
        protected localStorage: LocalStorageService,
        settingsService: HypothesisConfigurationService,
        protected rootScope: ScopeService,
        protected idService: IdService,
        protected pendingRequestService: PendingRequestsService) {
            this.$window = window;
            this.clientId = idService.getGlobalClientId();
            this.settings = settingsService.getConfiguration();
            this.tokenUrl = resolve('token', this.settings.apiUrl || '');

            const self = this;
            this.tokenGetter = (): Observable<string> => self._defaultTokenGetter();

            const listenForTokenStorageEvents = ({ key }: {key?: string | null}) => {
                if (key === self.storageKey()) {
                    // Reset cached token information. Tokens will be reloaded from storage
                    // on the next call to `tokenGetter()`.
                    self.tokenInfoPromise = undefined;
                    self.rootScope.$broadcast(events.OAUTH_TOKENS_CHANGED);
                }
            }

            /**
            * Listen for updated access & refresh tokens saved by other instances of the
            * client.
            */
            this.$window.addEventListener('storage', listenForTokenStorageEvents);
    }

    /**
    * Show an error message telling the user that the access token has expired.
    */
    showAccessTokenExpiredErrorMessage(message: string) {
        this.flash.error(
            message,
            'Hypothesis login lost',
            {
                extendedTimeOut: 0,
                tapToDismiss: false,
                timeOut: 0,
            }
        );
    }

    /**
    * Return the storage key used for storing access/refresh token data for a given
    * annotation service.
    */
    storageKey(): string {
        // Use a unique key per annotation service. Currently OAuth tokens are only
        // persisted for the default annotation service. If in future we support
        // logging into other services from the client, this function will need to
        // take the API URL as an argument.
        var apiDomain = new URL(this.settings.apiUrl || '').hostname;
        // Percent-encode periods to avoid conflict with section delimeters.
        apiDomain = apiDomain.replace(/\./g, '%2E');
        return `hypothesis.oauth.${apiDomain}.token`;
    }

    /**
     * Fetch the last-saved access/refresh tokens for `authority` from local
     * storage.
     */
    loadToken(): TokenInfo | null {
        var token: any = this.localStorage.getObject(this.storageKey());

        if (!token ||
            typeof token.accessToken !== 'string' ||
            typeof token.refreshToken !== 'string' ||
            typeof token.expiresAt !== 'number') {
          return null;
        }

        return {
          accessToken: token.accessToken,
          refreshToken: token.refreshToken,
          expiresAt: token.expiresAt,
        };
    }

    /**
    * Persist access & refresh tokens for future use.
    */
    saveToken(token: TokenInfo) {
        this.localStorage.setObject(this.storageKey(), token);
    }

    /**
    * Exchange the refresh token for a new access token and refresh token pair.
    *
    * @param {string} refreshToken
    * @param {RefreshOptions} options
    * @return {Observable<TokenInfo>} Observable for the new access token
    */
    refreshAccessToken(refreshToken: string, options: RefreshOptions): Observable<TokenInfo> {
        const self = this;
        return this._oauthClient().pipe(
            mergeMap((client: IOAuthClient) => client.refreshToken(refreshToken)),
            map((tokenInfo: TokenInfo) => {
                if (options.persist) {
                    self.saveToken(tokenInfo);
                }
                return tokenInfo;
            })
        );
    }

    getOauthClient(): Observable<IOAuthClient> {
        return this._oauthClient();
    }

    protected _oauthClient(): Observable<IOAuthClient> {
        if (this.client) {
            const client: IOAuthClient = this.client;
            return new Observable((subscriber: Subscriber<IOAuthClient>) => {
                subscriber.next(client);
                subscriber.complete();
            });
        }

        const self = this;
        return this.apiRoutes.links().pipe(
            map((links: any) => {
                self.client = new OAuthClient(self.$http, {
                    clientId: self.settings.oauthClientId,
                    xClientId: self.clientId,
                    authorizationEndpoint: links['oauth.authorize'],
                    revokeEndpoint: links['oauth.revoke'],
                    tokenEndpoint: self.tokenUrl,
                    context: self.settings.context,
                }, this.pendingRequestService);
                return self.client;
            })
        );
    }

    /**
    * Login to the annotation service using OAuth.
    *
    * This displays a popup window which allows the user to login to the service
    * (if necessary) and then responds with an auth code which the client can
    * then exchange for access and refresh tokens.
    */
    login(): Observable<any> {
        const self = this;
        const authWindow: Window = openAuthPopupWindow(this.$window)!;

        return this._oauthClient().pipe(
            mergeMap((client: IOAuthClient) => client.authorize(self.$window, authWindow)),
            tap((code: string | undefined) => {
                // Save the auth code. It will be exchanged for an access token when the
                // next API request is made.
                this.authCode = code;
                this.tokenInfoPromise = undefined;
            })
        )
    }

    /**
    * Log out of the service (in the client only).
    *
    * This revokes and then forgets any OAuth credentials that the user has.
    */
    logout(): Observable<any> {
        const self = this;
        return zip([this.tokenInfoPromise!, this._oauthClient()]).pipe(
            map(([token, client]) => {
                return client.revokeToken(token?.accessToken || '');
            }),
            tap(() => {
                self.tokenInfoPromise = of(null);
                self.localStorage.removeItem(self.storageKey());
            })
        );
    }

    /**
    * Retrieve an access token for the API.
    *
    * @return {Observable <string>} The API access token.
    */
    protected _defaultTokenGetter(): Observable<string> {
        const self = this;

        if (this.tokenInfoPromise == undefined) {
            const cfg: HypothesisServiceConfig = serviceConfig(self.settings)!;

            // Check if automatic login is being used, indicated by the presence of
            // the 'grantToken' property in the service configuration.
            if (cfg && cfg.grantToken != undefined) {
                if (cfg.grantToken) {
                    // User is logged-in on the publisher's website.
                    // Exchange the grant token for a new access token.
                    this.tokenInfoPromise = this._oauthClient().pipe(
                        mergeMap((client: IOAuthClient) => {
                            return client.exchangeGrantToken(cfg.grantToken || '');
                        }),
                        catchError((err: any) => {
                            self.showAccessTokenExpiredErrorMessage('You must reload the page to annotate.');
                            return throwError(() => new Error(err));
                        })
                    );
                } else {
                    // User is anonymous on the publisher's website.
                    this.tokenInfoPromise = of(null);
                }
            } else if (this.authCode) {
                // Exchange authorization code retrieved from login popup for a new
                // access token.
                const code = this.authCode;
                // Auth codes can only be used once.
                this.authCode = undefined;

                this.tokenInfoPromise = this._oauthClient().pipe(
                    mergeMap((client: IOAuthClient) => client.exchangeAuthCode(code)),
                    map((tokenInfo: TokenInfo) => {
                        self.saveToken(tokenInfo);
                        return tokenInfo;
                    })
                )
            } else {
                // Attempt to load the tokens from the previous session.
                this.tokenInfoPromise = of(this.loadToken());
            }
        }

        const origToken: Observable<TokenInfo | null> = self.tokenInfoPromise!;
        return this.tokenInfoPromise.pipe(
            mergeMap((tokenInfo: TokenInfo|null) => {
                // No token available. User will need to log in.
                if (!tokenInfo) return throwError(() => new Error('No token available. Login needed.'));

                // A token refresh has been initiated via a call to `refreshAccessToken`
                // below since `tokenGetter()` was called.
                if (origToken !== self.tokenInfoPromise) return self._defaultTokenGetter();

                if (Date.now() <= tokenInfo.expiresAt) return of(tokenInfo.accessToken);

                var shouldPersist = true;
                // If we are using automatic login via a grant token, do not persist the
                // initial access token or refreshed tokens.
                const cfg = serviceConfig(self.settings);
                if (cfg && typeof cfg.grantToken !== 'undefined') {
                    shouldPersist = false;
                }

                self.tokenInfoPromise = self.refreshAccessToken(tokenInfo.refreshToken, {persist: shouldPersist}).pipe(
                    mergeMap((token: TokenInfo) => {
                        // Sanity check that prevents an infinite loop. Mostly useful in
                        // tests.
                        if (Date.now() > token.expiresAt) {
                            return throwError(() =>new Error('Refreshed token expired in the past'));
                        }
                        return of(token);
                    }),
                    catchError((err: any) => throwError(() => new Error(err)))
                );
                return self._defaultTokenGetter();
            })
        );
    }
}

@Injectable()
export class LeosOAuthAuthService extends OAuthAuthService implements ILeosOAuthAuthService {
    private frameConnected: boolean;
    private frameConnectedError?: any;
    private hostToken?: TokenInfo;

    constructor($http: HttpClient,
        apiRoutes: ApiRoutesService,
        flash: FlashService,
        localStorage: LocalStorageService,
        settingsService: HypothesisConfigurationService,
        rootScope: ScopeService,
        pendingRequestService: PendingRequestsService,
        idService: IdService,
        private bridge: BridgeService) {
            super($http, apiRoutes, flash, localStorage, settingsService, rootScope, idService, pendingRequestService);
            this.frameConnected = false;
            this.frameConnectedError = undefined;
            this.hostToken = undefined;

            const self = this;
            this.tokenGetter = (): Observable<string> => self._defaultLeosTokenGetter();
    }

    override storageKey(): string {
        const apiDomain = new URL(this.settings.apiUrl || '').hostname;
        return `hypothesis.oauth.${apiDomain.replace(/\./g, '%2E')}.token`;        
    }

    override saveToken(token: TokenInfo): void {
        this.localStorage.setObject(this.storageKey(), token);
    }

    private requestHostToken(): Observable<[string]> {
        const self = this;
        return new Observable((subscriber: Subscriber<[string]>) => {
            let promiseTimeout: number = window.setTimeout(() => {
                subscriber.error(new Error('timeout'));
            }, 1000);
            self.bridge.call('requestSecurityToken', (error: any, result: [string]) => {
                window.clearTimeout(promiseTimeout);
                if (error) {
                    subscriber.error(error);
                } else {
                    subscriber.next(result);
                }
                subscriber.complete();
            })
        });
    }

    private waitForFrameConnection(): Observable<void> {
        const self = this;
        return new Observable((subscriber: Subscriber<void>) => {
            const onSuccess = () => {
                subscriber.next();
                subscriber.complete();
            }
            const onError = (error: any) => {
                subscriber.error({error: error, skipImmediately: true });
                subscriber.complete();
            }

            if (self.frameConnected) {
                onSuccess();
                return;
            }
            if (self.frameConnectedError != undefined) {
                onError(self.frameConnectedError);
                return;
            }     
            self.rootScope.$on(events.FRAME_CONNECTED, () => {
                self.frameConnected = true;
                onSuccess();
            });
            self.rootScope.$on(events.FRAME_CONNECT_ERROR, (event: any, error: any) => {
                self.frameConnectedError = error;
                onError(error);
            });            
        });
    }

    private getHostToken(): Observable<[string]> {
        const self = this;
        return this.waitForFrameConnection().pipe(
            mergeMap(() => {
                return self.requestHostToken();
            }),
            catchError((err: any) => {
                return throwError(() => new Error(err));
            })
        );
    }

    protected _defaultLeosTokenGetter(): Observable<string> {
        if (this.hostToken && Date.now() <= this.hostToken.expiresAt) {
            return of(this.hostToken.accessToken);
        }

        const self = this;
        return zip([this.getHostToken(), this.getOauthClient()]).pipe(
            mergeMap((results) => {
                if (!results) return throwError(() => new Error("No token received"));
                const grantToken: string | undefined = (results.length > 1 && results[0].length > 0) ? results[0][0] : undefined;
                const client: IOAuthClient | undefined = (results.length > 1) ? results[1] : undefined ;

                if (grantToken == undefined) {
                    return throwError(() => new Error("Token is empty"));
                }
                if (client == undefined) {
                    return throwError(() => new Error("OAuth client is missing"));
                }
                return client.exchangeGrantToken(grantToken);
            }),
            mergeMap((token: TokenInfo) => {
                self.hostToken = token;
                self.saveToken(token);
                return of(token.accessToken);
            }),
            catchError((err: Error) => {
                console.debug('Error getting host token:', err.message);
                //self.bridge.call('showSidebar');
                //self.flash.error('An error occurred during authorization. Please reload the document.', 'Authorization failed');
                return self._defaultTokenGetter();
            })
        );
    }
}
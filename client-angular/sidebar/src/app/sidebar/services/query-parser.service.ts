/*
 * decaffeinate suggestions:
 * DS205: Consider reworking code to avoid use of IIFEs
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */

import { IStreamFilterService } from './stream-filter.service';
import { SearchFilterFacet, SearchFilterFacets } from '../models/search-filter.model';
import { StreamFilterOptions } from '../models/stream-filter.model';
import { Injectable } from '@angular/core';

type RuleFormatter = (value: any) => any;

interface IQueryParserRule {
    /**
     * to format the value (optional)
     */
    formatter?: RuleFormatter

    /**
     * json path mapping to the annotation field
     */
    path?: string | string[];

    /**
     * true|false (default: false)
     */
    case_sensitive?: boolean;

    /**
     * and|or for multiple values should it threat them as 'or' or 'and' (def: or)
     */
    and_or?: string;

    /**
     * if given it'll use this operator regardless of other circumstances
     */
    operator?: string;

    /**
     * backend specific options
     */
    options?: StreamFilterOptions;
}

class QueryParserRule implements IQueryParserRule {
    constructor(private settings: IQueryParserRule) {
        if (!this.settings) throw new Error("Empty settings for rules are not allowed");
        if (!this.settings.path) throw new Error("'path' property is missing in rule settings");
        if (!this.settings.and_or) throw new Error("'and_or' property is missing in rule settings");
    }

    get formatter(): RuleFormatter | undefined {
        return this.settings.formatter;
    }

    get path(): string | string[] | undefined {
        return this.settings.path;
    }

    get case_sensitive(): boolean {
        return !!this.settings.case_sensitive;
    }

    get and_or(): string | undefined {
        return this.settings.and_or;
    }

    get operator(): string | undefined {
        return this.settings.operator;
    }

    get options(): StreamFilterOptions | undefined {
        return this.settings.options;
    }
}

type QueryParserRules = {
    [propName: string]: IQueryParserRule;
    user: IQueryParserRule,
    text: IQueryParserRule,
    tag: IQueryParserRule,
    quote: IQueryParserRule,
    uri: IQueryParserRule,
    since: IQueryParserRule,
    any: IQueryParserRule
}

// This class will process the results of search and generate the correct filter
// It expects the following dict format as rules
// { facet_name : {
//      formatter: to format the value (optional)
//      path: json path mapping to the annotation field
//      case_sensitive: true|false (default: false)
//      and_or: and|or for multiple values should it threat them as 'or' or 'and' (def: or)
//      operator: if given it'll use this operator regardless of other circumstances
//
//      options: backend specific options
//      options.es: elasticsearch specific options
//      options.es.query_type : can be: simple (term), query_string, match, multi_match
//         defaults to: simple, determines which es query type to use
//      options.es.cutoff_frequency: if set, the query will be given a cutoff_frequency for this facet
//      options.es.and_or: match and multi_match queries can use this, defaults to and
//      options.es.match_type: multi_match query type
//      options.es.fields: fields to search for in multi-match query
// }
// The models is the direct output from visualsearch
export interface IQueryParserService {
    populateFilter(filter: IStreamFilterService, query: SearchFilterFacets): void;
}

@Injectable()
export class QueryParserService implements IQueryParserService {
    private rules: QueryParserRules;

    constructor() {
        this.rules = {
            user: new QueryParserRule({
                path: '/user',
                and_or: 'or',
            }),
            text: new QueryParserRule({
                path: '/text',
                and_or: 'and',
            }),
            tag: new QueryParserRule({
                path: '/tags',
                and_or: 'and',
            }),
            quote: new QueryParserRule({
                path: '/quote',
                and_or: 'and',
            }),
            uri: new QueryParserRule({
                formatter(uri) {
                    return uri.toLowerCase();
                },
                path: '/uri',
                and_or: 'or',
                options: {
                    es: {
                        query_type: 'match',
                        cutoff_frequency: 0.001,
                        and_or: 'and',
                    },
                },
            }),
            since: new QueryParserRule({
                formatter(past: string) {
                    let seconds: number = 0;
                    switch (past) {
                        case '5 min': seconds = 5*60; break;
                        case '30 min': seconds = 30*60; break;
                        case '1 hour': seconds = 60*60; break;
                        case '12 hours': seconds = 12*60*60; break;
                        case '1 day': seconds = 24*60*60; break;
                        case '1 week': seconds = 7*24*60*60; break;
                        case '1 month': seconds = 30*24*60*60; break;
                        case '1 year': seconds = 365*24*60*60; break;
                    }
                    return new Date(new Date().valueOf() - (seconds*1000));
                },
                path: '/created',
                and_or: 'and',
                operator: 'ge',
            }),
            any: new QueryParserRule({
                and_or: 'and',
                path:   ['/quote', '/tags', '/text', '/uri', '/user'],
                options: {
                    es: {
                        query_type: 'multi_match',
                        match_type: 'cross_fields',
                        and_or: 'and',
                        fields:   ['quote', 'tags', 'text', 'uri.parts', 'user'],
                    },
                },
            }),
        };
    }

    populateFilter(filter: IStreamFilterService, query: SearchFilterFacets) {
        // Populate a filter with a query object
        const categories: string[] = Object.keys(query);
        for (const category in categories) {
            const value: SearchFilterFacet = query[category];
            if (this.rules[category] === null) { continue; }

            let { terms } = value;
            if (!terms.length) { continue; }

            const rule: IQueryParserRule = this.rules[category];
            // Now generate the clause with the help of the rule
            const case_sensitive: boolean = !!rule.case_sensitive;
            const and_or: string = rule.and_or || 'or';
            const mapped_field: string | string[] = rule.path || '/' + category;

            let oper_part: string;
            let value_part: any[];
            if (and_or === 'or') {
                oper_part = rule.operator ? rule.operator : 'match_of';

                value_part = [];
                for (const term of terms) {
                    const formattedValue: any = rule.formatter ? rule.formatter(term) : term;
                    value_part.push(formattedValue);
                }
                if (typeof mapped_field === 'string') {
                    filter.addClause(mapped_field, oper_part, value_part, case_sensitive, rule.options || {});
                } else {
                    for(const field of mapped_field) {
                        filter.addClause(field, oper_part, value_part, case_sensitive, rule.options || {});
                    }
                }
            } else {
                oper_part = rule.operator || 'matches';
                for (const val of terms) {
                    value_part = rule.formatter ? rule.formatter(val) : val;
                    if (typeof mapped_field === 'string') {
                        filter.addClause(mapped_field, oper_part, value_part, case_sensitive, rule.options || {});
                    } else {
                        for(const field of mapped_field) {
                            filter.addClause(field, oper_part, value_part, case_sensitive, rule.options || {});
                        }
                    }
                }
            }
        }
    }
}
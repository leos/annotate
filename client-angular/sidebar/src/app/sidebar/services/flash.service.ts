'use strict';

import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

export interface IFlashService {
    readonly info: any;
    readonly success: any;
    readonly warning: any;
    readonly error: any;
}

/**
 * A service for displaying "flash" notification messages.
 */
@Injectable()
export class FlashService implements IFlashService {
    readonly info: any;
    readonly success: any;
    readonly warning: any;
    readonly error: any;

    constructor(toastr: ToastrService) {
        this.info = toastr.info.bind(toastr);
        this.success = toastr.success.bind(toastr);
        this.warning = toastr.warning.bind(toastr);
        this.error = toastr.error.bind(toastr);
    }
}

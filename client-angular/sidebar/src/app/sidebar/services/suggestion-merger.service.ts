'use strict';

import {BridgeService} from "../services/bridge.service";
import { Suggestion } from '../../../../../shared/models/annotation.model';
import { Observable, Subscriber, of, map, throwError, mergeMap, catchError, zip, zipAll } from 'rxjs';
import { Injectable } from '@angular/core';

// Prevent Babel inserting helper code after `@ngInject` comment below which breaks browserify-ngannotate.
var unused; // eslint-disable-line

const TIMEOUT_REQUEST_ANNOTATION_ANCHOR_IN_MS = 3000;
const TIMEOUT_REQUEST_MERGE_SUGGESTION_IN_MS = 60000;

export interface ISuggestionMergerService {
    checkValidityOfSuggestion(suggestion: Suggestion): Observable<Suggestion>;
    removeMergedSuggestions(suggestions: Suggestion[]): Suggestion[];
    processSuggestionMerging(suggestion: Suggestion): Observable<Suggestion>;
    processSuggestionMergings(suggestions: Suggestion[]): Observable<Suggestion[]>;
}

@Injectable()
export class SuggestionMergerService implements ISuggestionMergerService {
    constructor(private bridge: BridgeService) {}

    _getAnnotationAnchor(suggestion: Suggestion): Observable<Suggestion> {
        const bridge = this.bridge;
        return new Observable((subscriber: Subscriber<Suggestion>) => {
            const timeout = setTimeout((() => subscriber.error('Get annotation annchor timeout')), TIMEOUT_REQUEST_ANNOTATION_ANCHOR_IN_MS);
            bridge.call('requestAnnotationAnchor', suggestion, (error: any, [anchor] : [any]) => {
                clearTimeout(timeout);
                suggestion.anchorInfo = anchor;
                error ? subscriber.error(error) : subscriber.next(suggestion);
                subscriber.complete();
            });
        });
    }

    checkValidityOfSuggestion(suggestion: Suggestion): Observable<Suggestion> {
        if (!suggestion || !suggestion.anchorInfo) {
            return throwError(() => new Error('Suggestion is not valid.'));
        }
        return of(suggestion);
    }

    _sendMergeSuggestionRequest(suggestion: Suggestion): Observable<Suggestion> {
        const bridge = this.bridge;
        const anchor = suggestion.anchorInfo;
        anchor.newText = suggestion.text;

        return new Observable((subscriber: Subscriber<Suggestion>) => {
            const timeout = setTimeout((() => subscriber.error('Send merge suggestion timeout')), TIMEOUT_REQUEST_MERGE_SUGGESTION_IN_MS);
            bridge.call('requestMergeSuggestion', anchor, (error: any, [result]: [any]) => {
                clearTimeout(timeout);
                if (result && result.result && result.result !== 'SUCCESS') {
                    error = result;
                }
                error ? subscriber.error(error) : subscriber.next(suggestion);
                subscriber.complete();
            });
        });
    }

    _sendMergeSuggestionsRequest(suggestions: Suggestion[]): Observable<Suggestion[]> {
        const bridge = this.bridge;

        const anchors = suggestions.map((suggestion) => {
            const anchorInfo = Object.assign({}, suggestion.anchorInfo);
            anchorInfo.newText = suggestion.text;
            return anchorInfo;
        });

        return new Observable((subscriber: Subscriber<Suggestion[]>) => {
            const timeout = setTimeout((() => subscriber.error('Send merge suggestions timeout')), TIMEOUT_REQUEST_MERGE_SUGGESTION_IN_MS);
            bridge.call('requestMergeSuggestions', anchors, (error: any, [results]: [any]) => {
                clearTimeout(timeout);

                if (! results || ! Array.isArray(results)) {
                    subscriber.error("No valid result received from editor");
                }

                if (results.length !== suggestions.length) {
                    subscriber.error('Results have a different length than the provided input.');
                }

                results.forEach((anchor: any, index: number) => {
                  suggestions[index].hasBeenMerged = anchor.result === 'SUCCESS';
                });

                error ? subscriber.error(error) : subscriber.next(suggestions);
                subscriber.complete();
            });
        });
    }

    removeMergedSuggestions(suggestions: Suggestion[]): Suggestion[] {
        return suggestions.filter((suggestion: Suggestion) => suggestion.hasBeenMerged)
            .map((suggestion: Suggestion) => {
                delete suggestion.hasBeenMerged;
                return suggestion;
            });
    }

    processSuggestionMerging(suggestion: Suggestion): Observable<Suggestion> {
        const self = this;
        return this._getAnnotationAnchor(suggestion).pipe(
           mergeMap((suggestion: Suggestion) => self.checkValidityOfSuggestion(suggestion)),
           mergeMap((suggestion: Suggestion) => self._sendMergeSuggestionRequest(suggestion)),
           catchError((err: any) => throwError(() => new Error(err)))
        );
    }

    processSuggestionMergings(suggestions: any[]): Observable<any[]> {
        const self = this;
        return zip(suggestions.map((suggestion: Suggestion) => self._getAnnotationAnchor(suggestion)))
            .pipe(
                mergeMap((suggestions: Suggestion[]) => zip(
                    suggestions.map((suggestion: Suggestion) => self.checkValidityOfSuggestion(suggestion))
                )),
                mergeMap((suggestions: Suggestion[]) => self._sendMergeSuggestionsRequest(suggestions)),
                map((suggestions: Suggestion[]) => self.removeMergedSuggestions(suggestions)),
                catchError((error: any) => throwError(() => new Error(error)))
            );
    }
}
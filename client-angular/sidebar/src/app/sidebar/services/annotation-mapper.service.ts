'use strict';

import * as events from '../events';
import * as authorityChecker from '../authority-checker';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import {ApiService} from './api.service';
import { Annotation } from '../../../../../shared/models/annotation.model';
import {ScopeService,  IScopeService } from './scope.service';
import { Observable, map, zip } from 'rxjs';
import { Injectable } from '@angular/core';
import { HypothesisStoreService, IHypothesisStore } from '../store';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

function getExistingAnnotation(store: IHypothesisStore, id: string): any {
  return store.getState().annotations.find((annot: Annotation) => {
    return annot.id === id;
  });
}

//LEOS Change
function LEOS_processAnnotations(annotations: Annotation[], rootScope: IScopeService) {
  annotations.forEach(function (annotation) {
    if (annotation.group) {
      annotation.group = annotation.group.replace(' ', rootScope.getProperty('ANNOTATION_GROUP_SPACE_REPLACE_TOKEN'));
    }
  });
}

export interface IAnnotationMapperService {
    loadAnnotations(annotations: Annotation[], replies?: Annotation[]): void;
    LEOS_isToBeProcessed(annotation: Annotation): void;
    unloadAnnotations(annotations: Annotation[], reset?: boolean): void;
    createAnnotation(annotation: Annotation): Annotation;
    deleteAnnotation(annotation: Annotation): Observable<Annotation>;
    deleteAnnotations(annotations: Annotation[]): Observable<Annotation[]>;
    flagAnnotation(annot: Annotation): Observable<Annotation>;
    resetAnnotation(annotation: Annotation): Observable<Annotation>;
    resetAnnotations(annotations: Annotation[]): Observable<Annotation[]>;
    treatAnnotation(annotation: Annotation): Observable<Annotation>;
    treatAnnotations(annotations: Annotation[]): Observable<Annotation[]>;
    acceptSuggestion(annotation: Annotation): Observable<Annotation>;
    acceptSuggestions(annotations: Annotation[]): Observable<Annotation[]>;
    rejectSuggestion(annotation: Annotation): Observable<Annotation>;
    rejectSuggestions(annotations: Annotation[]): Observable<Annotation[]>;
}

// Wraps the annotation store to trigger events for the CRUD actions
@Injectable()
export class AnnotationMapperService implements IAnnotationMapperService {
    private settings: IHypothesisJsonConfig;

    constructor(private api: ApiService,
      private rootScope: ScopeService,
      settingsService: HypothesisConfigurationService,
      private store: HypothesisStoreService) {
        this.settings = settingsService.getConfiguration();
    }

    loadAnnotations(annotations: Annotation[], replies?: Annotation[]) {
        const self = this;
        const _annotations = annotations.concat(!replies ? [] : replies);
        //LEOS Change : remove white spaces from GROUP names
        LEOS_processAnnotations(_annotations, this.rootScope);

        var loaded: Annotation[] = [];
        _annotations.forEach(function (annotation: Annotation) {
          var existing = getExistingAnnotation(self.store, annotation.id || '');
          if (existing) {
            self.rootScope.$broadcast(events.ANNOTATION_UPDATED, annotation);
            return;
          }
          loaded.push(annotation);
        });

        this.rootScope.$broadcast(events.ANNOTATIONS_LOADED, loaded);
    }

    LEOS_isToBeProcessed(annotation: Annotation) {
        return authorityChecker.isISC(this.settings)
            && annotation && annotation.document
            && annotation.document.metadata['responseStatus'] !== undefined
            && annotation.document.metadata['responseStatus'] == 'SENT';
    }

    unloadAnnotations(annotations: Annotation[], reset?: boolean) {
        const self = this;
        let unloaded: Annotation[] = [];
        let toBeProcessed = false;
        annotations.forEach(function (annotation) {
            let existing = getExistingAnnotation(self.store, annotation.id || '');
            if (!!reset && existing && self.LEOS_isToBeProcessed(existing)) {
                toBeProcessed = true;
            } else if (existing && annotation !== existing) {
                annotation = Object.assign(annotation, existing);
                unloaded.push(annotation);
            } else {
                unloaded.push(annotation);
            }
        });
        if (unloaded.length>0) {
            this.rootScope.$broadcast(events.ANNOTATIONS_UNLOADED, unloaded);
        } else if ((reset == null || !reset) && toBeProcessed) {
            this.rootScope.$broadcast("reloadAnnotations");
        }
    }

    createAnnotation(annotation: Annotation): Annotation {
        this.rootScope.$broadcast(events.BEFORE_ANNOTATION_CREATED, annotation);
        return annotation;
    }

    deleteAnnotation(annotation: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.annotation.delete({ 
          id: annotation.id
        }, annotation).pipe(map(() => {
            self.rootScope.$broadcast(events.ANNOTATION_DELETED, annotation);
            return annotation;
        }));
    }

    deleteAnnotations(annotations: Annotation[]): Observable<Annotation[]> {
        const self = this;
        return this.api.annotation.deleteMultiple({}, {
          ids: annotations.map(a => a.id),
        }).pipe(map(() => {
            self.rootScope.$broadcast(events.ANNOTATIONS_DELETED, annotations);
            return annotations;
          }));
    }

    flagAnnotation(annot: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.annotation.flag({
          id: annot.id,
        }).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATION_FLAGGED, annot);
          return annot;
        }));
    }

    resetAnnotation(annotation: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.annotation.reset({
          id: annotation.id,
        }).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATION_DELETED, annotation);
          return annotation;
        }));
    }

    resetAnnotations(annotations: Annotation[]): Observable<Annotation[]> {
        const self = this;
        return this.api.annotation.resetMultiple({}, {
          ids: annotations.map(a => a.id),
        }).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATIONS_DELETED, annotations);
          return annotations;
        }));
    }

    treatAnnotation(annotation: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.annotation.treat({
          id: annotation.id
        }, annotation).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATION_DELETED, annotation);
          return annotation;
        }));
    }

    treatAnnotations(annotations: Annotation[]): Observable<Annotation[]> {
        const self = this;
        return this.api.annotation.treatMultiple({}, {
          ids: annotations.map(a => a.id),
          feedbacks: annotations.map(a => a.feedback)
        }).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATIONS_DELETED, annotations);
          return annotations;
        }));
    }

    acceptSuggestion(annotation: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.suggestion.accept({
          id: annotation.id
        }, annotation).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATION_DELETED, annotation);
          return annotation;
        }));
    }

    acceptSuggestions(annotations: Annotation[]): Observable<Annotation[]> {
        const self = this;
        return zip(annotations.map(annotation => self.api.suggestion.accept({
          id: annotation.id
        }, annotation))).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATIONS_DELETED, annotations);
          return annotations;
        }));
    }

    rejectSuggestion(annotation: Annotation): Observable<Annotation> {
        const self = this;
        return this.api.suggestion.reject({
          id: annotation.id
        }, annotation).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATION_DELETED, annotation);
          return annotation;
        }));
    }

    rejectSuggestions(annotations: Annotation[]): Observable<Annotation[]> {
        const self = this;
        return zip(annotations.map(annotation => self.api.suggestion.reject({
          id: annotation.id
        }, annotation))).pipe(map(() => {
          self.rootScope.$broadcast(events.ANNOTATIONS_DELETED, annotations);
          return annotations;
        }));
    }
}
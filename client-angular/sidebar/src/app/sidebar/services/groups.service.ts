/**
 * @ngdoc service
 * @name  groups
 *
 * @description Provides access to the list of groups that the user is currently
 *              a member of and the currently selected group in the UI.
 *
 *              The list of groups is initialized from the session state
 *              and can then later be updated using the add() and remove()
 *              methods.
 */
'use strict';

import * as events from '../events';
import { awaitStateChange } from '../util/state.util';
import { serviceConfig } from '../service-config';
import * as SYSTEM_IDS from '../../../../../shared/system-id';
import {ApiService} from './api.service';
import {LocalStorageService} from './local-storage.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Group } from '../models/groups.model';
import {ScopeService} from './scope.service';
import { Injectable } from '@angular/core';
import { Observable, of, mergeMap, map, from } from 'rxjs';
import {HypothesisStoreService, IStoreService} from '../store';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

const STORAGE_KEY = 'hypothesis.groups.focus';
//LEOS Change
const DEFAULT_GROUP = "__world__";

export interface IGroupsService {
    all(): Group[];
    getDefaultGroupId(): string;
    defaultGroupId(): string;
    getDocumentUriForGroupSearch(): Observable<string | null>;

    /**
    * Fetch the list of applicable groups from the API.
    *
    * The list of applicable groups depends on the current userid and the URI of
    * the attached frames.
    */
    load(): Observable<Group[]>;

    // Return the full object for the group with the given id.
    get(id: string): Group | undefined;

    /**
    * Leave the group with the given ID.
    * Returns a promise which resolves when the action completes.
    */
    leave(id: string): Observable<any>;

    /** Return the currently focused group. If no group is explicitly focused we
    * will check localStorage to see if we have persisted a focused group from
    * a previous session. Lastly, we fall back to the first group available.
    */
    focused(): Group | undefined;

    /** Set the group with the passed id as the currently focused group. */
    focus(id: string): void;
}

@Injectable()
export class GroupsService implements IGroupsService {
    private prevFocusedId?: string;
    private authority?: string;
    private documentUri?: string;
    private settings: IHypothesisJsonConfig;
    private isSidebar: boolean;

    constructor(private store: HypothesisStoreService,
        private api: ApiService,
        private localStorage: LocalStorageService,
        settingsService: HypothesisConfigurationService,
        private rootScope: ScopeService) {
            this.documentUri = undefined;
            this.settings = settingsService.getConfiguration();
            this.isSidebar = settingsService.isSidebar;
            const svc = serviceConfig(this.settings);
            this.authority = svc ? svc.authority : undefined;

            // Persist the focused group to storage when it changes.
            this.prevFocusedId = store.groups.focusedGroupId();
            const self = this;
            this.store.subscribe(() => {
                const focusedId = (self.authority === SYSTEM_IDS.ISC) ? self.settings.connectedEntity : self.store.groups.focusedGroupId();
                if (focusedId !== self.prevFocusedId) {
                    self.prevFocusedId = focusedId;
                    self.localStorage.setItem(STORAGE_KEY, focusedId);
                    // Emit the `GROUP_FOCUSED` event for code that still relies on it.
                    self.rootScope.$broadcast(events.GROUP_FOCUSED, focusedId);
                }
            });

            // reset the focused group if the user leaves it
            rootScope.$on(events.GROUPS_CHANGED, () => self.load());

            // refetch the list of groups when user changes
            // FIXME Makes a second api call on page load. better way?
            rootScope.$on(events.USER_CHANGED, () => self.load());

            // refetch the list of groups when document url changes
            rootScope.$on(events.FRAME_CONNECTED, () => {
                return self.getDocumentUriForGroupSearch().pipe(
                    mergeMap((uri: string | null) => {
                        if (self.documentUri !== uri) {
                            self.documentUri = uri || undefined;
                            return self.load();
                        }
                        return of();
                    })
                );
            });
    }

    getDefaultGroupId(): string {
        return DEFAULT_GROUP;
    }

    defaultGroupId(): string {
        return this.getDefaultGroupId();
    }

    getDocumentUriForGroupSearch(): Observable<string | null> {
        return awaitStateChange(this.store, (store: HypothesisStoreService) => {
            let uris: string[] = store.frames.searchUris();
            if (uris.length === 0) {
                return of(null);
            }

            // We get the first HTTP URL here on the assumption that group scopes must
            // be domains (+paths)? and therefore we need to look up groups based on
            // HTTP URLs (so eg. we cannot use a "file:" URL or PDF fingerprint).
            return of(uris.find((uri: string) => uri.startsWith('http')));            
        });
    }

    load(): Observable<Group[]> {
        let uri: Observable<string | null> = of(null);
        if (this.isSidebar) {
            uri = this.getDocumentUriForGroupSearch();
        }

        const self = this;
        return uri.pipe(
            mergeMap((uri: string | null) => {
                const params: any = {
                    expand: 'organization',
                };
                if (self.authority) {
                    params['authority'] = self.authority;
                }
                if (uri) {
                    params['document_uri'] = uri;
                }
                return self.api.groups.list(params);
            }),
            mergeMap((gs: Group[]) => {
                const isFirstLoad = self.store.groups.allGroups().length === 0;
                self.store.groups.loadGroups(gs);
                if (isFirstLoad) {
                    let prevFocusedGroup = self.localStorage.getItem(STORAGE_KEY);
                    if (prevFocusedGroup === null && self.authority === SYSTEM_IDS.ISC) {
                        prevFocusedGroup = self.settings.connectedEntity;
                    }
                    self.store.groups.focusGroup(prevFocusedGroup);
                }
                return of(self.store.groups.allGroups());                
            })
        );
    }

    all(): Group[] {
        return this.store.groups.allGroups();
    }

    get(id: string): Group | undefined {
        return this.store.groups.getGroup(id);
    }

    leave(id: string): Observable<any> {
        // The groups list will be updated in response to a session state
        // change notification from the server. We could improve the UX here
        // by optimistically updating the session state
        return this.api.group.member.delete({
            pubid: id,
            user: 'me',
        });
    }

    focused(): Group | undefined {
        return this.store.groups.focusedGroup() as Group;
    }

    focus(id: string): void {
        this.store.groups.focusGroup(id);
    }
}

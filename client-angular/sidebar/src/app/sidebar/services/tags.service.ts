'use strict';

import {LocalStorageService} from "./local-storage.service";
import { Injectable } from '@angular/core';

const TAGS_LIST_KEY = 'hypothesis.user.tags.list';
const TAGS_MAP_KEY = 'hypothesis.user.tags.map';

export interface TagModelClass {
    [key: string]: any;
}

/**
 * @typedef TagModel
 * @property {any} value - Tag identifier
 * @property {string} display - Display value of the tag
 */
export interface TagModel extends TagModelClass {
    value: any;
    display: string;
}

/**
 * @typedef Tag
 * @property {number} count - The number of times this tag has been used.
 * @property {number} updated - The timestamp when this tag was last used.
 */
export interface Tag extends TagModel {
    count: number;
    updated: number;
}

/**
 * Service for fetching tag suggestions and storing data to generate them.
 *
 * The `tags` service stores metadata about recently used tags to local storage
 * and provides a `filter` method to fetch tags matching a query, ranked based
 * on frequency of usage.
 */
export interface ITagsService {
    /**
    * Return a list of tag suggestions matching `query`.
    *
    * @param {string} query
    * @return {Tag[]} List of matching tags
    */
    filter(query: string): Tag[];

    /**
    * Update the list of stored tag suggestions based on the tags that a user has
    * entered for a given annotation.
    *
    * @param {Tag} tags - List of tags.
    */
    store(tags: Tag[]): void;
}

@Injectable()
export class TagsService implements ITagsService {
    constructor(private localStorage: LocalStorageService) {
    }

    filter(query: string): Tag[] {
        const savedTags = this.localStorage.getObject(TAGS_LIST_KEY) || [];

        return savedTags.filter((e: any) => {
            return e.toLowerCase().indexOf(query.toLowerCase()) !== -1;
        });
    }

    /**
    * Update the list of stored tag suggestions based on the tags that a user has
    * entered for a given annotation.
    *
    * @param {Tag} tags - List of tags.
    */
    store(tags: Tag[]) {
        // Update the stored (tag, frequency) map.
        const savedTags = this.localStorage.getObject(TAGS_MAP_KEY) || {};
        tags.forEach((tag) => {
            if (savedTags[tag.value]) {
                savedTags[tag.value].count += 1;
                savedTags[tag.value].updated = Date.now();
            } else {
                savedTags[tag.value] = {
                    text: tag.value,
                    count: 1,
                    updated: Date.now(),
                };
            }
        });
        this.localStorage.setObject(TAGS_MAP_KEY, savedTags);

        // Sort tag suggestions by frequency.
        const tagsList = Object.keys(savedTags).sort((t1, t2) => {
            if (savedTags[t1].count !== savedTags[t2].count) {
                return savedTags[t2].count - savedTags[t1].count;
            }
            return t1.localeCompare(t2);
        });
        this.localStorage.setObject(TAGS_LIST_KEY, tagsList);
    }
}

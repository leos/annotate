'use strict';

import * as _ from 'lodash';
import * as urlUtil from '../util/url.util';
import {ApiRoutesService} from './api-routes.service';
import {LeosOAuthAuthService} from './oauth-auth.service';
import {PendingRequestsService, IPendingRequestsService } from './pending-requests.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, zip, of, mergeMap } from 'rxjs';
import { IdService } from './id.service';

const get = _.get

/**
 * Translate the response from a failed API call into an Error-like object.
 *
 * The details of the response are available on the `response` property of the
 * error.
 */
interface ApiCallError extends Error {
    response: any;
}

function translateResponseToError(response: any): ApiCallError {
    var message;
    if (response.status <= 0) {
        message = 'Service unreachable.';
    } else {
        message = response.status + ' ' + response.statusText;
        if (response.data && response.data.reason) {
            message = message + ': ' + response.data.reason;
        }
    }
    const err = new Error(message) as ApiCallError;
    err.response = response;
    return err;
}

/**
 * Return a shallow clone of `obj` with all client-only properties removed.
 * Client-only properties are marked by a '$' prefix.
 */
function stripInternalProperties(obj: any): Object {
  var result: any = {};

  for (var k in obj) {
    if (obj.hasOwnProperty(k) && k[0] !== '$') {
      result[k] = obj[k];
    }
  }

  return result;
}

function forEachSorted (obj: any, iterator: any, context?: string): string[] {
    var keys = Object.keys(obj).sort();
    for (var i = 0; i < keys.length; i++) {
        iterator.call(context, obj[keys[i]], keys[i]);
    }
    return keys;
}


function serializeValue(v: any): string {
    if (typeof v === 'object') {
        return v instanceof Date ? v.toISOString() : JSON.stringify(v);
    }
    return `${v}`;
}

function encodeUriQuery (val: string): string {
    return encodeURIComponent(val).replace(/%20/g, '+');
}


// Serialize an object containing parameters into a form suitable for a query
// string.
//
// This is an almost identical copy of the default Angular parameter serializer
// ($httpParamSerializer), with one important change. In Angular 1.4.x
// semicolons are not encoded in query parameter values. This is a problem for
// us as URIs around the web may well contain semicolons, which our backend will
// then proceed to parse as a delimiter in the query string. To avoid this
// problem we use a very conservative encoder, found above.
function serializeParams(params: Object): HttpParams {
    if (!params) {
        return new HttpParams();
    }

    var encodedParams: any = {}
    forEachSorted(params, function(value: any, key: string) {
        if (value === null || typeof value === 'undefined') {
            return;
        }
        if (Array.isArray(value)) {
            value.forEach(function(v) {
                encodedParams[encodeUriQuery(key)] = encodeUriQuery(serializeValue(v));
            });
        } else {
            encodedParams[encodeUriQuery(key)] = encodeUriQuery(serializeValue(value));
        }
    }, undefined);

    return new HttpParams({fromObject: encodedParams});
}


function serializeParamsAsQuery(params?: Object): string {
    if (!params) {
        return '';
    }

    var encodedParams: string[] = [];
    forEachSorted(params, function(value: any, key: string) {
        if (value === null || typeof value === 'undefined') {
            return;
        }
        if (Array.isArray(value)) {
            value.forEach(function(v) {
                encodedParams.push(encodeUriQuery(key)  + '=' + encodeUriQuery(serializeValue(v)));
            });
        } else {
            encodedParams.push(encodeUriQuery(key) + '=' + encodeUriQuery(serializeValue(value)));
        }
    }, undefined);
    return encodedParams.join('&');
}


type ApiCall = (params: any, data?: any) => Observable<any>;

/**
 * Creates a function that will make an API call to a named route.
 *
 * @param $http - The Angular HTTP service
 * @param $q - The Angular Promises ($q) service.
 * @param links - Object or promise for an object mapping named API routes to
 *                URL templates and methods
 * @param route - The dotted path of the named API route (eg. `annotation.create`)
 * @param {Function} tokenGetter - Function which returns a Promise for an access token for the API.
 */

function createAPICall($http: HttpClient,
    links: () => Observable<any>, 
    route: string, 
    tokenGetter: () => Observable<any>,
    pendingRequests: IPendingRequestsService,
    clientId?: string): ApiCall {
        return (params: any, data: any): Observable<any> => {
            return zip(links(), tokenGetter()).pipe(
                mergeMap(([links, token]) => {
                    const descriptor = get(links, route);
                    const url = urlUtil.replaceURLParams(descriptor.url, params);
                    var headers: any = {};

                    if (data) {
                        // Make sure we use the correct Content-Type header, which would not be automatically done in case of DELETE bodies.
                        headers['Content-Type'] = 'application/json';
                    }
                    if (token) {
                        headers['Authorization'] = `Bearer ${token}`;
                    }
                    if (clientId) {
                        headers['X-Client-Id'] = clientId;
                    }

                    if ((descriptor.method === 'POST') && !data) {
                        // POST request parameters sent into request body
                        const reqParams = serializeParamsAsQuery(url.params);
                        headers['Content-Type'] = 'application/x-www-form-urlencoded';
                        return pendingRequests.watchRequest($http.post(url.url, reqParams, {
                            headers: new HttpHeaders(headers),
                            observe: 'response'
                        }));
                    } else {
                        const reqParams = serializeParams(url.params);
                        return pendingRequests.watchRequest($http.request(descriptor.method, url.url, {
                            headers: new HttpHeaders(headers), 
                            observe: 'response',
                            params: reqParams, 
                            body: data ? stripInternalProperties(data) : null
                        }));
                    }      
                }),
                mergeMap((response: HttpResponse<Object>) => {
                    return of(response.body);
                })
            );
        }
}

type SuggestionApiCall = {
    accept: ApiCall,
    reject: ApiCall
}

type AnnotationApiCall = {
    create: ApiCall,
    delete: ApiCall,
    get: ApiCall,
    update: ApiCall,
    updateFeedback: ApiCall,
    flag: ApiCall,
    hide: ApiCall,
    unhide: ApiCall,
    deleteMultiple: ApiCall,
    treat: ApiCall,
    treatMultiple: ApiCall,
    reset: ApiCall,
    resetMultiple: ApiCall
}

type GroupApiCall = {
    member: {
        delete: ApiCall
    }
}

type GroupsApiCall = {
    list: ApiCall
}

type ProfileApiCall = {
    groups: ApiCall,
    read: ApiCall,
    update: ApiCall
}

type PermissionsApiCall = {
    update: ApiCall
}

export interface IApiService {
    readonly search: ApiCall;
    readonly searchTemporary: ApiCall;
    readonly suggestion: SuggestionApiCall;
    readonly annotation: AnnotationApiCall;
    readonly group: GroupApiCall;
    readonly groups: GroupsApiCall;
    readonly profile: ProfileApiCall;
    readonly permissions: PermissionsApiCall;

    apiCall(route: string): ApiCall;
}

/**
 * API client for the Hypothesis REST API.
 *
 * Returns an object that with keys that match the routes in
 * the Hypothesis API (see http://h.readthedocs.io/en/latest/api/).
 *
 * This service handles authenticated calls to the API, using the `auth` service
 * to get auth tokens. The URLs for API endpoints are fetched from the `/api`
 * endpoint, a responsibility delegated to the `apiRoutes` service which does
 * not use authentication.
 */
@Injectable()
export class ApiService implements IApiService {
    private links: any;
    private clientId: string;

    //API calls
    readonly search: ApiCall;
    readonly searchTemporary: ApiCall;
    readonly suggestion: SuggestionApiCall;
    readonly annotation: AnnotationApiCall;
    readonly group: GroupApiCall;
    readonly groups: GroupsApiCall;
    readonly profile: ProfileApiCall;
    readonly permissions: PermissionsApiCall;

    constructor(private $http: HttpClient,
        apiRoutes: ApiRoutesService,
        idService: IdService,
        private auth: LeosOAuthAuthService,
        private pendingRequestsService: PendingRequestsService) {
            this.clientId = idService.getGlobalClientId();

            const links = () => apiRoutes.routes();
            this.links = links;

            const self = this;
            let _apiCall = (route: string): ApiCall => self.apiCall(route);

            this.search = _apiCall('search');
            this.searchTemporary = _apiCall('searchTemporary');

            this.suggestion = {
                accept: _apiCall('suggestion.accept'),
                reject: _apiCall('suggestion.reject')
            };

            this.annotation = {
                create: _apiCall('annotation.create'),
                delete: _apiCall('annotation.delete'),
                get: _apiCall('annotation.read'),
                update: _apiCall('annotation.update'),
                updateFeedback: _apiCall('annotation.updateFeedback'),
                flag: _apiCall('annotation.flag'),
                hide: _apiCall('annotation.hide'),
                unhide: _apiCall('annotation.unhide'),
                deleteMultiple: _apiCall('annotation.deleteMultiple'),
                treat: _apiCall('annotation.treat'),
                treatMultiple: _apiCall('annotation.treatMultiple'),
                reset: _apiCall('annotation.reset'),
                resetMultiple: _apiCall('annotation.resetMultiple')
            };

            this.group = {
                member: {
                    delete: _apiCall('group.member.delete'),
                },
            }

            this.groups = {
                list: _apiCall('groups.read'),
            }

            this.profile = {
                groups: _apiCall('profile.groups'),
                read: _apiCall('profile.read'),
                update: _apiCall('profile.update'),
            }

            this.permissions = {
                update: _apiCall('permissions.update'),
            }
    }

    apiCall(route: string): ApiCall {
        const self = this;
        return createAPICall(this.$http, 
            this.links, 
            route, 
            this.auth.tokenGetter, 
            this.pendingRequestsService, 
            this.clientId);
    }
}

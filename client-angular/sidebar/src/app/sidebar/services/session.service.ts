'use strict';

import * as events from '../events';
import { UserProfile } from '../models/session.model';
import { SessionState } from '../models/store.model';
import {AnalyticsService} from './analytics.service';
import {ApiService} from './api.service';
import { LeosOAuthAuthService } from './oauth-auth.service';
import {FlashService} from './flash.service';
import { IHypothesisJsonConfig, HypothesisServiceConfig } from '../../../../../shared/models/config.model';
import { serviceConfig } from '../service-config';
import {ScopeService} from './scope.service';
import { Injectable } from '@angular/core';
import {
    Observable,
    retry,
    map,
    of,
    throwError,
    catchError,
    tap
} from 'rxjs';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService } from './hypothesis-configuration.service';

var CACHE_TTL = 5 * 60 * 1000; // 5 minutes

/**
 * This service handles fetching the user's profile, updating profile settings
 * and logging out.
 *
 * Access to the current profile is exposed via the `state` property.
 *
 */
export interface ISessionService {
    // For the moment, we continue to expose the session state as a property on
    // this service. In future, other services which access the session state
    // will do so directly from store or via selector functions
    readonly state: SessionState;

    // Return the authority from the first service defined in the settings.
    // Return null if there are no services defined in the settings.
    getAuthority(): string | undefined;

    /**
    * Fetch the user's profile from the annotation service.
    *
    * If the profile has been previously fetched within `CACHE_TTL` ms, then this
    * method returns a cached profile instead of triggering another fetch.
    *
    * @return {Observable<UserProfile>} A promise for the user's profile data.
    */
    load(): Observable<UserProfile>;

    /**
    * Store the preference server-side that the user dismissed the sidebar
    * tutorial and then update the local profile data.
    */
    dismissSidebarTutorial(): Observable<UserProfile>;

    /**
    * Update the local profile data.
    *
    * This method can be used to update the profile data in the client when new
    * data is pushed from the server via the real-time API.
    *
    * @param {UserProfile} model
    * @return {UserProfile} The updated profile data
    */
    update(model: UserProfile): UserProfile;

    /**
    * Log the user out of the current session.
    */
    logout(): void;

    /**
    * Clear the cached profile information and re-fetch it from the server.
    *
    * This can be used to refresh the user's profile state after logging in.
    *
    * @return {Observable<UserProfile>}
    */
    reload(): Observable<UserProfile>;
}

@Injectable()
export class SessionService implements ISessionService {
    // Cache the result of load()
    private lastLoad?: Observable<UserProfile>;
    private lastLoadTime?: number;

    // Exposed for use in tests
    public profileFetchRetryOpts: any;

    private settings: IHypothesisJsonConfig;

    constructor(private analytics: AnalyticsService,
        private store: HypothesisStoreService,
        private api: ApiService,
        private auth: LeosOAuthAuthService,
        private flash: FlashService,
        settingsService: HypothesisConfigurationService,
        private rootScope: ScopeService) {
            this.settings = settingsService.getConfiguration();
            this.profileFetchRetryOpts = {};

            const self = this;
            this.rootScope.$on(events.OAUTH_TOKENS_CHANGED, () => self.reload());
    }

    get state(): SessionState {
        return this.store.state.session;
    }

    getAuthority(): string | undefined {
        const service: HypothesisServiceConfig | undefined = serviceConfig(this.settings) || undefined;
        if (service == undefined) {
            return undefined;
        }
        return service.authority;
    }

    load(): Observable<UserProfile> {
        if (this.lastLoadTime && (Date.now() - this.lastLoadTime) <= CACHE_TTL) return this.lastLoad!;

        // The load attempt is automatically retried with a backoff.
        //
        // This serves to make loading the app in the extension cope better with
        // flakey connectivity but it also throttles the frequency of calls to
        // the /app endpoint.
        this.lastLoadTime = Date.now();

        const self = this;
        const authority = this.getAuthority();
        var opts: any = {};
        if (authority) {
            opts['authority'] = authority;
            opts['connectedEntity'] = this.settings.connectedEntity; //LEOS Change
        }

        this.lastLoad = this.api.profile.read(opts).pipe(
            retry({
                count: 10,
                delay: 3000,
                resetOnSuccess: true
            }),
            map((sessionProfile: UserProfile) => {
                self.update(sessionProfile);
                self.lastLoadTime = Date.now()
                return sessionProfile;
            }),
            catchError((err: Error) => {
                self.lastLoadTime = undefined;
                return throwError(() => err);
            })
        );
        return this.lastLoad;
    }

    /**
    * Store the preference server-side that the user dismissed the sidebar
    * tutorial and then update the local profile data.
    */
    dismissSidebarTutorial(): Observable<UserProfile> {
        const self = this;
        return this.api.profile.update({}, {preferences: {show_sidebar_tutorial: false}}).pipe(
            map((profileModel: UserProfile) => self.update(profileModel))
        );
    }

    /**
    * Update the local profile data.
    *
    * This method can be used to update the profile data in the client when new
    * data is pushed from the server via the real-time API.
    *
    * @param {UserProfile} model
    * @return {UserProfile} The updated profile data
    */
    update(model: UserProfile): UserProfile {
        const prevSession: SessionState = this.store.getState().session;
        const userChanged = model.userid !== prevSession.userid;

        // Update the session model used by the application
        this.store.session.updateSession(model);

        this.lastLoad = of(model);
        this.lastLoadTime = Date.now();

        if (!userChanged) return model;
        this.rootScope.$broadcast(events.USER_CHANGED, {
            profile: model,
        });
        return model;
    }

    /**
    * Log the user out of the current session.
    */
    logout() {
        const self = this;
        return this.auth.logout().pipe(
            map(() => self.reload()),
            tap(() => self.analytics.track(this.analytics.events.LOGOUT_SUCCESS)),
            catchError((err?: string) => {
                self.flash.error('Log out failed');
                self.analytics.track(self.analytics.events.LOGOUT_FAILURE);
                return throwError(() => new Error(err));
            })
        );
    }

    /**
    * Clear the cached profile information and re-fetch it from the server.
    *
    * This can be used to refresh the user's profile state after logging in.
    *
    * @return {Observable<UserProfile>}
    */
    reload(): Observable<UserProfile> {
        this.lastLoad = undefined;
        this.lastLoadTime = undefined;
        return this.load();
    }
}
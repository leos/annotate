'use strict';

import { Injectable } from '@angular/core';
import { Annotation } from '../../../../../shared/models/annotation.model';

/**
 * Return true if a given `draft` is empty and can be discarded without losing
 * any user input
 */
function isEmpty(draft?: Draft): boolean {
    if (!draft) {
        return true;
    }
    return !draft.text && (draft.tags || []).length === 0;
}

export type DraftModel = {
    id?: string,
    $tag?: string
}

export interface DraftState {
    group?: string;
    isPrivate?: boolean;
    tags?: string[];
    text?: string;
    justification?: {
        text?: string
    };
    forwardJustification?: string;
    precedingText?: string;
    succeedingText?: string;
}

interface Draft extends DraftState {
    model?: DraftModel
}

export interface IDraftsService {
    /**
    * Returns the number of drafts - both unsaved new annotations, and unsaved
    * edits to saved annotations - currently stored.
    */
    count(): number;

    /**
    * Returns a list of local tags of new annotations for which unsaved drafts
    * exist.
    *
    * @return {Array<{$tag: string}>}
    */
    unsaved(): DraftModel[];

    /** Retrieve the draft changes for an annotation. */
    get(model: DraftModel): DraftState | null;

    /**
    * Returns the draft changes for an annotation, or null if no draft exists
    * or the draft is empty.
    */
    getIfNotEmpty(model: DraftModel): DraftState | null;

    /**
    * Update the draft version for a given annotation, replacing any
    * existing draft.
    */
    update(model: DraftModel, changes: DraftState): void;

    /** Remove the draft version of an annotation. */
    remove(model: DraftModel): void;

    /** Remove all drafts. */
    discard(): void;

    annotationState(annotation: any, isPrivate: boolean): DraftState;
}

/**
 * The drafts service provides temporary storage for unsaved edits to new or
 * existing annotations.
 *
 * A draft consists of:
 *
 * 1. `model` which is the original annotation domain model object which the
 *    draft is associated with. Domain model objects are never returned from
 *    the drafts service, they're only used to identify the correct draft to
 *    return.
 *
 * 2. `isPrivate` (boolean), `tags` (array of objects) and `text` (string)
 *    which are the user's draft changes to the annotation. These are returned
 *    from the drafts service by `drafts.get()`.
 *
 */
@Injectable()
export class DraftsService implements IDraftsService {
    _drafts: Draft[];

    constructor() {
        this._drafts = [];
    }

    /**
    * Returns true if 'draft' is a draft for a given
    * annotation.
    *
    * Annotations are matched by ID or local tag.
    */
    private _match(draft: Draft, model: DraftModel): boolean {
        if (!draft.model) return !model;
        return ((draft.model.$tag != null) && (model.$tag === draft.model.$tag)) ||
               ((draft.model.id != null) && (model.id === draft.model.id));
    }

    count(): number {
        return this._drafts.length;
    }

    unsaved(): DraftModel[] {
        return this._drafts.filter(function(draft: Draft) {
            return !draft.model || !draft.model.id;
        }).map(function(draft: Draft) {
            return draft.model || {};
        });
    }

    get(model: DraftModel): DraftState | null {
        for (var i = 0; i < this._drafts.length; i++) {
            const draft: Draft = this._drafts[i];
            if (this._match(draft, model)) {
                return {
                  group: draft.group,
                  isPrivate: draft.isPrivate,
                  tags: draft.tags,
                  text: draft.text,
                  justification: draft.justification,
                  forwardJustification : draft.forwardJustification
                }
            }
        }
        return null;
    }

    getIfNotEmpty(model: DraftModel): DraftState | null  {
        const draft: DraftState = this.get(model) || {};
        return isEmpty(draft) ? null : draft;
    }

    /**
    * Update the draft version for a given annotation, replacing any
    * existing draft.
    */
    update(model: DraftModel, changes: DraftState) {
        var newDraft: Draft = {
            model: { id: model.id, $tag: model.$tag },
            group: changes.group,
            isPrivate: changes.isPrivate,
            tags: changes.tags,
            text: changes.text,
            justification: changes.justification,
            forwardJustification: changes.forwardJustification
        };
        this.remove(model);
        this._drafts.push(newDraft);
    }

    /** Remove the draft version of an annotation. */
    remove(model: DraftModel) {
        const self = this;
        this._drafts = this._drafts.filter((draft: Draft) => {
            return !self._match(draft, model);
        });
    }

    /** Remove all drafts. */
    discard() {
        this._drafts = [];
    };

    annotationState(annotation: Annotation, isPrivate: boolean): DraftState {
        const draft: Draft | null = this.get(annotation);
        if (draft) return draft;

        const state: DraftState = {
          tags: annotation.tags,
          text: annotation.text,
          justification: annotation.justification,
          group: annotation.group,
          isPrivate: isPrivate,
          precedingText: annotation.precedingText,
          succeedingText: annotation.succeedingText,
          forwardJustification: annotation.forwardJustification,
        }
        return state;
    }
 }
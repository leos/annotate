'use strict';

import { Observable, from, catchError, throwError, Subscriber } from "rxjs";
import {LocalStorageService,  ILocalStorageService } from "./local-storage.service";
import {ScopeService} from '../services/scope.service';
import * as events from '../events';
import {BridgeService} from '../services/bridge.service';
import { Injectable } from '@angular/core';

var STORAGE_KEY = 'hypothesis.privacy';

function defaultLevel(localStorage: ILocalStorageService): string {
    const savedLevel = localStorage.getItem(STORAGE_KEY) as string;
    switch (savedLevel) {
        case 'private':
        case 'shared':
            return savedLevel;
        default:
            return 'shared';
    }
}

/**
 * Object defining which principals can read, update and delete an annotation.
 *
 * This is the same as the `permissions` field retrieved on an annotation via
 * the API.
 *
 * Principals are strings of the form `type:id` where `type` is `'acct'` (for a
 * specific user) or `'group'` (for a group).
 *
 */
export type Permissions = {
    /**
     * List of principals that can read the annotation
     */
    read: string[];

    /**
     * List of principals that can edit the annotation
     */
    update: string[];

    /**
     * List of principals that can delete the annotation
     */
    delete: string[];
}

/**
 * A service for generating and querying `Permissions` objects for annotations.
 *
 * It also provides methods to save and restore permissions preferences for new
 * annotations to local storage.
 */
export interface IPermissionsService {
    /**
    * Return the permissions for a private annotation.
    *
    * A private annotation is one which is readable only by its author.
    *
    * @param {string} userid - User ID of the author
    * @return {Permissions}
    */
    private(userid: string): Permissions;

    /**
    * Return the permissions for an annotation that is shared with the given
    * group.
    *
    * @param {string} userid - User ID of the author
    * @param {string} groupId - ID of the group the annotation is being
    * shared with
    * @return {Permissions}
    */
    shared(userid: string, groupId: string): Permissions;

    /**
    * Return the default permissions for an annotation in a given group.
    *
    * @param {string} userid - User ID of the author
    * @param {string} groupId - ID of the group the annotation is being shared
    * with
    * @return {Permissions}
    */
    default(userid: string, groupId: string): Permissions;

    /**
    * Set the default permissions for new annotations.
    *
    * @param {'private'|'shared'} level
    */
    setDefault(level: string): void;

    /**
    * Return true if an annotation with the given permissions is shared with any
    * group.
    *
    * @param {Permissions} perms
    * @return {boolean}
    */
    isShared(perms: Permissions): boolean;

    /**
    * Return true if a user can perform the given `action` on an annotation.
    *
    * @param {Permissions} perms
    * @param {'update'|'delete'} action
    * @param {string} userid
    * @return {boolean}
    */
    permits(perms: Permissions, action: string, userid: string): boolean;
}

export interface ILeosPermissionsService extends IPermissionsService {
    requestUserPermissions(): Observable<[string[]]>;
    getUserPermissions(): string[];
}

@Injectable()
export class PermissionsService implements IPermissionsService {
    constructor(protected localStorage: LocalStorageService) {
    }

    private(userid: string): Permissions {
        return {
            read: [userid],
            update: [userid],
            delete: [userid],
        }
    }

    shared(userid: string, groupId: string): Permissions {
        return Object.assign(this.private(userid), {
            read: ['group:' + groupId]
        });
    }

    default(userid: string, groupId: string): Permissions {
        if (defaultLevel(this.localStorage) === 'private') {
            return this.private(userid);
        } else {
            return this.shared(userid, groupId);
        }
    }

    setDefault(level: string) {
        this.localStorage.setItem(STORAGE_KEY, level);
    };

    isShared(perms: Permissions): boolean {
        return perms.read.some(function (principal) {
          return principal.indexOf('group:') === 0;
        });
    }

    permits(perms: Permissions, action: string, userid: string): boolean {
        return (perms as any)[action].indexOf(userid) !== -1;
    }
}

@Injectable()
export class LeosPermissionsService extends PermissionsService implements ILeosPermissionsService {
    private hostUserPermissions?: string[];

    constructor(private bridge: BridgeService, 
        localStorage: LocalStorageService,
        rootScope: ScopeService) {
            super(localStorage);

            const self = this;
            this.hostUserPermissions = undefined;
            this.requestUserPermissions().subscribe({
                next([permissions]) {
                    self.hostUserPermissions = permissions;
                    rootScope.$broadcast(events.LEOS_PERMISSIONS_LOADED);
                },
                error(err: any) {
                    self.hostUserPermissions = [];
                    rootScope.$broadcast(events.LEOS_PERMISSIONS_LOADED);
                }
            });
    }

    requestUserPermissions(): Observable<[string[]]> {
        const self = this;
        const _requestUserPermissionsFromHostBridge = () => {
            return new Promise<any>((resolve, reject) => {
                const timeout = setTimeout((() => reject('timeout')), 1500);
                self.bridge.call('requestUserPermissions', function (error: any, result: any) {
                  clearTimeout(timeout);
                  if (error) { return reject(error); } 
                  else { return resolve(result); }
                });
            })
        }

        const _requestUserPermissions = (attempt: number | undefined = undefined): Promise<any> => {
            const _attempt: number = attempt || 1;
            return new Promise((resolve, reject) => {
                _requestUserPermissionsFromHostBridge().then((result: any) => {
                    resolve(result);
                }).catch((err: any) => {
                    if ((typeof err === 'string') && (err == 'timeout') && (_attempt < 5)) {
                        console.debug(`Timeout requesting user permissions (attempts: ${_attempt})`);
                        setTimeout(() => {
                            _requestUserPermissions(_attempt + 1).then((result: any) => resolve(result))
                            .catch((err: any) => reject(err));          
                        }, 100);
                        return;
                    }
                    console.error('Request user permissions timeout', err);
                    reject(new Error('Request user permissions timeout'));  
                })
            });
        }

        return new Observable((subscriber: Subscriber<any>) => {
            // Without delay, the result of the host bridge method is not returend properly.
            from(_requestUserPermissions()).pipe(
                catchError((err: any) => throwError(() => new Error(err)))
            ).subscribe({
                next(result: any) {
                    subscriber.next(result);
                },
                error(err: any) {
                    subscriber.error(err);
                },
                complete() {
                    subscriber.complete();
                }
            })
        });
    }

    getUserPermissions(): string[] {
        if (!this.hostUserPermissions) {
            return [];
        }
        return this.hostUserPermissions;
    }
}
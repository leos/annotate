'use strict';

import {HypothesisStoreService} from '../store';
import { replaceURLParams, TUrlAndParams } from '../util/url.util';
import {ApiRoutesService} from './api-routes.service';
import { Injectable } from '@angular/core';

export interface IServiceUrlService {
    /**
     * A function that returns an absolute URL given a link name and params, by
     * expanding named URL templates received from the annotation service's API.
     *
     * The links object from the API is a map of link names to URL templates:
     *
     * {
     *   signup: "http://localhost:5000/signup",
     *   user: "http://localhost:5000/u/:user",
     *   ...
     * }
     *
     * Given a link name (e.g. 'user') and params (e.g. {user: 'bob'}) return
     * an absolute URL by expanding the named URL template from the API with the
     * given params (e.g. "http://localhost:5000/u/bob").
     *
     * Before the links object has been received from the API this function
     * always returns empty strings as the URLs. After the links object has been
     * received from the API this function starts returning the real URLs.
     *
     * @param {string} linkName - The name of the link to expand
     * @param {object} params - The params with which to expand the link
     * @returns {string} The expanded absolute URL, or an empty string if the
     *                   links haven't been received from the API yet
     * @throws {Error} If the links have been received from the API but the given
     *                 linkName is unknown
     * @throws {Error} If one or more of the params given isn't used in the URL
     *                 template
     *
     */
    get(linkName: string, params: any): string
}

@Injectable()
export class ServiceUrlService implements IServiceUrlService {
    constructor (private store: HypothesisStoreService, private apiRoutes: ApiRoutesService) {
        this.apiRoutes.links().subscribe({
            next(links: any){
                store.links.updateLinks(links)
            },
            error(error: Error) {
                console.warn('The links API request was rejected: ' + error.message);
                throw new Error('The links API request was rejected: ' + error.message);
            }
        });
    }

    get(linkName: string, params: any = {}): string {
        const links: any = this.store.getState().links;
        if (links === undefined) {
            return '';
        }
        if (links[linkName] === undefined) {
            return '';
        }

        const path: string = links[linkName];
        if (!path) {
            throw new Error('Unknown link ' + linkName);
        }

        const _params = params ?? {};
        const url: TUrlAndParams = replaceURLParams(path, _params);
        const unused = Object.keys(url.params);
        if (unused.length > 0) {
            throw new Error('Unknown link parameters: ' + unused.join(', '));
        }
        return url.url;
    }
}

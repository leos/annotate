'use strict';

import { buildThread } from '../build-thread';
import * as events from '../events';
import { Memoizer } from '../util/memoize.util';
import * as metadata from '../annotation-metadata';
import * as tabs from '../tabs';
import * as uiConstants from '../ui-constants';
import * as authorityChecker from '../authority-checker';
import { SearchFilterService, ISearchFilterService } from './search-filter.service';
import { ViewFilterService, IViewFilterService } from './view-filter.service';
import { SortCompareFunction, FilterFunction, ThreadFilterFunction, Thread } from '../../../../../shared/models/thread.model';
import { HypothesisStoreState } from '../models/store.model';
import {DraftsService, IDraftsService } from './drafts.service';
import {BridgeService} from '../services/bridge.service';
import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import { Annotation } from '../../../../../shared/models/annotation.model';
import  {ScopeService, IScopeService } from './scope.service';
import { Injectable } from '@angular/core';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

function truthyKeys(map: any): string[] {
    return Object.keys(map).filter(function (k) {
        return !!map[k];
    });
}

type SortFunctions = {
    [propName: string]: SortCompareFunction,
    Newest: SortCompareFunction,
    Oldest: SortCompareFunction,
    Location: SortCompareFunction
}

// Mapping from sort order name to a less-than predicate
const sortFns: SortFunctions = {
    Newest: function (a: any, b: any): boolean {
        return a.updated > b.updated;
    },
    Oldest: function (a: any, b: any): boolean {
        return a.updated < b.updated;
    },
    Location: function (a: any, b: any): boolean {
        return metadata.location(a) < metadata.location(b);
    },
};

/**
* Build the root conversation thread from the given UI state.
*
* @param state - The current UI state (loaded annotations, sort mode,
*        filter settings etc.)
*/
function buildRootThread(state: HypothesisStoreState, settings: any, searchFilter: ISearchFilterService, viewFilter: IViewFilterService): Thread {
    var sortFn: SortCompareFunction = sortFns[state.sortKey];
    var _filterFn: FilterFunction | undefined = undefined;

    if (state.filterQuery) {
        const filters = searchFilter.generateFacetedFilter(state.filterQuery);
        _filterFn = function (annot: Annotation): boolean {
            return viewFilter.filter([annot], filters).length > 0;
        };
    }

    var _threadFilterFn: ThreadFilterFunction | undefined = undefined;
    if (state.isSidebar && state.filterReseted) {
        _threadFilterFn = (thread: Thread): boolean => {
            if (!thread.annotation) {
                return false;
            }

            const showProcessedSentAnnotations = (`${settings.showStatusFilter}` !== "true")
                && authorityChecker.isISC(settings) || !authorityChecker.isISC(settings);
            return tabs.shouldShowInTab(thread.annotation, state.selectedTab || '', showProcessedSentAnnotations);
        }
    }

    // Get the currently loaded annotations and the set of inputs which
    // determines what is visible and build the visible thread structure
    return buildThread(state.annotations, {
        forceVisible: truthyKeys(state.forceVisible),
        expanded: state.expanded,
        highlighted: state.highlighted,
        sortCompareFn: sortFn,
        filterFn: _filterFn,
        threadFilterFn: _threadFilterFn,
    });
}

function deleteNewAndEmptyAnnotations (rootScope: IScopeService, drafts: IDraftsService, store: any) {
    store.getState().annotations.filter(function (ann: Annotation) {
        return metadata.isNew(ann) && !drafts.getIfNotEmpty(ann);
    }).forEach(function (ann: Annotation) {
        drafts.remove(ann);
        rootScope.$broadcast(events.ANNOTATION_DELETED, ann);
    });
}

/**
 * Root conversation thread for the sidebar and stream.
 *
 * This performs two functions:
 *
 * 1. It listens for annotations being loaded, created and unloaded and
 *    dispatches store.{addAnnotations|removeAnnotations} actions.
 * 2. Listens for changes in the UI state and rebuilds the root conversation
 *    thread.
 *
 * The root thread is then displayed by viewer.html
 */
export interface IRootThreadService {
    /**
    * Build the root conversation thread from the given UI state.
    * @param { HypothesisStoreState } state  - The current UI state (loaded annotations, sort mode,
    *        filter settings etc.)
    * @return {Thread}
    */
    thread(state: HypothesisStoreState): Thread
}


class ThreadMemoizer extends Memoizer<HypothesisStoreState,Thread> {
    constructor(memoizeFunction: (arg: HypothesisStoreState) => Thread) {
        super(memoizeFunction);
    }
}

@Injectable()
export class RootThreadService implements IRootThreadService {
    /**
     * Build the root conversation thread from the given UI state.
     * @return {Thread}
     */
    private _thread: ThreadMemoizer;

    constructor(store: HypothesisStoreService,
        drafts: DraftsService,
        bridge: BridgeService,
        searchFilter: SearchFilterService,
        viewFilter: ViewFilterService,
        configurationService: HypothesisConfigurationService,
        rootScope: ScopeService) {
            const settings: IHypothesisConfiguration = configurationService.getConfiguration();
            this._thread = new ThreadMemoizer((state: HypothesisStoreState): Thread => {
                return buildRootThread(state, settings, searchFilter, viewFilter);
            });

            // Listen for annotations being created or loaded
            // and show them in the UI.
            //
            // Note: These events could all be converted into actions that are handled by
            // the Redux store in store.
            const loadEvents = [
                events.ANNOTATION_CREATED,
                events.ANNOTATION_UPDATED
            ];
            loadEvents.forEach((event: string) => rootScope.$on(event, (event: Event, annotation: Annotation) => {
                    store.annotation.addAnnotations([annotation]);
                    bridge.call('LEOS_updateIdForCreatedAnnotation', annotation.$tag, annotation.id);
                })
            );

            rootScope.$on(events.ANNOTATIONS_LOADED, (event: Event, annotations: Annotation[]) => {
                    store.annotation.addAnnotations(annotations);
            });

            rootScope.$on(events.ANNOTATION_CREATED, (event: Event, ann: Annotation) => {
                bridge.call('LEOS_createdAnnotation', ann.$tag, ann.id)
            });

            const onBeforeAnnotationCreated = (event: Event, ann: Annotation) => {
                // When a new annotation is created, remove any existing annotations
                // that are empty.
                deleteNewAndEmptyAnnotations(rootScope, drafts, store);

                store.annotation.addAnnotations([ann]);

                // If the annotation is of type note or annotation, make sure
                // the appropriate tab is selected. If it is of type reply, user
                // stays in the selected tab.
                if (metadata.isPageNote(ann)) {
                    store.selection.selectTab(uiConstants.TAB_NOTES);
                } else if (metadata.isAnnotation(ann)) {
                    store.selection.selectTab(uiConstants.TAB_ANNOTATIONS);
                }

                (ann.references || []).forEach(function (parent) {
                    store.selection.setCollapsed(parent, false);
                });
            };
            rootScope.$on(events.BEFORE_ANNOTATION_CREATED, onBeforeAnnotationCreated);

            rootScope.$on(events.ANNOTATION_DELETED, (event: Event, annotation: Annotation) => {
                store.annotation.removeAnnotations([annotation]);
                if (annotation.id) {
                    store.selection.deselectAnnotation(annotation.id);
                }
            });

            rootScope.$on(events.ANNOTATIONS_DELETED, (event: Event, annotations: Annotation[]) => {
                store.annotation.removeAnnotations(annotations);
                annotations.forEach(function (annotation) {
                    if (annotation.id) {
                        store.selection.deselectAnnotation(annotation.id);
                    }
                });
            });

            rootScope.$on(events.ANNOTATIONS_UNLOADED, (event: Event, annotations: Annotation[]) => store.annotation.removeAnnotations(annotations));

            const onGroupFocused = (event: Event, focusedGroupId: string) => {
                var updatedAnnots = store.getState().annotations.filter(function (ann: Annotation) {
                    return metadata.isNew(ann) && !metadata.isReply(ann);
                }).map(function (ann: Annotation) {
                    return Object.assign(ann, {
                        group: focusedGroupId,
                    });
                });
                if (updatedAnnots.length > 0) {
                    store.annotation.addAnnotations(updatedAnnots);
                }
            }
            // Once the focused group state is moved to the app state store, then the
            // logic in this event handler can be moved to the annotations reducer.
            rootScope.$on(events.GROUP_FOCUSED, onGroupFocused);
    }

    thread(state: HypothesisStoreState): Thread {
        return this._thread.memoize(state)!;
    }
}
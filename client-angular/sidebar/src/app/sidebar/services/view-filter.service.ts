'use strict';

import * as annotationMetadata from '../annotation-metadata';
import * as authorityChecker from '../authority-checker';
import {UnicodeService} from './unicode.service';
import { ViewFilterFacet } from '../models/view-filter.model';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { Injectable } from '@angular/core';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

// Prevent Babel inserting helper code after `@ngInject` comment below which
// breaks browserify-ngannotate.
var unused; // eslint-disable-line

interface MatchFilter {
    matches(ann: any): boolean;
}

/**
 * Functions for extracting field values from annotations and testing whether
 * they match a query term.
 */
type FieldFilter = {
    /**
     * A function for a preliminary false match result
     */
    autofalse: (val: any) => boolean;

    /**
     * A function to extract to facet value for the annotation.
     */
    value: (val: any) => any;

    /**
     * A function to check if the extracted value matches the facet value
     */
    match: (term: any, value: any) => boolean;
}

type Normalizer = (val: string) => string;

/**
* Filter that matches annotations against a single field & term.
*
* eg. "quote:foo" or "text:bar"
*/
class TermFilter implements MatchFilter {
    /**
     * @param {string} field - Name of field to match
     * @param {string} term - Query term
     * @param {FieldFilter} checker - Functions for extracting term values from
     *   an annotation and checking whether they match a query term.
     */
    constructor(private field: string,
        private term: string,
        private checker: FieldFilter,
        private normalize: Normalizer) {
    }

    matches(ann: Annotation): boolean {
        const checker = this.checker;
        if (checker.autofalse && checker.autofalse(ann)) {
            return false;
        }

        var value = checker.value(ann);
        if (Array.isArray(value)) {
            const _normalize = (val: string) => this.normalize(val);
            value = value.map(_normalize.bind(this));
        } else {
            value = this.normalize(value);
        }
        return checker.match(this.term, value);
    }
}

/**
* Filter that combines other filters using AND or OR combinators.
*/
class BinaryOpFilter implements MatchFilter {
    /**
     * @param {'and'|'or'} op - Binary operator
     * @param {Filter[]} - Array of filters to test against
     */
    constructor(private operator: string, private filters: MatchFilter[]) {
    }

    matches(ann: Annotation): boolean {
        if (this.operator === 'and') {
            return this.filters.every(filter => filter.matches(ann));
        } else {
            return this.filters.some(filter => filter.matches(ann));
        }
    }
}

type FieldFilters = {
    quote: FieldFilter;
    since: FieldFilter;
    tag: FieldFilter;
    text: FieldFilter;
    uri: FieldFilter;
    user: FieldFilter;
    user_entity: FieldFilter;
    user_name: FieldFilter;
    group: FieldFilter;
    status: FieldFilter;
    responseVersion: FieldFilter;
    ISCReference: FieldFilter;
    responseId: FieldFilter;
    feedback: FieldFilter;
}

export interface IViewFilterService {
    /**
    * Filters a set of annotations.
    *
    * @param {Annotation[]} annotations
    * @param {Object} filters - Object providing Faceted filter
    * @return {string[]} IDs of matching annotations.
    */    
    filter(annotations: any[], filters: any): string[];
}

@Injectable()
export class ViewFilterService implements IViewFilterService {
    readonly fields: FieldFilters;

    constructor(private unicode: UnicodeService,
        settingsService: HypothesisConfigurationService) {
            const settings: IHypothesisJsonConfig = settingsService.getConfiguration();
            this.fields = {
                quote: {
                    autofalse: (ann: Annotation): boolean => (ann.references || []).length > 0,
                    value: (ann: Annotation): string => {
                        if (!ann.target) {
                            // FIXME: All annotations *must* have a target, so this check should
                            // not be required.
                            return '';
                        }

                        const target = ann.target[0];
                        const selectors = target.selector || [];
                        return selectors
                            .filter(s => s.type === 'TextQuoteSelector')
                            .map(s => s.exact)
                            .join('\n');
                    },
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                since: {
                    autofalse: (ann: Annotation): boolean => !ann.updated,
                    value: (ann: Annotation): Date => new Date(ann.updated!),
                    match: (term: number, value: Date): boolean => {
                        var delta: number = (Date.now() - value.getTime()) / 1000;
                        return delta <= term;
                    }
                },
                tag: {
                    autofalse: (ann: Annotation): boolean => !ann.tags,
                    value: (ann: Annotation): string[] => ann.tags!,
                    match: (term: string, value: string[]) => value.includes(term),
                },
                text: {
                    autofalse: (ann: Annotation): boolean => !ann.text,
                    value: (ann: Annotation): string => ann.text!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                uri: {
                    autofalse: (ann: Annotation): boolean => !ann.uri,
                    value: (ann: Annotation): string => ann.uri!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                user: {
                    autofalse: (ann: Annotation): boolean => !ann.user,
                    value: (ann: Annotation): string => ann.user!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                //LEOS changes as we need to search for DG/Entity
                user_entity: {
                    autofalse: (ann: Annotation): boolean => !ann.user_info || !ann.user_info.entity_name,
                    value: (ann: Annotation): string => ann.user_info!.entity_name!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                //LEOS changes as we may need to search for user's full name
                user_name: {
                    autofalse: (ann: Annotation): boolean => !ann.user_info || !ann.user_info.display_name,
                    value: (ann: Annotation): string => ann.user_info!.display_name!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                //LEOS changes search over group
                group: {
                    autofalse: (ann: Annotation): boolean => !ann.group,
                    value: (ann: Annotation): string => ann.group!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                //LEOS changes search over status
                status: {
                    autofalse: (ann: Annotation): boolean => !ann.status || !ann.status.status,
                    value: (ann: Annotation): string => ann.status!.status!,
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                responseVersion: {
                    autofalse: (ann: Annotation): boolean => !ann.document?.metadata || !ann.document!.metadata["responseVersion"],
                    value: (ann: Annotation): string => ann.document!.metadata["responseVersion"],
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                ISCReference: {
                    autofalse: (ann: Annotation): boolean => !ann.document?.metadata || !ann.document.metadata["ISCReference"],
                    value: (ann: Annotation): string => ann.document!.metadata["ISCReference"],
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                responseId: {
                    autofalse: (ann: Annotation): boolean => !ann.document?.metadata || !ann.document.metadata["responseId"],
                    value: (ann: Annotation): string => ann.document!.metadata["responseId"],
                    match: (term: string, value: string): boolean => value.indexOf(term) > -1,
                },
                feedback: {
                    autofalse: (ann: Annotation): boolean => !ann.feedback || !ann.feedback.text,
                    value: (ann: Annotation): Annotation => ann,
                    match: (term: any, value: Annotation): boolean => {
                        if ((value?.feedback?.text?.length || 0) <= 0) return false;
                        if (annotationMetadata.isProcessed(value)) return true;
                        return !authorityChecker.isISC(settings);
                    }
                }
            }
    }

    /**
    * Normalize a field value or query term for comparison.
    */
    _normalize(val: any): any {
        if (typeof val !== 'string') return val;
        return this.unicode.fold(this.unicode.normalize(val)).toLowerCase();
    }

    filter(annotations: any[], filters: any): string[] {
        // Convert the input filter object into a filter tree, expanding "any"
        // filters.
        const fieldFilters: MatchFilter[] = Object.entries(filters)
            .filter(([, filter]) => (filter as ViewFilterFacet).terms.length > 0)
            .map(([field, _filter]) => {
                const filter = _filter as ViewFilterFacet;
                const normalize = (val: string) => this._normalize(val);
                const terms: string[] = filter.terms.map((term: string | number) => normalize(String(term)));
                var termFilters;
                const fields = (this.fields as any)
                if (field === 'any') {
                    //LEOS changes as we may need to search for user's DG/Entity and full name
                    //LEOS changes as we may need to search for group
                    var anyFields = ['quote','text','tag','user','user_entity','user_name','group','responseVersion','ISCReference','responseId','feedback'];
                    termFilters = terms.map(term => new BinaryOpFilter('or', anyFields.map(field =>
                        new TermFilter(field, term, fields[field], normalize.bind(this))
                    )));
                } else {
                    termFilters = terms.map(term => new TermFilter(field, term, fields[field], normalize.bind(this)));
                }
                return new BinaryOpFilter(filter.operator, termFilters);
            });

        const rootFilter = new BinaryOpFilter('and', fieldFilters);
        return annotations
          .filter(ann => rootFilter.matches(ann))
          .map(ann => ann.id);
    }
}
'use strict';

import { SearchFilterFacets } from '../models/search-filter.model';
import { Injectable } from '@angular/core';

/**
 * Splits a search term into filter and data.
 *
 * ie. 'user:johndoe' -> ['user', 'johndoe']
 *     'example:text' -> [null, 'example:text']
 */
function splitTerm(term: string): (string | null)[] {
    let filter: string = term.slice(0, term.indexOf(':'));
    if (!filter) return [null, term];
    if (['group', 'quote', 'result', 'since',
       'tag', 'text', 'uri', 'user', 'user_name'].includes(filter)) {
            const data = term.slice(filter.length+1);
            return [filter, data];
    } else {
        // The filter is not a power search filter, so the whole term is data
        return [null, term];
    }
}

/**
 * Tokenize a search query.
 *
 * Splits `searchtext` into tokens, separated by spaces.
 * Quoted phrases in `searchtext` are returned as a single token.
 */
function tokenize(searchtext: string): string[] {
    if (!searchtext) { return []; }

    // Small helper function for removing quote characters
    // from the beginning- and end of a string, if the
    // quote characters are the same.
    // I.e.
    //   'foo' -> foo
    //   "bar" -> bar
    //   'foo" -> 'foo"
    //   bar"  -> bar"
    const _removeQuoteCharacter = (text: string): string => {
        let _text = ''.concat(text);
        const start = _text.slice(0,1);
        const end = _text.slice(-1);
        if (((start === '"') || (start === "'")) && (start === end)) {
          _text = _text.slice(1, _text.length - 1);
        }
        return _text;
    };

    let tokens: string[] = searchtext.match(/(?:[^\s"']+|"[^"]*"|'[^']*')+/g) || [];
    // Cut the opening and closing quote characters
    tokens = tokens.map(_removeQuoteCharacter);

    // Remove quotes for power search.
    // I.e. 'tag:"foo bar"' -> 'tag:foo bar'
    for (var index = 0; index < tokens.length; index++) {
        const token: string = tokens[index];
        const [filter, data] = splitTerm(token);
        if (filter) {
            tokens[index] = filter + ':' + (_removeQuoteCharacter(data || ''));
        }
    }

    return tokens;
}

function backendFilter(f?: string): string | undefined {
    return f === 'tag' ? 'tags' : f;
} 

function addToObj(obj: any, key: string, data: any): Object {
    if (obj[key]) {
        return obj[key].push(data);
    } else {
        return obj[key] = [data];
    }
};

export interface ISearchFilterService {
    /**
     * Parse a search query into a map of search field to term.
     *
     * @param {string} searchtext
     * @return {Object}
     */
    toObject(searchtext?: string): any

    /**
     * Parse a search query into a map of filters.
     *
     * Returns an object mapping facet names to Facet.
     *
     * Terms that are not associated with a particular facet are stored in the "any"
     * facet.
     *
     * @param {string} searchtext
     * @return {SearchFilterFacets}
     */
    generateFacetedFilter(searchtext: string): SearchFilterFacets
}

@Injectable()
export class SearchFilterService implements ISearchFilterService {
    /**
     * Parse a search query into a map of search field to term.
     *
     * @param {string} searchtext
     * @return {Object}
     */
    toObject(searchtext?: string): any {
        var obj = {};

        if (searchtext) {
            const terms = tokenize(searchtext);
            for (let term of terms) {
                let [filter, data] = splitTerm(term);
                if (!filter) {
                    filter = 'any';
                    data = term;
                }
                addToObj(obj, backendFilter(filter) || '', data);
            }
        }
        return obj;
    }

    /**
     * Parse a search query into a map of filters.
     *
     * Returns an object mapping facet names to Facet.
     *
     * Terms that are not associated with a particular facet are stored in the "any"
     * facet.
     *
     * @param {string} searchtext
     * @return {SearchFilterFacets}
     */
    generateFacetedFilter(searchtext: string): SearchFilterFacets {
        let terms: string[];
        let any: string[] = [];
        let quote: string[] = [];
        let result: string[] = [];
        let since: number[] = [];
        let tag: string[] = [];
        let text: string[] = [];
        let uri: string[] = [];
        let user: string[] = [];
        //LEOS change
        let group: string[] = [];
        let status: string[] = [];
        let user_name: string[] = [];
        let feedback: string[] = [];

        if (searchtext) {
            terms = tokenize(searchtext);
            for (const term of terms) {
                let t: RegExpExecArray | null;
                const colon_index: number = term.indexOf(':');
                const filter: string = term.slice(0, colon_index);
                const value: string = term.slice(colon_index+1);
                switch (filter) {
                case 'quote':
                    quote.push(value);
                    break;
                case 'result':
                    result.push(value);
                    break;
                case 'since':
                    {
                        // We'll turn this into seconds
                        const time: string = value.toLowerCase();
                        if (time.match(/^\d+$/)) {
                            // Only digits, assuming seconds
                            since.push(Number(time) * 1);
                        }
                        if (time.match(/^\d+sec$/)) {
                            // Time given in seconds
                            t = /^(\d+)sec$/.exec(time);
                            if (t) since.push(Number(t[1]) * 1);
                        }
                        if (time.match(/^\d+min$/)) {
                            // Time given in minutes
                            t = /^(\d+)min$/.exec(time);
                            if (t) since.push(Number(t[1]) * 60);
                        }
                        if (time.match(/^\d+hour$/)) {
                            // Time given in hours
                            t = /^(\d+)hour$/.exec(time);
                            if (t) since.push(Number(t) * 60 * 60);
                        }
                        if (time.match(/^\d+day$/)) {
                            // Time given in days
                            t = /^(\d+)day$/.exec(time);
                            if (t) since.push(Number(t[1]) * 60 * 60 * 24);
                        }
                        if (time.match(/^\d+week$/)) {
                            // Time given in week
                            t = /^(\d+)week$/.exec(time);
                            if (t) since.push(Number(t[1]) * 60 * 60 * 24 * 7);
                        }
                        if (time.match(/^\d+month$/)) {
                            // Time given in month
                            t = /^(\d+)month$/.exec(time);
                            if (t) since.push(Number(t[1]) * 60 * 60 * 24 * 30);
                        }
                        if (time.match(/^\d+year$/)) {
                            // Time given in year
                            t = /^(\d+)year$/.exec(time);
                            if (t) since.push(Number(t[1]) * 60 * 60 * 24 * 365);
                        }
                    }
                    break;
                case 'tag': tag.push(value); break;
                case 'text': text.push(value); break;
                case 'uri': uri.push(value); break;
                case 'user': user.push(value); break;
                //LEOS change
                case 'group': group.push(value); break;
                case 'status': status.push(value); break;
                case 'user_name': user_name.push(value); break;
                case 'feedback': feedback.push(value); break;
                default: any.push(term);
                }
            }
        }

        return {
            any: {
                terms: any,
                operator: 'and',
            },
            quote: {
                terms: quote,
                operator: 'and',
            },
            result: {
                terms: result,
                operator: 'min',
            },
            since: {
                terms: since,
                operator: 'and',
            },
            tag: {
                terms: tag,
                operator: 'and',
            },
            text: {
                terms: text,
                operator: 'and',
            },
            uri: {
                terms: uri,
                operator: 'or',
            },
            user: {
                terms: user,
                operator: 'or',
            },
            //LEOS change
            group: {
                terms: group,
                operator: 'or',
            },
            status: {
                terms: status,
                operator: 'or',
            },
            user_name: {
                terms: user_name,
                operator: 'or',
            },
            feedback: {
                terms: feedback,
                operator: 'and'
            }
        }
    }
}


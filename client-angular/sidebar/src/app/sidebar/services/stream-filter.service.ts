/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */

import { StreamFilterOptions } from '../models/stream-filter.model';
import { Injectable } from '@angular/core'

type StreamFilterActions = {
    create: boolean,
    update: boolean,
    delete: boolean
}

type StreamFilterClause = {
    field: string,
    operator: string,
    value: any,
    case_sensitive: boolean,
    options: StreamFilterOptions
}

type StreamFilter = {
    match_policy: string,
    clauses: StreamFilterClause[],
    actions: StreamFilterActions
}

export interface IStreamFilterService {
    getFilter(): StreamFilter;
    getMatchPolicy(): string;
    getClauses(): void;
    getActions(): StreamFilterActions;
    getActionCreate(): boolean;
    getActionUpdate(): boolean;
    getActionDelete(): boolean;
    setMatchPolicy(policy: string): IStreamFilterService;
    setMatchPolicyIncludeAny(): IStreamFilterService;
    setMatchPolicyIncludeAll(): IStreamFilterService;
    setMatchPolicyExcludeAny(): IStreamFilterService;
    setMatchPolicyExcludeAll(): IStreamFilterService;
    setActions(actions: StreamFilterActions): IStreamFilterService;
    setActionCreate(doAction: boolean): IStreamFilterService;
    setActionUpdate(doAction: boolean): IStreamFilterService;
    setActionDelete(doAction: boolean): IStreamFilterService;
    noClauses(): IStreamFilterService;
    addClause(field: string, operator: string, value: any, case_sensitive: boolean, options?: StreamFilterOptions): IStreamFilterService;
    resetFilter(): IStreamFilterService;
}

@Injectable()
export class StreamFilterService implements IStreamFilterService {
    private strategies = ['include_any', 'include_all', 'exclude_any', 'exclude_all'];
  
    private filter: StreamFilter;

    constructor() {
        this.filter = {
            match_policy :  'include_any',
            clauses : [],
            actions : {
                create: true,
                update: true,
                delete: true
            }
        }
    }

    getFilter(): StreamFilter { return this.filter; }
    getMatchPolicy(): string { return this.filter.match_policy; }
    getClauses() { return this.filter.clauses; }
    getActions(): StreamFilterActions { return this.filter.actions; }
    getActionCreate(): boolean { return this.filter.actions.create; }
    getActionUpdate(): boolean { return this.filter.actions.update; }
    getActionDelete(): boolean { return this.filter.actions.delete; }

    setMatchPolicy(policy: string): IStreamFilterService {
        this.filter.match_policy = policy;
        return this;
    }

    setMatchPolicyIncludeAny(): IStreamFilterService {
        this.filter.match_policy = 'include_any';
        return this;
    }

    setMatchPolicyIncludeAll(): IStreamFilterService {
        this.filter.match_policy = 'include_all';
        return this;
    }

    setMatchPolicyExcludeAny(): IStreamFilterService {
        this.filter.match_policy = 'exclude_any';
        return this;
    }

    setMatchPolicyExcludeAll(): IStreamFilterService {
        this.filter.match_policy = 'exclude_all';
        return this;
    }

    setActions(actions: StreamFilterActions): IStreamFilterService {
        this.filter.actions = actions;
        return this;
    }

    setActionCreate(doAction: boolean): IStreamFilterService {
        this.filter.actions.create = doAction;
        return this;
    }

    setActionUpdate(doAction: boolean): IStreamFilterService {
        this.filter.actions.update = doAction;
        return this;
    }

    setActionDelete(doAction: boolean): IStreamFilterService {
        this.filter.actions.delete = doAction;
        return this;
    }

    noClauses(): IStreamFilterService {
        this.filter.clauses = [];
        return this;
    }

    addClause(field: string, operator: string, value: any, case_sensitive: boolean, options?: StreamFilterOptions): IStreamFilterService {
        if (case_sensitive == null) { case_sensitive = false; }
        options = options || {};
        this.filter.clauses.push({
            field,
            operator,
            value,
            case_sensitive,
            options
        });
        return this;
    }

    resetFilter(): IStreamFilterService {
        this.setMatchPolicyIncludeAny();
        this.setActionCreate(true);
        this.setActionUpdate(true);
        this.setActionDelete(true);
        this.noClauses();
        return this;
    }
}

import { Injectable } from '@angular/core';
import { Subject, Observable, tap } from 'rxjs';

export interface IPendingRequestsService {
    pendingRequests: number;
    getObservable(): Observable<number>;
    watchRequest(request: Observable<any>): Observable<any>;
}


type PendingRequest = () => Observable<any>;

@Injectable()
export class PendingRequestsService implements IPendingRequestsService {
    private pendingSubject: Subject<number>;
    private pendingCount: number;

    constructor() {
        this.pendingCount = 0;
        this.pendingSubject = new Subject();
    }

    public get pendingRequests(): number {
        return this.pendingCount;
    }

    public getObservable(): Observable<number> {
        return this.pendingSubject.asObservable();
    }

    public watchRequest(request: Observable<any>): Observable<any> {
        const self = this;
        this.inc();
        return request.pipe(
            tap(() => {
                self.dec();
            })
        );
    }

    private inc(): void {
        this.pendingCount += 1;
        this.pendingSubject.next(this.pendingCount);
    }

    private dec(): void {
        this.pendingCount -= 1;
        this.pendingSubject.next(this.pendingCount);
    }
}
import { configFrom } from '../../../../../shared/config';
import addAnalytics from '../ga';
import * as apiUrls from '../get-api-url';
import { IHypothesisConfiguration, IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Injectable } from '@angular/core';
import { jsonConfigsFrom } from '../../../../../shared/config/json-config';
import { hostPageConfig } from '../host-config';

export interface IHypothesisConfgurationService {
    readonly isSidebar: boolean;
    setConfiguration(configuration: IHypothesisConfiguration): void;
    getConfiguration(): IHypothesisConfiguration;
}

@Injectable()
export class HypothesisConfigurationService implements IHypothesisConfgurationService {
    private configuration: IHypothesisConfiguration;
    private $window: Window;

    constructor() {
        this.$window = window;
        var configuration: IHypothesisConfiguration = jsonConfigsFrom(this.$window.document) as IHypothesisConfiguration;
        configuration = Object.assign(configuration, hostPageConfig(this.$window));
        configuration.apiUrl = apiUrls.getApiUrl(configuration);
        configuration.websocketUrl = apiUrls.getWSApiUrl(configuration);

        if(configuration.googleAnalytics){
            addAnalytics(configuration.googleAnalytics);
        }

        this.configuration = configuration;
    }

    get isSidebar(): boolean {
        if (this.$window.location.pathname.startsWith('/stream') ) {
            return false;
        }
        return !this.$window.location.pathname.startsWith('/a/');
    }

    public setConfiguration(configuration: IHypothesisConfiguration): void {
        this.configuration = Object.assign(this.configuration, configuration);
    }

    public getConfiguration(): IHypothesisConfiguration {
        return Object.assign({}, this.configuration);
    }
}
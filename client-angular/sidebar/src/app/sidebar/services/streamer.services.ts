'use strict';

import { default as queryString} from 'query-string';
import * as events from '../events';
import { Socket } from '../websocket';
import { ObjectWithId } from '../../../../../shared/models/common.model';
import {AnnotationMapperService} from './annotation-mapper.service';
import {LeosOAuthAuthService} from './oauth-auth.service';
import {GroupsService} from './groups.service';
import {SessionService} from './session.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Injectable, ChangeDetectorRef } from '@angular/core';
import {ScopeService} from './scope.service';
import {
    Observable,
    mergeMap,
    of,
    catchError
} from 'rxjs';
import { Annotation } from '../../../../../shared/models/annotation.model';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';
import { IdService } from './id.service';

const updateEvents = [
    events.ANNOTATION_DELETED,
    events.ANNOTATION_UPDATED,
    events.ANNOTATIONS_UNLOADED,
];

// weblogic: bringing access-token message as first
const configOrder = (a: string, b: string) => {
    return a === 'do-auth' ? -1 : 1;
}

/**
 * Open a new WebSocket connection to the Hypothesis push notification service.
 * Only one websocket connection may exist at a time, any existing socket is
 * closed.
 *
 * @param $rootScope - Scope used to $apply() app state changes
 *                     resulting from WebSocket messages, in order to update
 *                     appropriate watchers.
 * @param annotationMapper - The local annotation store
 * @param groups - The local groups store
 * @param session - Provides access to read and update the session state
 * @param settings - Application settings
 */
export interface IStreamerService {
    clientId: string;

    /**
    * Send a configuration message to the push notification service.
    * Each message is associated with a key, which is used to re-send
    * configuration data to the server in the event of a reconnection.
    */
    setConfig(key: string, configMessage: any): void;

    /**
    * Connect to the Hypothesis real time update service.
    *
    * If the service has already connected this does nothing.
    *
    * @return {Observable} Promise which resolves once the WebSocket connection
    *                   process has started.
    */
    connect(changeDetectorRef: ChangeDetectorRef): Observable<void>;

    /**
    * Connect to the Hypothesis real time update service.
    *
    * If the service has already connected this closes the existing connection
    * and reconnects.
    *
    * @return {Observable} Promise which resolves once the WebSocket connection
    *                   process has started.
    */
    reconnect(changeDetectorRef: ChangeDetectorRef): Observable<void>;

    applyPendingUpdates(): void;
    countPendingUpdates(): number;
    hasPendingDeletion(id: string): boolean;
}

@Injectable()
export class StreamerService implements IStreamerService {
    private socket: any;
    private configMessages: any;

    readonly clientId: string;
    //Fix fot timeout in weblogic
    private pingService: any; //LEOS:keeping WLS socket alive needs a message every 30 sec

    // The streamer maintains a set of pending updates and deletions which have
    // been received via the WebSocket but not yet applied to the contents of the
    // app.
    //
    // This state should be managed as part of the global app state in
    // store, but that is currently difficult because applying updates
    // requires filtering annotations against the focused group (information not
    // currently stored in the app state) and triggering events in order to update
    // the annotations displayed in the page.

    // Map of ID -> updated annotation for updates that have been received over
    // the WS but not yet applied
    private pendingUpdates: any;

    // Set of IDs of annotations which have been deleted but for which the
    // deletion has not yet been applied
    private pendingDeletions: any;
    private settings: IHypothesisJsonConfig;

    constructor(private annotationMapper: AnnotationMapperService,
        private store: HypothesisStoreService,
        private auth: LeosOAuthAuthService,
        private groups: GroupsService,
        private session: SessionService,
        configurationService: HypothesisConfigurationService,
        idService: IdService,
        private rootScope: ScopeService) {
            this.clientId = idService.getGlobalClientId();
            this.configMessages = {};
            this.pendingUpdates = {};
            this.pendingDeletions = {};
            this.settings = configurationService.getConfiguration();

            const self = this;
            const removePendingUpdates = (event: string, anns: Annotation[] | Annotation) => self._removePendingUpdates(event, anns);
            updateEvents.forEach((event: string) => rootScope.$on(event, removePendingUpdates));

            this.rootScope.$on(events.GROUP_FOCUSED, () => self._clearPendingUpdates());
    }

    private _startPingService(event: any): any {
        //Weblogic1213 close the socket in 30 sec.
        const socket: WebSocket = event.target;
        const sendData: string = JSON.stringify({'id': this.clientId, 'type': 'ping'});
        return window.setInterval(() => {
            socket.send(sendData);
        }, 25000);
    }

    private _stopPingService() {
        window.clearInterval(this.pingService);
    }

    private _removePendingUpdates(event: string, _anns: Annotation[] | Annotation) {
        const anns: Annotation[] = Array.isArray(_anns) ? _anns : [_anns];

        const self = this;
        anns.forEach((ann: Annotation) => {
            delete self.pendingUpdates[ann.id!];
            delete self.pendingDeletions[ann.id!];
        });
    }

    private _clearPendingUpdates () {
        this.pendingUpdates = {};
        this.pendingDeletions = {};
    }

    private _handleAnnotationNotification(message: any) {
        const action = message.options.action;
        var annotations = message.payload;

        // LEOS Change
        // LEOS-5903 : [EC][ISC] Annotations in preparation visible in EdiT
        const hasServices = (this.settings != undefined) && (this.settings.services != undefined)
            && (this.settings.services.length > 0)
        const currentAuth: string | undefined = hasServices ? this.settings.services![0].authority : undefined;
        
        const self = this;
        switch (action) {
        case 'create':
        case 'update':
        case 'past':
            annotations.forEach(function (ann: Annotation) {
                // In the sidebar, only save pending updates for annotations in the
                // focused group, since we only display annotations from the focused
                // group and reload all annotations and discard pending updates
                // when switching groups.

                const annAuth = !!ann.document && !!ann.document.metadata ? ann.document.metadata.systemId : null;
                if (!currentAuth || !annAuth || currentAuth == annAuth) {
                    // LEOS Change
                    // LEOS-4455 : GSC 322: Annotation: Notifications displayed only for comments posted to Collaborators
                    // ANOT-203: do not restrict on the "focused" group, because updates posted to other groups were not shown
                    self.groups.all().forEach(function (group) {
                        if ((ann.group === group.id) || !self.store.viewer.isSidebar()) {
                            self.pendingUpdates[ann.id!] = ann;
                        }
                    });
                }
                //--------------
            });
            break;
        case 'delete':
            annotations.forEach(function (ann: Annotation) {
                // Discard any pending but not-yet-applied updates for this annotation
                delete self.pendingUpdates[ann.id!];

                // If we already have this annotation loaded, then record a pending
                // deletion. We do not check the group of the annotation here because a)
                // that information is not included with deletion notifications and b)
                // even if the annotation is from the current group, it might be for a
                // new annotation (saved in pendingUpdates and removed above), that has
                // not yet been loaded.
                if (self.store.annotation.annotationExists(ann.id!)) {
                    self.pendingDeletions[ann.id!] = true;
                }
            });
            break;
        }

        if (!this.store.viewer.isSidebar()) {
            this.applyPendingUpdates();
        }
    }

    private _handleSessionChangeNotification(message: any) {
        this.session.update(message.model);
        this.groups.load();
    }

    private _handleSocketOnError (event: any) {
        console.warn('Error connecting to H push notification service:', event);

        // In development, warn if the connection failure might be due to
        // the app's origin not having been whitelisted in the H service's config.
        //
        // Unfortunately the error event does not provide a way to get at the
        // HTTP status code for HTTP -> WS upgrade requests.
        const websocketHost = new URL(this.settings.websocketUrl || '').hostname;
        if (['localhost', '127.0.0.1'].indexOf(websocketHost) !== -1) {
          console.warn('Check that your H service is configured to allow ' +
                       'WebSocket connections from ' + window.location.origin);
        }
    }

    private _handleSocketOnMessage (event: any) {
        // Wrap message dispatches in $rootScope.$apply() so that
        // scope watches on app state affected by the received message
        // are updated
        //const eventData = events.length > 0 ? events[0].data : '';       
        let message = JSON.parse(event.data);
        if (!message) return;

        if (message.type === 'annotation-notification') {
            this._handleAnnotationNotification(message);
            return;
        }
        if (message.type === 'session-change') {
            this._handleSessionChangeNotification(message);
            return;
        }
        if (message.type === 'whoyouare') {
            const userid = this.store.getState().session.userid;
            if (message.userid !== userid) {
                console.warn('WebSocket user ID "%s" does not match logged-in ID "%s"', message.userid, userid);
            }
            return;
        }
        if (message.type === 'pong') return;
        console.warn('received unsupported notification', message.type);
    }

    private _sendClientConfig () {
        const self = this;
        Object.keys(this.configMessages).sort(configOrder).forEach(function (key) {
            if (self.configMessages[key]) {
                self.socket.send(self.configMessages[key]);
            }
        });
    }

    public setConfig(key: string, configMessage: any) {
        this.configMessages[key] = configMessage;
        if (this.socket && this.socket.isConnected()) {
            this.socket.send(configMessage);
        }
    }

    private _connect(): Observable<void> {
        // If we have no URL configured, don't do anything.
        if (!this.settings.websocketUrl) {
            return of();
        }

        const self = this;
        return this.auth.tokenGetter().pipe(
            mergeMap((token?: string) => {
                var url: string;
                if (token) {
                    // Include the access token in the URL via a query param. This method
                    // is used to send credentials because the `WebSocket` constructor does
                    // not support setting the `Authorization` header directly as we do for
                    // other API requests.
                    const parsedURL = new URL(self.settings.websocketUrl || '');
                    const queryParams = queryString.parse(parsedURL.search);
                    queryParams['access_token'] = token;
                    parsedURL.search = queryString.stringify(queryParams);
                    url = parsedURL.toString();
                } else {
                    url = self.settings.websocketUrl || '';
                }
                const socket = new Socket(url);

                socket.on('open', () => {
                    self._sendClientConfig()
                });

                socket.on('open', (event: any) => {
                    self._startPingService(event)
                });

                socket.on('error', (event: any) => {
                    self._handleSocketOnError(event)
                });

                socket.on('error', () => {
                    self._stopPingService()
                });

                socket.on('close', () => {
                    self._stopPingService()
                });

                socket.on('message', (event: any) => {
                    self._handleSocketOnMessage(event)
                });
                self.socket = socket;

                //LEOS Change start
                // Configure the token in case it is not picked from uri for weblogic 12.1.3
                self.setConfig('do-auth', {
                    type: 'access-token',
                    value: token,
                });
                //LEOS Change end

                // Configure the client ID
                self.setConfig('client-id', {
                    messageType: 'client_id',
                    value: self.clientId,
                });

                // Send a "whoami" message. The server will respond with a "whoyouare"
                // reply which is useful for verifying that authentication worked as
                // expected.
                self.setConfig('auth-check', {
                    type: 'whoami',
                    id: 1,
                });
                return of();
            }),
            catchError((err: any) => {
                console.error('Failed to fetch token for WebSocket authentication', err);
                return of();
            })
        );
    };

    public connect(): Observable<void> {
        if (this.socket) {
          return of();
        }
        return this._connect();
    }

    public reconnect(): Observable<void> {
        if (this.socket) {
          this.socket.close();
          this._stopPingService();
        }
        return this._connect();
    }

    public applyPendingUpdates() {
        const updates: any[] = Object.values(this.pendingUpdates);
        const deletions: ObjectWithId[] = Object.keys(this.pendingDeletions).map(function (id) {
            return {id: id};
        });

        if (updates.length) {
            this.annotationMapper.loadAnnotations(updates);
        }
        if (deletions.length) {
            this.annotationMapper.unloadAnnotations(deletions);
        }

        this.pendingUpdates = {};
        this.pendingDeletions = {};
    }

    public countPendingUpdates(): number {
        return Object.keys(this.pendingUpdates).length +
               Object.keys(this.pendingDeletions).length;
    }

    public hasPendingDeletion(id: string): boolean {
        return this.pendingDeletions.hasOwnProperty(id);
    }
}

'use strict';

import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Injectable } from '@angular/core'
import { Angulartics2GoogleAnalytics } from 'angulartics2';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

type GlobalOpts = {
    category?: string
}

const VIA_REFERRER = /^https:\/\/(qa-)?via.hypothes.is\//;

function globalGAOptions(win: Window, settings: IHypothesisJsonConfig): GlobalOpts {
    settings = settings || {};

    var globalOpts = {
        category: '',
    };

    const validTypes = ['chrome-extension', 'firefox-extension', 'embed', 'bookmarklet', 'via'];

    // The preferred method for deciding what type of app is running is
    // through the setting of the appType to one of the valid types above.
    // However, we also want to capture app types where we were not given
    // the appType setting explicitly - these are the app types that were
    // added before we added the analytics logic
    const appType: string = (settings.appType || '');
    if(validTypes.indexOf(appType.toLowerCase()) > -1){
        globalOpts.category = appType.toLowerCase();
    } else if(win.location.protocol === 'chrome-extension:'){
        globalOpts.category = 'chrome-extension';
    } else if(VIA_REFERRER.test(win.document.referrer)){
        globalOpts.category = 'via';
    } else {
        globalOpts.category = 'embed';
    }

    return globalOpts;
};

type AnalyticEvents = {
    ANNOTATION_CREATED: string,
    ANNOTATION_DELETED: string,
    ANNOTATION_FLAGGED: string,
    ANNOTATION_SHARED: string,
    ANNOTATION_UPDATED: string,
    ANNOTATION_TREATED: string,
    ANNOTATION_RESET: string,
    DOCUMENT_SHARED: string,
    GROUP_LEAVE: string,
    GROUP_SWITCH: string,
    GROUP_VIEW_ACTIVITY: string,
    HIGHLIGHT_CREATED: string,
    HIGHLIGHT_UPDATED: string,
    HIGHLIGHT_DELETED: string,
    LOGIN_FAILURE: string,
    LOGIN_SUCCESS: string,
    LOGOUT_FAILURE: string,
    LOGOUT_SUCCESS: string,
    PAGE_NOTE_CREATED: string,
    PAGE_NOTE_UPDATED: string,
    PAGE_NOTE_DELETED: string,
    REPLY_CREATED: string,
    REPLY_UPDATED: string,
    REPLY_DELETED: string,
    SIDEBAR_OPENED: string,
    SIGN_UP_REQUESTED: string,
}

/**
 * @param  {string} event This is the event name that we are capturing
 *  in our analytics. Example: 'sidebarOpened'. Use camelCase to track multiple
 *  words.
 */
const _events: AnalyticEvents = {
    ANNOTATION_CREATED: 'annotationCreated',
    ANNOTATION_DELETED: 'annotationDeleted',
    ANNOTATION_FLAGGED: 'annotationFlagged',
    ANNOTATION_SHARED: 'annotationShared',
    ANNOTATION_UPDATED: 'annotationUpdated',
    ANNOTATION_TREATED: 'annotationTreated',
    ANNOTATION_RESET: 'annotationReset',
    DOCUMENT_SHARED: 'documentShared',
    GROUP_LEAVE: 'groupLeave',
    GROUP_SWITCH: 'groupSwitch',
    GROUP_VIEW_ACTIVITY: 'groupViewActivity',
    HIGHLIGHT_CREATED: 'highlightCreated',
    HIGHLIGHT_UPDATED: 'highlightUpdated',
    HIGHLIGHT_DELETED: 'highlightDeleted',
    LOGIN_FAILURE: 'loginFailure',
    LOGIN_SUCCESS: 'loginSuccessful',
    LOGOUT_FAILURE: 'logoutFailure',
    LOGOUT_SUCCESS: 'logoutSuccessful',
    PAGE_NOTE_CREATED: 'pageNoteCreated',
    PAGE_NOTE_UPDATED: 'pageNoteUpdated',
    PAGE_NOTE_DELETED: 'pageNoteDeleted',
    REPLY_CREATED: 'replyCreated',
    REPLY_UPDATED: 'replyUpdated',
    REPLY_DELETED: 'replyDeleted',
    SIDEBAR_OPENED: 'sidebarOpened',
    SIGN_UP_REQUESTED: 'signUpRequested',
};

export interface IAnalyticsService {
    readonly options: GlobalOpts;
    readonly events: AnalyticEvents;
    track(event: string, label?: any, metricValue?: any): void;
}

/**
 * Analytics API to simplify and standardize the values that we
 * pass to the Angulartics service.
 *
 * These analytics are based on google analytics and need to conform to its
 * requirements. Specifically, we are required to send the event and a category.
 *
 * We will standardize the category to be the appType of the client settings
 */
@Injectable()
export class AnalyticsService implements IAnalyticsService {
    readonly options: GlobalOpts;
    readonly events: AnalyticEvents;

    constructor(private $analytics: Angulartics2GoogleAnalytics, 
        settingsService: HypothesisConfigurationService) {
            const settings: IHypothesisJsonConfig = settingsService.getConfiguration();
            this.events = _events;
            this.options = (window != undefined) ? globalGAOptions(window, settings) : {};
    }

    track(event: string, label?: any, metricValue?: any) {
      this.$analytics.eventTrack(event, Object.assign({}, {
        label: label ? label : undefined,
        metricValue: isNaN(metricValue) ? undefined : metricValue,
      }, this.options));
    }
}
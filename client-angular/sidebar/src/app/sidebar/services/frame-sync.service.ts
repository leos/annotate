
'use strict';

import * as events from '../events';
import * as bridgeEvents from '../../../../../shared/bridge-events';
import * as metadata from '../annotation-metadata';
import * as uiConstants from '../ui-constants';
import * as _ from 'lodash';
import {DiscoveryService} from '../services/discovery.service';
import {BridgeService} from '../services/bridge.service';
import {RootThreadService} from './root-thread.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { Injectable } from '@angular/core';
import {ScopeService} from './scope.service';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from './hypothesis-configuration.service';

export interface AnnotatorDocumentInfo {
  uri: string;
  metadata: any;
  frameIdentifier: string | null;
}

const debounce = _.debounce;

export interface AnnotationFrameInfo {
    tag: string;
    msg: {
        document: any;
        target: any;
        uri: string;
        visible: boolean;
        tags: string[];
        id: string;
    }
}

/**
 * Return a minimal representation of an annotation that can be sent from the
 * sidebar app to a connected frame.
 *
 * Because this representation will be exposed to untrusted third-party
 * JavaScript, it includes only the information needed to uniquely identify it
 * within the current session and anchor it in the document.
 */
export function formatAnnot(ann: any): AnnotationFrameInfo {
  return {
    tag: ann.$tag,
    msg: {
      document: ann.document,
      target: ann.target,
      uri: ann.uri,
      visible:ann.showHighlight,
      tags: ann.tags,  //LEOS change - be able to assign different class for highlight and comments/suggestions
      id: ann.id, //LEOS change - associate highlight with annotation for canvas line draw
    },
  };
}

export interface IFrameSyncService {
    /**
    * Watch for changes to the set of annotations displayed in the sidebar and
    * notify connected frames about new/updated/deleted annotations.
    */
    setupSyncToFrame(): void;

    /**
    * Listen for messages coming in from connected frames and add new annotations
    * to the sidebar.
    */
    setupSyncFromFrame(): void;

    /**
    * Query the Hypothesis annotation client in a frame for the URL and metadata
    * of the document that is currently loaded and add the result to the set of
    * connected frames.
    */
    addFrame(channel: any): void;

    destroyFrame(frameIdentifier: string): void;

    /**
    * Find and connect to Hypothesis clients in the current window.
    */
    connect(): void;

    /**
    * Focus annotations with the given tags.
    *
    * This is used to indicate the highlight in the document that corresponds to
    * a given annotation in the sidebar.
    *
    * @param {string[]} tags
    */
    focusAnnotations(tags: string[]): void;

    /**
    * Scroll the frame to the highlight for an annotation with a given tag.
    *
    * @param {string} tag
    */
    scrollToAnnotation(tag: string): void;

    /**
    * LEOS
    *
    * Selects the clicked annotation
    */
    LEOS_selectAnnotation(annotation: Annotation): void;

    LEOS_selectAnnotations(annotations: Annotation[]): void;

    deselectAllAnnotations(): void;

    refreshAnnotationLinks(): void;

    showSidebar(): void;

    hideSidebar(): void;
}

/**
 * This service runs in the sidebar and is responsible for keeping the set of
 * annotations displayed in connected frames in sync with the set shown in the
 * sidebar.
 */
@Injectable()
export class FrameSyncService implements IFrameSyncService {
    private inFrame: Set<string>;
    private settings: IHypothesisJsonConfig;
  
    constructor(private discovery: DiscoveryService,
        private store: HypothesisStoreService,
        private bridge: BridgeService,
        private rootScope: ScopeService,
        private rootThread: RootThreadService,
        settingsService: HypothesisConfigurationService) {
          this.settings = settingsService.getConfiguration();
          this.inFrame = new Set();
    }

    setupSyncToFrame() {
        // List of loaded annotations in previous state
        var prevAnnotations: Annotation[] = [];
        var prevFrames: any[] = [];
        var prevPublicAnns = 0;

        const self = this;
        this.store.subscribe(function () {
          var state = self.store.getState();
          if ((state.annotations === prevAnnotations && state.frames === prevFrames) ||
              ((state.annotations.length > 0)  && (typeof state.annotations[0].id != 'undefined')
                && (typeof state.annotations[0].showHighlight == 'undefined'))) {
            return;
          }

          var publicAnns = 0;
          var inSidebar = new Set();
          var added: Annotation[] = [];

          state.annotations.forEach(function (annot: Annotation) {
            if (metadata.isReply(annot)) {
              // The frame does not need to know about replies
              return;
            }

            if (metadata.isPublic(annot)) {
              ++publicAnns;
            }

            inSidebar.add(annot.$tag);
            if (annot.$tag && !self.inFrame.has(annot.$tag)) {
              added.push(annot);
            }
          });
          var deleted = prevAnnotations.filter(function (annot) {
            return !inSidebar.has(annot.$tag) && !metadata.isReply(annot);
          });
          prevAnnotations = state.annotations;
          prevFrames = state.frames;

          // We currently only handle adding and removing annotations from the frame
          // when they are added or removed in the sidebar, but not re-anchoring
          // annotations if their selectors are updated.
          if (added.length > 0) {
            self.bridge.call('loadAnnotations', added.map(formatAnnot));
            added.forEach(function (annot: Annotation) {
              if (annot.$tag) self.inFrame.add(annot.$tag);
            });
          }
          deleted.forEach(function (annot: Annotation) {
            self.bridge.call('deleteAnnotation', formatAnnot(annot));
            if (annot.$tag) self.inFrame.delete(annot.$tag);
          });
          if (deleted.length > 0) {
            self.rootScope.$broadcast('refreshAnnotations');
          }

          var frames = self.store.frames.frames();
          if (frames.length > 0) {
            if (frames.every(function (frame: any) { return frame.isAnnotationFetchComplete; })) {
              if (publicAnns === 0 || publicAnns !== prevPublicAnns) {
                self.bridge.call(bridgeEvents.PUBLIC_ANNOTATION_COUNT_CHANGED, publicAnns);
                prevPublicAnns = publicAnns;
              }
            }
          }
        });
    }

    setupSyncFromFrame() {
        const self = this;

        // A new annotation, note or highlight was created in the frame
        this.bridge.on('beforeCreateAnnotation', function (event: any) {
          self.inFrame.add(event.tag);
          var annot = Object.assign({}, event.msg, {$tag: event.tag});
          self.rootScope.$broadcast(events.BEFORE_ANNOTATION_CREATED, annot);
        });

        this.bridge.on('destroyFrame', self.destroyFrame.bind(this));

        // Map of annotation tag to anchoring status
        // {
        //   anchoringStatus: 'anchored'|'orphan'|'timeout',
        //   anchoredRangeText: "The text in the document, that corresponds to the
        //                        anchor position"
        // }
        //
        // Updates are coalesced to reduce the overhead from processing
        // triggered by each `UPDATE_ANCHOR_STATUS` action that is dispatched.
        var anchoringStatusUpdates: any = {};
        var scheduleAnchoringStatusUpdate = debounce(() => {
          self.store.annotation.updateAnchorStatus(anchoringStatusUpdates);
          self.rootScope.$broadcast(events.ANNOTATIONS_SYNCED, Object.keys(anchoringStatusUpdates));
          anchoringStatusUpdates = {};
        }, 10);

        // Anchoring an annotation in the frame completed
        this.bridge.on('sync', function (events_: any[]) {
          events_.forEach(function (event) {
            self.inFrame.add(event.tag);
            anchoringStatusUpdates[event.tag] = {
              anchorLeosDeleted: !!event.msg.$leosDeleted,
              anchorSelectorMoved: !!event.msg.$selectorMoved,
              anchoringStatus: event.msg.$orphan ? 'orphan' : 'anchored',
              anchoredRangeText: event.msg.anchoredRangeText
            };
            scheduleAnchoringStatusUpdate();
          }); 
        });

        this.bridge.on('showAnnotations', function (tags: string[]) {
          self.store.selection.selectTab(uiConstants.TAB_ANNOTATIONS);
          if (tags.length) {
            self.rootScope.$broadcast(events.SCROLL_ANNOTATION_INTO_VIEW, tags[0]);
          }
          self.bridge.call('LEOS_refreshAnnotationLinkLines'); //#LEOS Change
        });

        this.bridge.on('focusAnnotations', function (tags: string[]) {
          self.store.selection.focusAnnotations(tags || []);
        });

        this.bridge.on('scrollToAnnotation', function (tag: string) {
          self.rootScope.$broadcast(events.SCROLL_ANNOTATION_INTO_VIEW, tag);
        });

        this.bridge.on('toggleAnnotationSelection', function (tags: string[]) {
          self.store.selection.toggleSelectedAnnotations(self.store.annotation.findIDsForTags(tags));
        });

        this.bridge.on('sidebarOpened', function () {
          self.rootScope.$broadcast('sidebarOpened');
        });

        this.bridge.on('sidebarClosed', function () {
          self.rootScope.$broadcast('sidebarClosed');
        });

        // These invoke the matching methods by name on the Guests
        this.bridge.on('showSidebar', function () {
          self.bridge.call('showSidebar');
        });
        this.bridge.on('hideSidebar', function () {
          self.bridge.call('hideSidebar');
        });
        this.bridge.on('setVisibleHighlights', function (state?: boolean) {
          self.bridge.call('setVisibleHighlights', state);
        });
    }

    addFrame(channel: any) {
        const self = this;
        channel.call('getDocumentInfo', function (err: any, info: AnnotatorDocumentInfo) {
          if (err) {
            console.error('Error getting document info', err);
            self.rootScope.$broadcast(events.FRAME_CONNECT_ERROR, err);
            channel.destroy();
            return;
          }

          self.rootScope.$broadcast(events.FRAME_CONNECTED);
          self.store.frames.connectFrame({
            id: info.frameIdentifier || '',
            metadata: info.metadata,
            uri: info.uri,
          });
        });
    }

    destroyFrame(frameIdentifier: string) {
        var frames = this.store.frames.frames();
        var frameToDestroy = frames.find(function (frame: any) {
          return frame.id === frameIdentifier;
        });
        if (frameToDestroy) {
          this.store.frames.destroyFrame(frameToDestroy);
        }
    }

    /**
    * Find and connect to Hypothesis clients in the current window.
    */
    connect() {
        const self = this;
        var discovery = this.discovery.createDiscovery(window, { server: true, appId: this.settings.sidebarAppId });
        let createChannelCb = (source: Window, origin: string, token: string) => self.bridge.createChannel(source, origin, token);
        discovery.startDiscovery(createChannelCb);
        this.bridge.onConnect(this.addFrame.bind(this));

        this.setupSyncToFrame();
        this.setupSyncFromFrame();

        this.bridge.on('stateChangeHandler', (hostState?: string) => {
          self.store.setHostState(hostState);
          if (self.store.state.hostState === 'OPEN') {
            self.bridge.call('LEOS_clearSelection');
          } else {
            self.bridge.call('LEOS_cancelFilterHighlights');
          }
        });

        this.bridge.on('reloadAnnotations', function () {
          self.rootScope.$broadcast('reloadAnnotations');
        });

        this.bridge.on('LEOS_requestFilteredAnnotations', function () {
          self.rootScope.$broadcast('LEOS_requestFilteredAnnotations');
        });

        this.bridge.on('LEOS_responseFilteredAnnotations', function (annotations: Annotation[]) {
          self.bridge.call('LEOS_responseFilteredAnnotations', annotations);
        });

        this.bridge.on('LEOS_clearSelectedAnnotations', (annotationIdToSelectInstead: string) => {
          if (annotationIdToSelectInstead) {
            const thread = self.rootThread.thread(self.store.getState());
            if (thread.children.some(child => child.id === annotationIdToSelectInstead)) {
              self.store.selection.toggleSelectedAnnotations([annotationIdToSelectInstead]);
            } else {
              // Ignore selection of annotations from document that are currently not shown in the sidebar, e.g. when a search filter is applied.
              // Therefore, deselect the annotation clicked on again in the document.
              const annot: Annotation | undefined = self.store.annotation.findAnnotationByID(annotationIdToSelectInstead);
              if (annot) {
                self.LEOS_selectAnnotation(annot);
              }

            }
          } else {
            self.store.selection.deselectAllAnnotations();
          }
        });

        this.bridge.on('LEOS_changeOperationMode', function (operationMode: string) {
          self.store.setOperationMode(operationMode);
        });

        this.bridge.on('LEOS_syncCanvas', function (iFrameOffsetLeft: number, delayResp: any) {
          self.rootScope.$broadcast('LEOS_syncCanvas', iFrameOffsetLeft, delayResp);
        });

        this.bridge.on('LEOS_syncCanvasResp', function () {
          self.bridge.call('LEOS_syncCanvasResp');
        });

        this.bridge.on('LEOS_setVisibleGuideLines', function (state: boolean | undefined, storePrevState: boolean | undefined) {
          self.bridge.call('LEOS_setVisibleGuideLines', state, storePrevState);
        });

        this.bridge.on('LEOS_restoreGuideLinesState', function () {
          self.bridge.call('LEOS_restoreGuideLinesState');
        });

        this.bridge.on('LEOS_updateIdForCreatedAnnotation', function (annotationTag: string, createdAnnotationId: string) {
          self.bridge.call('LEOS_updateIdForCreatedAnnotation', annotationTag, createdAnnotationId);
        });

        this.bridge.on('LEOS_refreshAnnotationLinkLines', function () {
          self.bridge.call('LEOS_refreshAnnotationLinkLines');
        });
    }

    focusAnnotations(tags: string[]) {
        this.bridge.call('focusAnnotations', tags);
    }

    scrollToAnnotation(tag: string) {
        this.bridge.call('scrollToAnnotation', tag);
    }

    scrollAnnotations() {
        this.bridge.call('scrollAnnotations');
    }

    LEOS_selectAnnotation(annotation: Annotation) {
        this.bridge.call('LEOS_selectAnnotation', annotation);
    }

    LEOS_selectAnnotations(annotations: Annotation[]) {
        this.bridge.call('LEOS_selectAnnotations', annotations);
    }

    deselectAllAnnotations() {
        this.bridge.call('LEOS_deselectAllAnnotations');
    }

    refreshAnnotationLinks() {
        this.bridge.call('LEOS_refreshAnnotationLinkLines');
    }

    showSidebar() {
        this.bridge.call('showSidebar');
    }

    hideSidebar() {
        this.bridge.call('hideSidebar');
    }
}
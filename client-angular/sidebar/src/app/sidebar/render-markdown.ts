'use strict';

import { escapeHtml } from './imports/escape-html';
import * as _katex from 'katex';
import * as showdown from 'showdown';

const katex = (_katex as any).default; 

type MathBlock = {
  id: number,
  expression: string,
  inline: boolean,
}

type MathInfo = { content: string, mathBlocks: MathBlock[] }

type TargetBlank = { type: string, filter: (text: string) => string};

function targetBlank(): TargetBlank[] {
  function filter(text: string): string {
    return text.replace(/<a href=/g, '<a target="_blank" href=');
  }
  return [{type: 'output', filter: filter}];
}

var converter: showdown.Converter;

function renderMarkdown(markdown: string): string {
  if (!converter) {
    // see https://github.com/showdownjs/showdown#valid-options
    converter = new showdown.Converter({
      extensions: [targetBlank],
      simplifiedAutoLink: true,
      // Since we're using simplifiedAutoLink we also use
      // literalMidWordUnderscores because otherwise _'s in URLs get
      // transformed into <em>'s.
      // See https://github.com/showdownjs/showdown/issues/211
      literalMidWordUnderscores: true,
    });
  }
  return converter.makeHtml(markdown);
}

function mathPlaceholder(id: number): string {
  return '{math:' + id.toString() + '}';
}

/**
 * Parses a string containing mixed markdown and LaTeX in between
 * '$$..$$' or '\( ... \)' delimiters and returns an object containing a
 * list of math blocks found in the string, plus the input string with math
 * blocks replaced by placeholders.
 */
function extractMath(content: string): MathInfo {
  const mathBlocks: MathBlock[] = [];
  var pos = 0;
  var replacedContent = content;

  while (true) { // eslint-disable-line no-constant-condition
    const blockMathStart: number = replacedContent.indexOf('$$', pos);
    const inlineMathStart: number = replacedContent.indexOf('\\(', pos);

    if (blockMathStart === -1 && inlineMathStart === -1) {
      break
    }

    var mathStart: number;
    var mathEnd: number;
    if (blockMathStart !== -1 &&
        (inlineMathStart === -1 || blockMathStart < inlineMathStart)) {
      mathStart = blockMathStart;
      mathEnd = replacedContent.indexOf('$$', mathStart + 2);
    } else {
      mathStart = inlineMathStart;
      mathEnd = replacedContent.indexOf('\\)', mathStart + 2);
    }

    if (mathEnd === -1) {
      break;
    } else {
      mathEnd = mathEnd + 2;
    }

    var id: number = mathBlocks.length + 1;
    var placeholder: string = mathPlaceholder(id);
    mathBlocks.push({
      id: id,
      expression: replacedContent.slice(mathStart + 2, mathEnd - 2),
      inline: inlineMathStart !== -1,
    });

    var replacement: string;
    if (inlineMathStart !== -1) {
      replacement = placeholder;
    } else {
      // Add new lines before and after math blocks so that they render
      // as separate paragraphs
      replacement = '\n\n' + placeholder + '\n\n';
    }

    replacedContent = replacedContent.slice(0, mathStart) +
                      replacement +
                      replacedContent.slice(mathEnd);
    pos = mathStart + replacement.length;
  }

  return {
    mathBlocks: mathBlocks.concat([]),
    content: replacedContent,
  };
}

function insertMath(html: string, mathBlocks: MathBlock[]): string {
  return mathBlocks.reduce(function (html: string, block: MathBlock) {
    var renderedMath: string;
    try {
      if (block.inline) {
        renderedMath = katex.renderToString(block.expression);
      } else {
        renderedMath = katex.renderToString(block.expression, {
          displayMode: true,
        });
      }
    } catch (err) {
      renderedMath = escapeHtml(block.expression);
    }
    return html.replace(mathPlaceholder(block.id), renderedMath);
  }, html);
}

export function renderMathAndMarkdown(markdown: string, sanitizeFn: Function): string {
  // KaTeX takes care of escaping its input, so we want to avoid passing its
  // output through the HTML sanitizer. Therefore we first extract the math
  // blocks from the input, render and sanitize the remaining markdown and then
  // render and re-insert the math blocks back into the output.
  const mathInfo: MathInfo = extractMath(markdown);
  const markdownHTML: string = sanitizeFn(renderMarkdown(mathInfo.content));
  return insertMath(markdownHTML, mathInfo.mathBlocks);
}


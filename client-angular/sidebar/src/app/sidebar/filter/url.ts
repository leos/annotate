'use strict';

/**
 * URL encode a string, dealing appropriately with null values.
 */
export function encode(str: string) {
  if (str) {
    return window.encodeURIComponent(str);
  }
  return '';
}
'use strict';

import { HypothesisServiceConfig, IHypothesisJsonConfig } from '../../../../shared/models/config.model';
import * as SYSTEM_ID from '../../../../shared/system-id';
import { serviceConfig } from './service-config';

function isISC(settings: IHypothesisJsonConfig): boolean {
  const svc: HypothesisServiceConfig | null = serviceConfig(settings);
  return svc?.authority === SYSTEM_ID.ISC;
}

function getAuthority(settings: IHypothesisJsonConfig):string {
  return isISC(settings) ? SYSTEM_ID.ISC : SYSTEM_ID.LEOS;
}
export {
  isISC,
  getAuthority
};

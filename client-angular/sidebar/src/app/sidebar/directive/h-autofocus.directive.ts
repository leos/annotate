'use strict';

import { Directive, ElementRef, OnInit, Injectable } from '@angular/core';

/** An attribute directive that focuses an <input> when it's linked by Angular.
 *
 * The HTML5 autofocus attribute automatically puts the keyboard focus in an
 * <input> on page load. But this doesn't work for <input>s that are
 * rendered by JavaScript/Angular after page load, for example an <input> that
 * is shown/hidden by JavaScript when an ng-if condition becomes true.
 *
 * To automatically put the keyboard focus on such an input when it's linked by
 * Angular, attach this directive to it as an attribute:
 *
 *   <input ng-if="..." h-autofocus>
 *
*/
@Injectable()
@Directive({
    selector: '[hAutofocus]'
})
export class AutofocusDirective implements OnInit {
    constructor(private elementRef: ElementRef<HTMLElement>) {
    }

    public ngOnInit(): void {
        this.elementRef.nativeElement.focus();
    }
}

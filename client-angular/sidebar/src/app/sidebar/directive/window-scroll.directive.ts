'use strict';

import { Directive, ElementRef, OnInit, OnDestroy, Injectable, Input, ChangeDetectorRef} from '@angular/core';
import $ from '../imports/jquery';

@Injectable()
@Directive({
    selector: '[windowScroll]'
})
export class WindowScrollDirective implements OnInit, OnDestroy {
    @Input() windowScroll: any;

    private isActive!: boolean;
    private html!: HTMLElement;
    private view!: Window;
    private onScroll!: VoidFunction;

    constructor(private elementRef: ElementRef<HTMLElement>, private changeDetector: ChangeDetectorRef) {

    }

    public ngOnInit(): void {
        this.isActive = true;
        const element :JQuery<HTMLElement> = $(this.elementRef.nativeElement);
        this.html = element.prop('ownerDocument').documentElement as HTMLElement;
        this.view = element.prop('ownerDocument').defaultView as Window;

        const self = this;
        this.onScroll = () => {
            let clientHeight = self.html.clientHeight;
            let scrollHeight = self.html.scrollHeight;
            if (self.view.scrollY + clientHeight >= (scrollHeight - clientHeight)) {
                if (self.isActive) {
                    self.isActive = false;
                    self.changeDetector.detectChanges();
                }
            } else {
                self.isActive = true;
            }
        };
        this.view.addEventListener('scroll', () => self.onScroll(), false);      
    }

    public ngOnDestroy(): void {
        const self = this;
        this.view.removeEventListener('scroll', () => self.onScroll());
    }
}
'use strict';

import { Directive, ElementRef, OnInit, OnDestroy, Input } from '@angular/core';

interface TooltipState {
  direction?: string;
  target?: HTMLElement | null;
}

/**
 * A custom tooltip similar to the one used in Google Docs which appears
 * instantly when activated on a target element.
 *
 * The tooltip is displayed and hidden by setting its target element.
 *
 *  var tooltip = new Tooltip(document.body);
 *  tooltip.setState({target: aWidget}); // Show tooltip
 *  tooltip.setState({target: null}); // Hide tooltip
 *
 * The tooltip's label is derived from the target element's 'aria-label'
 * attribute.
 *
 * @param {Element} rootElement - The container for the tooltip.
 */
class Tooltip {
    private _el: HTMLDivElement;
    private _labelEl: HTMLElement | null;
    public _state?: TooltipState;

    constructor(readonly rootElement: HTMLElement) {
        this._el = rootElement.ownerDocument.createElement('div');
        this._el.innerHTML = '<span class="tooltip-label js-tooltip-label"></span>';
        this._el.className = 'tooltip';

        rootElement.appendChild(this._el);
        this._labelEl = this._el.querySelector('.js-tooltip-label');

        this.setState({
            direction: 'down',
        });
    }

    get state(): TooltipState  | undefined{
      return Object.freeze(this._state);
    }

    setState(newState: TooltipState) {
        this._state = newState;
        this.render();
    }

    render() {
        var TOOLTIP_ARROW_HEIGHT = 7;

        if (!this._state?.target) {
          this._el.style.visibility = 'hidden';
          return;
        }

        var target: HTMLElement = this._state.target;
        var label: string = target.getAttribute('aria-label') || '';
        if (this._labelEl != null) {
          this._labelEl.textContent = label;
        }

        var tooltipRect = this._el.getBoundingClientRect();
        var targetRect = target.getBoundingClientRect();
        var top;

        if (this._state.direction === 'up') {
          top = targetRect.bottom + TOOLTIP_ARROW_HEIGHT;
        } else {
          top = targetRect.top - tooltipRect.height - TOOLTIP_ARROW_HEIGHT;
        }
        var left = targetRect.right - tooltipRect.width;

        this._el.classList.toggle('tooltip--up', this._state.direction === 'up');
        this._el.classList.toggle('tooltip--down', this._state.direction === 'down');

        this._el.style.visibility = '';
        this._el.style.top = `${top}px`;
        this._el.style.left = `${left}px`;
    };
}

/**
 * Attribute directive which displays a custom tooltip when hovering the
 * associated element.
 *
 * The associated element should use the `aria-label` attribute to specify
 * the tooltip instead of the `title` attribute, which would trigger the
 * display of the browser's native tooltip.
 *
 * Example: '<button aria-label="Tooltip label" h-tooltip></button>'
 */
@Directive({
  selector: '[hTooltip]'
})
export class TooltipDirective implements OnInit, OnDestroy {
    @Input()
    public hTooltip: any;

    private static theTooltip: Tooltip | undefined = undefined;

    constructor(private elementRef: ElementRef<HTMLElement>) {
        if (!TooltipDirective.theTooltip) {
            TooltipDirective.theTooltip = new Tooltip(document.body);
        }
    }

    public ngOnInit(): void {
      const el = this.elementRef.nativeElement;
      const theTooltip = TooltipDirective.theTooltip;
      el.addEventListener('mouseover', function () {
        var direction = el.getAttribute('tooltip-direction') || 'down';
        theTooltip?.setState({
          direction: direction,
          target: el,
        });
      });

      el.addEventListener('mouseout', function () {
        theTooltip?.setState({target: null});
      });
    }

    public ngOnDestroy(): void {
      if (TooltipDirective.theTooltip?.state?.target === this.elementRef.nativeElement) {
        TooltipDirective.theTooltip?.setState({target: null});
      }   
    }
}

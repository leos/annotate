'use strict';

import { Directive, ElementRef, Injectable, OnInit, Input } from '@angular/core';
import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import { HypothesisConfigurationService } from '../services/hypothesis-configuration.service';

interface ISupportedPropSettings {
    accentColor: string;
    appBackgroundColor: string;
    ctaBackgroundColor: string;
    ctaTextColor: string;
    selectionFontFamily: string;
    annotationFontFamily: string;
}

/**
 * The BrandingDirective brings theming configuration to our sidebar
 * by allowing the branding hypothesis settings to be reflected on items
 * that use this directive with the corresponding branding value.
 *
 * How to use:
 *   <element [hbranding]="'supportedProp1, supportedProp2'">
 *
 * Use `hBranding` to trigger this directive. Inside the attribute value,
 * add a comma separated list of what branding properties should be applied.
 * The attribute values match what the integrator would specify. For example,
 * if "superSpecialTextColor" is supported, the integrator could specify
 * `superSpecialTextColor: 'blue'` in the branding settings. Then any element that
 * included `hBranding="'superSpecialTextColor'"` would have blue placed on the
 * text's color.
 *
 * See below for the supported properties.
 */
@Injectable()
@Directive({
    selector: '[hBranding]'
})
export class BrandingDirective implements OnInit {
    @Input() 
    public hBranding?: string;

    private _hasBranding: boolean;
    private _supportedPropSettings: ISupportedPropSettings;
    private settings: IHypothesisConfiguration;

    constructor(configurationService: HypothesisConfigurationService, private elementRef: ElementRef<HTMLElement>) {
        this.settings = configurationService.getConfiguration();
        this._hasBranding = (this.settings.branding != undefined);

        // This is the list of supported property declarations
        // we support. The key is the name and how it should be reflected in the
        // settings by the integrator while the value (in the whitelist) is
        // the type of .style property being set. The types are pretty simple for now
        // and are a one-to-one mapping between the branding type and style property.
        this._supportedPropSettings = {
            accentColor: 'color',
            appBackgroundColor: 'backgroundColor',
            ctaBackgroundColor: 'backgroundColor',
            ctaTextColor: 'color',
            selectionFontFamily: 'fontFamily',
            annotationFontFamily: 'fontFamily',
        };
    }

    public ngOnInit(): void {
        if(!this._hasBranding) return;

        const self = this;
        this._getValidBrandingAttribute(this.hBranding || '').forEach(function(attr: string) {
            var propVal: string | undefined | null = self.settings.branding[attr];
            if(!propVal) return;
            // the _supportedPropSettings holds the .style property name
            // that is being set
            self.elementRef.nativeElement.style[(self._supportedPropSettings as any)[attr]] = propVal;
        });
    }

    private _getValidBrandingAttribute(attrString: string) {
        const self = this;
        return attrString.split(',').map(function(attr){
            return attr.trim();
        }).filter(function filterAgainstWhitelist(attr){
            return attr in self._supportedPropSettings;
        });
    }
}

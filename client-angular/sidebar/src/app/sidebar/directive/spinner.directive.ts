'use strict';

import { Component, ElementRef, OnInit, Injectable } from '@angular/core';

@Injectable()
@Component({
    selector: '.spinner',
    template: '<span><span></span></span>',
    // styleUrls: ['../styles/components/spinner.scss']
})
export class SpinnerDirective implements OnInit {
    constructor(/* private $animate, */ private elementRef: ElementRef<HTMLElement>) {
    }

    public ngOnInit(): void {
        //this.$animate.enabled(false, this.elementRef.nativeElement);
    }
}
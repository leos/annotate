'use strict';


import { Directive, ElementRef, Injectable, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import {ScopeService, IScopeService } from '../services/scope.service';

/**
 * Install an event handler on an element.
 *
 * The event handler follows the same behavior as the ng-<event name>
 * directives that Angular includes. This means:
 *
 *  - The handler function is passed an object with an $event property
 *  - The handler function is executed in the context of `$scope.$apply()`
 *
 * @param {Element} element
 * @param {Array<string>} events
 * @param {Function} handler
 */
function addEventHandler($scope: ScopeService, changeDetectorRef: ChangeDetectorRef, element: Element, events: string[], handler: Function) {
    var callback = function (event: any) {
        $scope.$apply(changeDetectorRef, () => {
            handler($scope, {$event: event});
        });
    };
    events.forEach(function (name) {
        element.addEventListener(name, callback);
    });
}



/**
 * A directive which adds an event handler for mouse press or touch to
 * a directive. This is similar to `ng-click` etc. but reacts either on
 * mouse press OR touch.
 */
@Injectable()
@Directive({
  selector: '[hOnTouch]'
})
export class OnTouchDirective implements OnInit {
    @Input() 
    public hOnTouch: any;

    constructor(private rootScopeService: ScopeService, private changeDetectorRef: ChangeDetectorRef, private elementRef: ElementRef<HTMLElement>) {
    }

    public ngOnInit(): void {
        //var fn = this.$parse(this.attributeValue, null /* interceptor */);
        addEventHandler(
          this.rootScopeService,
          this.changeDetectorRef,
          this.elementRef.nativeElement,
          ['click', 'mousedown', 'touchstart'],
          this.hOnTouch
        );    
    }
}

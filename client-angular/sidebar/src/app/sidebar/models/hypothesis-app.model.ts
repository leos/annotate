export interface IHypothesisSearchController {
    query: () => string | undefined;
    update: (query: string) => void;
    filterReseted: () => boolean;
    setFilterReseted: (filterReset: boolean) => void;
}
  
export interface HypothesisAuthState {
    status: string,
    displayName?: string;
    userid?: string;
    username?: string;
    provider?: string;
}
import { ObjectWithId } from '../../../../../shared/models/common.model'

export type ApplicationRole = 'USER' | 'SUPPORT' | 'ADMIN';

export type ProfilePreferences = {
    show_sidebar_tutorial: boolean
}

export interface ProfileGroup extends ObjectWithId {
    name: string;
    public: string;
}

export interface ProfileUserInfoEntity extends ObjectWithId {
    name: string;
    organizationName: string;
}

export type ProfileUserInfo = {
    display_name: string,
    entity_name: string,
    entities: ProfileUserInfoEntity[],
}

export type ProfileFeatures = {
    defer_realtime_updates: boolean,
    flag_action: boolean,
    embed_cachebuster: boolean,
    total_shared_annotations: boolean,
    search_for_doi: boolean,
    client_oauth: boolean,
    api_render_user_info: boolean,
    orphans_tab: boolean,
    filter_highlights: boolean,
    overlay_highlighter: boolean,
    client_display_names: boolean,
    forward_annotations: boolean,
    add_feedback: boolean
}

export type ProfileFlash = {
    info: string[],
    warning: string[],
    success: string[],
    error: string[]
}

/**
 * An object returned by the API (`GET /api/profile`) containing profile data
 * for the current user.
 */
export interface UserProfile {
    authority?: string;
    /** The authenticated user ID or null if the user is not logged in. */
    userid?: string;
    status?: string;
    /** A map of preference names and values. */
    preferences?: ProfilePreferences;
    groups?: ProfileGroup[];
    user_info?: ProfileUserInfo;
    /** A map of features that are enabled for the current user. */
    features?: ProfileFeatures;
    flash?: ProfileFlash;
    annotateVersion?: string;
}

export interface User {
    login: string;
    firstName: string;
    lastName: string;
    entities: UserEntity[];
    email: string;
    roles: ApplicationRole[];
    id: number;
    name: string;
    connectedEntity: UserEntity | null;
    defaultEntity: UserEntity | null;
    lang: string;
}

export interface UserEntity {
    id: string;
    name: string;
    organizationName: string;
}
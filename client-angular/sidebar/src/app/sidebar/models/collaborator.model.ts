export type DocumentRole = 'OWNER' | 'CONTRIBUTOR' | 'REVIEWER' | 'AUTHOR' | 'VIEWER';

export interface Collaborator {
    id: string;
    name?: string;
    login: string;
    entity: Entity;
    role: DocumentRole;
    additionalRole?: DocumentRole;
    clientSystem?: ClientSystem;
}

export interface ClientSystem {
    clientId: string;
    displayName: string;
}

export interface Entity {
    id: string;
    name: string;
    organizationName: string;
}

import { ViewFilterFacet } from './view-filter.model'

export interface SearchFilterFacet extends ViewFilterFacet {
    /**
     * {'and'|'or'|'min'}
     */
    lowercase?: boolean,
}

export type SearchFilterFacets = {
    [propName: string]: SearchFilterFacet,
    any: SearchFilterFacet,
    quote: SearchFilterFacet,
    result: SearchFilterFacet,
    since: SearchFilterFacet,
    tag: SearchFilterFacet,
    text: SearchFilterFacet,
    uri: SearchFilterFacet,
    user: SearchFilterFacet,
    group: SearchFilterFacet,
    status: SearchFilterFacet,
    user_name: SearchFilterFacet,
    feedback: SearchFilterFacet
}
type GroupLinks = {  
    html: string
}

export type Group = {
    name: string,
    id: string,
    scoped?: boolean,
    type?: string,
    public?: boolean,
    private?: boolean,
    links?: GroupLinks,
    logo?: string,
    organization?: GroupOrganization,
    url?: string
}

export type GroupOrganization = {
    id?: string,
    name?: string,
    logo?: string,
    groups?: Group[],
}
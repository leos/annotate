import { EditorState } from '../markdown-commands';

export type NewStateFunction = (state: EditorState) => EditorState;

export interface EditorExitData {
    id: string;
    initialText?: string;
    text?: string;
    editor?: HTMLTextAreaElement;
    focused?: boolean;
    time?: number;
    logicPlus?: boolean;
    remove?: boolean;
}
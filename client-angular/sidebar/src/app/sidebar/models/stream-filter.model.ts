/**
 * Elasticsearch specific options
 */
export type ElasticSearchOptions = {
    /**
     * can be: simple (term), query_string, match, multi_match
     * defaults to: simple, determines which es query type to use
     */
    query_type?: string,

    /**
     * if set, the query will be given a cutoff_frequency for this facet
     */
    cutoff_frequency?: number,

    /**
     * match and multi_match queries can use this, defaults to and
     */
    and_or?: string,

    /**
     * multi_match query type
     */
    match_type?: string,

    /**
     * fields to search for in multi-match query
     */
    fields?: string[]
}

/**
 * backend specific options
 * options.es: elasticsearch specific options
 */
export type StreamFilterOptions = {
    es?: ElasticSearchOptions
}
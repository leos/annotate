export interface ViewFilterFacet {
    terms: string[] | number[];
    operator: string;
}
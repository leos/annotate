/**
 * An object holding the details of an access token from the tokenUrl endpoint.
 * @typedef {Object} TokenInfo
 * @property {string} accessToken  - The access token itself.
 * @property {number} expiresAt    - The date when the timestamp will expire.
 * @property {string} refreshToken - The refresh token that can be used to
 *                                   get a new access token.
 */
export type TokenInfo = {
    accessToken: string,
    refreshToken: string,
    expiresAt: number
}

/**
 * OAuthClient configuration.
 *
 * @typedef {Object} Config
 * @property {string} clientId - OAuth client ID
 * @property {string} tokenEndpoint - OAuth token exchange/refresh endpoint
 * @property {string} authorizationEndpoint - OAuth authorization endpoint
 * @property {string} revokeEndpoint - RFC 7009 token revocation endpoint
 * @property {string} context - User´s context
 * @property {() => string} [generateState] - Authorization "state" parameter generator
 */
export type OAuthClientConfig = {
    /**
     * OAuth client ID
     */
    clientId?: string,

    /**
     * X-Client-Id request header value
     */
    xClientId: string;

    /**
     * OAuth token exchange/refresh endpoint
     */
    tokenEndpoint?: string,

    /**
     * OAuth authorization endpoint
     */
    authorizationEndpoint?: string,

    /**
     * RFC 7009 token revocation endpoint
     */
    revokeEndpoint?: string,

    /**
     * User´s context
     */
    context?: string,


    /**
     * Authorization "state" parameter generator
     */
    generateState?: () => string
}




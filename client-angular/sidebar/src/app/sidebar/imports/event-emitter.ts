/** 
 * Re-Implementation of the tiny-emitter project including types
 * @see https://github.com/scottcorgan/tiny-emitter
 */

interface TinyEvent {
  fn: Function;
  ctx?: string;
}

interface TinyEventDict {
  [propName: string] : TinyEvent[];
}

export interface IEventEmitter {
  on(name: string, callback: Function, ctx?: string): IEventEmitter;
  once(name: string, callback: Function, ctx?: string): IEventEmitter;
  emit(name: string, ...data: any[]): IEventEmitter;
  off(name: string, callback?: Function): IEventEmitter;
}

export class TinyEmitter implements IEventEmitter {
  protected eventDict: TinyEventDict;

  constructor() {
    this.eventDict = {};
  }

  on(name: string, callback: Function, ctx?: string): IEventEmitter {  
    if (!this.eventDict[name]) {
      this.eventDict[name] = [];
    }
    this.eventDict[name].push({
      fn: callback,
      ctx: ctx  
    });
    return this;
  }

  once(name: string, callback: Function, ctx?: string): IEventEmitter {
    var self = this;

    const listener = () => {
      self.off(name, listener);
      callback.apply(ctx, arguments);
    };

    listener._ = callback
    return this.on(name, listener, ctx); 
  }

  emit(name: string, ...data: any[]): IEventEmitter {
    if (!this.eventDict[name]) return this;

    const events: TinyEvent[] = this.eventDict[name].slice();
    events.forEach((event: TinyEvent) => {
      event.fn.apply(event.ctx, data);
    });
    return this;
  }

  off(name: string, callback?: Function): IEventEmitter {
    var events: TinyEvent[] | undefined = this.eventDict[name];
    var liveEvents: TinyEvent[] = [];

    if ((events != undefined) && (callback != undefined)) {
      events.forEach((event: TinyEvent) => {
        if (event.fn !== callback && (event.fn as any)._ !== callback) {
          liveEvents.push(event);
        }
      });
    }

    // Remove event from queue to prevent memory leak
    // Suggested by https://github.com/lazd
    // Ref: https://github.com/scottcorgan/tiny-emitter/commit/c6ebfaa9bc973b33d110a84a307742b7cf94c953#commitcomment-5024910
    (liveEvents.length) ? this.eventDict[name] = liveEvents : delete this.eventDict[name];
    return this;
  }
}

export default class EventEmitter extends TinyEmitter {
  constructor() {
      super();
  }

  override emit(event: string, ...args: any[]): IEventEmitter {
      return super.emit(event, args);
  }

  override on(event: string, callback: Function, ctx?: string): IEventEmitter {
      // New version of tiny-emitter returns an array of event / event data instead of the data directly
      // Add callback wrapper the returns the event data the old way
      const simpleCallback = (eventData: any) => {
          if (!eventData || eventData.length !== 1) {
              callback(eventData);
              return;
          }
          callback(eventData[0]);
      }
      return super.on(event, simpleCallback, ctx);
  }
}
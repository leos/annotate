import * as ScrollIntoView from 'scroll-into-view';

const scrollIntoView = (ScrollIntoView as any).default;
export default scrollIntoView;
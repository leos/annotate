'use strict';

import * as ANNOTATION_STATUS from './annotation-status';
import * as RESPONSE_STATUS from '../../../../shared/response-status';
import * as SYSTEM_ID from '../../../../shared/system-id';
import * as FEEDBACK_STATUS from '../../../../shared/feedback-status';
import { Annotation } from '../../../../shared/models/annotation.model';

/**
 * Utility functions for querying annotation metadata.
 */

type DocumentMetadata = {
  uri: string;
  domain: string;
  title: string;
}

/** Extract a URI, domain and title from the given domain model object.
 *
 * @param {object} annotation An annotation domain model object as received
 *   from the server-side API.
 * @returns {object} An object with three properties extracted from the model:
 *   uri, domain and title.
 *
 */
export function documentMetadata(annotation: Annotation): DocumentMetadata {
  var uri: string = annotation.uri || '';
  var domain: string;
  try {
    domain = new URL(uri).hostname;
  } catch (e) {
    domain = '';
  }
  var title: string = domain;

  if (annotation.document && annotation.document.title) {
    title = annotation.document.title;
  }

  if (domain === 'localhost') {
    domain = '';
  }

  return {
    uri: uri,
    domain: domain,
    title: title,
  };
}

export type DomainAndTitle = {
  domain: string,
  titleText: string | null,
  titleLink: string | null
}

/**
 * Return the domain and title of an annotation for display on an annotation
 * card.
 */
export function domainAndTitle(annotation: Annotation): DomainAndTitle {
  return {
    domain: domainTextFromAnnotation(annotation),
    titleText: titleTextFromAnnotation(annotation),
    titleLink: titleLinkFromAnnotation(annotation),
  };
}

function titleLinkFromAnnotation(annotation: Annotation): string | null {
  var titleLink: string | undefined = annotation.uri;

  if (titleLink && !(titleLink.indexOf('http://') === 0 || titleLink.indexOf('https://') === 0)) {
    // We only link to http(s) URLs.
    titleLink = undefined;
  }

  if (annotation.links && annotation.links.incontext) {
    titleLink = annotation.links.incontext;
  }

  return titleLink || null;
}

function domainTextFromAnnotation(annotation: Annotation): string {
  var document: DocumentMetadata = documentMetadata(annotation);

  var domainText = '';
  if (document.uri && document.uri.indexOf('file://') === 0 && document.title) {
    var parts = document.uri.split('/');
    var filename = parts[parts.length - 1];
    if (filename) {
      domainText = filename;
    }
  } else if (document.domain && document.domain !== document.title) {
    domainText = document.domain;
  }

  return domainText;
}

function titleTextFromAnnotation(annotation: Annotation): string {
  var document: DocumentMetadata = documentMetadata(annotation);

  var titleText = document.title;
  if (titleText.length > 30) {
    titleText = titleText.slice(0, 30) + '…';
  }

  return titleText;
}

/** Return `true` if the given annotation is a reply, `false` otherwise. */
export function isReply(annotation: Annotation): boolean {
  return (annotation.references || []).length > 0;
}

//Return "true" if the annotation is a forward annotation, "false" otherwise
export function isForward(annotation: Annotation): boolean {
  return annotation.forwarded || false;
}

/** Return `true` if the given annotation is new, `false` otherwise.
 *
 * "New" means this annotation has been newly created client-side and not
 * saved to the server yet.
 */
export function isNew(annotation: Annotation): boolean {
  return !annotation.id;
}

/** Return `true` if the given annotation is public, `false` otherwise. */
export function isPublic(annotation: Annotation): boolean {
  var isPublic = false;

  if (!annotation.permissions) {
    return isPublic;
  }

  annotation.permissions.read?.forEach(function(perm) {
    var readPermArr = perm.split(':');
    if (readPermArr.length === 2 && readPermArr[0] === 'group') {
      isPublic = true;
    }
  });

  return isPublic;
}

/**
 * Return `true` if `annotation` has a selector.
 *
 * An annotation which has a selector refers to a specific part of a document,
 * as opposed to a Page Note which refers to the whole document or a reply,
 * which refers to another annotation.
 */
function hasSelector(annotation: Annotation): boolean {
  if (!annotation.target) return false;
  if (annotation.target.length == 0) return false;
  if (!annotation.target[0].selector) return false;
  return annotation.target[0].selector.length > 0;
}

/**
 * Return `true` if the given annotation is not yet anchored.
 *
 * Returns false if anchoring is still in process but the flag indicating that
 * the initial timeout allowed for anchoring has expired.
 */
export function isWaitingToAnchor(annotation: Annotation): boolean {
  return hasSelector(annotation) &&
         (typeof annotation.$orphan === 'undefined') &&
         !annotation.$anchorTimeout;
}

/** Return `true` if the given annotation is an orphan. */
export function isOrphan(annotation: Annotation): boolean {
  return hasSelector(annotation) && !!annotation.$orphan;
}

export function isOrphanAndNotProcessed(annotation: Annotation): boolean {
  return hasSelector(annotation) && !!annotation.$orphan && !isProcessed(annotation);
}

/** Return `true` if the given annotation is a page note. */
export function isPageNote(annotation: Annotation): boolean {
  return !hasSelector(annotation) && !isReply(annotation);
}

export function isPageNoteAndNotProcessed(annotation: Annotation): boolean {
  return !hasSelector(annotation) && !isReply(annotation) && !isProcessed(annotation);
}

/** Return `true` if the given annotation is a top level annotation, `false` otherwise. */
export function isAnnotation(annotation: Annotation): boolean {
  return !!(hasSelector(annotation) && !isOrphan(annotation));
}

export function isAnnotationAndNotProcessed(annotation: Annotation): boolean {
  return !!(hasSelector(annotation) && !isOrphan(annotation) && !isProcessed(annotation));
}

function isAnnotationAndProcessed(annotation: Annotation): boolean {
  return !!(hasSelector(annotation) && !isOrphan(annotation) && isProcessed(annotation));
}

export function isAnnotationAndProcessedSent(annotation: Annotation): boolean {
  if (isAnnotationAndProcessed(annotation)) return isSent(annotation);
  return isAnnotationAndNotProcessed(annotation);
}

/** Return a numeric key that can be used to sort annotations by location.
 *
 * @return {number} - A key representing the location of the annotation in
 *                    the document, where lower numbers mean closer to the
 *                    start.
 */
export function location(annotation: Annotation): number {
  if (annotation) {
    var targets = annotation.target || [];
    for (var i = 0; i < targets.length; i++) {
      var selectors = targets[i].selector || [];
      for (var k = 0; k < selectors.length; k++) {
        if (selectors[k].type === 'TextPositionSelector') {
          return selectors[k].start;
        }
      }
    }
  }
  return Number.POSITIVE_INFINITY;
}

/**
 * Return the number of times the annotation has been flagged
 * by other users. If moderation metadata is not present, returns `null`.
 *
 * @return {number|null}
 */
export function flagCount(ann: Annotation): number | null {
  return ann.moderation?.flagCount || null;
}

export function hasContent(annotation: Annotation) : boolean {
  return (annotation.text?.length || 0)> 0;
}

export function hasFeedback(annotation: Annotation) : boolean {
  if (!annotation.feedback) return false;
  if (!annotation.feedback.text) return false;
  return annotation.feedback.text.length > 0;
}

export function hasTags(annotation: Annotation) : boolean {
  return (annotation.tags?.length || 0) > 0;
}

export function isFeedbackSent(annotation: Annotation): boolean {
    if (!hasFeedback(annotation)) return false;
    return (annotation.feedback?.status === FEEDBACK_STATUS.SENT);
}

export function isHighlight(annotation: Annotation) : boolean {
  if (annotation.tags?.includes('highlight') || false) return true;
  // Once an annotation has been saved to the server there's no longer a simple property that says whether it's a highlight or not. Instead an annotation is
  // considered a highlight if it a) has content, b) is linked to a specific part of the document and c) has not been censored (hidden).
  if (isNew(annotation) || isPageNote(annotation) || isReply(annotation) || annotation.hidden) {
    return false;
  }
  return !hasContent(annotation) && !hasTags(annotation);
}

export function isSuggestion(annotation: Annotation) : boolean {
  return (annotation.tags != undefined) && annotation.tags.includes('suggestion');
}

export function isNormal(annotation: Annotation) : boolean {
  return annotation.status?.status === ANNOTATION_STATUS.NORMAL;
}

export function isProcessed(annotation: Annotation) : boolean {
  return annotation.status?.status !== ANNOTATION_STATUS.NORMAL;
}

export function isSent(annotation: Annotation) : boolean {
  return annotation.document !== undefined && annotation.document.metadata !== undefined 
    && annotation.document.metadata['responseStatus'] === RESPONSE_STATUS.SENT;
}

export function isTreated(annotation: Annotation) : boolean {
	return annotation.status?.status === ANNOTATION_STATUS.TREATED;
}

export function isDeleted(annotation: Annotation) : boolean {
	return annotation.status?.status === ANNOTATION_STATUS.DELETED;
}

export function isAcceptedOrRejected(annotation: Annotation) : boolean {
	return isSuggestion(annotation) 
    && annotation.status !== undefined 
    && (annotation.status.status === ANNOTATION_STATUS.ACCEPTED || annotation.status.status === ANNOTATION_STATUS.REJECTED);
}

export function isContribution(annotation: Annotation) : boolean {
  return annotation.document?.metadata?.contributionId !== undefined;
}

export function isContributionInPreparation(annotation: Annotation) : boolean {
  if(!isContribution(annotation)) return false;
  return annotation.document?.metadata?.contributionStatus === RESPONSE_STATUS.IN_PREPARATION;
}

export function isISC(annotation: Annotation | undefined): boolean {
  if (!annotation) return false;
  return annotation.document?.metadata?.systemId === SYSTEM_ID.ISC;
}
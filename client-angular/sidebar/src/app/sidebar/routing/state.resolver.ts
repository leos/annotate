import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';
import {GroupsService,  IGroupsService } from '../services/groups.service';
import  { SessionService,ISessionService } from '../services/session.service';
import { mergeMap, tap } from 'rxjs';
import { Group } from '../models/groups.model';

const StateResolver: ResolveFn<Group[]> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const groupsService: IGroupsService = inject(GroupsService);
    const sessionService: ISessionService = inject(SessionService);
    return sessionService.load().pipe(
        mergeMap(() => {
            return groupsService.load();
        })
    )      
}

export default StateResolver;
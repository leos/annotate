'use strict';

import * as retry from 'retry';
import EventEmitter from './imports/event-emitter';

// see https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
const CLOSE_NORMAL = 1000;

// Minimum delay, in ms, before reconnecting after an abnormal connection close.
const RECONNECT_MIN_DELAY = 1000;

/**
 * Socket is a minimal wrapper around WebSocket which provides:
 *
 * - Automatic reconnection in the event of an abnormal close
 * - Queuing of messages passed to send() whilst the socket is
 *   connecting
 * - Uses the standard EventEmitter API for reporting open, close, error
 *   and message events.
 */
export class Socket extends EventEmitter {
  /** 
   * Queue of JSON objects which have not yet been submitted
  */
  messageQueue: any[];
  socket: WebSocket | null;
  operation: any;

  constructor(private url: string) {
    super();

    // queue of JSON objects which have not yet been submitted
    this.messageQueue = [];

    // the current WebSocket instance
    this.socket = null;

    // a pending operation to connect a WebSocket
    this.operation = null;

    this.connect();
  }
    sendMessages() {
      while (this.messageQueue.length > 0) {
        var messageString: string = JSON.stringify(this.messageQueue.shift());
        this.socket?.send(messageString);
      }
    }

    // Connect the websocket immediately. If a connection attempt is already in
    // progress, do nothing.
    connect() {
      var self = this;
      if (this.operation) {
        return;
      }

      this.operation = retry.operation({
        minTimeout: RECONNECT_MIN_DELAY * 2,
        // Don't retry forever -- fail permanently after 10 retries
        retries: 10,
        // Randomize retry times to minimise the thundering herd effect
        randomize: true,
      });

      this.operation.attempt(function () {
        const socket = new WebSocket(self.url);
        socket.onopen = (event: Event) => {
          self.onOpen();
          self.emit('open', event);
        };
        socket.onclose = (event: CloseEvent) => {
          if (event.code === CLOSE_NORMAL) {
            self.emit('close', event);
            return;
          }
          var err = new Error('WebSocket closed abnormally, code: ' + event.code);
          console.warn(err);
          self.onAbnormalClose(err);
        };
        socket.onerror = (event: Event) => {
          self.emit('error', event);
        };
        socket.onmessage = (event: MessageEvent) => {
          self.emit('message', event);
        };
        self.socket = socket;
      });
    }

    // onOpen is called when a websocket connection is successfully established.
    onOpen() {
      this.operation = null;
      this.sendMessages();
    }

    // onAbnormalClose is called when a websocket connection closes abnormally.
    // This may be the result of a failure to connect, or an abnormal close after
    // a previous successful connection.
    onAbnormalClose(error: any) {
      // If we're already in a reconnection loop, trigger a retry...
      if (this.operation) {
        if (!this.operation.retry(error)) {
          console.error('reached max retries attempting to reconnect websocket');
        }
        return;
      }
      // ...otherwise reconnect the websocket after a short delay.
      var delay = RECONNECT_MIN_DELAY;
      var self = this;
      delay += Math.floor(Math.random() * delay);
      this.operation = setTimeout(function () {
        self.operation = null;
        self.connect();
      }, delay);
    }

    /** Close the underlying WebSocket connection */
    close() {
      this.socket?.close();
    };

    /**
     * Send a JSON object via the WebSocket connection, or queue it
     * for later delivery if not currently connected.
     */
    send(message: any) {
      this.messageQueue.push(message);
      if (this.isConnected()) {
        this.sendMessages();
      }
    };

    /** Returns true if the WebSocket is currently connected. */
    isConnected() {
      return this.socket?.readyState === WebSocket.OPEN;
    }
}

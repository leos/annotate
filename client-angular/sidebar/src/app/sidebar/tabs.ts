'use strict';

// Selectors that calculate the annotation counts displayed in tab headings
// and determine which tab an annotation should be displayed in.

import { countIf } from './util/array.util';
import * as metadata from './annotation-metadata';
import * as uiConstants from './ui-constants';
import * as ANNOTATION_STATUS from './annotation-status';
import { Annotation } from '../../../../shared/models/annotation.model';

/**
 * Return the tab in which an annotation should be displayed.
 *
 * @param {Annotation} ann
 */
export function tabForAnnotation(ann: Annotation): string {
  if (metadata.isOrphan(ann)) {
    return uiConstants.TAB_ORPHANS;
  } else if (metadata.isPageNote(ann)) {
    return uiConstants.TAB_NOTES;
  } else {
    return uiConstants.TAB_ANNOTATIONS;
  }
}

/**
 * Return true if an annotation should be displayed in a given tab.
 *
 * @param {Annotation} ann
 * @param {string} tab - The TAB_* value indicating the tab
 */
export function shouldShowInTab(ann: Annotation, tab: string, showProcessedSentAnnotations: boolean = false) {
  if (metadata.isWaitingToAnchor(ann)) {
    // Until this annotation anchors or fails to anchor, we do not know which
    // tab it should be displayed in.
    return false;
  }
  if (tabForAnnotation(ann) !== tab) return false;
  if (!ann.status) return true;
  if (!showProcessedSentAnnotations || !metadata.isProcessed(ann)) return (ann.status.status === ANNOTATION_STATUS.NORMAL);
  return metadata.isSent(ann);
}

/**
 * Return the counts for the headings of different tabs.
 *
 * @param {Annotation[]} annotations - List of annotations to display
 */
export function counts(annotations: Annotation[], includeProcessedSentAnnotations: boolean = false) {
  var counts = {
    notes: countIf(annotations, metadata.isPageNoteAndNotProcessed),
    annotations: countIf(annotations, !includeProcessedSentAnnotations ? metadata.isAnnotationAndNotProcessed : metadata.isAnnotationAndProcessedSent),
    orphans: countIf(annotations, metadata.isOrphanAndNotProcessed),
    anchoring: countIf(annotations, metadata.isWaitingToAnchor),
  };

  return counts;
}
'use strict';

import { Injectable }  from '@angular/core';
import { IHypothesisJsonConfig } from '../../../../shared/models/config.model';
import { IHypothesisConfgurationService } from './services/hypothesis-configuration.service';

export interface CrossOriginRequest {
  id: string;
  method: string;
}

export interface CrossOriginResponse {
  jsonrpc: string;
  id: string;
  result?: any;
  error?: any;
}

export interface ICrossOriginRpc {
  start(): void;
  receiveMessage(event: any): void;
  /** Return a JSON-RPC response to the given JSON-RPC request object. */
  jsonRpcResponse(request: CrossOriginRequest): CrossOriginResponse;
}


/**
 * Begin responding to JSON-RPC requests from frames on other origins.
 *
 * Register a window.postMessage() event listener that receives and responds to
 * JSON-RPC requests sent by frames on other origins using postMessage() as the
 * transport layer.
 *
 * Only frames whose origin is in the rpcAllowedOrigins config setting will be
 * responded to.
 *
 * This is a very partial implementation of a JSON-RPC 2.0 server:
 *
 * http://www.jsonrpc.org/specification
 *
 * The only part that we support so far is receiving JSON-RPC 2.0 requests (not
 * notifications) without any parameters and sending back a successful
 * response. Notifications (JSON-RPC calls that don't require a response),
 * method parameters, and error responses are not yet supported.
 *
 */
@Injectable()
export class CrossOriginRpc implements ICrossOriginRpc {

  private settings: IHypothesisJsonConfig;

  constructor(private $window: Window,
    private store: any, 
    private settingsService: IHypothesisConfgurationService) {
      this.settings = settingsService.getConfiguration();
  }

  start(): void {
    const self = this;
    this.$window.addEventListener('message', (event: any) => self.receiveMessage(event));
  }

  receiveMessage(event: any): void {
      const allowedOrigins: string[] = this.settings.rpcAllowedOrigins || [];
      if (!allowedOrigins.includes(event.origin)) {
        return;
      }
  
      // The entire JSON-RPC request object is contained in the postMessage()
      // data param.
      const jsonRpcRequest = event.data as CrossOriginRequest;
      const jsonRpcResponse: CrossOriginResponse = this.jsonRpcResponse(jsonRpcRequest);
      event.source?.postMessage(jsonRpcResponse, event.origin);
  };

  /** Return a JSON-RPC response to the given JSON-RPC request object. */
  jsonRpcResponse(request: CrossOriginRequest): CrossOriginResponse {
    // The set of methods that clients can call.
    const methods: any = {
      'searchUris': this.store.searchUris,
    };

    const method: Function | undefined = methods[request.method];

    let response: CrossOriginResponse = {
      'jsonrpc': '2.0',
      'id': request.id,
    };

    if (method) {
      response.result = method();
    } else {
      response.error = {
        'code': -32601,
        'message': 'Method not found',
      };
    }
    return response;
  }  
}
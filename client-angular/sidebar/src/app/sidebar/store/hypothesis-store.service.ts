import { IStoreService, StoreService } from './store.service';
import AnnotationStore, { IAnnotationStore, AnnotationStoreState } from './modules/annotation-store.service';
import FramesStore, { IFramesStore, FramesStoreState } from './modules/frames-store.service';
import GroupsStore, { IGroupsStore, GroupsStoreState } from './modules/groups-store.service';
import LinksStore, { ILinksStore, LinksStoreState } from './modules/links-store.service';
import SelectionStore, { ISelectionStore, SelectionStoreState } from './modules/selection-store.service';
import SessionStore, { ISessionStore, SessionStoreState } from './modules/session-store.service';
import ViewerStore, { IViewerStore, ViewerStoreState } from './modules/viewer-store.service';
import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import { subscribeChildStore } from './util';
import { Injectable } from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

export interface HypothesisStoreState extends AnnotationStoreState,
    FramesStoreState,
    GroupsStoreState,
    LinksStoreState,
    SelectionStoreState,
    SessionStoreState,
    ViewerStoreState 
{
        operationMode?: string;
        hostState?: string;
}

type StoreState = AnnotationStoreState 
    | FramesStoreState  
    | GroupsStoreState 
    | LinksStoreState 
    | SelectionStoreState 
    | SessionStoreState 
    | ViewerStoreState;

export interface IHypothesisStore extends IStoreService<HypothesisStoreState> {
    readonly annotation: IAnnotationStore;
    readonly frames: IFramesStore;
    readonly groups: IGroupsStore;
    readonly links: ILinksStore;
    readonly selection: ISelectionStore;
    readonly session: ISessionStore;
    readonly viewer: IViewerStore;

    setOperationMode(operationMode?: string): void
    setHostState(hostState?: string): void;
}

@Injectable()
export class HypothesisStoreService extends StoreService<HypothesisStoreState> implements IHypothesisStore {
    private annotationStore: IAnnotationStore;
    private framesStore: IFramesStore;
    private groupsStore: IGroupsStore;
    private linksStore: ILinksStore;
    private selectionStore: ISelectionStore;
    private sessionStore: ISessionStore;
    private viewerStore: IViewerStore;


    constructor(configurationService: HypothesisConfigurationService) {
        const settings: IHypothesisConfiguration = configurationService.getConfiguration();
        const annotationStore = new AnnotationStore();
        const framesStore = new FramesStore();
        const groupsStore = new GroupsStore();
        const linksStore = new LinksStore();
        const selectionStore = new SelectionStore(settings);
        const sessionStore = new SessionStore();
        const viewerStore = new ViewerStore();

        super(Object.assign({
                operationMode: undefined,
                hostState: undefined
            },
            annotationStore.getState(),
            framesStore.getState(),
            groupsStore.getState(),
            selectionStore.getState(),
            sessionStore.getState(),
            viewerStore.getState(),
            linksStore.getState()
        ));

        annotationStore.setSelectionStore(selectionStore);
        annotationStore.setViewerStore(viewerStore);
        this.annotationStore = annotationStore;
        this.subscribeChildStore(annotationStore); 

        this.framesStore = framesStore;
        this.subscribeChildStore(framesStore);  

        this.groupsStore = groupsStore;
        this.subscribeChildStore(groupsStore);  

        this.linksStore = linksStore;
        this.subscribeChildStore(linksStore); 

        selectionStore.setAnnotationStore(this.annotationStore);
        this.selectionStore = selectionStore;
        this.subscribeChildStore(selectionStore);  

        this.sessionStore = sessionStore;
        this.subscribeChildStore(sessionStore);  

        this.viewerStore = viewerStore;
        this.subscribeChildStore(viewerStore);
    }

    private subscribeChildStore(childStore: IStoreService<StoreState>) {
        const self = this;
        subscribeChildStore(childStore, (state: StoreState) => {
            self._state = Object.assign(self._state, state);
            self.emitStateUpdate();
        });           
    }

    get annotation(): IAnnotationStore {
        return this.annotationStore;
    }

    get frames(): IFramesStore {
        return this.framesStore;
    }

    get groups(): IGroupsStore {
        return this.groupsStore;
    }

    get links(): ILinksStore {
        return this.linksStore;
    }

    get selection(): ISelectionStore {
        return this.selectionStore;
    }

    get session(): ISessionStore {
        return this.sessionStore;
    }

    get viewer(): IViewerStore {
        return this.viewerStore;
    }

    setHostState(hostState?: string): void {
        this._state.hostState = hostState;
        this.emitStateUpdate();
    }

    setOperationMode(operationMode?: string): void {
        this._state.operationMode = operationMode;
        this.emitStateUpdate();
    }
}
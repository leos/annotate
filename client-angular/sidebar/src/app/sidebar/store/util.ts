'use strict';

import { IStoreService, SubscribeCallback } from './store.service';

export function subscribeChildStore(childStore: IStoreService<any>, subscribeCallback: SubscribeCallback<any>) {
  childStore.getObservable().subscribe({
    next(state: any) {
      subscribeCallback(state);
    }
  });
}
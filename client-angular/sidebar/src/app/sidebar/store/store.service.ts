import { Observable, Subject } from 'rxjs';

export type SubscribeCallback<T> = (state: T) => void;

export type UnsubscribeFunction = VoidFunction;

export interface IStoreService<T> {
    readonly state: T;
    getState(): T;
    getObservable(): Observable<T>;
    subscribe(action: SubscribeCallback<T>): UnsubscribeFunction;
    unsubscribeAll(): void;
}

export class StoreService<T> implements IStoreService<T> {
    protected subscriptions: { [id:string]: SubscribeCallback<T> };
    protected _state: T;
    protected stateSubject: Subject<T>;
    private nextSubscripionId: number;

    constructor(state: T) {
        this._state = Object.assign({}, state);
        this.subscriptions = {};
        this.stateSubject = new Subject();
        this.nextSubscripionId = 0;
    }

    get state(): T {
        return Object.assign({}, this._state);
    }

    public getState(): T {
        return Object.assign({}, this._state);
    }

    public getObservable(): Observable<T> {
        return this.stateSubject.asObservable();
    }

    public subscribe(subscription: SubscribeCallback<T>): UnsubscribeFunction {
        const subscriptionId: string = this.getNextSubscripionId().toString();
        this.subscriptions[subscriptionId] = subscription;

        return () => {
            if (!this.subscriptions[subscriptionId]) return;
            delete this.subscriptions[subscriptionId];
        };
    }

    private getNextSubscripionId(): number {
        this.nextSubscripionId += 1;
        return this.nextSubscripionId;
    }

    public unsubscribeAll(): void {
        this.subscriptions = {};
    }

    protected emitStateUpdate(): void {
        const state: T = this._state;
        this.stateSubject.next(state);

        const keys: string[] = Object.keys(this.subscriptions);
        for(const key of keys) {
            this.subscriptions[key](state);
        }
    }
} 
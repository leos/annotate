import { IStoreService, StoreService } from '../store.service';
import { UserProfile } from '../../models/session.model'

export interface SessionState extends UserProfile {
}

export interface SessionStoreState {
    session: SessionState;
}

export interface ISessionStore extends IStoreService<SessionStoreState> {
    /**
     * Update the session state.
     */
    updateSession(session: SessionState): void;

    /**
     * Return true if a given feature flag is enabled.
     *
     * @param {string} feature - The name of the feature flag. This matches the
     *        name of the feature flag as declared in the Hypothesis service.
     */
    isFeatureEnabled(feature: string): boolean;

    /**
     * Returns the current user's profile fetched from the `/api/profile` endpoint.
     */
    profile(): SessionState;
}

export default class SessionStore extends StoreService<SessionStoreState> implements ISessionStore {
    constructor() {
       super({
            session: {
                features: undefined,
                preferences: undefined,
                userid: undefined
            }
        });
    }

    updateSession(session: SessionState): void {
        this._state.session = session;
        this.emitStateUpdate();
    }
    
    isFeatureEnabled(feature: string): boolean {
        if (!this.state.session.features) return false;
        const features = this.state.session.features as any;
        return features[feature];
    }

    profile(): SessionState {
        return this.state.session;
    }
}
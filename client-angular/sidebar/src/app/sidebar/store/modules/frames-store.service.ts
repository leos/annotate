import { IStoreService, StoreService } from '../store.service';

export interface Frame {
    id: string;
    metadata?: any;
    uri?: string;
    isAnnotationFetchComplete?: boolean;
}

export interface FramesStoreState {
    /** The list of frames connected to the sidebar app */
    frames: Frame[];
}

export interface IFramesStore extends IStoreService<FramesStoreState> {
    /**
     * Add a frame to the list of frames currently connected to the sidebar app.
     */
    connectFrame(frame: Frame): void;

    /**
     * Remove a frame from the list of frames currently connected to the sidebar app.
     */
    destroyFrame(frame: Frame): void;

    /**
     * Update the `isAnnotationFetchComplete` flag of the frame.
     */
    updateFrameAnnotationFetchStatus(uri: string, status: boolean): void;

    /**
     * Return the list of frames currently connected to the sidebar app.
     */
    frames(): Frame[];

    searchUrisForFrame(frame: Frame): string[];

    /**
     * Return the set of URIs that should be used to search for annotations on the
     * current page.
     */    
    searchUris(): string[];

}

export default class FramesStore extends StoreService<FramesStoreState> implements IFramesStore { 
    constructor() {
        super({ frames: [] })
    }

    connectFrame(frame: Frame): void {
        this._state.frames.push(frame);
        this.emitStateUpdate();
    }

    destroyFrame(frame: Frame): void {
        this._state.frames = this.state.frames.filter((f: Frame) => f.id != frame.id);
        this.emitStateUpdate();
    }

    updateFrameAnnotationFetchStatus(uri: string, status: boolean): void {
        this._state.frames = this.state.frames.map((frame: Frame) => {
            let match: boolean = (frame.uri === uri);
            if (!match) return frame;

            frame.isAnnotationFetchComplete = status;
            return frame;
        });
        this.emitStateUpdate();
    }

    frames(): Frame[] {
        return this._state.frames.concat([]);
    }

    searchUrisForFrame(frame: Frame): string[] {
        var uris: string[] = !frame.uri ? [] : [frame.uri];

        if (frame.metadata?.documentFingerprint) {
            uris = frame.metadata.link.map((link: any) => {
              return link.href;
            });
        }
        if (frame.metadata?.link) {
            frame.metadata.link.forEach(function (link: any) {
              if (link.href.startsWith('doi:')) {
                uris.push(link.href);
              }
            });
        }
        return uris;       
    }

    searchUris(): string[] {
        const self = this;
        return this.state.frames.reduce((uris: string[], frame: Frame) => {
            return uris.concat(self.searchUrisForFrame(frame))
        }, []);
    }
}
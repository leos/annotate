import { IStoreService, StoreService } from '../store.service';
import { ISelectionStore } from './selection-store.service';
import { IViewerStore } from './viewer-store.service';
import { Annotation } from '../../../../../../shared/models/annotation.model';
import * as arrayUtil from '../../util/array.util';
import * as metadata from '../../annotation-metadata';
import * as uiConstants from '../../ui-constants';


/**
 * Return a copy of `current` with all matching annotations in `annotations`
 * removed.
 */
function excludeAnnotations(current: Annotation[], annotations: Annotation[]): Annotation[] {
    var ids: any = {};
    var tags: any = {};
    annotations.forEach(function (annot: Annotation) {
      if (annot.id) {
        ids[annot.id!] = true;
      }
      if (annot.$tag) {
        tags[annot.$tag!] = true;
      }
    });
    return current.filter(function (annot) {
      var shouldRemove = (annot.id && (annot.id in ids)) ||
                         (annot.$tag && (annot.$tag in tags));
      return !shouldRemove;
    });
}
  
function findByID(annotations: Annotation[], id?: string): Annotation | undefined {
    return annotations.find((annot: Annotation) => {
        return annot.id === id;
    });
}
  
function findByTag(annotations: Annotation[], tag?: string): Annotation | undefined {
    return annotations.find((annot: Annotation) => {
        return annot.$tag === tag;
    });
}
  
/**
 * Initialize the status flags and properties of a new annotation.
 */
function initializeAnnot(annotation: Annotation, tag: string): Annotation {
    var orphan = annotation.$orphan;

    if (!annotation.id) {
        // Currently the user ID, permissions and group of new annotations are
        // initialized in the <annotation> component controller because the session
        // state and focused group are not stored in the Redux store. Once they are,
        // that initialization should be moved here.

        // New annotations must be anchored
        orphan = false;
    }

    return Object.assign({}, annotation, {
        // Flag indicating whether waiting for the annotation to anchor timed out.
        $anchorTimeout: false,
        $tag: annotation.$tag || tag,
        $orphan: orphan,
    });
}

export interface AnchoringUpdateState {
  anchorLeosDeleted?: boolean;
  anchoringStatus?: string;
  anchoredRangeText?: string;
  anchorSelectorMoved?: boolean;
}

export interface AnnotationStoreState {
    annotations: Annotation[];

    /** 
    * The local tag to assign to the next annotation that is loaded into the app
    */ 
    nextTag: number;
}

export interface IAnnotationStore extends IStoreService<AnnotationStoreState> {
  /** Remove annotations from the currently displayed set. */
  removeAnnotations(annotations: Annotation[]): void;

  addAnnotations(annotations: Annotation[], now?: Date): void;
  
  /**
   * Updating the flagged status of an annotation.
   *
   * @param {string} id - Annotation ID
   * @param {boolean} isFlagged - The flagged status of the annotation. True if
   *        the user has flagged the annotation.
   */  
  updateFlagStatus(id: string, isFlagged: boolean): void
  
  updateAnnotationStatus(id: string, status: string): void;
  
  /** Set the currently displayed annotations to the empty set. */
  clearAnnotations(): void;

  /**
   * Update the anchoring status of an annotation.
   *
   * @param {{ [tag: string]: {
   *  anchoringStatus: 'anchored'|'orphan'|'timeout',
   *  anchoredRangeText: "The text of the range, that this annotation has been anchored to"
   * }
   * } } statusUpdates - A map of annotation tag to orphan status and anchored range text
   */
  updateAnchorStatus(statusUpdates: Object): void;

  /**
   * Update the local hidden state of an annotation.
   *
   * This updates an annotation to reflect the fact that it has been hidden from
   * non-moderators.
   */
  hideAnnotation(id: string): void;
  
  /**
   * Update the local hidden state of an annotation.
   *
   * This updates an annotation to reflect the fact that it has been made visible
   * to non-moderators.
   */
  unhideAnnotation(id: string): void;
  /**
   * Return all loaded annotations which have been saved to the server.
   *
   * @returns Annotation[]
   */
  savedAnnotations(): Annotation[];

  /** Return true if the annotation with a given ID is currently loaded. */
  annotationExists(id: string): boolean;

  /**
   * Return the IDs of annotations that correspond to `tags`.
   *
   * If an annotation does not have an ID because it has not been created on
   * the server, there will be no entry for it in the returned array.
   *
   * @param {string[]} Local tags of annotations to look up
   */
  findIDsForTags( tags: string[]): string[];

  /**
   * Return the annotation with the given ID.
   */
  findAnnotationByID(id: string): Annotation | undefined;
  findAnnotationByTag(tag: string): Annotation | undefined;
  getAnnotationCount(): number;
  getVisibleAnnotationCount(): number;
  getNotesCount(): number;
  getVisibleNotesCount(): number;
  setSelectionStore(selectionStore?: ISelectionStore): void;
  setViewerStore(viewerStore?: IViewerStore): void;
}

export default class AnnotationStore extends StoreService<AnnotationStoreState> implements IAnnotationStore {
  private selectionStore?: ISelectionStore;
  private viewerStore?: IViewerStore;

  constructor() {
      super({
        annotations: [],
        nextTag: 1
     });
  }

  setSelectionStore(selectionStore?: ISelectionStore): void {
    this.selectionStore = selectionStore;
  }

  setViewerStore(viewerStore?: IViewerStore): void {
    this.viewerStore = viewerStore;
  }

  addAnnotations(annotations: Annotation[], now?: Date): void {
    const self = this;
    const _now: Date = now || new Date();
    const annots: Annotation[] = annotations.map((annot: Annotation) => {
      if (annot.id) { return annot; }
      return Object.assign({
        // Date.prototype.toISOString returns a 0-offset (UTC) ISO8601
        // datetime.
        created: _now.toISOString(),
        updated: _now.toISOString(),
      }, annot);
    });


    const added = annotations.filter((annot: Annotation) => {
      return !findByID(self.state.annotations, annot.id);
    });
    this.processAnnotations(annots);
    if (!this.viewerStore?.isSidebar()) return;

    const anchoringIDs: string[] = added.filter(metadata.isWaitingToAnchor)
      .map((ann: Annotation) => ann.id!);
    if (anchoringIDs.length == 0) return;

    // If anchoring fails to complete in a reasonable amount of time, then
    // we assume that the annotation failed to anchor. If it does later
    // successfully anchor then the status will be updated.
    const ANCHORING_TIMEOUT = 500; 
    setTimeout(() => {
      // Find annotations which haven't yet been anchored in the document.
      const anns: Annotation[] = self.state.annotations;
      const annsStillAnchoring: Annotation[] = anchoringIDs.map((id: string) => findByID(anns, id)!)
        .filter((ann: Annotation) => ann != undefined)
        .filter((ann: Annotation) => {
          return metadata.isWaitingToAnchor(ann);
        });

      // Mark anchoring as timed-out for these annotations.
      const anchorStatusUpdates: Object = annsStillAnchoring.reduce((updates: any, ann: Annotation) => {
        updates[ann.$tag!] = { 
          anchoredRangeText: (updates[ann.$tag!] != undefined) ? updates[ann.$tag!].anchoredRangeText : undefined, 
          anchoringStatus: 'timeout'
        };
        return updates;
      }, {});
      self.updateAnchorStatus(anchorStatusUpdates);
    }, ANCHORING_TIMEOUT);      
  }

  private processAnnotations(annotations: Annotation[]): void {
      var updatedIDs: any = {};
      var updatedTags: any = {};
  
      var added: Annotation[] = [];
      var unchanged: Annotation[] = [];
      var updated: Annotation[] = [];
      var nextTag = this.state.nextTag;

      annotations.forEach((annotation: Annotation) => {
          let existing: Annotation | undefined = undefined;
          if (annotation.id) {
            existing = findByID(this.state.annotations, annotation.id);
          }
          if (existing == undefined && annotation.$tag) {
            existing = findByTag(this.state.annotations, annotation.$tag);
          }
    
          if (existing != undefined) {
            // Merge the updated annotation with the private fields from the local
            // annotation
            updated.push(Object.assign({}, existing, annotation));
            if (annotation.id) {
              updatedIDs[annotation.id] = true;
            }
            if (existing.$tag) {
              updatedTags[existing.$tag] = true;
            }
          } else {
            added.push(initializeAnnot(annotation, 't' + nextTag));
            ++nextTag;
          }

      });

      this.state.annotations.forEach(function (annotation: Annotation) {
          let isUnchanged = (!annotation.id || !updatedIDs[annotation.id]);
          isUnchanged = isUnchanged && (!annotation.$tag || !updatedTags[annotation.$tag]);
          if (isUnchanged) {
            unchanged.push(annotation);
          }
      });

      this._state.nextTag = nextTag;
      this.updateAnnotations(added.concat(updated).concat(unchanged));
  }
  
  removeAnnotations(annotations: Annotation[]): void {
    const remainingAnnotations: Annotation[] = excludeAnnotations(this.state.annotations, annotations);
    
    var selectedTab: string | undefined = this.selectionStore?.state.selectedTab;
    if (selectedTab === uiConstants.TAB_ORPHANS &&
        arrayUtil.countIf(remainingAnnotations, metadata.isOrphan) === 0) {
      selectedTab = uiConstants.TAB_ANNOTATIONS;
    }

    this.updateAnnotations(remainingAnnotations);
    this.selectionStore?.selectTab(selectedTab);   
  }

  updateFlagStatus(id: string, isFlagged: boolean): void {
    const annotations: Annotation[] = this.state.annotations
      .map((annot: Annotation) => {
          const isMatch: boolean = (annot.id === id);
          if (!isMatch) return annot;
          if (annot.flagged === isFlagged) return annot;
          
          const newAnnot = Object.assign({}, annot, {
            flagged: isFlagged,
          });
          if (!newAnnot.moderation) return newAnnot;

          const countDelta: number = isFlagged ? 1 : -1;
          newAnnot.moderation = Object.assign({}, annot.moderation, {
            flagCount: annot.moderation?.flagCount || 0 + countDelta,
          });
          return newAnnot;         
      });
      this.updateAnnotations(annotations);
  }

  updateAnnotationStatus(id: string, status: string): void {
    const annotations: Annotation[] = this.state.annotations.map((annot: Annotation) => {
      const match = (annot.id === id);
      if (!match) return annot;

      const newAnn: Annotation = Object.assign({}, annot);
      if (newAnn.status) {
        newAnn.status.status = status;
      }
      return newAnn;
    });
    this.updateAnnotations(annotations);       
  }

  clearAnnotations(): void {
    this.updateAnnotations([]);
  }

  updateAnchorStatus(statusUpdates: any) {
    const annotations: Annotation[] = this.state.annotations
      .map((annot: Annotation) => {
        if (!annot.$tag) {
          return annot;
        }
        if (!statusUpdates[annot.$tag]) {
          return annot;
        }
        const updateState: AnchoringUpdateState = statusUpdates[annot.$tag];
        if (updateState.anchoringStatus === 'timeout') {
          return Object.assign({}, annot, { $anchorTimeout: true });
        } else {
          return Object.assign({}, annot, {
            $orphan: updateState.anchoringStatus === 'orphan',
            $leosDeleted: updateState.anchorLeosDeleted,
            $selectorMoved: updateState.anchorSelectorMoved,
            anchoredRangeText: updateState.anchoredRangeText
          });
        }
      });
      
    this.updateAnnotations(annotations);
  }

  hideAnnotation(id: string): void {
    const annotations: Annotation[] = this.state.annotations.map((ann: Annotation) => {
      if (ann.id !== id) {
        return ann;
      }
      return Object.assign({}, ann, { hidden: true });
    });

    this.updateAnnotations(annotations);
  }

  unhideAnnotation(id: string): void {
    var annotations: Annotation[] = this.state.annotations.map((ann: Annotation) => {
      if (ann.id !== id) {
        return ann;
      }
      return Object.assign({}, ann, { hidden: false });
    });

    this.updateAnnotations(annotations);
  }
  
  savedAnnotations(): Annotation[] {
    return this.state.annotations
      .filter((ann: Annotation) => !metadata.isNew(ann));
  }

  annotationExists(id: string): boolean {
    return this.state.annotations
      .some((annot: Annotation) => (annot.id === id));
  }

  findIDsForTags( tags: string[]): string[] {
    const state: AnnotationStoreState = this.state;
    var ids: string [] = [];
    tags.forEach((tag: string) => {
      var annot: Annotation | undefined = findByTag(state.annotations, tag);
      if ((annot != undefined) && (annot.id != undefined)) {
        ids.push(annot.id);
      }
    });
    return ids;
  }    

  findAnnotationByID(id: string): Annotation | undefined {
    return findByID(this.state.annotations, id);
  }
  
  findAnnotationByTag(tag: string): Annotation | undefined {
    return findByTag(this.state.annotations, tag);
  }

  getAnnotationCount(): number {
    return this.state.annotations.length;
  }

  getVisibleAnnotationCount(): number {
    return this.state.annotations.filter((annotation: Annotation) => !!annotation.visible).length;
  }

  getNotesCount(): number {
    return this.state.annotations.filter((annotation: Annotation) => metadata.isPageNote(annotation)).length;
  }

  getVisibleNotesCount(): number {
    return this.state.annotations.filter((annotation: Annotation) => metadata.isPageNote(annotation))
      .filter((annotation: Annotation) => !!annotation.visible).length;
  }

  private updateAnnotations(annotations: Annotation[]) {
    this._state.annotations = annotations;
    this.emitStateUpdate();
  }
}
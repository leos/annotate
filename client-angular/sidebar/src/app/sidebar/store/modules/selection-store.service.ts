import { toSet } from '../../util/array.util';
import * as uiConstants from '../../ui-constants';
import * as annotationMetadata from '../../annotation-metadata';
import { IStoreService, StoreService } from '../store.service';
import { IAnnotationStore } from './annotation-store.service';
import { Annotation, Suggestion } from '../../../../../../shared/models/annotation.model';

/**
* Default starting tab.
*/
var TAB_DEFAULT: string = uiConstants.TAB_ANNOTATIONS;

 /**
  * Default sort keys for each tab.
  */
var TAB_SORTKEY_DEFAULT: any = {};
TAB_SORTKEY_DEFAULT[uiConstants.TAB_ANNOTATIONS] = 'Location';
TAB_SORTKEY_DEFAULT[uiConstants.TAB_NOTES] = 'Oldest';
TAB_SORTKEY_DEFAULT[uiConstants.TAB_ORPHANS] = 'Location';

/**
 * Available sort keys for each tab.
 */
var TAB_SORTKEYS_AVAILABLE: any = {};
TAB_SORTKEYS_AVAILABLE[uiConstants.TAB_ANNOTATIONS] = ['Newest', 'Oldest', 'Location'];
TAB_SORTKEYS_AVAILABLE[uiConstants.TAB_NOTES] = ['Newest', 'Oldest'];
TAB_SORTKEYS_AVAILABLE[uiConstants.TAB_ORPHANS] = ['Newest', 'Oldest', 'Location'];


interface SelectionSettings {
    //annotation id:true pairs.
    annotations?: string;
    query?: string;
}

function initialSelection(settings: SelectionSettings): Object | undefined {
    var selection: any = {};
    if (settings.annotations && !settings.query) {
        selection[settings.annotations] = true;
    }
    return freeze(selection);
}

function freeze(selection: Object): Object | undefined {
    return (Object.keys(selection).length > 0) ? Object.freeze(selection) : undefined;
}

export interface SelectionStoreState {
    /** Contains a map of annotation tag:true pairs. */
    focusedAnnotationMap?: Object;

    /** Contains a map of annotation id:true pairs. */
    selectedAnnotationMap?: Object;

    /** 
     * Map of annotation IDs to expanded/collapsed state. For annotations not
     * present in the map, the default state is used which depends on whether
     * the annotation is a top-level annotation or a reply, whether it is
     * selected and whether it matches the current filter.
     */
    expanded: Object;

    /** 
     * Set of IDs of annotations that have been explicitly shown
     * by the user even if they do not match the current search filter
     */
    forceVisible: Object;

    /** IDs of annotations that should be highlighted  */
    highlighted: string[];

    filterQuery: string | undefined;
    filterReseted: boolean;
    selectedTab?: string;

    /** Key by which annotations are currently sorted. */
    sortKey: string;

    /** Keys by which annotations can be sorted. */
    sortKeysAvailable: string[];
}

export interface ISelectionStore extends IStoreService<SelectionStoreState> {
    select(selection: Object): void;

    /**
     * Set the currently selected annotation IDs.
     */
    selectAnnotations(ids: string[]): void;
    selectAnnotation(id?: string): void;
    deselectAnnotation(id?: string): void;
    deselectAllAnnotations(): void;

    /** Toggle whether annotations are selected or not. */
    toggleSelectedAnnotations(ids: string[]): void;

    /**
     * Sets whether a given annotation should be visible, even if it does not
     * match the current search query.
     *
     * @param {string} id - Annotation ID
     * @param {boolean} visible
     */
    setForceVisible(id: string, visible: boolean): void;
    
    /**
     * Sets which annotations are currently focused.
     *
     * @param {Array<string>} Tags of annotations to focus
     */
    focusAnnotations(tags: string[]): void;
    
    setCollapsed(id: string,  collapsed: boolean): void;

    /**
     * Highlight annotations with the given `ids`.
     *
     * This is used to indicate the specific annotation in a thread that was
     * linked to for example.
     */
    highlightAnnotations(ids: string[]): void;

    /** Set the type annotations to be displayed. */
    selectTab(type?: string): void;

    /** Set the query used to filter displayed annotations. */
    setFilterQuery(query: string): void;
    
    /** Sets the sort key for the annotation list. */
    setSortKey(key: string): void;
    setFilterReseted(filterReseted: boolean): void;

    clearFilteredAndSelectedAnnotations(): void;
    clearSearchFilter(): void;

    /**
     * Returns true if the annotation with the given `id` is selected.
     */
    isAnnotationSelected(id: string): boolean;

    /**
     * Return true if any annotations are currently selected.
     */
    hasSelectedAnnotations(): boolean;

    getSelectedAnnotationCount(): number;
    getSelectedAnnotations(): Annotation[];
    getSelectedSuggestions(): Suggestion[];

    getSelectedNotes(): Annotation[];
    getSelectedNotesCount(): number;

    setAnnotationStore(annotationStore?: IAnnotationStore): void;
}

export default class SelectionStore extends StoreService<SelectionStoreState> implements ISelectionStore {
    private annotationStore?: IAnnotationStore;

    constructor(settings: SelectionSettings) {
        super({
            focusedAnnotationMap: undefined,
            selectedAnnotationMap: Object.assign({}, initialSelection(settings)),
            expanded: Object.assign({}, initialSelection(settings) || {}),
            forceVisible: {},
            highlighted: [],
            filterQuery: settings.query ? `${settings.query}` : undefined,
            filterReseted : true,
            selectedTab: TAB_DEFAULT,
            sortKey: TAB_SORTKEY_DEFAULT[TAB_DEFAULT],
            sortKeysAvailable: TAB_SORTKEYS_AVAILABLE[TAB_DEFAULT]
        })
    }

    setAnnotationStore(annotationStore?: IAnnotationStore): void {
        this.annotationStore = annotationStore;
    }

    select(selection: Object): void {
        this._state.selectedAnnotationMap = freeze(selection);
        this.emitStateUpdate();
    }

    selectAnnotations(ids: string[]): void {
        this.select(toSet(ids));
    }

    toggleSelectedAnnotations(ids: string[]): void {
        const selection: any = Object.assign({}, this.state.selectedAnnotationMap);
        for (var i = 0; i < ids.length; i++) {
            var id = ids[i];
            if (selection[id]) {
                delete selection[id];
            } else {
                selection[id] = true;
            }
        }
        this.select(selection);
    }

    setForceVisible(id: string, visible: boolean): void {
        const forceVisible: any = Object.assign({}, this.state.forceVisible);
        forceVisible[id] = visible;

        this._state.forceVisible = forceVisible;
        this.emitStateUpdate();
    }

    focusAnnotations(tags: string[]): void {
        this._state.focusedAnnotationMap = freeze(toSet(tags));
        this.emitStateUpdate();
    }

    setCollapsed(id: string,  collapsed: boolean): void {
        const expanded: any = Object.assign({}, this.state.expanded);
        expanded[id] = !collapsed;
        this._state.expanded = expanded;
        this.emitStateUpdate();
    }

    highlightAnnotations(ids: string[]): void {
        this._state.highlighted = ids;
        this.emitStateUpdate();
    }

    selectTab(tab?: string): void {
        // Do nothing if the "new tab" is not a valid tab.
        if ((tab !== uiConstants.TAB_ANNOTATIONS) 
            && (tab !== uiConstants.TAB_NOTES)
            && (tab !== uiConstants.TAB_ORPHANS)) {
                return;
        }

        // Shortcut if the tab is already correct, to avoid resetting the sortKey
        // unnecessarily.
        if (this.state.selectedTab === tab) {
            return;
        }

        this._state.selectedTab = tab;
        this._state.sortKey = TAB_SORTKEY_DEFAULT[tab];
        this._state.sortKeysAvailable = TAB_SORTKEYS_AVAILABLE[tab]
        this.emitStateUpdate();
    } 

    setFilterQuery(query: string): void {
        this._state.filterQuery = query;
        this._state.forceVisible = {};
        this._state.expanded = {};
        this.emitStateUpdate();
    }

    setSortKey(key: string): void {
        this._state.sortKey = key;
        this.emitStateUpdate();
    }
    
    setFilterReseted(filterReseted: boolean): void {
        this._state.filterReseted = filterReseted;
        this.emitStateUpdate();
    }  

    deselectAnnotation(id?: string): void {
        var selection: any = Object.assign({}, this.state.selectedAnnotationMap);
        if (!selection || !id) return;
        
        delete selection[id];
        this.select(selection);
    }

    selectAnnotation(id?: string): void {
        var selection: any | undefined = Object.assign({}, this.state.selectedAnnotationMap);
        if (!selection || !id) return;
        
        selection[id] = true;
        this.select(selection);
    }

    clearFilteredAndSelectedAnnotations(): void {
        this._state.filterQuery = undefined;
        this._state.selectedAnnotationMap = undefined;
        this.emitStateUpdate();
    }

    clearSearchFilter(): void {
        this._state.filterQuery = undefined;
        this.emitStateUpdate();
    }

    deselectAllAnnotations(): void {
        this._state.selectedAnnotationMap = undefined;
        this.emitStateUpdate();
    }

    isAnnotationSelected(id: string): boolean {
        return this.state.selectedAnnotationMap?.hasOwnProperty(id) || false;
    }

    hasSelectedAnnotations(): boolean {
        return this.getSelectedAnnotationCount() > 0; 
    }

    getSelectedAnnotationCount(): number {
        if (!this.state.selectedAnnotationMap) return 0;
        return Object.keys(this.state.selectedAnnotationMap).length;
    }


    getSelectedAnnotations(): Annotation[] {
        if (!this.state.selectedAnnotationMap) return [];
        const selectedAnnotationsIds = Object.keys(this.state.selectedAnnotationMap);
        const selectedAnnotations = this.annotationStore?.getState().annotations
            .filter((annotation: Annotation) => selectedAnnotationsIds.includes(annotation.id || '')) || [];
        return selectedAnnotations.concat([]);
    }

    getSelectedNotes(): Annotation[] {
        if (!this.state.selectedAnnotationMap) return [];
        const selectedAnnotationsIds = Object.keys(this.state.selectedAnnotationMap);
        const selectedAnnotations = this.annotationStore?.getState().annotations
            .filter((annotation: Annotation) => annotationMetadata.isPageNote(annotation))
            .filter((annotation: Annotation) => selectedAnnotationsIds.includes(annotation.id || '')) || [];
        return selectedAnnotations.concat([]);
    }

    getSelectedNotesCount(): number {
        if (!this.state.selectedAnnotationMap) return 0;
        return this.getSelectedNotes().length;
    }

    getSelectedSuggestions(): Suggestion[] {
        return this.getSelectedAnnotations().filter(annotationMetadata.isSuggestion);
    }
}
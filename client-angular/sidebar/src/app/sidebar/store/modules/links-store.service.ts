import { IStoreService, StoreService } from '../store.service';

export interface LinksStoreState {
    links?: any;
}

export interface ILinksStore extends IStoreService<LinksStoreState> {
    updateLinks(newLinks: any[]): void;
}

export default class LinksStore extends StoreService<LinksStoreState> implements ILinksStore {
    constructor() {
        super({ links: undefined });
    }

    updateLinks(newLinks: any): void {
        this._state.links = newLinks;
        this.emitStateUpdate();
    }
}
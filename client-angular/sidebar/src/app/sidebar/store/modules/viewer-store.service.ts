import { IStoreService, StoreService } from '../store.service';

export interface ViewerStoreState {
    /**
     * Flag that indicates whether the app is the sidebar and connected to
     * a page where annotations are being shown in context.
     *
     * Note that this flag is not available early in the lifecycle of the
     * application.
     */
    isSidebar: boolean;
    visibleHighlights: boolean;
}

export interface IViewerStore extends IStoreService<ViewerStoreState> {
    /** Set whether the app is the sidebar */
    setAppIsSidebar(isSidebar: boolean): void;

    /**
     * Sets whether annotation highlights in connected documents are shown
     * or not.
     */    
    setShowHighlights(showHighlights: boolean): void;

    /**
     * Returns true if the app is being used as the sidebar in the annotation
     * client, as opposed to the standalone annotation page or stream views.
     */
    isSidebar(): boolean;
}

export default class ViewerStore extends StoreService<ViewerStoreState> implements IViewerStore {
    constructor() {
        super({
            isSidebar: true,
            visibleHighlights: false
        });
    }

    setAppIsSidebar(isSidebar: boolean): void {
        this._state.isSidebar = isSidebar;
        this.emitStateUpdate();
    }

    setShowHighlights(showHighlights: boolean): void {
        this._state.visibleHighlights = showHighlights;
        this.emitStateUpdate();
    }

    isSidebar(): boolean {
        return this.state.isSidebar;
    }
}
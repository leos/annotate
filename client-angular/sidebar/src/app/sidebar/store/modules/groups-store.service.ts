import { IStoreService, StoreService } from '../store.service';
import { Group } from '../../models/groups.model';

export interface GroupsStoreState {
    /**
     * List of groups.
     */
    groups: Group[];

    /**
     * ID of currently selected group.
     */
    focusedGroupId?: string,
}

export interface IGroupsStore extends IStoreService<GroupsStoreState> {
    /**
     * Set the current focused group.
     *
     * @param {string} id
     */    
    focusGroup(id: string): void;

    /**
     * Update the set of loaded groups.
     *
     * @param {Group[]} groups
     */    
    loadGroups(groups: Group[]): void;

    /**
     * Return the currently focused group.
     *
     * @return {Group|undefined}
     */
    focusedGroup(): Group | undefined;

    /**
     * Return the current focused group ID or `null`.
     *
     * @return {string|null}
     */
    focusedGroupId(): string | undefined;

    /**
     * Return the group with the given ID.
     *
     * @return {Group|undefined}
     */    
    getGroup(id: string): Group | undefined;

    /**
     * Return the list of all groups.
     *
     * @return {Group[]}
     */    
    allGroups(): Group[];
}

export default class GroupsStore extends StoreService<GroupsStoreState> implements IGroupsStore {
    constructor() {
        super({
            groups: [],
            focusedGroupId: undefined        
        })
    }

    focusGroup(id: string): void {
        const group: Group | undefined = this._state.groups.find((g: Group) => g.id === id);
        this._state.focusedGroupId = (group != undefined) ? id : undefined;
        this.emitStateUpdate();
    }

    loadGroups(groups: Group[]): void {
        let focusedGroupId: string | undefined = this.state.focusedGroupId;
        const isResetFocusedGroup: boolean = !this.state.focusedGroupId 
            || !groups.find((g: Group) => g.id === this.state.focusedGroupId);
        // Reset focused group if not in the new set of groups.
        if (isResetFocusedGroup) {
            focusedGroupId = (groups.length > 0) ? groups[0].id : undefined;
        }
        this._state.focusedGroupId = focusedGroupId;
        this._state.groups = groups;
        this.emitStateUpdate();
    }

    focusedGroup(): Group | undefined {
        return !this._state.focusedGroupId ? undefined : this.getGroup(this._state.focusedGroupId);     
    }

    focusedGroupId(): string | undefined {
        return this._state.focusedGroupId;
    }

    allGroups(): Group[] {
        return this._state.groups;
    }   

    getGroup(id: string): Group | undefined {
        return this._state.groups.find((group: Group) => group.id === id);
    }
}
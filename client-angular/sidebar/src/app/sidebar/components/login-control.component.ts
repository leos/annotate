'use strict';

import * as bridgeEvents from '../../../../../shared/bridge-events';
import { isThirdPartyUser } from '../util/account-id.util';
import { serviceConfig } from '../service-config';
import { BridgeService } from '../services/bridge.service';
import { ServiceUrlService } from '../services/service-url.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { HypothesisAuthState } from '../models/hypothesis-app.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

@Component({
  selector: 'login-control',
  templateUrl: '../templates/login-control.component.html',
  // styleUrls: ['../styles/components/login-control.scss']
})
export class LoginControlComponent {

  /**
  * Whether or not to use the new design for the control.
  *
  * FIXME: should be removed when the old design is deprecated.
  */
  @Input()
  public newStyle?: boolean;

  /**
   * An object representing the current authentication status.
   */
  @Input()
  public auth?: HypothesisAuthState;

  /**
  * Called when the user clicks on the "About this version" text.
  */
  @Output()
  public onShowHelpPanel: EventEmitter<void>;

  /**
  * Called when the user clicks on the "Log in" text.
  */
  @Output()
  public onLogin: EventEmitter<void>;

  /**
  * Called when the user clicks on the "Sign Up" text.
  */
  @Output()
  public onSignUp: EventEmitter<void>;

  /**
  * Called when the user clicks on the "Log out" text.
  */
  @Output()
  public onLogout: EventEmitter<void>;

  private settings: IHypothesisJsonConfig;

  private $window: Window;

  constructor(private bridge: BridgeService, 
    readonly serviceUrl: ServiceUrlService, 
    settingsService: HypothesisConfigurationService){
      this.onShowHelpPanel = new EventEmitter();
      this.onLogin = new EventEmitter();
      this.onSignUp = new EventEmitter();
      this.onLogout = new EventEmitter();
      this.$window = window;
      this.settings = settingsService.getConfiguration();
  }

  isThirdPartyUser(): boolean {
    return isThirdPartyUser(this.auth?.userid || '', this.settings.authDomain || '');
  };

  get shouldShowLogOutButton(): boolean {
    if (this.auth?.status !== 'logged-in') {
      return false;
    }
    var service = serviceConfig(this.settings);
    if (service && !service.onLogoutRequestProvided) {
      return false;
    }
    return true;
  };

  get shouldEnableProfileButton(): boolean {
    var service = serviceConfig(this.settings);
    return (service == null) ? true : !!service.onProfileRequestProvided;
  };

  showProfile() {
    if (this.isThirdPartyUser()) {
      this.bridge.call(bridgeEvents.PROFILE_REQUESTED);
      return;
    }
    this.$window.open(this.serviceUrl.get('user', {user: this.auth?.username}));
  };

  public onSignUpClick() {
    this.onSignUp.emit();
  }

  public onLoginClick() {
    this.onLogin.emit();
  }

  public onShowHelpPanelClick() {
    this.onShowHelpPanel.emit();
  }

  public onLogoutClick() {
    this.onLogin.emit();
  }
}
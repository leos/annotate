import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core'
import { HttpClient } from '@angular/common/http';

interface ApiUrlOption {
    method?: string;
    responseInterceptor?: Function;
}

function isEqual(obj1: any, obj2: any) {
    if (typeof obj1 != typeof obj2) return false;
    if (obj1 === obj2) return true;
    if (typeof obj1 !== 'object') return false;
    if (!obj1 || !obj2) return false;

    const obj1Props: string[] = Object.keys(obj1);
    const obj2Props: string[] = Object.keys(obj2);
    if (obj1Props.length != obj2Props.length) return false;
    for (const prop of obj1Props) {
        if(!obj2Props.includes(prop)) return false;
        if(!isEqual(obj1[prop], obj2[prop])) return false;
    }
    return true;
}

function isDuplicate(arr: any[] | undefined, item: any): boolean {
    if(arr == undefined) {
        return false;
    }

    var duplicate = false;
    for(var i=0;i<arr.length;i++){
        if ((duplicate = isEqual(arr[i], item))) {
            break;
        }
    }
    return duplicate;
};

type PredicateFuntion = (item: any, index?: number, arr?: any[]) => boolean;

/**
 * Return all items of an array that fitting the value of an item.
 * @param arr - Array to search for fitting items
 * @param item - Item to look for
 * @returns 
 */
function filterItems(arr: any[], expression: string | Object | PredicateFuntion): any[] {
    const containsString = (item: any, value: string): boolean => {
        if (typeof item !== 'object') {
            const _item: string = `${item}`;
            return _item.startsWith(value);   
        }
        const properties: string[] = Object.keys(item);
        for(const property of properties) {
            if (containsString(item[property], value)) {
                return true;
            }
        }
        return false;
    }

    return arr.filter((item: any, index: number, arr: any[]) => {
        if (typeof expression === 'function') {
            return expression(item, index, arr);
        }
        if (typeof expression === 'object') {
            return isEqual(item, expression);
        }
        return containsString(item, expression);
    });
}

/*** 
 * Re-implementation of angular-multiple-select addon
 * @see https://github.com/jagdeep-singh/angularMultipleSelect
*/
@Component({
    selector: 'multiple-select-autocomplete',
    templateUrl: '../templates/multiple-select-autocomplete.component.html',
})
export class MultipleSelectAutocomplete implements OnInit, OnChanges {
    @Input()
    public suggestionsArr?: any[];

    @Output()
    public suggestionsArrChange: EventEmitter<any[]>;

    @Input()
    public modelArr?: any[];

    @Output()
    public modelArrChange: EventEmitter<any[]>;

    @Input()
    public apiUrl?: string;

    @Input()
    public placeholder?: string;

    @Input()
    public objectProperty?: string;

    @Input()
    public apiUrlOption?: ApiUrlOption;

    @Input()
    public required?: boolean;

    @Input()
    public name?: string;

    @Input()
    public errMsgRequired?: boolean;

    @Input()
    public inputValue?: string;

    @Input()
    public closeAfterSelected?: boolean;

    @Input()
    public filterByText?: boolean;

    @Output()
    public beforeSelectItem: EventEmitter<any>;

    @Output()
    public afterSelectItem: EventEmitter<any>;
    
    @Output()
    public beforeRemoveItem: EventEmitter<any>;

    @Output()
    public afterRemoveItem: EventEmitter<any>;

    public _selectedItemIndex: number;

    private _isHover: boolean;
    private _isFocused: boolean;

    constructor(private httpClient: HttpClient) {
        this.modelArrChange = new EventEmitter();
        this.suggestionsArrChange = new EventEmitter();
        this.beforeSelectItem = new EventEmitter();
        this.afterSelectItem = new EventEmitter();
        this.beforeRemoveItem = new EventEmitter();
        this.afterRemoveItem = new EventEmitter();
        this._selectedItemIndex = 0;
        this._isHover = false;
        this._isFocused = false;        
    }

    get selectedItemIndex(): number {
        return this._selectedItemIndex; 
    }

    public ngOnInit(): void {
        this.inputValue = "";
        this.placeholder = !this.placeholder ? "" : this.placeholder;

        if(!this.suggestionsArr || this.suggestionsArr?.length === 0) {
            if(this.apiUrl != undefined && this.apiUrl != "")
                this.getSuggestionsList();
            else{
                //console.log("*****Angular-multiple-select **** ----- Please provide suggestion array list or url");
            }
        } 

        if(!this.modelArr) {
            this.modelArr = [];
        }
    }

    public ngOnChanges(changes: any) {
    }

    public get isHover(): boolean {
        return this._isHover
    }

    public get isFocused(): boolean {
        return this._isFocused;
    }

    private getSuggestionsList(): void {
        const url: string = this.apiUrl || '';
        const method: string = this.apiUrlOption?.method || 'GET';
        
        const self = this;
        this.httpClient.request(method, url).subscribe({
            next(response: any) {
                if (self.apiUrlOption?.responseInterceptor) {
                    self.apiUrlOption.responseInterceptor(response);
                }
                self.suggestionsArr = response['data'];
            },
            error(err: any) {
                //console.log("*****Angular-multiple-select **** ----- Unable to fetch list");
            }
        }); 
    }

    onFocus(): void {
        this._isFocused = true
    }

    onMouseEnter(): void {
        this._isHover = true
    }

    onMouseLeave(): void {
        this._isHover = false;
    }

    onBlur(): void {
        this._isFocused = false;
    }

    onChange($event : Event): void {
        this._selectedItemIndex = 0;
        if(this.filterByText && this.inputValue != ""){
            if(!this.suggestionsArr){
                this.suggestionsArr = [];
            }
            if(this.suggestionsArr[0] && this.suggestionsArr[0]["type"] === "TEXT" 
                && this.modelArr?.indexOf(this.suggestionsArr[0]) === -1)
                {
                    this.suggestionsArr[0] = {
                        id: this.inputValue,
                        name: this.inputValue,
                        type: "TEXT"
                    };
                }
            else {
                this.suggestionsArr = [{
                    id: this.inputValue,
                    name: this.inputValue,
                    type: "TEXT"
                }].concat(this.suggestionsArr);
            } 
            this.suggestionsArrChange.emit(this.suggestionsArr);
        }
    }

    keyParser($event: KeyboardEvent) {
        const self = this;
        const keys: any = {
            38: 'up',
            40: 'down',
            8 : 'backspace',
            13: 'enter',
            9 : 'tab',
            27: 'esc'
        };
        const key: string = keys[$event.code];
        if(key == 'backspace' && this.inputValue == ""){
            if((this.modelArr?.length || 0) != 0){
                this.removeAddedValues(this.modelArr![this.modelArr!.length-1]);
            }
            return;
        }

        if(key == 'down'){
            var filteredSuggestionArr = filterItems(this.suggestionsArr || [], this.inputValue || '');
            filteredSuggestionArr = filterItems(filteredSuggestionArr, (item: any) => self.alreadyAddedValues(item));
            if(this.selectedItemIndex < filteredSuggestionArr.length -1) {
                this._selectedItemIndex++;
            }
            return;
        }

        if(key == 'up' && this.selectedItemIndex > 0){
            this._selectedItemIndex--;
            return
        }

        if(key == 'esc') {
            this._isHover = false;
            this._isFocused = false;
            return;
        }

        if(key != 'enter') return;
        var filteredSuggestionArr = this.filteredSuggestions;
        if(this.selectedItemIndex < filteredSuggestionArr.length) {
            this.onSuggestedItemsClick(filteredSuggestionArr[this.selectedItemIndex]);
        }
    }

    onSuggestedItemsClick(selectedValue: any): void {
        this.beforeSelectItem.emit(selectedValue);
        this.modelArr?.push(selectedValue);
        this.modelArrChange.emit(this.modelArr);
        this.afterSelectItem.emit(selectedValue);

        if((this.suggestionsArr?.length || 0) == (this.modelArr?.length || 0) || this.closeAfterSelected === true){
            this._isHover = false;
        }
        this.inputValue = '';
    }

    onSearchButtonClick() : void {
        var filteredSuggestionArr = this.filteredSuggestions;
        if(this.selectedItemIndex < filteredSuggestionArr.length) {
            this.onSuggestedItemsClick(filteredSuggestionArr[this.selectedItemIndex]);
        }
    }

    alreadyAddedValues(item: any): boolean {
        return isDuplicate(this.modelArr, item);
    }

    removeAddedValues(item: any): void {
        if (this.modelArr == undefined) return;

        const itemIndex: number = this.modelArr.indexOf(item);
        if (itemIndex == -1) return;

        this.beforeRemoveItem.emit(item);
        this.modelArr.splice(itemIndex, 1);
        this.modelArrChange.emit(this.modelArr);
        this.afterRemoveItem.emit(item);

        if(item["type"] === "TEXT" && item["name"] != this.inputValue){
            if(this.suggestionsArr){
                var suggestionIndex = this.suggestionsArr.indexOf(item);
                this.suggestionsArr.splice(suggestionIndex, 1);
                this.suggestionsArrChange.emit(this.suggestionsArr);
            }
        }
    }

    mouseEnterOnItem(index: number): void {
        this._selectedItemIndex = index;
    }

    get filteredSuggestions(): any[] {
        const self = this;
        const suggestions: any[] = filterItems(this.suggestionsArr || [], this.inputValue || '');
        return filterItems(suggestions, (item: any) => !self.alreadyAddedValues(item));
    }
}
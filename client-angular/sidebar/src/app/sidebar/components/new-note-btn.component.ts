'use strict';

import * as events from '../events';
import {ScopeService} from '../services/scope.service'
import { Component } from '@angular/core';
import {HypothesisStoreService} from '../store';

@Component({
  selector: 'new-note-btn',
  templateUrl: '../templates/new-note-btn.component.html',
  // styleUrls: ['../styles/components/new-note.scss']
})
export class NewNoteBtnComponent {
  constructor(private store: HypothesisStoreService, private rootScope: ScopeService) {}

  onNewNoteBtnClick() {
    var topLevelFrame = this.store.frames.frames().find(f=>!f.id);
    var annot = {
      target: [],
      uri: topLevelFrame?.uri,
    };

    this.rootScope.$broadcast(events.BEFORE_ANNOTATION_CREATED, annot);
  };
}

'use strict';

import * as annotationMetadata from '../annotation-metadata';
import { Thread } from '../../../../../shared/models/thread.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface ChangeCollapsedEvent {
  id?: string;
  collapsed?: boolean;
}

export interface ForceVisibleEvent {
  thread?: Thread;
}

export interface AnyAnnotationEditingStateChangedEvent {
  editing: boolean;
}

function hiddenCount(thread: Thread): number {
  var isHidden = thread.annotation && !thread.visible;
  return thread.children.reduce(function (count, reply) {
    return count + hiddenCount(reply);
  }, isHidden ? 1 : 0);
}

function visibleCount(thread: Thread): number {
  const isVisible = (thread.annotation != undefined) && thread.visible;
  return thread.children.reduce(function (count: number, reply: Thread) {
    return count + visibleCount(reply);
  }, isVisible ? 1 : 0);
}

function showAllChildren(thread: Thread, showFn: (event: ForceVisibleEvent) => void) {
  thread.children.forEach((child: Thread) => {
    showFn({ thread: child });
    showAllChildren(child, showFn);
  });
}

function showAllParents(thread: Thread, showFn: (event: ForceVisibleEvent) => void) {
  var _thread: Thread | undefined = thread;
  while ((_thread.parent != undefined) && (_thread.parent.annotation != undefined)) {
    showFn({
      thread: _thread.parent
    });
    _thread = _thread.parent;
  }
}

@Component({
  selector: 'annotation-thread',
  templateUrl: '../templates/annotation-thread.component.html',
  // styleUrls: ['../styles/components/annotation-thread.scss']
})
export class AnnotationThreadComponent {
  // Flag that tracks whether the content of the annotation is hovered,
  // excluding any replies.
  public annotationHovered: boolean;
  private replyInEditingState: boolean;
  private annotationInEditingState: boolean;
  private oldEditingState: boolean;

  /** The annotation thread to render. */
  @Input()
  public thread?: Thread;

  /**
   * Specify whether document information should be shown
   * on annotation cards.
   */
  @Input()
  public showDocumentInfo?: boolean;

  /** Called when the user clicks on the expand/collapse replies toggle. */
  @Output()
  public onChangeCollapsed: EventEmitter<ChangeCollapsedEvent>;

  /**
   * Called when the user clicks the button to show this thread or
   * one of its replies.
   */
  @Output()
  public onForceVisible: EventEmitter<ForceVisibleEvent>;

  /**
   * Indicates whether any child annotation in the hierarchy is in editing
   * mode.
   */
  @Output()
  public onAnyAnnotationEditingStateChanged: EventEmitter<AnyAnnotationEditingStateChangedEvent>;

  @Input()
  public isRootSuggestion?: boolean;

  constructor() {
    this.annotationHovered = false;
    this.replyInEditingState = false;
    this.annotationInEditingState = false;
    this.oldEditingState = false;
    this.onChangeCollapsed = new EventEmitter();
    this.onForceVisible = new EventEmitter();
    this.onAnyAnnotationEditingStateChanged = new EventEmitter();
  }

  get checkIsRootSuggestion (): boolean {
    if (this.isTopLevelThread) {
      return annotationMetadata.isSuggestion(this.thread?.annotation || {});
    } else {
      return this.isRootSuggestion || false;
    }
  };

  toggleCollapsed () {
    this.onChangeCollapsed.emit({
      id: this.thread?.id,
      collapsed: !this.thread?.collapsed,      
    });
  };

  changeCollapsed(event: ChangeCollapsedEvent) {
    this.onChangeCollapsed.emit(event);
  }

  get threadClasses(): Object {
    return {
      'annotation-thread': true,
      'annotation-thread--reply': (this.thread?.depth || 0) > 0,
      'annotation-thread--top-reply': this.thread?.depth === 1,
    };
  };

  get threadToggleClasses(): Object {
    return {
      'annotation-thread__collapse-toggle': true,
      'is-open': !this.thread?.collapsed,
      'is-hovered': this.annotationHovered,
    };
  };

  get annotationClasses(): Object {
    return {
      annotation: true,
      'annotation--reply': (this.thread?.depth || 0) > 0,
      'is-collapsed': this.thread?.collapsed,
      'is-highlighted': this.thread?.highlightState === 'highlight',
      'is-dimmed': this.thread?.highlightState === 'dim',
    };
  };

  /**
   * Show this thread and any of its children
   */
  showThreadAndReplies() {
    if (!this.thread) return;

    const self = this;
    const forceVisible = (event: ForceVisibleEvent) => self.onForceVisible.emit(event);
    showAllParents(this.thread, forceVisible);
    this.onForceVisible.emit({ thread: this.thread });
    showAllChildren(this.thread, forceVisible);
  };

  forceVisible(event: ForceVisibleEvent) {
    this.onForceVisible.emit(event);
  }


  get isTopLevelThread(): boolean {
    return !this.thread?.parent;
  };

  /**
   * Return the total number of annotations in the current
   * thread which have been hidden because they do not match the current
   * search filter.
   */
  get hiddenCount(): number {
    if (!this.thread) return 0;
    return hiddenCount(this.thread);
  };

  shouldShowReply(child: Thread): boolean {
    return visibleCount(child) > 0;
  };

  evaluateEditingState() {
    const editing = this.replyInEditingState || this.annotationInEditingState;
    if (this.oldEditingState !== editing) {
      this.onAnyAnnotationEditingStateChanged.emit({ editing });
    }
    this.oldEditingState = editing;
  }

  onAnnotationEditStateChanged(event: {editing: boolean}) {
    this.annotationInEditingState = event.editing;
    this.evaluateEditingState();
  }

  onReplyEditingStateChanged(event: AnyAnnotationEditingStateChangedEvent) {
    this.replyInEditingState = event.editing;
    this.evaluateEditingState();
  }

  get isChildReplyInEditingState(): boolean {
    return this.replyInEditingState;
  }

  onMouseEnter(): void {
    this.annotationHovered = true;
  }

  onMouseLeave(): void {
    this.annotationHovered = false;
  }

  getThreadAndRepliesText(): string {
    return `View ${this.hiddenCount} more in conversation`
  }

  trackById(index: number, item: any): string {
    return item.id;
  }
}

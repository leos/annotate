'use strict';

import * as authorityChecker from '../authority-checker';
import * as OPERATION_MODES from '../../../../../shared/operationMode';
import $ from '../imports/jquery';
import {GroupsService, IGroupsService } from '../services/groups.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Group } from '../models/groups.model';
import { Component, 
  Input, 
  Output, 
  EventEmitter, 
  ElementRef,
  OnInit
} from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';


/**
 * @description Displays a combined privacy/selection post button to post
 *              a new annotation
 */
@Component({
  selector: 'publish-annotation-btn',
  templateUrl: '../templates/publish-annotation-btn.component.html',
  // styleUrls: ['../styles/components/publish-annotation-btn.scss'],
})
export class PublishAnnotationBtnComponent implements OnInit {
  @Input()
  public group?: Group;

  @Input()
  public canPost?: boolean;

  @Input()
  public isShared?: boolean;

  @Input()
  public isReply?: boolean;

  @Input()
  public isForward?: boolean;

  @Input()
  public originGroup?: string; 

  @Output()
  public onUpdateSelectedGroup: EventEmitter<{group: Group}>;

  @Output()
  public onCancel: EventEmitter<void>;

  @Output()
  public onSave: EventEmitter<void>;

  @Output()
  public onSetPrivacy: EventEmitter<{level: string}>;

  public showDropdown: boolean;

  readonly privateLabel : string;
  private groupsToLoad: IGroupsService | undefined;
  private $element: JQuery<HTMLElement>;
  readonly settings: IHypothesisJsonConfig;

  constructor(groups: GroupsService, 
    elementRef: ElementRef,
    settingsService: HypothesisConfigurationService) {
      this.settings = settingsService.getConfiguration();
      this.groupsToLoad = authorityChecker.isISC(this.settings) ? undefined : groups;    
      this.showDropdown = false;
      this.privateLabel = 'Only Me';
      this.$element = $(elementRef.nativeElement);

      this.onUpdateSelectedGroup = new EventEmitter();
      this.onCancel = new EventEmitter();
      this.onSave = new EventEmitter();
      this.onSetPrivacy = new EventEmitter();
  }

  public ngOnInit(): void {
    this.onShowDropdownChange(false);
  }

  private onShowDropdownChange(showDropdown: boolean) {
    let annotationElement: Element | null = this.$element[0].closest('annotation');
    
    if (!annotationElement) return;
    annotationElement = annotationElement.querySelector('textarea');
    annotationElement?.dispatchEvent(new CustomEvent('menuColaborators', { detail: showDropdown } ))
  }

  groupCategory(group: Group): string {
    return group.type === 'open' ? 'public' : 'group';
  };

  getAllGroups(): Group[] {
    var hasGroups = this.groupsToLoad !== undefined;
    var searchBarSelectGroup = hasGroups ? this.groupsToLoad!.focused() : undefined;
    if (!hasGroups) {
      return [];
    } else if (searchBarSelectGroup?.type === 'open') {
      return this.groupsToLoad!.all();
    } else if (searchBarSelectGroup != null) {
      return [searchBarSelectGroup];
    }
    return []
  };

  isAuthorityVisible(): boolean {
    return !authorityChecker.isISC(this.settings);
  };

  get publishDestination(): string | undefined {
    if(this.settings.operationMode === OPERATION_MODES.PRIVATE) {
      return this.group?.name;
    }
    return this.isShared ? this.group?.name : this.privateLabel;
  };

  setPrivacy(event: {level: string}) {
    this.onSetPrivacy.emit(event);
  };

  updateSelectedGroup(group?: Group) {
    if (!group) return;
    this.onUpdateSelectedGroup.emit({group});
  }

  get hasDropdown(): boolean {
    return !this.isReply && !authorityChecker.isISC(this.settings);
  }

  onSaveClick(event: Event) {
    event.stopPropagation();
    this.onSave.emit();
  }

  onCancelClick(event: Event) {
    event.stopPropagation();
    this.onCancel.emit();
  }

  get isDropdownOpen(): boolean {
    return this.showDropdown;
  }

  onToggleDropdown(event: Event, dropdown: NgbDropdown) {
    this.showDropdown = !this.showDropdown;
    this.onShowDropdownChange(this.showDropdown);
    if (this.showDropdown) {
      dropdown.open();
    } else {
      dropdown.close();
    }
  }

  onDropdownOpenChange(wasOpened: boolean) {
    if (wasOpened == this.showDropdown) return;
    this.showDropdown = wasOpened;
    this.onShowDropdownChange(this.showDropdown);
  }

  get publishBtnTitle(): string {
    return `Publish this annotation to ${this.publishDestination}`;
  }
}
'use strict';

import * as dateUtil from '../util/date.util';
import {TimeService, TimeServiceDate } from '../services/time.service';
import { Component, Input, OnDestroy, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'timestamp',
  templateUrl: '../templates/timestamp.component.html'
})
export class TimestampComponent implements OnDestroy, OnChanges {
  // A fuzzy, relative (eg. '6 days ago') format of the timestamp.
  public relativeTimestamp: string;

  // A formatted version of the timestamp (eg. 'Tue 22nd Dec 2015, 16:00')
  public absoluteTimestamp : string;

  private cancelTimestampRefresh?: VoidFunction;

  //Bindings
  @Input()
  public className?: string;

  @Input()
  public href?: string;

  @Input()
  public timestamp?: TimeServiceDate;

  constructor(private time: TimeService){
    this.relativeTimestamp = '';
    this.absoluteTimestamp = '';
    this.cancelTimestampRefresh = undefined;
  }

  updateTimestamp() {
    this.relativeTimestamp = this.time.toFuzzyString(this.timestamp || null);
    this.absoluteTimestamp = dateUtil.format(new Date(this.timestamp as string));

    if (this.timestamp) {
      if (this.cancelTimestampRefresh) {
        this.cancelTimestampRefresh();
      }
      const self = this;
      this.cancelTimestampRefresh = this.time.decayingInterval(this.timestamp, function () {
        self.updateTimestamp();
      });
    }
  }

  public ngOnChanges (changes: SimpleChanges): void {
    if (!changes['timestamp']) return;
    this.updateTimestamp();
  };

  public ngOnDestroy (): void {
    if (this.cancelTimestampRefresh) {
      this.cancelTimestampRefresh();
    }
  };
}
import { Component, Input } from '@angular/core';
import { FrameSyncService } from '../services/frame-sync.service';

@Component({
  selector: 'toggle-sidebar-button',
  templateUrl: '../templates/toggle-sidebar-button.component.html',
})
export class ToggleSidebarButtonComponent {
  @Input()
  public isSidebarOpen?: boolean;

  constructor (private frameSync: FrameSyncService) {
  }

  public onClick(): void {
    this.isSidebarOpen ? this.frameSync.hideSidebar() : this.frameSync.showSidebar();
  }
}
'use strict';

import { isThirdPartyUser } from '../util/account-id.util';
import { isThirdPartyService } from '../util/is-third-party-service.util';
import { serviceConfig } from '../service-config';
import { Memoizer } from '../util/memoize.util';
import { groupsByOrganization } from '../util/group-organizations.util';
import {AnalyticsService} from '../services/analytics.service';
import {GroupsService} from '../services/groups.service';
import {ServiceUrlService} from '../services/service-url.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Group } from '../models/groups.model';
import { HypothesisAuthState } from '../models/hypothesis-app.model';
import { Component, Input, OnInit } from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

const groupOrganizations: Memoizer<Group[], Group[]> = new Memoizer((groups: Group[]) => groupsByOrganization(groups));

@Component({
  selector: 'group-list',
  templateUrl: '../templates/group-list.component.html',
  // styleUrls: ['../styles/components/group-list.scss']
})
export class GroupListComponent implements OnInit {
  private isThirdPartyService : any;
  public thirdPartyGroupIcon : any;

  /**
   * Bindings
   */
  @Input()
  public auth? : HypothesisAuthState;

  private settings: IHypothesisJsonConfig;
  private $window: Window;

  constructor (private analytics: AnalyticsService, 
    readonly groups: GroupsService, 
    settingsService: HypothesisConfigurationService, 
    private serviceUrl: ServiceUrlService) {
      this.$window = window;
      this.settings = settingsService.getConfiguration();
  }

  public ngOnInit(): void {
    var svc = serviceConfig(this.settings);
    if (svc && svc.icon) {
      this.thirdPartyGroupIcon = svc.icon;
    }

    this.isThirdPartyService = isThirdPartyService(this.settings);
  }

  createNewGroup () {
    this.$window.open(this.serviceUrl.get('groups.new', {}), '_blank');
  };

  focusedIcon () : boolean {
    const focusedGroup = this.groups.focused();
    return focusedGroup && (
      focusedGroup.organization?.logo || this.thirdPartyGroupIcon
    );
  };

  focusedIconClass () : string {
    const focusedGroup = this.groups.focused();
    return (focusedGroup && focusedGroup.type === 'private') ? 'group' : 'public';
  };

  get isThirdPartyUser (): boolean {
    return isThirdPartyUser(this.auth?.userid || '', this.settings.authDomain || '');
  };

  leaveGroup (groupId : string) {
    var groupName : string | undefined = this.groups.get(groupId)?.name;
    var message = 'Are you sure you want to leave the group "' +
      groupName + '"?';
    if (this.$window.confirm(message)) {
      this.analytics.track(this.analytics.events.GROUP_LEAVE);
      this.groups.leave(groupId);
    }
  };

  orgName (groupId : string) : boolean {
    const group: Group | undefined = this.groups.get(groupId);
    if (!group) return false;
    return (group.organization != undefined) && (group.organization.name != undefined);
  };

  groupOrganizations (): Group[] {
    return groupOrganizations.memoize(this.groups.all())!;
  };

  viewGroupActivity () {
    this.analytics.track(this.analytics.events.GROUP_VIEW_ACTIVITY);
  };

  focusGroup (groupId : string) {
    this.analytics.track(this.analytics.events.GROUP_SWITCH);
    this.groups.focus(groupId);
  };

  /**
   * Show the share link for the group if it is not a third-party group
   * AND if the URL needed is present in the group object. We should be able
   * to simplify this once the API is adjusted only to return the link
   * when applicable.
   */
  shouldShowActivityLink (groupId : string): boolean {
    const group: Group | undefined = this.groups.get(groupId);
    if (!group) return false;
    return (group.links?.html != undefined) && !this.isThirdPartyService;
  };

  get isLoggedOut(): boolean {
    return this.auth?.status === 'logged-out';
  }

  get isLoggedIn(): boolean {
    return this.auth?.status === 'logged-in';
  }

  get isFocusedGroupPublic(): boolean | undefined {
    return this.groups.focused()?.private;
  }

  get focusedGroupName(): string | undefined {
    return this.groups.focused()?.name;
  }

  trackByGroup(index: number, item: any): any {
    return 'group-' + (item.id || index);
  }
}
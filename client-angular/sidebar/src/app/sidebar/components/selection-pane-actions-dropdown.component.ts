'use strict';

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'selection-pane-actions-dropdown',
    templateUrl: '../templates/selection-pane-actions-dropdown.component.html',
})
export class SelectionPaneActionsDropdownComponent {
    @Output()
    public accept: EventEmitter<void>;

    @Output()
    public reject: EventEmitter<void>;

    @Output()
    public delete: EventEmitter<void>;

    @Output()
    public treat: EventEmitter<void>;

    @Output()
    public reset: EventEmitter<void>;

    @Input()
    public areExtendedActionsShown?: boolean;

    @Input()
    public isAcceptAndRejectDisabled?: boolean;

    @Input()
    public isDeleteDisabled?: boolean;

    @Input()
    public isTreatDisabled?: boolean;

    @Input()
    public isResetDisabled?: boolean;

    @Input()
    public selectedAnnotationCount?: number;

    constructor() {
        this.accept = new EventEmitter();
        this.reject = new EventEmitter();
        this.delete = new EventEmitter();
        this.treat = new EventEmitter();
        this.reset = new EventEmitter();
    }

    public onAccept(): void {
        this.accept.emit();
    }

    public onReject(): void {
        this.reject.emit();
    }

    public onDelete(): void {
        this.delete.emit();
    }
    public onTreat(): void {
        this.treat.emit();
    }

    public onReset(): void {
        this.reset.emit();
    }

    public toggleDropdown(dropdown: NgbDropdown): void {
        if (dropdown.isOpen()) {
            dropdown.close();
        } else {
            dropdown.open();
        }
    }

    public get hasActions(): boolean {
        if (!this.isAcceptAndRejectDisabled) return true;
        if (!this.isDeleteDisabled) return true;
        if (!this.isTreatDisabled) return true;
        return !this.isResetDisabled;
    }
}

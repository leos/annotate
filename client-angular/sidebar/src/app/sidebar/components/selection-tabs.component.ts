'use strict';

import * as sessionUtil from '../util/session.util';
import * as uiConstants from '../ui-constants';
import {FrameSyncService} from '../services/frame-sync.service';
import {SessionService} from '../services/session.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Component, Input } from '@angular/core';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

const TAB_ANNOTATIONS = uiConstants.TAB_ANNOTATIONS;
const TAB_NOTES = uiConstants.TAB_NOTES;
const TAB_ORPHANS = uiConstants.TAB_ORPHANS;

@Component({
  selector: 'selection-tabs',
  templateUrl: '../templates/selection-tabs.component.html',
  // styleUrls: ['../styles/components/selection-tabs.scss']
})
export class SelectionTabsComponent {
  //Bindings
  @Input()
  public isLoading? : boolean;

  @Input()
  public isWaitingToAnchorAnnotations? : boolean;

  @Input()
  public selectedTab? : string;

  @Input()
  public totalAnnotations?: number;

  @Input()
  public totalNotes?: number;

  @Input()
  public totalOrphans?: number;

  readonly isThemeClean: boolean;
  readonly enableExperimentalNewNoteButton?: boolean;

  selectOrphansTab: VoidFunction;
  selectAnnotationsTab: VoidFunction;
  selectNotesTab: VoidFunction;

  constructor(private frameSync: FrameSyncService, 
    private session: SessionService, 
    settingsService: HypothesisConfigurationService, 
    private store: HypothesisStoreService) {
      const settings: IHypothesisJsonConfig = settingsService.getConfiguration();
      this.isThemeClean = (settings.theme === 'clean');
      this.enableExperimentalNewNoteButton = settings.enableExperimentalNewNoteButton;

      const self = this;
      this.selectAnnotationsTab = () => self.selectTab(TAB_ANNOTATIONS);
      this.selectNotesTab = () => self.selectTab(TAB_NOTES);
      this.selectOrphansTab = () => self.selectTab(TAB_ORPHANS);
  }

  selectTab(type: string): void {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
    this.store.selection.selectTab(type);
  };

  get showAnnotationsUnavailableMessage (): boolean {
    if (this.selectedTab !== TAB_ANNOTATIONS) return false;
    return (this.totalAnnotations === 0) ? !this.isWaitingToAnchorAnnotations : false;
  };

  get showNotesUnavailableMessage (): boolean {
    return this.selectedTab === TAB_NOTES && this.totalNotes === 0;
  };

  get showSidebarTutorial (): boolean {
    return sessionUtil.shouldShowSidebarTutorial(this.session.state);
  };

  get showNewNoteBtn(): boolean {
    return (this.selectedTab === TAB_NOTES) && this.enableExperimentalNewNoteButton || false;
  }

  get isAnnotationsTabSelected(): boolean {
    return this.selectedTab === TAB_ANNOTATIONS;
  }

  get isNotesTabSelected(): boolean {
    return this.selectedTab === TAB_NOTES;
  }

  get isOrphansTabSelected(): boolean {
    return this.selectedTab === TAB_ORPHANS;
  }
}
'use strict';

import {AnalyticsService} from '../services/analytics.service';
import { 
  Component, 
  ElementRef, 
  SimpleChanges, 
  EventEmitter, 
  Output 
} from '@angular/core';
import $ from '../imports/jquery';
import {HypothesisStoreService} from '../store';

const VIA_PREFIX = 'https://via.hypothes.is/';


@Component({
  selector: 'share-dialog',
  templateUrl: '../templates/share-dialog.component.html'
})
export class ShareDialogComponent {
  private viaInput: HTMLInputElement;
  private _viaPageLink: string;
  private $element: JQuery<HTMLElement>;

  @Output()
  public onClose: EventEmitter<void>; 

  constructor(elementRef: ElementRef, 
    private analytics: AnalyticsService, 
    private store: HypothesisStoreService) {
      this.$element = $(elementRef.nativeElement);
      this.viaInput = this.$element[0].querySelector('.js-via') as HTMLInputElement;
      this.viaInput?.focus();
      this.viaInput?.select();
      this.onClose = new EventEmitter();
      this._viaPageLink = '';
  }

  get viaPageLink(): string {
    return this._viaPageLink;
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['frames'] == undefined) return;
    this.updateViaLink(changes['frames'].currentValue);
  }

  private get frames(): any {
    return this.store.frames.frames();

  }

  urlEncode(url: string): string {
    return window.encodeURI(url);
  }

  onCloseClick() {
    this.onClose.emit();
  }

  onShareClick(target: any) {
    if (!target) return;
    this.analytics.track(this.analytics.events.DOCUMENT_SHARED, target);
  }

  updateViaLink(frames: any[]) {
    if (!frames.length) {
      this._viaPageLink = '';
      return;
    }

    // Check to see if we are on a via page. If so, we just return the URI.
    if (frames[0].uri.indexOf(VIA_PREFIX) === 0) {
      this._viaPageLink = frames[0].uri;
    } else {
      this._viaPageLink = VIA_PREFIX + frames[0].uri;
    }
  }
}
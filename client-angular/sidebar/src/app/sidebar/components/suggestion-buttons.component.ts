'use strict';

import * as OPERATION_MODES from '../../../../../shared/operationMode';

import * as authorizer from '../authorizer';
import * as events from '../events';
import { Annotation, Suggestion } from '../../../../../shared/models/annotation.model';
import {AnalyticsService} from '../services/analytics.service';
import {AnnotationMapperService} from '../services/annotation-mapper.service';
import {FlashService} from '../services/flash.service';
import {GroupsService} from '../services/groups.service';
import {  LeosPermissionsService } from '../services/permissions.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import {SuggestionMergerService} from '../services/suggestion-merger.service';
import {ScopeService} from '../services/scope.service';
import { 
  Component, 
  Input,
  Output, 
  OnInit, 
  ChangeDetectorRef,
  EventEmitter
} from '@angular/core';
import { catchError, mergeMap, from, throwError, Observable } from 'rxjs'
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

@Component({
  selector: 'suggestion-buttons',
  templateUrl: '../templates/suggestion-buttons.component.html',
  // styleUrls: ['../styles/components/suggestion.scss']
})
export class SuggestionButtonsComponent implements OnInit {
  private isAccepting : boolean;

  @Input()
  public annotation?: Annotation;

  @Input()
  public checkFeedback?: () => Observable<void>;

  @Input()
  public hideRejectButton?: boolean;

  @Output()
  public rejectAnnotation: EventEmitter<void>;

  private settings: IHypothesisJsonConfig;

  constructor(private analytics: AnalyticsService, 
    private annotationMapper: AnnotationMapperService,
    private flash: FlashService,
    private groups: GroupsService, 
    private permissions: LeosPermissionsService, 
    private store: HypothesisStoreService, 
    configurationService: HypothesisConfigurationService, 
    private suggestionMerger: SuggestionMergerService,
    private rootScope: ScopeService,
    private changeDetectorRef: ChangeDetectorRef) {
      this.settings = configurationService.getConfiguration();
      this.isAccepting = false;
      this.rejectAnnotation = new EventEmitter();
  }

  /**
   * Initialize the controller instance.
   *
   * All initialization code except for assigning the controller instance's
   * methods goes here.
   */
  public ngOnInit(): void {
    const self = this;
    this.rootScope.$on(events.LEOS_PERMISSIONS_LOADED, () => {
      self.changeDetectorRef.detach();
      self.changeDetectorRef.detectChanges();
      self.changeDetectorRef.reattach();
    });

    if (!this.store.state.operationMode && this.settings.operationMode) {
      this.store.setOperationMode(this.settings.operationMode);
    }
  }

  get showButtons (): boolean {
    if (authorizer.canMergeSuggestion(this.annotation || {}, this.permissions, this.settings)) return true;
    let originalTextHasBeenModified = authorizer.originalTextHasBeenModified(this.annotation || {});
    return originalTextHasBeenModified;
  }

  get isAcceptDisabled (): boolean {
    return this.store.state.hostState === 'OPEN'
      || this.isAccepting
      || authorizer.originalTextHasBeenModified(this.annotation || {})
      || this.isLeavingEmptyParagraph(this.annotation || {});
  }

  get isAnnotationGroupInUserGroups (): boolean {
    const userGroups = this.groups.all().map(group => group.id);
    return userGroups.includes(this.annotation?.group || '');
  }

  get isRejectDisabled (): boolean {
    return this.store.state.operationMode === OPERATION_MODES.READ_ONLY || this.store.state.operationMode === OPERATION_MODES.STORED_READ_ONLY
      || this.isAccepting;
  }

  getAcceptTitle (): string {

    if (authorizer.originalTextHasBeenModified(this.annotation || {})) {
      return 'The original text has been modified. The suggestion can not be automatically merged.';
    }

    if (this.isAcceptDisabled) {
      return 'Not possible to accept suggestion while editing';
    }

    return 'Accept suggestion';
  }

  getRejectTitle (): string {
    return this.isRejectDisabled
      ? 'Not possible to reject suggestion while editing'
      : 'Reject suggestion';
  }

  accept (): void {
    this._checkFeedback().subscribe(() => {
      this.isAccepting = true;
      if (!this.annotation?.user) {
        this.flash.info('Please log in to accept suggestions.');
        return;
      }

      const self = this;
      this.suggestionMerger.processSuggestionMerging(this.annotation).pipe(
        mergeMap((suggestion: Suggestion) => {
          return self.annotationMapper.acceptSuggestion(suggestion);
        }),
        catchError((err: any) => {
          return throwError(() => new Error('Suggestion content merging failed'));
        })
      ).subscribe({
        next() {
          self.analytics.track(self.analytics.events.ANNOTATION_DELETED);
          self.flash.success('Suggestion successfully merged');
        },
        error(err: any) {
          self.flash.error('Suggestion content merging failed');
        },
        complete() {
          self.isAccepting = false;
        }
      });
    });
  }

  onReject(): void {
    this.rejectAnnotation.emit();
  }

  private _checkFeedback(): Observable<void> {
    return !this.checkFeedback ? from(Promise.resolve()) : this.checkFeedback();
  }

  private isLeavingEmptyParagraph(annotation: Annotation) {
    return this.isEmpty(annotation?.succeedingText)
        && this.isEmpty(annotation?.precedingText)
        && this.isEmpty(annotation?.text);
  }

  private isEmpty(str: string|undefined) {
    return (!str || str.length === 0 );
  }
}

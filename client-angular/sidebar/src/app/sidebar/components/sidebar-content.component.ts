'use strict';

import SearchClient from '../search-client';
import * as events from '../events';
import { isThirdPartyService } from '../util/is-third-party-service.util';
import { Memoizer } from '../util/memoize.util';
import * as tabs from '../tabs';

import * as authorityChecker from '../authority-checker';
import * as OPERATION_MODES from '../../../../../shared/operationMode';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { HypothesisAuthState, IHypothesisSearchController } from '../models/hypothesis-app.model';
import {AnalyticsService} from '../services/analytics.service';
import {IBridgeService } from '../../../../../shared/bridge';
import {BridgeService} from '../services/bridge.service';
import {AnnotationMapperService} from '../services/annotation-mapper.service';
import {ApiService} from '../services/api.service';
import {FrameSyncService} from '../services/frame-sync.service';
import {GroupsService} from '../services/groups.service';
import {RootThreadService} from '../services/root-thread.service';
import {StreamerService} from '../services/streamer.services';
import {StreamFilterService} from '../services/stream-filter.service';
import { Thread } from '../../../../../shared/models/thread.model';
import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import {ScopeService} from '../services/scope.service';
import { 
  Component, 
  OnInit, 
  OnChanges, 
  SimpleChanges, 
  ChangeDetectorRef, 
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import $ from '../imports/jquery';
import { HypothesisStoreService, HypothesisStoreState } from '../store';
import {Observable, Subscriber} from 'rxjs';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { SessionService } from '../services/session.service';
import { authStateFromUserProfile } from '../util/converter.util';
import {User, UserProfile} from '../models/session.model';
import { DefaultFilterSearchController } from '../util/filter-search';
import {AnnotationStatus} from "../annotation-status";
import {Collaborator} from "../models/collaborator.model";

function firstKey(object: Object): string | null {
  for (var k in object) {
    if (!Object.prototype.hasOwnProperty.call(object, k)) {
      continue;
    } else if ((k === 'undefined') || (k === 'null')) {
      return null;
    }
    return k;
  }
  return null;
}

/**
 * Returns the group ID of the first annotation in `results` whose
 * ID is a key in `selection`.
 */
function groupIDFromSelection(selection: Object, results: Annotation[]): string | null {
  var id = firstKey(selection);
  var annot: Annotation | undefined = results.find(function (annot: Annotation) {
    return annot.id === id;
  });
  if (!annot) {
    return null;
  }
  return annot.group || null;
}

interface QueryJson {
  uri: string[];
  group?: string;
  connectedEntity?: string;
  metadatasets?: string;
  mode?: string;
}

@Component({
  selector: 'sidebar-content',
  templateUrl: '../templates/sidebar-content.component.html'
})
export class SidebarContentComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {

  public auth?: HypothesisAuthState;
  public search?: IHypothesisSearchController;

  private _totalNotes!: number;
  private _totalAnnotations!: number;
  private _totalOrphans!: number;
  private _storedAnnots: Annotation[] = [];
  private _feedbackAnnots: Annotation[] = [];
  public isWaitingToAnchorAnnotations: boolean;

  public selectedTab?: string;
  private searchClients: SearchClient[];
  private previousGroupId?: string;

  private settings: IHypothesisConfiguration;

  public rootThread?: Thread;
  public isFilterReseted: boolean;

  private _visibleCount: Memoizer<Thread,number>;

  constructor(private changeDetectorRef: ChangeDetectorRef,
    private analytics: AnalyticsService, 
    private session: SessionService,
    private bridge: BridgeService, 
    private store: HypothesisStoreService, 
    private annotationMapper: AnnotationMapperService, 
    private api: ApiService, 
    private frameSync: FrameSyncService,
    private groups: GroupsService,
    private rootScope: ScopeService, 
    private rootThreadService: RootThreadService, 
    settingsService: HypothesisConfigurationService, 
    private streamer: StreamerService,
    private streamFilter: StreamFilterService) {
      this.rootThread = undefined;
      this.searchClients = [];
      this.settings = settingsService.getConfiguration();
      this.isFilterReseted = store.state.filterReseted;
      this.isWaitingToAnchorAnnotations = true;

     const self = this;
      var unsubscribeAnnotationUI: VoidFunction = this.store.subscribe((state: HypothesisStoreState) => {
        self.rootThread = self._thread(state);
        self.isFilterReseted = state.filterReseted;
        self.selectedTab = state.selectedTab;
    
        const includeProcessedSentAnnotations = (`${self.settings.showStatusFilter}` !== "true") &&
            authorityChecker.isISC(self.settings) || !authorityChecker.isISC(self.settings);
        var counts = tabs.counts(state.annotations, includeProcessedSentAnnotations);
        
        self._totalNotes = counts.notes;
        self._totalAnnotations = counts.annotations;
        self._totalOrphans = counts.orphans;
        self.isWaitingToAnchorAnnotations = (counts.anchoring > 0);
      });

      this.search = new DefaultFilterSearchController(store);

      // Reload the view when the user switches accounts
      this.rootScope.$on(events.USER_CHANGED, function (event: any, data: any) {
        self.auth = authStateFromUserProfile(data.profile);
      });

      rootScope.$on('$destroy', () => unsubscribeAnnotationUI());

      this._visibleCount = new Memoizer((thread: Thread): number => {
        return thread.children.reduce(function (count, child) {
          return count + self.visibleCount(child);
        }, thread.visible ? 1 : 0);
      });
  }

  public ngOnDestroy(): void {
      
  }

  public ngOnInit() {
    this.previousGroupId = undefined;

    const self = this;
    this.streamer.connect().subscribe(() => {});
    this.session.load().subscribe((profile: UserProfile) => {
      const auth = authStateFromUserProfile(profile);
      // If the user is logged in, we connect nevertheless
      if (auth.status === 'logged-in') {
        this.streamer.connect().subscribe(() => {});
      }
      self.auth = auth;
    });

    this.rootScope.$on('sidebarOpened', function () {
      self.analytics.track(self.analytics.events.SIDEBAR_OPENED);
      self.streamer.connect().subscribe(() => {});
  
      //LEOS Change - make default loaded group as Collaborators
      if(!authorityChecker.isISC(self.settings) &&
      self.groups.focused() && self.groups.focused()?.id !== self.groups.defaultGroupId()) {
        self.groups.focus(self.groups.defaultGroupId());
      }
    });

    this.rootScope.$on("LEOS_requestFilteredAnnotations", function () {
      if (!self.rootThread) return;
      var replies = self.extractAnnotationReplies(self.rootThread);
      var annotations = self.extractAnnotationChildren(self.rootThread);
  
      var filteredAnnotations = {
        "rows": annotations,
        "total": self.rootThread.children.length,
        "replies": replies
      };
      self.bridge.call('LEOS_responseFilteredAnnotations', JSON.stringify(filteredAnnotations));
    });
  
    this.rootScope.$on(events.USER_CHANGED, function () {
      self.streamer.reconnect().subscribe(() => {});
    });
  
    this.rootScope.$on(events.ANNOTATIONS_SYNCED, function (event: any, tags: string[]) {
      // When a direct-linked annotation is successfully anchored in the page,
      // focus and scroll to it
      var selectedAnnot = self.firstSelectedAnnotation();
      if (!selectedAnnot) {
        return;
      }
      var matchesSelection = tags.some(function (tag) {
        return tag === selectedAnnot?.$tag;
      });
      if (!matchesSelection) {
        return;
      }
      self.focusAnnotation(selectedAnnot);
      self.scrollToAnnotation(selectedAnnot);
  
      self.store.selection.selectTab(tabs.tabForAnnotation(selectedAnnot));
    });

    this.rootScope.$on(events.ANNOTATION_CREATED, function (ev: string, annot: Annotation) {
      if (annot && (self.settings.operationMode === OPERATION_MODES.STORED || self.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY)) {
        self._feedbackAnnots.push(annot);
        self.sendFeedbackIds();
      }
    });

    this.rootScope.$on(events.ANNOTATION_DELETED, function (ev: string, annot: Annotation) {
      if (annot && annot.references && annot.status && annot.status.status
          && (self.settings.operationMode === OPERATION_MODES.STORED || self.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY)) {
        annot.status.status = AnnotationStatus.DELETED;
        self.sendFeedbackIds();
      }
    });

    this.rootScope.$on('reloadAnnotations', function () {
      self.store.selection.deselectAllAnnotations();
      self.frameSync.deselectAllAnnotations();
      self.loadAnnotations();
    });

    this.rootScope.$on('refreshAnnotations', function () {
      self.store.selection.deselectAllAnnotations();
      self.loadAnnotations();
    });

    const threadList: Element | null = document.querySelector('thread-list');
    if (threadList != null) {
      $(threadList).on('scroll', () => {
        this.bridge.call('LEOS_refreshAnnotationLinkLines');
      });
    }
  };

  ngAfterViewInit(): void {
    this.loadAnnotations();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['totalAnnotations'] != undefined) {
      if (changes['totalAnnotations'].currentValue > 0) return;
      if (this.search?.filterReseted()) return;
      this.rootScope.$broadcast(events.LEOS_RESET_FILTER);
    }
    
    // Re-fetch annotations when focused group, logged-in user or connected frames
    // change.
    const prevGroupId: string | undefined = (changes['focusedGroupId'] != undefined) ? changes['focusedGroupId'].previousValue : this.previousGroupId;
    if (prevGroupId !== this.focusedGroupId) {
        // The focused group may be changed during loading annotations as a result
        // of switching to the group containing a direct-linked annotation.
        //
        // In that case, we don't want to trigger reloading annotations again.
        if (this.isLoading) {
          return;
        }
        this.store.selection.clearFilteredAndSelectedAnnotations();
        this.frameSync.deselectAllAnnotations();  
    }
    this.previousGroupId = prevGroupId;
    this.loadAnnotations();
  }

  get focusedGroupId(): string | undefined {
    return this.groups.focused()?.id;
  }

  get profileUserId(): string | undefined {
    return this.store.session.profile().userid
  }

  get searchUris(): string[] {
    return this.store.frames.searchUris();
  }

  get totalNotes(): number {
    return this._totalNotes;
  }

  get totalAnnotations(): number {
    return this._totalAnnotations;
  }

  get totalOrphans(): number {
    return this._totalOrphans;
  }

  get isLoading(): boolean {
    if (!this.store.frames.frames().some((frame) => frame.uri)) {
      // The document's URL isn't known so the document must still be loading.
      return true;
    }
    if (this.searchClients.length > 0) {
      // We're still waiting for annotation search results from the API.
      return true;
    }
    return false;
  }

  thread(): Thread {
    return this.rootThreadService.thread(this.store.getState());
  }

  _thread(state: HypothesisStoreState): Thread {
    return this.rootThreadService.thread(state);
  }

  focusAnnotation(annotation: Annotation) {
    var highlights: string[] = [];
    if (annotation) {
      highlights = (annotation.$tag == undefined) ? [] : [annotation.$tag];
    }
    this.frameSync.focusAnnotations(highlights);
  }

  onScrollTo(event: { annotation: Annotation }) {
    this.scrollToAnnotation(event.annotation);
  }

  scrollToAnnotation(annotation: Annotation) {
    if (!annotation || annotation.$tag == undefined) {
      return;
    }
    this.frameSync.scrollToAnnotation(annotation.$tag);
    this.frameSync.LEOS_selectAnnotation(annotation); //LEOS Change
  }

  /**
   * Returns the Annotation object for the first annotation in the
   * selected annotation set. Note that 'first' refers to the order
   * of annotations passed to store when selecting annotations,
   * not the order in which they appear in the document.
   */
  firstSelectedAnnotation(): Annotation | undefined {
    if (this.store.getState().selectedAnnotationMap) {
      var id: string = Object.keys(this.store.getState().selectedAnnotationMap || {})[0];
      return this.store.getState().annotations.find(function (annot) {
        return annot.id === id;
      });
    } else {
      return undefined;
    }
  }

  private _resetAnnotations() {
    this.annotationMapper.unloadAnnotations(this.store.annotation.savedAnnotations(), true);
  }

  private _sortReplies(dbAnnots: Annotation[]) {
    return dbAnnots.filter((a) => {
      if (a.references && a.references.length > 0) {
        const annot = dbAnnots.find((mainAnnot) => a.references && a.references.length > 0 && mainAnnot.id === a.references[0]);
        return annot && annot.uri === a.uri;
      }
      return true;
    });
  }

  private _loadAnnotationsFor(uris: string[], group?: string) {
    var searchClient = new SearchClient(this.api.search, {
      // If no group is specified, we are fetching annotations from
      // all groups in order to find out which group contains the selected
      // annotation, therefore we need to load all chunks before processing
      // the results
      incremental: group != undefined,
    });
    this.searchClients.push(searchClient);
    const self = this;
    searchClient.on('results', function (results: Annotation[]) {
      if (self.store.selection.hasSelectedAnnotations()) {
        // Focus the group containing the selected annotation and filter
        // annotations to those from this group
        var groupID = groupIDFromSelection(
          self.store.getState().selectedAnnotationMap || {}, results);
        if (!groupID) {
          // If the selected annotation is not available, fall back to
          // loading annotations for the currently focused group
          groupID = self.groups.focused()!.id;
        }
        results = results.filter(function (result) {
          return result.group === groupID;
        });
        self.groups.focus(groupID);
      }

      if (results.length) {
        self._feedbackAnnots = results;
        results = self._sortReplies(results);
        self.annotationMapper.loadAnnotations(results);
      }
    });
    searchClient.on('end', function () {
      // Remove client from list of active search clients.
      //
      // $evalAsync is required here because search results are emitted
      // asynchronously. A better solution would be that the loading state is
      // tracked as part of the app state.
      self.rootScope.$apply(self.changeDetectorRef, () => {
        self.searchClients.splice(self.searchClients.indexOf(searchClient), 1);
      });

      self.store.frames.frames().forEach(function (frame: any) {
        if (0 <= uris.indexOf(frame.uri)) {
          self.store.frames.updateFrameAnnotationFetchStatus(frame.uri, true);
        }
      });
    });

    // LEOS Change
    var queryJson: QueryJson = {
      uri: uris, 
      group: group, 
      connectedEntity: undefined, 
      metadatasets: undefined, 
      mode: undefined
    };
    if(this.settings.connectedEntity) {
      queryJson.connectedEntity = this.settings.connectedEntity;
    }
    //set operation mode normal/contribution
    if(this.settings.operationMode === OPERATION_MODES.PRIVATE) {
      queryJson.mode = "private"
    } else if(this.settings.operationMode === OPERATION_MODES.NORMAL) {
      queryJson.mode = "normal"
    }
    this.requestSearchMetadata().subscribe({
      next([metadatasets]) {
        if (metadatasets !== null) {
          const leosMetadata = JSON.parse(metadatasets);
          queryJson.metadatasets = JSON.stringify(leosMetadata);
          searchClient.get(queryJson);
        }
        else {
          searchClient.get(queryJson);
        }
      },
      error(err: string) {
        console.debug(err);
        searchClient.get(queryJson);
      }
    });
  }

  /**
   * Load annotations for all URLs associated with `frames`.
   */
  loadAnnotations() {
    this._resetAnnotations();

    this.searchClients.forEach(function (client) {
      client.cancel();
    });

    if (this.settings.temporaryData) {
        this.loadTemporaryAnnotations();
        return;
    }
    this._storedAnnots = [];
    this._feedbackAnnots = [];
    this.loadPersistentAnnotations();
    this.loadStoredContributionAnnotations();
  }

  loadStoredContributionAnnotations() {
    const self = this;
    if (self.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY || self.settings.operationMode === OPERATION_MODES.STORED) {
      let loadStoredContributionAnnotationsPromise = new Promise(function (resolve, reject) {
        var promiseTimeout = setTimeout(() => resolve(''), 60000);
        const uri = self.store.frames.frames().length > 0 ? self.store.frames.frames()[0].uri : '';
        self.bridge.call('requestStoredDocumentAnnotations', uri, function (error: any, result: unknown) {
          clearTimeout(promiseTimeout);
          if (error) {
            return reject(error);
          } else {
            return resolve(result);
          }
        })
      });
      loadStoredContributionAnnotationsPromise.then((annotations: any) => {
        let annotList: Annotation[] = [];
        let replies: Annotation[] = [];
        if (annotations && annotations !== '' && annotations.length > 0 && annotations[0].rows) {
          annotList = annotations[0].rows;
          replies = annotations[0].replies;
          self.annotationMapper.loadAnnotations(annotList, replies);
          self._storedAnnots = annotList;
          if (replies) {
            replies.forEach((reply) => self._storedAnnots?.push(reply));
          }
          let results = self._sortReplies(self._storedAnnots);
          self.annotationMapper.loadAnnotations(results);
          self.sendFeedbackIds();
        }
      });
    }
  }

  loadTemporaryAnnotations() {
    const self = this;
    if (this.settings.temporaryData?.id.length === 0) {
        return;
    }
    if (this.settings.temporaryData?.document.length === 0) {
        return;
    }
    var searchClient = new SearchClient(this.api.searchTemporary, undefined);
    this.searchClients.push(searchClient);
    searchClient.on('results', function (results: any) {
      if (results.length) {
        self.annotationMapper.loadAnnotations(results);
      }
    });
    searchClient.on('end', () => {});
    searchClient.getTemporary(this.settings.temporaryData?.id || '', this.settings.temporaryData?.document || '');
  }

  loadPersistentAnnotations() {
    if (this.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY || this.settings.operationMode === OPERATION_MODES.STORED) {
      return;
    }
    // If there is no selection, load annotations only for the focused group.
    //
    // If there is a selection, we load annotations for all groups, find out
    // which group the first selected annotation is in and then filter the
    // results on the client by that group.
    //
    // In the common case where the total number of annotations on
    // a page that are visible to the user is not greater than
    // the batch size, this saves an extra roundtrip to the server
    // to fetch the selected annotation in order to determine which group
    // it is in before fetching the remaining annotations.
    var group = this.store.selection.hasSelectedAnnotations() ?
      null : this.groups.focused()!.id;

    var searchUris = this.store.frames.searchUris();
    if (searchUris.length > 0) {
      this._loadAnnotationsFor(searchUris, group || undefined);

      this.streamFilter.resetFilter().addClause('/uri', 'one_of', searchUris, false);
      this.streamer.setConfig('filter', {filter: this.streamFilter.getFilter()});
    }
  }

  sendFeedbackIds() {
    const uri = this.store.frames.frames().length > 0 ? this.store.frames.frames()[0].uri : '';
    let feedbackIds : string[] = []
    if (uri && uri.includes('revision-')) {
      if (this._storedAnnots.length > 0) {
        this._storedAnnots.forEach((sAnnot) => {
          const sId = sAnnot.id;
          const sStatus = sAnnot.status?.status;
          if (sAnnot.feedbackToBeSent && sId && (!sStatus || sStatus === 'NORMAL')) {
            feedbackIds.push(sId);
          }
        });
      }
      if (this._feedbackAnnots.length > 0) {
        this._storedAnnots.forEach((sAnnot) => {
          const sId = sAnnot.id;
          const sStatus = sAnnot.status?.status;
          if (sId && sAnnot.references && (!sStatus || sStatus === 'NORMAL')) {
            let stored = false;
            if (!sAnnot.feedbackToBeSent) {
              stored = true;
            }
            if (!stored) {
              feedbackIds.push(sId);
            }
          }
        })
        this._feedbackAnnots.forEach((fAnnot) => {
          const fId = fAnnot.id;
          const fStatus = fAnnot.status?.status;
          if (fId && (!fStatus || fStatus === 'NORMAL')) {
            let stored = false;
            this._storedAnnots?.forEach((sAnnot => {
              if (sAnnot.id && fId === sAnnot.id) {
                stored = true;
              }
            }));
            if (!stored) {
              feedbackIds.push(fId);
            }
          }
        })
      }
      this.bridge.call('requestCountSentFeedbacks', feedbackIds);
    }
  }

  extractAnnotationChildren(thread: Thread): Annotation[] {
    var annotations: Annotation[] = [];
    if (thread.totalChildren > 0) {
      for (let child of thread.children) {
        if (child.annotation != undefined) {
          annotations.push(child.annotation);
        }
      }
    }
    return annotations;
  }

  extractReplies(thread: Thread): Annotation[] {
    var replies: Annotation[] = [];
    if ((thread?.replyCount || 0) > 0) {
      for (let child of thread.children) {
        if (child.annotation != undefined) {
          replies.push(child.annotation);
        }
        Array.prototype.push.apply(replies, this.extractReplies(child));
      }
    }
    return replies;
  }

  extractAnnotationReplies(thread: Thread): Annotation[] {
    var replies: Annotation[] = [];
    if (thread.totalChildren > 0) {
      for (let child of thread.children) {
        Array.prototype.push.apply(replies, this.extractReplies(child));
      }
    }
    return replies;
  }

  setCollapsed(event: {id: string, collapsed: boolean}) {
    this.store.selection.setCollapsed(event.id, event.collapsed);
  };

  forceVisible (event: {thread: Thread}) {
    const thread = event.thread;
    this.store.selection.setForceVisible(thread.id || '', true);
    if (thread.parent) {
      this.store.selection.setCollapsed(thread.parent.id || '', false);
    }
  };

  //LEOS Change
  get selectionPaneAvailable (): boolean {
    if (this.settings.operationMode === OPERATION_MODES.READ_ONLY || this.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY) return false;
    return this.visibleCount() > 0;
  };

  get selectedAnnotationUnavailable (): boolean {
    var selectedID = firstKey(this.store.getState().selectedAnnotationMap || {});
    return !this.isLoading &&
           !!selectedID &&
           !this.store.annotation.annotationExists(selectedID);
  };

  get shouldShowLoggedOutMessage(): boolean {
    // If user is not logged out, don't show CTA.
    if (this.auth?.status !== 'logged-out') {
      return false;
    }

    // If user has not landed on a direct linked annotation
    // don't show the CTA.
    if (!this.settings.annotations) {
      return false;
    }

    // The CTA text and links are only applicable when using Hypothesis
    // accounts.
    if (isThirdPartyService(this.settings)) {
      return false;
    }

    // The user is logged out and has landed on a direct linked
    // annotation. If there is an annotation selection and that
    // selection is available to the user, show the CTA.
    var selectedID = firstKey(this.store.getState().selectedAnnotationMap || {});
    return !this.isLoading &&
           !!selectedID &&
           this.store.annotation.annotationExists(selectedID);
  }

  visibleCount (thread?: Thread): number {
    return (thread ? this._visibleCount.memoize(thread) : this._visibleCount.memoize(this.thread())) || 0;
  }

  // LEOS Change
  requestSearchMetadata(): Observable<any[]> {
    const bride: IBridgeService = this.bridge;
    return new Observable((subscriber: Subscriber<any[]>) => {
      var isTimeout = false;
      var promiseTimeout = window.setTimeout(() => {
        subscriber.error('Search metadata request timeout');
        subscriber.complete();
        isTimeout = true;
      }, 1000);

      bride.call('requestSearchMetadata', (error: any, result: any) => {
        clearTimeout(promiseTimeout);
        if (isTimeout) return;

        if (error) {
          subscriber.error(error);
        } else {
          subscriber.next(result);
        }
        subscriber.complete();
      });
    });
  }

  requestSearchUsers(userId: string): Observable<User[]> {
    const bride: IBridgeService = this.bridge;
    return new Observable((subscriber: Subscriber<User[]>) => {
      var isTimeout = false;
      var promiseTimeout = window.setTimeout(() => {
        subscriber.error('Search users request timeout');
        subscriber.complete();
        isTimeout = true;
      }, 1000);

      bride.call('requestSearchUsers', userId, (error: any, result: User[]) => {
        clearTimeout(promiseTimeout);
        if (isTimeout) return;

        if (error) {
          subscriber.error(error);
        } else {
          subscriber.next(result);
        }
        subscriber.complete();
      });
    });
  }

  requestListCollaborators(): Observable<Collaborator[]> {
    const bride: IBridgeService = this.bridge;
    return new Observable((subscriber: Subscriber<Collaborator[]>) => {
      var isTimeout = false;
      var promiseTimeout = window.setTimeout(() => {
        subscriber.error('List collaborators request timeout');
        subscriber.complete();
        isTimeout = true;
      }, 1000);

      bride.call('requestListCollaborators', (error: any, result: Collaborator[]) => {
        clearTimeout(promiseTimeout);
        if (isTimeout) return;

        if (error) {
          subscriber.error(error);
        } else {
          subscriber.next(result);
        }
        subscriber.complete();
      });
    });
  }

  get isUserLoggedIn(): boolean {
    return this.auth?.status === 'logged-in';
  }

  get isUserLoggedOut(): boolean {
    return this.auth?.status === 'logged-out';
  }

  login(): void {

  }
}
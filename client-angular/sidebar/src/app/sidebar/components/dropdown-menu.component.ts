import * as authorityChecker from '../authority-checker';
import { Group } from '../models/groups.model';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ElementRef } from '@angular/core';
import $ from '../imports/jquery';

@Component({
    selector: 'dropdown-menu',
    templateUrl: '../templates/dropdown-menu.component.html'
})
export class DropdownMenuComponent implements OnInit, OnDestroy {
    @Input()
    public groups?: Group[];
    
    @Input()
    public privateLabel?: string;
    
    @Input()
    public settings?: IHypothesisJsonConfig;
    
    @Input()
    public group?: Group;

    @Input()
    public isforward?: boolean;
    
    @Input()
    public originGroup?: string;

    @Output()
    public onUpdateSelectedGroup: EventEmitter<Group>;

    @Output()
    public onSetPrivacy: EventEmitter<{level: string}>;

    private domElement: HTMLElement;
    private threadList: HTMLElement | null;

    private $element: JQuery<HTMLElement>;

    constructor(elementRef: ElementRef<HTMLElement>){
        this.$element = $(elementRef.nativeElement)
        this.domElement = this.$element[0];
        this.threadList = this.domElement.closest('thread-list');
        this.onUpdateSelectedGroup = new EventEmitter();
        this.onSetPrivacy = new EventEmitter();
    }

    public ngOnInit () {
        this.computeLayout();
        if (this.threadList) {
            this.threadList.addEventListener('scroll', this.computeLayout);
        }
    }

    public ngOnDestroy () {
        if (this.threadList) {
            this.threadList.removeEventListener('scroll', this.computeLayout);
        }
    }

    get isAuthorityVisible (): boolean {
        var isVisible = true;
        if (authorityChecker.isISC(this.settings!)) {
            isVisible = false;
        }
        return isVisible;
    }

    setPrivacy (level: string) {
        this.onSetPrivacy.emit({ level });
    }

    groupCategory (group: Group): string {
        return group.type === 'open' ? 'public' : 'group';
    }

    updateSelectedGroup (group: Group) {
        this.onUpdateSelectedGroup.emit(group);
    }
 
    /**
     * Determines whether the popup should appear above or below the button.
     * In addition the appropriate height is computed in order to prevent the popup from stretching beyond the
     * visible area.
     */
    computeLayout () {
        const menuContainer: HTMLElement | null = this.domElement.querySelector('.publish-annotation-btn__dropdown-container');
        if (!menuContainer) return;

        const anchorElement: HTMLElement | null = menuContainer.closest('.publish-annotation-btn__dropdown-anchor');
        if (!anchorElement) return;

        const threadList: HTMLElement | null = menuContainer.closest('thread-list');
        if (!threadList) {
            return;
        }

        const anchorRect = anchorElement.getBoundingClientRect();
        const threadListRect = threadList.getBoundingClientRect();

        const spaceToTop = anchorRect.top - threadListRect.top;
        const spaceToBottom = threadListRect.bottom - anchorRect.bottom;


        if (spaceToBottom > spaceToTop) {
            this.applyLayoutBelowButton(menuContainer, spaceToBottom);
        } else {
            this.applyLayoutAboveButton(menuContainer, spaceToTop);
        }

    };

    /**
     * Applies the changes to the element in order to display it above the anchor.
     * @param {HTMLElement} menuElement 
     * @param {number} space
     */
    applyLayoutAboveButton (menuElement: HTMLElement, space: number) {
        const menuScrollContainer = menuElement.querySelector('.publish-annotation-btn__dropdown-scrollcontainer');
        const publishButton = menuElement.closest('.publish-annotation-btn');

        if (!publishButton) return;
        const buttonRect = publishButton.getBoundingClientRect();

        let height = space;
        height -= buttonRect.height; // account for the publish button over which the menu is being moved
        height -= 7; // account for arrow size
        height -= 30; // add some margin

        const style = `
            max-height: ${height}px;
        `;

        const menuStyle = `
            ${style}
            bottom: ${buttonRect.height + 7}px;
        `;

        if (!menuScrollContainer) return;
        menuScrollContainer.setAttribute('style', style);
        menuElement.setAttribute('style', menuStyle);

        if (!menuElement || !menuElement.parentElement) return;
        const arrow = menuElement.parentElement.querySelector('.publish-annotation-btn__dropdown-arrow');

        if (!arrow) return;
        arrow.setAttribute('style', `bottom: ${buttonRect.height + 7 + 1}px`)
        arrow.classList.remove('arrow-at-top');
        arrow.classList.add('arrow-at-bottom');
    };

    /**
     * Applies the changes to the element in order to display it below the anchor.
     * @param {HTMLElement} menuElement 
     * @param {number} space
     */
    applyLayoutBelowButton (menuElement: HTMLElement, space: number) {

        const menuScrollContainer = menuElement.querySelector('.publish-annotation-btn__dropdown-scrollcontainer');

        let height = space;
        height -= 7; // account for arrow size
        height -= 30; // add some margin

        const style = `
            max-height: ${height}px;
        `;

        if (!menuScrollContainer) return;
        menuScrollContainer.setAttribute('style', style);
        menuElement.setAttribute('style', style);

        if (!menuElement || !menuElement.parentElement) return;
        const arrow = menuElement.parentElement.querySelector('.publish-annotation-btn__dropdown-arrow');

        if (!arrow) return;
        arrow.setAttribute('style', `top: ${7 + 1}px`)
        arrow.classList.remove('arrow-at-bottom');
        arrow.classList.add('arrow-at-top');
    };

    trackByGroup(index: number, item: any): any {
        return `group-${item?.id || index}`;
    }
}

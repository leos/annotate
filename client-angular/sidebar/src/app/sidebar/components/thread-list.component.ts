'use strict';

import * as events from '../events';
import * as metadata from '../annotation-metadata';

import { Thread } from '../../../../../shared/models/thread.model';
//import scopeTimeout from '../util/scope-timeout.util';
import {FrameSyncService} from '../services/frame-sync.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import VirtualThreadList, { VirtualThreadListState } from '../virtual-thread-list';
import { Annotation } from '../../../../../shared/models/annotation.model';
import {ScopeService} from '../services/scope.service';
import {
  Component, 
  Input, 
  Output, 
  OnInit, 
  OnDestroy, 
  OnChanges, 
  EventEmitter,
  SimpleChanges,
  ElementRef
} from '@angular/core';
import $ from '../imports/jquery';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { ChangeCollapsedEvent, ForceVisibleEvent } from './annotation-thread.component';

/**
 * Returns the height of the thread for an annotation if it exists in the view
 * or `null` otherwise.
 */
function getThreadHeight(id: string): number | null {
  const threadElement: HTMLElement | null = window.document.getElementById(id);
  if (!threadElement) {
    return null;
  }

  // Note: `getComputedStyle` may return `null` in Firefox if the iframe is
  // hidden. See https://bugzilla.mozilla.org/show_bug.cgi?id=548397
  var style = window.getComputedStyle(threadElement);
  if (!style) {
    return null;
  }

  // Get the height of the element inside the border-box, excluding
  // top and bottom margins.
  var elementHeight = threadElement.getBoundingClientRect().height;

  // Get the bottom margin of the element. style.margin{Side} will return
  // values of the form 'Npx', from which we extract 'N'.
  var marginHeight = parseFloat(style.marginTop) +
                     parseFloat(style.marginBottom);

  return elementHeight + marginHeight;
}

var virtualThreadOptions = {
  // identify the thread types that need to be rendered
  // but not actually visible to the user
  invisibleThreadFilter: function(thread: Thread){
    // new highlights should always get rendered so we don't
    // miss saving them via the render-save process
    return thread.annotation?.$highlight && metadata.isNew(thread.annotation);
  },
};

interface AnnotationObject {
  annotation?: Annotation;
}

interface ThreadListState {
  offscreenLowerHeight: string;
  offscreenUpperHeight: string;
  visibleThreads: Thread[];
  invisibleThreads: Thread[];  
}

/**
 * Component which displays a virtualized list of annotation threads.
 */
@Component({
  selector: 'thread-list',
  templateUrl: '../templates/thread-list.component.html',
  // styleUrls: ['../styles/components/thread-list.scss']
})
export class ThreadListComponent implements OnInit, OnDestroy, OnChanges {
  /** `scrollRoot` is the `Element` to scroll when scrolling a given thread into view. */
  private scrollRoot: HTMLElement; 
  readonly isThemeClean : boolean;

  /** 
   * `visibleThreads` keeps track of the subset of all threads matching the
   * current filters which are in or near the viewport and the view then renders
   * only those threads, using placeholders above and below the visible threads
   * to reserve space for threads which are not actually rendered. 
   */
  private visibleThreads: VirtualThreadList;
  private _virtualThreadList: ThreadListState;
  private onChangedTimeout?: number;
  private scrollIntoViewTimeout?: number;
  private $element: JQuery<HTMLElement>;

  /** The root thread to be displayed by the thread list. */
  @Input()
  public thread?: Thread;

  @Input()
  public showDocumentInfo?: boolean;

  /**
   * Called when the user clicks a link to show an annotation that does not
   * match the current filter.
   */
  @Output()
  public onForceVisible: EventEmitter<{thread: Thread}>;

  /** Called when the user focuses an annotation by hovering it. */
  @Output()
  public onFocus: EventEmitter<{annotation: Annotation}>;

  /** Called when a user toggles the expansion state of an annotation thread. */
  @Output()
  public onChangeCollapsed: EventEmitter<{id: string, collapsed: boolean}>;

  @Output()
  public onSelect: EventEmitter<{annotation: Annotation}>;

  private settings: IHypothesisJsonConfig;

  private $window: Window;

  constructor(
    elementRef: ElementRef, 
    configurationService: HypothesisConfigurationService, 
    private frameSync: FrameSyncService, 
    private store: HypothesisStoreService,
    private rootScope: ScopeService) {
      this.onFocus = new EventEmitter();
      this.onForceVisible = new EventEmitter();
      this.onChangeCollapsed = new EventEmitter();
      this.onSelect = new EventEmitter();

      this.$window = window;
      this.settings = configurationService.getConfiguration();
      this.$element = $(elementRef.nativeElement);
      this.scrollRoot = this.$element[0];
      this.isThemeClean = this.settings.theme === 'clean';
      this.onChangedTimeout = undefined;
      this.scrollIntoViewTimeout = undefined;

      const options = Object.assign({
        scrollRoot: this.scrollRoot,
      }, virtualThreadOptions);

      this.visibleThreads = new VirtualThreadList(this.$window, this.thread, options);

      this._virtualThreadList = {
        offscreenLowerHeight: '0px',
        offscreenUpperHeight: '0px',
        visibleThreads: [],
        invisibleThreads: []
      };
  }

  get virtualThreadList(): ThreadListState {
    return this._virtualThreadList;
  }

  isSelected(annotation?: Annotation): boolean {
    const _annotation: Annotation = annotation || { id: '' };
    return this.store.selection.isAnnotationSelected(_annotation.id!);
  }

  isHovered(annotation?: Annotation): boolean {
    return !!annotation?.hovered;
  }
  
  isProcessed(annotation?: Annotation): boolean {
    const _annotation: Annotation = annotation || {};
    return metadata.isProcessed(_annotation);
  }

  public ngOnInit(): void {
    const self = this;
    this.visibleThreads.on('changed', function (state: VirtualThreadListState) {
      self._virtualThreadList = {
        visibleThreads: state.visibleThreads,
        invisibleThreads: state.invisibleThreads,
        offscreenUpperHeight: `${state.offscreenUpperHeight}px`,
        offscreenLowerHeight: `${state.offscreenLowerHeight}px`,
      };
      self.onChangedTimeout = self.$window.setTimeout(() => {
        state.visibleThreads.forEach(function (thread: Thread) {
          const height = getThreadHeight(thread.id || '');
          if (!height) {
            return;
          }
          self.visibleThreads.setThreadHeight(thread.id || '', height);
        });
      }, 50);
    });

    this.rootScope.$on(events.BEFORE_ANNOTATION_CREATED, function (event: Event, annotation: Annotation) {
      if (annotation.$highlight || metadata.isReply(annotation)) {
        return;
      }
      self.scrollIntoView(annotation.$tag || '');
    });

    this.rootScope.$on(events.SCROLL_ANNOTATION_INTO_VIEW, (event: Event, tag: string) => {
      const annotation = self.store.annotation.findAnnotationByTag(tag);
      if (!annotation || annotation.$highlight || metadata.isReply(annotation)) {
        return;
      }
      self.scrollIntoView(annotation.id || '');
    });
  }

  /**
   * Return the vertical scroll offset for the document in order to position the
   * annotation thread with a given `id` or $tag at the top-left corner
   * of the view.
   */
  scrollOffset(id: string): number {
    var maxYOffset = this.scrollRoot.scrollHeight - this.scrollRoot.clientHeight;
    return Math.min(maxYOffset, this.visibleThreads.yOffsetOf(id));
  }

  /** Scroll the annotation with a given ID or $tag into view. */
  scrollIntoView(id: string): void {
    const self = this;
    var estimatedYOffset = this.scrollOffset(id);
    this.scrollRoot.scrollTop = estimatedYOffset;

    // As a result of scrolling the sidebar, the target scroll offset for
    // annotation `id` might have changed as a result of:
    //
    // 1. Heights of some cards above `id` changing from an initial estimate to
    //    an actual measured height after the card is rendered.
    // 2. The height of the document changing as a result of any cards heights'
    //    changing. This may affect the scroll offset if the original target
    //    was near to the bottom of the list.
    //
    // So we wait briefly after the view is scrolled then check whether the
    // estimated Y offset changed and if so, trigger scrolling again.
    this.scrollIntoViewTimeout = window.setTimeout(() => {
      var newYOffset = self.scrollOffset(id);
      if (newYOffset !== estimatedYOffset) {
        self.scrollIntoView(id);
      }
    }, 200);
  }

  public ngOnChanges (changes: SimpleChanges): void {
    if (changes['thread'] == undefined) return;
    this.visibleThreads.setRootThread(changes['thread'].currentValue);
  }

  public ngOnDestroy(): void {
    if (this.scrollIntoViewTimeout) window.clearTimeout(this.scrollIntoViewTimeout);
    if (this.onChangedTimeout) window.clearTimeout(this.onChangedTimeout);
    this.visibleThreads.detach();
  }

  /** Called when a user selects an annotation. */
  onSelectClick(event: Event, annotation?: Annotation | null) {
    event.preventDefault();
    event.stopPropagation();
    if (!annotation) return;

    this.store.selection.toggleSelectedAnnotations([annotation.id || '']);
    this.frameSync.scrollToAnnotation(annotation.$tag || '');
    this.frameSync.LEOS_selectAnnotation(annotation);
    this.onSelect.emit({annotation: annotation!});
  }

  _onFocus(annotation?: Annotation | null) {
    if (!annotation) return;
    this.onFocus.emit({annotation});
  }

  _onChangeCollapsed(event: ChangeCollapsedEvent ) {
    this.onChangeCollapsed.emit({id: event.id || '', collapsed: event.collapsed || false })
  }

  _onForceVisible(event: ForceVisibleEvent) {
    if (!event.thread) return;
    this.onForceVisible.emit({thread: event.thread});
  }

  trackByItemId(index: number, item: any): any {
    return item.id;
  }
}
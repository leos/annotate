import { Component, Input } from '@angular/core';

@Component({
  selector: 'sidebar-header',
  templateUrl: '../templates/sidebar-header.component.html',
})
export class SidebarHeaderComponent {
  
  @Input()
  public isSidebarOpen?: boolean;

  constructor () {}
}
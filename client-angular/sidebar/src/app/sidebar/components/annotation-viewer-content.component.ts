'use strict';

import {ApiService, IApiService } from '../services/api.service';
import {RootThreadService} from '../services/root-thread.service';
import {StreamerService} from '../services/streamer.services';
import {StreamFilterService} from '../services/stream-filter.service';
import {AnnotationMapperService} from '../services/annotation-mapper.service';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { Thread } from '../../../../../shared/models/thread.model';
import {HypothesisStoreService} from '../store';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable, mergeMap, zip, of } from 'rxjs';

/**
 * Fetch all annotations in the same thread as `id`.
 *
 * @return Observable<Array<Annotation>>
 */
function fetchThread(api: IApiService, id: string): Observable<Annotation[]> {
  return api.annotation.get({id: id}).pipe(
    mergeMap((annot: Annotation) => {
      if (annot.references != undefined && annot.references.length > 0) {
        return api.annotation.get({id: annot.references[0]}) as Observable<Annotation>;
      }
      return of(annot);
    }),
    mergeMap((annot: Annotation) => {
      return  zip(of(annot), api.search({references: annot.id}));
    }),
    mergeMap(([annot, searchResult]) => {
      let annotations = [annot] as Annotation[];
      return of(annotations.concat(searchResult.rows));
    })
  );
}

type AnnotationViewerSearch = {
  update: (query: string) => void;
}

@Component({
  selector: 'annotation-viewer',
  templateUrl: '../templates/annotation-viewer-content.component.html'
})
export class AnnotationViewerContentComponent implements OnInit {
  public rootThread?: Thread;
  public ready: Observable<void>;

  @Input()
  public search?: AnnotationViewerSearch;

  constructor(changeDetectorRef: ChangeDetectorRef,
    private $location: Location, 
    $routeParams: ActivatedRoute, 
    private store: HypothesisStoreService, 
    api: ApiService, 
    rootThreadService: RootThreadService, 
    streamer: StreamerService,
    streamFilter: StreamFilterService, 
    annotationMapper: AnnotationMapperService) {
      store.viewer.setAppIsSidebar(false);

      const self = this;
      store.subscribe(() => {
        self.rootThread = rootThreadService.thread(store.getState());
      });

      const _id = $routeParams.snapshot.paramMap.get('id') || '';
      this.ready = fetchThread(api, _id).pipe(
        mergeMap((annots: Annotation[]) => {
          annotationMapper.loadAnnotations(annots);
    
          var topLevelAnnot = annots.filter(function (annot: Annotation) {
            return Promise.resolve();
          })[0];
      
          if (!topLevelAnnot) {
            return Promise.resolve();
          }
      
          streamFilter
            .setMatchPolicyIncludeAny()
            .addClause('/references', 'one_of', topLevelAnnot.id, true)
            .addClause('/id', 'equals', topLevelAnnot.id, true);
          streamer.setConfig('filter', { filter: streamFilter.getFilter() });
          streamer.connect().subscribe(() => {});
      
          annots.forEach(function (annot: Annotation) {
            store.selection.setCollapsed(annot?.id || '', false);
          });
      
          if (topLevelAnnot.id !== _id) {
            store.selection.highlightAnnotations([_id]);
          }
          return Promise.resolve();          
        })
      );   
  }

  public ngOnInit(): void {
    const self = this;
    this.search = {
      update: function (query: string) {
        self.$location.go('/stream', `q=${query}`);
      }
    };
  }

  setCollapsed (event: {id: string, collapsed: boolean}) {
    this.store.selection.setCollapsed(event.id, event.collapsed);
  };
}
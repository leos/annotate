import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import * as FEEDBACK_STATUS from '../../../../../shared/feedback-status';
import * as OPERATION_MODES from '../../../../../shared/operationMode';

import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import { Feedback } from '../../../../../shared/models/feedback.model';
import { Observable } from 'rxjs';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

@Component({
    selector: 'annotation-feedback',
    templateUrl: '../templates/annotation-feedback.component.html',
    // styleUrls: ['../styles/leos-annotation-feedback.scss']
})
export class AnnotationFeedbackComponent implements OnInit {
    @Input()
    public feedback?: Feedback;

    @Input()
    public isIsc?: boolean;

    @Input()
    public isAddingFeedback?: boolean;

    @Input()
    public onSaveFeedback?: (obj: { feedbackText: string }) => Observable<void>;

    @Output()
    public onEditFeedback: EventEmitter<{ unsavedFeedback: string }>;

    @Output()
    public stopAddingFeedback: EventEmitter<void>;


    private feedbackCollapsedState : boolean;
    private feedbackText?: string | null;
    private isSaving : boolean;
    private settings : IHypothesisConfiguration;

    constructor(settingsService: HypothesisConfigurationService){
        this.onEditFeedback = new EventEmitter();
        this.stopAddingFeedback = new EventEmitter();

        this.isSaving = false;
        this.feedbackCollapsedState = true;
        this.settings = settingsService.getConfiguration();
    }


    public ngOnInit() {
        this.feedbackText = (this.feedback != null) ? this.feedback.text : null;
    }

    get feedbackLabel() : string {
        return this.isFeedbackTextEmpty() ? 'Add feedback' : 'Feedback';
    }

    get feedbackEditedBy(): string {
        let editor: string | null = this._getFeedbackEditor();
        if (!editor) return '';
        if (editor.length > 15) {
            editor = `${editor.substring(0,15)}...`;
        }

        let date = new Date(this._getFeedbackUpdated());
        let lastEdited = date.toLocaleString("en-GB", { dateStyle: "short", timeStyle: "short" });
        return `Last edited by ${editor} (${lastEdited})`;
    }

    private _getFeedbackEditor (): string | null {
        if(!this.feedback) return null;
        if(!this.feedback.updatedByDisplayName) return null;
        return this.feedback.updatedByDisplayName;
    }

    private _getFeedbackUpdated (): string {
        if(!this.feedback) return '';
        return this.feedback.updatedAt || '';
    }

    getFeedbackText (): string | undefined {
        return this.feedbackText || undefined;
    }

    hasUnsavedChanges (): boolean {
        if (!this.feedback || !this.feedback.text) {
            return (this.feedbackText != null) ? (this.feedbackText.length > 0) : false;
        }
        return (this.feedback.text != this.feedbackText);
    }

    get isEditorReadOnly (): boolean {
        if (this.isProcessed()) return true;
        return this.settings.operationMode === OPERATION_MODES.READ_ONLY || this.settings.operationMode === OPERATION_MODES.STORED_READ_ONLY;
    }

    get isFeedbackCollapsed (): boolean {
        if (this.isAddingFeedback) {
            this.feedbackCollapsedState = false;
        }
        return this.feedbackCollapsedState;
    }

    isFeedbackHasEditor (): boolean {
        if (!this.feedback) return false;
        if (!this.feedback.updatedByUser) return false;
        return true;
    }

    get isHideSave (): boolean {
        return this.isEditorReadOnly;
    }

    isProcessed (): boolean {
        if (!this.feedback) return false;
        return this.isStatusSent();
    }

    isStatusSent (): boolean {
        if (!this.feedback) return false;
        return (this.feedback.status === FEEDBACK_STATUS.SENT);
    }

    get isSaveDisabled (): boolean {
        return this.isSaving || !this.hasUnsavedChanges();
    }

    get isShowFeedbackIcon(): boolean {
        if (this.isFeedbackTextEmpty()) return false;
        if (this.isStatusSent()) return false;
        return !this.isIsc;
    }

    get isShowFeedbackMetadata (): boolean {
        if(this.isIsc) return false;
        return this.isFeedbackHasEditor();
    }

    isFeedbackTextEmpty (): boolean {
        if (!this.feedbackText) return true;
        return this.feedbackText.length == 0;
    }

    save(event: Event) {
        event.stopPropagation();
        if (!this.onSaveFeedback) return;
        const self = this;
        this.isSaving = true;
        this.onSaveFeedback({feedbackText: this.feedbackText || ''}).subscribe(() => {
            self.isSaving = false;
            self.stopAddingFeedback.emit();
        });
    }

    setFeedbackText (event: {text?: string}) {
        this.feedbackText = event.text;
        this.onEditFeedback.emit({unsavedFeedback: event.text || ''});
    };

    toggleFeedbackCollapsed(event: Event) {
        event.stopPropagation();
        this.feedbackCollapsedState = !this.feedbackCollapsedState;
        if(this.isAddingFeedback) {
            this.stopAddingFeedback.emit()
        }
    };

    setExitData(obj: any) {};
}
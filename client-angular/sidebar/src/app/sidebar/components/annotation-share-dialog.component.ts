'use strict';

import { Component, 
  Input,
  ChangeDetectorRef, 
  ElementRef, 
  SimpleChanges, 
  OnInit, 
  OnChanges, 
  OnDestroy, 
  EventEmitter
} from '@angular/core';
import {AnalyticsService} from '../services/analytics.service';
import { Group } from '../models/groups.model';

@Component({
  selector: 'annotation-share-dialog',
  templateUrl: '../templates/annotation-share-dialog.component.html',
  // styleUrls: ['../styles/components/annotation-share-dialog.scss']
})
export class AnnotationShareDialogComponent implements OnInit, OnChanges, OnDestroy {
  private _copyToClipboardMessage?: string;

  private shareLinkInput?: HTMLInputElement;

  private timeout?: number;

  @Input()
  public group?: Group;
  
  @Input()
  public uri?: string;
  
  @Input()
  public isPrivate?: boolean;
  
  @Input()
  public isOpen?: boolean;
  
  @Input()
  public onClose: EventEmitter<void>;



  constructor(private elementRef: ElementRef<Element>, 
    private changeDetectorRef: ChangeDetectorRef,
    private analytics: AnalyticsService) {
      this._copyToClipboardMessage = undefined;
      elementRef.nativeElement.querySelector
      this.onClose = new EventEmitter();
  }

  public ngOnInit(): void {
    this.shareLinkInput = $(this.elementRef.nativeElement).find('input')[0] as HTMLInputElement;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['isOpen'] == undefined) return;

    const isOpen = changes['isOpen'].currentValue as boolean;
    if (!isOpen) return;

    const self = this;
    this.timeout = window.setTimeout(() => {
      self.shareLinkInput?.focus();
      self.shareLinkInput?.select();
    });
  }

  public ngOnDestroy() {
    clearTimeout(this.timeout);
  }

  public _onClose() {
    this.onClose.emit();
  }
 
  copyToClipboard (event: any) {
    const $container: JQuery<Element> =event.target.parent();
    const shareLinkInput: HTMLInputElement = $container.find('input')[0] as HTMLInputElement;

    try {
      shareLinkInput.select();

      // In some browsers, execCommand() returns false if it fails,
      // in others, it may throw an exception instead.
      if (!document.execCommand('copy')) {
        throw new Error('Copying link failed');
      }

      this._copyToClipboardMessage = 'Link copied to clipboard!';
    } catch (ex) {
      this._copyToClipboardMessage = 'Select and copy to share.';
    } finally {
      const self = this;
      self.changeDetectorRef.detach();
      setTimeout(function () {
        self._copyToClipboardMessage = undefined;
        self.changeDetectorRef.detectChanges();
        self.changeDetectorRef.reattach();
      }, 1000);
    }
  };

  onShareClick (target?: string) {
    if(target){
      this.analytics.track(this.analytics.events.ANNOTATION_SHARED, target);
    }
  };

  get copyToClipboardMessage(): string | undefined {
    return this._copyToClipboardMessage;
  }

  getRouterLink(): string {
    return `https://twitter.com/intent/tweet?url=${encodeURI(this.uri || '')}&hashtags=annotated`
  }

  getMailtoRouterLink(): string {
    return `mailto:?subject=Let's%20Annotate&amp;body=${this.uri || ''}`;
  }
}
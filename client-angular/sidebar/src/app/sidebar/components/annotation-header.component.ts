'use strict';

import * as annotationMetadata from '../annotation-metadata';
import * as authorizer from '../authorizer';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Memoizer } from '../util/memoize.util';
import { isThirdPartyUser, username } from '../util/account-id.util';
import {AnalyticsService} from '../services/analytics.service';
import {AnnotationMapperService} from '../services/annotation-mapper.service';
import {FlashService} from '../services/flash.service';
import {FeaturesService} from '../../sidebar/services/features.service';
import {GroupsService} from '../../sidebar/services/groups.service';
import { IHypothesisConfiguration } from '../../../../../shared/models/config.model';
import {ServiceUrlService} from '../../sidebar/services/service-url.service';
import {LeosPermissionsService} from '../services/permissions.service';
import { Annotation, AnnotationUserInfo } from '../../../../../shared/models/annotation.model';
import { Group } from '../models/groups.model';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { DraftState } from '../services/drafts.service';
import { HypothesisStoreService } from '../store';
import { FrameSyncService } from '../services/frame-sync.service';
import { Observable, from } from 'rxjs';

/**
 * Header component for an annotation card.
 *
 * Header which displays the username, last update timestamp and other key
 * metadata about an annotation.
 */
@Component({
  selector: 'annotation-header',
  templateUrl: '../templates/annotation-header.component.html'
})
export class AnnotationHeaderComponent {
  @Input()
  public annotation?: Annotation;

  @Input()
  public isPrivate?: boolean;

  @Input()
  public isEditing?: boolean;

  @Input()
  public isHighlight?: boolean;

  @Input()
  public isOrphan?: boolean;

  @Input()
  public isProcessed?: boolean;

  @Input()
  public isForward?: boolean;

  @Input()
  public isSuggestion?: boolean;

  @Output()
  public onReplyCountClick: EventEmitter<void>;

  @Input()
  public replyCount?: number;

  @Input()
  public showDocumentInfo?: boolean;

  @Output()
  public edit: EventEmitter<void>;

  @Output()
  public delete: EventEmitter<void>;
  
  @Input()
  public forward?: boolean;

  @Output()
  public reply: EventEmitter<void>;

  @Input()
  public isDeleted?: boolean;

  @Input()
  public isSaving?: boolean;

  @Input()
  public id?: string;

  @Input()
  public state?: DraftState;

  @Input()
  public isReply?: boolean;

  @Input()
  public checkFeedback?: () => Observable<void>;

  @Output()
  public treatAnnotation: EventEmitter<void>;

  @Output()
  public forwardAnnotation: EventEmitter<void>;

  @Output()
  public addFeedback: EventEmitter<void>;

  private settings: IHypothesisConfiguration;
  private _documentMeta: Memoizer<Annotation, annotationMetadata.DomainAndTitle>;

  public onCheckUnsavedFeedbackAndWarn: () => Observable<void>;

  constructor (private analytics: AnalyticsService,
    private annotationMapper: AnnotationMapperService, 
    private features: FeaturesService,
    private flash: FlashService,
    private frameSync: FrameSyncService,
    private groups: GroupsService,
    private permissions: LeosPermissionsService, 
    settingsService: HypothesisConfigurationService,
    public serviceUrl: ServiceUrlService,
    private store: HypothesisStoreService) {
      this.edit = new EventEmitter();
      this.delete = new EventEmitter();
      this.reply = new EventEmitter();
      this.onReplyCountClick = new EventEmitter();
      this.treatAnnotation = new EventEmitter();
      this.forwardAnnotation = new EventEmitter();
      this.addFeedback = new EventEmitter();
      this.settings = settingsService.getConfiguration();
      this._documentMeta = new Memoizer((annotation: Annotation) => annotationMetadata.domainAndTitle(annotation));

      const self = this;
      this.onCheckUnsavedFeedbackAndWarn = () => self._checkUnsavedFeedbackAndWarn();
  }

  get user(): string {
    if (!this.annotation) return '';
    return this.annotation.user || '';
  };

  get displayName(): string {
    const annotation: Annotation = this.annotation || { user_info: undefined, user: undefined };
    const userInfo: AnnotationUserInfo | undefined = annotation.user_info;
    const isThirdPartyUser_ = isThirdPartyUser(this.user, this.settings.authDomain || '');
    if (this.features.flagEnabled('client_display_names') || isThirdPartyUser_) {
      // userInfo is undefined if the api_render_user_info feature flag is off.
      if (userInfo) {
        // display_name is null if the user doesn't have a display name.
        if (userInfo.display_name) {
          return userInfo.display_name;
        }
      }
    }
    return username(annotation.user);
  };

  get entityName(): string {
    var userInfo: any = this.annotation ? this.annotation.user_info : null;
    var dn:string='';
    var isThirdPartyUser_ = isThirdPartyUser(this.annotation?.user, this.settings.authDomain || '');
    if (this.features.flagEnabled('client_display_names') || isThirdPartyUser_) {
        // userInfo is undefined if the api_render_user_info feature flag is off.
        if (userInfo) {
          // entity_name is null if the user doesn't have a entity associated.
          if (!!userInfo.entities && annotationMetadata.isISC(this.annotation) && !annotationMetadata.isSent(this.annotation || {}))
            userInfo.entities.forEach((val:any,ind:number)=>{
              if (val.name.indexOf(userInfo.entity_name) == 0)
                dn = val.name;
            });
          // entity_name is null if the user doesn't have a entity associated. and (06.12.22) if no sector found in previous
          if ((dn == '') && userInfo.entity_name){
            dn = userInfo.entity_name;
          }
        }
    }
    return dn;
  };

  get isThirdPartyUser(): boolean {
    if (!this.annotation) return false;
    return isThirdPartyUser(this.annotation.user, this.settings.authDomain || '');
  };

  get isSuggestionActionsShown (): boolean {
    if (this.isEditing) return false;
    if (!this.isSuggestion) return false;
    if (this.isProcessed) return false;
    if (this.isOrphan) return false;
    return !this.isForward;
  }

  get isSelectionCheckboxShown() : boolean {
    if(!this.annotation?.id) return false;
    if(this.isEditing) return false;
    if(this.isSaving) return false;
    if(this.isReply) return false;
    return true;
  }

  get isAnnotationSelected() : boolean {
    if(!this.annotation || !this.annotation.id) return false;
    return this.store.selection.isAnnotationSelected(this.annotation.id);
  }

  get thirdPartyUsernameLink(): string | null {
    if(!this.annotation) return null;
    return this.settings.usernameUrl ?
      this.settings.usernameUrl + username(this.annotation.user):
      null;
  };

  group (): Group | null  {
    const group: string = this.annotation ? (this.annotation.group || '') : '';
    return this.groups.get(group) || null;
  };

  
  documentMeta (): any {
    return this._documentMeta.memoize(this.annotation || {});
  };

  get updated(): string | number {
    if (!this.annotation) return '';
    return this.annotation.updated || '';
  };

  get htmlLink(): string {
    if (!this.annotation) return '';
    if (this.annotation['links'] && this.annotation['links']['html']) {
      return this.annotation['links']['html'] || '';
    }
    return '';
  };

  getRepliesText(): string {
    return `${this.replyCount} replies`;
  }

  replyCountClick() {
    this.onReplyCountClick.emit();
  }

  private _checkUnsavedFeedbackAndWarn(): Observable<void> {
    return !this.checkFeedback ? from(Promise.resolve()) : this.checkFeedback();
  }

  public onRejectAnnotation(): void {
    const self = this;
    setTimeout(() => {
      self._checkUnsavedFeedbackAndWarn().subscribe(() => {
        const msg = 'Are you sure you want to reject this suggestion?';
        if (window.confirm(msg)) {
          self.annotationMapper.rejectSuggestion(self.annotation || {}).subscribe({
            next() {
              self.analytics.track(self.analytics.events.ANNOTATION_DELETED)
            },
            error(err: any) {
              self.flash.error(err.message);
            }
          })
        }
      })
    });    
  }

  get enableSuggestionButtons (): boolean {
    if (this.annotation?.$leosDeleted) return false;
    if (this.annotation?.$selectorMoved) return false;
    if (authorizer.canMergeSuggestion(this.annotation || {}, this.permissions, this.settings)) return true;
    let originalTextHasBeenModified = authorizer.originalTextHasBeenModified(this.annotation || {});
    return originalTextHasBeenModified;
  }

  public onDelete(): void {
    this.delete.emit();
  }

  public onReply(): void {
    this.reply.emit();
  }

  public onEdit(): void {
    this.edit.emit();
  }
  public onForwardAnnotation(): void {
    this.forwardAnnotation.emit();
  }

  public onAddFeedback(): void {
    this.addFeedback.emit();
  }

  public onTreatAnnotation(): void {
    this.treatAnnotation.emit();
  }
}
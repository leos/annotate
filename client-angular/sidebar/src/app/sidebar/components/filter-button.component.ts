'use strict';

import {ScopeService} from '../services/scope.service';
import {Component} from '@angular/core';

@Component({
  selector: 'filter-button',
  templateUrl: '../templates/filter-button.component.html'
})
export class FilterButtonComponent {
  constructor (private rootScope: ScopeService) {
  }

  onSearchButtonClick() {
    this.rootScope.$broadcast('filterPane:toggleVisibility');
  }
}

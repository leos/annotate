'use strict';

//import { diff_match_patch } from 'diff-match-patch';
import * as imp_diff_match_patch from 'diff-match-patch'
const diff_match_patch = (imp_diff_match_patch as any).default.diff_match_patch;
import 'diff-match-patch-line-and-word';

import * as annotationMetadata from '../annotation-metadata';
import * as authorityChecker from '../authority-checker';
import * as authorizer from '../authorizer';
import * as events from '../events';
import { isThirdPartyUser } from '../util/account-id.util';
import * as OPERATION_MODES from '../../../../../shared/operationMode';
import * as ORIGIN_MODES from '../../../../../shared/originMode';
import $ from '../imports/jquery';

/**
 * Inject imports for services and values
 */
import {AnalyticsService} from '../services/analytics.service';
import { IPermissionsService, LeosPermissionsService } from '../services/permissions.service';
import {AnnotationMapperService} from '../services/annotation-mapper.service';
import {ApiService} from '../services/api.service';
import {DraftsService, DraftState } from '../services/drafts.service';
import {FlashService} from '../services/flash.service';
import {GroupsService} from '../services/groups.service';
import {ScopeService} from '../services/scope.service';
import {ServiceUrlService} from '../services/service-url.service';
import {SessionService} from '../services/session.service';
import {StreamerService} from '../services/streamer.services';
import {FeaturesService} from '../services/features.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { Group } from '../models/groups.model';
import { profileUserInfoToAnnotationUserInfo } from '../util/converter.util';
import { 
  Component, 
  Input,
  Output, 
  OnInit,
  ElementRef,
  EventEmitter,
  ChangeDetectorRef
} from '@angular/core';
import { 
  Observable, 
  map, 
  catchError, 
  tap,
  from,
  of,
  mergeMap,
} from 'rxjs';
import { Feedback } from '../../../../../shared/models/feedback.model';
import {HypothesisStoreService} from '../store';
import { EditorExitData } from '../models/markdown.model';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

type SaveFeedbackEvent = { feedbackText?: string }

interface MetadataToDisplay {
  [key: string]: any;
}

interface Metadata {
  key: string;
  value: any;
}

type MainingFunction = (obj: EditorExitData) => boolean;

class EditorsMap extends Map<string, EditorExitData> {
  private _maining?: MainingFunction;

  constructor(){
    super();
  }

  get maining(): MainingFunction | undefined {
    return this._maining;
  }

  set maining(newMaining: MainingFunction) {
    this._maining = newMaining;
  }
}

/**
* Return a copy of `annotation` with changes made in the editor applied.
*/
function unescapeText(text: string): string {
    let unescapedText = "";
    const innerNodes = new DOMParser().parseFromString(text, "text/html").querySelectorAll("body")[0].childNodes;
    for (let i = 0; i < innerNodes.length; i++) {
      const e: any = innerNodes[i];
      if (e.nodeType == Node.TEXT_NODE) {
        unescapedText += e.textContent;
      } else if (e.nodeType == Node.ELEMENT_NODE) {
        unescapedText += e["outerHTML"].replace(e["innerHTML"], unescapeText(e["innerHTML"]));
      }
    }
    return unescapedText;
}

function sanitizeTextBeforeSave(text: string): string {
    let sanitizedText = unescapeText(text);
    sanitizedText = sanitizeSpellcheckerContent(sanitizedText);
    return sanitizedText;
}

function sanitizeSpellcheckerContent(content: string): string {
  const match = content.match(/<p[^>]*>(.*?)<\/p>/);
  if (match) {
    return match[1];
  }
  return content;
}

// Private helper exposed for use in unit tests.
export function updateModel(annotation: Annotation, changes: DraftState, permissions: IPermissionsService) {
  var userid = annotation.user || '';
  var updatedText = sanitizeTextBeforeSave(changes.text || '');

  return Object.assign({}, annotation, {
    // Apply changes from the draft
    group: changes.group,
    tags: changes.tags,
    text: updatedText,
    justification: changes.justification,
    forwardJustification : changes.forwardJustification,
    permissions: changes.isPrivate ?
      permissions.private(userid) : permissions.shared(userid, annotation.group || ''),
  });
}

@Component({
  selector: 'annotation',
  templateUrl: '../templates/annotation.component.html',
  /* styleUrls: [
    '../styles/components/annotation.scss',
    '../styles/components/compare.scss',
    '../styles/leos-annotation.scss',
    '../styles/leos-compare.scss',
  ] */
})
export class AnnotationComponent implements OnInit {
  /**
   * Determines whether controls to expand/collapse the annotation body
   * are displayed adjacent to the tags field.
   */
  private canCollapseBody : boolean;

  /** Determines whether the annotation body should be collapsed. */
  private _collapseBody : boolean;

  /** True if the annotation is currently being saved. */
  private _isSaving :boolean;
  /** True if the 'Share' dialog for this annotation is currently open. */
  public showShareDialog : boolean;

  public editorsArray: EditorsMap;

  private metadataToDisplay: MetadataToDisplay;
  private newlyCreatedByHighlightButton?: any;
  public originalAnnotationUser?: string;
  private unsavedFeedback? : string;
  public markAsTreated : boolean;
  private initialText?: string;
  private justificationCollapsedState: boolean;
  private oldEditingState: boolean;
  public noBlur: boolean;
  public setExitData!: (obj : any) => void;

  @Input()
  public annotation!: Annotation;

  @Input()
  public showDocumentInfo?: boolean;

  @Input()
  public replyCount?: number;

  @Input()
  public isCollapsed?: boolean;

  @Input()
  public isRootSuggestion?: boolean;

  /** Warning message **/
  @Input()
  public editedExit?: boolean;

  @Input()
  public isChildReplyInEditingState?: boolean;

  @Output()
  public onReplyCountClick: EventEmitter<void>;

  @Output()
  public onEditStateChanged: EventEmitter<{editing: boolean}>;

  public isAddingFeedbackState: boolean;

  public saveFeedback: (event: {feedbackText?: string}) => Observable<void>;
  public onCheckFeedback: () => Observable<void>;

  private $element: JQuery<HTMLElement>;
  private settings: IHypothesisJsonConfig;
  private $window: Window;

  constructor (elementRef: ElementRef,
    private changeDetectorRef: ChangeDetectorRef,
    private analytics: AnalyticsService,
    private store: HypothesisStoreService, private annotationMapper: AnnotationMapperService,
    private api: ApiService, private drafts: DraftsService,
    private flash: FlashService, private groups: GroupsService,
    private permissions: LeosPermissionsService, private serviceUrl: ServiceUrlService,
    private session: SessionService, settingsService: HypothesisConfigurationService,
    private streamer: StreamerService, private features: FeaturesService,
    private rootScope: ScopeService) {
      this.$window = window;
      this.$element = $(elementRef.nativeElement);
      this.metadataToDisplay = {};
      this.originalAnnotationUser = undefined;
      this.unsavedFeedback = undefined;
      this.markAsTreated = false;
      this.justificationCollapsedState = true;
      this.noBlur = false;
      this.oldEditingState = false;
      this.settings = settingsService.getConfiguration();
      this.onReplyCountClick = new EventEmitter();
      this.onEditStateChanged = new EventEmitter();

      /** Determines whether controls to expand/collapse the annotation body
       * are displayed adjacent to the tags field.
       */
      this.canCollapseBody = false;

      /** Determines whether the annotation body should be collapsed. */
      this._collapseBody = true;

      /** True if the annotation is currently being saved. */
      this._isSaving = false;

      /** True if the 'Share' dialog for this annotation is currently open. */
      this.showShareDialog = false;

      /** Warning message **/
      this.editedExit = false;

      this.isAddingFeedbackState = false;

      this.editorsArray = new EditorsMap();
      this.editorsArray.maining = function(obj: EditorExitData) {
        let eObj: EditorExitData | undefined = this.get(obj.id);
        if (obj.focused)
          this.forEach((e: EditorExitData) => e.focused = false);
        if (eObj == undefined){
          this.set(obj.id, obj);
        } else {
          if (obj.initialText != undefined) eObj.initialText = obj.initialText;
          if (obj.text != undefined) eObj.text = obj.text;
          if (obj.focused != undefined) eObj.focused = obj.focused;
          if (obj.time != undefined) eObj.time = obj.time;
          this.set(obj.id, eObj);
        }
        return !!obj.logicPlus;
      }

      const self = this;
      this.saveFeedback = (event: SaveFeedbackEvent) => self._saveFeedback(event);
      this.onCheckFeedback = () => self._checkUnsavedFeedbackAndWarn();
  }

  get isSaving(): boolean {
    return this._isSaving;
  }

  get collapseBody(): boolean {
    return this._collapseBody;
  }

  /** Save an annotation to the server. */
  private _save(annot: Annotation, loggedUserId?: string, loggedUserDisplayName?: string): Observable<Annotation> {
    var saved: Observable<Annotation>;
    var updating = !!annot.id;

    if (authorityChecker.isISC(this.settings)) {
      //LEOS-3992 : In ISC context, we need to certify that the group is always the connectedEntity
      if (this.settings.connectedEntity) {
        annot.group = this.settings.connectedEntity;
      }

      //LEOS-3839 : In ISC context, if annotation is SENT and we are editing it, we need to increment the responseVersion
      if (annot.document
        && annotationMetadata.isSent(annot)
        && this.settings.displayMetadataCondition.responseVersion
        && !isNaN(parseInt(this.settings.displayMetadataCondition.responseVersion))) {
        annot.document!.metadata!['responseVersion'] = this.settings.displayMetadataCondition.responseVersion;
        if (loggedUserId && loggedUserDisplayName) {
          annot.user = loggedUserId;
          annot.user_info!.display_name = loggedUserDisplayName;
        }
      }
    }



    saved = this.sendUserPermissions().pipe(
      mergeMap(() => {
        if (updating) {
          //LEOS-4046 : Send connectedEntity on annotation create
          return this.api.annotation.update({ id: annot.id, connectedEntity: this.settings.connectedEntity }, annot);
        } else {
          //LEOS-4046 : Send connectedEntity on annotation update
          return this.api.annotation.create({ connectedEntity: this.settings.connectedEntity }, annot);
        }
      })
    );

    const self = this;
    return saved.pipe(
      map((savedAnnot: Annotation) => {
        var event: string;

        // Copy across internal properties which are not part of the annotation
        // model saved on the server
        savedAnnot.$tag = annot.$tag;
        Object.keys(annot).filter((key: string) => key.startsWith('$'))
          .forEach((key: string) => {(savedAnnot as any)[key] = (annot as any)[key];})

        if (self.isReply) {
          event = updating ? self.analytics.events.REPLY_UPDATED : self.analytics.events.REPLY_CREATED;
        } else if (self.isHighlight) {
          event = updating ? self.analytics.events.HIGHLIGHT_UPDATED : self.analytics.events.HIGHLIGHT_CREATED;
        } else if (annotationMetadata.isPageNote(self.annotation)) {
          event = updating ? self.analytics.events.PAGE_NOTE_UPDATED : self.analytics.events.PAGE_NOTE_CREATED;
        } else {
          event = updating ? self.analytics.events.ANNOTATION_UPDATED : self.analytics.events.ANNOTATION_CREATED;
        }

        self.analytics.track(event);
        return savedAnnot;
      })
    );
  }

  /* prevent annotation editor lost focusing*/
  onMouseDown (event: any){
    const self = this;
    this.noBlur = true;

    setTimeout(() => {
      let ee: { time?: number, editor?: HTMLElement } | undefined;
      self.editorsArray.forEach((e: EditorExitData) => {
        let change = (ee == undefined)
          || (!ee.time && e.time)
          || (ee.time && e.time && (ee.time < e.time));
        if (!change) return;
        ee = {'time': e.time, 'editor': e.editor};
      });
      ee?.editor?.focus();
    }, 333);
  }

  /**
    * Initialize the controller instance.
    *
    * All initialization code except for assigning the controller instance's
    * methods goes here.
    */
  public ngOnInit(): void {
    this.originalAnnotationUser = this.annotation.user;

    const self = this;
    this.setExitData = (obj : EditorExitData): void => {
      let and = self.editorsArray.maining!(obj);
      let _bad: boolean = false;
      let _focused: boolean = false;

      self.editorsArray.forEach((e: EditorExitData)=> {
        _bad = _bad || (e.initialText !== e.text);
        _focused = _focused || !!e.focused;
      });
      if (typeof obj.remove != 'undefined')
      self.editorsArray.delete(obj.id);

      let alarm: boolean = !_focused && self.editing &&  _bad;
      if (typeof and != 'undefined')
        alarm = alarm && and;
        self.editedExit = alarm;

      let m: HTMLElement | null = self.$element[0].closest('.thread-list__card');
      if (!!m) {
        m.style.backgroundColor = !!alarm ? 'rgb(248, 148, 6, 0.2)' /*#F89406*/ : '';
      }
      self.rootScope.$apply(self.changeDetectorRef);
    }

    /**
    * `true` if this AnnotationController instance was created as a result of
    * the highlight button being clicked.
    *
    * `false` if the annotation button was clicked, or if this is a highlight
    * or annotation that was fetched from the server (as opposed to created
    * new client-side).
    */
    this.newlyCreatedByHighlightButton = this.annotation?.$highlight || false;

    // New annotations (just created locally by the client, rather then
    // received from the server) have some fields missing. Add them.
    //
    // FIXME: This logic should go in the `addAnnotations` Redux action once all
    // required state is in the store.
    this.annotation.user = this.annotation.user || this.session.state.userid;
    this.annotation.user_info = this.annotation.user_info || profileUserInfoToAnnotationUserInfo(this.session.state.user_info);

    //LEOS-3992 : on ISC -> always use connectedEntity when creating new annotations
    if (authorityChecker.isISC(this.settings) && this.groups.get(this.settings.connectedEntity || '') && annotationMetadata.isNew(this.annotation)) {
      this.annotation.group = this.groups.get(this.settings.connectedEntity || '')?.id;
      this.groups.focus(this.annotation.group || '');
    }
    //If group not defined means: not in ISC or connectedEntity not valid. In that case, use default behaviour
    if (!this.annotation.group) {
      this.annotation.group = this.annotation.group || this.groups.focused()?.id;
    }

    if (!this.annotation.permissions) {
      // ANOT-372: also in PRIVATE mode, annotations are posted publicly from now on
      this.annotation.permissions = this.permissions.default(this.annotation.user || '', this.annotation.group || '');
    }
    this.annotation.text = this.annotation.text;
    this.annotation.precedingText = this.annotation.precedingText;
    this.annotation.succeedingText = this.annotation.succeedingText;
    this.annotation.justification = this.annotation.justification || { text: '' };
    this.annotation.forwardJustification = this.annotation.forwardJustification || '';
    this.annotation.forwarded = this.annotation.forwarded || false;

    if (!Array.isArray(this.annotation.tags)) {
      this.annotation.tags = [];
    }

    // Automatically save new highlights to the server when they're created.
    // Note that this line also gets called when the user logs in (since
    // AnnotationController instances are re-created on login) so serves to
    // automatically save highlights that were created while logged out when you
    // log in.
    this.saveNewHighlight();

    // If this annotation is not a highlight and if it's new (has just been
    // created by the annotate button) or it has edits not yet saved to the
    // server - then open the editor on AnnotationController instantiation.
    if (!this.newlyCreatedByHighlightButton) {
      if (annotationMetadata.isNew(this.annotation) || this.drafts.get(this.annotation)) {
        this.edit();
      }
    }

    if (this.annotation.document && this.annotation.document.metadata && !this.isReplyOfRootSuggestion) {
      this.metadataToDisplay = Object.keys(this.annotation.document.metadata)
        .filter(key => Object.keys(this.settings.displayMetadataCondition).indexOf(key) !== -1)
        .reduce((obj: any, key: string) => {
          obj[self.settings.displayMetadataCondition[key]] = self.annotation.document?.metadata![key];
          return obj;
        }, {});
    }

    this.annotation.hovered = false;

    this.markAsTreated = annotationMetadata.isTreated(this.annotation);
  }

  deleteOrphanSuggestion (): boolean {
    var isOrphanTab = this.store.getState().selectedTab === 'orphan';
    if (!isOrphanTab) {
      return this.isSuggestion;
    } else {
      return false;
    }
  }

  showButtons(): void {
    this.annotation.hovered = true;
  }

  hideButtons(): void {
    this.annotation.hovered = false;
  }

  get isDocumentNote (): boolean {
    return annotationMetadata.isPageNote(this.annotation);
  }

  get isContribution (): boolean {
    return annotationMetadata.isContribution(this.annotation);
  }

  get isSuggestion (): boolean {
    return annotationMetadata.isSuggestion(this.state);
  }

  updateSelectedGroup (event :{group: Group}) {
    this.updateDraft('group', event.group.id);
  }

  get selectedGroup(): string | undefined {
    return this.state.group;
  }

  getMetadata (): Metadata[] {
    return Object.entries(this.metadataToDisplay).map((metadata: [string, any]) => {
      return {key: metadata[0], value: metadata[1]};
    });
  }

  getReaders (): string | undefined  {
    return this.annotation.group;
  }

  shouldDisplayMetadata (): boolean {
    return (Object.keys(this.metadataToDisplay).length >= 0);
  }

  getMetadataInfoStyle (keytoFind: string): string {
    var index = Object.keys(this.metadataToDisplay).indexOf(keytoFind);
    return `leos-metadata-info-${index}`;
  }

  get diffText (): string {
    var htmlDiff: string = this.state.text || '';
    if ((this.editing && !this.isForward)) {
      return htmlDiff;
    }
    var origText = this.quote;
    if (this.isSuggestion && origText) {
      var dmp = new diff_match_patch();
      var textDiff = dmp.diff_wordMode(origText, this.state.text || '');
      htmlDiff = '<span class="leos-content-modified">';
      for (let d of textDiff) {
        if (d[0] === -1) {
          htmlDiff += `<span class="leos-content-removed">${d[1]}</span>`;
        }
        else if (d[0] === 0) {
          htmlDiff += d[1];
        }
        else if (d[0] === 1) {
          htmlDiff += `<span class="leos-content-new">${d[1]}</span>`;
        }
      }
      htmlDiff += '</span>';
    }
    return htmlDiff;
  }

  get justificationText (): string | undefined {
    return this.state.justification ? this.state.justification?.text : undefined;
  }

  setJustificationText (event: {text?: string}) {
    this.updateDraft('justification', {text:  event.text});
  }

  get isReplyOfRootSuggestion (): boolean {
    return !!this.isRootSuggestion && this.isReply;
  }

  get replyLabel (): string {

    if (this.isChildReplyInEditingState) {
      return 'Add reply';
    }

    let label: string = this.isCollapsed ? 'Show' : 'Hide';
    label += ' ';
    if (this.replyCount === 1) { // eslint-disable-line no-lonely-if
      label += 'reply';
    } else {
      label += 'replies';
    }
    return label;
  }

  get isISC (): boolean {
	  return authorityChecker.isISC(this.settings);
  }

  get isProcessed (): boolean {
    return annotationMetadata.isProcessed(this.annotation);
  }

  get wasDeleted (): boolean {
	  return annotationMetadata.isDeleted(this.annotation);
  }

  get isAcceptedOrRejected (): boolean {
	  return annotationMetadata.isAcceptedOrRejected(this.annotation);
  }

  get isDeleteButtonShown (): boolean {
    return !this.isSaving
      && !this.isProcessed
      && authorizer.canDeleteAnnotation(this.annotation, this.permissions, this.settings, this.session.state.userid || '', this.isRootSuggestion)
      && !(authorizer.canMergeSuggestion(this.annotation, this.permissions, this.settings) && this.deleteOrphanSuggestion());
  };

  get isEditButtonShown (): boolean {
    return !this.isSaving
      && !this.isProcessed
      && authorizer.canUpdateAnnotation(this.annotation, this.permissions, this.settings, this.session.state.userid || '');
  }

  get isReplyButtonShown (): boolean {
    return !this.isSaving
      && !this.isProcessed
      && !this.state.isPrivate
      && authorizer.canReplyToAnnotation(this.annotation, this.permissions, this.settings, this.session.state.userid || '', this.store);
  }

  get isForwardButtonShown (): boolean {
    if (!this.features.flagEnabled('forward_annotations')) return false;
    if (authorityChecker.isISC(this.settings)) return false;
    if (this.isSaving) return false;
    if (!authorizer.canForwardAnnotation(this.annotation, this.permissions, this.settings, this.session.state.userid || '')) return false;
    if (this.state.isPrivate) return false;
    if (this.isProcessed) return false;
    return !this.isForward;
  }

  get isContributionLabelShown (): boolean {
    return !this.isSaving
      && (this.isContribution || this.isOldContribution)
      && this.annotation.user_info?.display_name !== this.annotation.document?.metadata!['ISCReference']
      && authorizer.canUpdateAnnotation(this.annotation, this.permissions, this.settings, (this.session.state.userid || ''))
  }

  get isOldContribution (): boolean {
    return this.annotation.document != undefined && this.annotation.document.metadata != undefined
      && this.annotation.document.metadata['originMode'] === ORIGIN_MODES.PRIVATE;
  }

  get isJustificationShown (): boolean {
    if (this.editing) {
      return this.isSuggestion;
    } else {
      return this.isSuggestion && !this.isJustificationTextEmpty;
    }
  }

  get isShowActionFooter (): boolean {
    if (this.isSaving) return false;
    if (this.editing) return false;
    return (this.id != null);
  }

  get isShowSuggestionButtons (): boolean {
    if (!this.isSuggestion) return false;
    if (this.isProcessed) return false;
    if (this.isOrphan) return false;
    return !this.isForward;
  }

  get isMarkAsTreatedShown (): boolean {
    if(!authorizer.canTreatAnnotation(this.annotation, this.permissions, this.settings, this.session.state.userid || '')) {
      return false;
    };

    return !this.isReply
        && !this.isDocumentNote
        && !this.isISC
        && !this.wasDeleted
        && !this.isAcceptedOrRejected;
  }

  get isJustificationCollapsed (): boolean {
    return this.justificationCollapsedState;
  }

  toggleJustificationCollapsed(): void {
    if (this.justificationCollapsedState) {
        this.initialText = this.justificationText;
    }
    this.justificationCollapsedState = !this.justificationCollapsedState;
  }

  get justificationLabel (): string {
    if (this.editing) {
      if (this.justificationText) {
        return 'Justification';
     } else {
        return 'Add justification';
     }
    } else {
      if (this.isJustificationCollapsed) {
        return 'Show justification';
      } else {
        return 'Hide justification';
      }
    }
  }

  get isAnnotationForwarded (): boolean {
    return this.annotation?.forwarded || false;
  }

  get forwardJustifText (): string {
    return this.state.forwardJustification ? (this.state.forwardJustification || '') : '';
  }

  setForwardJustifText (event: {text?: string}): void {
    this.drafts.update(this.annotation, {
      isPrivate: this.state.isPrivate,
      group: this.state.group,
      tags: this.state.tags,
      text: this.annotation.text, // take over the original annotation
      forwardJustification: event.text,
      justification: { text : this.annotation.justification?.text || '' },
    });
  }

  get user (): string | undefined {
    if ((!this.annotation.user) && (this.session.state.userid)) {
      this.annotation.user = this.session.state.userid;
      this.annotation.user_info = {
        display_name : this.session.state.user_info?.display_name,
        entity_name : this.session.state.user_info?.entity_name,
        entities: this.session.state.user_info?.entities.map(a => {
          return {
            id: a.id || '',
            name: a.name,
            organizationName : a.organizationName
          };
        })
      };
    }
    return this.annotation.user;
  }

  /** Save this annotation if it's a new highlight.
   *
   * The highlight will be saved to the server if the user is logged in,
   * saved to drafts if they aren't.
   *
   * If the annotation is not new (it has already been saved to the server) or
   * is not a highlight then nothing will happen.
   *
   */
  saveNewHighlight(): void {
    const self = this;
    if (!annotationMetadata.isNew(this.annotation)) {
      // Already saved.
      return;
    }

    if (!this.isHighlight) {
      // Not a highlight,
      return;
    }

    if (this.annotation.user) {
      // User is logged in, save to server.
      // Highlights are always private.
      this.annotation.permissions = this.permissions.private(this.annotation.user);
      this._save(this.annotation).subscribe((model: Annotation) => {
          model.$tag = self.annotation.$tag;
          self.rootScope.$broadcast(events.ANNOTATION_CREATED, model);
      });
    } else {
      // User isn't logged in, save to drafts.
      this.drafts.update(this.annotation, this.state);
    }
  }

  authorize (action: string): boolean {
    return this.permissions.permits(
      {
        read : this.annotation.permissions?.read || [],
        update : this.annotation.permissions?.update || [],
        delete : this.annotation.permissions?.delete || []
      },
      action,
      (this.session.state.userid || '')
    );
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#flag
    * @description Flag the annotation.
    */
  flag(): void {
    if (!this.session.state.userid) {
      this.flash.error(
        'You must be logged in to report an annotation to the moderators.',
        'Login to flag annotations'
      );
      return;
    }

    const self = this;
    this.annotationMapper.flagAnnotation(this.annotation).pipe(
      tap(() => {
        self.analytics.track(self.analytics.events.ANNOTATION_FLAGGED);
        self.store.annotation.updateFlagStatus(self.annotation.id || '', true);
      }),
      catchError((err: Error) => {
        self.flash.error(err.message, 'Flagging annotation failed');
        return from(Promise.resolve());
      })
    );
  }

  onMarkAsTreatedCheckboxChange(event: Event) {
    const self = this;
    this.treat(this.markAsTreated).subscribe((markedAsTreated: boolean) => {
      self.markAsTreated = markedAsTreated;
      if (event.target != null) {
        (event.target as HTMLInputElement).checked = markedAsTreated;
      }
    });
  }

  treatAnnotation(){
    this.treat(this.markAsTreated).subscribe();
  }

  private treat(markAsTreated: boolean): Observable<boolean> {
    const self = this;
    if (!markAsTreated) {
      return this._checkUnsavedFeedbackAndWarn().pipe(
        mergeMap(() => {
          if (!self.$window.confirm('Are you sure you want to mark as processed?')) return of(false);
          return self.sendUserPermissions().pipe(
            mergeMap(() => {
              return self.annotationMapper.treatAnnotation(self.annotation)
            }),
            mergeMap(() => {
              self.analytics.track(self.analytics.events.ANNOTATION_DELETED);
              return of(true);
            }),
            catchError((err: Error) => {
              self.flash.error(err.message, 'Marking annotation as processed failed');
              return of(false);
            })
          );
        })
      )
    }
    if (this.$window.confirm('Are you sure you want to mark as not processed?')) {
      return this.sendUserPermissions().pipe(
        mergeMap(() => {
          return this.annotationMapper.resetAnnotation(this.annotation);
        }),
        mergeMap(() => {
          self.analytics.track(self.analytics.events.ANNOTATION_DELETED);
          return of(false);
        }),
        catchError((err: Error) => {
          self.flash.error(err.message, 'Marking annotation as not processed failed');
          return of(true);
        })
      )
    };
    return of(true);
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#delete
    * @description Deletes the annotation.
    */
  delete (): void {
    const self = this;
    this._checkUnsavedFeedbackAndWarn().pipe(
      mergeMap(() => {
        return self.sendUserPermissions()
      })
    ).subscribe(() => {
      self.$window.setTimeout(() => {
        // Don't use confirm inside the digest cycle.
        if (self.$window.confirm('Are you sure you want to delete this annotation?')) {
          self.annotationMapper.deleteAnnotation(self.annotation).subscribe({
            next() {
              let event;
              if (self.isReply) {
                event = self.analytics.events.REPLY_DELETED;
              } else if (self.isHighlight) {
                event = self.analytics.events.HIGHLIGHT_DELETED;
              } else if (annotationMetadata.isPageNote(self.annotation)) {
                event = self.analytics.events.PAGE_NOTE_DELETED;
              } else {
                event = self.analytics.events.ANNOTATION_DELETED;
              }
              self.analytics.track(event);
            },
            error(err: Error) {
              self.flash.error(err.message, 'Deleting annotation failed');
              return from(Promise.resolve());
            }
          })
        }
      });
    });
  }

  private sendUserPermissions(): Observable<void> {
    return this.api.permissions.update({}, { permissions: this.permissions.getUserPermissions() }).pipe(
      mergeMap(() => of(undefined)),
      catchError(() => of(undefined))
    )
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#edit
    * @description Switches the view to an editor.
    */
  edit(): void {
    if (!this.drafts.get(this.annotation)) {
      this.drafts.update(this.annotation, this.state);
    }
  }

  /**
   * @ngdoc method
   * @name annotation.AnnotationController#editing.
   * @returns {boolean} `true` if this annotation is currently being edited
   *   (i.e. the annotation editor form should be open), `false` otherwise.
   */
  get editing () : boolean {
    const isEditing: boolean = (this.drafts.get(this.annotation) != null) && !this.isSaving;
    if (this.oldEditingState !== isEditing) {
      if(this.onEditStateChanged) this.onEditStateChanged.emit({ editing: isEditing });
    }
    this.oldEditingState = isEditing;
    return isEditing;
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#group.
    * @returns {Object} The full group object associated with the annotation.
    */
  get group (): Group | undefined {
    return this.groups.get(this.state.group || '');
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotaitonController#hasContent
    * @returns {boolean} `true` if this annotation has content, `false`
    *   otherwise.
    */
  get hasContent() : boolean{
    return annotationMetadata.hasContent(this.state);
  }

  /**
    * Return the annotation's quote if it has one or `null` otherwise.
    */
  get quote (): string | null {
    if (this.annotation.target?.length === 0) {
      return null;
    }
    var target = this.annotation.target![0];
    if (!target.selector) {
      return null;
    }
    var quoteSel = target.selector.find(function (sel) {
      return sel.type === 'TextQuoteSelector';
    });
    return quoteSel ? quoteSel.exact : null;
  }

  get id (): string | undefined {
    return this.annotation.id;
  }

  get precedingText (): string | undefined {
    return this.annotation.precedingText;
  }

  get succeedingText (): string | undefined {
    return this.annotation.succeedingText;
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#isHighlight.
    * @returns {boolean} true if the annotation is a highlight, false otherwise
    */
  get isHighlight (): boolean {
    return this.newlyCreatedByHighlightButton || annotationMetadata.isHighlight(this.annotation);
  }

  get isSent (): boolean {
    return annotationMetadata.isSent(this.annotation);
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#isShared
    * @returns {boolean} True if the annotation is shared (either with the
    * current group or with everyone).
    */
  get isShared (): boolean {
    return !this.state.isPrivate;
  }

  get isJustificationTextEmpty(): boolean {
    let justificationText: string | undefined = this.justificationText;
    return (justificationText == undefined) || (justificationText.length == 0);
  }

  // Save on Meta + Enter or Ctrl + Enter.
  onKeydown (event: KeyboardEvent) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      event.preventDefault();
      this.save();
    }
  }

  toggleCollapseBody (event: Event) {
    event.stopPropagation();
    this._collapseBody = !this.collapseBody;
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#reply
    * @description
    * Creates a new message in reply to this annotation.
    */
  reply(): void {
    var references = (this.annotation.references || []).concat((this.annotation.id || ''));
    var group = this.annotation.group;
    var replyPermissions;
    var userid = this.session.state.userid;
    if (userid) {
      replyPermissions = this.state.isPrivate ?
        this.permissions.private(userid) : this.permissions.shared(userid, group || '');
    }
    this.annotationMapper.createAnnotation({
      group: group,
      references: references,
      permissions: replyPermissions,
      target: [{ source: this.annotation.target![0].source }],
      uri: this.annotation.uri,
    });
  }

  forward (): void {
    var group: string = this.annotation.group || '';
    var forwardPermissions;
    var userid: string = this.session.state.userid || '';
    if (userid) {
      forwardPermissions = this.state.isPrivate ?
        this.permissions.private(userid) : this.permissions.shared(userid, group);
    }
    var forwardedAnnot: Annotation;

    // if a reply is forwarded the root annotation is copied an then some necessary adjustmends are made
    if(this.isReply) {
      var parentId : string = this.annotation.references![0];
      var parent = this.store.annotation.findAnnotationByID(parentId);
      forwardedAnnot = Object.assign({}, parent);
      forwardedAnnot.replyText = this.annotation.text;
      forwardedAnnot.isReply = true;
      forwardedAnnot.forwardJustification = "";
    }
    else {
      forwardedAnnot = Object.assign({}, this.annotation);
    }
    // clear the original tag to avoid interference with original annotation and
    // assign the next tag to be used; events will later pass by the nextTag and increase it
    forwardedAnnot.$tag = "t" + this.store.getState().nextTag;
    forwardedAnnot.forwarded = true;
    forwardedAnnot.references = undefined;
    forwardedAnnot.id = undefined;
    var now = new Date();
    forwardedAnnot.created = now.toISOString();
    forwardedAnnot.updated = now.toISOString();
    forwardedAnnot.originGroup=this.annotation.group;
    // set the current user!
    forwardedAnnot.user_info = profileUserInfoToAnnotationUserInfo(this.session.state.user_info);
    forwardedAnnot.permissions = forwardPermissions;
    forwardedAnnot.user = userid;

    this.annotationMapper.createAnnotation(forwardedAnnot);
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#revert
    * @description Reverts an edit in progress and returns to the viewer.
    */
  revert(): void {
    this.drafts.remove(this.annotation);
    if (annotationMetadata.isNew(this.annotation)) {
      this.rootScope.$broadcast(events.ANNOTATION_DELETED, this.annotation);
    }
  }

  // LEOS change
  /**
   * @returns Whether the text of this annotation can be submitted
   * (i.e. is not empty and for forwards, if a group to forward it to has been choosen).
   */
  get isTextValid (): boolean {
    if (!this.isSuggestion && !this.hasContent) return false;
    if (!this.isForward) return true;
    //if it is forwarded and a group to forward it to has been chosen
    return this.state.group != this.annotation.originGroup;
  }

  save (): void {
    if (!this.annotation.user) {
      this.flash.info('Please log in to save your annotations.');
      return;
    }
    if (!this.isTextValid && this.isShared) { // LEOS change
      this.flash.info('Please add text or a tag before publishing.');
      return;
    }

    var updatedModel = updateModel(this.annotation, this.state, this.permissions);

    // Optimistically switch back to view mode and display the saving
    // indicator
    this._isSaving = true;

    const loggedUserId: string | undefined = this.session.state.userid;
    const loggedUserDisplayName: string | undefined = this.session.state.user_info?.display_name;
    const self = this;

    this._save(updatedModel, loggedUserId, loggedUserDisplayName).subscribe({
      next(model: Annotation) {
        model.user_info!.entities = model.user_info?.entities || self.annotation.user_info?.entities;
        updatedModel = Object.assign(updatedModel, model);

        const event = annotationMetadata.isNew(self.annotation) ? events.ANNOTATION_CREATED
          : events.ANNOTATION_UPDATED;
        self.drafts.remove(self.annotation);
        self.rootScope.$broadcast(event, updatedModel);
      },
      error(err: Error) {
        self.edit();
        self.flash.error(err.message, 'Saving annotation failed');
      },
      complete() {
        self._isSaving = false;
      },
    });
  }

  /**
    * @ngdoc method
    * @name annotation.AnnotationController#setPrivacy
    *
    * Set the privacy this.settings on the annotation to a predefined
    * level. The supported levels are 'private' which makes the annotation
    * visible only to its creator and 'shared' which makes the annotation
    * visible to everyone in the group.
    *
    * The changes take effect when the annotation is saved
    */
  setPrivacy (event: {level: string}): void {
    // When the user changes the privacy level of an annotation they're
    // creating or editing, we cache that and use the same privacy level the
    // next time they create an annotation.
    // But _don't_ cache it when they change the privacy level of a reply.
    const privacy = event.level;
    if (!annotationMetadata.isReply(this.annotation)) {
      this.permissions.setDefault(privacy || '');
    }
    this.updateDraft('isPrivate', (privacy === 'private'));
  }

  tagSearchURL (tag: string): string | null {
    if (this.isThirdPartyUser) {
      return null;
    }
    return this.serviceUrl.get('search.tag', { tag: tag });
  }

  get isOrphan (): boolean {
    if (typeof this.annotation.$orphan === 'undefined') {
      return this.annotation.$anchorTimeout;
    }
    return this.annotation.$orphan;
  }

  get isThirdPartyUser (): boolean {
    return isThirdPartyUser(this.annotation.user || '', this.settings.authDomain || '');
  }

  get isDeleted (): boolean {
    return this.streamer.hasPendingDeletion(this.annotation.id || '');
  }

  get isHiddenByModerator (): boolean {
    return !!this.annotation.hidden;
  }

  canFlag (): boolean {
    // Users can flag any annotations except their own.
    return this.session.state.userid !== this.annotation.user;
  }

  get isFlagged (): boolean {
    return !!this.annotation.flagged;
  }

  get isReply (): boolean {
    return annotationMetadata.isReply(this.annotation);
  }

  get isForward (): boolean {
    return annotationMetadata.isForward(this.annotation);
  }

  get incontextLink (): string {
    if (this.annotation.links) {
      return this.annotation.links.incontext ||
        this.annotation.links.html ||
        '';
    }
    return '';
  }

  /**
   * Sets whether or not the controls for expanding/collapsing the body of
   * lengthy annotations should be shown.
   */
  setBodyCollapsible (event: {collapsible: boolean}): void {
    this.rootScope.$apply(this.changeDetectorRef, () => {
      if (event.collapsible === this.canCollapseBody) {
        return;
      }
      this.canCollapseBody = event.collapsible;
    });
  }

  setText(event: {text?: string}): void {
    this.updateDraft('text', event.text);
  }

  updateDraft(property: string, value: any): void {
    const state = this.state;
    var changes: any = {
        group: state.group,
        isPrivate: state.isPrivate,
        tags: state.tags,
        text: state.text,
        justification: state.justification,
        forwardJustification: state.forwardJustification
    }
    changes[property] = value;

    this.drafts.update(this.annotation, changes);
  }

  setTags(tags: string[]): void {
    this.updateDraft('tags', tags);
  }

  get state(): DraftState {
    const isAnnotationPrivate = !this.permissions.isShared({
      delete : this.annotation.permissions?.delete || [],
      read : this.annotation.permissions?.read || [],
      update : this.annotation.permissions?.update|| []
    });
    return this.drafts.annotationState(this.annotation, isAnnotationPrivate);
  }

  /**
   * Return true if the CC 0 license notice should be shown beneath the
   * annotation body.
   */
  get shouldShowLicense(): boolean {
    if (!this.editing || !this.isShared) {
      return false;
    }
    return this.group?.type !== 'private';
  };

  get isFooterShown(): boolean {
    if(this.editing) return true;
    if(!this.isReply && (this.replyCount || 0) > 0) return true;
    if(this.editedExit) return true;
    return this.isSaving;
  }

  get isHasFeedback (): boolean {
    return annotationMetadata.hasFeedback(this.annotation);
  }

  addFeedback() : void {
    this.isAddingFeedbackState = true;
  }

  get isAddingFeedback() : boolean {
    return this.isAddingFeedbackState;
  }

  onStopAddingFeedback() : void {
    this.isAddingFeedbackState = false;
  }

  get isFeedbackShown (): boolean {
    if (`${this.settings.feedbackActive}` != "true") return false;
    if (!this.features.flagEnabled("add_feedback")) return false;
    if (!this.id) return false;
    if (this.isSaving) return false;
    if (this.isHighlight) return false;
    if (this.editing) return false;
    if (this.isReply) return false;
    if (this.isProcessed) return this.isHasFeedback;
    if (this.isISC) return false;
    if (this.isHasFeedback) return true;
    return this.isAddingFeedback && (this.settings.operationMode !== OPERATION_MODES.READ_ONLY && this.settings.operationMode !== OPERATION_MODES.STORED_READ_ONLY);
  }

  private _saveFeedback (event: SaveFeedbackEvent): Observable<void> {
    const reqParams = {
      id: this.annotation.id,
      connectedEntity: this.settings.connectedEntity
    };

    return this.api.annotation.updateFeedback(reqParams, {'text': event.feedbackText}).pipe(
      map((feedback?: Feedback) => {
        if (feedback != null) {
          this.unsavedFeedback = undefined;
          this.annotation.feedback = feedback;
        }
        return;
      })
    );
  }

  onReplyCount(): void {
    this.onReplyCountClick.emit();
  }

  onNoBlurChange(noBlur: boolean) {
    this.noBlur = noBlur;
  }

  private _checkUnsavedFeedbackAndWarn (): Observable<void> {
    if(this.hasUnsavedFeedback) {
      var msg = "Do you want to save your feedback?";
      if(this.$window.confirm(msg)) {
        return this.saveFeedback({ feedbackText: this.unsavedFeedback});
      }
    }
    return from(Promise.resolve());
  }

  get hasUnsavedFeedback (): boolean {
    return (this.unsavedFeedback != undefined) && this.unsavedFeedback.length > 0;
  }

  setUnsavedFeedback (event: {unsavedFeedback?: string}): void {
    this.unsavedFeedback = event.unsavedFeedback;
  }

  get isNormalOrFeedback (): boolean {
    //09.12.2022 have to show feedback switch on normal annotation or feedbacked one
    return (annotationMetadata.isNormal(this.annotation) || (annotationMetadata.hasFeedback(this.annotation)));
  }
}
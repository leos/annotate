'use strict';

import { HypothesisAuthState } from '../models/hypothesis-app.model';
import { Component, Input } from '@angular/core';
import {  formatDate } from '@angular/common';


@Component({
  selector: 'help-link',
  templateUrl: '../templates/help-link.component.html'
})
export class HelpLinkComponent {
  @Input()
  public version?: string;

  @Input()
  public userAgent?: string;

  @Input()
  public url?: string;

  @Input()
  public documentFingerprint?: any;

  @Input()
  public auth?: HypothesisAuthState;

  @Input()
  public dateTime?: Date;

  constructor() {}

  get helpLink(): string {
    const dateTimeStr: string =  (this.dateTime != undefined) ? formatDate(this.dateTime, 'dd MMM yyyy HH:mm:ss Z', 'en-US') : '-';
    return `mailto:support@hypothes.is?subject=Hypothesis%20support&amp;body=Version:%20${this.version || '-'}%0D%0AUser%20Agent:%20${this.userAgent || '-'}%0D%0AURL:%20${this.url || '-'}%0D%0APDF%20fingerprint:%20${this.documentFingerprint || '-'}%0D%0AUsername:%20${this.auth?.username || '-'}%0D%0ADate:%20${dateTimeStr}`
  }
}
'use strict';

import { HypothesisAuthState } from '../models/hypothesis-app.model';
import { HypothesisStoreService } from '../store';
import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

/**
 * @ngdoc directive
 * @name helpPanel
 * @description Displays product version and environment info
 */
@Component({
  selector: 'help-panel',
  templateUrl: '../templates/help-panel.component.html',
  // styleUrls: ['../styles/components/help-panel.scss']
})
export class HelpPanelComponent implements OnChanges{
  @Input()
  public auth?: HypothesisAuthState;

  @Output()
  public onClose: EventEmitter<void>;

  public version: string;
  public userAgent: string;
  public dateTime: Date;
  public documentFingerprint: any;
  public url: string;

  constructor(private store: HypothesisStoreService) {
      this.onClose = new EventEmitter();
      this.userAgent = window.navigator.userAgent;
      this.version = '__VERSION__';  // replaced by versionify
      this.dateTime = new Date();
      this.documentFingerprint = null;
      this.url = '';
  }

  public ngOnChanges(changes: SimpleChanges) {
    if(changes['storeFrames'] == undefined) return;

    const frames: any[] = changes['storeFrames'].currentValue;
    if (frames.length === 0) {
      return;
    }
    this.url = frames[0].uri;
    this.documentFingerprint = frames[0].metadata.documentFingerprint;    
  }

  private get storeFrames(): any {
    return this.store.frames.frames();
  }

  public onCloseButtonClick() {
    this.onClose?.emit();
  }
}
'use strict';

import { isThirdPartyService } from '../util/is-third-party-service.util';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { IHypothesisSearchController, HypothesisAuthState } from '../models/hypothesis-app.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

@Component({
  selector: 'top-bar',
  templateUrl: '../templates/top-bar.component.html',
})
export class TopBarComponent {
  @Input()
  public auth?: HypothesisAuthState;

  @Input()
  public isSidebar?: boolean;

  @Input()
  public isSidebarOpen?: boolean;

  @Input()
  public searchController?: IHypothesisSearchController;

  @Input()
  public pendingUpdateCount?: number;

  @Output()
  public onApplyPendingUpdates: EventEmitter<void>;

  @Output()
  public onShowHelpPanel: EventEmitter<void>;

  @Output()
  public onLogin: EventEmitter<void>;

  @Output()
  public onLogout: EventEmitter<void>;

  @Output()
  public onSharePage: EventEmitter<void>;

  @Output()
  public onSignUp: EventEmitter<void>;

  readonly isThemeClean : boolean;

  private settings: IHypothesisJsonConfig;

  constructor(settingsService: HypothesisConfigurationService){
    this.settings = settingsService.getConfiguration();
    this.isThemeClean = (this.settings.theme === 'clean');
    this.onShowHelpPanel = new EventEmitter();
    this.onApplyPendingUpdates = new EventEmitter();
    this.onLogin = new EventEmitter();
    this.onSharePage = new EventEmitter();
    this.onSignUp = new EventEmitter();
    this.onLogout = new EventEmitter();
  }

  get showSharePageButton (): boolean {
    return !isThirdPartyService(this.settings);
  };

  showHelpPanel(): void {
    this.onShowHelpPanel.emit();
  }

  login(): void {
    this.onLogin.emit();
  }

  logout(): void {
    this.onLogout.emit();
  }

  applyPendingUpdates(): void {
    this.onApplyPendingUpdates.emit();
  }

  sharePage() {
    this.onSharePage.emit();
  }

  signUp() {
    this.onSignUp.emit();
  }

  searchControllerUpdate(event: {$query: string}) {
    this.searchController?.update(event.$query);
  }
}
'use strict';

import * as annotationMetadata from '../annotation-metadata';
import {ApiService} from '../services/api.service';
import {FlashService} from '../services/flash.service';
import { Annotation } from '../../../../../shared/models/annotation.model';
import { Component, Input } from '@angular/core';
import {HypothesisStoreService} from '../store';

/**
 * Banner shown above flagged annotations to allow moderators to hide/unhide the
 * annotation from other users.
 */
@Component({
  selector: 'moderation-banner',
  templateUrl: '../templates/moderation-banner.component.html',
  // styleUrls: ['../styles/components/moderation-banner.scss']
})
export class ModerationBannerComponent {
  @Input()
  public annotation?: Annotation;

  constructor(private store: HypothesisStoreService, 
    private flash: FlashService, 
    private api: ApiService) {
  }

  get flagCount(): number | null {
    return annotationMetadata.flagCount(this.annotation || {});
  };

  get isHidden(): boolean {
    return this.annotation?.hidden || true;
  };

  get isHiddenOrFlagged(): boolean {
    return this.flagCount !== null && (this.flagCount > 0 || this.isHidden);
  };

  get isReply(): boolean {
    return annotationMetadata.isReply(this.annotation || {});
  };

  /**
   * Hide an annotation from non-moderator users.
   */
  hideAnnotation() {
    const self = this;
    this.api.annotation.hide({id: this.annotation?.id}).subscribe({
      next() {
        self.store.annotation.hideAnnotation(self.annotation?.id || '');
      },
      error() {
        self.flash.error('Failed to hide annotation');
      },
    });
  };

  /**
   * Un-hide an annotation from non-moderator users.
   */
  unhideAnnotation () {
    const self = this;
    this.api.annotation.unhide({id: this.annotation?.id}).subscribe({
      next() {
        self.store.annotation.unhideAnnotation(self.annotation?.id || '');
      },
      error() {
        self.flash.error('Failed to unhide annotation');
      },
    });
  };
}
'use strict';

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sort-dropdown',
  templateUrl: '../templates/sort-dropdown.component.html'
})
export class SortDropdownComponent {
  @Input()
  public sortKey?: string;

  @Input()
  public sortKeysAvailable?: string[];

  @Output()
  public changeSortKey: EventEmitter<{sortKey: string}>;

  constructor(){
    this.changeSortKey = new EventEmitter();
  }

  public onChangeSortKey(key: string) {
    this.changeSortKey.emit({sortKey: key});
  }

  public toggleDropdown(dropdown: NgbDropdown): void {
    if (dropdown.isOpen()) {
      dropdown.close();
    } else {
      dropdown.open();
    }
  }
}

'use strict';

import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Observable, from, mergeMap, of, catchError  } from 'rxjs';
import $ from '../imports/jquery';

interface IconContainer {
  [propName: string]: () => Observable<string>;  
  refresh: () => Observable<string>;
  cursor: () => Observable<string>;
}

const icons: IconContainer = { 
  refresh: () => readSvgIcon('images/icons/refresh.svg'), 
  cursor: () => readSvgIcon('images/icons/cursor.svg')
};


function readSvgIcon(filePath: string): Observable<string> {
  return from(fetch(filePath)).pipe(
    mergeMap((response: Response) => from(response.text())),
    catchError(() => of(''))
  )
}

/**
 * The <svg-icon> component renders SVG icons using inline <svg> tags,
 * enabling their appearance to be customized via CSS.
 *
 * This matches the way we do icons on the website, see
 * https://github.com/hypothesis/h/pull/3675
 */
@Component({
  selector: 'svg-icon',
  template: '<svg class="hypothesis-svg-icon"></svg>'
})
export class SvgIconComponent implements OnInit {
  /** The name of the icon to load. */
  @Input()
  public name?: string;
  
  private $element: JQuery<HTMLElement>;

  constructor(elementRef: ElementRef) {
    this.$element = $(elementRef.nativeElement);
  }

  public ngOnInit() {
    if (this.name == undefined || !icons[this.name]) {
      throw new Error('Unknown icon: ' + this.name);
    }

    const $element: JQuery<HTMLElement> = this.$element;
    icons[this.name]().subscribe({
      next(iconData: string) {
        $element[0].innerHTML = iconData;
      }
    })
  };
}
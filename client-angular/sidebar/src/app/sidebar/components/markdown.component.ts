'use strict';

import * as _ from 'lodash';

import * as commands from '../markdown-commands';
import * as mediaEmbedder from '../media-embedder';
import { renderMathAndMarkdown as renderMarkdown } from '../render-markdown';
import { NewStateFunction, EditorExitData } from '../models/markdown.model';
import { Component,
  ElementRef,
  EventEmitter, 
  Input, 
  Output, 
  OnChanges, 
  SimpleChanges,
  SecurityContext,
  OnInit,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {ScopeService} from '../services/scope.service';

const debounce = _.debounce;


function focusInput(input?: HTMLElement): number {
  // When the visibility of the editor changes, focus it.
  // A timeout is used so that focus() is not called until
  // the visibility change has been applied (by adding or removing
  // the relevant CSS classes)
  return window.setTimeout(() => input?.focus(), 0);
}


/**
 * @name markdown
 * @description
 * This directive controls both the rendering and display of markdown, as well as
 * the markdown editor.
 */
@Component({
  selector: 'markdown',
  templateUrl: '../templates/markdown.component.html',
  // styleUrls: ['../styles/components/markdown.scss']
})
export class MarkdownComponent implements OnInit, OnChanges, OnDestroy {
  private input?: HTMLTextAreaElement;
  private isTextPasted: boolean;

  //Bindings
  @Input()
  public customTextClass?: string | Object;

  @Input()
  public readOnly?: boolean;

  @Input()
  public text?: string;

  @Input()
  public noBlur?: boolean;

  @Output()
  public noBlurChange: EventEmitter<boolean>;

  @Output()
  public onEditText: EventEmitter<{text?: string}>;

  @Output()
  public onSetExitData: EventEmitter<EditorExitData>;

  private previewElementObserver?: MutationObserver;

  // Create lost focus unsave warning 
  private initialText: string;
  private menuShown: boolean;
  private timerBlur: number | null;
  private preview: boolean;
  private componentId: string;
  private $element: HTMLElement;
  public previewText?: string;

  constructor(elementRef: ElementRef<HTMLElement>,
    private rootScope: ScopeService,
    private changeDetectorRef: ChangeDetectorRef,
    private domSanitizer: DomSanitizer) {
      this.previewElementObserver = undefined;
      this.componentId = window.crypto.randomUUID();
      this.$element = elementRef.nativeElement;
      this.isTextPasted = false;
      this.initialText = '';
      this.menuShown = false;
      this.preview = false;
      this.timerBlur = null;
      this.onEditText = new EventEmitter();
      this.onSetExitData = new EventEmitter();
      this.noBlurChange = new EventEmitter();
  }

  public ngOnInit() {
    const self = this;
    this.input = this.$element.getElementsByTagName('textarea')[0];

    const previewElement: HTMLElement | undefined = (this.$element.querySelector('.js-markdown-preview') || undefined) as HTMLElement;
    if (previewElement) {
      mediaEmbedder.replaceLinksWithEmbeds(previewElement);
      this.previewElementObserver = new MutationObserver((mutations, observer) => {
        mediaEmbedder.replaceLinksWithEmbeds(previewElement);
      })
      this.previewElementObserver.observe(previewElement, { attributes: true, characterData: true })
    }

    // Keyboard shortcuts for bold, italic, and link.
    this.$element.addEventListener('keydown', function(e) {
      var shortcuts: any = {
        66: self.insertBold.bind(self),
        73: self.insertItalic.bind(self),
        75: self.insertLink.bind(self),
      };

      var shortcut: VoidFunction = shortcuts[e.keyCode];
      if (shortcut && (e.ctrlKey || e.metaKey)) {
        e.preventDefault();
        shortcut();
      }
    });

    const handleInputChange = debounce(() => {
      if (!self.input) return;
      if (self.isTextPasted) {
          // ANOT-314: only keep characters 9 (tab), 10, 11, 13 (line breaks), but drop control characters
          self.input.value = self.input.value.replace(/[\u0002-\u0008]/g, '')
                                   .replace(/[\u000E-\u001F]/g, '')
                                   .replace(/\u000C/g, '');
          self.isTextPasted = false;
      }

      self.rootScope.$apply(self.changeDetectorRef, () => {
        self.onEditText.emit({text: self.input?.value});
      });
    }, 100);
    this.input?.addEventListener('input', handleInputChange);
  }

  public ngOnDestroy(): void {
      this.previewElementObserver?.disconnect();
  }

  public ngOnChanges(changes: SimpleChanges) {
    const self = this;
    // Exit preview mode when leaving edit mode
    if (changes['readOnly']?.currentValue) {
      this.preview = false; 
    }    

    // Re-render the markdown when the view needs updating.
    if (changes['text']) {
      this.previewText = renderMarkdown(changes['text']?.currentValue || '', (value: string): string => {
        return self.domSanitizer.sanitize(SecurityContext.HTML, value) || '';
      });
    }

    this.initialText = this.text || '';
    if (!this.showEditor) {
      this.onSetExitData.emit({
        id: this.componentId,
        focused: true,
        remove: true
      });
      return;    
    }
    if (!this.input) return;
    this.input.value = this.text || '';
    focusInput(this.input);
  }
  
  get showEditor(): boolean {
    return !this.readOnly && !this.preview;
  }

  /**
   * Transform the editor's input field with an editor command.
   */
  updateState(newStateFn: NewStateFunction) {
    var newState = newStateFn({
      text: this.input?.value,
      selectionStart: this.input?.selectionStart,
      selectionEnd: this.input?.selectionEnd,
    });

    if (this.input != undefined) {
      this.input.value = newState.text || '';
      this.input.selectionStart = newState.selectionStart || 0;
      this.input.selectionEnd = newState.selectionEnd || 0;
      this.input.focus();
    }
 
    // The input field currently loses focus when the contents are
    // changed. This re-focuses the input field but really it should
    // happen automatically.
    this.onEditText.emit({text: this.input?.value});
  }

  insertBold () {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleSpanStyle(state, '**', '**', 'Bold');
    });
  };

  insertItalic () {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleSpanStyle(state, '*', '*', 'Italic');
    });
  };

  insertMath () {
    this.updateState(function (state: commands.EditorState) {
      var before = state?.text?.slice(0, state.selectionStart);

      if (before?.length === 0 ||
          before?.slice(-1) === '\n' ||
          before?.slice(-2) === '$$') {
        return commands.toggleSpanStyle(state, '$$', '$$', 'Insert LaTeX');
      } else {
        return commands.toggleSpanStyle(state, '\\(', '\\)',
                                            'Insert LaTeX');
      }
    });
  }

  insertLink () {
    this.updateState(function (state: commands.EditorState) {
      return commands.convertSelectionToLink(state, undefined);
    });
  }

  insertIMG () {
    this.updateState(function (state: commands.EditorState) {
      return commands.convertSelectionToLink(state,
        commands.LinkType.IMAGE_LINK);
    });
  }

  insertList () {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '* ');
    });
  }

  insertNumList () {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '1. ');
    });
  }

  insertQuote () {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '> ');
    });
  }

  togglePreview () {
    this.preview = !this.preview;
  };

  onPaste (event: Event) {
    this.isTextPasted = true;
    event.stopPropagation();
  }

  onTextAreaBlur(event: Event) {
    this.stopTimer();
    if (this.noBlur) {
      this.noBlurChange.emit(false); 
      return;
    }

    const self = this;
    this.timerBlur = window.setTimeout(()=> {
        self.timerBlur = null;
        self.onSetExitData.emit({
          'id': self.componentId,
          'initialText': self.initialText,
          'text': self.text,
          'logicPlus': !self.menuShown,
          'focused': false
        });
    },333);
  }

  onTextAreaFocus(event: Event) {
    this.noBlurChange.emit(false); 
    this.onSetExitData.emit({
      id: this.componentId,
      initialText: this.initialText,
      focused: true,
      editor: this.input || undefined,
      time:(new Date).getTime()
    }); 
  }

 onMenuColaborators(event: Event): void{
    // Event from menu to awoid warning
    const _event = event as CustomEvent;
    this.menuShown = _event.detail;
    if (!this.menuShown){
      this.input?.focus();
      return;
    }
    this.stopTimer();
    this.onSetExitData.emit({
      id: this.componentId,
      initialText: this.initialText,
      text: this.text,
      logicPlus: !this.menuShown,
      focused: false
    });
 }

  private stopTimer(): void {
    if (!this.timerBlur) return;
    clearTimeout(this.timerBlur);
    this.timerBlur = null;
  }

  get customCssClass(): string | Object | undefined {
    if (this.preview) return 'markdown-preview';
    return this.customTextClass;
  }
}
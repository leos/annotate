'use strict';

import * as sessionUtil from '../util/session.util';
import {SessionService} from '../services/session.service';
import { Component } from '@angular/core';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';

/**
 * @name sidebarTutorial
 * @description Displays a short tutorial in the sidebar.
 */
@Component({
  selector: 'sidebar-tutorial',
  templateUrl: '../templates/sidebar-tutorial.component.html',
  // styleUrls: ['../styles/components/sidebar-tutorial.scss']
})
export class SidebarTutorialComponent {
  readonly isThemeClean : boolean;

  constructor(private session: SessionService, configurationService: HypothesisConfigurationService){
    this.isThemeClean = configurationService.getConfiguration().theme === 'clean';
  }

  get showSidebarTutorial (): boolean {
    return sessionUtil.shouldShowSidebarTutorial(this.session.state);
  }

  dismiss () {
    this.session.dismissSidebarTutorial().subscribe(()=>{});
  }
}
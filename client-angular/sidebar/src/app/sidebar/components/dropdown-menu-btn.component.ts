'use strict';

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dropdown-menu-btn',
  templateUrl: '../templates/dropdown-menu-btn.component.html',
  // styleUrls: ['../styles/components/dropdown-menu-btn.scss', '../styles/leos-dropdown-menu-btn.scss'],
})
export class DropdownMenuBtnComponent {
  @Input()
  public isDisabled?: boolean;

  @Input()
  public label?: string;

  @Input()
  public dropdownMenuLabel?: string;

  @Input()
  public hasDropdown?: boolean;

  @Output()
  public click: EventEmitter<Event>;
  
  @Output()
  public toggleDropdown: EventEmitter<Event>;

  constructor(){
    this.toggleDropdown = new EventEmitter();
    this.click = new EventEmitter();
  }

  buttonClick ($event: Event) {
    this.click.emit($event);
  }

  onToggleDropdown ($event: Event) {
    $event.stopPropagation();

    const self = this;
    setTimeout(() => {
      self.toggleDropdown.emit($event);
    }, 0);
  }
}

'use strict';

import * as ANNOTATION_STATUS from '../annotation-status';

import * as events from '../events';
import { isISC as isISCAuthority} from '../authority-checker';
import { Annotation } from '../../../../../shared/models/annotation.model';
import {BridgeService} from '../services/bridge.service';
import {FrameSyncService} from '../services/frame-sync.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { IHypothesisSearchController } from '../models/hypothesis-app.model';
import { Group } from '../models/groups.model';
import {GroupsService} from '../services/groups.service';
import {ScopeService} from '../services/scope.service';
import { Component, Input, Output, OnInit, OnDestroy, ChangeDetectorRef, EventEmitter } from '@angular/core';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { SessionService } from '../services/session.service';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

interface IdAndNameObject {
  id: string;
  name: string;
}

interface SimpleItem extends IdAndNameObject {}

interface SimpleGroup extends IdAndNameObject {}

interface SimpleAuthor extends IdAndNameObject {}

interface FilterObject {
  id: string;
  name: string;
  type: string;
}

/**
 * @name leosFilterPane
 * @description Displays a filter pane in the sidebar.
 */
@Component({
  selector: 'filter-pane',
  templateUrl: '../templates/filter-pane.component.html',
  // styleUrls: ['../styles/components/filter-pane.scss', '../styles/leos-filter-pane.scss']
})
export class FilterPaneComponent implements OnInit, OnDestroy {
  private GROUP_TYPE = 'Group=';
  private AUTHORS_TYPE = 'Authors=';
  private TYPE_TYPE = 'Type=';
  private STATUS_TYPE = 'Status=';
  private CUSTOM_TEXT_TYPE = 'Custom=';
  private FILTER_SEPARATOR = ', ';
  private GROUP_FILTER_PREFIX = 'group:';
  private AUTHOR_FILTER_PREFIX = 'user_name:';
  private TYPE_FILTER_PREFIX = 'tag:';
  private STATUS_FILTER_PREFIX = 'status:';
  private FEEDBACK_FILTER_PREFIX = 'feedback:';
  private QUOTE = '"';

  public selectedFilters: any[];
  public selectedGroups: Group[];
  public selectedAuthors: SimpleAuthor[];
  public authorsList?: SimpleAuthor[];
  public groupsList?: SimpleGroup[];
  public customText: string;
  public type?: string;
  public status?: string;
  public leosFilterPaneVisible : boolean;

  readonly isThemeClean : boolean;

  private settings: IHypothesisJsonConfig;
  private storeUnsubscriber: VoidFunction[];

  public filterByGroup : boolean;
  public filterByAuthor: boolean;
  public filterByText: boolean;
  public filtersChanged: boolean;

  public filterObjectList: FilterObject[];
  public selectedGroupsAndAuthors: FilterObject[];

  /*
   *Bindings
   */
  @Input()
  public searchController?: IHypothesisSearchController;

  @Input()
  public pendingUpdatesCount?: number;

  @Output()
  public applyPendingUpdates: EventEmitter<void>;

  constructor(private changeDetectorRef: ChangeDetectorRef,
    private bridge: BridgeService,
    private frameSync: FrameSyncService,
    private groups: GroupsService,
    private rootScope: ScopeService,
    private sessionService: SessionService,
    settingsService: HypothesisConfigurationService,
    private store: HypothesisStoreService) {
        this.settings = settingsService.getConfiguration();
        this.applyPendingUpdates = new EventEmitter();
        this.storeUnsubscriber = [];

        this.isThemeClean = this.settings.theme === 'clean';
        this.leosFilterPaneVisible = false;

        this.selectedFilters = [];
        this.selectedGroups = [];
        this.selectedAuthors = [];
        this.authorsList = undefined;
        this.groupsList = undefined;
        this.customText = '';
        this.type = undefined;
        this.status = undefined;

        this.filtersChanged = false;
        this.filterByGroup = false;
        this.filterByAuthor = false;
        this.filterByText = false;
        this.filterObjectList = [];
        this.selectedGroupsAndAuthors = [];
  }

  public ngOnInit() {
    const self = this;

    this.rootScope.$on(events.ANNOTATION_CREATED, function () {
      //No need to clear selected filters as this at most adds new authors or groups. Just need to reload authors and groups based on annotations
      self._normalizeAnnotationsGroups();
      self.loadFilters();
    });

    this.storeUnsubscriber.push(this.store.selection.subscribe(() => {
      if (!self.filtersChanged) {
        self.loadFilters();
      }
      self.filtersChanged = false;
    }));

    this.storeUnsubscriber.push(this.store.annotation.subscribe(() => {
      self._reloadAuthorsList();
    }));
    this.rootScope.$on(events.ANNOTATION_DELETED, () => self._reloadAuthorsList());
    this.rootScope.$on(events.ANNOTATIONS_DELETED, () => self._reloadAuthorsList());
    this.rootScope.$on(events.LEOS_RESET_FILTER, () => self.clearFilters());
    this.onFilterInit();
  }

  public ngOnDestroy(): void {
      this.storeUnsubscriber.forEach((unsubscribe: VoidFunction) => unsubscribe());
  }

  get isLeosFilterPaneVisible(): boolean {
    return this.leosFilterPaneVisible;
  }

  onFilterInit () {
    this._resetFilters();
    this._doSearch();
  };

  onTypeSelect(event: Event) {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();

    const type: string = !event.target ? '' : (event.target as any)['value'] || '';
    var filterTypeIndex = this._filterTypeSelected(type);
    this._removeSelectedFilterType(this.TYPE_TYPE);
    this._addSelectedFilter(this.TYPE_TYPE, type, filterTypeIndex);
    this._doSearch();
  };

  onStatusSelect(event: Event) {
    const status: string = !event.target ? '' : (event.target as any)['value'] || '';
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
    if (this.isShowStatusFilter) {
      var filterStatusIndex = this._filterTypeSelected(status);
      this._removeSelectedFilterType(this.STATUS_TYPE);
      this._addSelectedFilter(this.STATUS_TYPE, status, filterStatusIndex);
    }
    this._doSearch();
  };

  afterSelectItem(item: FilterObject) {
      this.store.selection.deselectAllAnnotations();
      this.frameSync.deselectAllAnnotations();

      if(item.type === this.AUTHORS_TYPE) {
        this._addSelectedFilter(this.AUTHORS_TYPE, item.name);
      }
      if(item.type === this.GROUP_TYPE) {
        this._addSelectedFilter(this.GROUP_TYPE, item.name);
      }
      if(item.type === "TEXT"){
        this._addSelectedFilter(this.CUSTOM_TEXT_TYPE, item.name);
      }
      this._doSearch();
  }

  afterRemoveItem(item : FilterObject) {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
    this._removeSelectedFilterValue(item.type, item.name);
    this._doSearch();
  }

  onCustomTextInput(event: Event) {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
    this._removeSelectedFilterValue(this.CUSTOM_TEXT_TYPE);

    if(this.customText !== '') {
      this._addSelectedFilter(this.CUSTOM_TEXT_TYPE, this.customText);
    }
    this._doSearch();
  };

  toggleAdvancedFilter(){
    this.leosFilterPaneVisible = !this.leosFilterPaneVisible;

    if(this.leosFilterPaneVisible) {
      if(this.status === undefined) {
        this.status = 'Non-Processed';
        if (this.isShowStatusFilter) {
          this._addSelectedFilter(this.STATUS_TYPE, this.status, -1);
        }
      }
      if(this.type === undefined) {
        this.type = 'All';
        this._addSelectedFilter(this.TYPE_TYPE, this.type, -1);
      }
    }
  }


  get isShowFeedbackFilter(): boolean {
    return `${this.settings.feedbackActive}` == "true";
  }

  get isShowAllWithFeedbackFilter(): boolean {
    if (!this.isShowFeedbackFilter) return false;
    return isISCAuthority(this.settings);
  }

  get isClearButtonDisabled(): boolean {
    return !!this.searchController?.filterReseted();
  };

  get isShowStatusFilter(): boolean {
    return `${this.settings.showStatusFilter}` == "true"
  }

  clearFilters () {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
    this.store.selection.clearSearchFilter();
    this._resetFilters();
    this._doSearch();
    this.changeDetectorRef.detectChanges();
  };

  get multipleSelectPlaceholder(): string {
    return `Search by ${this.getSelectedFilterText()}`;
  }

  get filterButtonLabel() : string {
    return `By ${this.getSelectedFilterText()}`;
  }

  private getSelectedFilterText(): string {
    if(!this.filterByAuthor && ! this.filterByGroup && !this.filterByText) {
      return  "...";
    }

    const selectedFilters: string[] = [];
    if(this.filterByGroup) {
      selectedFilters.push("Group");
    }
    if(this.filterByAuthor) {
      selectedFilters.push("Author");
    }
    if(this.filterByText) {
      selectedFilters.push("Text");
    }   
    return selectedFilters.join(', ');
  }

  /******************
   * PRIVATE METHODS
   ******************/
  private _buildGroupId (group : string): string {
    return this.GROUP_FILTER_PREFIX + group;
  };

  private _buildGroupName (group: string): string {
    return group.replace(this.rootScope.getProperty('ANNOTATION_GROUP_SPACE_REPLACE_TOKEN'),' ');
  };

  private _filterTypeSelected(type: string): number {
    return this.selectedFilters.findIndex((element: string) => element.includes(type));
  };

  private _addSelectedFilter(type: string, item: string, addAtIndex: number = 0) {
    var filterTypeIndex = this._filterTypeSelected(type);
    if(filterTypeIndex > -1) {
      this.selectedFilters[filterTypeIndex] += this.FILTER_SEPARATOR + item;
    } else {
      this.selectedFilters.splice(addAtIndex, 0, type + item);
    }
  };

  private _removeSelectedFilterValue(type: string, item?: string) {
    var filterTypeIndex = this._filterTypeSelected(type);
    if (filterTypeIndex > -1) {
      var filter = this.selectedFilters[filterTypeIndex];
      if(filter.indexOf(this.FILTER_SEPARATOR) > -1 && filter.indexOf(this.FILTER_SEPARATOR) < filter.indexOf(item)) { //item is NOT THE FIRST among the same items of its type
        this.selectedFilters[filterTypeIndex] = filter.replace(this.FILTER_SEPARATOR + item, '');
      } else if(filter.indexOf(this.FILTER_SEPARATOR, filter.indexOf(item)) > -1) { //item is NOT THE LAST among the same items of its type
        this.selectedFilters[filterTypeIndex] = filter.replace(item + this.FILTER_SEPARATOR, '');
      } else {
        this.selectedFilters.splice(filterTypeIndex, 1);
      }
    }
  };

  /**
   * Reload authors list, if one of the 'deleted' authors was present on selectedFilter, remove him
   */
  private _reloadAuthorsList () {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();

    //preserve the previous authors list before reloading
    var oldAuthors = this.authorsList === undefined ? [] : this.authorsList;
    this.loadFilters();

    const self = this;
    //get difference between both will give all authors that no longer have annotations present (should be removed from authors filter)
    var diff = oldAuthors.filter(function (value) { return !self.authorsList?.includes(value); });
    diff.forEach(function (value) {
      //remove author from summary list
      self._removeSelectedFilterValue(self.AUTHORS_TYPE, value.id);
      //remove author from selected authors list
      self.selectedGroupsAndAuthors = self.selectedGroupsAndAuthors.filter(function (value) { return self.authorsList?.includes(value) });
    });
    this._doSearch();
  };

  private _removeSelectedFilterType (type: string) {
    var filterTypeIndex: number = this._filterTypeSelected(type);
    if(filterTypeIndex > -1) {
      this.selectedFilters.splice(filterTypeIndex, 1);
    }
  };

  private _normalizeAnnotationsGroups () {
    const self = this;
    this.store.getState().annotations.forEach(function (item) {
      item.group = item.group?.replace(' ', self.rootScope.getProperty('ANNOTATION_GROUP_SPACE_REPLACE_TOKEN'));
    });
  };

  public loadFilters(): void {
    if (this.filterByText) {
      this.filterObjectList = this.filterObjectList.filter(a => a["type"] === "TEXT");
    }
    else {
      this.filterObjectList = [];
    }
    this.loadAuthorsFilter();
    this.loadGroupsFilter();

    this.filtersChanged = true;
    this.searchController?.setFilterReseted(this.isResetedFilter);
  }

  public loadGroupsFilter () {
    if (!this.filterByGroup) return;
    if(!this.filterByAuthor && !this.filterByText) {
      this.filterObjectList = [];
    }

    const self = this;
    if(this.filterByGroup) {
        this.groupsList = [];
        //take only the name of the group to build the group filter
        this.store.getState().annotations?.forEach((annotation: Annotation) => {
            //Skip Collaborators group as selecting it is the same as not having any groups filtered
            if(annotation.group === self.groups.defaultGroupId()){
              return;
            }
            var groupId : string = self._buildGroupId(annotation.group || '');
            var groupName : string = self._buildGroupName(annotation.group || '');
            var groupObj: FilterObject = {
              id:   groupId,
              name: groupName,
              type: self.GROUP_TYPE
            };
            if(self.filterObjectList.filter((group: Group) => (group.id === groupObj.id)).length === 0){
              self.filterObjectList.push(groupObj);
            }
        });
    }
  }

  private _authorToFilterObject(author: string[]): FilterObject {
    return {
      id: this.AUTHOR_FILTER_PREFIX + this.QUOTE + author[0] + this.QUOTE,
      name: author[1],
      type: this.AUTHORS_TYPE
    };
  }


  private _toAuthorMap(annotation : Annotation): string[] {
    return [annotation.user_info?.display_name || '', annotation.user_info?.display_name || ''];
  }

  private _toAuthorObject(author: string[]): SimpleAuthor {
    return {id: this.AUTHOR_FILTER_PREFIX + this.QUOTE + author[0] + this.QUOTE, name: author[1]};
  }

  loadAuthorsFilter () {
    if(!this.filterByAuthor) return;
    if (!this.filterByGroup && !this.filterByText) {
      this.filterObjectList = [];
    }

    const self = this;
    const annotations: Annotation[] =  this.store.getState().annotations || [];
    const filterObjects: FilterObject[] = annotations.map((annotation : Annotation) => self._toAuthorMap(annotation))
      .map((author: string[]) => self._authorToFilterObject(author))
      .reduce((authors: FilterObject[], value: FilterObject) => {
        if (!authors.some((author) => author.id === value.id)) {
          authors.push(value);
        }
        return authors;
      }, []);
    this.filterObjectList = this.filterObjectList.concat(filterObjects);
  };

  private _resetFilters () {
    this.selectedFilters = [];
    this.selectedGroups = [];
    this.selectedAuthors = [];
    this.selectedGroupsAndAuthors = [];
    this.filterByAuthor = false;
    this.filterByGroup = false;
    this.filterByText = false;
    this.customText = '';
    this.type = 'All';
    this.status = 'Non-Processed';
    this._addSelectedFilter(this.TYPE_TYPE, this.type, -1);
    if (this.isShowStatusFilter) {
      this._addSelectedFilter(this.STATUS_TYPE, this.status, -1);
    }
    this.bridge.call('LEOS_refreshAnnotationLinkLines');
  }

  get isResetedFilter(): boolean {
    if (this.type !== 'All') return false;
    if (this.status !== 'Non-Processed') return false;
    if (this.customText !== '') return false;
    if (this.filterByAuthor) return false;
    if (this.filterByGroup) return false;
    if (this.filterByText) return false;
    return this.selectedGroupsAndAuthors.length == 0;
  }

  private _doSearch () {
    var searchQuery = '';
    //handle By Type
    if (this.type === 'AllWithProcessed') {
        searchQuery += ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.DELETED + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.REJECTED + ' '
            + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.ACCEPTED + ' '
            + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.NORMAL + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.TREATED;
    } else if (this.type === 'Comments') {
      searchQuery += ' ' + this.TYPE_FILTER_PREFIX + 'comment';
    } else if (this.type === 'Suggestions') {
      searchQuery += ' ' + this.TYPE_FILTER_PREFIX + 'suggestion';
    }
    if (this.type === 'AnnotationsWithFeedback') {
        searchQuery += ' ' + this.FEEDBACK_FILTER_PREFIX + 'all';
    }
    if (this.isShowStatusFilter) {
      if (this.status === 'Processed') {
        searchQuery += ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.DELETED + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.REJECTED + ' '
            + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.ACCEPTED + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.TREATED;
      } else if (this.status === 'Non-Processed') {
        searchQuery += ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.NORMAL;
      } else if (this.status === 'All') {
        searchQuery += ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.DELETED + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.REJECTED + ' '
            + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.ACCEPTED + ' '
            + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.NORMAL + ' ' + this.STATUS_FILTER_PREFIX + ANNOTATION_STATUS.TREATED;
      }
    }
    //handle By Group and Author
    this.selectedGroupsAndAuthors.forEach(function (filterObject: FilterObject) {
      searchQuery += ' ' + filterObject.id;
    });
    //handle Custom
    if (this.customText !== undefined && this.customText !== '') {
      searchQuery += ' ' + this.customText;
    }
    //do search
    this.searchController?.update(searchQuery);
    this.bridge.call('LEOS_refreshAnnotationLinkLines');
    this.bridge.call('LEOS_refreshAnnotationHighlights', this.store.getState().annotations);
    this.searchController?.setFilterReseted(this.isResetedFilter);
  }

  get annotateVersion() : string | undefined {
    return this.sessionService.state.annotateVersion;
  }

  public toggleDropdown(dropdown: NgbDropdown): void {
    if (dropdown.isOpen()) {
        dropdown.close();
    } else {
        dropdown.open();
    }
  }

  public onApplyPendingUpdates(): void {
    this.applyPendingUpdates.emit();
  }
}
'use strict';

import * as UI_CONSTANTS from '../ui-constants';

import * as annotationMetadata from '../annotation-metadata';
import * as authorityChecker from '../authority-checker';
import * as authorizer from '../authorizer';
import { AnalyticsService } from '../services/analytics.service';
import { AnnotationMapperService } from '../services/annotation-mapper.service';
import { FlashService } from '../services/flash.service';
import { FrameSyncService } from '../services/frame-sync.service';
import { SessionService } from '../services/session.service';
import { SuggestionMergerService } from '../services/suggestion-merger.service';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { Annotation, Suggestion } from '../../../../../shared/models/annotation.model';
import { LeosPermissionsService } from '../services/permissions.service';
import { Thread } from '../../../../../shared/models/thread.model';
import $ from '../imports/jquery';
import { 
  Component, 
  Input,
  ElementRef
} from '@angular/core';
import { mergeMap, map, catchError, throwError, Observable, of } from 'rxjs';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'selection-pane',
  templateUrl: '../templates/selection-pane.component.html',
  // styleUrls: ['../styles/components/selection-pane.scss', '../styles/leos-selection-pane.scss']
})
export class SelectionPaneComponent {
  readonly TAB_ANNOTATIONS = UI_CONSTANTS.TAB_ANNOTATIONS;
  readonly TAB_NOTES = UI_CONSTANTS.TAB_NOTES;
  readonly TAB_ORPHANS = UI_CONSTANTS.TAB_ORPHANS;

  //Bindings
  @Input()
  public filterActive? : boolean;

  @Input()
  public filterMatchCount? : number;

  @Input()
  public selectedTab? : string;

  @Input()
  public thread? : Thread;

  public setSortKey: (sortKey: string) => void;

  private isActionPending: boolean;
  private $element: JQuery<HTMLElement>;
  private settings: IHypothesisJsonConfig;
  private $window: Window;

  constructor(elementRef: ElementRef,
    private api: ApiService,
    private analytics: AnalyticsService,
    private annotationMapper: AnnotationMapperService,
    private flash: FlashService,
    private frameSync: FrameSyncService,
    private permissions: LeosPermissionsService,
    private session: SessionService,
    settingsService: HypothesisConfigurationService,
    private store: HypothesisStoreService,
    private suggestionMerger: SuggestionMergerService){
      this.setSortKey = (sortKey: string) => store.selection.setSortKey(sortKey);
      this.$window = window;
      this.settings = settingsService.getConfiguration();
      this.isActionPending = false;
      this.$element = $(elementRef.nativeElement);
  }

  deselectAllWidth(): string {
    const width: number = this.$element[0].querySelector('.leos-selection-pane__flexbox')?.lastElementChild?.getBoundingClientRect().width || 0;
    return `${width}px`;
  }

  accept () {
    if (this.isAcceptAndRejectDisabled) return;
    const self = this;
    const selectedSuggestions = this.getMergableSelectedAnnotations();
    const selectedSuggestionsCount = selectedSuggestions.length;
    let message = 'Are you sure you want to accept the ' + selectedSuggestionsCount + ' selected suggestion';
    if (selectedSuggestionsCount !== 1) message += 's';
    message += '?';

    if (this.$window.confirm(message)) {
      this.isActionPending = true;

      this.sendUserPermissions().subscribe(() => {
        self.suggestionMerger.processSuggestionMergings(selectedSuggestions).pipe(
          mergeMap((suggestions: Suggestion[]) => {
            return self.annotationMapper.acceptSuggestions(suggestions);
          }),
          map((suggestions: Suggestion[]) => {
            return self.logAnnotationDeletionToAnalytics(suggestions);
          }),
          catchError((err: any) => {
            return throwError(() => err);
          })
        ).subscribe({
          next(suggestions: Suggestion[]) {
            const notMergedSuggestionsCount = selectedSuggestions.length - suggestions.length;
            const mergedSuggestionsCount = suggestions.length;
            if (notMergedSuggestionsCount === 0) {
              const title = self.getSuggestionsMergedText(mergedSuggestionsCount);
              self.flash.success(title);
            } else if (mergedSuggestionsCount === 0) {
              const title = self.getSuggestionsNotMergedText(notMergedSuggestionsCount);
              self.flash.error(title);
            } else {
              const title = self.getSuggestionsMergedText(mergedSuggestionsCount);
              const message = self.getSuggestionsNotMergedText(notMergedSuggestionsCount);
              self.flash.warning(message, title);
            }
          },
          error(err: any) {
            self.flashActionFailure(err, 'Accepting', true, selectedSuggestionsCount)
          },
          complete() {
            self.isActionPending = false;
          }
        });
      })
    }
  }

  getSuggestionsMergedText(count: number): string {
    let text = count + ' Suggestion';
    if (count !== 1) text += 's';
    text += ' accepted';
    return text;
  }

  getSuggestionsNotMergedText(count: number): string {
    let text = count + ' Suggestion';
    if (count !== 1) text += 's';
    text += ' not accepted';
    return text;
  }

  delete() {
    if (this.isDeleteDisabled) return;
    const selectedAnnotations = this.getDeletableSelectedAnnotations();
    const selectedAnnotationsCount = selectedAnnotations.length;
    let message = 'Are you sure you want to delete the ' + selectedAnnotationsCount + ' selected deletable annotation';
    if (selectedAnnotationsCount !== 1) message += 's';
    message += '?';
    if (this.$window.confirm(message)) {
      this.isActionPending = true;
      const self = this;

      this.sendUserPermissions().subscribe(() => {
        self.annotationMapper.deleteAnnotations(selectedAnnotations).subscribe({
          next(annotations: Annotation[]) {
            let title = 'Annotation';
            if (annotations.length !== 1) title += 's';
            title += ' deleted';
            self.flash.success(title);
          },
          error(error: any) {
            self.flashActionFailure(error, 'Deleting', false, selectedAnnotationsCount)
          },
          complete() {
            self.isActionPending = false
          }
        });
      })
    }
  }

  reject() {
    if (this.isAcceptAndRejectDisabled) return;
    const selectedSuggestions = this.getMergableSelectedAnnotations();
    const selectedSuggestionsCount = selectedSuggestions.length;
    let message = 'Are you sure you want to reject the ' + selectedSuggestionsCount + ' selected suggestion';
    if (selectedSuggestionsCount !== 1) message += 's';
    message += '?';
    if (this.$window.confirm(message)) {
      const self = this;
      this.isActionPending = true;

      this.sendUserPermissions().subscribe(() => {
        self.annotationMapper.rejectSuggestions(selectedSuggestions).subscribe({
          next(suggestions: Suggestion[]) {
            let title = 'Suggestion';
            if (suggestions.length !== 1) title += 's';
            title += ' rejected';
            self.flash.success(title);
          },
          error(error: any) {
            self.flashActionFailure(error, 'Rejecting', true, selectedSuggestionsCount);
          },
          complete() {
            self.isActionPending = false
          }
        })
      })
    }
  }

  logAnnotationDeletionToAnalytics(annotations: Annotation[]): Annotation[] {
    annotations.forEach((annotation: Annotation) => {
      let event: string;
      if (annotationMetadata.isHighlight(annotation)) {
        event = this.analytics.events.HIGHLIGHT_DELETED;
      } else if (annotationMetadata.isPageNote(annotation)) {
        event = this.analytics.events.PAGE_NOTE_DELETED;
      } else if (annotationMetadata.isReply(annotation)) {
        event = this.analytics.events.REPLY_DELETED;
      } else {
        event = this.analytics.events.ANNOTATION_DELETED;
      }
      this.analytics.track(event);
    });
    return annotations;
  }

  flashActionFailure(error: Error, action: string, isSuggestion: boolean, annotationCount: number) {
    let title = action + ' ';
    if (isSuggestion)
      title += 'suggestion';
    else
      title += 'annotation';
    if (annotationCount !== 1) title += 's';
    title += ' failed';
    const message = error.message ? error.message : error;
    this.flash.error(message, title);
  }

  deselectAll() {
    this.store.selection.deselectAllAnnotations();
    this.frameSync.deselectAllAnnotations();
  }

  /* Conditions to display extended actions: Accept, Reject, Mark as Processed and Mark as Not Processed */
  get areExtendedActionsShown (): boolean {
    return !authorityChecker.isISC(this.settings);
  }

  get isAcceptAndRejectDisabled(): boolean {
    return this.getMergableSelectedAnnotations().length === 0 || this.isActionPending;
  };

  deleteTitleText (): string {
    return "Delete the selected annotations";
  };

  getMergableSelectedAnnotations(): Annotation[] {
    return this.store.selection.getSelectedAnnotations()
      .filter((annotation: Annotation) => !annotation.$leosDeleted)
      .filter((annotation: Annotation) => !annotation.$selectorMoved)
      .filter((annotation: Annotation) => authorizer.canMergeSuggestion(annotation, this.permissions, this.settings));
  }

  get isTreatDisabled(): boolean {
	  return this.getTreatableSelectedAnnotations().length === 0 || this.isActionPending;
  };

  getTreatableSelectedAnnotations(): Annotation[] {
    return this.store.selection.getSelectedAnnotations()
        .filter((annotation: Annotation) => authorizer.canTreatAnnotation(annotation, this.permissions, this.settings, this.session.state.userid || ''));
  };

  treat () {
    if (this.isTreatDisabled) return;
	  const selectedAnnotations = this.getTreatableSelectedAnnotations();
    const selectedAnnotationsCount = selectedAnnotations.length;
    let message = 'Are you sure you want to mark ' + selectedAnnotationsCount + ' selected annotation';
    if (selectedAnnotationsCount !== 1) message += 's';
    message += ' as processed?';
    if (this.$window.confirm(message)) {
      const self = this;
      this.isActionPending = true;

      this.sendUserPermissions().subscribe(() => {
        self.annotationMapper.treatAnnotations(selectedAnnotations).subscribe({
          next(annotations: Annotation[]) {
            let title = annotations.length + ' Annotation';
            if (annotations.length !== 1) title += 's';
            title += ' marked as processed';
            self.flash.success(title);
          },
          error(error: any) {
            self.flashActionFailure(error, 'Marking as Processed', false, selectedAnnotationsCount);
          },
          complete() {
            self.isActionPending = false;
          }
        })
      })
    }
  }

  get isResetDisabled(): boolean {
	  return this.getResettableSelectedAnnotations().length === 0|| this.isActionPending;
  }

  getResettableSelectedAnnotations(): Annotation[] {
    return this.store.selection.getSelectedAnnotations()
        .filter((annotation: Annotation) => authorizer.canResetAnnotation(annotation, this.permissions, this.settings));
  }

  reset() {
    if (this.isResetDisabled) return;
	  const selectedAnnotations = this.getResettableSelectedAnnotations();
    const selectedAnnotationsCount = selectedAnnotations.length;
    let message = 'Are you sure you want to mark ' + selectedAnnotationsCount + ' selected annotation';
    if (selectedAnnotationsCount !== 1) message += 's';
    message += ' as not processed?';
    if (this.$window.confirm(message)) {
      const self = this;
      this.isActionPending = true;

      this.sendUserPermissions().subscribe(() => {
        self.annotationMapper.resetAnnotations(selectedAnnotations).pipe(
          map((annotations: Annotation[]) => self.logAnnotationDeletionToAnalytics(annotations))
        ).subscribe({
          next(annotations: Annotation[]) {
            let title = annotations.length + ' Annotation'
            if (annotations.length !== 1) title += 's';
            title += ' marked as not processed';
            self.flash.success(title);
          },
          error(error: any) {
            self.flashActionFailure(error, 'Marking as Not Processed', false, selectedAnnotationsCount);
          },
          complete() {
            self.isActionPending = false
          }
        })
      })
    }
  }

  get isDeleteDisabled (): boolean {
    return this.getDeletableSelectedAnnotations().length === 0
      || this.isActionPending;
  }

  getDeletableSelectedAnnotations(): Annotation[] {
    return this.store.selection.getSelectedAnnotations()
      .filter((annotation: Annotation) => authorizer.canDeleteAnnotation(annotation, this.permissions, this.settings, this.session.state.userid || ''));
  }

  get isDeselectAllDisabled(): boolean {
    return !this.store.selection.hasSelectedAnnotations();
  }

  get isSelectAllDisabled(): boolean {
    const annotationCount: number = (this.selectedTab !== this.TAB_NOTES) ? this.store.annotation.getVisibleAnnotationCount()
        : this.store.annotation.getVisibleNotesCount();
    const selectedAnnotationCount: number = (this.selectedTab !== this.TAB_NOTES) ? this.store.selection.getSelectedAnnotationCount()
        : this.store.selection.getSelectedNotesCount();
    return annotationCount === 0 || (selectedAnnotationCount === annotationCount);
  }

  selectAllFilteredActionableAnnotations () {
    const annotationsToSelect = this.thread?.children.map(threadChild => threadChild.annotation)
      .filter((annotation?: Annotation) => (annotation != undefined)) as Annotation[];
    this.store.selection.selectAnnotations(annotationsToSelect.map((annotation: Annotation) => (annotation.id || '')));
    this.frameSync.LEOS_selectAnnotations(annotationsToSelect);
  }

  getAllFilteredActionableAnnotations(): Annotation[] {
    const allFilteredActionableAnnotations = this.thread?.children.map(threadChild => threadChild.annotation)
      .filter((annotation?: Annotation) => {
        if (!annotation) return false;
        return authorizer.canMergeSuggestion(annotation, this.permissions, this.settings)
          || authorizer.canDeleteAnnotation(annotation, this.permissions, this.settings, this.session?.state?.userid || '')
          || authorizer.canTreatAnnotation(annotation, this.permissions, this.settings, this.session.state.userid || '')
          || authorizer.canResetAnnotation(annotation, this.permissions, this.settings);
      }) as Annotation[];
    return allFilteredActionableAnnotations;
  }

  get selectedAnnotationCount (): number {
    return this.store.selection.getSelectedAnnotationCount();
  }

  toggleDropdown(dropdown: NgbDropdown): void {
    if (dropdown.isOpen()) {
      dropdown.close();
    } else {
      dropdown.open();
    }
  }

  get isSelectionPaneShown(): boolean {
    return !this.isSelectAllDisabled || !this.isDeselectAllDisabled;
  }

  get sortKey(): string {
    return this.store.getState().sortKey;
  }

  get sortKeysAvailable (): string[] {
    return this.store.getState().sortKeysAvailable;
  }

  onChangeSortKey(event: { sortKey: string }) {
    this.setSortKey(event.sortKey);
  }

  private sendUserPermissions(): Observable<void> {
    return this.api.permissions.update({}, { permissions: this.permissions.getUserPermissions() }).pipe(
      mergeMap(() => of(undefined)),
      catchError(() => of(undefined))
    )
  }
}
import { Input, Output, EventEmitter, Component } from "@angular/core";
import { Annotation } from "../../../../../shared/models/annotation.model";
import { DraftState } from "../services/drafts.service";
import { HypothesisStoreService } from "../store";
import { LeosPermissionsService } from "../services/permissions.service";
import { SessionService } from "../services/session.service";
import { HypothesisConfigurationService } from "../services/hypothesis-configuration.service";
import { FeaturesService } from "../services/features.service";
import { IHypothesisConfiguration } from "../../../../../shared/models/config.model";
import * as annotationMetadata from '../annotation-metadata';
import * as authorityChecker from '../authority-checker';
import * as authorizer from '../authorizer';
import * as OPERATION_MODES from '../../../../../shared/operationMode';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs";

@Component({
  selector: 'annotation-actions-menu-button',
  templateUrl: '../templates/annotation-actions-menu-button.component.html',
})
export class AnnotationActionsMenuButtonComponent {
    @Input()
    public annotation?: Annotation;

    @Input()
    public isEditing?: boolean;

    @Input()
    public isSaving?: boolean;

    @Input()
    public isHighlight?: boolean;

    @Input()
    public isForward?: boolean;

    @Input()
    public showRejectAction?: boolean;

    @Input()
    public state?: DraftState;

    @Input()
    public checkUnsavedFeedbackAndWarn?: Observable<void>;

    @Output()
    public deleteAnnotation : EventEmitter<void>;

    @Output()
    public treatAnnotation : EventEmitter<void>;

    @Output()
    public replyAnnotation : EventEmitter<void>;

    @Output()
    public forwardAnnotation : EventEmitter<void>;

    @Output()
    public editAnnotation : EventEmitter<void>;

    @Output()
    public addFeedback : EventEmitter<void>;

    @Output()
    public rejectAnnotation: EventEmitter<void>;

    public isRootSuggestion?: boolean;

    private $window: Window;
    private settings: IHypothesisConfiguration;

    constructor(private store: HypothesisStoreService,
        private permissions: LeosPermissionsService,
        private session: SessionService,
        private features: FeaturesService,
        configurationService: HypothesisConfigurationService) {
            this.settings = configurationService.getConfiguration();
            this.$window = window;
            this.deleteAnnotation = new EventEmitter();
            this.treatAnnotation = new EventEmitter();
            this.replyAnnotation = new EventEmitter();
            this.forwardAnnotation = new EventEmitter();
            this.editAnnotation = new EventEmitter();
            this.addFeedback = new EventEmitter();
            this.rejectAnnotation = new EventEmitter();
    }

    get isMenuButtonShown(): boolean {
        return !this.isEditing;
    }

    get hasAllowedActions(): boolean {
        if (this.isEditShown) return true;
        if (this.isDeleteShown) return true;
        if (this.isMarkAsTreatedShown) return true;
        if (this.isReplyShown) return true;
        if (this.isForwardShown) return true;
        if (this.isMarkAsNotTreatedShown) return true;
        return this.isFeedbackShown;
    }

    toggleDropdown(dropdown: NgbDropdown): void {
        if (dropdown.isOpen()) {
            dropdown.close();
        } else {
            dropdown.open();
        }
    }

    closeDropdown(dropdown: NgbDropdown): void {
        if (dropdown.isOpen()) {
            dropdown.close();
        }
    }

    get isDeleteShown(): boolean {
        if (this.isSaving) return false;
        if (this.isProcessed) return false;
        if (!authorizer.canDeleteAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '', this.isRootSuggestion))
            return false;
        return !(authorizer.canMergeSuggestion(this.annotation || {}, this.permissions, this.settings) && this.deleteOrphanSuggestion);
    }

    get isEditShown(): boolean {
        if (this.isSaving) return false;
        if (this.isProcessed) return false;
        return authorizer.canUpdateAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '');
    }

    get isReplyShown(): boolean {
        if (this.isSaving) return false;
        if (this.isProcessed) return false;
        if (this.state?.isPrivate) return false;

        return authorizer.canReplyToAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '', this.store);
    }

    get isForwardShown(): boolean {
        if (this.isSaving) return false;
        if (!this.features.flagEnabled('forward_annotations')) return false;
        if (authorityChecker.isISC(this.settings)) return false;
        if (!authorizer.canForwardAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '')) return false;
        if (this.state?.isPrivate) return false;
        if (this.isProcessed) return false;
        return !this.isForward;
    }

    get isSuggestion(): boolean {
        return annotationMetadata.isSuggestion(this.state || {});
    }

    get isOrphan(): boolean {
        if (typeof this.annotation?.$orphan === 'undefined') {
          return this.annotation?.$anchorTimeout;
        }
        return !!this.annotation?.$orphan;
    }

    get isMarkAsTreatedShown(): boolean {
        if(!authorizer.canTreatAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '')) {
          return false;
        };
        return !this.isReply
            && !this.isDocumentNote
            && !this.isISC
            && !this.wasDeleted
            && !this.isAcceptedOrRejected;
    }

    get isMarkAsNotTreatedShown(): boolean {
        if (authorizer.canTreatAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '')) return false;
        if (!authorizer.canResetAnnotation(this.annotation || {}, this.permissions, this.settings)) return false;
        return annotationMetadata.isTreated(this.annotation || {});
    }

    get isProcessed(): boolean {
        return annotationMetadata.isProcessed(this.annotation || {});
    }

    get wasDeleted(): boolean {
        return annotationMetadata.isDeleted(this.annotation || {});
    }

    get isAcceptedOrRejected(): boolean {
        return annotationMetadata.isAcceptedOrRejected(this.annotation || {});
    }

    get isReply():boolean {
        return annotationMetadata.isReply(this.annotation || {});
    }

    get isISC(): boolean {
        return authorityChecker.isISC(this.settings);
    }

    get isDocumentNote(): boolean {
        return annotationMetadata.isPageNote(this.annotation || {});
    }

    get isHasFeedback(): boolean {
        return annotationMetadata.hasFeedback(this.annotation || {});
    }

    get isFeedbackShown(): boolean {
        if (this.isSaving) return false;
        if (`${this.settings.feedbackActive}` != "true") return false;
        if (!this.features.flagEnabled("add_feedback")) return false;
        if (!this.annotation?.id) return false;
        if (this.isEditing) return false;
        if (this.isReply) return false;
        if (this.isHighlight) return false;
        if (this.isProcessed) return this.isHasFeedback;
        if (this.isISC) return false;
        if (this.annotation.uri?.includes('revision-')
            && !authorizer.canUpdateAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '')) {
            return false;
        }
        if(!authorizer.canFeedbackAnnotation(this.annotation || {}, this.permissions, this.settings, this.session.state.userid || '')) {
            return false;
        }

        return (this.isHasFeedback ? false : this.settings.operationMode !== OPERATION_MODES.READ_ONLY && this.settings.operationMode !== OPERATION_MODES.STORED_READ_ONLY);
    }

    get deleteOrphanSuggestion (): boolean {
        var isOrphanTab = this.store.getState().selectedTab === 'orphan';
        if (!isOrphanTab) {
          return this.isSuggestion;
        } else {
          return false;
        }
    }

    onDeleteAnnotation(): void {
        this.deleteAnnotation.emit();
    }

    onTreatAnnotation(): void {
        this.treatAnnotation.emit();
    }

    onReplyAnnotation(): void {
        this.replyAnnotation.emit();
    }

    onForwardAnnotation(): void {
        this.forwardAnnotation.emit();
    }

    onEditAnnotation(): void {
        this.editAnnotation.emit();
    }

    onAddFeedback(): void {
        this.addFeedback.emit();
    }

    onRejectAnnotation(): void {
        this.rejectAnnotation.emit();
    }
}
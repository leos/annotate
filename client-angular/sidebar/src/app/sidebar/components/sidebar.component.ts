'use strict';

import scrollIntoView from '../imports/scroll-into-view';
import * as events from '../events';

import { serviceConfig } from '../service-config';
import * as bridgeEvents from '../../../../../shared/bridge-events';
import { HypothesisServiceConfig, IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { HypothesisAuthState, IHypothesisSearchController } from '../models/hypothesis-app.model';
import {AnalyticsService} from '../services/analytics.service';
import {IOAuthAuthService, LeosOAuthAuthService} from '../services/oauth-auth.service';
import {BridgeService} from '../services/bridge.service';
import {DraftsService,  DraftModel } from '../services/drafts.service';
import {FlashService} from '../services/flash.service';
import {FrameSyncService} from '../services/frame-sync.service';
import {ScopeService} from '../services/scope.service';
import {SessionService} from '../services/session.service';
import {ServiceUrlService} from '../services/service-url.service';
import {StreamerService} from '../services/streamer.services';
import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { Observable, catchError, from, map, throwError } from 'rxjs';
import {HypothesisStoreService} from '../store';
import {HypothesisConfigurationService} from '../services/hypothesis-configuration.service';
import { DefaultFilterSearchController } from '../util/filter-search';
import $ from '../imports/jquery';


interface AppDialogState {
  visible: boolean;
}

interface Coordinate {
  id: string;
  x: number;
  y: number;
}

@Component({
  selector: 'sidebar',
  templateUrl: '../templates/sidebar.component.html',
  // styleUrls: ['../styles/components/sidebar.scss', '../styles/leos-sidebar.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  /** 
   * Information about the current user's authentication status.
   * When the controller instantiates we do not yet know if the user is
   * logged-in or not, so it has an initial status of 'unknown'. This can be
   * used by templates to show an intermediate or loading state.
   */
  @Input()
  public auth?: HypothesisAuthState;

  /** 
   * Check to see if we're in the sidebar, or on a standalone page such as
   * the stream page or an individual annotation page.
   */
  public isSidebar : boolean;
  public shareDialog : AppDialogState;
  public helpPanel : AppDialogState;
  private _isSidebarOpen: boolean;

  readonly search: IHypothesisSearchController;
  private authService: IOAuthAuthService;
  public setSortKey: Function;

  private scrollIntoViewTimeout?: number;
  private settings: IHypothesisJsonConfig;
  private $document: JQuery<Document>;
  private $window: Window;

  constructor (
    private analytics: AnalyticsService, 
    private store: HypothesisStoreService, 
    auth: LeosOAuthAuthService, 
    private bridge: BridgeService, 
    private drafts: DraftsService, 
    private flash: FlashService, 
    private frameSync: FrameSyncService,
    private rootScope: ScopeService,
    private serviceUrl: ServiceUrlService, 
    private session: SessionService, 
    settingsService: HypothesisConfigurationService, 
    private streamer: StreamerService) {
      this.$window = window;
      this.$document = $(this.$window.document);
      this.settings = settingsService.getConfiguration();
      this.authService = auth;
      this.scrollIntoViewTimeout = undefined;
      this.setSortKey = (event: {sortKey: string}) => store.selection.setSortKey(event.sortKey);

      // App dialogs
      this.shareDialog = {visible: false};
      this.helpPanel = {visible: false};  
      this.isSidebar = (this.$window.top !== this.$window);
      this.search = new DefaultFilterSearchController(store);
      this._isSidebarOpen = false;
  }

  public ngOnInit(): void {
    //LEOS Change : set global annotation group tokenizer token
    if(this.rootScope.getProperty('ANNOTATION_GROUP_SPACE_REPLACE_TOKEN') == undefined) {
      this.rootScope.storeProperty('ANNOTATION_GROUP_SPACE_REPLACE_TOKEN', "#");
    }

    const self = this;
    //LEOS Change : sync canvas
    this.rootScope.$on('LEOS_syncCanvas', (event: any, iFrameOffsetLeft: number, delayResp: number) => {
      setTimeout(() => {
        let coordinates: Coordinate[] = [];
        let annotationElems: Element[] = Array.prototype.slice.call(document.querySelectorAll('.is-suggestion'));
        annotationElems = annotationElems.concat(Array.prototype.slice.call(document.querySelectorAll('.is-comment')));
        annotationElems = annotationElems.concat(Array.prototype.slice.call(document.querySelectorAll('.is-highlight')));
        annotationElems.forEach(function (annotationElem: Element) {
          let left = iFrameOffsetLeft + 20;
          let top = annotationElem.getBoundingClientRect().top + 50;

          coordinates.push({id:annotationElem.id,x:left,y:top});
        });

        self.bridge.call('LEOS_syncCanvasResp', coordinates);
        
      }, delayResp || 0);
    });

    if (this.isSidebar) {
      this.frameSync.connect();
    }

    this.rootScope.$on('sidebarOpened', () => {
        self._isSidebarOpen = true;
    });

    this.rootScope.$on('sidebarClosed', () => {
        self._isSidebarOpen = false;
    });
  }

  get isSidebarOpen(): boolean {
    return this._isSidebarOpen;
  }

  public ngOnDestroy(): void {
      if (this.scrollIntoViewTimeout) window.clearTimeout(this.scrollIntoViewTimeout);
  }

  get sortKey(): string {
    return this.store.getState().sortKey;
  }

  get sortKeysAvailable (): string[] {
    return this.store.getState().sortKeysAvailable;
  };

  get showSidebarToggleBtn(): boolean {
    return `${this.settings.showSidebarToggleBtn}` == "true";
  }

  /** Scroll to the view to the element matching the given selector */
  scrollToView(selector: string) {
    const self = this;
    // Add a timeout so that if the element has just been shown (eg. via ngIf)
    // it is added to the DOM before we try to locate and scroll to it.
    this.scrollIntoViewTimeout = window.setTimeout(() => {
      scrollIntoView(self.$document[0].querySelector(selector));
    });
  }

  /**
   * Start the login flow. This will present the user with the login dialog.
   *
   * @return {Observable<void>} - A Promise that resolves when the login flow
   *   completes. For non-OAuth logins, always resolves immediately.
   */
  login(): Observable<void> {
    if (serviceConfig(this.settings)) {
      // Let the host page handle the login request
      this.bridge.call(bridgeEvents.LOGIN_REQUESTED);
      return from(Promise.resolve());
    }

    const self = this;
    return this.authService.login().pipe(
      map(() => {
        self.session.reload();
        return;
      }),
      catchError((err: Error) => {
        self.flash.error(err.message);
        return throwError(() => {});
      })      
    )
  };

  signUp() {
    this.analytics.track(this.analytics.events.SIGN_UP_REQUESTED);

    if (serviceConfig(this.settings)) {
      // Let the host page handle the signup request
      this.bridge.call(bridgeEvents.SIGNUP_REQUESTED);
      return;
    }
    this.$window.open(this.serviceUrl.get('signup', {}));
  };

  /** Display the dialog for sharing the current page */ 
  share () {
    this.shareDialog.visible = true;
    this.scrollToView('share-dialog');
  };

  showHelpPanel() {
    var service = serviceConfig(this.settings) || {} as HypothesisServiceConfig;
    if (service.onHelpRequestProvided) {
      // Let the host page handle the help request.
      this.bridge.call(bridgeEvents.HELP_REQUESTED);
      return;
    }

    this.helpPanel.visible = true;
  };

  // Prompt to discard any unsaved drafts.
  promptToLogout () : boolean {
    // TODO - Replace this with a UI which doesn't look terrible.
    var text = '';
    if (this.drafts.count() === 1) {
      text = 'You have an unsaved annotation.\n' +
        'Do you really want to discard this draft?';
    } else if (this.drafts.count() > 1) {
      text = 'You have ' + this.drafts.count() + ' unsaved annotations.\n' +
        'Do you really want to discard these drafts?';
    }
    return (this.drafts.count() === 0 || this.$window.confirm(text));
  };

  // Log the user out.
  logout (): void {
    if (!this.promptToLogout()) {
      return;
    }
    const self = this;
    this.drafts.unsaved().forEach((draft: DraftModel) => {
      self.rootScope.$broadcast(events.ANNOTATION_DELETED, draft);
    });
    this.drafts.discard();

    if (serviceConfig(this.settings)) {
      // Let the host page handle the signup request
      this.bridge.call(bridgeEvents.LOGOUT_REQUESTED);
      return;
    }

    this.session.logout();
  }

  countPendingUpdates (): number { 
    return this.streamer.countPendingUpdates() 
  }

  get pendingUpdatesCount(): number {
    return this.countPendingUpdates();
  }

  applyPendingUpdates () { 
    this.streamer.applyPendingUpdates() 
  }

  onShareDialogClose() {
    this.shareDialog.visible = false
  }

  onHelpPanelClose() {
    this.helpPanel.visible = false;
  }
}
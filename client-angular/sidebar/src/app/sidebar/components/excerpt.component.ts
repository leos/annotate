'use strict';

import { ContentStyle, ExcerptOverflowMonitor, ExcerptState, IExcerptOverflowMonitor } from '../util/excerpt-overflow-monitor.util';
import {ScopeService} from '../services/scope.service';
import { 
  Component, 
  Input, 
  Output, 
  EventEmitter, 
  OnInit, 
  OnDestroy, 
  OnChanges,
  AfterViewInit,
  SimpleChanges, 
  ElementRef, 
  ChangeDetectorRef 
} from '@angular/core';

interface BottomShadowStyles {
  'excerpt__shadow': boolean;
  'excerpt__shadow--transparent': boolean;
  'is-hidden': boolean;
}

@Component({
  selector: 'excerpt',
  templateUrl: '../templates/excerpt.component.html',
  // styleUrls: ['../styles/components/excerpt.scss']
})
export class ExcerptComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {
  /** Whether or not expansion should be animated. Defaults to true. */
  @Input()
  public animate? : boolean;

  /**
   * The data which is used to generate the excerpt's content.
   * When this changes, the excerpt will recompute whether the content
   * is overflowing.
   */
  @Input()  
  public contentData? : any;

  /**
   * Specifies whether controls to expand and collapse
   * the excerpt should be shown inside the <excerpt> component.
   * If false, external controls can expand/collapse the excerpt by
   * setting the 'collapse' property.
   */
  @Input()
  public inlineControls? : boolean;

  /** 
   * The height of this container in pixels when collapsed.
   */
  @Input()
  public collapsedHeight? : number;

  /**
   * The number of pixels by which the height of the excerpt's content
   * must extend beyond the collapsed height in order for truncation to
   * be activated. This prevents the 'More' link from being shown to expand
   * the excerpt if it has only been truncated by a very small amount, such
   * that expanding the excerpt would reveal no extra lines of text.
   */
  @Input()
  public overflowHysteresis? : number;
  
  /** Whether or not truncation should be enabled */
  @Input()
  public enabled? : boolean;

  /** Sets whether or not the excerpt is collapsed. */
  @Input()  
  public collapse? : boolean;

  /**
   * Called when the collapsibility of the excerpt (that is, whether or
   * not the content height exceeds the collapsed height), changes.
   *
   * Note: This function is *not* called from inside a digest cycle,
   * the caller is responsible for triggering any necessary digests.
   */
  @Output()  
  public onCollapsibleChanged: EventEmitter<{collapsible: boolean}>;

  readonly contentStyle: () => ContentStyle;

  private overflowMonitor : IExcerptOverflowMonitor;
  private overflowing : boolean;
  private $element: HTMLElement;

  constructor (elementRef: ElementRef<HTMLElement>,
      private changeDetectorRef: ChangeDetectorRef) {
        this.collapse = true;
        this.$element = elementRef.nativeElement;
        this.animate = (this.animate === undefined);
        this.enabled = (this.enabled === undefined);
        this.overflowing = false;
        this.onCollapsibleChanged = new EventEmitter();

        const self = this;
        this.contentStyle = (): ContentStyle => self.overflowMonitor.contentStyle();

        this.overflowMonitor = new ExcerptOverflowMonitor({
          getState(): ExcerptState {
            return {
              enabled: self.enabled,
              animate: self.animate,
              collapsedHeight: self.collapsedHeight,
              collapse: self.collapse,
              overflowHysteresis: self.overflowHysteresis,
            };
          },
          contentHeight(): number {
            if (!self.$element) {
              return 0;
            }
            return self.$element.scrollHeight;
          },
          onOverflowChanged(overflowing: boolean) {
            self.overflowing = overflowing;
            self.onCollapsibleChanged.emit({collapsible: overflowing});
            // Even though this change happens outside the framework, we use
            // $digest() rather than $apply() here to avoid a large number of full
            // digest cycles if many excerpts update their overflow state at the
            // same time. The onCollapsibleChanged() handler, if any, is
            // responsible for triggering any necessary digests in parent scopes.
            self.changeDetectorRef.detectChanges();
          },
        }, (callback) => window.requestAnimationFrame(callback));
  }

  public ngAfterViewInit(): void {    
    this.overflowMonitor.check();
  }

  public ngOnInit() {
    // Trigger an initial calculation of the overflow state.
    //
    // This is performed asynchronously so that the content of the <excerpt>
    // has settled - ie. all Angular directives have been fully applied and
    // the DOM has stopped changing. This may take several $digest cycles.
    this.overflowMonitor.check();
  }

  public onResize(event: Event) {
    // Listen for document events which might affect whether the excerpt
    // is overflowing, even if its content has not changed.
    this.overflowMonitor.check();
  }

  public ngOnDestroy() {
    const self = this;
    //window.removeEventListener('resize', () => self.overflowMonitor.check());
  }
  
  public ngOnChanges(changes: SimpleChanges) {
      // Watch input properties which may affect the overflow state
      if (changes['enabled'] != undefined) return this.overflowMonitor.check(); 
      if (changes['contentData'] != undefined) return this.overflowMonitor.check();

      // Watch for changes to the visibility of the excerpt.
      // Unfortunately there is no DOM API for this, so we rely on a digest
      // being triggered after the visibility changes.      
      if (!!changes['isElementHidden']) return this.overflowMonitor.check();
  }

  get isExpandable (): boolean {
    return this.overflowing && !!this.collapse;
  }

  get isCollapsible (): boolean {
    return this.overflowing && !this.collapse;
  }

  toggle (event: Event) {
    // When the user clicks a link explicitly to toggle the collapsed state,
    // the event is not propagated.
    event.stopPropagation();
    this.collapse = !!this.collapse;
  }

  expand () {
    // When the user expands the excerpt 'implicitly' by clicking at the bottom
    // of the collapsed excerpt, the event is allowed to propagate. For
    // annotation cards, this causes clicking on a quote to scroll the view to
    // the selected annotation.
    this.collapse = false;
  }

  get showInlineControls (): boolean {
    return this.overflowing && (this.inlineControls || false);
  }

  get bottomShadowStyles(): BottomShadowStyles {
    return {
      'excerpt__shadow': true,
      'excerpt__shadow--transparent': !!this.inlineControls,
      'is-hidden': !this.isExpandable,
    };
  };

  // Test if the element or any of its parents have been hidden by
  // an 'ng-show' directive
  get isElementHidden(): boolean {
    var el: HTMLElement | null = this.$element;
    while (el) {
      if (el.classList.contains('ng-hide')) {
        return true;
      }
      el = el.parentElement;
    }
    return false;
  }

}
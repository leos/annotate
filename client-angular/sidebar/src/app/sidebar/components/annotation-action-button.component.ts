'use strict';

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'annotation-action-button',
  templateUrl: '../templates/annotation-action-button.component.html',
  // styleUrls: ['../styles/components/tooltip.scss']
})
export class AnnotationActionButtonComponent {

  @Input()
  public icon?: string;

  @Input()
  public isDisabled?: boolean;

  @Input()
  public label?: string;

  @Output()
  public onClick: EventEmitter<Event>;

  constructor() {
    this.onClick = new EventEmitter();
  }

  public doClick(event: Event): void {
    event.stopPropagation();
    this.onClick.emit(event);
  }

  get cssSytle(): string {
    return `${this.icon || ''} btn-icon`;
  }
}
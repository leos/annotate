'use strict';

import $ from '../imports/jquery';
import { 
  Component, 
  Input, 
  Output, 
  EventEmitter, 
  ElementRef,
  OnChanges,
  SimpleChanges 
} from '@angular/core';
import {PendingRequestsService} from '../services/pending-requests.service';

@Component({
  selector: 'search-input',
  templateUrl: '../templates/search-input.component.html',
  // styleUrls: ['../styles/components/simple-search.scss']
})
export class SearchInputComponent implements OnChanges {
  // Specifies whether the search input field should always be expanded,
  // regardless of whether the it is focused or has an active query.
  //
  // If false, it is only expanded when focused or when 'query' is non-empty
  @Input()
  public alwaysExpanded?: boolean;

  @Input()
  public query?: any;

  @Output()
  public onSearch: EventEmitter<{$query: string}>;

  private button: JQuery<HTMLButtonElement>;
  private input: HTMLInputElement;
  private form: HTMLFormElement;
  private loading: boolean;

  constructor(elementRef: ElementRef,
    pendingRequestsService: PendingRequestsService) {
    const $element: JQuery<HTMLElement> = $(elementRef.nativeElement);
    this.button = $element.find('button') as JQuery<HTMLButtonElement>;
    this.input = $element.find('input')[0] as HTMLInputElement;
    this.form = $element.find('form')[0] as HTMLFormElement;
    this.loading = false;
    this.onSearch = new EventEmitter();

    const self = this;
    this.button.on('click', function () {
      self.input.focus();
    });

    if (this.form) {
      this.form.onsubmit = function (ev: SubmitEvent) {
        ev.preventDefault();
        self.onSearch.emit({$query: self.input.value});
      };
    }

    pendingRequestsService.getObservable().subscribe({
      next(pendingRequests: number) {
        self.loading = pendingRequests > 0;
      }
    });
  }

  get isLoading(): boolean {
    return this.loading;
  }

  get inputClasses(): any {
    return {'is-expanded': this.alwaysExpanded || this.query};
  };

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['query']) {
      this.input.value = changes['query'].currentValue;
    }
  };
}

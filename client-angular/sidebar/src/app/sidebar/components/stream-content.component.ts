'use strict';

import {AnnotationMapperService} from '../services/annotation-mapper.service';
import {ApiService} from '../services/api.service';
import {QueryParserService} from '../services/query-parser.service';
import {RootThreadService} from '../services/root-thread.service';
import {SearchFilterService} from '../services/search-filter.service';
import {StreamFilterService} from '../services/stream-filter.service';
import {StreamerService} from '../services/streamer.services';
import {ScopeService} from '../services/scope.service';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core'
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from "@angular/router";
import {HypothesisStoreService} from '../store';
import { Thread } from '../../../../../shared/models/thread.model';
import { tap, catchError, throwError } from 'rxjs';

@Component({
  selector: 'stream-content',
  templateUrl: '../templates/stream-content.component.html'
})
export class StreamContentComponent implements OnInit {
  private offset : number;

  public rootThread!: Thread;

  @Input()
  public search?: any;

  constructor(private changeDetectorRef: ChangeDetectorRef,
    private $location: Location,
    private router: Router,
    private $routeParams: ActivatedRoute, 
    private annotationMapper: AnnotationMapperService, 
    private store: HypothesisStoreService,
    private api: ApiService, 
    private queryParser: QueryParserService,
    private rootScope: ScopeService, 
    private rootThreadService: RootThreadService, 
    private searchFilter: SearchFilterService, 
    private streamFilter: StreamFilterService, 
    private streamer: StreamerService) {
      store.viewer.setAppIsSidebar(false);
      /** `offset` parameter for the next search API call. */
      this.offset = 0;
  }
  
  public ngOnInit(): void {
    const self = this;
    this.search.query = () => self.getQueryParameter('id') || '';
    this.search.update = (q: any) => self.router.navigate(['/search'], { queryParams: { q } });

    // Re-do search when query changes
    var lastQuery = this.getQueryParameter('q');
    this.rootScope.$on('$routeUpdate', function () {
      if (self.getQueryParameter('q') !== lastQuery) {
        self.store.annotation.clearAnnotations();
        self.$location.historyGo(0);
      }
    });
    // Set up updates from real-time API.
    this.streamFilter
      .resetFilter()
      .setMatchPolicyIncludeAll();

    var terms = this.searchFilter.generateFacetedFilter(this.getQueryParameter('q') || '');
    this.queryParser.populateFilter(this.streamFilter, terms);
    this.streamer.setConfig('filter', {filter: this.streamFilter.getFilter()});
    this.streamer.connect().subscribe(() => {});

    // Perform the initial search
    this.fetch(20);

    this.store.subscribe(() => {
      self.rootThread = self.rootThreadService.thread(this.store.getState());
    });

    // Sort the stream so that the newest annotations are at the top
    this.store.selection.setSortKey('Newest');
  }

  setCollapsed(event: {id: string, collapsed: boolean}) {
    this.store.selection.setCollapsed(event.id, event.collapsed);
  }

  forceVisible(event: {thread: Thread}) {
    if (!event.thread.annotation) return;
    this.store.selection.setForceVisible(event.thread.annotation.id || '', true);
  }

  loadMore(limit: number) {
    this.fetch(limit);
  }

  /** Load annotations fetched from the API into the app. */
  load (result: any) {
    this.offset += result.rows.length;
    this.annotationMapper.loadAnnotations(result.rows, result.replies);
  };

  /**
   * Fetch the next `limit` annotations starting from `offset` from the API.
   */
  fetch (limit : number) {
    const query = Object.assign({
      _separate_replies: true,
      offset: this.offset,
      limit: limit,
    }, this.searchFilter.toObject(this.getQueryParameter('q') || ''));

    const self = this;
    this.api.search(query).pipe(
      tap((result: any) => {
        self.load(result);
      }),
      catchError((err: any) => { 
        console.error(err);
        return throwError(() => new Error(err)); 
      })
    );
  }
  
  getQueryParameter(name: string): string | null {
    return this.$routeParams.snapshot.paramMap.get(name);
  }
}
'use strict';

import * as _ from 'lodash';
import CKEDITOR from "ckeditor4";
import * as commands from '../markdown-commands';
import * as mediaEmbedder from '../media-embedder';
import { renderMathAndMarkdown as renderMarkdown } from '../render-markdown';
import { NewStateFunction, EditorExitData } from '../models/markdown.model';
import $ from '../imports/jquery';
import {
  Component, 
  Input, 
  Output, 
  EventEmitter,
  ElementRef, 
  SecurityContext,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ScopeService } from '../services/scope.service';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
import { IHypothesisJsonConfig } from '../../../../../shared/models/config.model';
import { HypothesisConfigurationService } from '../services/hypothesis-configuration.service';

const debounce = _.debounce;

interface EditorConfigToolbar {
  name: string;
  items: string[];
}

interface EditorConfig {
  disableAutoInline?: boolean;
  removeButtons?: string;
  extraPlugins?: string;
  height?: string;
  startupFocus?: string;
  toolbar: EditorConfigToolbar[];
}

const SPELLCHECKER_TYPE = {
  qas: "qas",
  wsc: "wsc",
  disabled: "disabled"
}

function focusInput(input: HTMLTextAreaElement): number {
  // When the visibility of the editor changes, focus it.
  // A timeout is used so that focus() is not called until
  // the visibility change has been applied (by adding or removing
  // the relevant CSS classes)
  return window.setTimeout(() => input.focus(), 0);
}

@Component({
  selector: 'ckeditor-markdown',
  templateUrl: '../templates/ckeditor-markdown.component.html'
})
export class CkEditorMarkdownComponent implements OnInit, OnChanges, OnDestroy {
  @Output()
  public onEditText: EventEmitter<{text?: string}>;

  @Output()
  public onSetExitData: EventEmitter<EditorExitData>;

  @Output()
  public noBlurChange: EventEmitter<boolean>;

  @Input()
  public text?: string;

  @Input()
  public disableStyling?: boolean;

  @Input()
  public disableLink?: boolean;

  @Input()
  public customTextClass?: any;

  @Input()
  public readOnly?: boolean;

  @Input()
  public noBlur?: boolean;

  private input?: HTMLTextAreaElement;
  private output?: HTMLElement;
  public editorConfig: EditorConfig;
  // Create lost focus unsave warning 
  public initialText?: string;
  private menuShown: boolean;
  private preview: boolean;
  private timeouts: number[];
  private editor?: CKEDITOR.editor;
  private componentId: string;
  private $element: JQuery<HTMLElement>;
  private prevShowEditorValue?: boolean;

  public previewText?: string;
  private handleInputChange: _.DebouncedFunc<() => void>;

  private settings: IHypothesisJsonConfig;

  constructor(private elementRef: ElementRef<HTMLElement>,
    private changeDetectorRef: ChangeDetectorRef,
    private domSanitizer: DomSanitizer,
    private settingsService: HypothesisConfigurationService,
    private rootScope: ScopeService) {
      this.componentId = window.crypto.randomUUID();
      this.$element = $(elementRef.nativeElement);

      this.timeouts = [];
      this.initialText = '';
      this.menuShown = false;
      this.prevShowEditorValue = undefined;
      this.onEditText = new EventEmitter();
      this.onSetExitData = new EventEmitter();
      this.noBlurChange = new EventEmitter(); 
      this.settings = settingsService.getConfiguration();
      this.editorConfig = {
        disableAutoInline: true,
        extraPlugins: 'autolink,specialchar',
        height: '8em',
        startupFocus: 'end',
        toolbar: [
          { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
          { name: 'insert', items: [ 'SpecialChar' ]},
          { name: 'links', items: [ 'Link', 'Unlink' ] }
        ],
      }
      
      const spellCheckerName = this.settings.spellChecker?.name;
      if (spellCheckerName === SPELLCHECKER_TYPE.qas) {
        this.addExternalSpellCheckerPlugin().then(() => { console.debug("[ANNOT] Added external spellchecker plugin to CKEditor!!!"); });
      }
      
      const self = this;
      // Keyboard shortcuts for bold, italic, and link.
      this.$element.on('keydown', function(e: JQuery.KeyDownEvent) {
        var shortcuts: any = {
          66: self.insertBold.bind(self),
          73: self.insertItalic.bind(self),
          75: self.insertLink.bind(self),
        };

        var shortcut = shortcuts[e.keyCode];
        if (shortcut && (e.ctrlKey || e.metaKey)) {
          e.preventDefault();
          shortcut();
        }
      });

      this.preview = false;
      
      this.handleInputChange = debounce(function () {
        self.rootScope.$apply(changeDetectorRef, () => {
          self.onEditText.emit({text: self.editor?.getData()});
        })
      }, 100);
  }

  public ngOnInit() {
    this.input = this.$element.find('textarea')[0] as HTMLTextAreaElement;
    this.output = this.elementRef.nativeElement.querySelector('.js-markdown-preview') || undefined;

    if (!!this.disableStyling && !!this.disableLink) {
      this.editorConfig.removeButtons = 'Bold,Italic,Link,Unlink';
    }
    else if (!!this.disableStyling) {
      this.editorConfig.removeButtons = 'Bold,Italic';
    }
    else if (!!this.disableLink) {
      this.editorConfig.removeButtons = 'Link,Unlink';
    }

    const self = this;
    // Adding event handler to react on spell checker changes
    window.document.addEventListener('AnnotateSpellCheckerReplace', function(data) {
        // Event will be triggered for all editors.The editor that has hasFocus == true is the current used editor.
        if (self.editor?.editable() === undefined) {
            return;
        }
        if (self.editor?.editable().hasFocus !== true) {
            return;
        }
        self.handleInputChange();
    });

    window.CKEDITOR.on('dialogDefinition', (event: any) => {
      const dialogName = event.data['name'];
      const dialogDefinition = event.data['definition'];
      const dialog = dialogDefinition.dialog;

      if (dialogName === 'link') {
        // Set "New Windows (_blank)" as default target option.
        const targetTab = dialogDefinition.getContents('target');
        const targetField = targetTab.get('linkTargetType');
        targetField['default'] = '_blank';

        // Hide tab row.
        dialog.parts.tabs.$.style.display = 'none';
        const dialogContents = dialog.parts.dialog.$.querySelector('.cke_dialog_contents');
        dialogContents.style['margin-top'] = 0;
        dialogContents.style['border-top'] = 'none';

        dialogDefinition.dialog.on('show', function () {
          // Make height adjust automaticaly.
          const dialogContentsBody = dialog.parts.dialog.$.querySelector('.cke_dialog_contents_body');
          dialogContentsBody.style.height = 'auto';

          // Hide link type field.
          const linkTypeFieldRow = dialog.parts.dialog.$.querySelector('div > table tr:nth-child(2)');
          linkTypeFieldRow.style.display = 'none';

          // Hide protocol field.
          const protocolFieldCell = dialog.parts.dialog.$.querySelector('div > table tr:nth-child(3) tr tr td:first-child');
          protocolFieldCell.style.display = 'none';
        });

        // Disable resizing dialog window.
        dialogDefinition.resizable = window.CKEDITOR.DIALOG_RESIZE_NONE;
      }
    }); 
  }

  public ngOnChanges(changes: SimpleChanges): void {
    // Re-render the markdown when the view needs updating.
    if (changes['text'] != undefined) {
      const self = this;
      const sanitize = (value: string | null): string => self.domSanitizer.sanitize(SecurityContext.HTML, value) || '';
      this.previewText = !!(this.disableStyling) ? sanitize(this.text || '') : renderMarkdown(this.text || '', sanitize);
      if (this.output) {
        if (!this.disableLink && this.output) {
          mediaEmbedder.replaceLinksWithEmbeds(this.output);
        }
      }
    }
    // Exit preview mode when leaving edit mode
    if (changes['readOnly'] != undefined) {
      this.preview = false;
    }

    if (this.showEditor === this.prevShowEditorValue) return;
    this.prevShowEditorValue = this.showEditor;
    this.initialText = this.text || '';
    if (!this.showEditor) {
        this.onSetExitData.emit({id: this.componentId, focused: true, remove: true});
    }
  }

  public onEditorReady(event: CKEditor4.EventInfo): void {
    this.editor = event.editor;
    this.noBlurChange.emit(false);
    const element: HTMLElement | null = document.getElementById('cke_' + event.editor.name);
    // Somehow, an on event listener can not be directly added to the editor instance. Therefore, the editor is retrieved anew.
    if (element != null) element.onclick = function(event) { event.stopPropagation(); };
  }

  public onEditorFocus(): void {
    this.noBlurChange.emit(false);
    this.onSetExitData.emit({
      id: this.componentId, 
      initialText: this.initialText, 
      focused: true,
      editor: this.input || undefined,
      time:(new Date).getTime()
    });
  }

  public onEditorBlur(): void {
    if (this.noBlur) {
      this.noBlurChange.emit(false); 
    } else {
      if (!this.onSetExitData) return;
      this.onSetExitData.emit({
        id: this.componentId,
        initialText: this.initialText,
        text: this.text,
        logicPlus: !this.menuShown,
        focused: false
      });
    }
  }

  public onEditorPaste(event: CKEditor4.EventInfo): void {
    var pastedText = event.data['dataValue'];
    // ANOT-314: only keep characters 9 (tab), 10, 11, 13 (line breaks), but drop control characters
    event.data['dataValue'] = pastedText.replace(/[\u0001-\u0008]/g, '')
                                     .replace(/[\u000E-\u001F]/g, '')
                                     .replace(/\u000C/g, '');
  }

  public onEditorChange(event: CKEditor4.EventInfo): void {
    this.handleInputChange();
  }

  public ngOnDestroy() {
    for(const timout of this.timeouts) {
      clearTimeout(timout);
    }
  }

  insertBold() {
    if (!this.disableStyling) {
      this.updateState(function (state) {
        return commands.toggleSpanStyle(state, '**', '**', 'Bold');
      });
    }
  };

  insertItalic() {
    if (!this.disableStyling) {
      this.updateState(function (state) {
        return commands.toggleSpanStyle(state, '*', '*', 'Italic');
      });
    }
  };

  insertMath() {
    this.updateState(function (state: any) {
      var before = state.text.slice(0, state.selectionStart);

      if (before.length === 0 ||
          before.slice(-1) === '\n' ||
          before.slice(-2) === '$$') {
        return commands.toggleSpanStyle(state, '$$', '$$', 'Insert LaTeX');
      } else {
        return commands.toggleSpanStyle(state, '\\(', '\\)',
                                            'Insert LaTeX');
      }
    });
  };

  insertLink() {
    if (!this.disableLink) {
      this.updateState(function (state: commands.EditorState) {
        return commands.convertSelectionToLink(state);
      });
    }
  };

  insertIMG() {
    this.updateState(function (state: commands.EditorState) {
      return commands.convertSelectionToLink(state,
        commands.LinkType.IMAGE_LINK);
    });
  };

  insertList() {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '* ');
    });
  };

  insertNumList() {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '1. ');
    });
  };

  insertQuote() {
    this.updateState(function (state: commands.EditorState) {
      return commands.toggleBlockStyle(state, '> ');
    });
  }

  togglePreview() {
    this.preview = !this.preview;
  };

  get showEditor(): boolean {
    return !this.readOnly && !this.preview;
  };

  /**
   * Transform the editor's input field with an editor command.
   */
  private updateState(newStateFn: NewStateFunction) {
      var newState: commands.EditorState = newStateFn({
        text: this.input?.value || '',
        selectionStart: this.input?.selectionStart || 0,
        selectionEnd: this.input?.selectionEnd || 0,
      });
      
      if (this.input) {
        this.input.value = newState.text || '';
        this.input.selectionStart = newState.selectionStart || 0;
        this.input.selectionEnd = newState.selectionEnd || 0;
    
        // The input field currently loses focus when the contents are
        // changed. This re-focuses the input field but really it should
        // happen automatically.
        this.input.focus();
      }
      this.onEditText.emit({text: this.input?.value});
  }

  async addExternalSpellCheckerPlugin() {
    const pluginName = 'spellchecker';
    const pluginUrl = this.settings.spellChecker?.sourceUrl || '';
    const externalPluginScript = document.getElementById("qas-spellchecker");
    if (externalPluginScript) {
      this.editorConfig.extraPlugins = this.editorConfig.extraPlugins + ',' + pluginName;
      window.CKEDITOR.plugins.addExternal(pluginName, pluginUrl, '');
    }
  }

}
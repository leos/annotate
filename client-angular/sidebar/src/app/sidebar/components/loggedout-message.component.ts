'use strict';

import {ServiceUrlService} from '../services/service-url.service';
import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'loggedout-message',
  templateUrl: '../templates/loggedout-message.component.html',
  // styleUrls: ['../styles/components/loggedout-message.scss']
})
export class LoggedoutMessageComponent {
  /**
   * Called when the user clicks on the "Log in" text.
   */
  @Output()
  public onLogin: EventEmitter<void>;

  constructor(readonly serviceUrl: ServiceUrlService) {
    this.onLogin = new EventEmitter();
  }

  public onLoginClick() {
    this.onLogin.emit();
  }
}
declare namespace CKEDITOR {
  interface CKEditorPluginsEditor {
    addExternal(name: string, path: string, fileName: string): void;
    get(name: string): string;
  }
}
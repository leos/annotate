'use strict';

import { AnnotationCompareFunction, BuildThreadOptions, ReplySortCompareFunction, SortCompareFunction, Thread, ThreadMapFunction } from '../../../../shared/models/thread.model'
import { isNormal as isAnnotationNormal } from './annotation-metadata';
import { Annotation } from '../../../../shared/models/annotation.model';
import {DELETED} from "./annotation-status";

/** Default state for new threads, before applying filters etc. */
var DEFAULT_THREAD_STATE: Thread = {
  id: undefined,
  annotation: undefined,
  parent: undefined,
  collapsed: false,
  visible: true,
  children: [],
  totalChildren: 0,
  highlightState: undefined,
};

/**
 * Returns a persistent identifier for an Annotation.
 * If the Annotation has been created on the server, it will have
 * an ID assigned, otherwise we fall back to the local-only '$tag'
 * property.
 */
function id (annotation: Annotation): string {
  return (annotation.id || annotation.$tag) || '';
}

/**
 * Link the annotation with ID `id` to its parent thread.
 *
 * @param {Object} threads - Providing list / items of ThreadState's. Map of annotation ID -> container
 * @param {string} id
 * @param {Array<string>} parents - IDs of parent annotations, from the
 *        annotation's `references` field.
 */
function setParentID (threads: any, id: string, parents: string[]) {
  if (threads[id].parent || !parents.length) {
    // Parent already assigned, do not try to change it.
    return;
  }
  var parentID: string = parents[parents.length-1];
  if (!threads[parentID]) {
    // Parent does not exist. This may be a reply to an annotation which has
    // been deleted. Create a placeholder Thread with no annotation to
    // represent the missing annotation.
    const newThread: Thread = Object.assign({}, DEFAULT_THREAD_STATE, {
      id: parentID,
      children: [],
    });
    threads[parentID] = newThread;
    setParentID(threads, parentID, parents.slice(0,-1));
  }

  var grandParentID: string = threads[parentID].parent;
  while (grandParentID) {
    if (grandParentID === id) {
      // There is a loop in the `references` field, abort.
      return;
    } else {
      grandParentID = threads[grandParentID].parent;
    }
  }

  threads[id].parent = parentID;
  threads[parentID].children.push(threads[id]);
}

/**
 * Creates a thread of annotations from a list of annotations.
 *
 * Given a flat list of annotations and replies, this generates a hierarchical
 * thread, using the `references` field of an annotation to link together
 * annotations and their replies. The `references` field is a possibly
 * incomplete ordered list of the parents of an annotation, from furthest to
 * nearest ancestor.
 *
 * @param {Array<Annotation>} annotations - The input annotations to thread.
 * @return {Thread} - The input annotations threaded into a tree structure.
 */
function threadAnnotations (annotations: Annotation[]): Thread {
  // Map of annotation ID -> container
  var threads: any = {};

  // Build mapping of annotation ID -> thread
  annotations.forEach(function (annotation: Annotation) {
    threads[id(annotation)] = Object.assign({}, DEFAULT_THREAD_STATE, {
      id: id(annotation),
      annotation: annotation,
      children: [],
    });
  });

  // Set each thread's parent based on the references field
  annotations.forEach(function (annotation) {
    if (!annotation.references) {
      return;
    }
    setParentID(threads, id(annotation), annotation.references);
  });

  // Collect the set of threads which have no parent as
  // children of the thread root
  var roots: Thread[] = [];
  Object.keys(threads).map((id) => (threads[id] as Thread)).forEach((thread: Thread) => {
    if (!thread.parent) {
      // Top-level threads are collapsed by default
      thread.collapsed = true;
      roots.push(thread);
    }
  });

  var root: Thread = {
    annotation: undefined,
    children: roots,
    visible: true,
    collapsed: false,
    totalChildren: roots.length,
  };

  return root;
}

/**
 * Returns a copy of `thread` with the thread
 * and each of its children transformed by mapFn(thread).
 *
 * @param {Thread} thread
 * @param {ThreadMapFunction} mapFn
 * @returns {Thread}
 */
function mapThread (thread: Thread, mapFn: ThreadMapFunction): Thread {
  return Object.assign({}, mapFn(thread), {
    children: thread.children.map(function (child: Thread) {
      return mapThread(child, mapFn);
    }),
  });
}

/**
 * Return a sorted copy of an array of threads.
 *
 * @param {Array<Thread>} threads - The list of threads to sort
 * @param {AnnotationCompareFunction} compareFn
 * @return {Array<Thread>} Sorted list of threads
 */
function sort(threads: Thread[], compareFn: AnnotationCompareFunction): Thread[] {
  return threads.slice().sort(function (a, b) {
    // Threads with no annotation always sort to the top
    if (!a.annotation || !b.annotation) {
      if (!a.annotation && !b.annotation) {
        return 0;
      } else {
        return !a.annotation ? -1 : 1;
      }
    }

    if (compareFn(a.annotation, b.annotation)) {
      return -1;
    } else if (compareFn(b.annotation, a.annotation)) {
      return 1;
    } else {
      return 0;
    }
  });
}

/**
 * Return a copy of `thread` with siblings of the top-level thread sorted according
 * to `compareFn` and replies sorted by `replyCompareFn`.
 */
function sortThread(thread: Thread, compareFn: SortCompareFunction, replyCompareFn: ReplySortCompareFunction): Thread {
  var children = thread.children.map(function (child: Thread) {
    return sortThread(child, replyCompareFn, replyCompareFn);
  });

  return Object.assign({}, thread, {
    children: sort(children, compareFn),
  });
}

/**
 * Return a copy of @p thread with the `replyCount` and `depth` properties
 * updated.
 */
function countRepliesAndDepth(thread: Thread, depth: number): Thread {
  var children = thread.children.map(function (c) {
    return countRepliesAndDepth(c, depth + 1);
  });
  return Object.assign({}, thread, {
    children: children,
    depth: depth,
    replyCount: children.reduce(function (total, child) {
      if (child.annotation && child.annotation.status && child.annotation.status.status && child.annotation.status.status != DELETED) {
        return total + 1 + (child.replyCount || 0);
      } else {
        return total + 0 + (child.replyCount || 0);
      }
    }, 0),
  });
}

/** Return true if a thread has any visible children. */
function hasVisibleChildren (thread: Thread): boolean {
  return thread.children.some(function (child) {
    return child.visible || hasVisibleChildren(child);
  });
}

/**
 * Default options for buildThread()
 */
const DEFAULT_BUILD_OPTS: BuildThreadOptions = {
  /**
   * List of IDs of annotations that should be shown even if they
   * do not match the current filter.
   */
  forceVisible: undefined,
  /**
   * Predicate function that returns true if an annotation should be
   * displayed.
   */
  filterFn: undefined,
  /**
   * A filter function which should return true if a given annotation and
   * its replies should be displayed.
   */
  threadFilterFn: undefined,
  /**
   * Mapping of annotation IDs to expansion states.
   */
  expanded: {},
  /** List of highlighted annotation IDs */
  highlighted: [],
  /**
   * Less-than comparison function used to compare annotations in order to sort
   * the top-level thread.
   */
  sortCompareFn: function (a, b) {
    return a.id < b.id;
  },
  /**
   * Less-than comparison function used to compare annotations in order to sort
   * replies.
   */
  replySortCompareFn: function (a, b) {
    return a.created < b.created;
  },
};

/**
 * Project, filter and sort a list of annotations into a thread structure for
 * display by the <annotation-thread> directive.
 *
 * buildThread() takes as inputs a flat list of annotations,
 * the current visibility filters and sort function and returns
 * the thread structure that should be rendered.
 *
 * @param {Array<Annotation>} annotations - A list of annotations and replies
 * @param {Options} buildOptions
 * @return {Thread} - The root thread, whose children are the top-level
 *                    annotations to display.
 */
export function buildThread(annotations: any[], buildOptions: BuildThreadOptions): Thread {
  const opts  = Object.assign({}, DEFAULT_BUILD_OPTS, buildOptions);

  var thread: Thread = threadAnnotations(annotations);

  // Mark annotations as visible or hidden depending on whether
  // they are being edited and whether they match the current filter
  // criteria
  var shouldShowThread = (annotation: any): boolean => {
    if ((opts.forceVisible && opts.forceVisible.indexOf(id(annotation)) !== -1) || !annotation.id) {
      // Also show annotations being created; Their id property is not defined yet.
      return true;
    }
    if (opts.filterFn) {
      return opts.filterFn(annotation);
    }
    return isAnnotationNormal(annotation);
  };

  // Set the visibility and highlight states of threads
  thread = mapThread(thread, function (thread) {
    const isVisible = thread.annotation ? (thread.visible && shouldShowThread(thread.annotation)) : false;

    if (thread.annotation) {
        thread.annotation.visible = isVisible;
        thread.annotation.showHighlight = isVisible;
    }

    var highlightState: string | null = null;
    if ((opts.highlighted != undefined) && opts.highlighted.length > 0) {
      var isHighlighted = thread.annotation &&
        opts.highlighted.indexOf(thread.id!) !== -1;
      highlightState = isHighlighted && isVisible ? 'highlight' : 'dim';
    }

    return Object.assign({}, thread, {
      highlightState: highlightState,
      visible: isVisible,
    });
  });

  // Expand any threads which:
  // 1) Have been explicitly expanded OR
  // 2) Have children matching the filter
  thread = mapThread(thread, function (thread: Thread) {
    let id: string = thread.id!;

    // If the thread was explicitly expanded or collapsed, respect that option
    if (Object.prototype.hasOwnProperty.call(opts.expanded, id)) {
      return Object.assign({}, thread, {collapsed: !opts.expanded[id]});
    }

    var hasUnfilteredChildren = opts.filterFn && hasVisibleChildren(thread);

    return Object.assign({}, thread, {
      collapsed: thread.collapsed &&
                 !hasUnfilteredChildren,
    });
  });

  // Remove top-level threads which contain no visible annotations
  thread.children = thread.children.filter(function (child) {
    return child.visible || hasVisibleChildren(child);
  });

  // Get annotations which are of type notes or annotations depending
  // on the filter.
  if (opts.threadFilterFn) {
    thread.children = thread.children.filter(opts.threadFilterFn);
  }

  // Sort the root thread according to the current search criteria
  thread = sortThread(thread, opts.sortCompareFn!, opts.replySortCompareFn!);

  // Update `replyCount` and `depth` properties
  thread = countRepliesAndDepth(thread, -1);
  return thread;
}

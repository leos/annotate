'use strict';

import { Thread } from '../../../../shared/models/thread.model';
import EventEmitter from './imports/event-emitter';
import * as _ from 'lodash';

const debounce = _.debounce;

/**
 * @typedef Options
 * @property {Function} [invisibleThreadFilter] - Function used to determine
 *   whether an off-screen thread should be rendered or not.  Called with a
 *   `Thread` and if it returns `true`, the thread is rendered even if offscreen.
 * @property {Element} [scrollRoot] - The scrollable Element which contains the
 *   thread list. The set of on-screen threads is determined based on the scroll
 *   position and height of this element.
 */
export type VirtualThreadOptions = {
  /**
   * Function used to determine
   * whether an off-screen thread should be rendered or not.  Called with a
   * `Thread` and if it returns `true`, the thread is rendered even if offscreen.
   */  
  invisibleThreadFilter?: Function,

  /**
   * The scrollable Element which contains the
   * thread list. The set of on-screen threads is determined based on the scroll
   * position and height of this element.
   */  
  scrollRoot?: Element,
}

export interface VirtualThreadListState {
  offscreenLowerHeight: number;
  offscreenUpperHeight: number;
  visibleThreads: Thread[];
  invisibleThreads: Thread[];
}

/**
 * VirtualThreadList is a helper for virtualizing the annotation thread list.
 *
 * 'Virtualizing' the thread list improves UI performance by only creating
 * annotation cards for annotations which are either in or near the viewport.
 *
 * Reducing the number of annotation cards that are actually created optimizes
 * the initial population of the list, since annotation cards are big components
 * that are expensive to create and consume a lot of memory. For Angular
 * applications this also helps significantly with UI responsiveness by limiting
 * the number of watchers (functions created by template expressions or
 * '$scope.$watch' calls) that have to be run on every '$scope.$digest()' cycle.
 */
export default class VirtualThreadList extends EventEmitter {
  private _rootThread?: Thread;
  private _options: VirtualThreadOptions;
  private _heights: Map<string, number>;
  private _detach: VoidFunction;
  readonly scrollRoot: Element;
  readonly window: Window;

  /**
   * @param {Window} container - The Window displaying the list of annotation threads.
   * @param {Thread} rootThread - The initial Thread object for the top-level
   *        threads.
   * @param {VirtualThreadOptions} options
   */
  constructor(window_: Window, rootThread: Thread | undefined, options: VirtualThreadOptions) {
    super();

    var self = this;

    this._rootThread = rootThread;

    this._options = Object.assign({}, options);

    // Cache of thread ID -> last-seen height
    this._heights = new Map();

    this.window = window_;
    this.scrollRoot = options.scrollRoot || document.body;

    var debouncedUpdate = debounce(function () {
      self._updateVisibleThreads();
    }, 20);
    this.scrollRoot.addEventListener('scroll', debouncedUpdate);
    this.window.addEventListener('resize', debouncedUpdate);

    this._detach = function () {
      self.scrollRoot.removeEventListener('scroll', debouncedUpdate);
      self.window.removeEventListener('resize', debouncedUpdate);
    };
  }

  /**
   * Detach event listeners and clear any pending timeouts.
   *
   * This should be invoked when the UI view presenting the virtual thread list
   * is torn down.
   */
  detach() {
    this._detach();
  }

  /**
   * Sets the root thread containing all conversations matching the current
   * filters.
   *
   * This should be called with the current Thread object whenever the set of
   * matching annotations changes.
   */
  setRootThread(thread: Thread) {
    if (thread === this._rootThread) {
      return;
    }
    this._rootThread = thread;
    this._updateVisibleThreads();
  }

  /**
   * Sets the actual height for a thread.
   *
   * When calculating the amount of space required for offscreen threads,
   * the actual or 'last-seen' height is used if known. Otherwise an estimate
   * is used.
   *
   * @param {string} id - The annotation ID or $tag
   * @param {number} height - The height of the annotation thread.
   */
  setThreadHeight(id: string, height: number) {
    if (isNaN(height) || height <= 0) {
      throw new Error(`Invalid thread height ${height}`);
    }
    this._heights.set(id, height);
  }

  private _height(id: string): number {
    // Default guess of the height required for a threads that have not been
    // measured
    var DEFAULT_HEIGHT = 200;
    return this._heights.has(id) ? this._heights.get(id)!: DEFAULT_HEIGHT;
  }

  /** Return the vertical offset of an annotation card from the top of the list. */
  yOffsetOf(id: string): number {
    var self = this;
    var allThreads: Thread[] = this._rootThread?.children || [];
    var matchIndex: number = allThreads.findIndex(function (thread) {
      return thread.id === id;
    });
    if (matchIndex === -1) {
      return 0;
    }
    return allThreads.slice(0, matchIndex).reduce(function (offset, thread) {
      return offset + self._height(thread.id || '');
    }, 0);
  }

  /**
   * Recalculates the set of visible threads and estimates of the amount of space
   * required for offscreen threads above and below the viewport.
   *
   * Emits a `changed` event with the recalculated set of visible threads.
   */
  private _updateVisibleThreads() {
    /** 
     * Space above the viewport in pixels which should be considered 'on-screen'
     * when calculating the set of visible threads
     */
    var MARGIN_ABOVE = 800;

    /** Same as MARGIN_ABOVE but for the space below the viewport */ 
    var MARGIN_BELOW = 800;

    /**  
     * Estimated height in pixels of annotation cards which are below the
     * viewport and not actually created. This is used to create an empty spacer
     * element below visible cards in order to give the list's scrollbar the
     * correct dimensions. 
     */
    var offscreenLowerHeight = 0;

    /** Same as offscreenLowerHeight but for cards above the viewport. */ 
    var offscreenUpperHeight = 0;

    /** 
     * List of annotations which are in or near the viewport and need to
     * actually be created.
     */ 
    var visibleThreads: Thread[] = [];

    /** 
     * List of annotations which are required to be rendered but we do not
     * want them visible. This is to ensure that we allow items to be rendered
     * and initialized (for saving purposes) without having them be presented
     * in out of context scenarios (i.e. in wrong order for sort)
     */
    var invisibleThreads: Thread[] = [];

    var allThreads: Thread[] = this._rootThread?.children || [];
    var visibleHeight: number = this.window.innerHeight;
    var usedHeight = 0;
    var thread: Thread;

    for (var i = 0; i < allThreads.length; i++) {
      thread = allThreads[i];
      var threadHeight = this._height(thread.id || '');

      var added = false;

      if (usedHeight + threadHeight < this.scrollRoot.scrollTop - MARGIN_ABOVE) {
        // Thread is above viewport
        offscreenUpperHeight += threadHeight;
      } else if (usedHeight <
        this.scrollRoot.scrollTop + visibleHeight + MARGIN_BELOW) {

        // Thread is either in or close to the viewport
        visibleThreads.push(thread);
        added = true;
      } else {

        // Thread is below viewport
        offscreenLowerHeight += threadHeight;
      }

      // any thread that is not going to go through the render process
      // because it is already outside of the viewport should be checked
      // to see if it needs to be added as an invisible render. So it will
      // be available to go through rendering but not visible to the user
      if(!added &&
          this._options.invisibleThreadFilter &&
          this._options.invisibleThreadFilter(thread)){
        invisibleThreads.push(thread);
      }

      usedHeight += threadHeight;
    }

    this.emit('changed', {
      offscreenLowerHeight: offscreenLowerHeight,
      offscreenUpperHeight: offscreenUpperHeight,
      visibleThreads: visibleThreads,
      invisibleThreads: invisibleThreads,
    });
  }
}
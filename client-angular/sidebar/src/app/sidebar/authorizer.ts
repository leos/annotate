'use strict';

import * as OPERATION_MODES from '../../../../shared/operationMode';
import * as annotationMetadata from './annotation-metadata';
import * as authorityChecker from './authority-checker';

import { buildAccountId } from './util/account-id.util';
import {  IHypothesisJsonConfig } from '../../../../shared/models/config.model';
import { Annotation } from '../../../../shared/models/annotation.model';
import { ILeosPermissionsService } from './services/permissions.service';
import { Permissions } from './services/permissions.service';

export function canTreatAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string) {
  if (annotationMetadata.isTreated(annotation) 
      || annotationMetadata.isAcceptedOrRejected(annotation)
      || annotationMetadata.isDeleted(annotation) 
      || !isActionAllowedOnOperationMode(annotation, settings, null)) {
    return false;
  }
  if (annotation.uri?.includes('revision-')
      && !permissions.permits(getPermissionsForAnnotation(annotation), 'delete', userId)
      && !permissions.permits(getPermissionsForAnnotation(annotation), 'update', userId)) {
    return false;
  }

  return permissions.getUserPermissions().includes('CAN_MARK_TREATED');
}

export function canResetAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig) {
  if (!annotationMetadata.isTreated(annotation) 
      || annotationMetadata.isAcceptedOrRejected(annotation)
      || annotationMetadata.isDeleted(annotation) 
      || !isActionAllowedOnOperationMode(annotation, settings, null)) {
    return false;
  }
  return true;
}

export function canDeleteAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string, isRootSuggestion: boolean = false) {
  if (!isActionAllowedOnOperationMode(annotation, settings, userId) || annotationMetadata.isProcessed(annotation)) {
    return false;
  }
  //Validate SENT annotations
  if (annotationMetadata.isSent(annotation)
    && (authorityChecker.isISC(settings) || (annotationMetadata.isReply(annotation) && isRootSuggestion))) {
    if (annotation.document?.metadata['responseId'] !== settings.connectedEntity) {
      // Annotation not from same group as connected unit, so no edit nor delete are allowed.
      return false;
    } else {
      return true;
    }
  }
  //Allow to delete annotations IN_PREPARATION from same DG
  if (canUpdateOrDeleteAnnotationInPreparation(annotation, settings)) {
    return true;
  }
  if (annotation.uri?.includes('revision-') && !permissions.permits(getPermissionsForAnnotation(annotation), 'delete', userId)) {
    return false;
  }
  if (permissions.getUserPermissions().includes('CAN_DELETE')) {
    return true;
  }
  return permissions.permits(getPermissionsForAnnotation(annotation), 'delete', userId);
}

export function canMergeSuggestion(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig):boolean  {
  if (!isActionAllowedOnOperationMode(annotation, settings, null) || annotationMetadata.isProcessed(annotation)) {
    return false;
  }

  return permissions.getUserPermissions().includes('CAN_MERGE_SUGGESTION')
    && annotationMetadata.isSuggestion(annotation)
    && !originalTextHasBeenModified(annotation);
}

export function originalTextHasBeenModified(annotation: Annotation): boolean {
  const leosSelector = annotation.target![0].selector?.find(selector => selector.type === 'LeosSelector')
  const originalAnchoredText = leosSelector !== undefined ? leosSelector.exact : undefined;
  const anchoredTextMatchesOriginalText = annotation.anchoredRangeText === originalAnchoredText;

  return !anchoredTextMatchesOriginalText;
}
export function canFeedbackAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string | null): boolean {
  if (!permissions.getUserPermissions().includes('CAN_COMMENT') && !permissions.getUserPermissions().includes('CAN_SUGGEST')) return false;
  return true;
}
export function canUpdateAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string | null): boolean {
  if (!isActionAllowedOnOperationMode(annotation, settings, userId) || annotationMetadata.isProcessed(annotation)) return false;

  const isAnnotationSent = annotationMetadata.isSent(annotation);
  const isIscUser = authorityChecker.isISC(settings);
  if (isAnnotationSent && isIscUser) {
      // If Annotation not from same group as connected unit, so no edit nor delete are allowed.
      return (annotation.document?.metadata['responseId'] === settings.connectedEntity);
  }
  //Allow to update annotations IN_PREPARATION is from same DG
  if (canUpdateOrDeleteAnnotationInPreparation(annotation, settings)) {
    return true;
  }
  if (permissions.permits(getPermissionsForAnnotation(annotation), 'update', userId || '')) return true;
  if (annotation.uri?.includes('revision-') && !permissions.permits(getPermissionsForAnnotation(annotation), 'update', userId || '')) return false;
  if (!permissions.getUserPermissions().includes('CAN_EDIT_ALL_ANNOTATIONS')) return false;
  return !annotationMetadata.isISC(annotation);
}

function isCoordinator(settings: IHypothesisJsonConfig, userId: string): boolean {
  if (!userId || !settings || !settings.coordinators) {
    return false;
  }
  let authority = authorityChecker.getAuthority(settings);
  for (let pos = 0 ; pos < settings.coordinators.length ; pos++) {
    if (userId === buildAccountId(settings.coordinators[pos], authority)) {
      return true;
    }
  }
  return false;
}

function isActionAllowedOnOperationMode(annotation: Annotation, settings: IHypothesisJsonConfig, userId: string | null, store: any = null): boolean {
  //In READ ONLY mode no action is allowed
  if (settings.operationMode === OPERATION_MODES.READ_ONLY || settings.operationMode === OPERATION_MODES.STORED_READ_ONLY) {
    return false;
  }
  //In PRIVATE mode user can only operate over contributions being IN_PREPARATION
  if (settings.operationMode === OPERATION_MODES.PRIVATE) {
    var basicRequirements = !annotationMetadata.isSent(annotation)
      && authorityChecker.isISC(settings)
      && !annotationMetadata.isProcessed(annotation)
      && ( userId === null || userId === annotation.user || isCoordinator(settings, userId));
    if (!basicRequirements) return false;

    if(annotationMetadata.isReply(annotation)) {
      // replying is only allowed on contributions, therefore we must check if the thread root
      // is a contribution
      var parentId : String = annotation.references ? annotation.references[0] : '';
      var rootAnnotation = store.findAnnotationByID(parentId);

      return rootAnnotation.document.metadata['responseId'] === settings.connectedEntity
        && annotationMetadata.isContributionInPreparation(rootAnnotation);
    } 

    // no reply
    return annotation.document?.metadata['responseId'] === settings.connectedEntity
      && annotationMetadata.isContributionInPreparation(annotation);
  }
  return true;
}

export function canReplyToAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string, store: any):boolean {
  if (!isActionAllowedOnOperationMode(annotation, settings, null, store)) {
    return false;
  }
  if (!permissions.getUserPermissions().includes('CAN_COMMENT') && !permissions.getUserPermissions().includes('CAN_SUGGEST')) return false;
  var isIsc = authorityChecker.isISC(settings);
  return !isIsc && !annotationMetadata.isSent(annotation)
       || isIsc && settings.operationMode === OPERATION_MODES.PRIVATE;
}

export function canForwardAnnotation(annotation: Annotation, permissions: ILeosPermissionsService, settings: IHypothesisJsonConfig, userId: string):boolean {
  if (!isActionAllowedOnOperationMode(annotation, settings, null)) {
    return false;
  }
  if (!permissions.getUserPermissions().includes('CAN_UPDATE')) return false;
  if (annotation.uri?.includes('revision-')
      && !permissions.permits(getPermissionsForAnnotation(annotation), 'delete', userId)
      && !permissions.permits(getPermissionsForAnnotation(annotation), 'update', userId)) {
    return false;
  }
  return !authorityChecker.isISC(settings) && !annotationMetadata.isSent(annotation);
}

function canUpdateOrDeleteAnnotationInPreparation(annotation: Annotation, settings: IHypothesisJsonConfig) {
  //Allow to update or delete annotations IN_PREPARATION from if same DG
  return !annotationMetadata.isSent(annotation) 
    && authorityChecker.isISC(settings) 
    && annotation.document?.metadata['responseId'] === settings.connectedEntity;
}

function getPermissionsForAnnotation(annotation: Annotation): Permissions {
  return {
    read: annotation.permissions?.read || [],
    update: annotation.permissions?.update || [],
    delete: annotation.permissions?.delete || [],
  }
} 
'use strict';

export enum AnnotationStatus {
  ACCEPTED = 'ACCEPTED',
  DELETED = 'DELETED',
  NORMAL = 'NORMAL',
  REJECTED = 'REJECTED',
  TREATED = 'TREATED',
}

export const ACCEPTED = AnnotationStatus.ACCEPTED;
export const DELETED = AnnotationStatus.DELETED;
export const NORMAL = AnnotationStatus.NORMAL;
export const REJECTED = AnnotationStatus.REJECTED;
export const TREATED = AnnotationStatus.TREATED;
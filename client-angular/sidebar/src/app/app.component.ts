import { Component, OnInit } from '@angular/core';
import { SessionService } from './sidebar/services/session.service';
import { HypothesisConfigurationService } from './sidebar/services/hypothesis-configuration.service';
import { IHypothesisJsonConfig } from '../../../shared/models/config.model';
import { HypothesisAuthState } from './sidebar/models/hypothesis-app.model';
import { ScopeService } from './sidebar/services/scope.service';
import { UserProfile } from './sidebar/models/session.model';
import { authStateFromUserProfile } from './sidebar/util/converter.util';
import * as events from './sidebar/events';

function _addConfigQAS(doc: Document, serviceUrlString: string) {
  if (!doc.getElementById('qas-config')) {
    var script = doc.createElement('script');
    script.id = "qas-config"
    script.innerHTML = `{
          window.SPELLCHECKER_CONFIG = {
              SUGGESTIONS_LIMIT: 5,
              LANGUAGE: "en-GB", // LEOS will supply the language based on the document
              API_URL: '${serviceUrlString}', 
              API_RESOURCE_PATH_CHECK: "/check", 
              API_RESOURCE_PATH_LANGUAGES: "/languages", 
              DISABLE_USER_CHOICE_LANGUAGE: false,// Disabled for cypress tests
              DISABLE_USER_CHOICE_PROOFREADING_CHECKS: false,
              isLeos: false
          };
      }`;
    doc.body.appendChild(script);
  }
}

function _addSpellCheckerConfig(doc: Document, serviceUrlString: string) {
  if (!serviceUrlString || serviceUrlString.length == 0) {
      return;
  }

  const serviceUrl = new URL(serviceUrlString);
  if (!doc.getElementById('spellChecker-config')) {
      var script = doc.createElement('script');
      script.id = "spellChecker-config";
      script.innerHTML = `
          function handleReplace(data, instance) {
              const replaceEvent = new CustomEvent('AnnotateSpellCheckerReplace', { detail: data } );
              window.document.dispatchEvent(replaceEvent);
          }

          function onStatistics(data, instance) {
              if (!data) {
                  return;
              }
              if (data.action !== 'replace') {
                  return;
              }
              handleReplace(data, instance);
          }

          window.WEBSPELLCHECKER_CONFIG = {
              autoSearch: true,
              autoDestroy: true,
              disableAutoSearchIn: [
                '#suggestion-editor',
                '#multiple-select-input',
                '#search-input'
              ],
              enableGrammar: true,
              disableDictionariesPreferences: true,
              lang: 'en_GB',
              serviceProtocol: '${serviceUrl.protocol.substring(0, serviceUrl.protocol.length - 1)}',
              serviceHost: '${serviceUrl.hostname}',
              servicePort: '${serviceUrl.port}',
              servicePath: '${serviceUrl.pathname}',
              onStatistics: onStatistics
          };
      `;
      doc.body.appendChild(script);
  }
}

function _addSpellCheckerScript(doc: Document, sourceUrl: string, name: string) {
  if (!sourceUrl || sourceUrl.length == 0) {
      return;
  }

  if (!doc.getElementById('spellChecker-service')) {
      var script = doc.createElement('script');
      script.id = name;
      script.addEventListener('error', _onErrorLoad, false);
      script.type = 'text/javascript';
      script.src = sourceUrl;
      script.async = true;
      doc.head.appendChild(script);
      function _onErrorLoad(event: any) {
          console.debug('Error occurred while loading spell checker script ', event);
          doc.head.removeChild(script);
      }
  }
}

/**
 * Return the user's authentication status from their profile.
 *
 * @param {Profile} profile - The profile object from the API.
 */
function authStateFromProfile(profile: UserProfile): HypothesisAuthState {
  return authStateFromUserProfile(profile);
}

let SPELLCHECKER_TYPE = {
  qas: "qas",
  wsc: "wsc",
  disabled: "disabled"
}

@Component({
  selector: 'hypothesis-app',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  public title = 'annotate-client';
  
  private settings: IHypothesisJsonConfig;

  /** 
   * This stores information about the current user's authentication status.
   * When the controller instantiates we do not yet know if the user is
   * logged-in or not, so it has an initial status of 'unknown'. This can be
   * used by templates to show an intermediate or loading state.
   */
  public auth: HypothesisAuthState;

  constructor(private session: SessionService,
    private rootScope: ScopeService,
    settingsService: HypothesisConfigurationService) {
      this.settings = settingsService.getConfiguration();
      this.auth = { status: 'unknown' };
  }

  public ngOnInit(): void {
    const spellCheckerName = this.settings.spellChecker?.name;
    if (spellCheckerName === SPELLCHECKER_TYPE.wsc) {
      _addSpellCheckerConfig(window.document, this.settings.spellChecker?.serviceUrl || '');
      _addSpellCheckerScript(window.document, this.settings.spellChecker?.sourceUrl || '', 'spellChecker-service');
    } else if (spellCheckerName === SPELLCHECKER_TYPE.qas) {
      _addConfigQAS(window.document, this.settings.spellChecker?.serviceUrl || '');
      _addSpellCheckerScript(window.document, this.settings.spellChecker?.sourceUrl || '', 'qas-spellchecker');
    }

    const self = this;
    this.session.load().subscribe((profile: UserProfile) => {
      self.auth = authStateFromProfile(profile);
      // Reload the view when the user switches accounts
      self.rootScope.$on(events.USER_CHANGED, function (event: any, data: any) {
        self.auth = authStateFromProfile(data.profile);
      });
    });
  }
}

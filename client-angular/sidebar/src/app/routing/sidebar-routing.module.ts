import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import StateResolver from '../sidebar/routing/state.resolver';

import {AnnotationViewerContentComponent} from '../sidebar/components/annotation-viewer-content.component';
import {StreamContentComponent} from '../sidebar/components/stream-content.component';
import {SidebarContentComponent} from '../sidebar/components/sidebar-content.component';


const routes: Routes = [
  { path: 'a/:id', component: AnnotationViewerContentComponent, resolve: { state: StateResolver } },
  { path: 'stream', component: StreamContentComponent, resolve: { state: StateResolver } },
  { path: '**', component: SidebarContentComponent, resolve: { state: StateResolver }}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SidebarRoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SidebarModule } from './sidebar/sidebar.module';
import { SidebarComponent } from './sidebar/components/sidebar.component';
import { NgbModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    SidebarModule
  ],
  providers: [
    SidebarComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

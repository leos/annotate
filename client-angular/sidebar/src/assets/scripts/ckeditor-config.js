/**
 * Global variable, that specifies the base path that is used
 * by the ckeditor in order to fetch additional resources.
 * (see https://ckeditor.com/docs/ckeditor4/latest/guide/dev_basepath.html)
 */
var CKEDITOR_BASEPATH = '/annotate/client/ckeditor/';
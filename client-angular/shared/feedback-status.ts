'use strict';

export enum FeedbackStatus {
    IN_PREPARATION = 'IN_PREPARATION',
    SENT = 'SENT',
}

export const IN_PREPARATION = FeedbackStatus.IN_PREPARATION;
export const SENT = FeedbackStatus.SENT;
'use strict';

export enum OperationMode {
    NORMAL = 'NORMAL',
    READ_ONLY = 'READ_ONLY',
    PRIVATE = 'PRIVATE',
    STORED_READ_ONLY = 'STORED_READ_ONLY',
    STORED = 'STORED'
}

export const NORMAL = OperationMode.NORMAL;
export const READ_ONLY = OperationMode.READ_ONLY;
export const STORED_READ_ONLY = OperationMode.STORED_READ_ONLY;
export const STORED = OperationMode.STORED;
export const PRIVATE = OperationMode.PRIVATE;
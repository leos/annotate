'use strict';

/**
 * Defines the set of global events that are dispatched on $rootScope
 */
export enum SidbarEvents {
  // Internal state changes
  FRAME_CONNECTED = 'frameConnected',

  FRAME_CONNECT_ERROR = 'frameConnectError',

  // Session state changes
  /** The list of groups changed */
  GROUPS_CHANGED = 'groupsChanged',

  /** The logged-in user changed */
  USER_CHANGED = 'userChanged',

  /**
  * API tokens were fetched and saved to local storage by another client
  * instance.
  */
  OAUTH_TOKENS_CHANGED = 'oauthTokensChanged',

  // UI state changes

  /** The currently selected group changed */
  GROUP_FOCUSED = 'groupFocused',

  // Annotation events

  /** A new annotation has been created locally. */
  BEFORE_ANNOTATION_CREATED = 'beforeAnnotationCreated',

  /** Annotations were anchored in a connected document. */
  ANNOTATIONS_SYNCED = 'sync',

  /** An annotation was created on the server and assigned an ID. */
  ANNOTATION_CREATED = 'annotationCreated',

  /** An annotation was either deleted or unloaded. */
  ANNOTATION_DELETED = 'annotationDeleted',

  /** Multiple annotations were either deleted or unloaded. */
  ANNOTATIONS_DELETED = 'annotationsDeleted',

  /** An annotation was flagged. */
  ANNOTATION_FLAGGED = 'annotationFlagged',

  /** An annotation has been updated. */
  ANNOTATION_UPDATED = 'annotationUpdated',

  /** A set of annotations were loaded from the server. */
  ANNOTATIONS_LOADED = 'annotationsLoaded',

  /** An annotation is unloaded. */
  ANNOTATIONS_UNLOADED = 'annotationsUnloaded',

  /** An annotation was reset to normal. */
  ANNOTATION_RESET = 'annotationReset',

  /** An annotation was treated. */
  ANNOTATION_TREATED = 'annotationTreated',

  SCROLL_ANNOTATION_INTO_VIEW = 'scrollAnnotationIntoView',

  LEOS_PERMISSIONS_LOADED = 'leosPermissionsLoaded',

  LEOS_RESET_FILTER = 'leosResetFilter',

  TOGGLE_SIDEBAR = 'toggleAnnotatorSidebar'
}

// Internal state changes
export const FRAME_CONNECTED = SidbarEvents.FRAME_CONNECTED;

export const FRAME_CONNECT_ERROR = SidbarEvents.FRAME_CONNECT_ERROR;

// Session state changes

/** The list of groups changed */
export const GROUPS_CHANGED = SidbarEvents.GROUPS_CHANGED;

/** The logged-in user changed */
export const USER_CHANGED = SidbarEvents.USER_CHANGED;

/**
* API tokens were fetched and saved to local storage by another client
* instance.
*/
export const OAUTH_TOKENS_CHANGED = SidbarEvents.OAUTH_TOKENS_CHANGED;

// UI state changes

/** The currently selected group changed */
export const GROUP_FOCUSED = SidbarEvents.GROUP_FOCUSED;

// Annotation events

/** A new annotation has been created locally. */
export const BEFORE_ANNOTATION_CREATED = SidbarEvents.BEFORE_ANNOTATION_CREATED;

/** Annotations were anchored in a connected document. */
export const ANNOTATIONS_SYNCED = SidbarEvents.ANNOTATIONS_SYNCED;

/** An annotation was created on the server and assigned an ID. */
export const ANNOTATION_CREATED = SidbarEvents.ANNOTATION_CREATED;

/** An annotation was either deleted or unloaded. */
export const ANNOTATION_DELETED = SidbarEvents.ANNOTATION_DELETED;

/** Multiple annotations were either deleted or unloaded. */
export const ANNOTATIONS_DELETED = SidbarEvents.ANNOTATIONS_DELETED;

/** An annotation was flagged. */
export const ANNOTATION_FLAGGED = SidbarEvents.ANNOTATION_FLAGGED;

/** An annotation has been updated. */
export const ANNOTATION_UPDATED = SidbarEvents.ANNOTATION_UPDATED;

/** A set of annotations were loaded from the server. */
export const ANNOTATIONS_LOADED = SidbarEvents.ANNOTATIONS_LOADED;

/** An annotation is unloaded. */
export const ANNOTATIONS_UNLOADED = SidbarEvents.ANNOTATIONS_UNLOADED;

/** An annotation was reset to normal. */
export const ANNOTATION_RESET = SidbarEvents.ANNOTATION_RESET;

/** An annotation was treated. */
export const ANNOTATION_TREATED = SidbarEvents.ANNOTATION_TREATED;

export const SCROLL_ANNOTATION_INTO_VIEW = SidbarEvents.SCROLL_ANNOTATION_INTO_VIEW;

export const LEOS_PERMISSIONS_LOADED = SidbarEvents.LEOS_PERMISSIONS_LOADED;

export const LEOS_RESET_FILTER = SidbarEvents.LEOS_RESET_FILTER;

export const TOGGLE_SIDEBAR = SidbarEvents.TOGGLE_SIDEBAR;
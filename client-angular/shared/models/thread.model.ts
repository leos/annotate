import { Annotation } from "./annotation.model";

// Function for comparing annotations to determine their sort order.
export type SortCompareFunction = (a: any, b: any) => boolean;

export type FilterFunction = (annot: Annotation) => boolean;

export type ThreadFilterFunction = (thread: Thread) => boolean;

export type ReplySortCompareFunction = (a: any, b: any) => boolean;

export type AnnotationCompareFunction = (ann1: Annotation, ann2: Annotation) => boolean;

/**
 * State for threads
 */
export interface Thread {
   /**
   * The ID of this thread. This will be the same as the annotation ID for
   * created annotations or the `$tag` property for new annotations.
   */
   id?: string;

   /**
    * The Annotation which is displayed by this thread.
    *
    * This may be null if the existence of an annotation is implied by the
    * `references` field in an annotation but the referenced parent annotation
    * does not exist.
    */
   annotation?: Annotation;

   /** The parent thread ID */
   parent?: Thread;

   /** True if this thread is collapsed, hiding replies to this annotation. */
   collapsed: boolean;

   /** True if this annotation matches the current filters. */
   visible: boolean;

   /** Replies to this annotation. */
   children: Thread[];

   /**
     * The total number of children of this annotation,
     * including any which have been hidden by filters.
     */
   totalChildren: number;

   /**
    * The highlight state of this annotation:
    *  undefined - Do not (de-)emphasize this annotation
    *  'dim' - De-emphasize this annotation
    *  'highlight' - Emphasize this annotation
    */
   highlightState?: string;

   depth?: number;

   replyCount?: number;
}

export type BuildThreadOptions = {
    /**
     * List of IDs of annotations that should be shown even if they
     * do not match the current filter.
     */    
    forceVisible?: string[],

    /**
     * Mapping of annotation IDs to expansion states.
     */
    expanded?: any,

    /** 
     * List of highlighted annotation IDs 
     */
    highlighted?: string[],

    /**
     * Less-than comparison function used to compare annotations in order to sort
     * the top-level thread.
     */
    sortCompareFn?: SortCompareFunction,

    /**
     * Predicate function that returns true if an annotation should be
     * displayed.
     */    
    filterFn?: FilterFunction,

    /**
     * A filter function which should return true if a given annotation and
     * its replies should be displayed.
     */    
    threadFilterFn?: ThreadFilterFunction,

    /**
     * Less-than comparison function used to compare annotations in order to sort
     * replies.
     */
    replySortCompareFn?: ReplySortCompareFunction
}

export type ThreadMapFunction = (thread: Thread) => Thread;
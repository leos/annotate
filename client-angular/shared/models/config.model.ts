export type HypothesisConfigFunction = () => any; 

export type HypothesisConfigWindow = Window & typeof globalThis & { hypothesisConfig: HypothesisConfigFunction };

export type HypothesisServiceConfig = {
    authority?: string,
    apiUrl?: string,
    websocketUrl?: string,
    grantToken?: string,
    icon?: string,
    onHelpRequestProvided?: boolean,
    onProfileRequestProvided?: boolean,
    onLogoutRequestProvided?: boolean,
    // Used for testing purpose
    key?: string
}
            
/**
 * Data to enable the spell checking in the annotation client. 
 */
export type HypothesisSpellChecker = {
    /** Set this value to true to enabled spell checker in the hypothesis app */
    name: string,
    /** URL to the WebSpellChecker service */
    serviceUrl: string,
    /** URL to the wscbundle */
    sourceUrl: string
}
            
/**
 * Enable loading of temporary stored annotations. When set, general annotation loading is disabled.
 */
export type HypothesisTemporaryData = {
    /** ID of the temporary data to read */
    id: string,
    /** Name of the document to request annotations from */
    document: string
}
            
export interface IHypothesisJsonConfig {
    annotations?: string,
    appType?: string,
     /** If not set or `false`, feedback features (including filter) are disabled */
    feedbackActive?: boolean;
    isAngularUI?: boolean;
    sidebarContainer?: string;
    annotationContainer?: string;
    leosDocumentRootNode?: string;
    ignoredTags?: string[];
    allowedSelectorTags?: string;
    editableSelector?: string;
    notAllowedSuggestSelector?: string;
    displayMetadataCondition?: any;
    operationMode?: string;
    showStatusFilter?: boolean;
    showSidebarToggleBtn?:boolean;
    showGuideLinesButton?: boolean;
    disableSuggestionButton?: boolean;
    disableHighlightButton?: boolean;
    annotationPopupDefaultStatus?: string;
    connectedEntity?: string;
    context?: string;
    sidebarAppUrl?: string;
    /** ID of the sidebar app. Allow multiple sidebar instances */ 
    sidebarAppId?: string;
    legFileName?: string;
    /** Base path to use within the app.html */ 
    basePath?: string;

    /** 
     * URL where client assets are served from. Used when injecting the client
     * into child iframes.
     */ 
    assetRoot?: string;
    services?: HypothesisServiceConfig[];
    spellChecker?: HypothesisSpellChecker;
    temporaryData?: HypothesisTemporaryData;
    oauthClientId?: string;
    coordinators?: any[];
    theme?: string;
    usernameUrl?: string;
    branding?: any;
    enableExperimentalNewNoteButton?: boolean;
    rpcAllowedOrigins?: string[];
    apiUrl?: string;
    websocketUrl?: string;
    authDomain?: string;
    googleAnalytics?: any;
}

export interface IHypothesisConfiguration extends IHypothesisJsonConfig {
    readonly annotations: string;

    /** 
     * URL of the client's boot script. Used when injecting the client into
     * child iframes.
     */
    readonly clientUrl: string;

    /** Provide Method */
    readonly onLayoutChange: any;
    readonly openSidebar: boolean;
    readonly query: string;
    readonly showHighlights: string;

    /** 
     * Subframe identifier given when a frame is being embedded into
     * by a top level client
     */ 
    readonly subFrameIdentifier?: string;
}

export interface Feedback {
    text?: string;
    updatedAt?: string;
    updatedByUser?: string;
    updatedByDisplayName?: string;
    status: string;
}
import { ObjectWithId } from './common.model';
import { Feedback } from './feedback.model';

/**
 * Representing the structure for a document link
 */
type AnnotationDocumentLink = {
    /**
     * URI 
     */
    href?: string;
}

type AnnotationLinks = {
    html?: string;
    incontext?: string;
}


/**
 * Representing the structure for a document 
 */
type AnnotationDocument = {
    /**
     * Mainly used for denoting a map of simple metadata values
     */
    metadata?: any,
    title?: string,
    link?: AnnotationDocumentLink[]
}

/**
 * Representing the structure for the path to the annotated text 
 */
type AnnotationTargets = {
    /**
     * URI 
     */
    source?: string;
    selector?: any[];
}

/**
 * Representing the structure for permissions on an annotation 
 */
export type AnnotationPermissions = {
    read?: string[],
    admin?: string[],
    update?: string[],
    delete?: string[] 
}

/**
 * Representing the justification assigned to a suggestion;
 * this property of a {@link Annotation} is null for other annotation types
 */
export type AnnotationJustification = {
    text?: string;
}

/**
 * Representing the entities assigned to a user in UD-repo
 */
export type AnnotationUserEntity = {
    id: string,
    name?: string,
    organizationName?: string,
}

/**
 * Representing user information, e.g. display name (or more information in the future)
 */
export type AnnotationUserInfo = {
    display_name?: string;
    entity_name?: string;
    entities?: AnnotationUserEntity[];
}

/**
 * Providing information about the status of an annotation: state, time of change, ...
 */
type AnnotationStatus = {
    status?: string,
    updated?: string,
    updated_by?: string,
    user_info?: string,
    sent_deleted?: boolean
}

type AnnotationModeration = {
    flagCount?: number,
}



/**
 * Representing the top-level structure for an annotation
 */
export interface Annotation extends ObjectWithId {
    created?: string | number;
    document?: AnnotationDocument;
    text?: string;
    group?: string;
    tags?: string[];
    uri?: string;
    target?: AnnotationTargets[];

    /**
     * List of parent items
     */
    references?: string[];
    feedbackToBeSent?: boolean;
    user?: string;
    permissions?: AnnotationPermissions;
    precedingText?: string;
    succeedingText?: string;
    justification?: AnnotationJustification;
    originGroup?: string;
    feedback?: Feedback;
    feedbackUpdated?: string;
    updated?: string | number;
    linkedAnnot?: string;
    user_info?: AnnotationUserInfo;
    status?: AnnotationStatus;
    forwarded?: boolean;
    forwardJustification?: string;
    replyText?: string;

    // Properties are set while runtime
    $anchorTimeout?: any;
    $leosDeleted?: boolean;
    $selectorMoved?: boolean;
    $comment?: boolean;
    $suggestion?: boolean;
    $highlight?: boolean,
    $orphan?: boolean;
    $pageNote?: boolean;
    $tag?: string;
    showHighlight?: boolean;
    anchoredRangeText?: string;
    moderation?: AnnotationModeration;
    hidden?: boolean;
    highlight?: boolean;
    highlights?: Element[];
    isRangeNotExactMatch?: boolean;
    flagged?: boolean;
    links?: AnnotationLinks;
    hovered?: boolean;
    isReply?: boolean;
    visible?: boolean;
}

export interface Suggestion extends Annotation {
    hasBeenMerged?: boolean;
    anchorInfo?: any;
}
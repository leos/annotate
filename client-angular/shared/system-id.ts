'use strict';

export enum SystemId {
  ISC = 'ISC',
  LEOS = 'LEOS'
}

export const ISC = SystemId.ISC;
export const LEOS = SystemId.LEOS;
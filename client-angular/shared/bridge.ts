'use strict';

import RPC from './frame-rpc';
import { IFrameRPC } from './frame-rpc';

/**
 * The Bridge service sets up a channel between frames and provides an events
 * API on top of it.
 */
export interface IBridgeService {
  /**
   * Destroy all channels created with `createChannel`.
   *
   * This removes the event listeners for messages arriving from other windows.
   */
  destroy(): void;

  /**
   * Create a communication channel between this window and `source`.
   *
   * The created channel is added to the list of channels which `call`
   * and `on` send and receive messages over.
   *
   * @param {Window} source - The source window.
   * @param {string} origin - The origin of the document in `source`.
   * @param {string} token
   * @return {IFrameRPC} - Channel for communicating with the window.
   */
  createChannel(source: Window, origin: string, token: string): IFrameRPC;
  
  /**
   * Make a method call on all channels, collect the results and pass them to a
   * callback when all results are collected.
   *
   * @param {string} method - Name of remote method to call.
   * @param {any[]} args - Arguments to method.
   * @param [Function] callback - Called with an array of results.
   */
  call(method: string, ...args: any[]): Promise<any>;

  /**
   * Register a callback to be invoked when any connected channel sends a
   * message to this `Bridge`.
   *
   * @param {string} method
   * @param {Function} callback
   * @returns {IBridgeService} Object itself
   */
  on(method: string, callback: Function): IBridgeService;

  /**
   * Unregister any callbacks registered with `on`.
   *
   * @param {string} method
   */
  off(method: string): IBridgeService;

  /**
   * Add a function to be called upon a new connection.
   *
   * @param {Function} callback
   */
  onConnect(callback: Function): IBridgeService;
}


/**
 * The Bridge service sets up a channel between frames and provides an events
 * API on top of it.
 */
export class DefaultBridgeService implements IBridgeService {
  links: any[];

  channelListeners: any;

  private onConnectListeners: any;

  constructor() {
    this.links = [];
    this.channelListeners = {};
    this.onConnectListeners = [];
  }

  destroy() {
    Array.from(this.links).map((link) =>
      link.channel.destroy());
  }

  createChannel(source: Window, origin: string, token: string): IFrameRPC {
    var channel: IFrameRPC;
    var connected = false;
    const self = this;

    var ready: Function = (): void => {
      if (connected) { return; }
      connected = true;
      (Array.from(self.onConnectListeners) as any[]).forEach((cb: Function) => {
        cb.call(null, channel, source)
      });
    };

    var connect: Function = (_token: string, cb: Function) => {
      if (_token === token) {
        cb();
        ready();
      }
    };

    var listeners = Object.assign(this.channelListeners, { connect });

    // Set up a channel
    channel = new RPC(window, source, origin, listeners);

    // Fire off a connection attempt
    channel.call('connect', token, ready);

    // Store the newly created channel in our collection
    this.links.push({
      channel,
      window: source,
    });

    return channel;
  }

  call(method: string, ...args: any[]): Promise<any> {
    var cb: Function | null = null;
    const self = this;
    if (typeof(args[args.length - 1]) === 'function') {
      cb = args[args.length - 1];
      args = args.slice(0, -1);
    }

    var _makeDestroyFn = (c: any) => {
      return (error: any) => {
        c.destroy();
        self.links = (Array.from(self.links).filter((l) => l.channel !== c).map((l) => l));
        throw error;
      };
    };

    var promises = this.links.map(function(l) {
      var p = new Promise(function(resolve, reject) {
        var timeout = setTimeout((() => resolve(null)), 60000); //LEOS Change: increase timeout
        try {
          return l.channel.call(method, ...Array.from(args), function(err: any, result: any) {
            clearTimeout(timeout);
            if (err) { return reject(err); } else { return resolve(result); }
          });
        } catch (error) {
          var err = error;
          return reject(err);
        }
      });

      // Don't assign here. The disconnect is handled asynchronously.
      return p.catch(_makeDestroyFn(l.channel));
    });

    var resultPromise: Promise<any> = Promise.all(promises);
    if (!cb) {
      return resultPromise;
    }
    return resultPromise
      .then(results => cb!(null, results))
      .catch(error => cb!(error));
  }

  on(method: string, callback: Function): IBridgeService {
    if (this.channelListeners[method]) {
      throw new Error(`Listener '${method}' already bound in Bridge`);
    }
    this.channelListeners[method] = callback;
    return this;
  }

  off(method: string): IBridgeService {
    delete this.channelListeners[method];
    return this;
  }

  onConnect(callback: Function): IBridgeService {
    this.onConnectListeners.push(callback);
    return this;
  }
}

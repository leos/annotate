'use strict';

export enum ResponseStatus {
  IN_PREPARATION = 'IN_PREPARATION',
  SENT = 'SENT'
}

export const IN_PREPARATION = ResponseStatus.IN_PREPARATION;
export const SENT = ResponseStatus.SENT;

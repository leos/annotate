'use strict';

import { configFuncSettingsFrom } from './config-func-settings-from';
import { isBrowserExtension } from './is-browser-extension';
import { jsonConfigsFrom } from './json-config';
import { HypothesisConfigWindow } from '../models/config.model';

export type HostPageSettingOption = {
    defaultValue?: any,
    allowInBrowserExt?: boolean | null;
}

export interface IHypothesisSettings {
    /**
     * Return the `#annotations:*` ID from the given URL's fragment.
     *
     * If the URL contains a `#annotations:<ANNOTATION_ID>` fragment then return
     * the annotation ID extracted from the fragment. Otherwise return `null`.
     *
     * @return {string|null} - The extracted ID, or null.
     */
    readonly annotations: string;

    /**
     * Return the href URL of the first annotator client link in the given document.
     *
     * Return the value of the href attribute of the first
     * `<link type="application/annotator+html" rel="hypothesis-client">` element in the given document.
     *
     * This URL is used to identify where the client is from and what url should be
     *    used inside of subframes
     *
     * @return {string} - The URL that the client is hosted from
     *
     * @throws {Error} - If there's no annotator link or the first annotator has
     *   no href.
     *
     */  
    readonly clientUrl: string;

    readonly showHighlights: string;

    /**
     * Return the href URL of the first annotator sidebar link in the given document.
     *
     * Return the value of the href attribute of the first
     * `<link type="application/annotator+html" rel="sidebar">` element in the given document.
     *
     * This URL is used as the src of the sidebar's iframe.
     *
     * @return {string} - The URL to use for the sidebar's iframe.
     *
     * @throws {Error} - If there's no annotator link or the first annotator has
     *   no href.
     *
     */  
    readonly sidebarAppUrl: string;

    /**
     * Return the config.query setting from the host page or from the URL.
     *
     * If the host page contains a js-hypothesis-config script containing a
     * query setting then return that.
     *
     * Otherwise if the host page's URL has a `#annotations:query:*` (or
     * `#annotations:q:*`) fragment then return the query value from that.
     *
     * Otherwise return null.
     *
     * @return {string|null} - The config.query setting, or null.
     */  
    readonly query: string;

    /**
     * Reads the host page setting value with given name
     * @param {string} name - Name of the setting to read
     * @param {HostPageSettingOption} options - Additional options like default value and if browser extension is allowed
     *  (an instance of the hypothesis client can be distributed in a browser extension or embedded in a website).
     * @returns {any | null} Value of the host page setting if found else null.
     */
    hostPageSetting(name: string, options?: HostPageSettingOption): any;
}

export class HypothesisSettings implements IHypothesisSettings {
    private jsonConfigs: any;
    private configFuncSettings: any;

    constructor(private _window: HypothesisConfigWindow) {
        this.jsonConfigs = jsonConfigsFrom(_window.document);
        this.configFuncSettings = configFuncSettingsFrom(_window);
    }

    get annotations(): string {
        /** Return the annotations from the URL, or null. */
        const annotationsFromURL = (): string | null => {
            // Annotation IDs are url-safe-base64 identifiers
            // See https://tools.ietf.org/html/rfc4648#page-7
            var annotFragmentMatch = this._window.location.href.match(/#annotations:([A-Za-z0-9_-]+)$/);
            if (annotFragmentMatch) {
                return annotFragmentMatch[1];
            }
            return null;
        }
        return this.jsonConfigs.annotations || annotationsFromURL();
    }

    get clientUrl(): string {
        var link = this._window.parent.document.querySelector('link[type="application/annotator+javascript"][rel="hypothesis-client"]') as HTMLLinkElement ??
            this._window.document.querySelector('link[type="application/annotator+javascript"][rel="hypothesis-client"]') as HTMLLinkElement;

        if (!link) {
            throw new Error('No application/annotator+javascript (rel="hypothesis-client") link in the document');
        }

        if (!link.href) {
            throw new Error('application/annotator+javascript (rel="hypothesis-client") link has no href');
        }

        return link.href;
    }

    get showHighlights(): string {
        var showHighlights_ = this.hostPageSetting('showHighlights');
        if (showHighlights_ === null) {
            showHighlights_ = 'always';  // The default value is 'always'.
        }

        // Convert legacy keys/values to corresponding current configuration.
        if (typeof showHighlights_ === 'boolean') {
            return showHighlights_ ? 'always' : 'never';
        }

        return showHighlights_;
    }

    get sidebarAppUrl(): string {
        var link = this._window.parent.document.querySelector('link[type="application/annotator+html"][rel="sidebar"]') as HTMLLinkElement ??
            this._window.document.querySelector('link[type="application/annotator+html"][rel="sidebar"]') as HTMLLinkElement;

        if (!link) {
            throw new Error('No application/annotator+html (rel="sidebar") link in the document');
        }

        if (!link.href) {
            throw new Error('application/annotator+html (rel="sidebar") link has no href');
        }

        return link.href;
    }

    get query(): string {
        /** Return the query from the URL, or null. */
        const queryFromURL = (): string | null => {
            var queryFragmentMatch = this._window.location.href.match(/#annotations:(query|q):(.+)$/i);
            if (queryFragmentMatch) {
                try {
                    return decodeURIComponent(queryFragmentMatch[2]);
                } catch (err) {
                    // URI Error should return the page unfiltered.
                }
            }
            return null;
        }
        return this.jsonConfigs.query || queryFromURL();
    }  

    hostPageSetting(name: string, options: HostPageSettingOption = { defaultValue: null, allowInBrowserExt: null }): any {
        const allowInBrowserExt = options.allowInBrowserExt || false;
        const hasDefaultValue = typeof options.defaultValue !== 'undefined';

        if (!allowInBrowserExt && isBrowserExtension(this.sidebarAppUrl)) {
            return hasDefaultValue ? options.defaultValue : null;
        }

        if (this.configFuncSettings.hasOwnProperty(name)) {
            return this.configFuncSettings[name];
        }

        if (this.jsonConfigs.hasOwnProperty(name)) {
            return this.jsonConfigs[name];
        }

        if (hasDefaultValue) {
            return options.defaultValue;
        }
        return null;
    }
}

export function settingsFrom(window_: HypothesisConfigWindow): IHypothesisSettings {
    return new HypothesisSettings(window_);
}
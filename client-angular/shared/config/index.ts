'use strict';
import { IHypothesisSettings, settingsFrom } from './hypothesis-settings';
import { IHypothesisConfiguration, HypothesisConfigWindow } from '../../shared/models/config.model'

export const FRAME_DEFAULT_WIDTH = 620;
export const FRAME_DEFAULT_MIN_WIDTH = 220;
export const FRAME_DEFAULT_CLOSE_WITH = 0;
export const LEOS_HOVER_ANNOTATION_COLOR = '#9CE6FF';
export const LEOS_SELECTED_ANNOTATION_COLOR = '#6ABCFA';

/**
 * Reads the Hypothesis configuration from the environment.
 *
 * @param {Window} window_ - The Window object to read config from.
 */
export function configFrom (window_: HypothesisConfigWindow): IHypothesisConfiguration {
    const settings: IHypothesisSettings = settingsFrom(window_);

    return {
        annotations: settings.annotations,
        assetRoot: settings.hostPageSetting('assetRoot', {defaultValue: null, allowInBrowserExt: true}),
        branding: settings.hostPageSetting('branding'),
        clientUrl: settings.clientUrl,
        enableExperimentalNewNoteButton: settings.hostPageSetting('enableExperimentalNewNoteButton'),
        theme: settings.hostPageSetting('theme'),
        usernameUrl: settings.hostPageSetting('usernameUrl'),
        onLayoutChange: settings.hostPageSetting('onLayoutChange'),
        openSidebar: settings.hostPageSetting('openSidebar', {defaultValue: null, allowInBrowserExt: true}),
        query: settings.query,
        services: settings.hostPageSetting('services'),
        showHighlights: settings.showHighlights,
        sidebarAppId: settings.hostPageSetting('sidebarAppId'),
        sidebarAppUrl: settings.sidebarAppUrl,
        spellChecker: settings.hostPageSetting('spellChecker'),
        temporaryData: settings.hostPageSetting('temporaryData'),
        subFrameIdentifier: settings.hostPageSetting('subFrameIdentifier', {defaultValue: null, allowInBrowserExt: true}),
        feedbackActive: settings.hostPageSetting('feedbackActive', { defaultValue: "true", allowInBrowserExt: null }),
        isAngularUI: settings.hostPageSetting('isAngularUI', {defaultValue: false, allowInBrowserExt: null}), // LEOS Change
        sidebarContainer: settings.hostPageSetting('sidebarContainer'), //LEOS Change
        annotationContainer: settings.hostPageSetting('annotationContainer'), //LEOS Change
        leosDocumentRootNode: settings.hostPageSetting('leosDocumentRootNode'), //LEOS Change
        ignoredTags: settings.hostPageSetting('ignoredTags', {defaultValue: [], allowInBrowserExt: null}), //LEOS Change
        allowedSelectorTags: settings.hostPageSetting('allowedSelectorTags', {defaultValue: '*', allowInBrowserExt: null}), //LEOS Change
        editableSelector: settings.hostPageSetting('editableSelector', {defaultValue: '', allowInBrowserExt: null}), // LEOS Change
        notAllowedSuggestSelector: settings.hostPageSetting('notAllowedSuggestSelector', {defaultValue: '*', allowInBrowserExt: null}), // LEOS Change
        displayMetadataCondition: settings.hostPageSetting('displayMetadataCondition', {defaultValue: {}, allowInBrowserExt: null}), // LEOS Change
        operationMode: settings.hostPageSetting('operationMode', {defaultValue: 'NORMAL', allowInBrowserExt: null}), // LEOS Change
        showStatusFilter: settings.hostPageSetting('showStatusFilter', {defaultValue: false, allowInBrowserExt: null}), // LEOS Change
        showSidebarToggleBtn: settings.hostPageSetting('showSidebarToggleBtn', {defaultValue: false, allowInBrowserExt: null}), // LEOS Change
        showGuideLinesButton: settings.hostPageSetting('showGuideLinesButton', {defaultValue: true, allowInBrowserExt: null}), // LEOS Change
        disableSuggestionButton: settings.hostPageSetting('disableSuggestionButton', {defaultValue: false, allowInBrowserExt: null}), // LEOS Change
        disableHighlightButton: settings.hostPageSetting('disableHighlightButton', {defaultValue: false, allowInBrowserExt: null}), // LEOS Change
        annotationPopupDefaultStatus: settings.hostPageSetting('annotationPopupDefaultStatus', {defaultValue: 'HIDE', allowInBrowserExt: null}), // LEOS Change
        connectedEntity: settings.hostPageSetting('connectedEntity', {defaultValue: '', allowInBrowserExt: null}), // LEOS Change
        coordinators: settings.hostPageSetting('coordinators', {defaultValue: [], allowInBrowserExt: null}), // LEOS Change
        context: settings.hostPageSetting('context', {defaultValue: '', allowInBrowserExt: null}), // LEOS Change
        legFileName: settings.hostPageSetting('legFileName', {defaultValue: '', allowInBrowserExt: null}), // LEOS Change
        basePath: settings.hostPageSetting('basePath', {defaultValue: null, allowInBrowserExt: null})
    };
}
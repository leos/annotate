'use strict';

export enum AnnotationPopupDefaultStatus {
    ON = 'ON',
    OFF = 'OFF',
    HIDE = 'HIDE',
}

export const ON = AnnotationPopupDefaultStatus.ON;
export const OFF = AnnotationPopupDefaultStatus.OFF;
export const HIDE = AnnotationPopupDefaultStatus.HIDE;
'use strict';

/**
 * This module defines the set of global events that are dispatched
 * across the bridge between the sidebar and annotator
 */

/**
 * Defines the set of global events that are dispatched
 * across the bridge between the sidebar and annotator
*/
export enum BridgeEvents {
  /**
  * The updated feature flags for the user
  */
  FEATURE_FLAGS_UPDATED = 'featureFlagsUpdated',

  /**
  * The sidebar is asking the annotator to open the partner site help page.
  */
   HELP_REQUESTED = 'helpRequested',

  /** The sidebar is asking the annotator to do a partner site log in
  *  (for example, pop up a log in window). This is used when the client is
  *  embedded in a partner site and a log in button in the client is clicked.
  */
  LOGIN_REQUESTED = 'loginRequested',

  /** The sidebar is asking the annotator to do a partner site log out.
  *  This is used when the client is embedded in a partner site and a log out
  *  button in the client is clicked.
  */
  LOGOUT_REQUESTED = 'logoutRequested',

  /**
  * The sidebar is asking the annotator to open the partner site profile page.
  */
  PROFILE_REQUESTED = 'profileRequested',

  /**
  * The set of annotations was updated.
  */
  PUBLIC_ANNOTATION_COUNT_CHANGED = 'publicAnnotationCountChanged',

  /**
  * The sidebar is asking the annotator to do a partner site sign-up.
  */
  SIGNUP_REQUESTED = 'signupRequested',
}

export const FEATURE_FLAGS_UPDATED = BridgeEvents.FEATURE_FLAGS_UPDATED;
export const HELP_REQUESTED = BridgeEvents.HELP_REQUESTED;
export const LOGIN_REQUESTED = BridgeEvents.LOGIN_REQUESTED;
export const LOGOUT_REQUESTED = BridgeEvents.LOGOUT_REQUESTED;
export const PROFILE_REQUESTED = BridgeEvents.PROFILE_REQUESTED;
export const PUBLIC_ANNOTATION_COUNT_CHANGED = BridgeEvents.PUBLIC_ANNOTATION_COUNT_CHANGED;
export const SIGNUP_REQUESTED = BridgeEvents.SIGNUP_REQUESTED;
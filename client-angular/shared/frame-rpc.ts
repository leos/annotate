'use strict';

/* eslint-disable */

/** This software is released under the MIT license:

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 */

/**
 * This is a modified copy of index.js from
 * https://github.com/substack/frame-rpc (see git log for the modifications),
 * upstream license above.
 */

var VERSION = '1.0.0';

type RPCMessage = {
    protocol: string,
    version: string,
    sequence: number,
    method?: string,
    response?: number,
    arguments: any[]   
}

export interface IFrameRPC {
    readonly src: Window;
    readonly dst: Window;
    readonly origin: string;

    destroy(): void;
    call(method: string, ...args: any[]): void;
    apply(method: string, ...args: any[]): void;
}

export default class FrameRPC implements IFrameRPC {
    readonly src: Window;
    readonly dst: Window;
    readonly origin: string;

    private _sequence: number;
    private _callbacks: any;
    private _methods: any;
    private _destroyed: boolean;
    private _onMessage: (ev: MessageEvent<RPCMessage>) => void;

    constructor(src: Window, dst: Window, origin: string, methods: any) {
        this.src = src;
        this.dst = dst;
        this._destroyed = false;

        if (origin === '*') {
            this.origin = '*';
        }
        else {
            var uorigin = new URL(origin);
            this.origin = uorigin.protocol + '//' + uorigin.host;
        }

        this._sequence = 0;
        this._callbacks = {};

        var self = this;
        const onMessage = (ev: MessageEvent<RPCMessage>) => {
            if (self._destroyed) return;
            if (self.dst !== ev.source) return;
            if (self.origin !== '*' && ev.origin !== self.origin) return;
            if (!ev.data || typeof ev.data !== 'object') return;
            if (ev.data.protocol !== 'frame-rpc') return;
            if (!Array.isArray(ev.data.arguments)) return;
            self._handle(ev.data);
        }

        this.src.addEventListener('message', onMessage);
        this._onMessage = onMessage;
        this._methods = (typeof methods === 'function'
            ? methods(this)
            : methods
        ) || {};
    }

    destroy() {
        this._destroyed = true;
        this.src.removeEventListener('message', this._onMessage);
    }

    call(method: string, ...args: any[]) {
        this.apply(method, ...args);
    }

    apply(method: string, ...args: any[]) {
        if (this._destroyed) return;
        var seq = this._sequence ++;

        if (typeof args[args.length - 1] === 'function') {
            this._callbacks[seq] = args[args.length - 1];
            args = args.slice(0, -1);
        }

        let rpcMessage: any = {
            protocol: 'frame-rpc',
            version: VERSION,
            sequence: seq,
            method: method,
            arguments: args
        };
        this.dst.postMessage(rpcMessage, this.origin);
    }

    private _handle(msg: RPCMessage) {
        if (this._destroyed) return;
        if (msg.method) {
            if (!this._methods.hasOwnProperty(msg.method)) return;
            const self = this;
            var args = msg.arguments.concat(function () {
                let rpcMessage: any = {
                    protocol: 'frame-rpc',
                    version: VERSION,
                    response: msg.sequence,
                    arguments: [].slice.call(arguments)
                }
                self.dst.postMessage(rpcMessage, self.origin);
            });
            self._methods[msg.method || ''].apply(self._methods, args);
        }
        else if (msg.hasOwnProperty('response')) {
            var cb = this._callbacks[msg.response || -1];
            delete this._callbacks[msg.response || -1];
            if (cb) cb.apply(null, msg.arguments);
        }
    };
}
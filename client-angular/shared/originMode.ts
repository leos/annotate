'use strict';

export enum OriginMode {
    PRIVATE = 'private'
}

export const PRIVATE = OriginMode.PRIVATE;